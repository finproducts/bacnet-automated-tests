import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"tasks()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The tasks() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "tasks() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"tasks() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.numRoutines  >=  0  &&  
  row.numRoutinesErrs  >=  0  &&  
  row.numVarErrs  >=  0  &&  
  row.numVars  >=  0  &&  
  row.programRef  !==  undefined  &&  
  row.targetRef  !==  undefined  &&  
  row.task  ===  "✓"  &&  
  row.taskStatus  !==  undefined,
  
  "tasks() query results contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | numRoutines: "  +  
  row.numRoutines  +  " | numRoutinesErrs: "  +  
  row.numRoutinesErrs  +  " | numVarErrs: "  + 
  row.numVarErrs  +  " | numVars: "  +  
  row.numVars  +  " | programRef: "  +  
  row.programRef  +  " | targetRef: "  +  
  row.targetRef  +  " | task: "  +  
  row.task  +  " | taskStatus: "  +  
  row.taskStatus);
}

}
);
  });  

});