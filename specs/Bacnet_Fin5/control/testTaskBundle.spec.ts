import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let programRef= 'read(program and name=="cityCenterAHU1VavRollUp")->id';

let targetRef= 'readAll(equip and navName=="AHU-1" and siteRef->dis=="City Center")->id';

let bundle= "ahu";

let parameter= programRef  +  ", "  +  targetRef  +  ", "  +  JSON.stringify(bundle);



let po = new axonPO({"testQuery":"taskBundle("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The taskBundle() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "taskBundle("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"taskBundle("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.airFlow  !==  undefined  &&  
  row.damper  !==  undefined  &&  
  row.occMode  !==  undefined  &&  
  row.roomSetpoint  !==  undefined  &&  
  row.targetRefDis  !==  undefined  &&  
  row.targetRef  !==  undefined,
  
  "taskBundle("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => airFlow: "  +  
  row.airFlow  +  " | airflowSetpoint: "  +  
  row.airflowSetpoint  +  " | damper: "  +  
  row.damper  +  " | deltaAirFlow: "  +  
  row.deltaAirFlow  +  " | deltaTemp: "  +  
  row.deltaTemp  +  " | occMode: "  +  
  row.occMode  +  " | roomSetpoint: "  +  
  row.roomSetpoint  +  " | roomTemp: "  +  
  row.roomTemp  +  " | targetRefDis: "  +  
  row.targetRefDis  +  " | targetRef: "  +  
  row.targetRef);
}

}
);
  });  

});