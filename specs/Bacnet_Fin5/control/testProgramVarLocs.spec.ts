import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let programName= "overHeatedZone";

let programId= "read(program and name=="  +  JSON.stringify(programName)  +  ")->id";

let randomParameter= programId;



let po = new axonPO({"testQuery":"programVarLocs("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The programVarLocs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "programVarLocs("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"programVarLocs("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.block  !==  undefined  &&  
  row.line  >=  0  &&  
  row.name  !==  undefined  &&  
  row.routine  ===  "main",
  
  "programVarLocs("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => block: "  +  
  row.block  +  " | line: "  +  
  row.line  +  " | name: "  +  
  row.name  +  " | routine: "  +  
  row.routine);
}

}
);
  });  

});