import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let program= "readAll(program)[3]->id";

let target= "readAll(vav)[1]->id";

let parameter= program  +  ","  +  target;



let po = new axonPO({"testQuery":"taskDiagram("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The taskDiagram() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "taskDiagram("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"taskDiagram("  +  parameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.index  >=  0  &&  
  row.type  !==  undefined,
  
  "taskDiagram("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | index: "  +  
  row.index  +  " | type: "  +  
  row.type);
}

}
);
  });  

});