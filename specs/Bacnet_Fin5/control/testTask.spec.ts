import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let programName= "overHeatedZone";

let programId= "read(program and name=="  +  JSON.stringify(programName)  +  ")->id";

let targetFilter= "read(equip and hvac and vav)";

let randomParameter= programId  +  ","  +  targetFilter;



let po = new axonPO({"testQuery":"task("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The task() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "task("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"task("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.kind  !==  undefined  &&  
  row.name  !==  undefined  &&  
  row.varStatus  ===  "ok"  &&  
  row.val  !==  undefined,
  
  "task("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => kind: "  +  
  row.kind  +  " | name: "  +  
  row.name  +  " varStatus: "  +  
  row.varStatus  +  " | val: "  +  
  row.val);
}

}
);
  });  

});