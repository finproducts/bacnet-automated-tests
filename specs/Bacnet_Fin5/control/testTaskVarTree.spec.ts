import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//not OK

//point id
let pointId= 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon faction
let randomParameter= pointId;



let po = new axonPO({"testQuery":"taskVarTree("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The taskVarTree() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "taskVarTree("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"taskVarTree("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.level  >=  0  &&  row.type  !==  undefined,
  
  "taskVarTree("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | level: "  +  
  row.level  +  " | type: "  + 
   row.type  +  " | kind: "  +  
   row.kind  +  " | binding: "  +  
   row.binding  +  " | name: "  +  
   row.name);
}

}
);
  });  

});