import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"blocks()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The blocks() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "blocks() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"blocks() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.block  ===  "✓"  &&  
row.blockStatus  ===  "ok"  &&  
row.color  !==  undefined  &&  
row.dis  !==  undefined  &&  
row.doc  !==  undefined  &&  
//row.ext  ===  "control"  &&  
row.name  !==  undefined  &&  
row.props  !==  undefined,

"blocks() query results contains valid data on row "  +  i  +  " => block: "  +  
row.block  +  " | blockStatus: "  +  
row.blockStatus  +  " | color: "  +  
row.color  +  " | dis: "  +  
row.dis  +  " | doc: "  +  
row.doc  +  " | ext: "  +  
//row.ext  +  " | name: "  +  
row.name  +  " | props: "  +  
row.props);
}

}
);
  });  

});