import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//overHeatedZone = "readAll(program)[3]->id"
//JB Tower Vav-04 = "readAll(vav)[3]->id"


let program= "readAll(program)[3]->id";

let target= "readAll(vav)[1]->id";

let routineName= "main";

let parameter= program  +  ","  +  target  +  ","  +  JSON.stringify(routineName);



let po = new axonPO({"testQuery":"runRoutine("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The runRoutine() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "runRoutine("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"runRoutine("  +  parameter  +  ") returned and object which doesn't contain an error.");

ok(testData[0].val.includes('fan.controlExt.TasksActor@')  ===  true  &&  
testData[0].val.includes('[controlExt::TasksActor]')  ===  true,
"runRoutine("  +  parameter  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});