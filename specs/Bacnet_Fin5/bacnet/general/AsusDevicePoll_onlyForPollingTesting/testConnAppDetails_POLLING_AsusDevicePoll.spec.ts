import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
Add the marker tag "bacnetForcePolling" on the connector AsusDevicePoll to enable polling
If the connector has connStatus "down" or any other status besides "ok" the info regarding Polling will not be displayed
Use bacnetPing(read(bacnetConn and dis=="AsusDevicePoll")) to change connStatus to "ok"
*/


//bacnet conn dis
let connDis = "AsusDevicePoll";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//argument passed to the axon function
let parameter = "\"bacnet\", read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";



let po = new axonPO({ "testQuery": "bacnetPing(" + bacnetConn + ")", "confirmQuery": "connAppDetails(" + parameter + ")" });
po.setup(function (instance) { }
)

describe('The connAppDetails_POLLING_AsusDevicePoll() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
 await po.assert(async function (testData, confirmData) {
    await ok(confirmData !== undefined && confirmData !== null,
        "connAppDetails(" + parameter + ") query result is not undefined or null.");

    await ok(typeof confirmData === "object" && confirmData.type !== "error",
        "connAppDetails(" + parameter + ") returned an object which doesn't contain an error.");

    await ok(confirmData[0].val.includes("isCOVEnabled:      false") === true &&
        confirmData[0].val.includes('supportsCOV:       true') === true &&
        confirmData[0].val.includes("deviceName:        " + connDis) === true &&
        confirmData[0].val.includes('defWriteDelay:     20sec') === true,

        "connAppDetails(" + parameter + ") query contains valid data => val: " +
        confirmData[0].val + "" + " " + "" + connDis + " connector has Polling enabled.");
    }
    );
  });

});