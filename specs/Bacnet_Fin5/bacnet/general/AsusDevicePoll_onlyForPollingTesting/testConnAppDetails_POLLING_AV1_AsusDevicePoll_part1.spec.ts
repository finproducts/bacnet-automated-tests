import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

//OK


/*
Point must be in watch!!!
Add the marker tag "bacnetForcePolling" on the connector AsusDevicePoll to enable polling
Use bacnetPing(read(bacnetConn and dis=="AsusDevicePoll")) to change the connStatus of the connector to "ok"
Point targeted: AV1
After re-adding the connector in DB Builder, drag the point in the nav tree again.
*/


//bacnet point targeted
let bacnetPoint = "AV1";

//bacnet conn dis
let connDis = "AsusDevicePoll";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//argument passed to the axon function
let parameter = "\"bacnet\", read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id";



let po = new axonPO({ "testQuery": "bacnetPing(" + bacnetConn + ")", "confirmQuery": "connAppDetails(" + parameter + ")" });
po.setup(function (instance) { }
)

describe('The testConnAppDetails_POLLING_AV1_AsusDevicePoll_part1() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
  await  po.assert(async function (testData, confirmData) {
      await ok(confirmData !== undefined && confirmData !== null,
        "connAppDetails(" + parameter + ") query result is not undefined or null.");

     await ok(typeof confirmData === "object" && confirmData.type !== "error",
        "connAppDetails(" + parameter + ") returned an object which doesn't contain an error.");

      await ok(confirmData[0].val.includes("isPolling: true") === true &&
        confirmData[0].val.includes("bacnetCur:      " + bacnetPoint) === true &&
        confirmData[0].val.includes("conn:           " + connDis) === true &&
        confirmData[0].val.includes("isPollingDegraded: false") === true &&
        confirmData[0].val.includes("watchStatus:  isPolling") === true,
        "connAppDetails(" + parameter + ") query contains valid data => val: " +
        confirmData[0].val + "" + " " + "" + "Point " + bacnetPoint + " from " + connDis + " is in Polling.");
    }
    );
  });

});