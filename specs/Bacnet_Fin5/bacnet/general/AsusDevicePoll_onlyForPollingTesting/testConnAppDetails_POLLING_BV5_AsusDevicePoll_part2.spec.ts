import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//OK

/*
Point must be in watch!!!
Add the marker tag "bacnetForcePolling" on the connector AsusDevicePoll to enable polling
Use bacnetPing(read(bacnetConn and dis=="AsusDevicePoll")) to change the connStatus of the connector to "ok"
Point targeted: BV3
After re-adding the connector in DB Builder, drag the point in the nav tree again.
*/


//bacnet point targeted
let bacnetPoint = "BV5";

//bacnet conn dis
let connDis = "AsusDevicePoll";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//new value for object property
let paramArray = [0, 1];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let newValue = paramArray[randomIndex];

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", \"present_value\", " + newValue;



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal" });
po.setup(function (instance) { }
)

describe('The bacnetWriteObjectProperty() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

await it('"Returns correct data when executed"', async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

      if (newValue === 0) {
       await ok(confirmData[0].val === false,
          "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "\n" + "Polling worked properly.New value is store in curVal.");
      }

      else
        if (newValue === 1) {
         await ok(confirmData[0].val === true,
            "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "\n" + "Polling worked properly.New value is store in curVal.");
        }



    }
    );
  });

});