import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
BAC-KMC_1_21 connector has COV enabled  
Use bacnetPing(read(bacnetConn and dis=="BAC-KMC_1_21")) to change the connStatus of the connector to "ok"
Point targeted: MSV2
After re-adding the connector in DB Builder, drag the point in the nav tree again.
*/


//bacnet point targeted
let bacnetPoint = "MSV2";

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//new value for object property
let paramArray = [1, 2, 3, 4];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let newValue = paramArray[randomIndex];

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", \"present_value\", " + newValue;



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal" });
po.setup(function (instance) { }
)

describe("The testConnAppDetails_COV_MSV2_BAC-KMC_1_21_part2() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function (testData, confirmData) {
      ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

      if (newValue === 1) {
        ok(confirmData[0].val === "OFF", "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
      }

      else
        if (newValue === 2) {
          ok(confirmData[0].val === "AUTO", "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
        }

        else
          if (newValue === 3) {
            ok(confirmData[0].val === "COOL", "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
          }

          else
            if (newValue === 3) {
              ok(confirmData[0].val === "HEAT", "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
            }





    }
    );
  });

});