import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';




//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = ["AV1", "AV2", "AV3", "AV4", "AV5", "AV6", "AV7", "AV8", "AV9", "AV10",
  "AV11", "AV12", "AV13", "AV14", "AV15", "AV16", "AV17", "AV18", "AV19", "AV20",
  "AV21", "AV22", "AV23", "AV24", "AV25", "AV26", "AV27", "AV28", "AV29", "AV30",
  "AV31", "AV32", "AV33", "AV34", "AV35", "AV36", "AV37", "AV38", "AV39", "AV40",
  "AV41", "AV42", "AV43", "AV44", "AV45", "AV46", "AV47", "AV48", "AV49", "AV50",
  "AV51", "AV52", "AV53", "AV54", "AV55", "AV56", "AV57"];

//bacnet conn dis                    
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`ANALOG_VALUE`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_ANALOG_VALUE_BAC-KMC_1_21() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(async function (testData, confirmData) {
      //testing bacnetLearn(parameter)
    await ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

       await ok(row.bacnetCur !== undefined &&
          row.kind === "Number" &&
          row.point === "✓" &&
          row.bacnetWrite !== undefined &&
          row.bacnetWriteLevel !== undefined,

          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " +
          row.bacnetCur + " | dis: " +
          row.dis + " | is: " +
          row.kind + " | point: " +
          row.point + " | bacnetWrite: " +
          row.bacnetWrite + " | bacnetWriteLevel: " +
          row.bacnetWriteLevel);

        if (row.bacnetCur === "AV1" ||
          row.bacnetCur === "AV2" ||
          row.bacnetCur === "AV3" ||
          row.bacnetCur === "AV4" ||
          row.bacnetCur === "AV5" ||
          row.bacnetCur === "AV6" ||
          row.bacnetCur === "AV7" ||
          row.bacnetCur === "AV8" ||
          row.bacnetCur === "AV9" ||
          row.bacnetCur === "AV10" ||
          row.bacnetCur === "AV11" ||
          row.bacnetCur === "AV12" ||
          row.bacnetCur === "AV13" ||
          row.bacnetCur === "AV14" ||
          row.bacnetCur === "AV15" ||
          row.bacnetCur === "AV16" ||
          row.bacnetCur === "AV17" ||
          row.bacnetCur === "AV18" ||
          row.bacnetCur === "AV19" ||
          row.bacnetCur === "AV20" ||
          row.bacnetCur === "AV21" ||
          row.bacnetCur === "AV22" ||
          row.bacnetCur === "AV23" ||
          row.bacnetCur === "AV24" ||
          row.bacnetCur === "AV25" ||
          row.bacnetCur === "AV26" ||
          row.bacnetCur === "AV27" ||
          row.bacnetCur === "AV28" ||
          row.bacnetCur === "AV29" ||
          row.bacnetCur === "AV30" ||
          row.bacnetCur === "AV31" ||
          row.bacnetCur === "AV32" ||
          row.bacnetCur === "AV33" ||
          row.bacnetCur === "AV34" ||
          row.bacnetCur === "AV35" ||
          row.bacnetCur === "AV36" ||
          row.bacnetCur === "AV37" ||
          row.bacnetCur === "AV38" ||
          row.bacnetCur === "AV39" ||
          row.bacnetCur === "AV40" ||
          row.bacnetCur === "AV41" ||
          row.bacnetCur === "AV42" ||
          row.bacnetCur === "AV43" ||
          row.bacnetCur === "AV44" ||
          row.bacnetCur === "AV45" ||
          row.bacnetCur === "AV46" ||
          row.bacnetCur === "AV47" ||
          row.bacnetCur === "AV48" ||
          row.bacnetCur === "AV49" ||
          row.bacnetCur === "AV50" ||
          row.bacnetCur === "AV51" ||
          row.bacnetCur === "AV52" ||
          row.bacnetCur === "AV53" ||
          row.bacnetCur === "AV54" ||
          row.bacnetCur === "AV55" ||
          row.bacnetCur === "AV56" ||
          row.bacnetCur === "AV57") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});