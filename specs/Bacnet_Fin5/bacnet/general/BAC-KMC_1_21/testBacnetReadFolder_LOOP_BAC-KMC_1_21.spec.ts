import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = ["LP1", "LP2", "LP3", "LP4"];

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`LOOP`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_LOOP_BAC-KMC_1_21() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null, 
        "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error", 
      "bacnetLearn("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray = []

let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

      await ok(row.bacnetCur !== undefined && 
          row.bacnetWrite !== undefined && 
          row.bacnetWriteLevel !== undefined && 
          row.dis !== undefined && 
          row.kind === "Number" && 
          row.point === "✓" && 
          row.writable === "✓", 
          
          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " + 
          row.bacnetCur + " | bacnetWrite: " + 
          row.bacnetWrite + " | bacnetWriteLevel: " + 
          row.bacnetWriteLevel + " | dis: " + 
          row.dis + " | is: " + 
          row.kind + " | point: " + 
          row.point + " | writable: " + 
          row.writable);

        if (row.bacnetCur === "LP1" || 
        row.bacnetCur === "LP2" || 
        row.bacnetCur === "LP3" || 
        row.bacnetCur === "LP4") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});