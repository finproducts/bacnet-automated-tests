import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
drag into DB Builder & path: BAC-KMC_1_21(site) ==> Floor 1 ==> Equip 1 ==> MSV_1
curStatus should have the value "ok" from "stale" after running bacnetSyncCur() 
point tags affected: curStatus, curVal, curErr
*/


//bacnet connector dis
let connDis = "BAC-KMC_1_21";

//MSV_1 from "BAC-KMC_1_21"
let parameter = "read(bacnetCur and kind==\"Str\" and bacnetCur==\"MSV1\" and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")";



let po = new axonPO({ "testQuery": "bacnetSyncCur(" + parameter + ")", "confirmQuery": parameter });
po.setup(function (instance) { }
)

describe("The bacnetSyncCur_enum_MSV1_BAC-KMC_1_21() query", async () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", async () => {
   await po.assert(async function (testData, confirmData) {
   await ok(testData !== undefined && testData !== null,
        "bacnetSyncCur(" + parameter + ") query result is not undefined or null.");

    await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetSyncCur(" + parameter + ") returned an object which doesn't contain an error.");

    await  ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "bacnetSyncCur(" + parameter + ") query returns valid data: " + JSON.stringify(testData));

      let row = confirmData[0]

     await ok(row.dis === connDis + ' Equip 1 OCCUPIED MODE' &&
        row.id !== undefined &&
        row.bacnetCur === "MSV1" &&
        row.cur === "✓" &&
        row.bacnetConnRefDis === connDis &&
        row.kind === "Str" &&
        row.siteRefDis === connDis &&
        row.curStatus === "ok" &&
        row.point === "✓" &&
        row.curErr === undefined &&
        row.writable === "✓" &&
        row.floorRefDis === "Floor 1" &&
        row.navName === "OCCUPIED MODE" &&
        row.bacnetWrite === "MSV1",

        parameter + " query contains valid data => dis: " +
        row.dis + " | id: " +
        row.id + " | bacnetCur: " +
        row.bacnetCur + " | cur: " +
        row.cur + " | bacnetConnRefDis: " +
        row.bacnetConnRefDis + " | kind: " +
        row.kind + " | siteRefDis: " +
        row.siteRefDis + " | is: " +
        row.curStatus + " | curVal: " +
        row.curVal + " | point: " +
        row.point + " | writable: " +
        row.writable + " | floorRefDis: " +
        row.floorRefDis + " | navName: " +
        row.navName + " | bacnetWrite: " +
        row.bacnetWrite + " | equipRefDis: " +
        row.equipRefDis + " | cmd: " +
        row.cmd);
    }
    );
  });

});