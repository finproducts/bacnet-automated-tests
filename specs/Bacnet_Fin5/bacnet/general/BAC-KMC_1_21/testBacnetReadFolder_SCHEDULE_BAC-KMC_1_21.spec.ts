import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//OK

//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = ["SCH1", "SCH2"];

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`SCHEDULE`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The Testing bacnetReadFolder_SCHEDULE_BAC-KMC_1_21() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

       await ok(row.bacnetCur !== undefined &&
          row.description !== undefined &&
          row.dis !== undefined && 
          row.kind !== undefined && row.point === "✓",

          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " +
          row.bacnetCur + " | description: " +
          row.description + " | dis: " +
          row.dis + " | is: " +
          row.kind + " | point: " +
          row.point);

        if (row.bacnetCur === "SCH1" ||
          row.bacnetCur === "SCH2") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});