import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet filter
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//DEV property of the bacnet connector
let dev = "DEV21";

let parameter = bacnetConn + "," + JSON.stringify(dev) + ", \"OBJECT_LIST\"";



let po = new axonPO({ "testQuery": "bacnetReadObjectProperty(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadObjectProperty_OBJECT_LIST_BAC-KMC_1_21() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetReadObjectProperty(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadObjectProperty(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData.length === 270
        && JSON.stringify(testData) === '[{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AI1"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AI2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName"'
        + ':"OBJECT_LIST","value":"AI3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AI4"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AI5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"AI6"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AI7"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"AI8"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AO7"},{"bacnetValueType":"ObjectId"'
        + ',"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AO8"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AO9"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AO10"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"AV1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV2"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"AV3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV4"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV6"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV7"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"AV8"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV9"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"AV10"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV11"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"AV12"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV13"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"AV14"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV15"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV16"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV17"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV18"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"AV19"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV20"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"AV21"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV22"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"AV23"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV24"},{"bacnetValueType":"ObjectId","objectType"'
        + ':"DEVICE","propertyName":"OBJECT_LIST","value":"AV25"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV26"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV27"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV28"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV29"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV30"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV31"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":'
        + '"AV32"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV33"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"AV34"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV35"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"AV36"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV37"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"AV38"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV39"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"AV40"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV41"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV42"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV43"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV44"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV45"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV46"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"AV47"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV48"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"AV49"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV50"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"AV51"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV52"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"AV53"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV54"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV55"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV56"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"AV57"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BO1"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BO2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"BO3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BO4"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"BO5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BO6"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"BV1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV2"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"BV3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV4"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV6"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV7"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV8"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV9"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"BV10"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV11"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"BV12"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV13"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"BV14"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV15"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"BV16"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV17"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV18"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV19"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV20"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV21"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV22"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"BV23"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV24"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"BV25"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV26"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"BV27"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV28"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"BV29"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV30"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV31"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV32"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV33"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV34"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV35"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"BV36"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV37"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"BV38"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV39"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"BV40"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV41"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"BV42"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV43"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV44"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV45"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV46"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV47"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV48"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"BV49"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"BV50"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"6:1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"DEV21"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"EE1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE2"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"EE3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE4"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE6"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE7"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE8"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"EE9"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value"'
        + ':"EE10"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:1001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:2001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName"'
        + ':"OBJECT_LIST","value":"10:2002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:2003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE"'
        + ',"propertyName":"OBJECT_LIST","value":"10:2004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:2005"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:2006"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:4001"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:5001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:5002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:5003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"10:5004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:5005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:6001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:8001"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:9001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:12001"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:15001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:16001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:17001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName"'
        + ':"OBJECT_LIST","value":"10:19001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:20001"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"10:128001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:128002"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:128003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:128004"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:128006"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:128007"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:128008"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:128009"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129001"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129003"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:129005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129006"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:129007"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129008"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129009"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:129010"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:130001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:130002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:130003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:130004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:131001"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:131002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:131003"},'
        + '{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:131004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:132001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:132002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:132003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:132004"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:132005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:132006"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:132007"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:132008"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:142001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:151001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:151002"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:151003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:151004"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:151005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:151006"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:151007"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:151008"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:152001"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:152002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:152003"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:152004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:152005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:152006"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:152007"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:152008"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:153001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:153002"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:153003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:153004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:153005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:153006"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:153007"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:153008"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:154001"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:154002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:154003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:154004"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:154005"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:154006"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:154007"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:154008"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:196001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"10:196002"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"10:196003"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"10:197001"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"LP1"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"LP2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"LP3"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"LP4"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"NC1"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"NC2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"PRG1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"PRG2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"PRG3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"PRG4"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"PRG5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"PRG6"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"PRG7"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"PRG8"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"PRG9"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"PRG10"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"SCH1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"SCH2"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"MSV1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"MSV2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"MSV3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"MSV4"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"MSV8"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"MSV9"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"MSV10"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"TL1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"TL2"},{"bacnetValueType":"ObjectId",'
        + '"objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"TL3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"TL4"},{"bacnetValueType":'
        + '"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"TL5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"TL6"},{'
        + '"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"142:1"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST",'
        + '"value":"142:2"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"142:3"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":'
        + '"OBJECT_LIST","value":"142:4"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"142:5"},{"bacnetValueType":"ObjectId","objectType":"DEVICE",'
        + '"propertyName":"OBJECT_LIST","value":"142:6"},{"bacnetValueType":"ObjectId","objectType":"DEVICE","propertyName":"OBJECT_LIST","value":"142:7"},{"bacnetValueType":"ObjectId","objectType":'
        + '"DEVICE","propertyName":"OBJECT_LIST","value":"142:8"}]',
        "bacnetReadObjectProperty(" + parameter + ") query results contains correct data: " + JSON.stringify(testData));
    }
    );
  });

});