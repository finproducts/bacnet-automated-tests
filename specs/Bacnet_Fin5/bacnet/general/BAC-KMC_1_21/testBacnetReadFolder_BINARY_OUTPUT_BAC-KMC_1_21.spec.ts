import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//ok

//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = ["BO1", "BO2", "BO3", "BO4"];

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`BINARY_OUTPUT`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_BINARY_OUTPUT_BAC-KMC_1_21() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
  await po.assert(async function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

      await ok(row.bacnetCur !== undefined &&
          row.dis !== undefined &&
          row.kind === "Bool" &&
          row.point === "✓" &&
          row.bacnetWrite !== undefined &&
          row.bacnetWriteLevel !== undefined &&
          row.enum !== undefined,

          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " +
          row.bacnetCur + " | dis: " +
          row.dis + " | is: " +
          row.kind + " | point: " +
          row.point + " | bacnetWrite: " +
          row.bacnetWrite + " | bacnetWriteLevel: " +
          row.bacnetWriteLevel + " | enum: " +
          row.enum);

        if (row.bacnetCur === "BO1" ||
          row.bacnetCur === "BO2" ||
          row.bacnetCur === "BO3" ||
          row.bacnetCur === "BO4") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});