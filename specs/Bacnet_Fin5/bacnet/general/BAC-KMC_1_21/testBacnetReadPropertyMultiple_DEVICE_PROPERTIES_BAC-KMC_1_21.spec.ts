import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//expected array
let expectedArray = ["MAX_APDU_LENGTH_ACCEPTED",
  "MODEL_NAME",
  "OBJECT_IDENTIFIER",
  "OBJECT_NAME",
  "PROTOCOL_REVISION",
  "PROTOCOL_SERVICES_SUPPORTED",
  "PROTOCOL_VERSION",
  "SEGMENTATION_SUPPORTED",
  "SYSTEM_STATUS",
  "VENDOR_NAME"];

//bacnet device targeted: "BAC-KMC_1_21"
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//device object (DEV + device instance number of "BAC-KMC_1_21")
let deviceObject = "DEV21";

//bacnet device properties
let deviceProps = "{max_apdu_length_accepted, " +
  "model_name, " +
  "object_identifier, " +
  "object_name, " +
  "protocol_revision, " +
  "protocol_services_supported, " +
  "protocol_version, " +
  "segmentation_supported, " +
  "system_status, " +
  "vendor_name}";

let parameter = bacnetConn + ", {" + JSON.stringify(deviceObject) + ":" + deviceProps + "}";



let po = new axonPO({ "testQuery": "bacnetReadPropertyMultiple(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadPropertyMultiple_DEVICE_PROPERTIES_BAC-KMC_1_21() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetReadPropertyMultiple (" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadPropertyMultiple (" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetValueType !== undefined &&
          row.objectId === deviceObject &&
          row.propertyName !== undefined &&
          row.value !== undefined,

          "bacnetReadPropertyMultiple (" + parameter + ") query contains valid data on row " + i + " => bacnetValueType: " +
          row.bacnetValueType + " | objectId: " +
          row.objectId + " | propertyName: " +
          row.propertyName + " | value: " +
          row.value);

        if (row.propertyName === "MAX_APDU_LENGTH_ACCEPTED" ||
          row.propertyName === "MODEL_NAME" ||
          row.propertyName === "OBJECT_IDENTIFIER" ||
          row.propertyName === "OBJECT_NAME" ||
          row.propertyName === "PROTOCOL_REVISION" ||
          row.propertyName === "PROTOCOL_SERVICES_SUPPORTED" ||
          row.propertyName === "PROTOCOL_VERSION" ||
          row.propertyName === "SEGMENTATION_SUPPORTED" ||
          row.propertyName === "SYSTEM_STATUS" ||
          row.propertyName === "VENDOR_NAME") {
          validArray.push(row.propertyName);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected properties: " + JSON.stringify(validArray));
    }
    );
  });

});