import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
bacnet connector targeted: "BAC-KMC_1_21"
creating a job for the function cacheQueryBacnetPoints()
*/

let connDis = "BAC-KMC_1_21";

let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

let category = "all";

let parameter = bacnetConnId + "," + JSON.stringify(category);



let po = new axonPO({ "testQuery": "backgroundQueryBacnetPoints(" + parameter + ")", "confirmQuery": "bacnetPoint" });
po.setup(function (instance) { }
)

describe('The backgroundQueryBacnetPoints_BAC-KMC_1_21() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "backgroundQueryBacnetPoints(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "backgroundQueryBacnetPoints(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined, "backgroundQueryBacnetPoints(" + parameter + ") query contains valid data => val: " + testData[0].val);

      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]

        ok(row.id !== undefined &&
          row.bacnetCur !== undefined &&
          row.bacnetPoint === "✓" && 
          row.hash !== undefined && 
          row.mod !== undefined &&
          row.objectName !== undefined &&
          row.selected !== undefined,

          "bacnetPoint query result contains valid data on row " + i + " => id: " +
          row.id + " | bacnetCur: " +
          row.bacnetCur + " | bacnetPoint: " +
          row.bacnetPoint + " | hash: " +
          row.hash + " | mod: " +
          row.mod + " | objectName: " +
          row.objectName + " | selected: " +
          row.selected);
      }

    }
    );
  });
});