import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//OK

//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = ["MSV1", "MSV2", "MSV3", "MSV4", "MSV8", "MSV9", "MSV10"];

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`MULTI_STATE_VALUE`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_MULTI_STATE_VALUE_BAC-KMC_1_21() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
  await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

       await ok(row.bacnetCur !== undefined &&
          row.description !== undefined &&
          row.dis !== undefined &&
          row.kind === "Str" &&
          row.point === "✓" &&
          row.enum !== undefined &&
          row.bacnetWrite !== undefined &&
          row.bacnetWriteLevel !== undefined,

          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " +
          row.bacnetCur + " | description: " +
          row.description + " | dis: " +
          row.dis + " | is: " +
          row.kind + " | point: " +
          row.point + " | enum: " +
          row.enum + " | bacnetWrite: " +
          row.bacnetWrite + " | bacnetWriteLevel: " +
          row.bacnetWriteLevel);

        if (row.bacnetCur === "MSV1" ||
          row.bacnetCur === "MSV2" ||
          row.bacnetCur === "MSV3" ||
          row.bacnetCur === "MSV4" ||
          row.bacnetCur === "MSV8" ||
          row.bacnetCur === "MSV9" ||
          row.bacnetCur === "MSV10") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});