import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//Not OK

//Bacnet connector targeted: "AsusDevice" 

//bacnet connector dis
let connDis = "AsusDevice";

let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";



let po = new axonPO({ "testQuery": "bacnetLearn(" + bacnetConn + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetLearn_AsusDevice() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null, 
        "bacnetLearn("  +  bacnetConn  +  "), query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error", 
      "bacnetLearn("  +  bacnetConn  +  "), returned an object which doesn\'t contain an error.");

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(testData[0].dis === "ANALOG VALUE" && 
        testData[1].dis === "BINARY VALUE" && 
        row.learn === row.dis.split(' ').join('_'), 
        
        "bacnetLearn(" + bacnetConn + ") query contains valid data on row " + i + " => dis: " + 
        row.dis + " | learn: " + 
        row.learn);
      }

    }
    );
  });

});