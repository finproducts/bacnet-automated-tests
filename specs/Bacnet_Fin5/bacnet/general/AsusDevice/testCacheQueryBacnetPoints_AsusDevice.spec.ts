import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

//OK


/*
bacnet conn targeted: "AsusDevice"
after running the function, the points targeted by the function will receive a marker tag called "bacnetPoint".
*/

//connector dis
let connDis = "AsusDevice";

//bacnet connector id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//category
let category = "all";

//argument passed to the axon function
let parameter = bacnetConnId + "," + JSON.stringify(category);



let po = new axonPO({ "testQuery": "cacheQueryBacnetPoints(" + parameter + ")", "confirmQuery": "readAll(bacnetPoint and bacnetCur)" });
po.setup(function (instance) { }
)

describe('The cacheQueryBacnetPoints_AsusDevice() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "cacheQueryBacnetPoints(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "cacheQueryBacnetPoints(" + parameter + ") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "cacheQueryBacnetPoints(" + parameter + ") query contains valid data: " + JSON.stringify(testData));

      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]

        ok(row.id !== undefined &&
          row.bacnetCur !== undefined &&
          row.bacnetPoint === "✓" &&
          row.hash !== undefined &&
          row.mod !== undefined &&
          row.objectName !== undefined &&
          row.selected !== undefined,

          "readAll(point and bacnetCur and bacnetConnRef->dis==" + JSON.stringify(connDis) + ") query result contains valid data on row " + i + " => id: " +
          row.id + " | bacnetCur: " +
          row.bacnetCur + " | bacnetPoint: " +
          row.bacnetPoint + " | hash: " +
          row.hash + " | mod: " +
          row.mod + " | objectName: " +
          row.objectName + " | selected: " +
          row.selected);
      }

    }
    );
  });

});