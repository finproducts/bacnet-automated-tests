import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK


/*
 bacnet connector targeted: "AsusDevice"
 */

//connector name
let connDis = "AsusDevice";

//bacnet connector targeted
let bacnetConn = "read(bacnetConn and bacnetDeviceName==" + JSON.stringify(connDis) + ")";

//argument passed to the axon function
let parameter = bacnetConn;



let po = new axonPO({ "testQuery": "finBacnetFormPriorityArray(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finBacnetFormPriorityArray_AsusDevice() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
   await po.assert(async function (testData, confirmData) {
    await  ok(testData !== undefined && testData !== null,
        "finBacnetFormPriorityArray(" + parameter + ") query result is not undefined or null.");

    await  ok(typeof testData === "object" && testData.type !== "error",
        "finBacnetFormPriorityArray(" + parameter + ") returned an object which doesn't contain an error.");
    
     await ok(testData[0].body !== undefined &&
        testData[0].cancelButton === "$okButton"&&
        testData[0].commitAction.includes('bacnet://10.10.200.99:47806/50047') === true &&
        testData[0].commitAction.includes("bacnetDeviceName:" + '\"AsusDevice\"') === true &&
        testData[0].commitButton === "$editButton" &&
        testData[0].dis === "Bacnet Priority Array" &&
        testData[0].finForm === "✓" &&
        testData[0].helpDoc !== undefined &&
        testData[0].name !== undefined,

        "finBacnetFormPriorityArray(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});