import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK


/*
 * Point must be in watch!!!
 * AsusDevice connector has COV enabled
 * Use bacnetPing(read(bacnetConn and dis=="AsusDevice")) to change the connStatus of the connector to "ok"
 * Point targeted: AV4
 * After re-adding the connector in DB Builder, drag the point in the nav tree again.
*/


//bacnet point targeted
let bacnetPoint = "AV4";

//bacnet conn dis
let connDis = "AsusDevice";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//new value for object property
function createRandomNumber() {

  let min = 1;
  let max = 100;
  let getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
};

let newValue = createRandomNumber();

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", \"present_value\", " + JSON.stringify(newValue);



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal" });



po.setup(function (instance) { }
)

describe('The testConnAppDetails_COV_AV4_AsusDevice_part2() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
    await po.assert(async function (testData, confirmData) {

     await ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

    if (confirmData[0] === newValue) {
    await ok(confirmData[0].val === newValue,
        "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + "and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "\n" + "COV worked properly. New value is store in curVal.");
    }
      });
   });
});