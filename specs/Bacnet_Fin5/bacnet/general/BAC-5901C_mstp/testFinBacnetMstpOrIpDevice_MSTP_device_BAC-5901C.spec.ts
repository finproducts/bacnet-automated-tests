import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetDis= "BAC-5901C";

let bacnetConnId= "read(bacnetConn and dis=="  +  JSON.stringify(bacnetDis)  +  ")->id";

let parameter= "["  +  bacnetConnId  +  "]";



let po = new axonPO({"testQuery": "finBacnetMstpOrIpDevice("  +  parameter  +  ")", "confirmQuery": "read(bacnetConn and dis=="  +  JSON.stringify(bacnetDis)  +  ")"});
po.setup(function (instance) {})


describe("testFinBacnetMstpOrIpDevice_MSTP_device_BAC-5901C.js",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Testing finBacnetMstpOrIpDevice_MSTP_device_BAC-5901C", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "finBacnetMstpOrIpDevice("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBacnetMstpOrIpDevice("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData) === "[{\"val\":null}]", "finBacnetMstpOrIpDevice(" + parameter + ") query contains valid data:" + JSON.stringify(testData));

ok(confirmData[0].mstpDevice  ===  "✓"  &&  
confirmData[0].dis  ===  bacnetDis  &&  
confirmData[0].uri.includes('bacnet://')  ===  true,
"read(bacnetConn and dis=="  +  JSON.stringify(bacnetDis)  +  ") query contains v" +
"Valid data => mstpDevice: " + confirmData[0].mstpDevice + " | dis: "  +  confirmData[0].dis  +  " | uri : "  +  confirmData[0].uri  +  
+  " mstpDevice marker tag has been successfully added.");
  });
    });
        });