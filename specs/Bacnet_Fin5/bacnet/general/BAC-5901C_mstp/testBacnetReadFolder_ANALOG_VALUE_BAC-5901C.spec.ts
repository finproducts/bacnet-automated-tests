import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["AV1", "AV2", "AV3", "AV4", "AV5", "AV6", "AV7", "AV8", "AV9","AV10",
                     "AV11", "AV12", "AV13", "AV14", "AV15", "AV16", "AV17", "AV18", "AV19", "AV20",
                     "AV21", "AV22", "AV23", "AV24", "AV25", "AV26", "AV27", "AV28", "AV29", "AV30",
                     "AV31", "AV32", "AV33", "AV34", "AV35", "AV36", "AV37", "AV38", "AV39", "AV40",
                     "AV41", "AV42", "AV43", "AV44", "AV45", "AV46", "AV47", "AV48", "AV49", "AV50",
                     "AV51", "AV52", "AV53", "AV54", "AV55", "AV56", "AV57", "AV58", "AV59", "AV60"];

let connDis= 'BAC-5901C';

let bacnetConn= "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

let folder= '`ANALOG_VALUE`';

let parameter= bacnetConn  +  ","  +  folder;


let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function(instance)
{}
)

describe('testBacnetReadFolder_ANALOG_VALUE_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadFolder_ANALOG_VALUE_BAC-5901C"', () => {
    po.assert(function(testData,confirmData) {
    ok(testData  !==  undefined  &&  testData  !==  undefined,
      "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

    ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
    "bacnetLearn("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetCur  !==  undefined  &&  
   row.is  ===  "ANALOG_VALUE"  &&  
   row.kind  ===  "Number"  &&  row.point  ===  "✓"  &&  
   row.bacnetWrite  !==  undefined  &&  
   row.bacnetWriteLevel  !==  undefined,
   
   "bacnetLearn("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetCur: "  +  
    row.bacnetCur  +  " | dis: "  +  
    row.dis  +  " | is: "  +  
    row.is  +  " | kind: "  +  
    row.kind  +  " | point: "  +  
    row.point  +  " | bacnetWrite: "  +  
    row.bacnetWrite  +  " | bacnetWriteLevel: "  +  
    row.bacnetWriteLevel);

validArray.push(row.bacnetCur);
}

ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),
"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});