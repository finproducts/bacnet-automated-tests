import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= "BAC-5901C";

let parameter= "read(bacnetCur and kind==\"Bool\" and bacnetCur==\"BV1\" and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")";



let po = new axonPO({ "testQuery": "bacnetSyncCur(" + parameter + ")", "confirmQuery": parameter });
po.setup(function(instance)
{}
)

describe('testBacnetSyncCur_bool_BV1_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetSyncCur_bool_BV1_BAC-5901C"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetSyncCur("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetSyncCur("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"bacnetSyncCur("  +  parameter  +  ") query returns valid data: "  +  JSON.stringify(testData));

let row= confirmData[0]

ok(row.dis  ===  connDis  +  " Equip 1 BV_01"  && 
   row.id  !==  undefined  &&  
   row.bacnetCur  ===  "BV1"  &&  
   row.cur  ===  "✓"  &&  
   row.bacnetConnRefDis  ===  connDis  &&  
   row.kind  ===  "Bool"  &&  
   row.siteRefDis  ===  connDis  &&  
   row.is  ===  "BINARY_VALUE"  &&  
   row.curStatus  ===  "ok"  &&  
   row.point  ===  "✓"  &&  
   row.curErr  ===  undefined  &&  
   row.writable  ===  "✓"  &&  
   row.floorRefDis  ===  "Floor 1"  &&  
   row.navName  ===  "BV_01"  &&  
   row.bacnetWrite  ===  "BV1"  &&  
   row.equipRefDis.includes(connDis)  ===  true  &&  
   row.cmd  ===  "✓",parameter  +  
   " query contains valid data => dis: "  +  
   row.dis  +  " | id: "  +  
   row.id  +  " | bacnetCur: "  +  
   row.bacnetCur  +  " | cur: "  +  
   row.cur  +  " | bacnetConnRefDis: "  +  
   row.bacnetConnRefDis  +  " | kind: "  +  
   row.kind  +  " | siteRefDis: "  +  
   row.siteRefDis  +  " | is: "  +  
   row.is  +  " | curStatus: "  +  
   row.curStatus  +  " | curVal: "  +  
   row.curVal  +  " | point: "  +  
   row.point  +  " | writable: "  +  
   row.writable  +  " | floorRefDis: "  +  
   row.floorRefDis  +  " | navName: "  +  
   row.navName  +  " | bacnetWrite: "  +  
   row.bacnetWrite  +  " | equipRefDis: "  +  
   row.equipRefDis  +  " | cmd: "  +  
   row.cmd);
}
);
  });  

});