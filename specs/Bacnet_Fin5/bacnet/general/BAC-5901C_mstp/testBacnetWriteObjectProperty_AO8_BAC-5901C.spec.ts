import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetConn= 'read(bacnetConn and dis=="BAC-5901C")';

let bacnetObject= "AO8";

let objectProperty= 'present_value';

function createRandomNumber() {

	var min = 1;
	var max = 30;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let newValue= createRandomNumber();

let parameter= bacnetConn  +  ","  +  JSON.stringify(bacnetObject)  +  ","  +  JSON.stringify(objectProperty)  +  ","  +  newValue;

let validation= bacnetConn  +  ","  +  JSON.stringify(bacnetObject)  +  ","  +  JSON.stringify(objectProperty);



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "bacnetReadObjectProperty(" + validation + ")" });
po.setup(function(instance)
{}
)

describe('testBacnetWriteObjectProperty_AO8_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetWriteObjectProperty_AO8_BAC-5901C"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetWriteObjectProperty("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetWriteObjectProperty("  +  parameter  +  ") returned an object which doesn''t contain an error.");

ok(testData[0].status  ===  true,
  "bacnetWriteObjectProperty("  +  parameter  +  ") query contains valid data => status "  +  
  testData[0].status  +  ". New value: "  +  newValue);

ok(confirmData[0].bacnetValueType  !==  undefined  &&  
   confirmData[0].objectType  ===  "ANALOG_OUTPUT"  &&  
   confirmData[0].propertyName  ===  objectProperty.toUpperCase()  &&  
   confirmData[0].value  ===  newValue,
   
   "bacnetReadObjectProperty("  +  validation  +  ") query contains valid data => bacnetValueType: "  +  
   confirmData[0].bacnetValueType  +  " | objectType: "  +  
   confirmData[0].objectType  +  " | propertyName: "  +  
   confirmData[0].propertyName  +  " | value: "  +  
   confirmData[0].value);
}
);
  });  

});