import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= 'BAC-5901C';

let bacnetConn= "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";


let po = new axonPO({ "testQuery": "bacnetLearn(" + bacnetConn + ")", "confirmQuery": "" });
po.setup(function(instance)
{}
)

describe('testBacnetLearn_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetLearn_BAC-5901C"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetLearn("  +  bacnetConn  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetLearn("  +  bacnetConn  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(testData[0].dis  ===  "ANALOG INPUT"  &&  
testData[1].dis  ===  "ANALOG OUTPUT"  &&
testData[2].dis  ===  "ANALOG VALUE"  &&  
testData[3].dis  ===  "BINARY INPUT"  &&  
testData[4].dis  ===  "BINARY OUTPUT"  &&
testData[5].dis  ===  "BINARY VALUE"  &&  
testData[6].dis  ===  "LOOP"  &&  
testData[7].dis  ===  "MULTI STATE VALUE"  &&
testData[8].dis  ===  "SCHEDULE"  &&  
testData[9].dis  ===  "TREND LOG"  &&  
row.learn  ===  row.dis.split(' ').join('_'),"bacnetLearn("  +  bacnetConn  +  ") query contains valid data on row "  +  i  +  " => dis: "  +  
row.dis  +  " | learn: "  +  
row.learn);
}

}
);
  });  

});