import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetPoint= "BV3";

let connDis= "BAC-5901C";

let bacnetConn= "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

let parameter = "\"bacnet\", read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id";



let po = new axonPO({ "testQuery": "bacnetPing(" + bacnetConn + ")", "confirmQuery": "connAppDetails(" + parameter + ")" });
po.setup(function (instance) { }
)

describe('testConnAppDetails_COV_BV3_BAC-5901C_part1.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Testing testConnAppDetails_COV_BV3_BAC-5901C_part1", () => {
    po.assert(function(testData,confirmData)
{
ok(confirmData  !==  undefined  &&  confirmData  !==  undefined,
  "connAppDetails("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"connAppDetails("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(confirmData[0].val.includes('isCov: true')  ===  true  
&&  confirmData[0].val.includes("bacnetCur:      "  +  bacnetPoint)  ===  true  
&&  confirmData[0].val.includes("conn:           "  +  connDis)  ===  true  
&&  confirmData[0].val.includes('isPollingDegraded: false')  ===  true  
&&  confirmData[0].val.includes('watchStatus:  isCov')  ===  true,

"connAppDetails(" + parameter + ") query contains valid data => val: " + confirmData[0].val
        + "\n" + " " + "\n" + "Point " + bacnetPoint + " from " + connDis + " is in COV.");
    }
    );
  });

});