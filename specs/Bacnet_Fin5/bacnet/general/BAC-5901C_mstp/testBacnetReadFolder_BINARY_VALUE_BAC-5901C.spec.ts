import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["BV1", "BV2", "BV3", "BV4", "BV5", "BV6", "BV7", "BV8", "BV9","BV10",
                     "BV11", "BV12", "BV13", "BV14", "BV15", "BV16", "BV17", "BV18", "BV19", "BV20",
                     "BV21", "BV22", "BV23", "BV24", "BV25", "BV26", "BV27", "BV28", "BV29", "BV30",
                     "BV31", "BV32", "BV33", "BV34", "BV35", "BV36", "BV37", "BV38", "BV39", "BV40",
                     "BV41", "BV42", "BV43", "BV44", "BV45", "BV46", "BV47", "BV48", "BV49", "BV50",
                     "BV51", "BV52", "BV53", "BV54", "BV55", "BV56", "BV57", "BV58", "BV59", "BV60"];

let connDis= "BAC-5901C";

let bacnetConn= "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

let folder= '`BINARY_VALUE`';

let parameter= bacnetConn  +  ","  +  folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function(instance)
{}
)

describe('testBacnetReadFolder_BINARY_VALUE_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadFolder_BINARY_VALUE_BAC-5901C"', () => {
    po.assert(function(testData,confirmData) {
 ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetLearn("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetCur  !==  undefined  &&  
   row.dis  !==  undefined  &&  
   row.is  ===  "BINARY_VALUE"  &&  
   row.kind  ===  "Bool"  &&  
   row.point  ===  "✓"  &&  
   row.bacnetWrite  !==  undefined  &&  
   row.bacnetWriteLevel  !==  undefined  &&  
   row.enum  ===  "OFF,ON"  &&  
   testData.length  ===  60,
   "bacnetLearn("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetCur: "  +  
   row.bacnetCur  +  " | dis: "  +  
   row.dis  +  " | is: "  +  
   row.is  +  " | kind: "  +  
   row.kind  +  " | point: "  +  
   row.point  +  " | bacnetWrite: "  +  
   row.bacnetWrite  +  " | bacnetWriteLevel: "  +  
   row.bacnetWriteLevel  +  " | enum: "  +  
   row.enum);

validArray.push(row.bacnetCur);
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),
"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});