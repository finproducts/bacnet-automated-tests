import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["MSV1", "MSV2", "MSV3", "MSV4", "MSV5"];

let connDis= "BAC-5901C";

let bacnetConn= "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

let folder= '`MULTI_STATE_VALUE`';

let parameter= bacnetConn  +  ","  +  folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function(instance)
{}
)

describe('testBacnetReadFolder_MULTI_STATE_VALUE_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadFolder_MULTI_STATE_VALUE_BAC-5901C"', () => {
    po.assert(function(testData,confirmData) {
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetLearn("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetCur  !==  undefined  &&  
   row.description  !==  undefined  &&  
   row.dis  !==  undefined  &&  
   row.is  ===  "MULTI_STATE_VALUE"  &&  
   row.kind  ===  "Str"  &&  
   row.point  ===  "✓"  &&  
   row.enum  !==  undefined  &&  
   row.bacnetWrite  !==  undefined  &&  
   row.bacnetWriteLevel  !==  undefined,
   "bacnetLearn("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetCur: "  +  
   row.bacnetCur  +  " | description: "  +  
   row.description  +  " | dis: "  +  
   row.dis  +  " | is: "  +  
   row.is  +  " | kind: "  +  
   row.kind  +  " | point: "  + 
   row.point  +  " | enum: "  +  
   row.enum  +  " | bacnetWrite: "  +  
   row.bacnetWrite  +  " | bacnetWriteLevel: "  +  
   row.bacnetWriteLevel);

validArray.push(row.bacnetCur);
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),
"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});