import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetConn= 'read(bacnetConn and dis=="BAC-5901C")';

let dev= "DEV1";

let parameter = bacnetConn + "," + JSON.stringify(dev) + ", \"OBJECT_LIST\"";

let po = new axonPO({ "testQuery": "bacnetReadObjectProperty(" + parameter + ")", "confirmQuery": "" });

po.setup(function(instance)
{}
)

describe('testBacnetReadObjectProperty_OBJECT_LIST_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadObjectProperty_OBJECT_LIST_BAC-5901C"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  
  testData  !==  undefined,
  "bacnetReadObjectProperty("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  
  testData.type  !==  "error",
  "bacnetReadObjectProperty("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetValueType  ===  "ObjectId"  &&  
row.objectType  ===  "DEVICE"  &&  
row.propertyName  ===  "OBJECT_LIST"  &&  
row.value  !==  undefined  &&  
testData.length  ===  300,
"bacnetReadObjectProperty("  +  parameter  +  ") query results contains correct data => bacnetValueType: "  +  
row.bacnetValueType  +  " | objectType: "  +  
row.objectType  +  " | propertyName: "  +  
row.propertyName  +  " | value: "  +  
row.value);
}

}
);
  });  

});