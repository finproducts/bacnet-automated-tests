import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


let expectedArray= ["STATUS_FLAGS", "BitList(0 0 0 0)", 
                     "OBJECT_NAME", "SCH_01", 
                     "OBJECT_TYPE", 17, 
                     "OBJECT_IDENTIFIER", "SCH1"];



let bacnetConn= 'read(bacnetConn and dis=="BAC-5901C")';

let bacnetObject= "SCH1";

let parameter= bacnetConn  +  ","  +  JSON.stringify(bacnetObject);



let po = new axonPO({ "testQuery": "bacnetReadObject(" + parameter + ")", "confirmQuery": "" });
po.setup(function(instance)
{}
)

describe('testBacnetReadObject_SCH1_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadObject_SCH1_BAC-5901C"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetReadObject("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetReadObject("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetValueType  !==  undefined  &&  
   row.objectType  ===  "SCHEDULE"  &&  
   row.propertyName  !==  undefined,
   "bacnetReadObject("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetValueType: "  +  
   row.bacnetValueType  +  " | objectType: "  +  
   row.objectType  +  " | propertyName: "  +  
   row.propertyName  +  " | value: "  +  
   row.value);

if( row.propertyName  ===  "STATUS_FLAGS"  &&  
    row.value  ===  "BitList(0 0 0 0)"  ||  
    row.propertyName  ===  "OBJECT_NAME"  &&  
    row.value  ===  "SCH_01"  ||  
    row.propertyName  ===  "OBJECT_TYPE"  && 
    row.value  ===  17  ||  row.propertyName  ===  "OBJECT_IDENTIFIER"  && 
    row.value  ===  "SCH1") 
      {
validArray.push(row.propertyName,row.value);
}

    
}

ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),
"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});