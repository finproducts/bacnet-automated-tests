import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';





let connDis= 'Local';

let addedPoint= 'AV1';



let po = new axonPO({"testQuery":"read(point and bacnetCur=="  +  JSON.stringify(addedPoint)  +  " and siteRef->dis=="  +  JSON.stringify(connDis)  +  ")","confirmQuery": ""});
po.setup(function(instance)
{}
)

describe("testDefaultWriteLevelTag_LOCAL_part2.js",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Testing defaultWriteLevelTag part 2", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  
  testData  !==  undefined,
  "read(point and bacnetCur=="  +  JSON.stringify(addedPoint)  +  " and siteRef->dis=="  +  JSON.stringify(connDis)  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  
 testData.type  !==  "error",
 "read(point and bacnetCur=="  +  JSON.stringify(addedPoint)  +  " and siteRef->dis=="  +  JSON.stringify(connDis)  +  ") returned an object which doesn''t contain an error.");

ok(testData[0].dis.includes('AV_1')  ===  false  &&  
testData[0].bacnetCur  ===  addedPoint  &&  
testData[0].bacnetConnRefDis  ===  connDis  &&  
testData[0].point  ===  "✓"  &&  
testData[0].cur  ===  "✓"  &&  
testData[0].siteRefDis  ===  connDis  &&  
testData[0].bacnetWriteLevel  ===  5,
"read(point and bacnetCur=="  +  JSON.stringify(addedPoint)  +  " and siteRef->dis=="  +  JSON.stringify(connDis)  +  ") query contains valid data => dis: "  +  
testData[0].dis  +  " | bacnetCur: "  +  
testData[0].bacnetCur  +  " | bacnetConnRefDis: "  +  
testData[0].bacnetConnRefDis  +  " | point: "  +  
testData[0].point  +  " | cur: "  +  
testData[0].cur  +  " | siteRefDis: "  +  
testData[0].siteRefDis  +  " | bacnetWriteLevel: "  +  
testData[0].bacnetWriteLevel);
}
);
  });  

});