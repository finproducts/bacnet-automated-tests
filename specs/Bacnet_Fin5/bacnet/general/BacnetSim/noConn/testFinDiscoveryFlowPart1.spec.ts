import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//Content finDiscoveryFlow:
//1.finBeginDiscovery()
//2.finDiscoveryStatusCheck()
//3.finDiscoveryAdd()
//4.finDiscoverySessionEnd()
//5.finDiscoverySessionCleanup()
//6.finDiscovery

//file system node module
let fs = require('fs');

//connector name
let connName = 'bacnet';

//argument passed to the axon function
let parameter = JSON.stringify(connName);

let po = new axonPO({"testQuery": "finBeginDiscovery("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBeginDiscovery() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finBeginDiscovery("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finBeginDiscovery("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined 
        && row.extName === "bacnet" 
        && row.extRef !== undefined 
        && row.finDiscovery === "✓" 
        && row.mod !== undefined 
        && row.status === "running" 
        && row.dis === "bacnet Discovery Session." 
        && row.idDis === "bacnet Discovery Session.", 
        "finBeginDiscovery("+ parameter +") query result contains invalid data on row "+ i +" => id: "+
        row.id +" | dis: "+ row.dis +" | extName: "+ row.extName +" | extRef: "+ row.extRef +" | finDiscovery: "+
        row.finDiscovery +" | mod: "+ row.mod +" | status: "+ row.status +" | idDis"+ row.idDis);
      }

      //writing the discovery id in finDiscoveryId.txt file
      let discoveryId = testData[0].id
      let txtFile = '.finDiscoveryId.txt'
      fs.writeFileSync(txtFile, discoveryId, 'utf8');
    });
  });  
});