import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



/*
 * Restarting the Finstack service
 * Make sure you have the FIN|Stack Windows Support pod enabled
 */

let po = new axonPO({"testQuery":"finWinRestartFinstack()", "confirmQuery":""});
po.setup(function(instance){});

describe('The finWinRestartFinstack() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finWinRestartFinstack() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finWinRestartFinstack() returned an object which doesn't contain an error.");

      ok(testData[0].val === "FINFramework restarting...", 
      "finWinRestartFinstack() query results contains invalid data => val: "+ testData[0].val);
    });
  });  
});