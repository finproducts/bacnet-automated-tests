import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let filter = 'point and bacnetCur';
let parameter = JSON.stringify(filter);

let po = new axonPO({"testQuery": "finFormUpdateBacnetTemplateInfoApply("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finFormUpdateBacnetTemplateInfoApply() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormUpdateBacnetTemplateInfoApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "finFormUpdateBacnetTemplateInfoApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"val":null}]', 
      "finFormUpdateBacnetTemplateInfoApply("+ parameter +") query result contains valid data: "+ JSON.stringify(testData));
    });
  });  
});