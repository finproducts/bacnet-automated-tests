import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * Validating DB Builder structure
 */

//site name
let siteName= 'Apogee TC16';

//equip dis
let equipDis= 'AHU';

//floor dis
let floorDis= 'Floor';

//ahu ref dis
let ahuRefDis= 'AHU01';

//tenant name
let tenantName= 'Bucharest';

//room number
let roomNumber= '78';


let po = new axonPO({"testQuery": "read(equip and siteRef->dis=="+ JSON.stringify(siteName) +" and vav)", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The read equip and siteRef query', () =>{
  beforeEach(async () => {
    await po.open();
  });

  //testing read(equip and siteRef->dis==siteName)
  it('Returns the correct results when executed', () => {
    po.assert(function(testData, confirmData) {

      ok(testData !== undefined 
      && testData !== null, "read(equip and siteRef->dis=="+ JSON.stringify(siteName) +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "read(equip and siteRef->dis=="+
      JSON.stringify(siteName) +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.id !== undefined 
        && row.siteRefDis === siteName 
        && row.dis === equipDis 
        && row.floorRefDis === floorDis 
        && row.ahuRefDis === ahuRefDis 
        && row.navName === equipDis 
        && row.tenantRefDis === tenantName 
        && row.roomNo === roomNumber, 
        "read(equip and siteRef->dis=="+ JSON.stringify(siteName) +") query result contains invalid data on row "+
        i +" => id: "+ row.id +" | siteRefDis: "+ row.siteRefDis +" | dis: "+ row.dis +" | floorRefDis: "+
        row.floorRefDis +" | ahuRefDis: "+ row.ahuRefDis +" | tenantRefDis: "+ row.tenantRefDis +" | roomNo: "+ row.roomNo);
      }
    });
  });
});