import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//report type: 0 or 1
let paramArray = ["0", "1"];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let reportType = paramArray[randomIndex];
let connGrid = 'readAll(bacnetConn)';
let parameter = reportType + "," + connGrid;

let po = new axonPO({"testQuery": "finNetworkReportForm("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinNetworkReportForm() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNetworkReportForm("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      &&  testData.type !== "error", "finNetworkReportForm("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined 
      && testData[0].cancelButton !== undefined 
      && testData[0].dis === "Network Report" 
      && testData[0].finForm === "✓" 
      && testData[0].width !== undefined 
      && testData[0].name === "finNetworkReportForm", 
      "finNetworkReportForm("+ parameter +") query result contains invalid data => body: "+ testData[0].body +" | cancelButton: "+
      testData[0].cancelButton +" | commitAction: "+ testData[0].commitAction +" | commitButton: "+
      testData[0].commitButton +" | dis: "+ testData[0].dis +" | finForm: "+ testData[0].finForm +" | width: "+
      testData[0].width +" | name: "+ testData[0].name);
    });
  });  
});