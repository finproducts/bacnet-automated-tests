import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
creating a bacnet device from JB Tower Vav-17
Bacnet server ext must be enabled in Settings (BACnet Server pod) !!!
must undo changes made in order to successfully re-run the test (delete bacnetDeviceRef from the equip) & from "ext"
*/

let today = new Date();
let dd = today.getDate();
let mm = today.getMonth() + 1;
let yyyy = today.getFullYear();

if(dd < 10) {
  dd  = "0" + dd;
}

if(mm < 10) {
  mm = "0" + mm;
}

let currentDate = yyyy + "-" + mm + "-" + dd;
console.log(currentDate);

//stage
let stage = 1;

//equip targeted for bacnet device
let equipTargeted = "[read(vav and siteRef->dis== \"JB Tower\" and navName==\"Vav-17\")->id]";

//instance
let instance= 89;

//mac address
let macAddress= 93;

//iteration
let iteration= 1;

//enable or disable (write local address checkbox)
let enable= false;

//network number
let netNumber = 50;

/* Don't change the network number !!!
 * Make sure when the tests are ran on AsusDevice (192.168.1.22:47806) to use this network number due to the fact
   that the bacnet devices ran on network 50
 */

 //local address
let localAddress = '26';

//argument passed to the axon function
let parameter = stage + "," + equipTargeted + "," + instance + "," + macAddress + "," + iteration + "," + enable +
"," + netNumber + "," + JSON.stringify(localAddress);

let po = new axonPO({"testQuery": "finBacnetAutoCreate("+ parameter +")",
"confirmQuery": "readById(read(vav and siteRef->dis== \"JB Tower\" and navName==\"Vav-17\")->bacnetDeviceRef)"});
po.setup(function(instance) {});

describe('The finBacnetAutoCreate_JB_TOWER_VAV_17() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined
      && testData !== null, "finBacnetAutoCreate("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object"
      && testData.type !== "error",
      "finBacnetAutoCreate("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData)=== "[{\"val\":null}]", "finBacnetAutoCreate("+ parameter +") query result contains invalid data: "+
       JSON.stringify(testData));
       console.log(confirmData[0].dis);

      ok(confirmData[0].dis === "Device("+instance +":"+ macAddress +")" 
         && confirmData[0].id !== undefined
         && confirmData[0].bacnetMacAddress === macAddress
         && confirmData[0].bacnetDevice === "✓"
         && confirmData[0].bacnetInstance === instance
         && confirmData[0].mod.includes(currentDate) === true,
      "readById(read(vav)->bacnetDeviceRef) query results contains invalid data => dis: "+ confirmData[0].dis +" | id: "+
      confirmData[0].id +" | bacnetMacAddress: "+ confirmData[0].bacnetMacAddress +" | bacnetDevice: "+
      confirmData[0].bacnetDevice +" | bacnetInstance: "+ confirmData[0].bacnetInstance +" | mod: "+
      confirmData[0].mod +"\n" + "Bacnet device successfully created.");
    });
  });  
});