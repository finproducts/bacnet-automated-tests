import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//filter
let filter = 'point';

//read or write param array
let arrray = ["read", "write"];
let randomIndex = Math.floor(Math.random() * arrray.length);
let readWrite = arrray[randomIndex];

//argument passed to the axon function
let parameter = JSON.stringify(filter) + "," + JSON.stringify(readWrite);

let po = new axonPO({"testQuery": "backgroundFinFormReadWriteApply("+ parameter +")", "confirmQuery": "jobStatusAll()"});
po.setup(function(instance) {});
describe('The testBackgroundFinFormReadWriteApply() query',() => {

  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined  
      && testData !== null,
      "backgroundFinFormReadWriteApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "backgroundFinFormReadWriteApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined,
      "backgroundFinFormReadWriteApply("+ parameter +") query contains valid data => val: "+ testData[0].val);

      let rowIndex;
      let i;
      let found = false;
      console.log(confirmData);
      console.log(testData[0].val);
      for(i = 0; i < confirmData.length; i++) {
        var row = confirmData[i];
        
        if(row.handle === testData[0].val 
         &&row.jobStatus === "doneOk"
         && row.dis.includes('finFormReadWriteApply') === true
         && row.progressMsg === "Complete"
        ){ 
        found = true;
        rowIndex = i;
        break;
        } 
      };
      equal(found, true, "Running jobStatusAll()... backgroundFinFormReadWriteApply job was successfully founded on row "+ 
      i +" => handle: "+ row.handle +" | jobStatus: "+ row.jobStatus +" | dis: "+ row.dis +" | progress: "+
      row.progress +" | progressMsg: "+ row.progressMsg +" | started: "+ row.started +" | runtime: "+ row.runtime);
    });
  });  
});