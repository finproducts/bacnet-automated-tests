import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * Running bacnet discovery session (with default params)
 */

//file system node module
let fs = require('fs');

//default params for a bacnet discovery session
let parameter = '\"bacnet\", "ver:\\"2.0\\"\\nbroadcastAddress,localAddress,instanceLow,instanceHi,networkNo,discoveryTimeout\\n\\"192.168.251.30\\",\\"0.0.0.0\\",0,4194303,65535,8\\n"';


let po = new axonPO({"testQuery": "finBeginDiscovery("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBeginDiscovery() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {

      ok(testData !== undefined 
      && testData !== null, "finBeginDiscovery("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finBeginDiscovery("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i]
        ok(row.id !== undefined 
        && row.extName === "bacnet" 
        && row.extRef !== undefined 
        && row.finDiscovery === "✓" 
        && row.mod !== undefined 
        && row.status === "running" 
        && row.dis === "bacnet Discovery Session." 
        && row.idDis === "bacnet Discovery Session.", 
        "finBeginDiscovery("+ parameter +") query result contains invalid data on row "+ i +" => id: "+
        row.id +" | dis: "+ row.dis +" | extName: "+ row.extName +" | extRef: "+ row.extRef +" | finDiscovery: "+
        row.finDiscovery +" | mod: "+ row.mod +" | status: "+ row.status +" | idDis" + row.idDis);
      }

      let discoveryId = testData[0].id
      let txtFile = '.finDiscoveryId.txt'
      fs.writeFileSync(txtFile,discoveryId,'utf8');
    });
  });  
});