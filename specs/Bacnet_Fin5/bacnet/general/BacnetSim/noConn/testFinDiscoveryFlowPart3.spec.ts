import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//files system node module
let fs = require('fs');

//read the discovery id saved at step 1 (testFinDiscoveryFlowPart1) in finDiscoveryId.txt file 
let txtFile = '.finDiscoveryId.txt';
let discoveryId = fs.readFileSync(txtFile,'utf8');


let po = new axonPO({"testQuery": "finDiscoverySessionEnd("+ discoveryId +")",
"confirmQuery": "finDiscoverySessionCleanup("+ discoveryId +")"});

po.setup(function(instance) {});

describe('The finDiscoverSessionEnd() query.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== undefined, "finDiscoverySessionEnd("+ discoveryId +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finDiscoverySessionEnd("+ discoveryId +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]", 
      "finDiscoverySessionEnd("+ discoveryId +") query result contains valid data: "+
      JSON.stringify(testData) +"\n"+ "Discovery session has been closed.");

      ok(JSON.stringify(confirmData) ===  "[{\"val\":null}]", 
      "finDiscoverySessionCleanup("+ discoveryId +") query result contains valid data: "+
      JSON.stringify(confirmData) +"\n" + "Discovery session has been cleaned.");
    });
  });  
});