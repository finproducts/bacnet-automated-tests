import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//file system module
let fs = require('fs');

//read the discovery id saved at step 1 (testFinDiscoveryFlowPart1) in finDiscoveryId.txt file 
let txtFile = '.finDiscoveryId.txt';
let discoveryId = fs.readFileSync(txtFile,'utf8');

let po = new axonPO({"testQuery": "finDiscovery", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finDiscovery() query.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finDiscovery query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finDiscovery returned an object which doesn't contain an error.");

      ok(testData[0].finDiscovery === "✓" 
      && testData[0].dis === "bacnet Discovery Session." 
      && testData[0].extName === "bacnet" 
      && testData[0].status === "finished",
      "finDiscovery query result contains invalid data => dis: "+ testData[0].dis +" | finDiscovery: "+
      testData[0].finDiscovery +" | extName: "+ testData[0].extName +" | status: "+ testData[0].status +"\n" +
      "Discovery session with the id "+ JSON.stringify(discoveryId) + "has been successfully erased.");
    });
  });  
});