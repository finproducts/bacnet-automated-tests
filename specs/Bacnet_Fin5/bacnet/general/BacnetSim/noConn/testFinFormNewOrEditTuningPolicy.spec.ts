import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "finFormNewOrEditTuningPolicy()", "confirmQuery":""});
po.setup(function(instance){});

describe('The testFinFormNewOrEditTuningPolicy() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormNewOrEditTuningPolicy() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finFormNewOrEditTuningPolicy() returned an object which doesn't contain an error.");

      ok(testData[0].body.includes('Create New Tuning Policy') === true 
      && testData[0].cancelButton === "$cancelButton" 
      && testData[0].commitAction.includes('finFormCreateTuningPolicy') === true 
      && testData[0].commitButton === "$applyButton" 
      && testData[0].dis === "Create or Edit Tuning Policy" 
      && testData[0].finForm === "✓" 
      && testData[0].name === "tuningPolicy", 
      "finFormNewOrEditTuningPolicy() query result contains valid data => body: " + testData[0].body +" | cancelButton: "+
      testData[0].cancelButton + " | commitAction: "+ testData[0].commitAction +" | commitButton: "+
      testData[0].commitButton +" | dis: "+ testData[0].dis +" | finForm: " + testData[0].finForm +" | helpDoc: "+
      testData[0].helpDoc +" | name: "+ testData[0].name);
    });
  });  
});