import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let filter = 'bacnetCur';
let readWrite = 'read';
let parameter = JSON.stringify(filter) + "," + JSON.stringify(readWrite);

let po = new axonPO({"testQuery": "finFormReadWriteApply("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinFormReadWriteApply() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormReadWriteApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "finFormReadWriteApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"val":null}]', "finFormReadWriteApply("+
      parameter +") query result contains valid data: "+ JSON.stringify(testData));
    });
  });
});