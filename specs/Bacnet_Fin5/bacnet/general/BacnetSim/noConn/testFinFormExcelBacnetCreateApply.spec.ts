import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//fields
let ahuName = "ahu";
let name = "Selenium";
let deviceInstance = "7003";
let floorName = "floor";
let siteName = "New Site";
let roomName = "null";
let host = "10.10.10.211";
let tenantName = "J2";

//argument passed to the axon function
let parameter = "[{ahuName: "+ JSON.stringify(ahuName) + ", name: "+ JSON.stringify(name) +", deviceInstance: "+
deviceInstance + ", floorName: "+ JSON.stringify(floorName) +", siteName: "+ JSON.stringify(siteName) +", roomName: "+
roomName +", host: "+ JSON.stringify(host) +", tenantName: "+ JSON.stringify(tenantName) +"}]";

let po = new axonPO({"testQuery": "finFormExcelBacnetCreateApply("+ parameter +")", "confirmQuery": "read(bacnetConn and dis=="+
JSON.stringify(name) +")"});

po.setup(function(instance) {});

describe('The testFinFormExcelBacnetCreateApply() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormExcelBacnetCreateApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finFormExcelBacnetCreateApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"val":null}]', 
      "finFormExcelBacnetCreateApply("+ parameter +") query result contains invalid data: "+ JSON.stringify(testData));

      //confirming
      ok(confirmData[0].id !== undefined 
      && confirmData[0].dis === name 
      && confirmData[0].description === (siteName +"."+ floorName +"."+ ahuName) 
      && confirmData[0].location === "." + tenantName 
      && confirmData[0].bacnetConn === "✓" 
      && confirmData[0].uri.includes(host) ===  true, 
      "read(bacnetConn and dis=="+ JSON.stringify(name) +") query result contains invalid data => id: "+
      confirmData[0].id +" | dis: "+ confirmData[0].dis +" | description: "+ confirmData[0].description +" | location: "+
      confirmData[0].location +" | bacnetConn: "+ confirmData[0].bacnetConn +" | uri: "+ confirmData[0].uri);
    });
  });
});