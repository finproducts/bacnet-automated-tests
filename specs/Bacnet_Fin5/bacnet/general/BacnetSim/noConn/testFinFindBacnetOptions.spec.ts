import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["IP Addresses", "ip",  
                    //"Hop Counts", "hop", 
                    "Network", "net", 
                    "MAC Addresses", "mac",
                    "Device Instance", "devIn"];

let filter = 'bacnetConn';

//argument passed to the axon function
let parameter = JSON.stringify(filter);

let po = new axonPO({"testQuery": "finFindBacnetOptions("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finFindBacnetOptions() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  //testing finFindBacnetOptions(parameter)
  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFindBacnetOptions("+ parameter +") query result is not undefined or null.");
      ok(typeof testData === "object" 
      && testData.type !== "error", "finFindBacnetOptions("+ parameter +") returned an object which doesn't contain an error.");

      let valuesFound = [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.name !== undefined 
        && row.pos !== undefined 
        && row.val !== undefined, 
        "finFindBacnetOptions("+ parameter +") query result contains invalid data on row "+ i +" => name: "+
        row.name +" | pos: "+ row.pos +" | val: "+ row.val);

        if(row.name === "IP Addresses" && row.val === "ip" 
        || row.name === "Hop Counts" && row.val === "hop" 
        || row.name === "Network" && row.val === "net" 
        || row.name === "MAC Addresses" && row.val === "mac" 
        || row.name === "Device Instance" && row.val === "devIn") { 
          valuesFound.push(row.name,row.val);
        }
      }
      ok(JSON.stringify(valuesFound.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected values: "  +  JSON.stringify(valuesFound.sort()));
    });
  });  
});