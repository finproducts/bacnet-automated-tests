import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//filter
let filter= 'readAll(bacnetConn)';

//argument passed to the axon function
let parameter= filter;

let po = new axonPO({"testQuery": "finBacnetAllNetworkErrCountReports("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBacnetAllNetworkErrCountReports() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData ) {
      ok(testData !== undefined 
      && testData !== null, "finBacnetAllNetworkErrCountReports("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "finBacnetAllNetworkErrCountReports("+ parameter +") returned an object which doesn't contain an error.");

      let i;
      for(i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.network !== undefined
        && row.conn !== undefined
        && row.errors !== undefined
        && row.networkNum !== undefined
        && row.totalPoints !==  undefined,
        "finBacnetAllNetworkErrCountReports("+ parameter +") query result contains invalid data on row "+ i +" => network: "+
        row.network +" | conn: "+ row.conn +" | errors: "+ row.errors +" | networkNum: "+
        row.networkNum +" | totalPoints: "+ row.totalPoints);
      }
    });
  });  
});