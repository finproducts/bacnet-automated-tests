import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//Running the query does not return any tags as of July 2018 so the whole test is invalid

//expected array of tag names
let expectTags = ["presentValue", "objectName", "location", "description", "vendorId", "vendorName"];

let po = new axonPO({"testQuery": "finBacnetDiscoveryTags()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBacnetDiscoveryTags() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData != null, "finBacnetDiscoveryTags() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finBacnetDiscoveryTags() returned an object which doesn't contain an error.");

      let tagsFound= [];

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.object !== undefined 
        && row.property !== undefined 
        && row.tagName !== undefined, 
        "finBacnetDiscoveryTags() query contains invalid data on row "+ i +" => object: "+
        row.object +" | property: "+ row.property +" | tagName: "+ row.tagName);

        if( row.tagName === "presentValue" 
        || row.tagName === "objectName" 
        || row.tagName === "location" 
        || row.tagName === "description" 
        || row.tagName === "vendorId" 
        || row.tagName === "vendorName" 
        || row.tagName === "modelName") {
          if( tagsFound.indexOf(row.tagName) === -1) {
            tagsFound.push(row.tagName);
          }
        } 
      }
      ok(JSON.stringify(tagsFound.sort()) === JSON.stringify(expectTags.sort()), 
      "Query result contains the expected tag names: "+ JSON.stringify(tagsFound));
    });
  });  
});