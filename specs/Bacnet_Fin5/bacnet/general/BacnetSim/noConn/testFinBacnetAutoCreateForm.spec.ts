import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 hardcoded data to trigger the UI form
 the function called with the below parameters doesn't work in Folio app: SyntaxErr:String interpolation not supported yet
 */

//params
let data = '@1a169f42-a1d26b60,[{floorRef: @1a16992d-9cbd4882, hvac, mod: dateTime(2016-05-11, 17:15:58.160, "UTC"),'
+' ahu, level: 0, icon16: `fan://equipExt/res/img/equipHvac.png`, children, indent: 0, disMacro: "\$siteRef \$navName", '
+' dis: "City Center AHU-1", siteRef: @1a169921-22d9c3b8, vavZone, id: @1a169f42-a1d26b60, chillerRef: @1b582753-d543d8a2, '
+' equip, treePath: `equip:/City Center/Floor 1/AHU-1`, navName: "AHU-1"}]';

//argument passed to the axon function
let parameter = data;

let po = new axonPO({"testQuery": "finBacnetAutoCreateForm("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBacnetAutoCreateForm() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  //testing finBacnetAutoCreateForm(parameter)
  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined
      && testData !== null, "finBacnetAutoCreateForm("+ parameter +") query result is not undefined or null.");
      ok(typeof testData === "object"
      && testData.type !== "error", "finBacnetAutoCreateForm("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined 
      && testData[0].commitAction !== undefined
      && testData[0].cancelButton === "$cancelButton"
      && testData[0].commitButton === "$applyButton"
      && testData[0].finForm === "✓"
      && testData[0].dis === "Auto Configure BACnet Devices"
      && testData[0].name === "finBacnetAutoCreateForm",
      "finBacnetAutoCreateForm("+ parameter +") query result contains invalid data => commitAction: "+
      testData[0].commitAction +" | cancelButton: "+ testData[0].cancelButton +" | commitButton: "+
      testData[0].commitButton +" | finForm: "+ testData[0].finForm +" | name: "+ testData[0].name +" | body: "+
      testData[0].body +" | helpDoc: "+ testData[0].helpDoc);
    });
  });  
});