import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



//bacnet connector targeted: "WriteSim"
let connDis = 'WriteSim';

//bacnet conn id
let bacnetConnId = "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")->id";

//selected dicts
let selectedDicts = '[{}]';

//argument passed to the axon function
let parameter = bacnetConnId + "," + selectedDicts;

let po = new axonPO({"testQuery": "finFormUpdateBacnetTemplateInfo("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinFormUpdateBacnetTemplateInfo() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormUpdateBacnetTemplateInfo("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finFormUpdateBacnetTemplateInfo("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined 
      && testData[0].cancelButton !== undefined 
      && testData[0].commitAction !== undefined 
      && testData[0].commitButton !== undefined 
      && testData[0].dis === "Update Bacnet Template Info" 
      && testData[0].finForm === "✓" 
      && testData[0].helpDoc !== undefined 
      && testData[0].name === "finUpdateBacnetTemplateInfoForm", 
      "finFormUpdateBacnetTemplateInfo("+ parameter +") query result contains valid data => body: "+
      testData[0].body +" | cancelButton: "+ testData[0].cancelButton +" | commitAction: "+
      testData[0].commitAction +" | commitButton: "+ testData[0].commitButton +" | dis: "+ testData[0].dis +" | finForm: "+
      testData[0].finForm +" | helpDoc: "+ testData[0].helpDoc +" | name: "+ testData[0].name);
    });
  });  
});