import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 manual function for finBacnetAutoCreate triggering the UI form
 */

//equip id
let equipId = 'read(vav)->id';

//instance
let instance = 1;

//mac address
let macAddress = 2;

//iteration
let iteration = 3;

//enable or disable [write local address checkbox]
let enable = false;

//network number
let netNumber = 4;

//local address
let localAddress = 5;

//argument passed to the axon function
let parameter= "["+ equipId +"],"+ instance +","+ macAddress +","+ iteration +","+ enable +","+ netNumber +","+ localAddress;

let po = new axonPO({"testQuery": "finBacnetAutoCreateManual("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBacnetAutoCreateManual() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined
      && testData !== null, "finBacnetAutoCreateManual("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object"
      && testData.type !== "error", "finBacnetAutoCreateManual("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined
      && testData[0].commitAction !== undefined 
      && testData[0].cancelButton === "$cancelButton" 
      && testData[0].commitButton === "$applyButton" 
      && testData[0].finForm === "✓" 
      && testData[0].dis === "Manually Configure BACnet Devices" 
      && testData[0].name === "finBacnetAutoCreateManual", 
      "finBacnetAutoCreateManual("+ parameter +") query result contains valid data => commitAction: "+
      testData[0].commitAction +" | cancelButton: "+ testData[0].cancelButton +" | commitButton: "+
      testData[0].commitButton +" | finForm: "+ testData[0].finForm +" | name: "+ testData[0].name +" | body: "+
      testData[0].body +" | helpDoc: "+ testData[0].helpDoc);
    });
  });  
});