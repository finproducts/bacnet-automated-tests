import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';





let ip = '192.168.1.99';

let port = '47806';

let deviceInstance = '4';

let networkAddress = '2';

let macAddress = '0"  +  deviceInstance  +  "000000';

let hop = '255';

let connDis = 'BacnetAddedDevice';

let uri = "bacnet://" + ip + ":" + port + "/" + deviceInstance + "?dnet=" + networkAddress + "&dadr=" + macAddress + "&hops=" + hop;

let localAddress = '192.168.1.12';

let parameter = JSON.stringify( connDis ) + "," + JSON.stringify( uri ) + "," + JSON.stringify( localAddress );



let po = new axonPO( { "\"testQuery\"": "\"bacnetAddDevice(\"  +  parameter  +  \")\"", "\"confirmQuery\"": "\"read(bacnetConn and dis==\"  +  JSON.stringify(connDis)  +  \")\"" } );
po.setup( function ( instance ) { }
)

describe( 'testBacnetAddDevice.js', () => {
  beforeEach( async () => {
    await po.open();
  } );

  it( '"Testing bacnetAddDevice"', () => {
    po.assert( function ( testData, confirmData ) {
      ok( testData !== null, 'bacnetAddDevice("  +  parameter  +  ") query result is not undefined or null.' );

      ok( typeof testData === "object" && testData.type !== "error", 'bacnetAddDevice("  +  parameter  +  ") returned an object which doesn\'t contain an error.' );

      ok(  testData[ 0 ].val !== undefined, "bacnetAddDevice(" + parameter + ") query contains valid data => val: " + testData[ 0 ].val );

      ok( confirmData[ 0 ].id === "@" + testData[ 0 ].val && confirmData[ 0 ].bacnetConn === "✓" && confirmData[ 0 ].uri === uri && confirmData[ 0 ].dis === connDis && confirmData[ 0 ].bacnetDevice === connDis && confirmData[ 0 ].mod !== undefined, "read(bacnetConn and dis==" + JSON.stringify( connDis ) + ") query contains valid data => id: " + confirmData[ 0 ].id + " | bacnetConn: " + confirmData[ 0 ].bacnetConn + " | uri: " + confirmData[ 0 ].uri + " | dis: " + confirmData[ 0 ].dis + " | bacnetDevice: " + confirmData[ 0 ].bacnetDevice + " | mod: " + confirmData[ 0 ].mod );
    }
    );
  } );

} );