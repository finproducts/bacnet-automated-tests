import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual, finEvalAsync } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';
import { async } from 'q';
import { findSafariExecutable } from 'selenium-webdriver/safari';


//if the spec fails, please execute "bacnetPoint" in folio and delete all records displayed



let connDis = 'WriteSim';

let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

let category = "all";

let parameter = bacnetConnId + "," + JSON.stringify(category);



let po = new axonPO({ "testQuery": "cacheQueryBacnetPoints(" + parameter + ")", "confirmQuery": "readAll(bacnetPoint and bacnetCur)" });
po.setup(function (instance) { }
)

describe('The cacheQueryBacnetPoints() query',async () => {
  beforeAll(async () => {
    await finEvalAsync("readAll(bacnetPoint).toRecList.map(r => diff(r, null,{remove})).commit");
  });
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
 await  po.assert(async function (testData, confirmData) {
  await ok(testData !== undefined && testData !== null,
        "cacheQueryBacnetPoints(" + parameter + ") query result is not undefined or null.");

  await ok(typeof testData === "object" && testData.type !== "error",
        "cacheQueryBacnetPoints(" + parameter + ") returned an object which doesn't contain an error.");

  await ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "cacheQueryBacnetPoints(" + parameter + ") query contains valid data: " + JSON.stringify(testData));

      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]

    await ok(row.id !== undefined &&
          row.bacnetCur !== undefined &&
          row.bacnetPoint === "✓" &&
          row.hash !== undefined &&
          row.mod !== undefined &&
          row.objectName !== undefined &&
          row.selected !== undefined,

          "readAll(point and bacnetCur and bacnetConnRef->dis==" + JSON.stringify(connDis) + ") query result contains valid data on row " +
          i + " => id: " +
          row.id + " | bacnetCur: " +
          row.bacnetCur + " | bacnetPoint: " +
          row.bacnetPoint + " | hash: " +
          row.hash + " | mod: " +
          row.mod + " | objectName: " +
          row.objectName + " | selected: " +
          row.selected);
      }

    }
    );
  });

});