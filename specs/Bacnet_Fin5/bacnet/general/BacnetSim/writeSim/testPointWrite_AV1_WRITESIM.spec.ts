import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

/*
  bacnet connector used is "WriteSim" & AV1 point (bacnetCur is the name of the point on the device & is unchangeable) 
  the point must be present in DB Builder
  the function writes the new value of the point on the device
  the point from DB Builder will have the new value stored on the property tags "writeVal" & "writeDef": read(point and bacnetCur=="AV1" and bacnetConnRef->dis=="WriteSim")->writeVal
 */


//bacnet connector dis
let connDis = "WriteSim";

//bacnetCur of the point used
let pointBacnetCur = "AV1";

//new value for object property: PRESENT_VALUE
function createRandomNumber() {

  var min = 1;
  var max = 333;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
};

let newValue = createRandomNumber();

//argument passed to the axon function
let parameter = "read(point and bacnetCur=="+ JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id," + newValue + ", 16, \"su\"";

//validating with "bacnetReadObjectProperty" to check if the new value was written on the device
let validation = "read(bacnetConn and dis=="+ JSON.stringify(connDis) + ")," + JSON.stringify(pointBacnetCur) + ",\"PRESENT_VALUE\"";



let po = new axonPO({ "testQuery": "pointWrite(" + parameter + ")", "confirmQuery": "bacnetReadObjectProperty(" + validation + ")" });
po.setup(function (instance) { }
)

describe('The pointWrite_AV1_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing pointWrite(parameter)
      ok(testData !== undefined && testData !== null,
        "pointWrite(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "pointWrite(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].val === newValue,
        "pointWrite(" + parameter + ") query contains valid data => val: " + testData[0].val);

      //validation with bacnetReadObjectProperty
      ok(confirmData[0].bacnetValueType === "Real" &&
        confirmData[0].objectType === "ANALOG_VALUE" &&
        confirmData[0].propertyName === "PRESENT_VALUE" &&
        confirmData[0].value === newValue,

        "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
        confirmData[0].bacnetValueType + " | objectType: " +
        confirmData[0].objectType + " | propertyName: " +
        confirmData[0].propertyName + " | value: " +
        confirmData[0].value);
    }
    );
  });

});