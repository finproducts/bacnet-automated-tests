import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

//OK


//bacnet connector targeted: "WriteSim"
let connDis = "WriteSim";

//new name for the connector targeted
let newName = "Nameless";

//argument passed to the axon function
let parameter = "[{newName:"  +  JSON.stringify(newName)  +  ", id: read(bacnetConn and bacnetDeviceName=="  +  JSON.stringify(connDis)  +  ")->id}]";



let po = new axonPO({ "testQuery": "finRenameBacnetConnsCommit(" + parameter + ")", "confirmQuery": "read(bacnetConn and dis==" + JSON.stringify(newName) + ")" });
po.setup(function (instance) { }
)

describe('The finRenameBacnetConnsCommit_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    console.log(parameter)
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finRenameBacnetConnsCommit(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finRenameBacnetConnsCommit(" + parameter + ") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "finRenameBacnetConnsCommit(" + parameter + ") query result contains valid data: " + JSON.stringify(testData));

      ok(confirmData[0].dis === newName &&
        confirmData[0].bacnetDeviceName === connDis &&
        confirmData[0].uri.includes('10.10.200.241') === true &&
        confirmData[0].bacnetConn === "✓", "read(bacnetConn and dis==" + JSON.stringify(newName) + ") query result contains valid data => dis: " +
        confirmData[0].dis + " | bacnetDeviceName: " +
        confirmData[0].bacnetDeviceName + "" + "Connector name was successfully changed from " + JSON.stringify(connDis) + " to " + JSON.stringify(newName) + ".");
    }
    );
  });

});