import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

/*
 * Validation for point AI1 from WriteSim!
 */

let pointNav = "AI_1";

//connector targeted: "WriteSim"
let connDis = "WriteSim";

//bacnet conn id
let connId = "[read(bacnetConn and dis=="+ JSON.stringify(connDis) +")->id.toStr]";

//offset param
let offset = 0;

//number of points
let numberPoints = 3;

//argument passed to the axon function
let parameter = connId + "," + offset + "," + numberPoints;

//function validation
let validation = "read(point and bacnetConnRef->dis=="+ JSON.stringify(connDis) +" and navName=="+ JSON.stringify(pointNav) +")->id,now()-10year..now()";



let po = new axonPO({ "testQuery" : "finSyncPoints("+ parameter +")","confirmQuery" : "hisRead("+ validation +")" });
po.setup(function (instance) { }
)

describe('The finSyncPoints_WRITESIM_part1() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finSyncPoints(parameter)
      ok(testData !== undefined && testData !== null,
        "finSyncPoints(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finSyncPoints(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].val === null, "finSyncPoints(" + parameter + ") query result contains valid data => val: " + testData[0].val);

      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]

        ok(confirmData[0].ts.includes("2013-01-01") === true &&
          row.ts !== undefined &&
          typeof row.v0 === "number" &&
          row.v0 === (i + 1) &&
          confirmData.length === 50 &&
          row.v0Unit === "°F",

          "hisRead(" + validation + ") query contains valid data on row " + i + " => ts: " +
          row.ts + " | v0: " +
          row.v0 + " | v0Unit: " +
          row.v0Unit);
      }

    }
    );
  });

});