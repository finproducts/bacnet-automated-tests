import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//Not Ok


let bacnetPoint = "MSV3";

let connDis = "WriteSim";

let bacnetConn = "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

let paramArray = [1, 2, 3];

let randomIndex = Math.floor(Math.random() * paramArray.length);

let newValue = paramArray[randomIndex];

let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", \"present_value\", " + newValue;



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal" });
po.setup(function (instance) { }
)

describe('The ConnAppDetails_COV_MSV3_part2() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

      if (newValue === 1) {
        ok(confirmData[0].val === "State_1", "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
      }

      else
        if (newValue === 2) {
          ok(confirmData[0].val === "State_2",
            "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
        }

        else
          if (newValue === 3) {
            ok(confirmData[0].val === "State_3",
              "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
          }




    }
    );
  });

});