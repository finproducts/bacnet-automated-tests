import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK


//bacnet connector targeted: "WriteSim"
let connDis = "WriteSim";

//bacnet connector id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//category
let category = "all";

//argument passed to the axon function
let parameter = bacnetConnId + "," + JSON.stringify(category);



let po = new axonPO({ "testQuery": "queryAllBacnetPoints(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The queryAllBacnetPoints_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null, 
        "queryAllBacnetPoints("  +  parameter  +  ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error", 
      "queryAllBacnetPoints("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetCur !== undefined && 
          row.objectName !== undefined && 
          row.selected !== undefined, 
          
          "queryAllBacnetPoints(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " + 
          row.bacnetCur + " | description: " + 
          row.description + " | objectName: " + 
          row.objectName + " | selected: " + 
          row.selected);
      }

    }
    );
  });

});