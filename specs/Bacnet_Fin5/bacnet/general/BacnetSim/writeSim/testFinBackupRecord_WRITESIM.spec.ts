import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//NOT OK


let connDis = "WriteSim";

let backupProps = "{references:true,graphics:true,connectors:true,programs:true,applyToAll:true}";

let filter = "site";

let today = new Date();

let dd = today.getDate();

let mm = today.getMonth()+1;

let yyyy = today.getFullYear();


if (dd < 10) {
   dd = "0" + dd
};

if (mm < 10) {
   mm = "0" + mm
};

let currentDate = yyyy + "-" + mm + "-" + dd;
console.log(currentDate);

let parameter = "read(site and dis==" + JSON.stringify(connDis) + ")->id," + backupProps + "," + JSON.stringify(filter);



let po = new axonPO({ "testQuery": "finBackupRecord(" + parameter + ")", "confirmQuery": "finBackup" });
po.setup(function (instance) { }
)

describe('The finBackupRecord_WRITESIM() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
  await  po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "finBackupRecord(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" &&
        testData.type !== "error",
        "finBackupRecord(" + parameter + ") returned an object which doesn't contain an error.");

     await ok(testData[0].id !== undefined &&
        testData[0].file === "application/zip" &&
        testData[0].finBackup === "✓" &&
        testData[0].dis === "Backup_" + connDis &&
        testData[0].mod.includes(currentDate) === true,
  

        "finBackupRecord(" + parameter + ") query results contains valid data => id: " +
        testData[0].id + " | file: " +
        testData[0].file + " | finBackup: " +
        testData[0].finBackup + " | dis: " +
        testData[0].dis + " | mod: " +
        testData[0].mod);

     await ok(confirmData[0].id !== undefined &&
        confirmData[0].file === "application/zip" &&
        confirmData[0].finBackup === "✓" &&
        confirmData[0].dis === "Backup_" + connDis &&
        testData[0].mod.includes(currentDate) === true,

        "finBackup query results contains valid data => id: " +
        confirmData[0].id + " | file: " +
        confirmData[0].file + " | finBackup: " +
        confirmData[0].finBackup + " | dis: " +
        confirmData[0].dis + " | mod: " +
        confirmData[0].mod + "" + "Backup of " +
        connDis + " site has been successfully created.");
    }
    );
  });

});