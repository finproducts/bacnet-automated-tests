import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK


//negative testing on "WriteSim" connector

//bacnet conn name
let connDis = "WriteSim";

//bacnet device targeted: "Write"
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//bacnet object
let bacnetObject = "AV1";

//bacnet object property
let propValue = 100;

//argument passed to the axon function
let parameter = bacnetConnId + ", [{propValue:" + propValue + ", propName:" + JSON.stringify(bacnetObject) + ", write: true}]";



let po = new axonPO({ "testQuery": "finReadDevicePropsCommit(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finReadDevicePropsCommit_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finReadDevicePropsCommit(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finReadDevicePropsCommit(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body.includes("The following properties were not changed:") === true &&
        testData[0].body.includes('The following properties cannot be written to:') === true &&
        testData[0].body.includes("- " + bacnetObject) === true &&
        testData[0].body === "[{\"name\":\"label\",\"text\":\"The following properties have been changed:\\nnone\\n\\nThe following properties were not changed:\\nnone\\n\\nThe following properties cannot be written to:\\n - AV1\",\"editorType\":\"label\"},{\"name\":\"okButton\",\"label\":\"Ok\",\"controlBar\":true,\"editorType\":\"button\"}]" &&
        testData[0].cancelButton === "$okButton" &&
        testData[0].dis === "Complete" &&
        testData[0].finForm === "✓" &&
        testData[0].name !== undefined,

        "finReadDevicePropsCommit(" + parameter + ") query contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | name: " +
        testData[0].name);
    }
    );
  });

});