import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK


let bacnetConnDis = "WriteSim";

let scheduleObject = "SCH3";

let writeSchedule = "scheduleHis(read(point and bacnetCur==" + JSON.stringify(scheduleObject) + "and bacnetConnRef->dis==" + JSON.stringify(bacnetConnDis) + ")->id, thisWeek() +1day, {timeline})";

let parameter = "read(bacnetConn and dis==" + JSON.stringify(bacnetConnDis) + ")->id," + JSON.stringify(scheduleObject) + "," + writeSchedule;

let validation = "read(bacnetConn and dis==" + JSON.stringify(bacnetConnDis) + "), read(bacnetCur==" + JSON.stringify(scheduleObject) + ")->id";



let po = new axonPO({
  "testQuery": "bacnetScheduleWrite(" + parameter + ")",
  "confirmQuery": "bacnetScheduleRead(" + validation + ")"
});
po.setup(function (instance) { }
)

describe('The bacnetScheduleWrite_SCH3_num_WRITESIM() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null, "bacnetScheduleWrite(" + parameter + ") query result is not undefined or null.");

      await ok(typeof testData === "object" &&
        testData.type !== "error", "bacnetScheduleWrite(" + parameter + ") returned an object which doesn't contain an error.");

     await ok(testData[0].val === true,
        "bacnetScheduleWrite(" + parameter + ") query returns valid data: " + testData[0].val);

      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i];

      await  ok(row.ts.includes("2019") === true &&
          row.val !== undefined &&
          confirmData.length === 11 &&
          confirmData[0].val === 100 &&
          confirmData[1].val === 100 &&
          confirmData[2].val === 100 &&
          confirmData[3].val === 100 &&
          confirmData[4].val === 100 &&
          confirmData[5].val === 100 &&
          confirmData[6].val === 100 &&
          confirmData[7].val === 100 &&
          confirmData[8].val === 100 &&
          confirmData[9].val === 100 &&
          confirmData[10].val === 100,
          "bacnetScheduleRead(" + validation + ") query contains valid data on row " + i + " => ts: " + row.ts + " | val: " + row.val);
      }

    }
    );
  });

});