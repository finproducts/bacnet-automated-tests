import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

/*
  bacnet connector used is "WriteSim" & AV1 point (bacnetCur is the name of the point on the device & is unchangeable) 
  the point must be present in DB Builder
  the point should have the value "null" on the level indicated by the "bacnetWriteLevel" tag (default is 16)
 */


//bacnet connector dis
let connDis = "WriteSim";

//bacnetCur of the point used
let pointBacnetCur = "AV1";

//value for object property: PRESENT_VALUE
let nullValue = null;

//argument passed to the axon function
let parameter = "read(point and bacnetCur==" + JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id," + nullValue + ", 16, \"su\"";

//validation
let validation = "read(point and bacnetCur==" + JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id";



let po = new axonPO({ "testQuery": "pointWrite(" + parameter + ")", "confirmQuery": "finBacnetPriorityArray(" + validation + ")" });
po.setup(function (instance) { }
)

describe('The pointWrite_AV1_NULL_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing pointWrite(parameter)
      ok(testData !== undefined && testData !== null,
        "pointWrite(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "pointWrite(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].val === nullValue, "pointWrite(" + parameter + ") query contains valid data => val: " + testData[0].val);


      //default bacnetWriteLevel is 16
      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]

        ok(row.level !== undefined &&
          row.levelDis !== undefined &&
          row.val !== undefined &&
          confirmData[15].level === 16 &&
          confirmData[15].levelDis === "Level 16" &&
          confirmData[15].val === JSON.stringify(nullValue),

          "finBacnetPriorityArray(" + validation + ") query results contains valid data on row " + i + " => level: " +
          row.level + " | levelDis: " +
          row.levelDis + " | val: " +
          row.val);
      }

    }
    );
  });

});