import { by, browser, element, ExpectedConditions as EC, $, $$ } from "protractor";
import { ok, equal, notEqual } from "../../../../../../helpers/nwapis";
import { waitService } from "../../../../../../helpers/wait-service";
import axonPO from "../../../../../../page_objects/finstack/axon.page";

//OK

/*
 * Validation for points BI1 & MSI1 from WriteSim!
 */
let pointNav1 = "BI_1";
let pointNav2 = "MI_1";

//connector targeted: "WriteSim"
let connDis = "WriteSim";

//function validation
let validation1 =
  "read(point and bacnetConnRef->dis==" + JSON.stringify(connDis) + " and navName==" + JSON.stringify(pointNav1) + ")->id,now()-10year..now()";

let validation2 =
  "read(point and bacnetConnRef->dis==" + JSON.stringify(connDis) + " and navName==" + JSON.stringify(pointNav2) + ")->id,now()-10year..now()";

let po = new axonPO({ "testQuery": "hisRead(" + validation1 + ")", "confirmQuery": "hisRead(" + validation2 + ")" });
po.setup(function (instance) { });

describe("The finSyncPoints_WRITESIM_part2() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(
          testData[0].ts.includes("2013-01-01") === true &&
          row.ts !== undefined &&
          typeof row.v0 === "boolean" &&
          testData.length === 100,
          "hisRead(" + validation1 + ") query contains valid data on row " + i + " => ts: " + 
          row.ts + " | v0: " + 
          row.v0
        );
      }

      let j;

      for (j = 0; j < confirmData.length; j++) {
        let row = confirmData[j];

        ok(
          confirmData[0].ts.includes("2013-01-01") === true &&
          row.ts !== undefined &&
          typeof row.v0 === "string" &&
          confirmData[1].v0 === "6" &&
          confirmData[2].v0 === "9" &&
          confirmData[3].v0 === "12" &&
          confirmData[4].v0 === "15" &&
          confirmData[5].v0 === "18" &&
          confirmData[confirmData.length - 1].v0 === "450" &&
          confirmData.length === 150,

          "hisRead(" + validation2 + ") query contains valid data on row " + j + " => ts: " +
          row.ts + " | v0: " +
          row.v0
        );
      }
    });
  });
});
