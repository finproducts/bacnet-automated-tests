import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';





let bacnetConn = 'read(bacnetConn and dis=="WriteSim")';

let bacnetObject = "MSV1";

let filter = "read(point and bacnetCur==" + JSON.stringify(bacnetObject) + ")";

let objectProperty = "present_value";

let paramArray = [1, 2, 3];

let randomIndex = Math.floor(Math.random() * paramArray.length);

let newValue = paramArray[randomIndex];

let parameter = bacnetConn + "," + filter + "," + JSON.stringify(objectProperty) + "," + JSON.stringify(newValue);

let validation = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + JSON.stringify(objectProperty);



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "bacnetReadObjectProperty(" + validation + ")" });
po.setup(function (instance) { }
)

describe('The bacnetWriteObjectProperty_MSV1() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetWriteObjectProperty(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetWriteObjectProperty(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " +
        testData[0].status + ". New value: " + newValue);

      if (newValue === 1) {
        ok(confirmData[0].bacnetValueType !== undefined &&
          confirmData[0].objectType === "MULTI_STATE_VALUE" &&
          confirmData[0].propertyName === objectProperty.toUpperCase() &&
          confirmData[0].value === 1,

          "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
          confirmData[0].bacnetValueType + " | objectType: " +
          confirmData[0].objectType + " | propertyName: " +
          confirmData[0].propertyName + " | value: " +
          confirmData[0].value);
      }

      else
        if (newValue === 2) {
          ok(confirmData[0].bacnetValueType !== undefined &&
            confirmData[0].objectType === "MULTI_STATE_VALUE" &&
            confirmData[0].propertyName === objectProperty.toUpperCase() &&
            confirmData[0].value === 2,

            "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
            confirmData[0].bacnetValueType + " | objectType: " +
            confirmData[0].objectType + " | propertyName: " +
            confirmData[0].propertyName + " | value: " +
            confirmData[0].value);
        }

        else
          if (newValue === 3) {
            ok(confirmData[0].bacnetValueType !== undefined &&
              confirmData[0].objectType === "MULTI_STATE_VALUE" &&
              confirmData[0].propertyName === objectProperty.toUpperCase() &&
              confirmData[0].value === 3,

              "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
              confirmData[0].bacnetValueType + " | objectType: " +
              confirmData[0].objectType + " | propertyName: " +
              confirmData[0].propertyName + " | value: " +
              confirmData[0].value);
          }




    }
    );
  });

});