import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted: "WriteSim"
let connDis = "WriteSim";

//bacnet conn id
let pointId = "readAll(point and bacnetCur and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")[0]->id";

//argument passed to the axon function
let parameter = pointId;



let po = new axonPO({ "testQuery": "finFormBacnetAddPoints(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finFormBacnetAddPoints_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finFormBacnetAddPoints(parameter)
      ok(testData !== undefined && testData !== null,
        "finFormBacnetAddPoints(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finFormBacnetAddPoints(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined && 
        testData[0].cancelButton === "$cancelButton" && 
        testData[0].commitAction.includes("finBacnetAddPointsApply") === true && 
        testData[0].commitButton === "$applyButton" && 
        testData[0].dis === "Add Bacnet Points" && 
        testData[0].finForm === "✓" && 
        testData[0].helpDoc !== undefined && 
        testData[0].name === "finBacnetAddPointsForm" && 
        testData[0].refreshTrees === "✓", 
        
        "finFormBacnetAddPoints(" + parameter + ") query result contains valid data => body: " + 
        testData[0].body + " | cancelButton: " + 
        testData[0].cancelButton + " | commitAction: " + 
        testData[0].commitAction + " | commitButton: " + 
        testData[0].commitButton + " | dis: " + 
        testData[0].dis + " | finForm: " + 
        testData[0].finForm + " | helpDoc: " + 
        testData[0].helpDoc + " | name: " + 
        testData[0].name + " | refreshTrees: " + 
        testData[0].refreshTrees);
    }
    );
  });

});