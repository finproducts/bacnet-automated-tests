import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

/*
 bacnet connector targeted: "WriteSim"
 */

//bacnet point targeted (must be writable) -> AV1
let bacnetCurName = "AV1";
let connDis = "WriteSim";
let bacnetPointId = "read(bacnetCur==" + JSON.stringify(bacnetCurName) + " and siteRef->dis==" + JSON.stringify(connDis) + ")->id";

//value
let value = "1";

//filter (level & value)
let filter = '{level: 16, levelDis: "Level 16", val:' + JSON.stringify(value) + '}';

//argument passed to the axon function
let parameter = filter + "," + bacnetPointId;



let po = new axonPO({ "testQuery": "finBacnetFormPriorityArrayEdit(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finBacnetFormPriorityArrayEdit() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finBacnetFormPriorityArrayEdit(parameter)
      ok(testData !== undefined && testData !== null,
        "finBacnetFormPriorityArrayEdit(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finBacnetFormPriorityArrayEdit(" + parameter + ") returned an object which doesn't contain an error.");

      let row = testData[0]

      ok(row.body.includes('\"name\":\"levelVal\",\"text\":\"' + value + '\"') === true &&
        row.body.includes('\"prompt\":\"Level 16 value\"') === true &&
        row.cancelButton === "$cancelButton" &&
        row.commitAction.includes(bacnetCurName) === true &&
        row.commitButton === "$applyButton" &&
        row.dis === "Change Bacnet Priority Array" &&
        row.finForm === "✓" &&
        row.name !== undefined,

        "finBacnetFormPriorityArrayEdit(" + parameter + ") query results contains valid data => body: " +
        row.body + " | cancelButton: " +
        row.cancelButton + " | commitAction: " +
        row.commitAction + " | commitButton: " +
        row.commitButton + " | dis: " +
        row.dis + " | finForm: " +
        row.finForm + " | name: " +
        row.name);
    }
    );
  });

});