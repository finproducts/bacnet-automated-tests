import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted: "WriteSim"
let connDis = "WriteSim";

//bacnet connector id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//category
let category = "all";

//argument passed to the axon function
let parameter = bacnetConnId + "," + JSON.stringify(category);



let po = new axonPO({ "testQuery": "backgroundQueryBacnetPoints(" + parameter + ")", "confirmQuery": "readJobQueryBacnetPoints(" + parameter + ').keepCols(["id","bacnetCur","bacnetPoint","hash","mod","objectName","selected"])' });
po.setup(function (instance) { }
)

describe('The readJobQueryBacnetPoints_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(confirmData !== undefined && confirmData !== null, 
        "readJobQueryBacnetPoints("  +  parameter  +  ") query result is not undefined or null.");

      ok(typeof confirmData === "object" && confirmData.type !== "error", 
      "readJobQueryBacnetPoints("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]
        ok(row.id !== undefined && 
          row.bacnetCur !== undefined &&
           row.bacnetPoint === "✓" && 
           row.hash !== undefined && 
           row.mod !== undefined && 
           row.objectName !== undefined && 
           row.selected !== undefined, 
           
           "readJobQueryBacnetPoints(" + parameter + ") query result contains valid data on row " + i + " => id: " + 
           row.id + " | bacnetCur: " + 
           row.bacnetCur + " | bacnetPoint: " + 
           row.bacnetPoint + " | hash: " + 
           row.hash + " | mod: " + 
           row.mod + " | objectName: " + 
           row.objectName + " | selected: " + 
           row.selected);
         }

    }
    );
  });

});