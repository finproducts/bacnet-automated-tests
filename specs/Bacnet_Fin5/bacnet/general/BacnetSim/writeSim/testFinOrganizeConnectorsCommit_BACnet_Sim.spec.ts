import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//OK

//bacnet connector targeted: 192.168.1.99
let connDis = "WriteSim";  // or "ReadSim"

//type
let ip = "ip";
let dnet = "dnet";

//dnet
let dnetVal = 2;

//folder name
let folderName = "BACnet conn";

//connector ip
let ipVal = "10.10.200.241:47806";

//argument passed to the axon function
let parameter = "[{type: " + JSON.stringify(ip) + ", elemVal: " + JSON.stringify(folderName) + ", val: " + JSON.stringify(ipVal) + "}], [{type: " + JSON.stringify(dnet) + ", elemVal: \"\", val: " + dnetVal + "}]";



let po = new axonPO({ "testQuery": "finOrganizeConnectorsCommit(" + parameter + ")", "confirmQuery": "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")" });
po.setup(function (instance) { }
)

describe('The finOrganizeConnectorsCommit_BACnet_Sim() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "finOrganizeConnectorsCommit(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "finOrganizeConnectorsCommit(" + parameter + ") returned an object which doesn't contain an error.");

        console.log(parameter)

     await ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "finOrganizeConnectorsCommit(" + parameter + ") query returns valid data: " + JSON.stringify(testData));

     await ok(confirmData[0].folderPath === "/" +
        folderName + " " + ip + ": " +
        ipVal + "/ -- Network " +
        dnetVal, "read(bacnetConn and dis==" + JSON.stringify(connDis) + ") query returns valid data => folderPath: " +
        confirmData[0].folderPath + "" + "Folder for dnet " +
        dnetVal + " was successfully created.");
    }
    );
  });

});