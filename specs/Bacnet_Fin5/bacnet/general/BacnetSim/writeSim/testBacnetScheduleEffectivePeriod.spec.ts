import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK


let bacnetConn = 'read(bacnetConn and dis=="WriteSim")';

let objArray = ["SCH1"];

let objIndex = Math.floor(Math.random() * objArray.length);

let bacnetObject = objArray[objIndex];

let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);



let po = new axonPO({ "testQuery": "bacnetScheduleEffectivePeriod (" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)


describe('The bacnetScheduleEffectivePeriod() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined &&
        testData !== null, "bacnetScheduleEffectivePeriod(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" &&
        testData.type !== "error", "'bacnetScheduleEffectivePeriod(" + parameter + ") returned an object which doesn't contain an error.");

      if (bacnetObject === "SCH1") {
        ok(testData[0].start.includes("2000-10-10") === true &&
          testData[0].end.includes("2001-10-10") === true, "bacnetScheduleEffectivePeriod(" + parameter + ") query contains valid data => start:" +
          testData[0].start + "|end:" + testData[0].end);
      }

      else
        if (bacnetObject === "SCH2") {
          ok(testData[0].start.includes("2001-11-11") === true &&
            testData[0].end.includes("2002-11-11") === true, 
            
            "bacnetScheduleEffectivePeriod(" + parameter + ") query contains valid data => start:" + 
            testData[0].start + "|end:" + 
            testData[0].end);
        }

        else
          if (bacnetObject === "SCH3") {
            ok(testData[0].start.includes("2002-12-12") === true &&
              testData[0].end.includes("2003-12-12") === true, 
              
              "bacnetScheduleEffectivePeriod(" + parameter + ") query contains valid data => start:" + 
              testData[0].start + " |end:" + 
              testData[0].end);
          }

    }
    );
  });

});