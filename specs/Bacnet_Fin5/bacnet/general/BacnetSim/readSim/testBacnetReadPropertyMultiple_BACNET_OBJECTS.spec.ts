import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["OBJECT_NAME", "OBJECT_TYPE", "OBJECT_IDENTIFIER"];

//"ReadSim" bacnet device
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//bacnet object
let objArray = ["ACC1", "AI1", "AO1", "AVG1", "BI1", "BO1", "BV1", "CMD1", "LC1", "LP1", "MSI1", "MSO1", "MSV1", "PC1", "SCH1",
                "ACC2", "AI2", "AO2", "AVG2", "BI2", "BO2", "BV2", "CMD2", "LC2", "LP2", "MSI2", "MSO2", "MSV2", "PC2", "SCH2",
                "ACC3", "AI3", "AO3", "AVG3", "BI3", "BO3", "BV3", "CMD3", "LC3", "LP3", "MSI3", "MSO3", "MSV3", "PC3", "SCH3"];

let objIndex = Math.floor(Math.random() * objArray.length);
let bacnetObject = objArray[objIndex];

//bacnet object properties
let objProps = 'object_name, object_type, object_identifier';

//argument passed to the axon function (also contains a false object & false properties for negative testing)
let parameter = bacnetConn +", {"+ JSON.stringify(bacnetObject) 
+": {"+ objProps +", false_property1}},{\"FALSE_OBJECT\": {"+ objProps +", false_property2}}";

let po = new axonPO({"testQuery": "bacnetReadPropertyMultiple("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetReadPropertyMultiple_BACNET_OBJECTS() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null, "bacnetReadPropertyMultiple("+ parameter +") query result is not undefined or null.");
      
      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetReadPropertyMultiple("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.bacnetValueType !== undefined &&
        row.objectId === bacnetObject &&
        row.propertyName !== undefined &&
        row.value !== undefined &&
        testData.length === 3,
        "bacnetReadPropertyMultiple("+ parameter +") query contains valid data on row "+
        i +" => bacnetValueType: "+ row.bacnetValueType +" | objectId: "+ row.objectId +" | propertyName: "+
        row.propertyName +" | value: "+ row.value);

        if( row.propertyName === "OBJECT_IDENTIFIER" 
        || row.propertyName === "OBJECT_TYPE" 
        || row.propertyName === "OBJECT_NAME") {
          validArray.push(row.propertyName);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      "Query result doesn't contain the expected properties: "+ JSON.stringify(validArray));
    });
  });  
});