import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



let connDis = 'ReadSim';
let bacnetConn = "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")";

let po = new axonPO({"testQuery": "bacnetGetUnhandledEvents("+ bacnetConn +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The tbacnetGetUnhandledEvents.js() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined , "bacnetGetUnhandledEvents("+ bacnetConn +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "bacnetGetUnhandledEvents("+
      bacnetConn +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[]",
      "bacnetGetUnhandledEvents("+ bacnetConn +") query contains valid data: "+ JSON.stringify(testData));
    });
  });  
});