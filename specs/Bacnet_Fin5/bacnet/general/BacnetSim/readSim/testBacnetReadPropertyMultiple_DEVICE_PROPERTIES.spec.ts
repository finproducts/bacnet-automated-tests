import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["MAX_APDU_LENGTH_ACCEPTED",
                     "MODEL_NAME", 
                     "OBJECT_IDENTIFIER",
                     "OBJECT_NAME",
                     "PROTOCOL_REVISION",
                     "PROTOCOL_SERVICES_SUPPORTED",
                     "PROTOCOL_VERSION",
                     "SEGMENTATION_SUPPORTED",
                     "SYSTEM_STATUS",
                     "VENDOR_NAME"];

//bacnet device targeted: "ReadSim"
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//device object (DEV + device instance number of "ReadSim")
let deviceObject = 'DEV2';

//bacnet device properties
let deviceProps = "{max_apdu_length_accepted, "+
                  "model_name, "+
                  "object_identifier, "+
                  "object_name, "+
                  "protocol_revision, "+
                  "protocol_services_supported, "+
                  "protocol_version, "+
                  "segmentation_supported, "+
                  "system_status, "+
                  "vendor_name}";

//argument passed to the axon function
let parameter = bacnetConn + ", {"+ JSON.stringify(deviceObject) + ":" + deviceProps + "}";

let po = new axonPO({"testQuery": "bacnetReadPropertyMultiple("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetReadPropertyMultiple_DEVICE_PROPERTIES() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== undefined, "bacnetReadPropertyMultiple ("+ parameter +") query result is not undefined or null.");
      
      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetReadPropertyMultiple ("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.bacnetValueType !== undefined &&
        row.objectId === deviceObject &&
        row.propertyName !== undefined &&
        row.value !== undefined &&
        testData.length === 10, 
        "bacnetReadPropertyMultiple ("+ parameter +") query contains valid data on row "+
        i +" => bacnetValueType: "+ row.bacnetValueType +" | objectId: "+ row.objectId +" | propertyName: "+
        row.propertyName +" | value: "+ row.value);

        if( row.propertyName === "MAX_APDU_LENGTH_ACCEPTED"
        || row.propertyName === "MODEL_NAME" 
        || row.propertyName === "OBJECT_IDENTIFIER"
        || row.propertyName === "OBJECT_NAME" 
        || row.propertyName === "PROTOCOL_REVISION" 
        || row.propertyName === "PROTOCOL_SERVICES_SUPPORTED" 
        || row.propertyName === "PROTOCOL_VERSION" 
        || row.propertyName === "SEGMENTATION_SUPPORTED" 
        || row.propertyName === "SYSTEM_STATUS" 
        || row.propertyName === "VENDOR_NAME") { 
          validArray.push(row.propertyName);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      "Query result doesn't contain the expected properties: "+ JSON.stringify(validArray));
    });
  });  
});