import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

/*
drag into DB Builder & path: ReadSim(site) ==> Floor 1 ==> Equip 1 ==> BV_1
curStatus should have the value "ok" from "stale" after running bacnetSyncCur() 
point tags affected: curStatus, curVal, curErr
*/

//BV_1 from "ReadSim"
let parameter = 'read(bacnetCur and kind=="Bool" and bacnetCur=="BV1" and bacnetConnRef->dis=="ReadSim")';

let po = new axonPO({"testQuery": "bacnetSyncCur("+ parameter +")", "confirmQuery": parameter});
po.setup(function(instance) {});

describe('The testBacnetSyncCur_bool_BV1() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null, "bacnetSyncCur("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetSyncCur("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
      "bacnetSyncCur("+ parameter +") query returns invalid data: "+ JSON.stringify(testData));

      let row= confirmData[0];

      console.log("a"+ row.floorRefDis + "a");

      ok(row.dis === "ReadSim Equip 1 BV_1" &&
      row.id !== undefined &&
      row.bacnetCur === "BV1" &&
      row.cur === "✓" &&
      //row.bacnetConnRef === "ReadSim" &&
      row.kind === "Bool" &&
      row.siteRefDis === "ReadSim" &&
      //row.is === "BINARY_VALUE" &&
      row.curStatus === "ok" &&
      row.curVal === false &&
      row.point === "✓" &&
      row.curErr === undefined &&
      row.writable === "✓" &&
      row.floorRefDis === "Floor 1" &&
      row.navName === "BV_1" &&
      row.bacnetWrite === "BV1" &&
      row.equipRefDis.includes('ReadSim') === true &&
      row.cmd === "✓",
      
      parameter + " query contains valid data => dis: "+
      row.dis +" | id: "+ row.id +" | bacnetCur: "+ row.bacnetCur +" | cur: "+
      row.cur +" | bacnetConnRefDis: "+ row.bacnetConnRefDis +" | kind: "+
      row.kind +" | siteRefDis: "+ row.siteRefDis +" | is: "+ row.is +" | curStatus: "+
      row.curStatus +" | curVal: "+ row.curVal +" | point: "+ row.point +" | writable: "+
      row.writable +" | floorRefDis: "+ row.floorRefDis +" | navName: "+ row.navName +" | bacnetWrite: "+
      row.bacnetWrite +" | equipRefDis: "+ row.equipRefDis +" | cmd: "+ row.cmd);
    });
  });  
});