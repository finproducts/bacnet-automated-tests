import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let connDis= 'ReadSim';
let bacnetConn= 'read(bacnetConn and dis=="+ JSON.stringify(connDis) +")';

let po = new axonPO({"testQuery":"bacnetLearn("+ bacnetConn +")", "confirmQuery":""});
po.setup(function(instance) {})

  describe('The bacnetLearn() query', () => {
    beforeEach(async () => {
      await po.open();
    });

    it('Returns correct data when executed', () => {
      po.assert(function(testData,confirmData) {

        ok(testData !== undefined && testData !== undefined, 'bacnetLearn("+ bacnetConn +") query result is not undefined or null.');
        ok(typeof testData === "object" && testData.type !== "error", 'bacnetLearn("+ bacnetConn +") returned an object which doesn\'t contain an error.');

        for(let i = 0; i < testData.length; i++) {
          let row = testData[i]
          ok(testData[0].dis === "ACCUMULATOR" &&
            testData[1].dis === "ANALOG INPUT" &&
            testData[2].dis === "ANALOG OUTPUT" &&
            testData[3].dis === "ANALOG VALUE" &&
            testData[4].dis === "AVERAGING" &&
            testData[5].dis === "BINARY INPUT" &&
            testData[6].dis === "BINARY OUTPUT" &&
            testData[7].dis === "BINARY VALUE" &&
            testData[8].dis === "COMMAND" &&
            testData[9].dis  === "LOAD CONTROL" &&
            testData[10].dis === "LOOP" &&
            testData[11].dis === "MULTI STATE INPUT" &&
            testData[12].dis === "MULTI STATE OUTPUT" &&
            testData[13].dis === "MULTI STATE VALUE" &&
            testData[14].dis === "PULSE CONVERTER" &&
            testData[15].dis === "SCHEDULE" &&
            row.learn === row.dis.split(' ').join('_'),"bacnetLearn("+ bacnetConn +") query contains valid data on row "+ i +" => dis: "+ row.dis +" | learn: "+ row.learn);
        }
      }
    );
  });  
});