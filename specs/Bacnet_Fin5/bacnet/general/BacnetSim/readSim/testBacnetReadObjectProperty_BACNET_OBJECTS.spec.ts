import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//"ReadSim" bacnet device
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//bacnet object
let objArray = ["ACC1", "AI1", "AO1", "AVG1", "BI1", "BO1", "BV1", "CMD1", "LC1", "LP1", "MSI1", "MSO1", "MSV1", "PC1", "SCH1",
                "ACC2", "AI2", "AO2", "AVG2", "BI2", "BO2", "BV2", "CMD2", "LC2", "LP2", "MSI2", "MSO2", "MSV2", "PC2", "SCH2",
                "ACC3", "AI3", "AO3", "AVG3", "BI3", "BO3", "BV3", "CMD3", "LC3", "LP3", "MSI3", "MSO3", "MSV3", "PC3", "SCH3"];

let objIndex = Math.floor(Math.random() * objArray.length);
let bacnetObject = objArray[objIndex];

//bacnet object property
let propArray = ["OBJECT_NAME", "OBJECT_TYPE", "OBJECT_IDENTIFIER"];
let propIndex = Math.floor(Math.random() * propArray.length);
let objectProperty = propArray[propIndex];

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + JSON.stringify(objectProperty);

let po = new axonPO({"testQuery": "bacnetReadObjectProperty("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetReadObjectProperty_BACNET_OBJECTS() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null,
      "bacnetReadObjectProperty("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error",
      "bacnetReadObjectProperty("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].bacnetValueType !== undefined &&
      testData[0].objectType !== undefined &&
      testData[0].propertyName === objectProperty &&
      testData[0].value !== undefined,
      "bacnetReadObjectProperty("+ parameter +") query doesn't contain valid data => bacnetValueType: "+
      testData[0].bacnetValueType +"|objectType: "+ testData[0].objectType +" | propertyName: "+
      testData[0].propertyName +" | value: "+ testData[0].value);
    });
  });  
});