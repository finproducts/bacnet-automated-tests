
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';
import { browser } from 'protractor';

/*
drag into DB Builder & path: ReadSim(site) ==> Floor 1 ==> Equip 1 ==> MV_1
curStatus should have the value "ok" from "stale" after running bacnetSyncCur() 
point tags affected: curStatus, curVal, curErr
*/

//MV_1 from "ReadSim"
let parameter = 'read(bacnetCur and kind=="Str" and bacnetCur=="MSV1" and bacnetConnRef->dis=="ReadSim")';

let po = new axonPO({"testQuery": "bacnetSyncCur("+ parameter +")", "confirmQuery": parameter});
po.setup(function(instance) {});

describe('The testBacnetSyncCur_enum_MSV1() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== undefined, "bacnetSyncCur("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetSyncCur("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
      "bacnetSyncCur("+ parameter +") query returns valid data: "+ JSON.stringify(testData));

      let row= confirmData[0];

      ok(row.dis === "ReadSim Equip 1 MV_1" &&
      row.id !== undefined &&
      row.bacnetCur === "MSV1" &&
      row.cur === "✓" &&
      row.bacnetConnRefDis === "ReadSim" &&
      row.kind === "Str" &&
      row.siteRefDis === "ReadSim" &&
      row.enum === "State_1,State_2,State_3" &&
      //row.is === "MULTI_STATE_VALUE" &&
      row.curStatus === "ok" &&
      row.curVal === "State_1" &&
      row.point === "✓" &&
      row.curErr === undefined &&
      row.writable === "✓" &&
      row.floorRefDis === "Floor 1" &&
      row.navName === "MV_1" &&
      row.bacnetWrite === "MSV1" &&
      row.equipRefDis.includes('ReadSim') === true  &&
      row.cmd === "✓", parameter + "query contains valid data => dis: "+ row.dis + " | id: "+
      row.id + " | bacnetCur: "+ row.bacnetCur +" | cur: "+ row.cur +" | bacnetConnRefDis: "+
      row.bacnetConnRefDis +" | kind: "+ row.kind +" | siteRefDis: "+ row.siteRefDis +" | is: "+
      row.is +" | curStatus: "+ row.curStatus +" | curVal: "+ row.curVal +" | point: "+ row.point +" | writable:  "+
      row.writable +" | floorRefDis: "+ row.floorRefDis +" | navName: "+ row.navName +" | bacnetWrite: "+
      row.bacnetWrite +" | equipRefDis: "+ row.equipRefDis +" | cmd: "+ row.cmd +" | enum: "+ row.enum);
    });
  });  
});