import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let expectedArray= ["AV1", "BV1"];
let connDis= 'ReadSim';
let bacnetConn= "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")";

let po = new axonPO({"testQuery":"bacnetGetAlarmSummary("+ bacnetConn +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The bacnetGetAlarmSummary() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns correct data when executed', async () => {
    po.assert(async function(testData,confirmData) {

      ok(testData !== undefined && testData !== null,
      "bacnetGetAlarmSummary("+ bacnetConn +") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
      "bacnetGetAlarmSummary(" + bacnetConn + ") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];

        ok(row.ackRequired === false &&
        row.alarmText === "From Alarm Summary!" &&
        row.bacnetConnRefDis === connDis &&
        row.eventObjectIdentifier !== undefined &&
        row.initiatingDeviceIdentifier === "DEV2" &&
        row.notifyType === "ALARM" &&
        row.toState !== undefined,
        "bacnetGetAlarmSummary("+ bacnetConn +") query contains valid data on row "+ i +" => ackRequired: "+ row.ackRequired +" | alarmText: "+ row.alarmText +" | bacnetConnRef: "+ row.bacnetConnRef +" | bacnetConnRefDis: "+ row.bacnetConnRefDis +" | eventObjectIdentifier: "+ row.eventObjectIdentifier +" | initiatingDeviceIdentifier: "+ row.initiatingDeviceIdentifier +" | notifyType: "+ row.notifyType +" | toState: "+ row.notifyType);

        if(row.eventObjectIdentifier === "AV1" && row.toState === "HIGH_LIMIT" ||
        row.eventObjectIdentifier === "BV1" && row.toState === "OFFNORMAL") {
          validArray.push(row.eventObjectIdentifier);
        }    
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), 
      "Query result contains the expected objects with alarms: "+ JSON.stringify(validArray));
    });
  });  
});