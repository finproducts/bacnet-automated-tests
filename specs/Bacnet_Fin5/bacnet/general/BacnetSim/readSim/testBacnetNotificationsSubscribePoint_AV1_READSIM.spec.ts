import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//point targeted
let bacnetCur= 'AV1';

//bacnet connector dis
let connDis= 'ReadSim';

//point expression
let pointExpression= "read(point and bacnetCur =="+ JSON.stringify(bacnetCur)+" and bacnetConnRef->dis=="+
JSON.stringify(connDis) +")";

//argument passed to the axon function
let parameter= "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")" + "," + pointExpression;

let po = new axonPO({"testQuery": "bacnetNotificationsSubscribePoint("+ parameter +")", "confirmQuery":""});
po.setup(function(instance) {})

describe('The bacnetNotificationsSubscribePoint_AV1() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined && testData !== null,
      "bacnetNotificationsSubscribePoint("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
      "bacnetNotificationsSubscribePoint("+ parameter +") returned an object which doesn't contain an error.");
      for(let i = 0; i < testData.length; i++) {
        let row= testData[i]
        ok(row.localDevId.includes('DEV') === true &&
        row.notificationId === "NC1" && row.pointId === bacnetCur &&
        row.result === true,
        "bacnetNotificationsSubscribePoint("+ parameter +") query contains valid data on row "+ i +" => localDevId: "+ row.localDevId +" | notificationId: "+ row.notificationId +" | pointId: "+ row.pointId +" | result: "+ row.result);
      }
    });
  });  
});