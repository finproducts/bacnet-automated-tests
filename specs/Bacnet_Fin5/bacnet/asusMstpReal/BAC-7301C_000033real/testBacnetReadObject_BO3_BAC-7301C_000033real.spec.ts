import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["STATUS_FLAGS", "BitList(0 0 0 0)",
                     "OBJECT_NAME", "BO_3", 
                     "OBJECT_TYPE", 4, 
                     "OBJECT_IDENTIFIER", "BO3"];

let bacnetConn= 'read(bacnetConn and dis=="BAC-7301C_000033real")';

let bacnetObject= 'BO3';

let parameter= bacnetConn  +  ","  +  JSON.stringify(bacnetObject);



let po = new axonPO({"\"testQuery\"":"\"bacnetReadObject(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetReadObject_BO3_BAC-7301C_000033real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadObject_BO3_BAC-7301C_000033real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetReadObject("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetReadObject("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetValueType  !==  undefined  &&  row.objectType  ===  "BINARY_OUTPUT"  &&  row.propertyName  !==  undefined,"bacnetReadObject("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetValueType: "  +  row.bacnetValueType  +  " | objectType: "  +  row.objectType  +  " | propertyName: "  +  row.propertyName  +  " | value: "  +  row.value);

if( row.propertyName  ===  "STATUS_FLAGS"  &&  row.value  ===  "BitList(0 0 0 0)"  ||  row.propertyName  ===  "OBJECT_NAME"  &&  row.value  ===  "BO_3"  ||  row.propertyName  ===  "OBJECT_TYPE"  &&  row.value  ===  4  ||  row.propertyName  ===  "OBJECT_IDENTIFIER"  &&  row.value  ===  "BO3") 
      {
validArray.push(row.propertyName,row.value);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});