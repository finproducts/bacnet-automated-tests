import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["SCH1", "SCH2", "SCH3", "SCH4", "SCH5", "SCH6", "SCH7", "SCH8"];

let connDis= 'BAC-7301C_000033real';

let bacnetConn= 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let folder= '`SCHEDULE`';

let parameter= bacnetConn  +  ","  +  folder;



let po = new axonPO({"\"testQuery\"":"\"bacnetLearn(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetReadFolder_SCHEDULE_BAC-7301C_000033real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadFolder_SCHEDULE_BAC-7301C_000033real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetLearn("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetLearn("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetCur  !==  undefined  &&  row.description  !==  undefined  &&  row.dis  !==  undefined  &&  row.is  ===  "SCHEDULE"  &&  row.kind  !==  undefined  &&  row.point  ===  "✓"  &&  testData.length  ===  8,"bacnetLearn("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetCur: "  +  row.bacnetCur  +  " | description: "  +  row.description  +  " | dis: "  +  row.dis  +  " | is: "  +  row.is  +  " | kind: "  +  row.kind  +  " | point: "  +  row.point);

if( row.bacnetCur  ===  "SCH1"  ||  row.bacnetCur  ===  "SCH2"  ||  row.bacnetCur  ===  "SCH3"  ||  row.bacnetCur  ===  "SCH4"  ||  row.bacnetCur  ===  "SCH5"  ||  row.bacnetCur  ===  "SCH6"  ||  row.bacnetCur  ===  "SCH7"  ||  row.bacnetCur  ===  "SCH8") 
      {
validArray.push(row.bacnetCur);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});