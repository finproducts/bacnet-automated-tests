import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetPoint= 'BV5';

let connDis= 'BAC-7301C_000033real';

let bacnetConn= 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let paramArray= [0,1];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let newValue= paramArray[randomIndex];

let parameter= bacnetConn  +  ","  +  JSON.stringify(bacnetPoint)  +  ", "present_value", "  +  newValue;



let po = new axonPO({"\"testQuery\"":"\"bacnetWriteObjectProperty(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"read(point and bacnetCur==\"  +  JSON.stringify(bacnetPoint)  +  \" and bacnetConnRef->dis==\"  +  JSON.stringify(connDis)  +  \")->curVal\""});
po.setup(function(instance)
{}
)

describe('testConnAppDetails_POLLING_BV5_BAC-7301C_000033real_part2.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing testConnAppDetails_POLLING_BV5_BAC-7301C_000033real_part2"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData[0].status  ===  false,"bacnetWriteObjectProperty("  +  parameter  +  ") query contains valid data => status "  +  testData[0].status  +  ". New value: "  +  newValue);

if( newValue  ===  0) 
      {
ok(confirmData[0].val  ===  false,'read(point and bacnetCur=="  +  JSON.stringify(bacnetPoint)  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")->curVal query contains valid data => val: "  +  confirmData[0].val  +  "
"  +  "Polling worked properly. New value is store in curVal.');
}

    else 
        if( newValue  ===  1) 
      {
ok(confirmData[0].val  ===  false,'read(point and bacnetCur=="  +  JSON.stringify(bacnetPoint)  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")->curVal query contains valid data => val: "  +  confirmData[0].val  +  "
"  +  "Polling worked properly. New value is store in curVal.');
}

    
      
}
);
  });  

});