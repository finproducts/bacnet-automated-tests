import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetConn= 'read(bacnetConn and dis=="BAC-7301C_000033real")';

let dev= 'DEV3080225';

let parameter= bacnetConn  +  ","  +  JSON.stringify(dev)  +  ", "OBJECT_LIST"";



let po = new axonPO({"\"testQuery\"":"\"bacnetReadObjectProperty(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetReadObjectProperty_OBJECT_LIST_BAC-7301C_000033real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadObjectProperty_OBJECT_LIST_BAC-7301C_000033real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetReadObjectProperty("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetReadObjectProperty("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(testData[0].error  ===  "j2inn.io.bacnet.query.result.BacnetFailureException: SEGMENTATION_NOT_SUPPORTED Object:"  +  dev  &&  testData[0].objectId  ===  dev  &&  testData[0].propertyName  ===  "OBJECT_LIST","bacnetReadObjectProperty("  +  parameter  +  ") query results contains correct data => error: "  +  testData[0].error  +  " | objectId: "  +  testData[0].objectId  +  " | propertyName: "  +  testData[0].propertyName);
}
);
  });  

});