import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetConn= 'read(bacnetConn and dis=="BAC-7301C_000033real")';

let analogObject= 'av3';

let booleanObject= 'bv2';

function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let analogObjectName= createRandomString();

let booleanObjectName= createRandomString();

function createRandomNumber() {

	var min = 1;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let analogObjectValue= createRandomNumber();

let arrayBoolVal= [0, 1];

let indexBool= Math.floor(Math.random()  *  arrayBoolVal.length);

let booleanObjectValue= arrayBoolVal[indexBool];

let parameter= bacnetConn  +  ", {"  +  analogObject  +  ":{present_value:["  +  analogObjectValue  +  "], object_name:["  +  JSON.stringify(analogObjectName)  +  "]},"  +  booleanObject  +  ":{present_value:["  +  booleanObjectValue  +  "], object_name:["  +  JSON.stringify(booleanObjectName)  +  "]}}";

let validation= bacnetConn  +  ", {"  +  analogObject  +  ":{present_value, object_name}, "  +  booleanObject  +  ":{present_value, object_name}}";



let po = new axonPO({"\"testQuery\"":"\"bacnetWritePropertyMultiple(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"bacnetReadPropertyMultiple(\"  +  validation  +  \")\""});
po.setup(function(instance)
{}
)

describe('testBacnetWritePropertyMultiple_BAC-7301C_000033real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetWritePropertyMultiple_BAC-7301C_000033real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetWritePropertyMultiple("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetWritePropertyMultiple("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let i;

for(i  =  0; i  <  testData.length; i++)
{
let rowTestData= testData[i]

ok(rowTestData.bacnetValueType  !==  undefined  &&  rowTestData.objectId  !==  undefined  &&  rowTestData.propertyName  !==  undefined  &&  rowTestData.value  !==  undefined,"bacnetWritePropertyMultiple("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => bacnetValueType: "  +  rowTestData.bacnetValueType  +  " | objectId: "  +  rowTestData.objectId  +  " | propertyName: "  +  rowTestData.propertyName  +  " | value: "  +  rowTestData.value);
}


let j;

for(j  =  0; j  <  confirmData.length; j++)
{
let rowConfirmData= confirmData[j]

ok(confirmData[0].bacnetValueType  ===  "CharString"  &&  confirmData[0].objectId  ===  analogObject.toUpperCase()  &&  confirmData[0].propertyName  ===  "OBJECT_NAME"  &&  confirmData[0].value  ===  analogObjectName  &&  confirmData[1].bacnetValueType  ===  "Real"  &&  confirmData[1].objectId  ===  analogObject.toUpperCase()  &&  confirmData[1].propertyName  ===  "PRESENT_VALUE"  &&  confirmData[1].value  ===  analogObjectValue  &&  confirmData[2].bacnetValueType  ===  "CharString"  &&  confirmData[2].objectId  ===  booleanObject.toUpperCase()  &&  confirmData[2].propertyName  ===  "OBJECT_NAME"  &&  confirmData[2].value  ===  booleanObjectName  &&  confirmData[3].bacnetValueType  ===  "Enumeration"  &&  confirmData[3].objectId  ===  booleanObject.toUpperCase()  &&  confirmData[3].propertyName  ===  "PRESENT_VALUE"  &&  confirmData[3].value  ===  booleanObjectValue,"Confirming... bacnetReadPropertyMultiple("  +  validation  +  ") query contains valid data on row "  +  j  +  " => bacnetValueType: "  +  rowConfirmData.bacnetValueType  +  " | objectId: "  +  rowConfirmData.objectId  +  " | propertyName: "  +  rowConfirmData.propertyName  +  " | value: "  +  rowConfirmData.value);
}

}
);
  });  

});