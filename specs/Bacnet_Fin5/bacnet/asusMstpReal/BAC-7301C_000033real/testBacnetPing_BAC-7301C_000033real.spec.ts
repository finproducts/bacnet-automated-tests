import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= 'BAC-7301C_000033real';

let bacnetConn= 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let validation= bacnetConn  +  ","  +  bacnetConn  +  ", "SYSTEM_STATUS"";



let po = new axonPO({"\"testQuery\"":"\"bacnetPing(\"  +  bacnetConn  +  \")\"","\"confirmQuery\"":"\"bacnetReadObjectProperty(\"  +  validation  +  \")\""});
po.setup(function(instance)
{}
)

describe('testBacnetPing_BAC-7301C_000033real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetPing_BAC-7301C_000033real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetPing("  +  bacnetConn  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetPing("  +  bacnetConn  +  ") returned an object which doesn\'t contain an error.');

ok(JSON.stringify(testData)  ===  "[{"val":null}]","bacnetPing("  +  bacnetConn  +  ") query results contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].value  ===  0,'bacnetReadObjectProperty("  +  validation  +  ") contains valid data => bacnetValueType: "  +  confirmData[0].bacnetValueType  +  " | objectType: "  +  confirmData[0].objectType  +  " | propertyName: "  +  confirmData[0].propertyName  +  " | value: "  +  confirmData[0].value  +  "
"  +  "CONFIRMED - Value is 0');
}
);
  });  

});