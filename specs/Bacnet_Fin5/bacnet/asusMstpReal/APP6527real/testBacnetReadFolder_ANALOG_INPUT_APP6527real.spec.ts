import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["AI4", "AI13", "AI15", "AI35", "AI50", "AI54", "AI125", "AI126"];

let connDis= 'APP6527real';

let bacnetConn= 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let folder= '`ANALOG_INPUT`';

let parameter= bacnetConn  +  ","  +  folder;



let po = new axonPO({"\"testQuery\"":"\"bacnetLearn(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetReadFolder_ANALOG_INPUT_APP6527real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadFolder_ANALOG_INPUT_APP6527real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetLearn("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetLearn("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetCur  !==  undefined  &&  row.is  ===  "ANALOG_INPUT"  &&  row.kind  ===  "Number"  &&  row.point  ===  "✓","bacnetLearn("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetCur: "  +  row.bacnetCur  +  " | dis: "  +  row.dis  +  " | is: "  +  row.is  +  " | kind: "  +  row.kind  +  " | point: "  +  row.point  +  " | bacnetWrite: "  +  row.bacnetWrite  +  " | bacnetWriteLevel: "  +  row.bacnetWriteLevel);

if( row.bacnetCur  ===  "AI4"  ||  row.bacnetCur  ===  "AI13"  ||  row.bacnetCur  ===  "AI15"  ||  row.bacnetCur  ===  "AI35"  ||  row.bacnetCur  ===  "AI50"  ||  row.bacnetCur  ===  "AI54"  ||  row.bacnetCur  ===  "AI125"  ||  row.bacnetCur  ===  "AI126") 
      {
validArray.push(row.bacnetCur);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});