import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= 'APP6527real';

let bacnetConn= 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let parameter= '"bacnet", read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")->id';



let po = new axonPO({"\"testQuery\"":"\"bacnetPing(\"  +  bacnetConn  +  \")\"","\"confirmQuery\"":"\"connAppDetails(\"  +  parameter  +  \")\""});
po.setup(function(instance)
{}
)

describe('testConnAppDetails_POLLING_APP6527real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing connAppDetails_POLLING_APP6527real"', () => {
    po.assert(function(testData,confirmData)
{
ok(confirmData  !==  undefined  &&  confirmData  !==  undefined,'connAppDetails("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",'connAppDetails("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(confirmData[0].val.includes('isCOVEnabled:      false')  ===  false  &&  confirmData[0].val.includes('supportsCOV:       false')  ===  false  &&  confirmData[0].val.includes("deviceName:        "  +  connDis.substring(0,7))  ===  false  &&  confirmData[0].val.includes('defWriteDelay:     20sec')  ===  false,'connAppDetails("  +  parameter  +  ") query contains valid data => val: "  +  confirmData[0].val  +  "
"  +  " "  +  "
"  +  connDis  +  " connector has Polling enabled.');
}
);
  });  

});