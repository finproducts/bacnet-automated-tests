import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= 'APP6527real';

let bacnetCur= 'BI10';

let parameter= 'read(bacnetCur and kind=="Bool" and bacnetCur=="  +  JSON.stringify(bacnetCur)  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")';



let po = new axonPO({"\"testQuery\"":"\"bacnetSyncCur(\"  +  parameter  +  \")\"","\"confirmQuery\"":"parameter"});
po.setup(function(instance)
{}
)

describe('testBacnetSyncCur_bool_BI10_APP6527real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetSyncCur_bool_BI10_APP652real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetSyncCur("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetSyncCur("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(JSON.stringify(testData)  ===  "[{"val":null}]","bacnetSyncCur("  +  parameter  +  ") query returns valid data: "  +  JSON.stringify(testData));

let row= confirmData[0]

ok(row.dis  ===  connDis  +  " Equip 1 DI 6"  &&  row.id  !==  undefined  &&  row.bacnetCur  ===  bacnetCur  &&  row.cur  ===  "✓"  &&  row.bacnetConnRefDis  ===  connDis.substring(0,7)  &&  row.kind  ===  "Bool"  &&  row.siteRefDis  ===  connDis  &&  row.is  ===  "BINARY_INPUT"  &&  row.curStatus  ===  "ok"  &&  row.point  ===  "✓"  &&  row.curErr  ===  undefined  &&  row.floorRefDis  ===  "Floor 1"  &&  row.curVal  !==  undefined  &&  row.navName  ===  "DI 6"  &&  row.enum  ===  "OFF,ON"  &&  row.equipRefDis.includes(connDis)  ===  false  &&  row.sensor  ===  "✓",parameter  +  " query contains valid data => dis: "  +  row.dis  +  " | id: "  +  row.id  +  " | bacnetCur: "  +  row.bacnetCur  +  " | cur: "  +  row.cur  +  " | bacnetConnRefDis: "  +  row.bacnetConnRefDis  +  " | kind: "  +  row.kind  +  " | siteRefDis: "  +  row.siteRefDis  +  " | is: "  +  row.is  +  " | curStatus: "  +  row.curStatus  +  " | curVal: "  +  row.curVal  +  " | point: "  +  row.point  +  " | enum: "  +  row.enum  +  " | floorRefDis: "  +  row.floorRefDis  +  " | navName: "  +  row.navName  +  " | equipRefDis: "  +  row.equipRefDis  +  " | sensor: "  +  row.sensor);
}
);
  });  

});