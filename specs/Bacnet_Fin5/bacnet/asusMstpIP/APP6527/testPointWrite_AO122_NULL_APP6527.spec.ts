import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= 'APP6527';

let pointBacnetCur= 'AO122';

let nullValue= undefined;

let parameter= 'read(point and bacnetCur=="  +  JSON.stringify(pointBacnetCur)  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")->id,"  +  nullValue  +  ", 16, "su"';

let validation= 'read(point and bacnetCur=="  +  JSON.stringify(pointBacnetCur)  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")->id';



let po = new axonPO({"testQuery":"pointWrite("  +  parameter  +  ")","confirmQuery":"finBacnetPriorityArray(" +  validation  +  ")"});
po.setup(function(instance)
{}
)

describe('testPointWrite_AO122_NULL_APP6527.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Testing pointWrite_AO122_NULL_APP6527", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,"pointWrite("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error","pointWrite("  +  parameter  +  ") returned an object which doesn\'t contain an error.");

ok(testData[0].val  ===  nullValue,"pointWrite("  +  parameter  +  ") query contains valid data => val: "  +  testData[0].val);

let i;

for(i  =  0; i  <  confirmData.length; i++)
{
let row= confirmData[i]

ok(row.level  !==  undefined  &&  row.levelDis  !==  undefined  &&  row.val  !==  undefined  &&  confirmData[15].level  ===  16  &&  confirmData[15].levelDis  ===  "Level 16"  &&  confirmData[15].val  ===  JSON.stringify(nullValue),"finBacnetPriorityArray("  +  validation  +  ") query results contains valid data on row "  +  i  +  " => level: "  +  row.level  +  " | levelDis: "  +  row.levelDis  +  " | val: "  +  row.val);
}

}
);
  });  

});