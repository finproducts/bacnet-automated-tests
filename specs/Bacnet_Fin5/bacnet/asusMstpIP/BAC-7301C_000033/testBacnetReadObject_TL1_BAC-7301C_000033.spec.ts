import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let bacnetConn = 'read(bacnetConn and dis=="BAC-7301C_000033")';

let bacnetObject = 'TL1';

let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);

let po = new axonPO({"testQuery": "bacnetReadObject("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadObject_TL1_BAC-7301C_000033.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadObject_TL1_BAC-7301C_000033", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetReadObject("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "bacnetReadObject("  +  parameter  +  ") returned an object which doesn\'t contai' 'n an error.");

        let validArray = []

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.propertyName !== undefined, "bacnetReadObject(" + parameter + ") query contains valid data on row " + i + " => bacnetValueType: " + row.bacnetValueType + " | objectType: " + row.objectType + " | propertyName: " + row.propertyName + " | value: " + row.value);

          if (row.propertyName === "OBJECT_NAME" && row.value === "TL_1" || row.propertyName === "OBJECT_TYPE" && row.value === 20 || row.propertyName === "OBJECT_IDENTIFIER" && row.value === "TL1" || row.propertyName === "LOG_DEVICE_OBJECT_PROPERTY" && row.value === "AI0" || row.propertyName === "RECORD_COUNT" && row.value === 0) {
            validArray.push(row.propertyName, row.value);
          }

        }

        ok(validArray.includes('OBJECT_NAME') === false && validArray.includes('TL_1') === false && validArray.includes('OBJECT_TYPE') === false && validArray.includes(20) === false && validArray.includes('OBJECT_IDENTIFIER') === false && validArray.includes('TL1') === false && validArray.includes('LOG_DEVICE_OBJECT_PROPERTY') === false && validArray.includes('AI0') === false && validArray.includes('RECORD_COUNT') === false && validArray.includes(0) === false, "Query result contains the expected elements: " + JSON.stringify(validArray));
      });
  });

});