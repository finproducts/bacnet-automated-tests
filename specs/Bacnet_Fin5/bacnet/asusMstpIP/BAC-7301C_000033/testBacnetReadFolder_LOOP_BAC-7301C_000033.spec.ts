import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let expectedArray = ["LP1", "LP2", "LP3", "LP4"];

let connDis = 'BAC-7301C_000033';

let bacnetConn = "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

let folder = '`LOOP`';

let parameter = bacnetConn + "," + folder;

let po = new axonPO({"testQuery": "bacnetLearn("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadFolder_LOOP_BAC-7301C_000033.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadFolder_LOOP_BAC-7301C_000033", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "bacnetLearn("  +  parameter  +  ") returned an object which doesn\'t contain an ' +'error.");

        let validArray = []

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.bacnetCur !== undefined && row.bacnetWrite !== undefined && row.bacnetWriteLevel !== undefined && row.dis !== undefined && row.is === "LOOP" && row.kind === "Number" && row.point === "✓" && row.writable === "✓" && testData.length === 4, "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " + row.bacnetCur + " | bacnetWrite: " + row.bacnetWrite + " | bacnetWriteLevel: " + row.bacnetWriteLevel + " | dis: " + row.dis + " | is: " + row.is + " | kind: " + row.kind + " | point: " + row.point + " | writable: " + row.writable);

          if (row.bacnetCur === "LP1" || row.bacnetCur === "LP2" || row.bacnetCur === "LP3" || row.bacnetCur === "LP4") {
            validArray.push(row.bacnetCur);
          }

        }

        ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
      });
  });

});