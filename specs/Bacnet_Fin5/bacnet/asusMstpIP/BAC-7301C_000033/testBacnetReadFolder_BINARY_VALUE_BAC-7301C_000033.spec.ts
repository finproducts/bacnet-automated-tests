import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let expectedArray = [
  "BV1",
  "BV2",
  "BV3",
  "BV4",
  "BV5",
  "BV6",
  "BV7",
  "BV8",
  "BV9",
  "BV10",
  "BV11",
  "BV12",
  "BV13",
  "BV14",
  "BV15",
  "BV16",
  "BV17",
  "BV18",
  "BV19",
  "BV20",
  "BV21",
  "BV22",
  "BV23",
  "BV24",
  "BV25",
  "BV26",
  "BV27",
  "BV28",
  "BV29",
  "BV30",
  "BV31",
  "BV32",
  "BV33",
  "BV34",
  "BV35",
  "BV36",
  "BV37",
  "BV38",
  "BV39",
  "BV40"
];

let connDis = 'BAC-7301C_000033';

let bacnetConn = 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let folder = '`BINARY_VALUE`';

let parameter = bacnetConn + "," + folder;

let po = new axonPO({"testQuery": "bacnetLearn("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadFolder_BINARY_VALUE_BAC-7301C_000033.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadFolder_BINARY_VALUE_BAC-7301C_000033", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "bacnetLearn("  +  parameter  +  ") returned an object which doesn\'t contain an  + 'error.");

        let validArray = []

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.bacnetCur !== undefined && row.dis !== undefined && row.is === "BINARY_VALUE" && row.kind === "Bool" && row.point === "✓" && row.bacnetWrite !== undefined && row.bacnetWriteLevel !== undefined && row.enum === "Off,On" && testData.length === 40, "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " + row.bacnetCur + " | dis: " + row.dis + " | is: " + row.is + " | kind: " + row.kind + " | point: " + row.point + " | bacnetWrite: " + row.bacnetWrite + " | bacnetWriteLevel: " + row.bacnetWriteLevel + " | enum: " + row.enum);

          if (row.bacnetCur === "BV1" || row.bacnetCur === "BV2" || row.bacnetCur === "BV3" || row.bacnetCur === "BV4" || row.bacnetCur === "BV5" || row.bacnetCur === "BV6" || row.bacnetCur === "BV7" || row.bacnetCur === "BV8" || row.bacnetCur === "BV9" || row.bacnetCur === "BV10" || row.bacnetCur === "BV11" || row.bacnetCur === "BV12" || row.bacnetCur === "BV13" || row.bacnetCur === "BV14" || row.bacnetCur === "BV15" || row.bacnetCur === "BV16" || row.bacnetCur === "BV17" || row.bacnetCur === "BV18" || row.bacnetCur === "BV19" || row.bacnetCur === "BV20" || row.bacnetCur === "BV21" || row.bacnetCur === "BV22" || row.bacnetCur === "BV23" || row.bacnetCur === "BV24" || row.bacnetCur === "BV25" || row.bacnetCur === "BV26" || row.bacnetCur === "BV27" || row.bacnetCur === "BV28" || row.bacnetCur === "BV29" || row.bacnetCur === "BV30" || row.bacnetCur === "BV31" || row.bacnetCur === "BV32" || row.bacnetCur === "BV33" || row.bacnetCur === "BV34" || row.bacnetCur === "BV35" || row.bacnetCur === "BV36" || row.bacnetCur === "BV37" || row.bacnetCur === "BV38" || row.bacnetCur === "BV39" || row.bacnetCur === "BV40") {
            validArray.push(row.bacnetCur);
          }

        }

        ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
      });
  });

});