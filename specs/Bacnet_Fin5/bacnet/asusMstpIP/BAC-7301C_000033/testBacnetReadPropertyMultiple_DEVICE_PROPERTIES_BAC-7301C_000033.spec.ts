import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let expectedArray = [
  "MAX_APDU_LENGTH_ACCEPTED",
  "MODEL_NAME",
  "OBJECT_IDENTIFIER",
  "OBJECT_NAME",
  "PROTOCOL_REVISION",
  "PROTOCOL_SERVICES_SUPPORTED",
  "PROTOCOL_VERSION",
  "SYSTEM_STATUS",
  "VENDOR_NAME"
];

let bacnetConn = 'read(bacnetConn and dis=="BAC-7301C_000033")';

let deviceObject = 'DEV3080225';

let deviceProps = '{max_apdu_length_accepted, "  +  "model_name, "  +  "object_identifier, "  +  "o' +
    'bject_name, "  +  "protocol_revision, "  +  "protocol_services_supported, "  +  ' +
    '"protocol_version, "  +  "system_status, "  +  "vendor_name}';

let parameter = bacnetConn + ", {" + JSON.stringify(deviceObject) + ":" + deviceProps + "}";

let po = new axonPO({"testQuery": "bacnetReadPropertyMultiple("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadPropertyMultiple_DEVICE_PROPERTIES_BAC-7301C_000033.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadPropertyMultiple_DEVICE_PROPERTIES_BAC-7301C_000033", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetReadPropertyMultiple ("  +  parameter  +  ") query result is not undefined" +
            ' or null.');

        ok(typeof testData === "object" && testData.type !== "error", "bacnetReadPropertyMultiple ("  +  parameter  +  ") returned an object which does" +
            'n\'t contain an error.');

        let validArray = []

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.bacnetValueType !== undefined && row.objectId === deviceObject && row.propertyName !== undefined && row.value !== undefined, "bacnetReadPropertyMultiple (" + parameter + ") query contains valid data on row " + i + " => bacnetValueType: " + row.bacnetValueType + " | objectId: " + row.objectId + " | propertyName: " + row.propertyName + " | value: " + row.value);

          if (row.propertyName === "MAX_APDU_LENGTH_ACCEPTED" || row.propertyName === "MODEL_NAME" || row.propertyName === "OBJECT_IDENTIFIER" || row.propertyName === "OBJECT_NAME" || row.propertyName === "PROTOCOL_REVISION" || row.propertyName === "PROTOCOL_SERVICES_SUPPORTED" || row.propertyName === "PROTOCOL_VERSION" || row.propertyName === "SYSTEM_STATUS" || row.propertyName === "VENDOR_NAME") {
            validArray.push(row.propertyName);
          }

        }

        ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected properties: " + JSON.stringify(validArray));
      });
  });

});