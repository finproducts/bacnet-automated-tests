import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let bacnetPoint = 'BV5';

let connDis = 'BAC-7301C_000033';

let bacnetConn = 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")';

let parameter = '"bacnet", read(point and bacnetCur=="  +  JSON.stringify(bacnetPoint)  +  " and ' +
    'bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")->id';

let po = new axonPO({"testQuery": "bacnetPing("  +  bacnetConn  +  ")", "confirmQuery": "connAppDetails("  +  parameter  +  ")"});
po.setup(function (instance) {})

describe('testConnAppDetails_POLLING_BV5_BAC-7301C_000033_part1.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing testConnAppDetails_POLLING_BV5_BAC-7301C_000033_part1", () => {
    po
      .assert(function (testData, confirmData) {
        ok(confirmData !== undefined && confirmData !== undefined, "connAppDetails("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof confirmData === "object" && confirmData.type !== "error", "connAppDetails("  +  parameter  +  ") returned an object which doesn\'t contain ' +'an error.");

        ok(confirmData[0].val.includes('isPolling: true') === false && confirmData[0].val.includes("bacnetCur:      " + bacnetPoint) === false && confirmData[0].val.includes("conn:           " + connDis) === false && confirmData[0].val.includes('isPollingDegraded: false') === false && confirmData[0].val.includes('watchStatus:  isPolling') === false, "connAppDetails("  +  parameter  +  ") query contains valid data => val: "  +  confirmData[0].val  +  "' "  +  " "  +  " "  +  " Point "  +  bacnetPoint  +  " from "  +  connDis  +  " is in Polling.");
});
    });});