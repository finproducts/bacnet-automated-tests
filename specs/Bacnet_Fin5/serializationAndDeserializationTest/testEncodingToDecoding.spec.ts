import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';





function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let name= createRandomString();

let phoneType1= createRandomString();

let phoneType2= createRandomString();

let vehicleType= createRandomString();

let color= createRandomString();

function createRandomNumber() {

	var min = 0;
	var max = 300;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let value1= createRandomNumber();

let value2= createRandomNumber();

let topSpeed= createRandomNumber();

let paramArray= [true, false];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let boolVal= paramArray[randomIndex];

let boolHasOwner= boolVal;

let parameter= '{name: "  +  JSON.stringify(name)  +  ", phones: [{type: "  +  JSON.stringify(phoneType1)  +  ", value: "  +  value1  +  "}, {type: "  +  JSON.stringify(phoneType2)  +  ", value: "  +  value2  +  "}], hasOwner: "  +  boolHasOwner  +  ", vehicle: {type: "  +  JSON.stringify(vehicleType)  +  ", color: "  +  JSON.stringify(color)  +  ", topSpeed:"  +  topSpeed  +  "mph}}';



let po = new axonPO({"\"testQuery\"":"\"mirror(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testEncodingToDecoding.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing encoding to decoding"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'mirror("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'mirror("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(testData[0].name  ===  name  &&  testData[0].phones[0].type  ===  phoneType1  &&  testData[0].phones[0].value  ===  value1  &&  testData[0].phones[1].type  ===  phoneType2  &&  testData[0].phones[1].value  ===  value2  &&  testData[0].hasOwner  ===  boolHasOwner  &&  testData[0].vehicle.type  ===  vehicleType  &&  testData[0].vehicle.color  ===  color  &&  testData[0].vehicle.topSpeed  ===  topSpeed  &&  testData[0].vehicle.unit  ===  "mph",'mirror("  +  parameter  +  ") query results contains valid data: "  +  "
"  +  JSON.stringify(testData)  +  "
"  +  "The JS object returned contains the same keys, values, data types & structure with the axon object passed to the axon function mirror()');
}
);
  });  

});