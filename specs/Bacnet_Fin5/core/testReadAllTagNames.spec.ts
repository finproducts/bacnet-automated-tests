import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible parameters
let parameterArray = ["equip", "point", "sensor", "filter", "run", "cur", "curVal", "leaving", "rooftop", "siteRef", "centralPlant", "navName"];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex];

let po = new axonPO({"testQuery": "readAllTagNames("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The readAllTagNames() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "readAllTagNames("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "readAllTagNames("+ randomParameter +") returned an object which doesn't contain an error.");

      let rowIndex, i;
      let found = false;

      for(i = 0; i < testData.length; i ++) {
        let row = testData[i];
        ok(row.name !== undefined
        && row.kind !==  undefined
        && row.count !== undefined, "readAllTagNames("+ randomParameter +") query results contains valid data on row "+ i +" =>"+
        " name: "+ row.name +" | kind: "+ row.kind +" | count: "+ row.count);

        if( row.name === randomParameter) {
          found = true;
          rowIndex = i;
        }
      }
      equal(found, true, "The query result contains the parameter - " + randomParameter + " - on row: "+ rowIndex);
    });
  });
});