import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomString() {	
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 10; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomString = '"'+ createRandomString() +'"' ;

let randomNumber = createRandomNumber();

//array of possible parameters
let paramArray = [randomString, randomNumber, true, false];

//randomizfe
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isBool("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testIsBool() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isBool("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isBool("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === randomString 
      || randomParameter === randomNumber) {
        ok(testData[0].val  ===  false,"isBool("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }

      else if(randomParameter === true 
      || randomParameter === false) {
        ok(testData[0].val === true,"isBool("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }     
    });
  });  
});