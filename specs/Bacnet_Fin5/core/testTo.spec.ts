import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that generates a random number
function getRandomNumber() {
	var min = 1;
	var max = 100;
	var generateRandomNumber = Math.floor(Math.random() * (max - min) + min);

	return generateRandomNumber;	
};

let value= getRandomNumber();

//array of operators
let unitArray = ["%", "cfm", "km"];
let randomIndex = Math.floor(Math.random() * unitArray.length);
let unit = unitArray[randomIndex];

//the random parameter that will be passed to the axon function
let parameter = value +","+ JSON.stringify(unit);

let po = new axonPO({"testQuery": "to("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testTo.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "to("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "to("+ parameter +") returned an object which doesn't contains an error.");

      ok(testData[0].val === value 
      && testData[0].valUnit === unit, 
      "to("+ parameter +") query results contain invalid data => val: "+ testData[0].val +" | valUnit: "+ testData[0].valUnit);
    });
  });  
});