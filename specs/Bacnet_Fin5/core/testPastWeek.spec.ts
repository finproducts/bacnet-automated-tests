import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "pastWeek()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The pastWeek() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'pastWeek() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", 'pastWeek() returned an object which doesn\'t contain an error.');

      ok(testData[0].val.includes('[proj::DateSpan]') === true, "pastWeek() query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});