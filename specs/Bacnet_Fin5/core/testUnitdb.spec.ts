import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "unitdb()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The unitdb()',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'unitdb() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", 'unitdb() returned an object which doesn\'t contain an error.');

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.name !== undefined 
        && row.quantity !== undefined 
        && row.symbol !== undefined,
        "unitdb() query results contain invalid data on row "+ i +" => name: "+ row.name +" | quantity: "+
        row.quantity +" | symbol: "+ row.symbol);
      }
    });
  });  
});