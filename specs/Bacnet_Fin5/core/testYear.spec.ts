import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//current year
let date = new Date();
let currentYear = date.getFullYear();

//argument passed to the axon function
let parameter = 'today()';

let po = new axonPO({"testQuery": "year("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The year() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "year("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "year("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === currentYear 
      && testData[0].valUnit === "yr",
      "year("+ parameter +") query results contain invalid data => val: "+ testData[0].val +" | valUnit: "+ testData[0].valUnit);
    });
  });  
});