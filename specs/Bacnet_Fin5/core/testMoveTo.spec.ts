import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = 0;
	var max = 3;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

//argument passed to the axon function
let moveToIndex = createRandomNumber(); 

let po = new axonPO({"testQuery": "[1,2,3,4,5].moveTo(5," + moveToIndex +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The moveTo() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "[1,2,3,4,5].moveTo(5,"+ moveToIndex +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "[1,2,3,4,5].moveTo(5,"+ moveToIndex +") returned an object which doesn't contain an error.");

      ok(testData[moveToIndex].val === 5, 
      "[1,2,3,4,5].moveTo(5,"+ moveToIndex +") query results contain invalid data on index "+ moveToIndex +": "+ JSON.stringify(testData));
    });
  });  
});