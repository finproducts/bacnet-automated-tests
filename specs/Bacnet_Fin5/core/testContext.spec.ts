import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery":"context()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The context() query.js', () =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "context() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "context() returned an object which doesn't contain an error.");

      let row = testData[0];

      ok(row.locale !== undefined 
      && row.projName === "demo" 
      && row.timeout !== undefined 
      && row.username === "su", 
      "context() query results contains invalid data => locale: "+ row.locale +" | projName: "+
      row.projName +" | timeout: "+ row.timeout +" | username: "+ row.username);
    });
  });  
});