import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 5; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

//string array
let fileArray = ["fileName.csv","fileName.xls","fileName.pdf","fileName.png","fileName.svg","fileName.xml"];
let randomIndex = Math.floor(Math.random() * fileArray.length);
let randomFileExt = fileArray[randomIndex];

//the random parameters that will be passed to the axon faction
let randomString1 = createRandomString();
let randomString2 = createRandomString();
let randomParameter = "`"+ "http://host:81/" + randomString1 +"/"+ randomString2 +"/"+ randomFileExt + "`";

let po = new axonPO({"testQuery": "uriPathStr("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The uriPathStr()',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "uriPathStr("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "uriPathStr("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === randomParameter.substring(15,40), 
      "uriPathStr("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});