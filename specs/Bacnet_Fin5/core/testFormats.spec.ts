import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//values of fileExt 
let fileExtArray = ["csv","xls","html","json","pdf","png","svg","trio","xml","zinc"];

let po = new axonPO({"testQuery": "formats()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The formats() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "formats() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "formats() returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];  

        if(fileExtArray.indexOf(row.fileExt) > -1) {
          ok(row.dis !== undefined 
          && row.fileExt !== undefined 
          && row.mimeType !== undefined 
          && row.writer !== undefined, 
          "formats() query results contains invalid data on row "+ i +" => dis: "+
          row.dis +" | fileExt: "+ row.fileExt +" | mimeType: "+ row.mimeType +" | writer: "+ row.writer);
        }
      }
    });
  });  
});