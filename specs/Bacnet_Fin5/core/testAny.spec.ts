import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import { ok, equal, notEqual } from "../../../helpers/nwapis";
import { waitService } from "../../../helpers/wait-service";
import axonPO from "../../../page_objects/finstack/axon.page";

//array values
let number1 = 1;
let number2 = 2;
let number3 = 3;

//argument passed to the axon function
let randomParameter ="["+ number1 +","+ number2 +","+ number3 +"]";

//random function selection
let funcArray = ["isOdd", "isEven"];
let randomIndex = Math.floor(Math.random() * funcArray.length);
let funcParam = funcArray[randomIndex];

let parameter = randomParameter + ".any("+ funcParam +")";

let po = new axonPO({"testQuery": parameter, "confirmQuery": ""});
po.setup(function(instance) {});

describe("The testAny() query.js", () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, parameter + "query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", parameter +" returned an object which doesn't contain an error.");

      if (number1 % 2 !== 0 ||
      number2 % 2 !== 0 ||
      (number3 % 2 !== 0 && funcParam === "isOdd")) {
        ok(testData[0].val === true,
        parameter + "query result contains invalid data: "+ testData[0].val);

      } else if (number1 % 2 === 0 
      ||number2 % 2 === 0 
      ||(number3 % 2 === 0 && funcParam === "isEven")) {
        ok(testData[0].val === true,
        parameter + " query result contains invalid data: " + testData[0].val);

      } else {
        ok(testData[0].val === false,
        parameter + " query result contains invalid data: " + testData[0].val);
      }
    });
  });
});
