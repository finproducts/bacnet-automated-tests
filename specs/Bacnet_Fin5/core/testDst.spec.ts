import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let paramArray = ['parseDateTime("2015-10-16T09:18:16.000Z DST","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")',
                'parseDateTime("2015-10-16T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")'];

let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery":"dst("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The dst()query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "dst("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "dst("+ randomParameter +") returned an object which doesn't contain an error.");

      if( randomIndex === 0) {
        ok(testData[0].val === true, 
        "dst("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }
      else if( randomIndex  ===  1) {
        ok(testData[0].val  ===  false, 
        "dst("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }
    });
  });  
});