import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//argument passed to the axon faction 
let parameter= 'readAll(point and unit=="%"),"curVal"';

let po = new axonPO({"testQuery": "colToList("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testColToList() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "colToList("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "colToList("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        let notificationMessage = "colToList("+ parameter +") query results contains valid data on row "+ i +" => "

        if(row.val !== undefined) {
          notificationMessage = notificationMessage + "val: "+ row.val;
        }

        if( JSON.stringify(testData[i]) === "{}") {
          notificationMessage = notificationMessage + "emptyObject: "+ JSON.stringify(testData[i]);
        }

      ok(testData[0].val  !==  undefined  ||  JSON.stringify(testData[0])  ===  "{}",notificationMessage);
      }
    });
  });  
});