import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//array of possible parameters
let paramArray = ["min", "avg", "max"];

let randomIndex = Math.floor(Math.random() *  paramArray.length);

//argument passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "[1,2,3,4,5].fold("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The fold()query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Return the expected results when executed', () => {
    po.assert(function(testData, confirmData) {

      ok(testData !== undefined 
      && testData !== null, "[1,2,3,4,5].fold("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "[1,2,3,4,5].fold("+ randomParameter +") returned an object which doesn't contain an error.");

      if(paramArray[randomIndex] === "min") {
        ok(testData[0].val === 1,
        "[1,2,3,4,5].fold("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }

      else if(paramArray[randomIndex] === "avg") {
        ok(testData[0].val  === 3,
        "[1,2,3,4,5].fold("+ randomParameter  +") query results contains invalid data: "+ testData[0].val);
      }

      else if( paramArray[randomIndex]  ===  "max") {
        ok(testData[0].val === 5, 
        "[1,2,3,4,5].fold("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }     
    });
  });  
});