import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let grid = "[{a:1,b:2}].toGrid()";

//new row added to the grid
let newRow = "{c:3}";

//argument passed to the axon function
let parameter = grid + "," + newRow;

let po = new axonPO({"testQuery": "addRow("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testAddRow() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "addRow("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "addRow("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"a":1,"b":2},{"c":3}]', "addRow("+
      parameter +") results contains invalid data: "+ JSON.stringify(testData));
    });
  });  
});