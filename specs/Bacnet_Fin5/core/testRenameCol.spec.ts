import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let grid = '[{a:1, b:2}].toGrid';

//column name to be changed
let oldColName = 'a';

//new column name
let newColName = 'selenium';

//argument passed to the axon function
let parameter = grid +","+ JSON.stringify(oldColName) +","+ JSON.stringify(newColName);

let po = new axonPO({"testQuery": "renameCol("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The renameCol() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "renameCol("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "renameCol("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].selenium === 1 
      && testData[0].b === 2, 
      "renameCol("+ parameter +") query results contain invalid data => selenium: "+ testData[0].selenium +" | b: "+ testData[0].b);
    });
  });  
});