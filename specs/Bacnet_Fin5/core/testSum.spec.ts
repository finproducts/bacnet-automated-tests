import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible parameters
let parameterArray = [{param1: -1 , param2: 3}, {param1: 200, param2: -300}, {param1: -500, param2: 0}];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameters that will be passed to the axon function
let randomParameter1 = parameterArray[randomIndex].param1;
let randomParameter2 = parameterArray[randomIndex].param2;

let po = new axonPO({"testQuery": "sum("+ randomParameter1 +", "+ randomParameter2 +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The sum() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "sum("+ randomParameter1 +","+ randomParameter2 +") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
      "sum("+ randomParameter1 +","+ randomParameter2 +") returned an object which doesn't contain an error.");

      ok(testData[0].val === (randomParameter1 + randomParameter2), 
      "sum("+ randomParameter1 +","+ randomParameter2 +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});