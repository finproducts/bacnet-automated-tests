import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
  var randomStr = "";
  var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i = 0; i < 5; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

//file extension array
let fileArray = ["fileName.csv","fileName.xls","fileName.html","fileName.json",
"fileName.pdf","fileName.png","fileName.svg","fileName.trio","fileName.xml","fileName.zinc"];

let randomIndex = Math.floor(Math.random() * fileArray.length);
let randomFileExt = fileArray[randomIndex];

//the random parameter that will be passed to the axon function
let randomString1 = createRandomString();
let randomString2 = createRandomString();
let randomParameter = "`"+ "http://host:81/" + randomString1 +"/"+ randomString2 +"/"+ randomFileExt + "`"+ ".uriPath";

let po = new axonPO({"testQuery" : randomParameter, "confirmQuery ": ""});
po.setup(function(instance) {})

describe('testUripathjs',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "" + randomParameter +" query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", ""+ randomParameter +" returned an object which doesn't contain an error.");

      ok(testData[0].val === randomString1 
      && testData[1].val === randomString2 
      && testData[2].val === randomFileExt,
      ""+ randomParameter +" query results contain invalid data => val: "+
      testData[0].val +" | val:"+ testData[1].val +" | val: "+ testData[2].val);
    });
  });  
});