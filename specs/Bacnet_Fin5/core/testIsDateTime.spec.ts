import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
	
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 10; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomString = '"'+ createRandomString() +'"';
let randomNumber = createRandomNumber();
let dateTime = 'parseDateTime("2015-10-15T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")';

//array of parameters randomly selected to be passed to the axon function
let paramArray = [randomString, randomNumber, dateTime, true, false];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isDateTime("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isDateTime() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isDateTime("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isDateTime("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter !== dateTime) {
        ok(testData[0].val === false, 
        "isDateTime("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }     

      else if(randomParameter === dateTime) { 
        ok(testData[0].val === true, 
        "isDateTime("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });
});