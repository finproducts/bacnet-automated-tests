import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//tree name
let treeName = 'equip';

//record
let recId = 'readAll(equip)[0]->id';

//argument passed to the axon function
let parameter = JSON.stringify(treeName) + "," + recId;

let po = new axonPO({"testQuery": "navCommonAncestor("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance){});

describe('The navCommonAncestor() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "navCommonAncestor("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "navCommonAncestor("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.id !== undefined 
        && row.dis !== null 
        && row.floor === "✓" 
        && row.mod !== undefined 
        && row.siteRef !== undefined, 
        "navCommonAncestor("+ parameter +") query results contain invalid data on row "+ i +" => id: "+ row.id +" | dis: "+
        row.dis +" | floor: "+ row.floor +" | mod: "+ row.mod +" | siteRef: "+ row.siteRef);
      }
    });
  });  
});