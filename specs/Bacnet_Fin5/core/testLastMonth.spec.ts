import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//get current month & year
let today = new Date();
let mm:any = today.getMonth();
let yyyy = today.getFullYear();

if(mm < 10 && mm != 0) {
  mm = "0" + mm;
}
else if(mm === 0) { 
  yyyy = yyyy - 1;
  mm = 12;
}

//the result format of the axon function lastMonth() 
let lastMonthAxonFormat = yyyy + "-" + mm + "-01,month [proj::DateSpan]";

let po = new axonPO({"testQuery": "lastMonth()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The lastMonth() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "lastMonth() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "lastMonth() returned an object which doesn't contain an error.");

      ok(testData[0].val === lastMonthAxonFormat, "lastMonth() query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});