import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//string
let string = 'selenium';

//argument that will be passed to the axon function
let parameter = JSON.stringify(string);

let po = new axonPO({"testQuery": "upper("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The upper() query ',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "upper("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "upper("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === string.toUpperCase(), "upper("+ parameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});