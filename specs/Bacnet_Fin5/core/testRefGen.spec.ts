import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "refGen()", "confirmQuery": "refGen()"});
po.setup(function(instance) {});

describe('The refGen() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'refGen() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", 'refGen() returned an object which doesn\'t contain an error.');

      ok(testData[0].val !== undefined, "refGen() query results contain invalid data => val: "+ testData[0].val);

      ok(confirmData !== undefined 
      && confirmData !== null, 'refGen() query result is not undefined or null.');

      ok(typeof confirmData === "object" 
      && confirmData.type !== "error", 'refGen() returned and object which doesn\'t contain an error.');

      ok(confirmData[0].val !== undefined, "refGen() query results contain invalid data => val: "+ confirmData[0].val);

      notEqual(testData[0].val,confirmData[0].val, 'refGen() query doesn\'t generate the same ref identifier.');
    });
  });  
});