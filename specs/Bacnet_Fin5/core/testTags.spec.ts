import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible parameters
let parameterArray = ['kind =="Ref"', 'ext =="equip"', 'tag =="dis"'];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

// the random parameter that will be passed to the axon function
let randomParameter= parameterArray[randomIndex];

let po = new axonPO({"testQuery": "tags("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The tags() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "tags("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "tags("+ randomParameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.doc !== undefined 
        && row.ext !== undefined 
        && row.kind === "Ref" 
        && row.tag !==  undefined, 
        "tags("+ randomParameter +") query results contain invalid data on row "+ i +" => "+ "doc: "+ row.doc +" | ext: "+
        row.ext +" | kind: "+ row.kind +" | tag: "+ row.tag);
      }
    });
  });  
});