import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";

import { ok, equal, notEqual } from "../../../helpers/nwapis";
import { waitService } from "../../../helpers/wait-service";
import axonPO from "../../../page_objects/finstack/axon.page";


//function that randomly creates a number between 1 and 6
function createRandomNumber() {
  var min = 1;
  var max = 6;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
}

let randomNumber1 = createRandomNumber();
let randomNumber2 = createRandomNumber();
let randomNumber3 = createRandomNumber();

//argument passed to the axon function
let randomParameter = "["+ randomNumber1 +","+ randomNumber2 +","+ randomNumber3 +"]";

//random function selection
let funcArray = ["isOdd", "isEven"];
let randomIndex = Math.floor(Math.random() * funcArray.length);
let funcParam = funcArray[randomIndex];
let parameter = randomParameter + ".all(" + funcParam + ")";

let po = new axonPO({"testQuery": parameter, "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testAll() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, parameter + "query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      parameter +" returned an object which doesn't contain an error.");

      if (randomNumber1 % 2 !== 0 
      && randomNumber2 % 2 !== 0 
      &&randomNumber3 % 2 !== 0 
      && funcParam === "isOdd") {
        ok(testData[0].val === true, parameter + " query result contains invalid data: " + testData[0].val);

      } else if (randomNumber1 % 2 === 0 
      && randomNumber2 % 2 === 0 
      && randomNumber3 % 2 === 0 
      && funcParam === "isEven") {
        ok(testData[0].val === true,
          parameter + " query result contains invalid data: " + testData[0].val);

      } else {
          ok(testData[0].val === false,
          parameter + " query result contains valid data: " + testData[0].val);
      }
    });
  });
});
