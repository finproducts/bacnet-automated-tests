import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//function that randomly creates a number
function createRandomNumber() {
	var min = 1;
	var max = 31;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomDay:any = createRandomNumber();

if(randomDay < 10) { 
  randomDay = "0" + randomDay;
}

//the random parameter that will be passed to the axon faction
let randomParameter = 'parseDateTime("2018-11-' + randomDay + 'T09:18:16.000Z DST","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")';

//expected result converted to miliseconds => (day) * h * min * sec * milisec
let expectedResultInMilisec = randomDay * 24 * 60 * 60 * 1000;

let po = new axonPO({"testQuery": "day("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The day() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {

      //testing day(randomParameter)
      ok(testData !== undefined 
      && testData !== null, "day("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "day("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === expectedResultInMilisec, 
      "day("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});