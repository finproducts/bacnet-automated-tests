import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//value
let value = '10km';

//argument that will be passed to the axon function
let parameter = value;

let po = new axonPO({"testQuery": "unit("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The unit() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "unit("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "unit("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === "km", "unit("+ parameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});