import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//numeric interval
let min = -1000000;
let max = 1000000;

//randomize
let getRandomNumber = Math.floor(Math.random() * (max - min) + min);

//the random parameter that will be passed to the axon faction
let randomParameter = '"'+ getRandomNumber +'"';

let po = new axonPO({"testQuery": "parseInt("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The parseInt() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined
      && testData !== null, "parseInt("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "parseInt("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === getRandomNumber, 
      "parseInt("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});