import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let paramArray = ["chartDemo","childrenCurVal","finEquipTemplateCallback"];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);
let randomSelect = paramArray[randomIndex];

//the random parameter that will be passed to the axon faction
let randomParameter = '"'+ randomSelect +'"';

let po = new axonPO({"testQuery": "readByName("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The readByName() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "readByName("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "readByName("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].name === randomSelect 
      && testData[0].id !== undefined 
      && testData[0].func === "✓", 
      "readByName("+ randomParameter +") query results contain invalid data => id: "+ testData[0].id +" | func: "+
      testData[0].func +" | name: "+ testData[0].name);
    });
  });  
});