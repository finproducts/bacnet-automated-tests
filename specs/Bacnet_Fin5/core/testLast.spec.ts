import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 5; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = createRandomString();

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();

//last member in the array passed as a argument to the axon function
let randomStringInArray = createRandomString();
let randomNumberInArray = createRandomNumber();
let randomArray = [randomStringInArray, randomNumberInArray, "abc"];

//argument that will be passed to the axon faction
let randomParameter = [randomString, randomNumber, randomArray];
let paramStringified = JSON.stringify(randomParameter);

let po = new axonPO({"testQuery": "last("+ paramStringified +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The last() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "last("+ paramStringified +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "last("+ paramStringified +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        ok(testData[i].val === randomArray[i],
        "last("+ paramStringified +") query results contain invalid data => val: "+ testData[i].val);
      }
    });
  });  
});