import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//randomly generates a number from 16 to 22
function createRandomDayOfTheMonth() {

	var min = 16;
	var max = 22;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let dayOfTheMonth= createRandomDayOfTheMonth();

//argument passed to the axon function
let randomParameter = "2015-11-" + dayOfTheMonth;

let po = new axonPO({"testQuery": "isWeekend("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isWeekend() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isWeekend("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isWeekend("+ randomParameter +") returned an object which doesn't contain an error.");

      if(dayOfTheMonth === 21 || dayOfTheMonth ===  22) {
        ok(testData[0].val === true, "isWeekend("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else {
        ok(testData[0].val === false, "isWeekend("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });  
});