import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//creating a string with 10 chars
function createRandomString() {
	
    var randomStr = "";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for(var i = 0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

//argument passed to the axon function
let randomParameter = "null, {"+ JSON.stringify(randomString) + ":marker()}, {add}";

//undo changes
let timestamp = 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

let po = new axonPO({"testQuery": "diff("+ randomParameter +")", "confirmQuery": "folioRestore("+ timestamp +")"});
po.setup(function(instance) {});

describe('The diff() query.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "diff("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "diff("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val.includes(randomString) === true,
      "diff("+ randomParameter +") query results contains invalid data => val: "+ testData[0].val);

      ok(JSON.stringify(confirmData) === "[{\"val\":null}]","Running folioRestore("+ timestamp +")... Changes undone.");
    });
  });  
});