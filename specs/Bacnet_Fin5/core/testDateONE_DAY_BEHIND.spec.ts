import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates a number
function createRandomNumber() {
	var min = 11;
	var max = 31;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();
let randomDate = "2016, 10, " + randomNumber;

//argument passed to the axon function
let paramArray = [randomDate, 'parseDateTime("2015-11-24T13:34:24+02:00 Athens", "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let randomParameter = paramArray[randomIndex];



let po = new axonPO({"testQuery": "date("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The date() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "date("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "date("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomIndex === 0) {
        ok(testData[0].val.includes("2016-10-" + (randomNumber-1)) === true, 
        "date("+ randomParameter +") query results contains invalid data => val: "+ testData[0].val);
      }

      else if(randomIndex === 1) {
        ok(testData[0].val.includes('2015-11-23') === true, 
        "date("+ randomParameter +") query results contains invalid data => val: "+ testData[0].val);
      }
    });
  });  
});