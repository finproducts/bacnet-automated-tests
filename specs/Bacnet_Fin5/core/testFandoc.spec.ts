import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
    var randomString = "";
    var charSequence = "abcdefghijklmnopqrstuvwxyz0123456789";

    for(var i = 0; i < 10; i++ )
        randomString += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomString;
};

//the random parameter that will be passed to the axon faction
let randomStringResult = createRandomString() + " " + createRandomString() + " " + createRandomString();
let randomParameter = '"' + randomStringResult + '"';

let po = new axonPO({"testQuery": "fandoc("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The fandoc() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Return the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "fandoc("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "fandoc("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].text === randomStringResult, 
      "fandoc("+ randomParameter +") query results contains invalid data: "+ testData[0].text);
    });
  });  
});