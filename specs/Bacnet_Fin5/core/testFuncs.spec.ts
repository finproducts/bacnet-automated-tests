import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "funcs()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The funcs() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'funcs() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", "funcs() returned an object which doesn't contain an error.");
      
      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.func === "✓" && row.name !== undefined, 
        "funcs() query results contains invalid data on row "+ i +" => func: "+ row.func +" | name: "+ row.name);
      }
    });
  });  
});