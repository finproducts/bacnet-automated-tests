import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 10; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = '"'+ createRandomString() +'"';

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomNumber= createRandomNumber();

//array of eligible arguments
let paramArray = [randomString, randomNumber];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isNumber("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isNumber() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isNumber("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isNumber("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === randomString) {
        ok(testData[0].val === false, "isNumber("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else if(randomParameter === randomNumber) {
        ok(testData[0].val === true, "isNumber("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });  
});