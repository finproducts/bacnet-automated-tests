import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//1st param
let pointId1 = 'readAll(point and siteRef->dis=="City Center")[0]->id';

//2nd param
let pointId2 = 'readAll(point and siteRef->dis=="City Center")[1]->id';

//the random parameter that will be passed to the axon faction
let parameter = "["+ pointId1 + "," + pointId2 +"]";

let po = new axonPO({"testQuery": "readByIds("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The readByIds() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "readByIds("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "readByIds("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined 
        && row.dis.includes('City Center') === true 
        && row.curStatus !== undefined 
        && row.curVal !== undefined 
        && row.disMacro !== undefined 
        && row.equipRef !== undefined 
        && row.floorRef !== undefined 
        && row.mod !== undefined 
        && row.navName !== undefined 
        && row.siteRef !== undefined, 
        "readByIds("+ parameter +") query results contain invalid data on row "+ i +" => id: "+ row.id +" | curStatus: "+
        row.curStatus +" | curVal: "+ row.curVal +" | disMacro: "+ row.disMacro +" | equipRef: "+ row.equipRef +" | floorRef: "+
        row.floorRef +" | mod: "+ row.mod +" | navName: "+ row.navName +" | siteRef: "+ row.siteRef);
      }
    });
  });  
});