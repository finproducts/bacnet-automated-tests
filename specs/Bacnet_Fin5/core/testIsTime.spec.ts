import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = 0;
	var max = 100;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNum = createRandomNumber();

//function that randomly generates a time variable
function createRandomTimeObject() {
 var time = {min: 0, maxHour: 24, maxMin: 59};
 var getHour = Math.floor(Math.random() * (time.maxHour - time.min) + time.min);
 var getMin = Math.floor(Math.random() * (time.maxMin - time.min) + time.min);
 var randomTimeObject = ((getHour < 10 ? "0" : "") + getHour) +":"+ ((getMin < 10 ? "0" : "") + getMin);
 return randomTimeObject;
};

let randomTime = createRandomTimeObject();

//array of possible parameters
let paramArray = [randomNum, randomTime];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"\"testQuery": "isTime("+ randomParameter +")","confirmQuery": ""});
po.setup(function(instance) {});

describe('The isTime() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isTime("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "isTime("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === randomNum) {
        ok(testData[0].val === false, 
        "isTime("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else if(randomParameter === randomTime) {
        ok(testData[0].val === true, 
        "isTime("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }      
    });
  });  
});