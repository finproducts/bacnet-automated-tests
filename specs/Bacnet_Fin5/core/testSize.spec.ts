import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//array of eligible parameters
let parameterArray = ["tags(ext)", 'tags(ext == "equip")'];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex];

let po = new axonPO({"testQuery": "size("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The size() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "size("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "size("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined, "size("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});