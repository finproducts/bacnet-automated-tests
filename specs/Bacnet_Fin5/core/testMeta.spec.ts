import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//filter
let filter = "ahu";

//argument passed to the axon function
let parameter = "readAll("+ filter +")";

let po = new axonPO({"testQuery": "meta("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The meta() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "meta("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "meta("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].projName === "demo", 
      "meta("+ parameter +") query results contain invalid data => projName: "+ testData[0].projName);
    });
  });  
});