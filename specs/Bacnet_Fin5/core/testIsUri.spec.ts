import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i = 0; i < 5; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = '"' + createRandomString() + '"';

//random uri format
let randomUri= "`"+ "http://host:81/" + createRandomString() +"/"+ createRandomString() +
 "/"+ createRandomString() +"`";

//array of possible parameters
let paramArray = [randomString, randomNumber, randomUri, true, false];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isUri("+ randomParameter + ")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isUri() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isUri("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isUri("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === randomUri) {
        ok(testData[0].val === true,
        "isUri("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
      }
      else if(randomParameter !== randomUri) {
        ok(testData[0].val === false,
        "isUri("+ randomParameter +") query results contains valid data => val: "+ testData[0].val);
      }
    });
  });  
});