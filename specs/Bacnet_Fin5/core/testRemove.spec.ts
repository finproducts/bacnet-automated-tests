import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//list
let list = '["a", "b", "c"]';

//index to remove from
let removeIndex = '0';

//argument passed to the axon function
let parameter = list + "," + removeIndex;

let po = new axonPO({"testQuery": "remove("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The remove() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "remove("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "remove("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"val":"b"},{"val":"c"}]', 
      "remove("+ parameter +") query results contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});