import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "lastWeek()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The lastWeek() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "lastWeek() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "lastWeek() returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined 
      && testData[0].val.includes('week [proj::DateSpan]') === true,
      "lastWeek() query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});