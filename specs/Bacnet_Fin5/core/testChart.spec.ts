import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



function createRandomNumber() {
	var min = 0;
	var max = 10;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//array of possible arguments
let paramArray = ["hour", "minute", "second"];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let timeUnit = paramArray[randomIndex];

//argument passed to the axon function
let parameter = "hisRead(readAll(point)["+ index +"]->id, now()-1" + timeUnit +"..now())";

let po = new axonPO({"testQuery": "chart("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The chart() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "chart("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "chart("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.ts !== undefined 
        && row.v0 !== undefined 
        &&  typeof row.v0 === "number", 
        "chart("+ parameter +") query result contains invalid data on row "+ i +" => ts: "+ row.ts +" | v0: "+ row.v0);
      }
    });
  });  
});