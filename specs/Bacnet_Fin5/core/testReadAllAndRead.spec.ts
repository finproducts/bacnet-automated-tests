import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible parameters
let parameterArray = ["equip", "point", "sensor", "filter", "run", "cur", "curVal", "leaving"];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex];

let po = new axonPO({"testQuery": "readAll("+ randomParameter +")", "confirmQuery": "read("+ randomParameter +")"});
po.setup(function(instance) {});

describe('testReadAllAndRead() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "readAll("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "readAll("+ randomParameter +") returned and object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined, "readAll("+ randomParameter +") query results contain invalid data on row "+ i +" => id: "+ row.id);
      }

      ok(confirmData !== undefined 
      && confirmData !== null, "read("+ randomParameter +") query result is not undefined or null.");

      ok(typeof confirmData === "object" 
      && confirmData.type !==  "error", "read("+ randomParameter +") returned and object which doesn't contain an error.");

      ok(confirmData[0].id !== undefined, "read("+ randomParameter +") contains invalid data => id: "+ confirmData[0].id);

      equal(testData[0].id,confirmData[0].id, 
      "The first id from readAll("+ randomParameter +"): "+
      testData[0].id +" is the same with the id from read("+ randomParameter +"): "+ confirmData[0].id);
    });
  });  
});