import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//get current year
let today = new Date();
let currentYear = today.getFullYear();

//array of possible parameters
let paramArray = [{m:1, d:31}, {m:2, d:28 , leapYearDay: 29}, {m:3, d:31}, {m:4, d:30}, {m:5, d:31}, {m:6, d:30},
                  {m:7, d:31}, {m:8, d:31}, {m:9, d:30}, {m:10, d:31}, {m:11, d:30}, {m:12, d:31}];

//randomize
let randomIndex = Math.floor(Math.random()  *  paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex].m;

//expected result converted to miliseconds => (day) * h * min * sec * milisec
let numDaysOfMonthInMilisec = paramArray[randomIndex].d * 24 * 60 * 60 * 1000;
let febDaysOfLeapYearInMilisec = paramArray[1].leapYearDay * 24 * 60 * 60 * 1000;
let isLeap;

let po = new axonPO({"testQuery": "numDaysInMonth("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The numDaysInMonth() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "numDaysInMonth("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "numDaysInMonth("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === paramArray[1].m 
      && (isLeap = new Date(currentYear, 1, 29).getMonth()==1) === true) {
        ok(testData[0].val === febDaysOfLeapYearInMilisec, 
        "numDaysInMonth("+ randomParameter +") query results contains valid data: "+ testData[0].val +" miliseconds");
      }
      else {
        ok(testData[0].val === numDaysOfMonthInMilisec, 
        "numDaysInMonth("+ randomParameter +") query results contain invalid data: "+ testData[0].val +" miliseconds");
      }     
    });
  });
});