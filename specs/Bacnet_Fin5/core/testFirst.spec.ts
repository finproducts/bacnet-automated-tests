import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 10; i++)
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = createRandomString();

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

//argument passed to the axon function
let randomNumber = createRandomNumber();
let randomParameter = "["+ JSON.stringify(randomString) +", "+ randomNumber +", "+ true +", "+ false +"]";

let po = new axonPO({"testQuery": "first("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The first() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Return the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "first("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "first("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === randomString,
      "first("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});