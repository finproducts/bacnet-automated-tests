import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



let po = new axonPO({"testQuery": "folioSnapshots()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The folioSnapshots() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'folioSnapshots() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", "folioSnapshots() returned an object which doesn't contain an error.");

      if(JSON.stringify(testData) === "[]") { 
        ok(JSON.stringify(testData) === "[]", "No snapshots found: "+ JSON.stringify(testData));
      }
      else { 
        for(let i = 0; i < testData.length; i++) {
          let row = testData[i];
          ok(row.ts !== undefined 
          && row.size !== undefined, 
          "folioSnapshots() query results contains invalid data on row "+ i +" => ts: "+ row.ts +" | size: "+ row.size);
        }
      }  
    });
  });  
});