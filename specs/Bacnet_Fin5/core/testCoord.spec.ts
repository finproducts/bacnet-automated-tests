import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {

	var min = -50;
	var max = 50;
	var getRandomNumber = Math.floor((Math.random() * (max - min) + min) * 100) / 100;
	return getRandomNumber;	
};

let randomNumber1 = createRandomNumber();
let randomNumber2 = createRandomNumber();
let returnedRandomNumber1, returnedRandomNumber2;

if(randomNumber1 == Math.round(randomNumber1)) { 
	returnedRandomNumber1 = randomNumber1 + ".0"
}

else {
		returnedRandomNumber1 = randomNumber1
}

if(randomNumber2 == Math.round(randomNumber2)) {    
	returnedRandomNumber1 = randomNumber1 + ".0"
}

else {
	returnedRandomNumber2 = randomNumber2
}

let randomParameter = randomNumber1 + "," + randomNumber2;

let po = new axonPO({"testQuery": "coord("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The coord() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed ', () => {
    po.assert(function(testData,confirmData) {
			ok(testData !== undefined 
			&& testData !== null, "coord("+ randomParameter +") query result is not undefined or null.");

			ok(typeof testData === "object" 
			&& testData.type !== "error", "coord("+ randomParameter +") returned an object which doesn't contain an error.");

			ok(testData[0].val === returnedRandomNumber1 + "," + returnedRandomNumber2, "coord("+
			randomParameter +") query results contains invalid data => val: "+ testData[0].val);
		});
  });  
});