import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible arguments
let parameterArray = ["readAll(ahu)", "blocks()", "tasks()"];

//randomize
let randomIndex = Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex];

let po = new axonPO({"testQuery": "colsToLocale("+ randomParameter +")","confirmQuery":""});
po.setup(function(instance) {});

describe('testColsToLocale.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('The colsToLocale() query', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "colsToLocale("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "colsToLocale("+ randomParameter +") returned and object which doesn't contain an error.");

      if( randomParameter  ===  "readAll(ahu)") { 
        for(let i = 0; i < testData.length; i++) {
          let row= testData[i];

          ok(row.id !== undefined 
          && row.disMacro === "$siteRef $navName" 
          && row.floorRef !== undefined 
          && row.mod !== undefined 
          && row.navName !== undefined 
          && row.siteRef !== undefined, 
          "colsToLocale("+ randomParameter +") query results contains valid data on row "+ i +" => id: "+
          row.id +" | disMacro: "+ row.disMacro +" | floorRef: "+ row.floorRef +" | mod: "+ row.mod +" | navName: "+
          row.navName +" | siteRef: "+ row.siteRef);
        }
      } else if(randomParameter === "blocks()") {
          for(let i = 0; i < testData.length; i++) {
            let row= testData[i];
            ok(row.block === "✓" 
            && row.blockStatus !== undefined 
            && row.color.indexOf('#') === 0 
            && row.dis !== undefined 
            && row.doc !== undefined 
            && row.ext !== undefined 
            && row.name !== undefined 
            && row.props !== undefined, 
            "colsToLocale("+ randomParameter +") query results contains valid data on row "+
            i +" => block: "+ row.block +" | blockStatus: "+ row.blockStatus +" | color: "+ row.color +" | dis: "+
            row.dis +" | doc: "+ row.doc +" | ext: "+ row.ext +" | name: "+ row.name +" | props: "+ row.props);
          }
      } else if(randomParameter === "tasks()") {
          for(let i = 0; i < testData.length; i++) {
            let row= testData[i];
            ok(row.dis !== undefined 
            && row.numRoutines >= 0 
            && row.numRoutinesErrs >= 0 
            && row.numVarErrs >= 0 
            && row.numVars >= 0 
            && row.programRef !== undefined 
            && row.targetRef !== undefined 
            && row.task === "✓" 
            && row.taskStatus !== undefined, 
            "colsToLocale("+ randomParameter +") query results contains valid data on row "+
            i +" => dis: "+ row.dis +" | numRoutines: "+ row.numRoutines +" | numRoutinesErrs: "+
            row.numRoutinesErrs +" | numVarErrs: "+ row.numVarErrs +" | numVars: "+ row.numVars +" | programRef: "+
            row.programRef +" | targetRef: "+ row.targetRef +" | task: "+ row.task +" | taskStatus: "+ row.taskStatus);
          }
        }
    });
  });  
});