import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

//the random parameter that will be passed to the axon faction
let randomParameter = createRandomNumber();

let po = new axonPO({"testQuery": "isEven("+ randomParameter +")", "confirmQuery":""});
po.setup(function(instance) {});

describe('The isEven() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isEven("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isEven("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter % 2 !== 0) {
        ok(testData[0].val === false, "isEven("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }

      else if(randomParameter % 2 === 0) { 
        ok(testData[0].val === true, "isEven("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });  
});