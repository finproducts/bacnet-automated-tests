import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//date time
let dt = 'now()';

//timezone
let tz = 'Chicago';

//argument passed to the axon function
let parameter = dt +","+ JSON.stringify(tz);

let po = new axonPO({"testQuery": "toTimeZone("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The toTimeZone() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "toTimeZone("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "toTimeZone("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined, "toTimeZone("+ parameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});