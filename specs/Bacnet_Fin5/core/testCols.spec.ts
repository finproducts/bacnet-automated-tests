import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//array of expected columns
let expectedArray = ["id [foliod::FolioCol]", "ahu [foliod::FolioCol]", "chillerRef [foliod::FolioCol]", "disMacro [foliod::FolioCol]", "dxCool [foliod::FolioCol]",
                    "equip [foliod::FolioCol]", "floorRef [foliod::FolioCol]", "gasHeat [foliod::FolioCol]", "hvac [foliod::FolioCol]", "miscRef [foliod::FolioCol]",
                    "navName [foliod::FolioCol]", "rooftop [foliod::FolioCol]", "siteRef [foliod::FolioCol]", "tz [foliod::FolioCol]", "vavZone [foliod::FolioCol]", "mod [foliod::FolioCol]"];

//array of eligible arguments
let parameterArray = ["readAll(ahu)"];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex];

let po = new axonPO({"testQuery": "cols("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The cols() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "cols("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "cols("+ randomParameter +") returned and object which doesn't contain an error.");

      let validArray = [];

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.val.includes('[foliod::FolioCol]') === true,
        "cols("+ randomParameter +") query results contains valid data on row "+ i +" => val: "+ row.val);

        if(row.val === "id [foliod::FolioCol]" 
        || row.val === "ahu [foliod::FolioCol]" 
        || row.val === "chillerRef [foliod::FolioCol]" 
        || row.val === "disMacro [foliod::FolioCol]" 
        || row.val === "dxCool [foliod::FolioCol]" 
        || row.val === "equip [foliod::FolioCol]" 
        || row.val === "floorRef [foliod::FolioCol]" 
        || row.val === "gasHeat [foliod::FolioCol]" 
        || row.val === "hvac [foliod::FolioCol]" 
        || row.val === "miscRef [foliod::FolioCol]" 
        || row.val === "navName [foliod::FolioCol]" 
        || row.val === "rooftop [foliod::FolioCol]" 
        || row.val === "siteRef [foliod::FolioCol]" 
        || row.val === "tz [foliod::FolioCol]" 
        || row.val === "vavZone [foliod::FolioCol]" 
        || row.val === "mod [foliod::FolioCol]") {
      
          validArray.push(row.val);
        }    
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), 
      "Query result doesn't contain the expected elements: "+ JSON.stringify(validArray));
    });
  });  
});