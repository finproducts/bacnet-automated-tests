import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//string value
let string = 'abcde';

//separator
let separator = 'c';

//argument passed to the axon function
let parameter = JSON.stringify(string) +","+ JSON.stringify(separator);

let po = new axonPO({"testQuery": "split("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The split() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "split("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "split("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(testData[0].val === "ab" 
        && testData[1].val === "de", 
        "split("+ parameter +") query results contain invalid data on row "+ i +" => val: "+ row.val);
      }
    });
  });  
});