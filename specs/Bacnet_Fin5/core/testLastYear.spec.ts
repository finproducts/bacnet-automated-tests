import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//get current year
let today = new Date();
let yyyy = today.getFullYear();

//the result format of the axon function lastYear() 
let lastYearAxonFormat = (yyyy-1) + "-01-01,year [proj::DateSpan]";

let po = new axonPO({"testQuery": "lastYear()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The lastYear() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "lastYear() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "lastYear() returned an object which doesn't contain an error.");

      ok(testData[0].val === lastYearAxonFormat, "lastYear() query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});