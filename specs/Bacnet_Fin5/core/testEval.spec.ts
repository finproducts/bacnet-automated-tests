import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//eval expression
let evalExpr= '-6 > -1';

//the random parameter that will be passed to the axon faction
let randomParameter= JSON.stringify(evalExpr);

let po = new axonPO({"testQuery": "eval("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The eval() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "eval("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "eval("+ randomParameter +") returned an object which doesn't contains an error.");

      ok(testData[0].val === false, 
      "eval("+ randomParameter +") query results contains valid data: "+ testData[0].val);
    });
  });  
});