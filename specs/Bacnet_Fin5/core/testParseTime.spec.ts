import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



let time:any = {"min":"0","maxHour":"24","maxMinAndSec":"59"};

//generate random hour
let getHour = Math.floor(Math.random() * (time.maxHour - time.min) + time.min);

//generate random minutes & seconds;
let getMinAndSec = Math.floor(Math.random() * (time.maxMinAndSec - time.min) + time.min);

//random time generation
let randomTime = ((getHour < 10 ? "0" : "") + getHour) + ":" + ((getMinAndSec < 10 ? "0" : "") +
 getMinAndSec) + ":" + ((getMinAndSec < 10 ? "0" : "") + getMinAndSec);

//the random parameter that will be passed to the axon faction
let randomParameter = '"' + randomTime + '"';

let po = new axonPO({"testQuery": "parseTime("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The parseTime() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "parseTime("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "parseTime("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === randomTime, "parseTime("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
    });
  });  
});