import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//argument passed to the axon faction 
let parameter= 'readAll(point and unit=="%"),"curVal",avg';

let po = new axonPO({"testQuery": "foldCol("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The foldCol',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "foldCol("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "foldCol("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined 
      && typeof testData[0].val === "number", 
      "foldCol("+ parameter +") query results contains invalid data => val: "+ testData[0].val);
    });
  });  
});