import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//function that randomly creates a number
function createRandomNumber() {

	var min = -50;
	var max = 50;
	var getRandomNumber = Math.floor((Math.random() * (max - min) + min) * 100)/100;
	return getRandomNumber;	
};

let randomNumberX1 = createRandomNumber();
let randomNumberX2 = createRandomNumber();
let randomNumberY1 = createRandomNumber();
let randomNumberY2 = createRandomNumber();

//argument passed to the axon function
let randomParameter= "coord("+ randomNumberX1 + "," + randomNumberY1 +"), coord("+ randomNumberX2 + "," + randomNumberY2 +")";

let po = new axonPO({"testQuery": "coordDist("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The coordDist() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "coordDist("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "coordDist("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val <= 0 
      || testData[0].val >= 0, "coordDist("+ randomParameter +") query results contains valid data: "+ testData[0].val);
    });
  });
});