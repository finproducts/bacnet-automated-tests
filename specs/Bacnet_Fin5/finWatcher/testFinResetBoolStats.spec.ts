import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


// not ok on Fin5 but ok on Fin4.5

//random selection
let paramArray= ["{runStarts}", "{runTime}", ""];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let resetStat= paramArray[randomIndex];

//argument passed to the axon function
let parameter= resetStat;

//validating
let validation= 'readAll(point and his and runTime and runStarts and kind=="Bool")';



let po = new axonPO({"testQuery":"finResetBoolStats("  +  parameter  +  ")","confirmQuery":validation});
po.setup(function(instance)
{}
)

describe("The finResetBoolStats() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finResetBoolStats("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finResetBoolStats("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finResetBoolStats("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));

let i;

for(i  =  0; i  <  testData.lenght; i++)
{
let row= confirmData[i]

if( resetStat  ===  "{runStarts}") 
      {
ok(row.runStarts  ===  0  &&  
  row.runTime  !==  0  &&  
  row.dis  !==  undefined,validation  +  
  
  " query contains valid data on row "  +  i + " => dis: " + row.dis +" | runTime: " + row.runTime  + " | runStarts:" +  row.runStarts);
}

    else 
        if( resetStat  ===  "{runTime}") 
      {
ok(row.runStarts  !==  0  &&  row.runTime  ===  0  &&  row.dis  !==  undefined,validation  +   
  " query contains valid data on row "  +  i +  " => dis: "  +  row.dis + " | runTime: "+row.runTime + " | runStarts:" +  row.runStarts);
}

    else 
        if( resetStat  ===  "") 
      {
ok(row.runStarts  ===  0  &&    row.runTime  ===  0  &&    row.dis  !==  undefined,validation  +  
  
  " query contains valid data on row " + i + " => dis: " + row.dis +  " | runTime: " + row.runTime  +  " | runStarts: "  +  row.runStarts);
}

    
      
      
}

}
);
  });  

});