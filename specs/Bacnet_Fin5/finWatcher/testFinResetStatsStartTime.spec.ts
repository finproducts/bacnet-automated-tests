import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//validation: points must have "statsResetTs" property tag after running finResetStatsStartTime()
let validation= 'readAll(point and his and runTime and runStarts and kind=="Bool")';



let po = new axonPO({"testQuery":"finResetStatsStartTime()","confirmQuery":validation});
po.setup(function(instance)
{}
)

describe("The finResetStatsStartTime() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finResetStatsStartTime() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finResetStatsStartTime() returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finResetStatsStartTime() query results contains valid data: "  +  JSON.stringify(testData));

let i;

for(i  =  0; i  <  testData.lenght; i++)
{
let row= confirmData[i]

ok(row.statsResetTs  !==  undefined,
  
  "After running finResetStatsStartTime() "  +  validation  +  " query results contains valid data on row "  +  i  
  +  " => dis: "  +  row.dis  
  +  " | runTime: "  +  row.runTime  
  +  " | runStarts: "  +  row.runStarts  
  +  " | statsResetTs: "  +  row.statsResetTs);
}

}
);
  });  

});