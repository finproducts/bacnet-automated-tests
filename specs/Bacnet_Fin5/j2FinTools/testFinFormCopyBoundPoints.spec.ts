import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let pointId= 'read(point and siteRef->dis=="City Center")->id';

let parameter= pointId  +  ',[{kind: "Number", dis: "City Center Vav-01 AirFlow", sensor, discharge, navName: "AirFlow",'
+' flow, tz: "Los_Angeles", air, mod: dateTime(2016-07-07, 16:35:40.988, "UTC"), curStatus: "ok",'
+' siteRef: @1eeaef2d-fd0e9c29, precision: 0, curVal: 83.04057158287007cfm, his, treePath: `equip:/City Center/Floor 1/Vav-01/AirFlow`,'
+' level: 0, equipRef: @1eeaff69-30d84ee5, indent: 0, floorRef: @1eeaf23d-b2ddf0f3, point, hisFunc: "hisAirFlow", unit: "cfm", minVal: 10,'
+' id: @1eeaff9e-a14e5d23, disMacro: "\$equipRef \$navName", cur, maxVal: 84}]';



let po = new axonPO({"testQuery":"finFormCopyBoundPoints("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormCopyBoundPoints() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormCopyBoundPoints("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormCopyBoundPoints("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finFormCopyBoundPoints("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | helpDoc: "  +  
  testData[0].helpDoc  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});