import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 10;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

let paramArray= ["equip"];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let randomParameter= paramArray[randomIndex];

let parameter= "readAll("  +  randomParameter  +  ")["  +  index  +  "]->id";



let po = new axonPO({"testQuery":"finFindRefs("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFindRefs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFindRefs("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFindRefs("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  !==  undefined,"finFindRefs("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val);
}
);
  });  

});