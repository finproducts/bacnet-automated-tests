import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//number generator
function createRandomNumber() {

	var min = 0;
	var max = 60;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let interval1= createRandomNumber();
let interval2= createRandomNumber();
let index= createRandomNumber();

//fold function
let paramArray= ["avg", "sum", "max"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let foldFn= paramArray[randomIndex];

//point rec
let pointRec= "readAll(point)["  +  index  +  "]";

//baReadHis should be used or not
let baHisBool= false;

//time zone
let timeZone= "Los_Angeles";

//current time span
let currentTimeSpan= "today()";

//baseline time span
let baseLineTimeSpan= "today()";

//argument passed to the axon function
let parameter= interval1  +  "sec,"  +  foldFn  +  ","  +  interval2  +  "sec,"  +  foldFn  +  ","  +  pointRec  +  ","  +  baHisBool  +  ","  +  JSON.stringify(timeZone)  +  ","  +  currentTimeSpan  +  ","  +  baseLineTimeSpan;



let po = new axonPO({"testQuery":"finHisReadBaseGetHis("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHisReadBaseGetHis() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHisReadBaseGetHis("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHisReadBaseGetHis("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.val.includes("HisGrid")  ===  true,
"finHisReadBaseGetHis("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => val: "  +  row.val);
}

}
);
  });  

});