import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//point record at index 10 => JB Tower Vav-01 Room Temp
let pointRec= 'readAll(point and kind=="Number")[10]';

//argument passed to the axon function
let parameter= pointRec;



let po = new axonPO({"testQuery":"finHardDelete("  +  parameter  +  ")","confirmQuery":pointRec});
po.setup(function(instance)
{}
)

describe("The finHardDelete() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHardDelete("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHardDelete("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finHardDelete("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].dis  !==  "JB Tower Vav-01 Room Temp",pointRec  +  " query returned a different point record. Point deleted successfully.");
}
);
  });  

});