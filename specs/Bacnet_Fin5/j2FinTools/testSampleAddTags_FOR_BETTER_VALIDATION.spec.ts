import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1nd param
let paramArray= ["chilled", "water", "cool", "air"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let colName= paramArray[randomIndex];

//2nd param
let filter= 'readAll(point)';

//argument passed to the axon function
let parameter= JSON.stringify(colName)  +  ", "  +  JSON.stringify(filter);



let po = new axonPO({"testQuery":"sampleAddTags("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The sampleAddTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "sampleAddTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"sampleAddTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"sampleAddTags("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});