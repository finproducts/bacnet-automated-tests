import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//filter arguments
let paramArray= ["point", "cmd and cool and cur and curTracksWrite and his and point and water and writable"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let filter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "[{flt:"  +  JSON.stringify(filter)  +  ", tableR: [{cnumUnit: 1, anumResults: 6, bnumKind: 1}]}]";


let po = new axonPO({"testQuery":"finGlobalControlFilterCheck("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGlobalControlFilterCheck() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlFilterCheck("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlFilterCheck("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( filter  ===  "cmd and cool and cur and curTracksWrite and his and point and water and writable") 
      {
ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finGlobalControlFilterCheck("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | helpDoc: "  +  
  testData[0].helpDoc  +  " | name: "  +  
  testData[0].name);
}

    else 
        if( filter  ===  "point") 
      {
ok(testData[0].body.includes('Please make sure all points are of the same kind')  ===  true  &&  
testData[0].cancelButton  !==  undefined  &&  
testData[0].dis.includes('Error')  ===  true  &&  
testData[0].finForm  ===  "✓"  &&  
testData[0].name  !==  undefined,

"finGlobalControlFilterCheck("  +  parameter  +  ") query result contains valid data => body: "  +  
testData[0].body  +  " | cancelButton: "  +  
testData[0].cancelButton  +  " | dis: "  +  
testData[0].dis  +  " | finForm: "  +  
testData[0].finForm  +  " | name: "  +  
testData[0].name);
}

    
      
}
);
  });  

});