import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1st param
let paramArray= ["point"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let parentFilter= paramArray[randomIndex];

//2nd param
let param2Array= ["air", "temp", "zone", "sensor"];
let randomIndex2= Math.floor(Math.random()  *  param2Array.length);
let childFilter= param2Array[randomIndex2];

//argument passed to the axon function
let parameter= JSON.stringify(parentFilter)  +  ","  +  JSON.stringify(childFilter);



let po = new axonPO({"testQuery":"finChildNumberData("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finChildNumberData() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finChildNumberData ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finChildNumberData ("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.avg  !==  undefined  &&  
  row.max  !==  undefined  &&  
  row.min  !==  undefined  &&  
  row.sum  !==  undefined,
  
  "finChildNumberData ("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | avg: "  +  
  row.avg  +  " | max: "  +  
  row.max  +  " | min: "  +  
  row.min  +  " | sum: "  +  
  row.sum);
}

}
);
  });  

});