import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK on Fin5 but OK in Fin4.5

//string generator
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let newTag= createRandomString()  +  "Tag";

//possible navNames
let paramArray= ["CHWV", "SAT", "MAT"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let navName= paramArray[randomIndex];

//column name
let colName= "navName";

//argument passed to the axon function
let parameter= "[{navName: "  +  JSON.stringify(navName)  +  ", addTags: "  +  JSON.stringify(newTag)  +  "}],"  +  JSON.stringify(colName);



let po = new axonPO({"testQuery":
"finAddTagsByNameApply("  +  parameter  +  ")","confirmQuery":"readAll(point and navName=="  +  JSON.stringify(navName)  +  ")"});
po.setup(function(instance)
{}
)

describe("The finAddTagsByNameApply() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finAddTagsByNameApply("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finAddTagsByNameApply("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  ===  null,
  "finAddTagsByNameApply("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

let i;

for(i  =  0; i  <  confirmData.length; i++)
{
let row= confirmData[i]

ok(row[newTag]  ===  "✓",
"readAll(point and navName=="+ JSON.stringify(navName) + ")contains valid data on row "+ i +": new tag " + JSON.stringify(newTag) + " is present.");
}

}
);
  });  

});