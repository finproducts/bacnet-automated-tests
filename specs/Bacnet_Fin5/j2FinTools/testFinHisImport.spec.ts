import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//Make sure there is a History.csv file with point history in FIN stack io folder!

//record => City Center Vav-20 AirFlow Setpoint
let record= 'read(point and writable and siteRef->dis=="City Center")';

let filenamePath= '`io/History.csv`';

let fn= "rewrite";

let historyDict= '{ts:"time", val:"CHWV"}';

let parameter= record  +  ","  +  filenamePath  +  ","  +  JSON.stringify(fn)  +  ","  +  historyDict;



let po = new axonPO({"testQuery":"readById(" + record + "->id).diff({-hisFunc, hisCollectCov: 1, hisCollectInterval: 15sec}).commit","confirmQuery":"finHisImport("  +  parameter  +  ")"});
po.setup(function(instance)
{}
)

describe("The finHisImport() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(confirmData  !==  undefined  &&  confirmData  !==  null,
  "finHisImport("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"finHisImport("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].dis  !==  undefined  &&  
  confirmData[0].numConverted  !==  undefined,
  
  "finHisImport("  +  parameter  +  ") query result contains valid data => id: "  +  
  confirmData[0].id  +  " | dis: "  +  
  confirmData[0].dis  +  " | numConverted: "  +  
  confirmData[0].numConverted);
}
);
  });  

});