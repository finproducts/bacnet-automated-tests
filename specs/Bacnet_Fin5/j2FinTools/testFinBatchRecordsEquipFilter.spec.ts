import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1st param
let paramArray= ["point", "equip"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let type= paramArray[randomIndex];

//2n param
let equipFilter= "";

//argument passed to the axon function
let parameter= JSON.stringify(type)  +  ","  +  JSON.stringify(equipFilter);



let po = new axonPO({"testQuery":"finBatchRecordsEquipFilter("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finBatchRecordsEquipFilter() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchRecordsEquipFilter ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchRecordsEquipFilter ("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.navName  !==  undefined  &&  
  row.mod  !==  undefined,
  
  "finBatchRecordsEquipFilter ("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | navName: "  +  
  row.navName  +  " | mod: "  +  
  row.mod);
}

}
);
  });  

});