import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 2;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

let queryName= "seleniumTest";

let parameter= '[readAll(equip and siteRef->dis=="City Center")['  +  index  +  ']->id], '  +  JSON.stringify(queryName);

let validation= queryName  +  "Query";



let po = new axonPO({"testQuery":"finCreateFloorPlanQuery("  +  parameter  +  ")","confirmQuery":validation});
po.setup(function(instance)
{}
)

describe("The finCreateFloorPlanQuery() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCreateFloorPlanQuery("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCreateFloorPlanQuery("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCreateFloorPlanQuery("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].dis  !==  undefined  &&  
  confirmData[0].seleniumTestOrder  !==  undefined  &&  
  confirmData[0].seleniumTestQuery  !==  undefined,
  
  "finCreateFloorPlanQuery("  +  parameter  +  ") has been successfully applied => id: "  +  
  confirmData[0].id  +  " | dis: "  +  
  confirmData[0].dis  +  " | seleniumTestOrder: "  +  
  confirmData[0].seleniumTestOrder  +  " | seleniumTestQuery: "  +  
  confirmData[0].seleniumTestQuery);
}
);
  });  

});