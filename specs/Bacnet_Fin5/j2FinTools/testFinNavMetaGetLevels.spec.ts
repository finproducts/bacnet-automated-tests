import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//OK


let po = new axonPO({"testQuery":"finNavMetaGetLevels()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavMetaGetLevels() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavMetaGetLevels() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavMetaGetLevels() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.name  !==  undefined  &&   testData[testData.length - 1].name  ===  "point",
"finNavMetaGetLevels() query result contains valid data on row "  +  i  +  " => name: "  +  row.name);
}

}
);
  });  

});