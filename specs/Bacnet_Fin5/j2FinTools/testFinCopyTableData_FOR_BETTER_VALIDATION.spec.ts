import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//string generator
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

function createRandomNumber() {

	var min = 0;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber= createRandomNumber();

let parameter= "[{column1:"  +  JSON.stringify(randomString)  +  ", column2:"  +  randomNumber  +  "}]";



let po = new axonPO({"testQuery":"finCopyTableData("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finCopyTableData() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCopyTableData("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCopyTableData("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCopyTableData("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});