import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible point kind
let paramArray= ['\\"Bool\\"', '\\"Number\\"'];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let filter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= '"point and kind=='  +  filter  +  '"';



let po = new axonPO({"testQuery":"finGlobalControlFilterTable("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGlobalControlFilterTable() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlFilterTable("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlFilterTable("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( filter  ===  "\"Bool\"") 
      {
ok(testData[0].anumResults  !==  undefined  &&  
  testData[0].bnumKind  !==  undefined  &&  
  testData[0].cnumUnit  !==  undefined,
  
  "finGlobalControlFilterTable("  +  parameter  +  ") query result contains valid data => anumResults: "  +  
  testData[0].anumResults  +  " | bnumKind: "  +  testData[0].bnumKind  +  " | cnumUnit: "  +  testData[0].cnumUnit);
}

    else 
        if( filter  ===  "\"Number\"") 
      {
ok(testData[0].errorMessage.includes('Make sure all points')  ===  true,
"finGlobalControlFilterTable("  +  parameter  +  ") query result contains valid data => errorMessage: "  +  testData[0].errorMessage);
}
      
}
);
  });  

});