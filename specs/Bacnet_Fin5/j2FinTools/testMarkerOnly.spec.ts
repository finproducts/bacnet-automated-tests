import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["point", "curVal", "filter"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll("  +  randomParameter  +  ")[0]";



let po = new axonPO({"testQuery":"markerOnly("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The markerOnly() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "markerOnly("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"markerOnly("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  ===  true,
  "markerOnly("  +  parameter  +  ") query result contains valid data => val: "  +  
  testData[0].val  +  ""  +  "The kind of the parameter is not a Marker.");
}
);
  });  

});