import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//WORKS ONLY WITH DAYS !

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 30;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let numDays= createRandomNumber();

let parameter= "now(), now() - "  +  numDays  +  "day";



let po = new axonPO({"testQuery":"finGetMinutesFromCur("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGetMinutesFromCur() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGetMinutesFromCur("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGetMinutesFromCur("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  !==  undefined,
  "finGetMinutesFromCur("  +  parameter  +  ") query result contains valid data => val: "  +  
  testData[0].val  +  " milisecs ("  +  (testData[0].val / 60000)  +  " minutes)");
}
);
  });  

});