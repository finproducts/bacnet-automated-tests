import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 50;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//argument passed to the axon function
let parameter= "readAll(point)["  +  index  +  "]";



let po = new axonPO({"testQuery":"finHisReadBaseGetTz("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHisReadBaseGetTz() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHisReadBaseGetTz("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  
testData.type  !==  "error",
"finHisReadBaseGetTz("  +  parameter  +  ") returned an object which doesn\'t contain an error.");

ok(testData[0].val  !==  undefined,
  "finHisReadBaseGetTz("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val);
}
);
  });  

});