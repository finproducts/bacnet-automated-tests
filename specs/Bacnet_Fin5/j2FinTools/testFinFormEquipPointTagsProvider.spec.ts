import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1st param
let pointsInEquip= 'readAll(point and equipRef==read(equip)->id).toRecIdList';

//2nd param
let paramArray= ["null"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let hideTagFilter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= pointsInEquip  +  ", "  +  hideTagFilter;



let po = new axonPO({"testQuery":"finFormEquipPointTagsProvider("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormEquipPointTagsProvider() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormEquipPointTagsProvider("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormEquipPointTagsProvider("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.count  !==  undefined  &&  
  row.kind  !==  undefined  &&  
  row.name  !==  undefined,
  
  "finFormEquipPointTagsProvider("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => count: "  +  
  row.count  +  " | kind: "  +  
  row.kind  +  " | name: "  +  
  row.name);
}

}
);
  });  

});