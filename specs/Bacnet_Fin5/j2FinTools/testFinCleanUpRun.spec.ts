import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["true", "false"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let form= paramArray[randomIndex];

//argument passed to the axon function
let randomParameter= form;



let po = new axonPO({"testQuery":"finCleanUpRun("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finCleanUpRun() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCleanUpRun("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCleanUpRun("  +  randomParameter  +  ") returned an object which doesn't contains an error.");

if( form  ===  "true") 
      {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.name  !==  undefined,
  "finCleanUpRun("  +  randomParameter  +  ") contains valid data on row "  +  i  +  " => name: "  +  row.name);
}

}

    else 
        if( form  ===  "false") 
      {
ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCleanUpRun("  +  randomParameter  +  ") query results contains valid data => val: "  +  JSON.stringify(testData));
}

    
      
}
);
  });  

});