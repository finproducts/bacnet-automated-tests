import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 3;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//array of possible arguments
let paramArray= ["site", "equip"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll("  +  randomParameter  +  ")["  +  index  +  "]->id";



let po = new axonPO({"testQuery":"breadcrumbs("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The breadcrumbs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "breadcrumbs("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"breadcrumbs("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  row.displayName  !==  undefined,
  "breadcrumbs("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | displayName: "  +  
  row.displayName  +  " | rightIconDirection: "  +  
  row.rightIconDirection);
}

}
);
  });  

});