import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//point id => City Center AHU-1 RAD
let filter= 'point and navName=="RAD" and equipRef->navName==\"AHU-1\" and siteRef->dis==\"City Center\"';

//write level
function createRandomNumber() {

	var min = 1;
	var max = 17;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let writeLevel= createRandomNumber();

let parameter= JSON.stringify(filter)  +  ",["  +  writeLevel  +  "]";



let po = new axonPO({"testQuery":"finCommandAuto("  +  parameter  +  ")","confirmQuery":"pointWriteArray(read("  +  filter  +  ")->id)"});
po.setup(function(instance)
{}
)

describe("The finCommandAuto() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCommandAuto("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCommandAuto("  +  parameter  +  ") returned an object which doesn't contains an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCommandAuto("  +  parameter  +  ") query results contains valid data => val: "  +  JSON.stringify(testData));

ok(confirmData[writeLevel-1].who  ===  "finCommandAuto",
"pointWriteArray(read("  +  filter  +  ")->id) query contains valid data => level: "  +  
confirmData[writeLevel-1].level  +  " | who: "  +  
confirmData[writeLevel-1].who  +  ""  +  "Command auto has been successfully written at level: "  +  writeLevel);
}
);
  });  

});