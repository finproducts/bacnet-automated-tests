import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//folioSnapshots()[index]->ts.toStr after folioSnapshots()
//IMPORTANT - Have a snapshot made before the modification to run it as an argument for the function folioRestore(tsBeforeTheChange) to unde the changes and keep the test clean
let getSnapshotTimestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 

let timestamp= getSnapshotTimestamp;

function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let newDis= createRandomString();

let newEquipFilter= createRandomString();

let newSummaryOn= createRandomString();

let parameter= 'read(summaryOn)->id,"'+ newDis +'", "'+ newSummaryOn +'", "'+ newEquipFilter 
+'", ["AirFlow", "AirFlow Setpoint", "Damper", "Occ Mode", "Room Setpoint", "Room Temp"]';



let po = new axonPO({"testQuery":"finEditSummaryCommit("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finEditSummaryCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finEditSummaryCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finEditSummaryCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].id  !==  undefined  &&  
  testData[0].dis  ===  newDis  &&  
  testData[0].equipFilter  ===  newEquipFilter  &&  
  testData[0].mod  !==  undefined  &&  
  testData[0].pointFilter  !==  undefined  &&  
  testData[0].summaryOn  ===  newSummaryOn,
  
  "finEditSummaryCommit("  +  parameter  +  ") query result contains valid data => id: "  + 
   testData[0].id  +  " | dis: "  +  
   testData[0].dis  +  " | equipFilter: "  +  
   testData[0].equipFilter  +  " | mod: "  +  
   testData[0].mod  +  " | pointFilter: "  +  
   testData[0].pointFilter  +  " | summaryOn: "  +  
   testData[0].summaryOn  +  ""  +  
   
   "Changes were made successfully. New dis is "  +  JSON.stringify(newDis)  +  ", new equip filter is "  +  JSON.stringify(newEquipFilter)  +  " and new summaryOn is "  +  JSON.stringify(newSummaryOn)  +  ".");

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  timestamp  +  ")... Restored.");
}
);
  });  

});