import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

/*
 * Must have "ReadSim" connector in connectors panel
 */

 //function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 5;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

let paramArray= ["ReadSim"];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let connName= paramArray[randomIndex];

let parameter= "[readAll(point and siteRef ->dis =="  +  JSON.stringify(connName)  +  ")["  +  index  +  "]->id]";



let po = new axonPO({"testQuery":"finExistingConnectors("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finExistingConnectors() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finExistingConnectors("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finExistingConnectors("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( connName  ===  "Aquarium") 
      {
ok(testData[0].id  !==  undefined  &&  testData[0].dis  ===  "Aquarium",
"finExistingConnectors("  +  parameter  +  ") query result contains valid data => id: "  +  testData[0].id  +  " | dis: "  +  testData[0].dis);
}

    else 
        if( connName  ===  "ReadSim") 
      {
ok(testData[0].id  !==  undefined  &&  testData[0].dis  ===  "ReadSim",
"finExistingConnectors("  +  parameter  +  ") query result contains valid data => id: "  +  testData[0].id  +  " | dis: "  +  testData[0].dis);
}

    
      
}
);
  });  

});