import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//use folioSnapshot to undo the changes
let folioRestoreParam= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 

//filter arguments
let paramArray= ["Emergency Set", "Emergency Auto", "Manual Set", "Manual Auto", "Set Default", "Set Null"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let action= paramArray[randomIndex];

//number generator
function createRandomNumber() {

	var min = 0;
	var max = 15;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

//point index
let index= createRandomNumber();

//argument passed to the axon function
let parameter= '[readAll(point and siteRef->dis=="City Center")['+ index +']->id], {name:'+ JSON.stringify(action) +', dur: false, value: false}, 10, "%", "Number"';


let po = new axonPO({"testQuery":"finGlobalControlCommit("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  folioRestoreParam  +  ")"});
po.setup(function(instance)
{}
)

describe("The finGlobalControlCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].name  !==  undefined,
  
  "finGlobalControlCommit("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | name: "  +  
  testData[0].name);

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore...Database restored to default.");
}
);
  });  

});