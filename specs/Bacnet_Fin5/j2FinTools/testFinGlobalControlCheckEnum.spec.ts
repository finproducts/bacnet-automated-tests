import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


 //This function is part of "Global Control". It finds the enums available based of the points passed. The enums are then returned for use in a comboBox. 
 //If all points do not have matching enums, then a single option is returned saying not all points have the same enums.

//array of possible arguments
let paramArray= ["0..1", "2..3", "3..4"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let interval= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll(enum)["  +  interval  +  "]";



let po = new axonPO({"testQuery":"finGlobalControlCheckEnum("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGlobalControlCheckEnum() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlCheckEnum("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlCheckEnum("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.val  !==  undefined,
  
  "finGlobalControlCheckEnum("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | val: "  +  row.val);
}

}
);
  });  

});