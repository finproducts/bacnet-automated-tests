import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK


let po = new axonPO({"testQuery":"finTagBlackList()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finTagBlackList() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finTagBlackList() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finTagBlackList() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.regex  !==  undefined,
  "finTagBlackList() query result contains valid data => dis: "  +  row.dis  +  " | regex: "  +  row.regex);
}

}
);
  });  

});