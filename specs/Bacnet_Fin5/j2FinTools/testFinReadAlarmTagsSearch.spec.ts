import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//program
let programRead= 'read(program)';

//argument passed to the axon function
let parameter= programRead  +  "->program";



let po = new axonPO({"testQuery":"finReadAlarmTagsSearch("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finReadAlarmTagsSearch() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finReadAlarmTagsSearch("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finReadAlarmTagsSearch("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.val.includes('tag')  ===  true,
"finReadAlarmTagsSearch("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => val: "  +  row.val);
}

}
);
  });  

});