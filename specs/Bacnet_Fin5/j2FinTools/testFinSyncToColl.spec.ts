import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//collect interval
function createRandomNumber() {

	var min = 15;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let collectInterval= createRandomNumber();

//change on value interval
function cov() {

	var min = 5;
	var max = 30;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let covInterval= cov();

//filter params
let filtParamArray= ["point"];
let randomIndexFilt= Math.floor(Math.random()  *  filtParamArray.length);
let filter= filtParamArray[randomIndexFilt];

//boolean params
let paramArrayBool= ["true"];
let randomIndexBool= Math.floor(Math.random()  *  paramArrayBool.length);
let randomBool= paramArrayBool[randomIndexBool];

//interval units param
let paramArrayUnit= ["sec", "min"];
let randomIndexUnit= Math.floor(Math.random()  *  paramArrayUnit.length);
let intervalUnits= paramArrayUnit[randomIndexUnit];

//case for intervalUnits variable
let transmutation;;

if( intervalUnits  ===  "sec") 
      {
transmutation  =  1
}

    else 
        if( intervalUnits  ===  "min") 
      {
transmutation  =  60
}

    
      ;

let parameter= JSON.stringify(filter)  +  ","  +  randomBool  +  ","  +  collectInterval  +  ","  +  JSON.stringify(intervalUnits)  +  ","  +  randomBool  +  ","  +  covInterval;



let po = new axonPO({"testQuery":"finSyncToColl("  +  parameter  +  ")","confirmQuery":"readAll(point)[0]"});
po.setup(function(instance)
{}
)

describe("The finSyncToColl() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSyncToColl("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSyncToColl("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finSyncToColl("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].hisCollectCov  ===  covInterval  &&  
  confirmData[0].hisCollectInterval  ===  (collectInterval * transmutation * 1000),
  
  "readAll(point)[0] - finSyncToColl("  +  parameter  +  ") has been applied successfully => id: "  +  
  confirmData[0].id  +  " | dis: "  +  
  confirmData[0].dis  +  " | hisCollectCov: "  +  
  confirmData[0].hisCollectCov  +  " | hisCollectInterval: "  +  
  confirmData[0].hisCollectInterval);
}
);
  });  

});