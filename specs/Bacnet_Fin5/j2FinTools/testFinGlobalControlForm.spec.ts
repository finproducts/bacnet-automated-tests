import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//point id
let pointId= 'read(point and writable and siteRef->dis=="City Center")->id';

//filter
let filter= "null";

//argument passed to the axon function
let parameter= "["  +  pointId  +  "],"  +  JSON.stringify(filter);



let po = new axonPO({"testQuery":"finGlobalControlForm("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGlobalControlForm() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlForm("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlForm("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finGlobalControlForm("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | helpDoc: "  +  
  testData[0].helpDoc  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});