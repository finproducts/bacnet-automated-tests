import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//1st param
let parentArray= ["equip"];
let randomIndexParent= Math.floor(Math.random()  *  parentArray.length);
let parentFilter= parentArray[randomIndexParent];

//2nd param
let childArray= ["air", "cool", "fan"];
let randomIndexChild= Math.floor(Math.random()  *  childArray.length);
let childFilter= childArray[randomIndexChild];

//argument passed to the axon function
let parameter= JSON.stringify(parentFilter)  +  ","  +  JSON.stringify(childFilter);



let po = new axonPO({"testQuery":"finChildBooleanData("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finChildBooleanData() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finChildBooleanData("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finChildBooleanData("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.offCount  >=  0  &&  
  row.onCount  >=  0  &&  
  row.totalCount  >=  0,
  
  "finChildBooleanData("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | offCount: "  +  
  row.offCount  +  " | onCount: "  +  
  row.onCount  +  " | totalCount: "  +  
  row.totalCount);
}

}
);
  });  

});