import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//filter 1
let filter1= "equip";

//filter 2
let filter2= "hvac";

//argument passed to the axon function
let parameter= JSON.stringify(filter1)  +  ","  +  JSON.stringify(filter2)  +  ",[]";



let po = new axonPO({"testQuery":"finBatchRecordsFilter("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finBatchRecordsFilter() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchRecordsFilter("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchRecordsFilter("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finBatchRecordsFilter("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});