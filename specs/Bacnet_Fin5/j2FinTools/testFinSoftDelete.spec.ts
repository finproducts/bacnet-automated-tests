import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//point record at index 0 => City Center Vav-16 Room Setpoint
let pointRec= 'readAll(point and kind=="Number")[0]';

//argument passed to the axon function
let parameter= pointRec;



let po = new axonPO({"testQuery":"finSoftDelete("  +  parameter  +  ")","confirmQuery":pointRec});
po.setup(function(instance)
{}
)

describe("The finSoftDelete() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSoftDelete("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSoftDelete("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finSoftDelete("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData)  +  ". The record was sent to the trash.");

ok(confirmData[0].dis  !==  "City Center Vav-16 Room Setpoint",pointRec + " query returned a different point record. Point deleted successfully.");
}
);
  });  

});