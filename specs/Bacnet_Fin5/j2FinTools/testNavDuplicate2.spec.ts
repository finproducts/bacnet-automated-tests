import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1st parameter
let level= "equip";

//2nd parameter - City Center AHU-1 MAD
let pointId= 'read(point and navName=="MAD" and siteRef->dis=="City Center")->id';

//3rd parameter - counter
function createRandomNumber() {

	var min = 1;
	var max = 3;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let getRandomCounter= createRandomNumber();
let counter= "{count:"  +  getRandomCounter  +  "}";

//the random parameter that will be passed to the axon faction
let randomParameter= JSON.stringify(level)  +  ","  +  pointId  +  ","  +  counter;

let folioRestoreParam= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 



let po = new axonPO({"testQuery":"navDuplicate2("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The navDuplicate2() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "navDuplicate2("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"navDuplicate2("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis.includes('MAD')  ===  true  &&  
row.id  !==  undefined,

"navDuplicate2("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
row.id  +  " | dis: "  +  row.dis);
}

}
);
  });  

});