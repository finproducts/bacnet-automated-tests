import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//number param
let number= 100;

//unit
let unit= 'km';

//argument passed to the axon function
let parameter= number  +  unit;



let po = new axonPO({"testQuery":"finRemoveUnits("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finRemoveUnits() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'finRemoveUnits("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'finRemoveUnits("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(testData[0].unit  ===  undefined  &&  testData[0].val  ===  number,"finRemoveUnits("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val);
}
);
  });  

});