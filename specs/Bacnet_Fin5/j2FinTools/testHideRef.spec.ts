import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["point", "curVal", "filter"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll("  +  randomParameter  +  ")[0]";



let po = new axonPO({"testQuery":"hideRef("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The hideRef() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "hideRef("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"hideRef("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  ===  false,
  "hideRef("  +  parameter  +  ") query result contains valid data => val: "  +  
  testData[0].val  +  ""  +  "The kind of the parameter is neither a Ref or a Marker.");
}
);
  });  

});