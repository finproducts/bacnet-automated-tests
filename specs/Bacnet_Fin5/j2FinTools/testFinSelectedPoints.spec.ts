import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 10;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index1= createRandomNumber();
let index2= createRandomNumber();
let index3= createRandomNumber();

//argument passed to the axon function
let parameter= "[readAll(point)["  +  index1  +  "]->id, readAll(point)["  +  index2  +  "]->id, readAll(point)["  +  index3  +  "]->id]";



let po = new axonPO({"testQuery":"finSelectedPoints("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finSelectedPoints() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSelectedPoints("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSelectedPoints("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.cur  !==  undefined  &&  
  row.curStatus  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.curVal  !==  undefined  &&  
  row.equipRef  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.kind  !==  undefined  &&  
  row.navName  !==  undefined  &&  
  row.siteRef  !==  undefined,
  
  "finSelectedPoints("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | cur: "  +  
  row.cur  +  " | curStatus: "  +  
  row.curStatus  +  " | disMacro: "  +  
  row.disMacro  +  " | curVal: "  +  
  row.curval  +  " | equipRef: "  +  
  row.equipRef  +  " | floorRef: "  +  
  row.floorRef  +  " | kind: "  +  
  row.kind  +  " | navName: "  +  
  row.navName  +  " | siteRef: "  +  
  row.siteRef);
}

}
);
  });  

});