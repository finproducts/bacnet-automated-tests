import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//folioSnapshots()[index]->ts.toStr after folioSnapshots()
//IMPORTANT - Have a snapshot made before the modification to run it as an argument for the function folioRestore(tsBeforeTheChange) to unde the changes and keep the test clean
let getSnapshotTimestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 

//argument passed to the axon function
let timestamp= getSnapshotTimestamp;

//function that randomly creates a string
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let newNoteTag= createRandomString();

let parameter= "[read(noteTags)->noteTags], "  +  JSON.stringify(newNoteTag);



let po = new axonPO({"testQuery":"finEditNoteTagsCommit("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finEditNoteTagsCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finEditNoteTagsCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finEditNoteTagsCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].id  !==  undefined  &&  
  testData[0].mod  !==  undefined  &&  
  testData[0].noteTags  ===  ","  +  newNoteTag,
 
  "finEditNoteTagsCommit("  +  parameter  +  ") query result contains valid data => id: "  +  
  testData[0].id  +  " | mod: "  +  testData[0].mod  +  " | noteTags: "  +  testData[0].noteTags);

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  timestamp  +  ")... Restored.");
}
);
  });  

});