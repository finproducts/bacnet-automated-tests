import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["Number", "Bool", "Enum", "Str"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= JSON.stringify(randomParameter);



let po = new axonPO({"testQuery":"finPointActions("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finPointActions() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finPointActions ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finPointActions ("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.expr  !==  undefined  &&  
  row.hvac_finCat  !==  undefined,
  
  "finPointActions ("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | expr: "  +  row.expr  +  " | hvac_finCat: "  +  row.hvac_finCat);
}

}
);
  });  

});