import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//random string creator
function createRandomString() {
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let title= createRandomString();

let message= createRandomString()  +  " "  +  createRandomString()  +  " "  +  createRandomString();

let parameter= JSON.stringify(title)  +  ","  +  JSON.stringify(message);



let po = new axonPO({"testQuery":"finFormAlertOk("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormAlertOk() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormAlertOk("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormAlertOk("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body.includes(message)  ===  true  &&  
testData[0].cancelButton  !==  undefined  &&  
testData[0].dis  ===  title  &&  
testData[0].finForm  ===  "✓"  &&  
testData[0].name  !==  undefined,

"finFormAlertOk("  +  parameter  +  ") query result contains valid data => body: "  +  
testData[0].body  +  " | cancelButton: "  +  
testData[0].cancelButton  +  " | dis: "  +  
testData[0].dis  +  " | finForm: "  +  
testData[0].finForm  +  " | name: "  +  
testData[0].name);
}
);
  });  

});