import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let ahu01CityCenterId= 'read(ahu and navName=="AHU-1" and siteRef->dis=="City Center")->id';

let parameter= ahu01CityCenterId  +  ',[{id: @1a169f42-a1d26b60, dis: "City Center AHU-1",'
+' floorRef: @1a16992d-9cbd4882, centralPlantRef: @1b582753-d543d8a2, children, equip,'
+' vavZone, hvac, treePath: `equip:/City Center/Floor 1/AHU-1`, siteRef: @1a169921-22d9c3b8, ahu,'
+' indent: 0, navName: "AHU-1", icon16: `fan://equipExt/res/img/equipHvac.png`, mod: dateTime(2016-03-15,'
+' 23:11:09.149, "UTC"), level: 0, vavAHU, disMacro: "\$siteRef \$navName"}]';



let po = new axonPO({"testQuery":"finFormFloorPlanQuery("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormFloorPlanQuery() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormFloorPlanQuery("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormFloorPlanQuery("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finFormFloorPlanQuery("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | helpDoc: "  +  
  testData[0].helpDoc  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});