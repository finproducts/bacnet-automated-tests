import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//for BACnet: bacnet://192.168.1.106/6 

//argument passed to the axon function
let parameter= '@1e977ad5-1052846d,[{id: @1e977ad5-1052846d, dis: "BACnet", geoCountry: "US", site, children, treePath: `equip:/BACnet`, tz: "Athens", indent: 0, icon16: `fan://equipExt/res/img/site.png`, mod: dateTime(2016-04-06, 07:44:21.482, "UTC"), level: 0, geoState: "AL"}]';



let po = new axonPO({"testQuery":"finFormUpdateConnectors("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormUpdateConnectors() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormUpdateConnectors("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  
testData.type  !==  "error",
"finFormUpdateConnectors("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finFormUpdateConnectors("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | helpDoc: "  +  
  testData[0].helpDoc  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});