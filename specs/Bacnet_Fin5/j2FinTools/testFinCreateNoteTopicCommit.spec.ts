import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//random string
let name= "SeleniumTopic";

//argument passed to the axon function
let parameter= JSON.stringify(name)  +  ', [], {tags: [], logic: "and", custom: ""}, {tags: [], logic: "and"},null';



let po = new axonPO({"testQuery":"finCreateNoteTopicCommit("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finCreateNoteTopicCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCreateNoteTopicCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCreateNoteTopicCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body.includes(name)  ===  true  &&  
testData[0].cancelButton  !==  undefined  &&  
testData[0].dis  !==  undefined  &&  
testData[0].finForm  ===  "✓"  &&  
testData[0].name  !==  undefined,

"finCreateNoteTopicCommit("  +  parameter  +  ") query result contains valid data => body: "  +  
testData[0].body  +  " | cancelButton: "  +  
testData[0].cancelButton  +  " | dis: "  +  
testData[0].dis  +  " | finForm: "  +  
testData[0].finForm  +  " | name: "  +  
testData[0].name);
}
);
  });  

});