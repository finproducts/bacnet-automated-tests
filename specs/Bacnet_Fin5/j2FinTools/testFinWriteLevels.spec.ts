import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';

//OK


//BACnet as a test subject


//transfer
let transfer= false;

//function that randomly creates a number
function createRandomNumber() {

	var min = 1;
	var max = 16;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let writeLevel= createRandomNumber();

//recs
let paramArray1= ["point"];
let randomIndex1= Math.floor(Math.random()  *  paramArray1.length);
let recs= paramArray1[randomIndex1];

//connection type
let paramArray2= ["bacnet"];
let randomIndex2= Math.floor(Math.random()  *  paramArray2.length);
let connType= paramArray2[randomIndex2];

//argument passed to the axon function
let parameter= JSON.stringify(recs)  +  ","  +  JSON.stringify(connType)  +  ","  +  writeLevel  +  ","  +  transfer;



let po = new axonPO({"testQuery":"finWriteLevels("  +  parameter  + ")","confirmQuery" : 'readAll(point and bacnetCur).toGrid.keepCols(["id",bacnetConnRef","bacnetCur","bacnetWriteLevel"])'});
po.setup(function(instance)
{}
)

describe("The finWriteLevels() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finWriteLevels("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finWriteLevels("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finWriteLevels("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

let i;

for(i  =  0; i  <  confirmData.length; i++)
{
let row= confirmData[i]

ok(row.bacnetWriteLevel  ===  writeLevel,
  "readAll(point and bacnetCur) query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | bacnetConnRef: "  +  
  row.bacnetConnRef  +  " | bacnetCur: "  +  
  row.bacnetCur  +  " | bacnetWriteLevel: "  +  
  row.bacnetWriteLevel  +  ""  +  "Write level has successfully been modified with the new value: "  +  row.bacnetWriteLevel);
}

}
);
  });  

});