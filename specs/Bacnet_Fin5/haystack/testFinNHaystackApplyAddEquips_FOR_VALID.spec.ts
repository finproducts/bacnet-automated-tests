import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//Commit action for Add Haystack Equip


//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//device type
let deviceType= "Equip";

//site name
let siteName= "Aquarium";

//id
let id= "";

//the argument that will be passed to the axon faction
let parameter= haystackConnId  +  ", "  +  JSON.stringify(deviceType)  +  ","  +  JSON.stringify(siteName)  +  ",["  +  id  +  "]";



let po = new axonPO({"testQuery":"finNHaystackApplyAddEquips("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackApplyAddEquips() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackApplyAddEquips("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackApplyAddEquips("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  ===  0,
  "finNHaystackApplyAddEquips("  +  parameter  +  ") query results contains valid data => rowsChanged: "  +  testData[0].rowsChanged);
}
);
  });  

});