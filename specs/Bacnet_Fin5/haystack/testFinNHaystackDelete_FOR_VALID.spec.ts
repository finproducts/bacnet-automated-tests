import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//Commit action for Delete Equip


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//equip delete
let deleteEquip = "@S.Aquarium.AHU_3";

//argument passed to the axon faction
let parameter = "readById("+ haystackConnId +"), ["+ deleteEquip +"]";



let po = new axonPO({"testQuery":"finNHaystackDelete("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackDelete() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackDelete("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackDelete("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  ===  1,
  "finNHaystackDelete("  +  parameter  +  ") query results contains valid data => rowsChanged: "  +  testData[0].rowsChanged);
}
);
  });  

});