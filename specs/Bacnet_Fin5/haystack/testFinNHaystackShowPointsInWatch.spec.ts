import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//get the watch id from the first index
let watchId = 'finNHaystackShowWatches(read(haystackConn and dis=="Aquarium")->id)[0]->id';

//argument passed to the axon faction
let parameter = haystackConnId +", "+ watchId;



let po = new axonPO({"testQuery": "finNHaystackShowPointsInWatch("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackShowPointsInWatch() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackShowPointsInWatch("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackShowPointsInWatch("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id.includes('@S.Aquarium.') === true 
        && row.curStatus === "ok", 
        "finNHaystackShowPointsInWatch("+ parameter +") query results contain invalid data on row "+ i +" => id: "+ row.id 
        +" | curStatus: "+ row.curStatus +" | curVal: "+ row.curVal);
      }
    });
  });
});