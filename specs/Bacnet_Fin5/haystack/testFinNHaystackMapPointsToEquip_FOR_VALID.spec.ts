import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//Component function inside finNHaystackMapPointsToEquipApply()

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//site navName
let site= "Aquarium";

//equip navName (duplicate slot exception; should be changed at each run)
let equip= "AHU_101";

//point id
let pointId= "";

//the argument that will be passed to the axon faction
let parameter= haystackConnId  +  ", "  +  JSON.stringify(site)  +  ","  +  JSON.stringify(equip)  +  ",["  +  pointId  +  "]";


let po = new axonPO({"testQuery":"finNHaystackMapPointsToEquip("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackMapPointsToEquip() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackMapPointsToEquip("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackMapPointsToEquip("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  >=  0,
  "finNHaystackMapPointsToEquip("  +  parameter  +  ") query results contains valid data => rowsChanged: "  +  testData[0].rowsChanged);
}
);
  });  

});