import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//Commit action for View Equip Types

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//add tag to apply
let addTag = 'ahu';

//the argument that will be passed to the axon faction
let parameter = haystackConnId + ", [{tags:"+ JSON.stringify(addTag) +",id: @type000, selected: true}]";

let po = new axonPO({"testQuery": "finNHaystackViewEquipTypesCommit("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackViewEquipTypesCommit() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackViewEquipTypesCommit("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackViewEquipTypesCommit("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
      "finNHaystackViewEquipTypesCommit("+ parameter +") query results contain invalid data: "+ JSON.stringify(testData));
    });
  });
});