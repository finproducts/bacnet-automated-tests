import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//Delete Equip

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//argument passed to the axon function
let parameter = haystackConnId +',[{connState: "open", moduleVersion: "1.2.5.18", productName: "Niagara AX", uri:'
                +' `http://localhost/haystack/`, moduleName: "nhaystack", id: @1f6519d7-01ce80a6, mod: dateTime(2016-09-09, 08:04:11.523, "UTC"),'
                +' tz: "Athens", haystackSlot: false, equipFilter: "", haystackConn, connStatus: "ok", pointFilter: "", username: "admin", productVersion: "3.8.38", dis: "Aquarium"}]';



let po = new axonPO({"testQuery":"finNHaystackFormDelete("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackFormDelete() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackFormDelete("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackFormDelete("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].commitAction.includes('finNHaystackDelete')  ===  true  &&  
testData[0].body  !==  undefined  &&  
testData[0].cancelButton  ===  "$cancelButton"  &&  
testData[0].commitButton  ===  "$applyButton"  &&  
testData[0].finForm  ===  "✓"  &&  
testData[0].name  ===  "create a note",

"finNHaystackFormDelete("  +  parameter  +  ") query result contains valid data => commitAction: "  +  testData[0].commitAction  
+  " | cancelButton: "  +  testData[0].cancelButton  
+  " | commitButton: "  +  testData[0].commitButton  
+  " | finForm: "  +  testData[0].finForm  
+  " | name: "  +  testData[0].name  
+  " | body: "  +  testData[0].body);
}
);
  });  

});