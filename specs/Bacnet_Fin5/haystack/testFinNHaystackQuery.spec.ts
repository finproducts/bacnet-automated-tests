import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//Nhaystack Query

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//argument passed to the axon function
let parameter = haystackConnId + ", null";

let po = new axonPO({"testQuery": "finNHaystackQuery("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finNHaystackQuery()',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackQuery("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "finNHaystackQuery("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].gotoAction.includes('nHaystackQueryApply') === true 
      && testData[0].cancelButton === "$cancelButton" 
      && testData[0].commitButton === "$applyButton" 
      && testData[0].finForm === "✓" 
      && testData[0].name === "finNHaystackQueryForm", 
      "finNHaystackQuery("+ parameter +") query result contain invalid data => got oAction: "+ testData[0].gotoAction 
      +" | cancelButton: "+ testData[0].cancelButton +" | commitButton: "+ testData[0].commitButton 
      +" | finForm: "+ testData[0].finForm +" | name: "+ testData[0].name);
    });
  });  
});