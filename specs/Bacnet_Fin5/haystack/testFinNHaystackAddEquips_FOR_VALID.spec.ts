import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//Component function inside finNHaystackApplyAddEquips()


//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//device type
let deviceType= "equip";

//site name
let siteName= "Aquarium";

//id
let id= "";

//the argument that will be passed to the axon faction
let parameter = haystackConnId +", "+ JSON.stringify(deviceType) +","+ JSON.stringify(siteName) +",["+ id +"]";



let po = new axonPO({"testQuery":"finNHaystackAddEquips(" +  parameter + ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackAddEquips() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
    
{
  
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackAddEquips("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackAddEquips("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged >= 0,
  "finNHaystackAddEquips(" +  parameter + ") query results contains valid data => rowsChanged: " + testData[0].rowsChanged);  
});
  });  

});