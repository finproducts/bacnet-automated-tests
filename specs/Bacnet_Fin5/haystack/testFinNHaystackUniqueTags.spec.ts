import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//tag
let tag = 'equip';

//argument passed to the axon faction
let parameter = haystackConnId +","+ JSON.stringify(tag);

let po = new axonPO({"testQuery": "finNHaystackUniqueTags("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackUniqueTags() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "finNHaystackUniqueTags("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackUniqueTags(" + parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.markers !== undefined 
        && row.count >= 0, "finNHaystackUniqueTags("+ parameter +") query results contain invalid data on row "+ i +" => count: "+
        row.count +" | markers: "+ row.markers);
      }
    });
  });  
});