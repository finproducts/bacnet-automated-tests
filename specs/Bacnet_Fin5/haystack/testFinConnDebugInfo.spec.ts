import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Haystack connector!

//argument passed to the axon function
let parameter= 'read(haystackConn)->id,[{id: read(haystackConn)->id, dis: "Aquarium", username: "admin",'
+' uri: `http://localhost/haystack/`, depth: 0, equipFilter: "", icon24: `http://localhost:85/pod/haystackExt/res/img/icon24.png`,'
+' productVersion: "3.8.38", connStatus: "ok", ext: "haystack", moduleVersion: "1.2.5.18", connTag: "haystackConn", tz: "Athens",'
+' productName: "Niagara AX", pointFilter: "", haystackConn, model: "haystackExt::HaystackModel", mod: dateTime(2016-04-26, 07:50:38.389, "UTC"),'
+' moduleName: "nhaystack", connState: "open", haystackSlot: false, iconTable: `fan://haystackExt/res/img/iconTable.png`}],null';



let po = new axonPO({"estQuery":"finConnDebugInfo("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finConnDebugInfo() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finConnDebugInfo("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finConnDebugInfo("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finConnDebugInfo("  +  parameter  +  ") query result contains valid data => body: "  +  testData[0].body  
  +  " | commitAction: "  +  testData[0].commitAction  
  +  " | commitButton: "  +  testData[0].commitButton  
  +  " | dis: "  +  testData[0].dis  
  +  " | finForm: "  +  testData[0].finForm  
  +  " | helpDoc: "  +  testData[0].helpDoc  
  +  " | name: "  +  testData[0].name);
}
);
  });  

});