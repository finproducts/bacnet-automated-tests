import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//filter
let filter = 'equip';

//the argument that will be passed to the axon faction
let parameter = haystackConnId +", "+ filter;

let po = new axonPO({"testQuery": "haystackRead("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The haystackRead()',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "haystackRead("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "haystackRead("+ parameter +") returned an object which doesn't contain an error.");

      let row = testData[0];

      ok(row.id !== undefined 
      && row.axSlotPath.includes(filter) === true 
      && row.axType === "nhaystack:HEquip" 
      && row.dis !== undefined 
      && row.navName !== undefined 
      && row.equip === "✓", 
      "haystackRead("+ parameter + ") query results contain invalid data => id: "+ row.id +" | axType: "+
      row.axType +" | dis: "+ row.dis +" | navName: "+ row.navName +" | axSlotPath: "+row.axSlotPath +" | equip: "+ row.equip);
    });
  });  
});