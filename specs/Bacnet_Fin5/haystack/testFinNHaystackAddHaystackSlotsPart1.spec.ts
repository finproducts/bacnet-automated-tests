import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Commit action for Add Haystack Slot

//use in conjunction with finNHaystackDeleteHaystackSlot()

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//filter
let targetFilter= "point";

//point navName on which the haystack slot will be added: TestHaystackSlot
let pointName= "SAT";

//the argument that will be passed to the axon faction
let parameter= haystackConnId +","+ JSON.stringify(targetFilter) +",[haystackRead(read(haystackConn and dis==\"Aquarium\")->id, point and navName=="+ JSON.stringify(pointName) +")->id]";



let po = new axonPO({"testQuery":"finNHaystackAddHaystackSlots("  +  parameter  +  ")","confirmQuery":"finNHaystackRebuildCache("  +  haystackConnId  +  ")"});
po.setup(function(instance)
{}
)

describe("The finNHaystackAddHaystackSlotsPart1() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackAddHaystackSlots("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackAddHaystackSlots("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  ===  1,
  "finNHaystackAddHaystackSlots(" +  parameter + ") query results contains valid data => rowsChanged: " + testData[0].rowsChanged);
}
);
  });  

});