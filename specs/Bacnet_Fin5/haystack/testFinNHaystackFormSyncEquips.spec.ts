import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//Nhaystack Sync Equips

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//argument passed to the axon function
let parameter= haystackConnId;



let po = new axonPO({"testQuery":"finNHaystackFormSyncEquips("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackFormSyncEquips() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackFormSyncEquips("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackFormSyncEquips("  +  parameter  +  ") returned an object which doesn't contain an error.");

let row= testData[0]

ok(row.body  !==  undefined  &&  
  row.cancelButton  ===  "$cancelButton"  &&  
  row.commitAction  ===  "finNHaystackSyncNiagaraEquipApply($connFilter,$targetFilter,$updateNavName)"  &&  
  row.commitButton  ===  "$applyButton"  &&  
  row.dis  ===  "Sync Niagara Equips"  &&  
  row.finForm  ===  "✓"  &&  
  row.helpDoc  !==  undefined  &&  
  row.name  ===  "finFormSyncNiagaraEquips"  &&  
  row.refreshTrees  ===  "✓",
  
  "finNHaystackFormSyncEquips("  +  parameter  +  ") query results contains valid data => body: "  +  row.body  
  +  " | cancelButton: "  +  row.cancelButton  
  +  " | commitAction: "  +  row.commitAction  
  +  " | commitButton: "  +  row.commitButton  
  +  " | dis: "  +  row.dis  
  +  " | finForm: "  +  row.finForm  
  +  " | helpDoc: "  +  row.helpDoc  
  +  " | name: "  +  row.name  
  +  " | refreshTrees: "  +  row.refreshTrees);
}
);
  });  

});