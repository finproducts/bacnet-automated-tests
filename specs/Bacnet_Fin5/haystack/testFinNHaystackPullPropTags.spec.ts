import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//Commit action for Pull Property Tags

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//selected tag you want to sync
let pullTag = 'point';

//filter on the selected tags that will be sync
let filterOn = 'point';

//the argument that will be passed to the axon faction
let parameter = haystackConnId + ", ["+ JSON.stringify(pullTag) +"],"+ JSON.stringify(filterOn);

let po = new axonPO({"testQuery": "finNHaystackPullPropTags("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finNHaystackPullPropTags() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackPullPropTags("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackPullPropTags("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined 
        && row.dis.includes('Aquarium') === true 
        && row.point === "✓" 
        && row.mod !== undefined, 
        "finNHaystackPullPropTags("+ parameter +") query results contain invalid data => id: "+ row.id 
        +" | dis: "+ row.dis +" | point: "+ row.point +" | mod: "+ row.mod);
      }
    });
  });  
});