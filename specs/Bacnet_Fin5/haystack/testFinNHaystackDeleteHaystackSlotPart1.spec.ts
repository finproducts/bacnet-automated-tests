import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//Commit action for Delete Haystack Slot

//use in conjunction with finNHaystackAddHaystackSlots()

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//point navName on which the haystack slot will be deleted: TestHaystackSlot
let pointName = "SAT";

//the argument that will be passed to the axon faction
let parameter = haystackConnId +",[haystackRead(read(haystackConn and dis==\"Aquarium\")->id, point and navName=="+ JSON.stringify(pointName) +")->id]";


let po = new axonPO({"testQuery":"finNHaystackDeleteHaystackSlot(" + parameter  + ")","confirmQuery":"finNHaystackRebuildCache(" + haystackConnId  + ")"});
po.setup(function(instance)
{}
)

describe("The finNHaystackDeleteHaystackSlotPart1() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackDeleteHaystackSlot("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackDeleteHaystackSlot("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  ===  1,
  "finNHaystackDeleteHaystackSlot("  +  parameter  +  ") query results contains valid data => rowsChanged: "  +  testData[0].rowsChanged);
}
);
  });  

});