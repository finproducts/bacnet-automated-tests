import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Advanced String Editor

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

let parameter= haystackConnId +',[{productName: "Niagara AX", connTag: "haystackConn", dis: "Aquarium",'
+' uri: `http://localhost/haystack/`, moduleVersion: "1.2.5.18", connErr: "sys::IOErr: java.net.ConnectException:'
+' Connection refused: connect", productVersion: "3.8.38", moduleName: "nhaystack", iconTable:'
+'`fan://haystackExt/res/img/iconTable.png`, depth: 0, tz: "Athens", model: "haystackExt::HaystackModel",'
+' haystackSlot: false, ext: "haystack", equipFilter: "", haystackConn, connStatus: "down", pointFilter: "",'
+' username: "admin", id: @1f6519d7-01ce80a6, mod: dateTime(2016-09-09, 08:04:11.523, "UTC"), icon24:'
+' `http://localhost:85/pod/haystackExt/res/img/icon24.png`, connState: "closed"}]';



let po = new axonPO({"testQuery":"finNHaystackFormAdvancedApplyBatchTags("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackFormAdvancedApplyBatchTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackFormAdvancedApplyBatchTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackFormAdvancedApplyBatchTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
testData[0].commitAction.includes('finNHaystackAdvancedApplyBatchTagsCommit')  ===  true  &&  
testData[0].cancelButton  ===  "$cancelButton"  &&  
testData[0].commitButton  ===  "$applyButton"  &&  
testData[0].finForm  ===  "✓"  &&  
testData[0].dis  !==  undefined  &&  
testData[0].helpDoc  !==  undefined,

"finNHaystackFormAdvancedApplyBatchTags("  +  parameter  +  ") query result contains valid data => commitAction: "  +  testData[0].commitAction  
+  " | cancelButton: "  +  testData[0].cancelButton  
+  " | commitButton: "  +  testData[0].commitButton  
+  " | finForm: "  +  testData[0].finForm  
+  " | name: "  +  testData[0].name  
+  " | body: "  +  testData[0].body  
+  " | helpDoc: "  +  testData[0].helpDoc);
}
);
  });  

});