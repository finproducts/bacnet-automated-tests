import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//Commit action for Haystack Sync To Niagara

//Niagara open
//if the error - There is no extended function called 'applyGridTags' - is returned, copy the most recent nHaystack.jar in modules folder (Niagara)

//haystack conn id: "Aquarium"
var haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//filter
let filter ="point";

//the argument that will be passed to the axon faction
let parameter = haystackConnId +", "+ JSON.stringify(filter);



let po = new axonPO({"testQuery":"finNHaystackApplyFolioFilter("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackApplyFolioFilter() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackApplyFolioFilter("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackApplyFolioFilter("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  ===  0,
  "finNHaystackApplyFolioFilter("  +  parameter  +  ") query results contains valid data => rowsChanged: "  +  testData[0].rowsChanged);
}
);
  });  

});