import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of eligible parameters
let parameterArray= [{q:"30, 0..29", r:false}, {q:"1,0..0", r:false}, {q:"0,0..1000", r:true}, {q:"-1,100..1", r:false}, {q:"-100,-1000..-99", r:true}];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex].q;

//the boolean value of expectedEvalResult variable for each eligible parameters within the array 
let expectedEvalResult= parameterArray[randomIndex].r;


let po = new axonPO({"testQuery":"matchPointVal("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)
describe("The matchPointVal() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "matchPointVal("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"matchPointVal("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

equal(testData[0].val,expectedEvalResult,"matchPointVal("  +  randomParameter  +  ") is evaluated as: "  +  testData[0].val);
}
);
  });  

});