import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//10 char random string
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

//array of possible arguments
let paramArray= [ "site"];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

//argument passed to the axon faction
let randomParameter= "readAll("  +  paramArray[randomIndex]  +  ")";



let po = new axonPO({"testQuery":"toEquips("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The toEquips() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
    "toEquips("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"toEquips("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

if( paramArray[randomIndex]  ===  randomString) 
      {
ok(JSON.stringify(testData)  ===  "[]","toEquips("  +  randomParameter  +  ") query result contains valid data: No equips found.");
}

    else 
        {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
    row.dis  !==  undefined  &&  
    row.disMacro  !==  undefined  &&  
    row.floorRef  !==  undefined  &&  
    row.mod  !==  undefined  &&  
    row.navName  !==  undefined,
    
    "toEquips("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
    row.id  +  " | dis: "  +  
    row.dis  +  " | disMacro: "  +  
    row.disMacro  +  " | floorRef: "  +  
    row.floorRef  +  " | mod: "  +  
    row.mod  +  " | navName: "  +  
    row.navName);
}

}

      
}
);
  });  

});