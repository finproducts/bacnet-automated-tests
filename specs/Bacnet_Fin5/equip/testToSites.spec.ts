import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

let paramArray= [randomString, "equip", "damper"];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let randomParameter= "readAll("  +  paramArray[randomIndex]  +  ")";

if( paramArray[randomIndex]  ===  randomString) 
      {
let randomParameter= "readAll("  +  JSON.stringify(paramArray[randomIndex])  +  ")"
};


let po = new axonPO({"testQuery":"toSites("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The toSites() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
    "toSites("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"toSites("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

if( paramArray[randomIndex]  ===  randomString) 
      {
ok(JSON.stringify(testData)  ===  "[]","toSites("  +  randomParameter  +  ") query result contains valid data: No equips found.");
}

    else 
        {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
    row.dis  !==  undefined  &&  
    row.mod  !==  undefined,
    
    "toSites("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
    row.id  +  " | dis: "  +  
    row.dis  +  " | geoAdddr: "  +  
    row.geoAddr  +  " | geoCity: "  +  
    row.geoCity  +  " | mod: "  +  
    row.mod  +  " | tz: "  +  
    row.tz);
}

}

      
}
);
  });  

});