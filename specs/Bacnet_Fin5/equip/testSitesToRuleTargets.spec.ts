import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//site dis
let siteDis= "City Center";

let siteId= "read(site and dis=="  +  JSON.stringify(siteDis)  +  ")->id";

let parameter= siteId;



let po = new axonPO({"testQuery":"sitesToRuleTargets("  +  parameter  +  ').toGrid.keepCols(["id","dis","floorRef","mod","navName","disMacro","siteRefDis"])' ,"confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The sitesToRuleTargets() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "sitesToRuleTargets("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"sitesToRuleTargets("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  10; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.dis.startsWith(siteDis)  ===  true,
  
  "sitesToRuleTargets("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRefDis: "  +  
  row.floorRefDis  +  " | mod: "  +  
  row.mod  +  " | navName: "  +  
  row.navName  +  " | siteRefDis: "  +  
  row.siteRefDis);
}

}
);
  });  

});