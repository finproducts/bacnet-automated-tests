import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Nokia Lumia 520 - platform: Windows Phone
let parameter= 'null, null, null, {platformOs:"Windows Phone", userAgent:"Mozilla 5.0 (compatible; MSIE 10.0; Windows Phone 8.0;"  +  "Trident 6.0; IEMobile 10.0; ARM; Touch; NOKIA; Lumia 520)"}';



let po = new axonPO({"testQuery":"finStackMobileMenu("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The bacnetWriteObjectProperty_Windows Phone() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finStackMobileMenu("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finStackMobileMenu("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.displayName  !==  undefined  &&  
  row.iconType  !==  undefined  &&  
  row.level  !==  undefined  &&  
  row.order  !==  undefined,
  
  "finStackMobileMenu("  +  parameter  +  ") query results contains valid data on row "  +  i  
  +  " => action: "  +  row.action  
  +  " | appId: "  +  row.appId  
  +  " | badgeType: "  +  row.badgeType  
  +  " | classes: "  +  row.classes  
  +  " | displayName: "  +  row.displayName  
  +  " | icon: "  +  row.icon  
  +  " | minUiTarget: "  +  row.minUiTarget);
}

}
);
  });  

});