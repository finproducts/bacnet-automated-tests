import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of eligible parameters
let parameterArray= ["sensor","point"];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex];



let po = new axonPO({"testQuery":"finUniqueActions("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finUniqueActions() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finUniqueActions("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finUniqueActions("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.count  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.expr  !==  undefined,
  
  "finUniqueActions("  +  randomParameter  +  ") query results contains valid data on row "  +  i  
  +  " => count: "  +  row.count  
  +  " | dis: "  +  row.dis  
  +  " | expr: "  +  row.expr);
}

}
);
  });  

});