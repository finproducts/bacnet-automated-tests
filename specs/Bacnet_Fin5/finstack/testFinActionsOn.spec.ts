import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//point record
let pointRec= 'read(point and siteRef->dis== "City Center")';

//argument passed to the axon faction
let filterExpr= pointRec;



let po = new axonPO({"testQuery":"finActionsOn("  +  filterExpr  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finActionsOn() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finActionsOn("  +  filterExpr  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finActionsOn("  +  filterExpr  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.expr.includes("point")  ===  true,
"finActionsOn("  +  filterExpr  +  ") query results contains valid data on row "  +  i  +  " => dis: "  +  row.dis  +  " | expr: "  +  row.expr);
//console.log(JSON.stringify(testData[i]));
}

}
);
});  

});