import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= JSON.stringify(createRandomString());

//array of possible parameters
let paramArray= [randomString];

//randomize
let randomIndex= Math.floor(Math.random()  *  paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= paramArray[randomIndex];



let po = new axonPO({"testQuery":"finRemoveCategory("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finRemoveCategory() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finRemoveCategory("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finRemoveCategory("  +  randomParameter  +  ") returned an object which doesn\'t contain an error.");

ok(testData[0].affected  >=  0,
  "finRemoveCategory("  +  randomParameter  +  ") query results contains valid data => affected: "  +  testData[0].affected);
}
);
  });  

});