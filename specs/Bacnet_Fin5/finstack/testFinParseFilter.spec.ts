import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//not OK


let po = new axonPO({"testQuery":"finParseFilter(point and writeVal < 10)","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finParseFilter() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
	"finParseFilter(point and writeVal < 10) query result is not undefined or null.");

this.ok(typeof testData === "object" && testData.type !== "error", 
"finParseFilter(point and writeVal < 10) returned an object which doesn't contain an error.");
		
 this.ok(testData[0].dis === null 
			&& testData[0].id !== undefined 
			&& testData[0].isAnd === "✓" 
			&& testData[0].andTags === "point" 
			&& testData[0].properties === "ver:\"3.0\"\nrightHandSide,valueType,control,leftHandSide\n\"10\",\"Number\",\"<\",\"writeVal\"\n\n"
			, 
			
				"finParseFilter(point and writeVal < 10) query results contains valid data => dis: " + testData[0].dis 
				+ " | id: " + testData[0].id 
				+ " | isAnd: "	+ testData[0].isAnd 
				+ " | andTags: " + testData[0].andTags 
				+ " | properties: " + testData[0].properties);
		
  } 
);
});  

});