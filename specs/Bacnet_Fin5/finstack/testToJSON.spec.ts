import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

//function that randomly creates a number
function createRandomNumber() {

	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomString= createRandomString();
let randomNumber= createRandomNumber();

let parameter= "["  +  [JSON.stringify(randomString), randomNumber, true, false]  +  "]";



let po = new axonPO({"testQuery":"toJSON("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The toJSON() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "toJSON("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"toJSON("  +  parameter  +  ") returned an object which doesn't contains an error.");

ok(testData[0].val  ===  "["  +  randomString  +  ","  +  randomNumber  +  ","  +  true  +  ","  +  false  +  "]",
"toJSON("  +  parameter  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});