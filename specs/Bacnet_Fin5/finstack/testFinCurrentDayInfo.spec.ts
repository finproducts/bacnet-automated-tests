import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//OK


let po = new axonPO({"testQuery":"finCurrentDayInfo()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finCurrentDayInfo() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCurrentDayInfo() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCurrentDayInfo() returned an object which doesn\'t contain an error.");

ok(testData[0].daysLeftMonth  >=  0  &&  
  testData[0].daysLeftYear  >=  0  &&  
  testData[0].daysPastMonth  >=  0  &&  
  testData[0].daysPastYear  >=  0  &&  
  testData[0].hoursLeftDay  >=  0  &&  
  testData[0].hoursPastDay  >=  0  &&  
  testData[0].monthsLeftYear  >=  0  &&  
  testData[0].monthsPastYear  >=  0,
  
  "finCurrentDayInfo() query results contains valid data => daysLeftMonth: "  +  
  testData[0].daysLeftMonth  +  " | daysLeftYear: "  +  
  testData[0].daysLeftYear  +  " | daysPastMonth: "  +  
  testData[0].daysPastMonth  +  " | daysPastYear: "  +  
  testData[0].daysPastYear  +  " | hoursLeftDay: "  +  
  testData[0].hoursLeftDay  +  " | hoursPastDay: "  +  
  testData[0].hoursPastDay  +  " | monthsLeftYear: "  +  
  testData[0].monthsLeftYear  +  " | monthsPastYear: "  +  
  testData[0].monthsPastYear);
}
);
  });  

});