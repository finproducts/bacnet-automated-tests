import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//testQuery
//array of eligible parameters
let parameterArray= [{q:'"wing","site"', msg:"wing was created successfully"}, {q:"region", msg:"region was created successfully"}];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex].q;

//the boolean value of expectedEvalResult variable for each eligible parameters within the array 
let expectedEvalResultOnAdd= parameterArray[randomIndex].msg;

//confirmQuery
let parameterArrayRemove= [{q:'"-wing","site"', msg:"wing was successfully removed"}, {q:'"-region"', msg:"region was successfully removed"}];
let parameterRemove;;
let expectedEvalResultOnRemove;;


if( randomParameter  ===  parameterArray[0].q  &&  expectedEvalResultOnAdd  ===  parameterArray[0].msg)  {
parameterRemove  =  parameterArrayRemove[0].q
expectedEvalResultOnRemove  =  parameterArrayRemove[0].msg
}

    else 
        if( randomParameter  ===  parameterArray[1].q  &&  expectedEvalResultOnAdd  ===  parameterArray[1].msg)       {
parameterRemove  =  parameterArrayRemove[1].q
expectedEvalResultOnRemove  =  parameterArrayRemove[1].msg
};


let po = new axonPO({"testQuery":"finNavMeta("  +  randomParameter  +  ")","confirmQuery":"finNavMeta("  +  parameterRemove  +  ")"});
po.setup(function(instance)
{}
)

describe("The finNavMeta() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavMeta("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavMeta("  +  randomParameter  +  ") returned an object which doesn't contains an error.");

ok(testData[0].val  ===  expectedEvalResultOnAdd,
  "finNavMeta("  +  randomParameter  +  ") query results contains valid data: "  +  testData[0].val);

ok(confirmData  !==  undefined  &&  confirmData  !==  null,
  "finNavMeta("  +  parameterRemove  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"finNavMeta("  +  parameterRemove  +  ") returned an object which doesn't contains an error.");

ok(confirmData[0].val  ===  expectedEvalResultOnRemove,
  "finNavMeta("  +  parameterRemove  +  ") query results contains valid data: "  +  confirmData[0].val);
}
);
  });  

});