import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array with schedule ref - [0]Normal Work Week, [1]Sport Weekend, [2]Summer School
let scheduleIdArray= [{refId:'read(schedule and dis=="Normal Work Week")->id', name:"Normal Work Week"}, 
                       {refId:'read(schedule and dis=="Sport Weekend")->id', name:"Sport Weekend"}, 
                       {refId:'read(schedule and dis=="Summer School")->id', name:"Summer School"}];

//randomize
let randomIndex= Math.floor(Math.random()  *  scheduleIdArray.length);

//the random parameter that will be passed to the axon faction
let randomSchedule= scheduleIdArray[randomIndex].refId;

//message received after running the function
let notificationMessage= scheduleIdArray[randomIndex].name;

//argument passed to the axon function
let randomParameter= randomSchedule  +  ",\"sat\",\"mon\"";

//undo
let undoChanges= randomSchedule  +  ",\"fri\",\"mon\"";



let po = new axonPO({"testQuery":"finScheduleCopyDays("  +  randomParameter  +  ")","confirmQuery":"finScheduleCopyDays("  +  undoChanges  +  ")"});
po.setup(function(instance)
{}
)

describe("The finScheduleCopyDays() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finScheduleCopyDays("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finScheduleCopyDays("  +  randomParameter  +  ") returned an object which doesn\'t contains an error.");

ok(testData[0].dis  ===  notificationMessage,
  "finScheduleCopyDays("  +  randomParameter  +  ") query results contains valid data: "  +  testData[0].dis);

ok(confirmData  !==  undefined  &&  confirmData  !==  null,
  "finScheduleCopyDays("  +  undoChanges  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"finScheduleCopyDays("  +  undoChanges  +  ") returned an object which doesn't contains an error.");

ok(confirmData[0].dis  ===  notificationMessage,
  "finScheduleCopyDays("  +  undoChanges  +  ") query results contains valid data: "  +  confirmData[0].dis);
}
);
  });  

});