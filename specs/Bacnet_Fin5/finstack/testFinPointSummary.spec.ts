import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//equip id
let equipId= 'read(equip and siteRef->dis=="City Center")->id';

//the random parameter that will be passed to the axon faction
let parameter= equipId;



let po = new axonPO({"testQuery":"finPointSummary("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finPointSummary() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finPointSummary("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finPointSummary("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].dis  ===  "City Center Vav-34"  && testData[0].id  !==  undefined,
"finPointSummary("  +  parameter  +  ") query results contains valid data => dis: "  +  testData[0].dis  +  " | id: "  +  testData[0].id);
}
);
  });  

});