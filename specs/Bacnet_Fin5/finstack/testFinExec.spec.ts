import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {snapshotTimestampForRestore as timestamp, trioFilePath}  from '../../../helpers/testingUtils';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK
 

//argument passed to the axon faction
let randomParameter= JSON.stringify(trioFilePath)  +  ", true";

//undo changes 



let po = new axonPO({"testQuery":"finExec(" +  randomParameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finExec() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finExec("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finExec("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].effected  ===  0,"finExec("  +  randomParameter  +  ") query results contains valid data => effected: "  +  testData[0].effected);

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  timestamp  +  ")... Changes undone.");
}
);
  });  

});