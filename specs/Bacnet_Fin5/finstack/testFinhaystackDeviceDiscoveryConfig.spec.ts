import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"finhaystackDeviceDiscoveryConfig()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finhaystackDeviceDiscoveryConfig() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finhaystackDeviceDiscoveryConfig() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finhaystackDeviceDiscoveryConfig() returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]","finhaystackDeviceDiscoveryConfig() contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});