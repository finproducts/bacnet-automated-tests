import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//site id: "Aquarium"
let siteId= 'read(site and dis=="Aquarium")->id';

//tag to syncronize
let tagToSync= 'tz';

//arguments passed to the axon function
let parameter= siteId  +  ", ["  +  JSON.stringify(tagToSync)  +  "]";



let po = new axonPO({"testQuery":"finSyncTags("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finSyncTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSyncTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSyncTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]","finSyncTags("  +  parameter  +  ") result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});