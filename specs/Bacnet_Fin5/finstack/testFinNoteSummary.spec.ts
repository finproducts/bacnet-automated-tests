import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"readAll(note).finNoteSummary()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNoteSummary() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "readAll(note).finNoteSummary() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"readAll(note).finNoteSummary() returned an object which doesn't contain an error.");

if( JSON.stringify(testData)  ===  "[]") 
      {
ok(JSON.stringify(testData)  ===  "[]","No notes found: "  +  JSON.stringify(testData));
}

    else 
        {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.author  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.noteSummary  ===  "✓",
  
  "readAll(note).finNoteSummary() query results contains valid data on row "  +  i  
  +  " => id: "  +  row.id  
  +  " | author: "  +  row.author  
  +  " | dis: "  +  row.dis  
  +  " | noteSummary: "  +  row.noteSummary);
}

}

      
}
);
  });  

});