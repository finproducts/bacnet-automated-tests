import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK


let po = new axonPO({"testQuery":"finConnReadAllConns()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finConnReadAllConns() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finConnReadAllConns() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finConnReadAllConns() returned an object which doesnt contain an error.");

if( JSON.stringify(testData)  ===  "[]") 
      {
ok(JSON.stringify(testData)  ===  "[]","No connection found: "  +  JSON.stringify(testData));
}

    else 
        {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.connProto  !==  undefined  &&  
  row.connTag  !==  undefined,
  
  "finConnReadAllConns() query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | connProto: "  +  
  row.connProto  +  " | connTag: "  +  
  row.connTag);

}

}

      
}
);
  });  

});