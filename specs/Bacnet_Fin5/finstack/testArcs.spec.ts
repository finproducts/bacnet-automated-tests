import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK but, it works just with 10 lines.

//array of eligible parameters
let parameterArray= ["point", "site", "equip", "filter", "curVal"];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex];



let po = new axonPO({"testQuery":"arcs(readAll("  +  randomParameter  +  "))","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The arcs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "arcs(readAll("  +  randomParameter  +  ")) query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"arcs(readAll("  +  randomParameter  +  ")) returned an object which does not contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.id  !==  undefined,
  "arcs(readAll("  +  randomParameter  +  ")) query results contains valid data on row "  +  i  +  " => dis: "  +  row.dis  +  " | id: "  +  row.id);
}

}
);
  });  

});