import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let stringName= "Selenium_"  +  createRandomString();

//argument passed to the axon function
let parameter= JSON.stringify(stringName);


let po = new axonPO({"testQuery":"finSubStart("  +  parameter  +  ")","confirmQuery":"finShowWatches()"});
po.setup(function(instance)
{}
)

describe("The finSubStart() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
  ok(testData !== undefined && testData !== null, 
    "finSubStart("+ parameter +") query result is not undefined or null.");

  ok(typeof testData === "object" && testData.type !== "error", 
  "finSubStart("+ parameter +") returned and object which doesn't contain an error.");
  
this.ok(JSON.stringify(testData) === "[]", 
    "finSubStart("+ parameter +") query results contains valid data: "+ JSON.stringify(testData)); 
               
    //running finShowWatches() to check for the new watch created
    var i;
    var found = false;
    for (i= 0 ; i < confirmData.length; i++){
      if (confirmData[i].dis === stringName){
        found = true;
        }
    }
     
    this.ok(found === true, 
        "Running finShowWatches()... Confirming that the watch with the debug string \""+ stringName +"\" was successfully created.");
}
);
  });  

});