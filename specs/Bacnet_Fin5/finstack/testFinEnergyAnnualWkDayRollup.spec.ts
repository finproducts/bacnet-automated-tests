import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//point id
let pointId= 'read(point and kind=="Number")->id';

//argument passed to the axon faction - random point id 
let randomParameter= pointId;



let po = new axonPO({"testQuery":"finEnergyAnnualWkDayRollup("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finEnergyAnnualWkDayRollup() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
      "finEnergyAnnualWkDayRollup("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finEnergyAnnualWkDayRollup("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

let notificationMessage= "finEnergyAnnualWkDayRollup("+ randomParameter +") query results contains valid data on row "+ i +" => dateStamp: "
+ row.dateStamp +" | week: "+ row.week;

if( row.mon  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | mon: "  +  row.mon
}

    

if( row.tue  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | tue: "  +  row.tue
}

    

if( row.thu  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | thu: "  +  row.thu
}

    

if( row.wed  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | wed: "  +  row.wed
}

    

if( row.fri  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | fri: "  +  row.fri
}

    

if( row.sat  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | sat: "  +  row.sat
}

    

if( row.sun  !==  undefined) 
      {
notificationMessage  =  notificationMessage  +  " | sun: "  +  row.sun
}

    

ok(row.dateStamp  !==  undefined  &&  row.week  >=  0,notificationMessage);
}

}
);
  });  

});