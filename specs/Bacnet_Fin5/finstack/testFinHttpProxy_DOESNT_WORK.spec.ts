import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString()
{
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));

    return randomStr;
};

let randomString= ""  +  createRandomString()  +  "";

//random url format
let randomUrl= "`"+ "http://localhost/"+ createRandomString() +"/"+ createRandomString() +"`";

//the random parameter that will be passed to the axon faction
let randomParameter= randomUrl;



let po = new axonPO({"testQuery":"finHttpProxy("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHttpProxy() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHttpProxy("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHttpProxy("  +  randomParameter  +  ") returned an object which doesn't contains an error.");

ok(testData[0].val.includes('<!DOCTYPE html')  ===  true,
"finHttpProxy("  +  randomParameter  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});