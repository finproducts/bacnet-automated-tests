import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//random ref id selection
let pointId= 'read(point and siteRef->dis=="City Center")->id';

//random target tag selection
let targetTag= 'point';

//tag to be cloned
let clonedTag= 'point';

//argument passed to the axon function
let parameter= pointId  +  ","  +  JSON.stringify(targetTag)  +  ", ["  +  JSON.stringify(clonedTag)  +  "]";



let po = new axonPO({"testQuery":"finRecordTagClone("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finRecordTagClone() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finRecordTagClone("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finRecordTagClone("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].affected  >=  0,
  "finRecordTagClone("  +  parameter  +  ") query results contains valid data: "  +  testData[0].affected);
}
);
  });  

});