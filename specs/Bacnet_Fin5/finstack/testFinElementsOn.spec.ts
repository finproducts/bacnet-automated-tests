import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of eligible parameters
let parameterArray= ["point","equip","ahu","filter"];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex];



let po = new axonPO({"testQuery":"finElementsOn(readAll("  +  randomParameter  +  ")[0..5],\"form\")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finElementsOn() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finElementsOn(readAll("  +  randomParameter  +  ")[0..10],\"form\") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finElementsOn(readAll("  +  randomParameter  +  ")[0..10],\"form\") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.lenght; i++)
{
let row= testData[i]

ok(row.formOn  !==  undefined  &&  
  row.name  !==  undefined  &&  
  row.src  !==  undefined  &&  
  row.func  ===  "✓"  &&  
  row.ext  ===  "finTools",
  
  "finElementsOn(readAll("  +  randomParameter  +  ")[0..10],\"form\") query results contains valid data on row "  +  i  +  " => "  +  "dis: "  +  
  row.dis  +  " | ext: "  +  
  row.ext  +  " | formOn: "  +  
  row.formOn  +  " | name: "  +  
  row.name  +  " | src: "  +  
  row.src  +  " | func: "  +  
  row.func);
}

}
);
  });  

});