import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//site dis
let siteDis= "City Center";

//site id
let siteId= "read(site and dis=="  +  JSON.stringify(siteDis)  +  ")->id";

//argument passed to the axon function
let parameter= siteId;



let po = new axonPO({"testQuery":"finNavMetaTagList("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavMetaTagList() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavMetaTagList("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavMetaTagList("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.filterTagProps  !==  undefined  &&  
  row.kind  !==  undefined  &&  
  row.name  !==  undefined  &&  
  row.site  ===  "✓",
  
  "finNavMetaTagList("  +  parameter  +  ") query results contains valid data on row " + i  
  +  " => filterTagProps: "  +  row.filterTagProps  
  +  " | kind: "  +  row.kind  
  +  " | name: "  +  row.name  
  +  " | site: "  +  row.site);
}

}
);
  });  

});