import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//restore to the latest timestamp
let getSnapshotTimestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 

//source to be copied
let sourceRef= 'readAll(floor and dis =="Floor 1")[0]->id';

//target location for the copied
let targetRef= 'readAll(floor and dis =="Floor 2")[0]->id';

//argument passed to the axon function: 
let parameter= sourceRef  +  ","  +  targetRef;



let po = new axonPO({"testQuery":"finNavMerge("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  getSnapshotTimestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finNavMerge() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavMerge("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavMerge("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]","finNavMerge("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  getSnapshotTimestamp  +  ")... Restored.");
}
);
  });  

});