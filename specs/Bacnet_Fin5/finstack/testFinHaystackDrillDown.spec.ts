import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {nHaystackConnIdGlobalVariable}           from '../../../helpers/testingUtils';


//OK


//specific test data string related with the haystack connection
let specificString= "Aquarium";

//argument passed to the axon function
let parameter= nHaystackConnIdGlobalVariable;



let po = new axonPO({"testQuery":"finHaystackDrillDown("  +  parameter  + ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHaystackDrillDown() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHaystackDrillDown("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHaystackDrillDown("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.idDis.includes(specificString)  ===  true  &&  
  row.axSlotPath  !==  undefined  &&  
  row.navName  !==  undefined,
  
  "finHaystackDrillDown("  +  parameter  +  ") query results contains valid data on row "  +  i  
  +  " => id: "  +  row.id  
  +  " | axSlotPath: "  +  row.axSlotPath  
  +  " | navName: "  +  row.navName  
  +  " | siteRef: "  +  row.siteRef  
  +  " | idDis: "  +  row.idDis);
}

}
);
  });  

});