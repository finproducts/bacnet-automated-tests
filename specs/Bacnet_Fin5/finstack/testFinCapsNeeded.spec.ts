import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of eligible parameters
let parameterArray= ["11000,3","100,9","5000,2","11000,100","700,10","1,1","1,10000","0,0","100000"];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex];



let po = new axonPO({"testQuery":"finCapsNeeded("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finCapsNeeded() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCapsNeeded("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCapsNeeded("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.acc  !==  undefined  &&  
  row.desc.startsWith('FIN Stack')  ===  true  &&  
  row.price  !==  undefined  &&  row.prodPart.startsWith('FIN')  ===  true,
  
  "finCapsNeeded("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => acc: "  +  
  row.acc  +  " | desc: "  +  
  row.desc  +  " | price: "  +  
  row.price  +  " | prodPart: "  +  
  row.prodPart);
}

}
);
  });  

});