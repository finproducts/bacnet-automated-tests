import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//application array
let appsArray= ["builder", "folio", "alarms", "bline", "weather", "users", "historian"];

//randomize
let randomIndex= Math.floor(Math.random()  *  appsArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= '["  +  JSON.stringify(appsArray[randomIndex])  +  "]';



let po = new axonPO({"testQuery":"finGetHelp("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGetHelp() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "finGetHelp("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGetHelp("  +  randomParameter  +  ") returned an object which doesn\'t contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.body  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.finHelpResource  ===  "✓"  &&  
  row.name  !==  undefined  &&  
  row.tags  !==  undefined  &&  
  row.title  !==  undefined,
  
  "finGetHelp("  +  randomParameter  +  ") query results contains valid data on row "  +  i  
  +  " => body: "  +  row.body  
  +  " | dis: "  +  row.dis  
  +  " | finHelpResource: "  +  row.finHelpResource  
  +  " | name: "  +  row.name  
  +  " | tags: "  +  row.tags  
  +  " | title: "  +  row.title);
}

}
);
  });  

});