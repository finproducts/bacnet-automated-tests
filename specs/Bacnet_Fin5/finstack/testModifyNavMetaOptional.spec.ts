import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1st argument
let parameterArray1= ["site", "seleniumTestLevel"];
let randomIndex1= Math.floor(Math.random()  *  parameterArray1.length);
let randomParameter1= parameterArray1[randomIndex1];

//2st argument
let parameterArray2= ["floor"];
let randomIndex2= Math.floor(Math.random()  *  parameterArray2.length);
let randomParameter2= parameterArray2[randomIndex2];

//argument passed to the axon function
let randomParameter= JSON.stringify(randomParameter1)  +  ","  +  JSON.stringify(randomParameter2);


let po = new axonPO({"testQuery":"modifyNavMetaOptional("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The modifyNavMetaOptional() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "modifyNavMetaOptional("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"modifyNavMetaOptional("  +  randomParameter  +  ") returned an object which doesn\t contain an error.");

if( randomParameter1  ===  "site"  &&  randomParameter2  ===  "floor") 
      {
ok(testData[0].val  ===  "yesLevel",
"modifyNavMetaOptional("  +  randomParameter  +  ") query results contains valid data => val: "  +  testData[0].val);
}

    else 
        if( randomParameter1  ===  "seleniumTestLevel"  &&  randomParameter2  ===  "floor") 
      {
ok(testData[0].val  ===  "/site/floor/seleniumTestLevel/equip/point","modifyNavMetaOptional("  +  randomParameter  +  ") query results contains valid data => val: "  +  testData[0].val);
}

    
      
}
);
  });  

});