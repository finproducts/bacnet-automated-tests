import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//rec id: "City Center Vav-20 AirFlow Setpoint"
let recId = 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter = recId;

let po = new axonPO({"testQuery": "readLink("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The readLink() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "readLink("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "readLink("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].dis.includes('City Center') === true, 
      "readLink("+ parameter +") query results contain invalid data => dis:"+ testData[0].dis);
    });
  });  
});