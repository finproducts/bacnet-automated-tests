import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 10; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

//generates the current year, month & day
function getCurrentDate() {
  var today = new Date();
  var day:any = today.getDate();
  var month:any = today.getMonth() + 1; 
  var year = today.getFullYear();

  if(day < 10) {
    day ='0'+ day;
  } 

  if(month < 10) {
    month = '0'+ month;
  }  
  var currentYearMonthAndDay = year +"-"+ month +"-"+ day;
  return currentYearMonthAndDay;
};

//get current date
let currentDate = getCurrentDate();

//the log message included in the argument, containing also the current date
let logMsg = "Log date " + currentDate + ": Some text as a placeholder for a log - " + randomString;

//argument passed to the axon function
let randomParameter = '"test",' + JSON.stringify(logMsg);

let po = new axonPO({"testQuery": "logDebug("+ randomParameter +")", "confirmQuery": "logRead ("+ currentDate +")"});
po.setup(function(instance) {});

describe('The logDebug() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "logDebug("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "logDebug("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) ===  "[{\"val\":null}]", 
      "logDebug("+ randomParameter +") query results contain invalid data. A new log at debug level was created.");
    });
  });  
});