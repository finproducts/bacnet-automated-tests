import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



let getSnapshotTimestamp = 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

//argument passed to the axon function
let timestamp = getSnapshotTimestamp;

let po = new axonPO({"testQuery": "folioRestore("+ timestamp +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The folioRestore() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "folioRestore("+ timestamp +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "folioRestore("+ timestamp +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]", 
      "folioRestore("+ timestamp +") query result contains invalid data: "+ JSON.stringify(testData));
    });
  });  
});