import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//tree name
let treeName = 'equip';

//records type
let parameterArray = ["equip", "point"];
let randomIndex = Math.floor(Math.random() * parameterArray.length);
let randomRecType = parameterArray[randomIndex];

//records
let recs = "readAll("+ randomRecType +")";

//argument passed to the axon function
let parameter = JSON.stringify(treeName) + "," + recs;

let po = new axonPO({"testQuery": "navFilter("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The navFilter() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "navFilter("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "navFilter("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id.startsWith('@') === true 
        && row.dis !== undefined, 
        "navFilter("+ parameter +") query results contain invalid data on row "+ i +" => id: "+ row.id +" | dis: "+ row.dis);
      }
    });
  });  
});