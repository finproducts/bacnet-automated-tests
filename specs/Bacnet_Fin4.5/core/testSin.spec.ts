import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates a number
function createRandomNumber() {
	var min = 0;
	var max = 99;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();

//argument passed to the axon faction
let randomParameter = randomNumber;

let po = new axonPO({"testQuery": "sin("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The sin() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "sin("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "sin("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined, "sin("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
    });
  });  
});