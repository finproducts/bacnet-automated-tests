import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//expected result
let expecRes = '__foldEnd__';

let po = new axonPO({"testQuery": "foldEnd()", "confirmQuery": ""});
po.setup(function(instance){});

describe('The foldEnd() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'foldEnd() query result is not undefined or null.');

      ok(typeof testData === "object" && testData.type !== "error",
      "foldEnd() returned an object which doesn't contain an error.");

      ok(testData[0].val === expecRes, "foldEnd() query results contains invalid data => val: "+ testData[0].val);
    });
  });  
}); 