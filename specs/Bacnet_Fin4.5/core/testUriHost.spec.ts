import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let paramArray= ["10.0.0.1","100.64.0.2","127.0.0.3","169.254.0.4","172.16.0.5","192.0.0.17","192.0.2.19","192.88.99.9","192.168.0.16",
                "198.18.0.13","198.51.100.10","203.0.113.20","224.0.0.4","240.0.0.3","255.255.255.255","localhost"];

//randomize index
let randomIndex = Math.floor(Math.random() * paramArray.length);

let randomSelect = paramArray[randomIndex];

//the random parameter that will be passed to the axon faction
let randomParameter = "`" + "http://"+ randomSelect +":81/pathSegment1/pathSegment2/filename.ext" +"`";

let po = new axonPO({"testQuery": "uriHost("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The uriHost() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "uriHost("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "uriHost("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === randomSelect, 
      "uriHost("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});