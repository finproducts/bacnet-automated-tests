import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let numericValue = '16';

//argument passed to the axon function
let parameter = numericValue;

let po = new axonPO({"testQuery": "toHex("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The toHex()',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "toHex("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "toHex("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === "10", "toHex("+ parameter +") query result contain invalid data => val: "+ testData[0].val);
    });
  });  
});