import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let grid = 'toGrid([{a:5, b:1}])';

//axon function
let axonFn = 'r=>r.set("a",(r->a)*(r->a))';

//argument passed to the axon faction 
let parameter = grid + "," + axonFn;

let po = new axonPO({"testQuery": "map("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The map() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined && testData !== undefined, "map("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "map("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"a":25,"b":1}]',
      "map("+ parameter +") results contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});