import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//argument passed to the axon faction 
let parameter= 'readAll(point and unit=="%")[0..3],["curVal"],"newCol",sum';

let po = new axonPO({"testQuery": "foldCols("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The foldCols() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "foldCols("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "foldCols("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.newCol !== undefined, 
        "foldCols("+ parameter +") query results contains invalid data on row "+ i +" => id: "+
        row.id +" | dis: "+ row.dis +" | cur: "+ row.cur +" | disMacro: "+ row.disMacro +" | floorRef: "+
        row.floorRef +" | hisFunc: "+ row.hisFunc +" | kind: "+ row.kind +" | mod: "+ row.mod +" | navName: "+
        row.navName +" | newCol: "+ row.newCol +" | precision: "+ row.precision +" | siteRef: "+
        row.siteRef +" | tz: "+ row.tz);
      }
    });
  });  
});