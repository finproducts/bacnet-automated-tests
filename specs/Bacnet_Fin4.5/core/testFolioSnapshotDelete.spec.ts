import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//timestamp of the snapshot
let timestamp = '2018-11-21T12:40:00.000Z UTC';

//date format of the snapshot timestamp
let dateFormat = "YYYY-MM-DD'T'hh:mm:SS.FFFFFFFFFz zzzz";

//expected value of the result: true - snapshot deleted / false - snapshot with the respective timestamp does not exist.
let expectedValue = false;

let po = new axonPO({"testQuery": "folioSnapshotDelete(parseDateTime(\""+
timestamp +"\", \""+ dateFormat +"\"))", "confirmQuery": ""});

po.setup(function(instance) {});

describe('The folioSnapshotDelete() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null,
      "folioSnapshotDelete(parseDateTime("+ timestamp +", "+ dateFormat +")) query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "folioSnapshotDelete(parseDateTime("+ timestamp +", "+
      dateFormat +")) returned an object which doesn't contain an error.");

      ok(testData[0].val === expectedValue, "Snapshot with timestamp: "+ timestamp +" deleted: "+ testData[0].val);
    });
  });  
});