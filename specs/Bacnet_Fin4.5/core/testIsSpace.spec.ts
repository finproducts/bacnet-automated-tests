import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomFloatNumber = createRandomNumber();

//Ascii char code: 9 - TAB, 13 - Enter, 32 - Space
//array of possible parameters
let paramArray = [randomFloatNumber, 32, 9, 13];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isSpace("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isSpace() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isSpace("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isSpace("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === randomFloatNumber) {
        ok(testData[0].val === false, 
        "isSpace("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else if( randomParameter  !==  randomFloatNumber) {
        ok(testData[0].val === true, 
        "isSpace("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });  
});