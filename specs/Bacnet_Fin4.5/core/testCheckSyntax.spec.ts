import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//expression of an axon function
let axonFn = '(f)=>do 1+1 end';

//argument passed to the axon function 
let parameter= JSON.stringify(axonFn);

let po = new axonPO({"testQuery": "checkSyntax("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The checkSyntax() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      &&  testData !==  null, "checkSyntax("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "checkSyntax("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[]", 
      "checkSyntax("+ parameter +") results contains invalid data: "+ JSON.stringify(testData));
    });
  });  
});