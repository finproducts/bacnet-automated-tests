import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//1st argument
//function that randomly creates an alphanumeric string of 10 characters

function createRandomString() {
	
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = createRandomString();

function createRandomNumber() {

	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();
let firstParameter = "["+ JSON.stringify(randomString) + "," + randomNumber +"]";

//2nd argument
//array of possible arguments
let paramArray = [createRandomString(), createRandomNumber()];

//randomize
let randomIndex = Math.floor(Math.random()  *  paramArray.length);

//the random parameter that will be passed to the axon function
let secondParameter = paramArray[randomIndex];

if(randomIndex === 0) {    
  var randomParameter = firstParameter + "," + JSON.stringify(secondParameter);
}

else if( randomIndex === 1)  {
  let randomParameter = firstParameter + "," + secondParameter;
}

let po = new axonPO({"testQuery": "add("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance){})

describe('Testing the add() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "add("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "add("+ randomParameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        ok(testData[2].val === secondParameter, 
        "add("+ randomParameter +") query results contains valid data on index "+ i +" => "+ testData[i].val);
      }
    });
  });  
});