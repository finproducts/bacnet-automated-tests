import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//point id
let pointId = 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter = pointId;

let po = new axonPO({"testQuery": "readById("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The readById() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "readById("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "readById("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].dis.includes('City Center') === true, 
      "readById("+ parameter +") query results contain invalid data => dis: "+ testData[0].dis);
    });
  });  
});