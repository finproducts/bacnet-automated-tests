import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let grid= '[{dis:"Bob", age:30}, {dis:"Ann", age:40}, {dis:"Dan", age:50}]';

//parameter passed to the axon function
let parameter = grid;

let po = new axonPO({"testQuery": "toGrid("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The toGrid()',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "toGrid("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "toGrid("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"age":30,"dis":"Bob"},{"age":40,"dis":"Ann"},{"age":50,"dis":"Dan"}]',
      "toGrid("+ parameter +") query result contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});