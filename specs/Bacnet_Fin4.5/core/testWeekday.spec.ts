import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function returns an integer from 0 to 6 of Date or DateTime, where 0 indicates Sunday and 6 indicates Saturday
//array of eligible parameters
let parameterArray= [{date:'parseDateTime("2015-10-15T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 4}, 
                     {date:'parseDateTime("2015-10-16T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 5},
	                   {date:'parseDateTime("2015-10-17T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 6},
                     {date:'parseDateTime("2015-10-18T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 0},
	                   {date:'parseDateTime("2015-10-19T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 1},
                     {date:'parseDateTime("2015-10-20T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 2},
	                   {date:'parseDateTime("2015-10-21T09:18:16.000Z UTC","YYYY-MM-DD\'T\'hh:mm:SS.FFFFFFFFFz zzzz")', day: 3}];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//argument passed to the axon function
let randomParameter = parameterArray[randomIndex].date;

//expected result converted to miliseconds => (day) * h * min * sec * milisec
let expectedResultInMilisec = parameterArray[randomIndex].day * 24 * 60 * 60 * 1000;

let po = new axonPO({"testQuery": "weekday("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testWeekday() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "weekday("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "weekday("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === expectedResultInMilisec, 
      "weekday("+ randomParameter +") query results contain invalid data: "+ testData[0].val +" miliseconds");
    });
  });
});