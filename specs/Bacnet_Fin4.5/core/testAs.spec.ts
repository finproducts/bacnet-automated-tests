import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



let value = 1;
let unit = 'km';

//argument that will be passed to the axon function
let parameter = value + "," + JSON.stringify(unit);



let po = new axonPO({"testQuery": "as("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});


describe('The testAs() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  //testing as(parameter)
  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "as("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "as("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === 1 
      && testData[0].valUnit === "km", "as("+ parameter +") query results contains valid data => val: "+
      testData[0].val +" | valUnit: "+ testData[0].valUnit);
    });
  });  
});