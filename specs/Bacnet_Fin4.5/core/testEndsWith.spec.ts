import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
	var randomString = "";
	var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for(let i = 0; i < 10; i++ )
		randomString += charSequence.charAt(Math.floor(Math.random() * charSequence.length));

	return randomString;
};

let randomStringResult = createRandomString();
let subStringLastThree = randomStringResult.substr(randomStringResult.length - 3);

//the random parameter that will be passed to the axon faction
let randomParameter = '"'+ randomStringResult + '"' + "," + '"' + subStringLastThree +'"';

let po = new axonPO({"testQuery": "endsWith("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The endsWith() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
			ok(testData !== undefined 
			&& testData !== null, "endsWith("+ randomParameter +") query result is not undefined or null.");

			ok(typeof testData === "object" 
			&& testData.type !== "error", "endsWith("+ randomParameter +") returned an object which doesn't contains an error.");

			if(randomStringResult.indexOf(subStringLastThree) == 7) { 
			ok(testData[0].val === true, 
			"endsWith("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
			}

			else { 
				ok(testData[0].val === false, 
				"endsWith("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
			}
		});
  });  
});