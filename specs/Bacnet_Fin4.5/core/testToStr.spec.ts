import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//number that will be converted to string
let number = 123;

//argument passed to the axon function
let parameter = number;

let po = new axonPO({"testQuery": "toStr("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance){});

describe('The toStr() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "toStr("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "toStr("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === JSON.stringify(number), 
      "toStr("+ parameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});