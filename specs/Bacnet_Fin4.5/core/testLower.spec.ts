import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates a upper case string of 10 characters
function createRandomUpperCaseString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(var i = 0; i < 10; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomUpperString = createRandomUpperCaseString();

//function that randomly creates a number
function createRandomNumber() {
	var min = 0;
	var max = 127;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();

//array of eligible arguments
let paramArray = [JSON.stringify(randomUpperString), randomNumber];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "lower("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The lower() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "lower("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "lower("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === JSON.stringify(randomUpperString)) {
        ok(testData[0].val === randomUpperString.toLowerCase(), 
        "lower("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else if(randomParameter >= 65 && randomParameter <= 90) {
        ok(testData[0].val === (randomNumber + 32),
        "lower("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else {
        ok(testData[0].val === randomNumber, 
        "lower("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });  
});