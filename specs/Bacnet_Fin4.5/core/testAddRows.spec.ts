import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid    
let grid = '[{a:1,b:2}].toGrid()';

//new rows added to the grid
let newRows = '[{c:3,d:4}].toGrid()';

//argument passed to the axon function
let parameter = grid + "," + newRows;

let po = new axonPO({"testQuery": "addRows("+ parameter + ")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testAddRows() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "addRows("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "addRows("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"a":1,"b":2},{"c":3,"d":4}]', "addRows("+
      parameter +") results contains invalid data: "+ JSON.stringify(testData));
    });
  });  
});