import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//string
let string = 'selenium';

//substring to be replaced
let substring = 'sele';

//new substring
let newSubstring = 'pandemo';

//argument passed to the axon function
let parameter = JSON.stringify(string) +","+ JSON.stringify(substring) +","+ JSON.stringify(newSubstring);

let po = new axonPO({"testQuery": "replace("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The replace() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "replace("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "replace("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === "pandemonium", "replace("+ parameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});