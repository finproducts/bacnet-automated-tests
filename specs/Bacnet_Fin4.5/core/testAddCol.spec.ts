import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let grid = '[{a:1,b:2}].toGrid()';

//name of the new column
let newColName = 'sum';

//mapping function: results stored in the new column
//sum of values from column a & b
let fn = '(row)=>do row->a + row->b end';

//argument passed to the axon faction
let parameter = grid + "," + JSON.stringify(newColName) + "," + fn;

let po = new axonPO({"testQuery": "addCol("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testAddCol() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "addCol("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "addCol("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"a":1,"b":2,"sum":3}]', "addCol("+ parameter +") results contains invalid data: "+
      JSON.stringify(testData));
    });
  });  
});