import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = -1;
	var max = 2;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber = createRandomNumber();

//argument passed to the axon function
let randomParameter = randomNumber;

let po = new axonPO({"testQuery": "asin("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testAsin() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "asin("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "asin("+ randomParameter +") returned an object which doesn't contain an error.");

      if( randomNumber  ===  -1) {
        ok(testData[0].val === -1.5707963267948966, "asin("+
        randomParameter +") query results contains invalid data: "+ testData[0].val);

      } else if(randomNumber  ===  0) {
          ok(testData[0].val === 0, "asin("+
           randomParameter +") query results contains invalid data: "+ testData[0].val);

      } else  if(randomNumber  ===  1) {
          ok(testData[0].val === 1.5707963267948966, 
          "asin("+ randomParameter +") query results contains invalid data: "+ testData[0].val);

      } else  if( randomNumber  ===  2) {
          ok(testData[0].val === null,
          "asin("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }
    });
  }); 
});