import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//time in milliseconds
let date = new Date();
let d:any = date.getDate();
let mm:any = date.getMonth() + 1;
let yyyy = date.getFullYear();

if(mm < 10 && mm !=  0) {    
  mm = "0" + mm;
}

if(d < 10 && d != 0) {    
  d = "0" + d;
}

let timeInMillis = date.getTime();

//argument passed to the axon faction
let time = timeInMillis;
let resultTime = yyyy + "-" + mm + "-" + d;

let po = new axonPO({"testQuery": "fromJavaMillis("+ time +")", "confirmQuery": ""});
po.setup(function(instance) {}) 

describe('The fromJavaMillis() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined  
      && testData !== null, "fromJavaMillis("+ time +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "fromJavaMillis("+ time +") returned an object which doesn't contain an error.");

      ok(testData[0].val.includes(resultTime) === true, 
      "fromJavaMillis("+ time +") query results contains invalid data => val: "+ testData[0].val);
    });
  });  
});