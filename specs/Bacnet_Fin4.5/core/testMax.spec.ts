import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//testData
//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let a = createRandomNumber();
let b = createRandomNumber();

//the argument that will be passed to the axon faction
let randomParameter = a + "," + b;

//confirmData
let paramArray = [1,2,3];

let testFoldMax = "[1,2,3].fold(max)";

let po = new axonPO({"testQuery": "max("+ randomParameter +")", "confirmQuery": testFoldMax});
po.setup(function(instance) {});

describe('The max()',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
        && testData !== null, "max("+ randomParameter +") query result is not undefined or null.");

        ok(typeof testData === "object" 
        && testData.type !== "error", "max("+ randomParameter +") returned an object which doesn't contain an error.");

        if(a > b) {
          ok(testData[0].val === a, "max("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
        }
        else {
          ok(testData[0].val === b, "max("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
        }
        ok(confirmData !== undefined 
        && confirmData !== null, '[1,2,3].fold(max) query result is not undefined or null.');

        ok(typeof confirmData === "object" 
        && confirmData.type !== "error", "[1,2,3].fold(max) returned an object which doesn't contain an error.");

        ok(confirmData[0].val === paramArray[2], "[1,2,3].fold(max) query result contain invalid data: "+ confirmData[0].val);
    });
  });  
});