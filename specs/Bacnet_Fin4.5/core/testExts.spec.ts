import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "exts()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The exts() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "exts() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "exts() returned an object which doesn't contain an error.");
     
      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        
        ok(row.dis !== undefined  
        && row.extStatus !== undefined 
        && row.icon24 !== undefined 
        && row.icon72 !== undefined 
        && row.iconTable !== undefined 
        && row.name !== undefined 
        && row.version !== undefined, 
        "exts() query results contains invalid data on row " + i +" => dis: "+ row.dis +" | extStatus: "+
        row.extStatus +" | icon24: "+ row.icon24 +" | icon72: "+ row.icon72 +" | iconTable: "+ row.iconTable +" | name: "+
        row.name +" | version: "+ row.version); 
      }
    });
  });  
});