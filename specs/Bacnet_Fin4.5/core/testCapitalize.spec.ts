import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a lowercase string of 10 characters
function createRandomString() {	
  var randomStr = "";
  var charSequence = "abcdefghijklmnopqrstuvwxyz";
  for(let i = 0; i < 10; i++) 
    randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
  return randomStr;
};

let randomString = createRandomString();

//the random parameter that will be passed to the axon faction
let randomParameter = JSON.stringify(randomString);

let po = new axonPO({"testQuery": "capitalize("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The capitalize() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "capitalize("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "capitalize("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val.charAt(0) === randomString.charAt(0).toUpperCase(), 
      "capitalize("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});