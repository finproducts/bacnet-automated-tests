import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//function that randomly creates a number
function createRandomNumber() {
	var min = -50;
	var max = 50;
	var getRandomNumber = Math.floor((Math.random() * (max - min) + min) * 100) / 100;
	return getRandomNumber;	
};

let randomNumberX = createRandomNumber();
let randomNumberY = createRandomNumber();

//argument passed to the axon function
let randomParameter = "coord("+ randomNumberX + "," + randomNumberY +")";

let po = new axonPO({"testQuery": "coordLng("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The coordLng() query.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "coordLng("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "coordLng("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === randomNumberY,
      "coordLng("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});