import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//grid
let grid = '[{a:1,b:2}].toGrid()';

//new row added to the grid
let metaData = 'metaInfo';

//argument passed to the axon function
let parameter = grid + ", {"+ metaData +"}";

let po = new axonPO({"testQuery": "addMeta("+ parameter +")", "confirmQuery": "meta(addMeta("+ parameter +"))"});
po.setup(function(instance) {});

describe('The testAddMeta() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "addMeta("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "addMeta("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"a":1,"b":2}]', "addMeta("+
      parameter +") results contains valid data: "+ JSON.stringify(testData));

      ok(confirmData[0].metaInfo === "✓", 
      "meta(addMeta("+ parameter +")) results contains invalid data => metaInfo: "+ confirmData[0].metaInfo);
    });
  });  
});