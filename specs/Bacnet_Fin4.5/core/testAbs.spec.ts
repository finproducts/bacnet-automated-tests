import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {

    var min = -100;
    var max = 100;
    var getRandomNumber = Math.random() * (max - min) + min;
    return getRandomNumber;    
};

let randomNumber = createRandomNumber();

//array of eligible arguments
let paramArray = [null, randomNumber];

//randomize
let randomIndex= Math.floor(Math.random()  *  paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "abs("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('testAbs.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  it("Testing abs", () => {
    po.assert(function(testData,confirmData) {

      ok(testData !== undefined 
      && testData !== null, "abs("+ randomParameter +") query result is not undefined or null.");
      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "abs("+ randomParameter +") returned an object which doesn't contain an error.");

      if( randomParameter  ===  undefined) { 
        ok(JSON.stringify(testData) === "[{\"val\":null}]", 
        "abs("+ randomParameter +") - Parameter entered is null: "+ JSON.stringify(testData));
      }

      else if(randomParameter === randomNumber) {
        ok(testData[0].val === Math.abs(randomNumber), 
        "abs("+ randomParameter +") query results contains valid data: "+ testData[0].val);
      }
    });
  });  
});