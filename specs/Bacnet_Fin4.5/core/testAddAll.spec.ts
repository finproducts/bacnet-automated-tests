import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//function that randomly creates an alphanumeric string of 10 characters

function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(let i= 0; i < 10; i++)
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

//1st argument
let randomString1 = createRandomString();
let randomNumber1 = createRandomNumber();
let firstParameter = "["+ JSON.stringify(randomString1) + "," + randomNumber1 +"]";

//2nd argument
let randomString2 = createRandomString();
let randomNumber2 = createRandomNumber();
let secondParameter = "["+ JSON.stringify(randomString2) + "," + randomNumber2 +"]";

//argument passed to the axon function
let randomParameter = firstParameter + "," + secondParameter;

let po = new axonPO({"testQuery": "addAll("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The addAll() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "addAll("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "addAll("+ randomParameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        ok(testData[2].val === randomString2 
        && testData[3].val === randomNumber2, 
        "addAll("+ randomParameter +") query results contains invalid data on index "+ i +" => "+ testData[i].val);
      }
    });
  });  
});