import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//assertion values
let date = new Date();
let month = date.getMonth() + 1;
let dateDay = date.getDate();

//array of eligible parameters
let parameterArray = ["D", "M"];
let randomIndex = Math.floor(Math.random() * parameterArray.length);
let randomFormat = parameterArray[randomIndex];

//the result format of the axon function lastMonth()
let parameter = "today(),"+ JSON.stringify(randomFormat);

let po = new axonPO({"testQuery": "format("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The format() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "format("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "format("+ parameter +") returned an object which doesn't contain an error.");

      if( randomFormat === "D") { 
        ok(testData[0].val === dateDay.toString(), 
        "format("+ parameter +") query results contains invalid data => val: "+ testData[0].val);
      }

      else if(randomFormat === "M") { 
        ok(testData[0].val === month.toString(), 
        "format("+ parameter +") query results contains invalid data => val: "+ testData[0].val);
      }
    });
  });  
});