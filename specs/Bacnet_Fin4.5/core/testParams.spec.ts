import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible parameters
let parameterArray = ["readById", "finElementOn", "params", "finUniqueActions", "sum"];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex] + ".params";

let po = new axonPO({"testQuery": ""+ randomParameter +"", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The params() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, ""+ randomParameter +" query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", ""+ randomParameter +" returned and object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.name !== undefined, ""+ randomParameter +" are => name: "+ row.name);
      }
    });
  });  
});