import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//dict
let dict = '{a:1, b:2, c:3}';

//argument passed to the axon function
let parameter = dict;

let po = new axonPO({"testQuery": "vals("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The vals() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "vals("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "vals("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(testData[0].val === 1 
        && testData[1].val === 2 
        && testData[2].val === 3, 
        "vals("+ parameter +") query results contain invalid data on row "+ i +" => val: "+ row.val);
      }
    });
  });  
});