import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "marker()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The marker() query',() => { 
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "marker() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "marker() returned an object which doesn't contain an error.");

      ok(testData[0].val === "✓", "marker() query results contain invalid data: "+ testData[0].val);
    });
  });  
});