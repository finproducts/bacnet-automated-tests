import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//unicode value
let unicodeVal = '0069';

//argument passed to the axon function
let parameter = unicodeVal;

let po = new axonPO({"testQuery": "toChar("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The toChar() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "toChar("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "toChar("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === "E", "toChar("+ parameter +") query result contain invalid data => val: "+ testData[0].val);
    });
  });  
});