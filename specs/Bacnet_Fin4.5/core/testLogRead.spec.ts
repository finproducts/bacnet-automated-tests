import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//generates the current year, month & day
function getCurrentDate() {
  var today = new Date();
  var day:any = today.getDate();
  var month:any = today.getMonth() + 1; 
  var year = today.getFullYear();

  if(day < 10) {
    day = '0'+ day;
  } 
  if(month < 10) {
    month = '0'+ month;
  } 
  var currentYearMonthAndDay = year +"-"+ month +"-"+ day;
  return currentYearMonthAndDay;
};

//argument passed to the axon function
let randomParameter = getCurrentDate();

let po = new axonPO({"testQuery": "logRead("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The logRead() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "logRead("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "logRead("+ randomParameter +") returned an object which doesn't contain an error.");

      if(JSON.stringify(testData) === "[]") {
        ok(JSON.stringify(testData) === "[]", 
        "logRead("+ randomParameter +") query results contain invalid data: No logs were submitted today.");
      }
      else if(JSON.stringify(testData) !== "[]") {
        for(let i = 0; i < testData.length; i++) {
          let row = testData[i];

          ok(row.category !== undefined 
          && row.level !== undefined 
          && row.msg !== undefined 
          && row.ts !== undefined, 
          "logRead("+ randomParameter +") query results contain invalid data on row "+ i +" => category: "+
          row.category +" | level: "+ row.level +" | msg: "+ row.msg +" | ts: "+ row.ts);
        }
      }
    });
  });  
});