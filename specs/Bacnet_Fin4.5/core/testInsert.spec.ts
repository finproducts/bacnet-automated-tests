import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 5; i++) {
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    };
    return randomStr;
};

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomString = createRandomString();
let randomNumber = createRandomNumber();
let newRandomString = createRandomString();
let paramArray = [JSON.stringify(randomString), randomNumber, true, false];

let randomIndex = Math.floor(Math.random() * paramArray.length);

//argument passed to the axon function
let randomParameter= "["+ paramArray +"]" +","+ randomIndex +","+ JSON.stringify(newRandomString);

let po = new axonPO({"testQuery": "insert("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The insert() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "insert("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "insert("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[randomIndex].val === newRandomString, 
      "insert("+ randomParameter +") query results contains invalid data: "+ JSON.stringify(testData) +" - "+
      JSON.stringify(newRandomString) +" is found on index "+ randomIndex);
    });
  });  
});