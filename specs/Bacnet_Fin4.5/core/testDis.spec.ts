import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//random param
let paramArray = ["point"];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let randomParameter = paramArray[randomIndex];

//argument passed to the axon function
let parameter = "read("+ randomParameter +")";

let po = new axonPO({"testQuery": "dis("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The dis() query.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed"', () => {
    po.assert(function(testData, confirmData) {

      //testing dis(randomParameter)
      ok(testData !== undefined 
      && testData !== null, "dis("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "dis("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val.startsWith('City Center') === true, 
      "dis("+ parameter +") query results contains invalid data => val: "+ testData[0].val);
    });
  });  
});