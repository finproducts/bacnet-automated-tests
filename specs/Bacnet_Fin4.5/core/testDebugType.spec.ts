import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {	
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i= 0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = JSON.stringify(createRandomString());

//array of possible arguments
let paramArray = [{q:"isStr("+ randomString +")", msg:"sys::Bool"},
 {q:"readAll(ahu)", msg: "foliod::FolioGrid"}, {q:"now()", msg:"sys::DateTime"}];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let randomParameter = paramArray[randomIndex].q;
let expectedResult = paramArray[randomIndex].msg;

let po = new axonPO({"testQuery": "debugType("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The debugType() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "debugType("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "debugType("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === expectedResult, 
      "debugType("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});