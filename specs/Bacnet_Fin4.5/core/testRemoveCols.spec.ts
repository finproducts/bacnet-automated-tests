import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//1st column name
let colName1 = 'a';

//2nd column name
let colName2 = 'b';

//3rd column name
let colName3 = 'c';

//grid
let grid = "[{"+ colName1 + ":1, "+ colName2 +":2, "+ colName3 +":3}].toGrid";

//argument passed to the axon function
let parameter = grid + ",["+ JSON.stringify(colName1) +","+ JSON.stringify(colName2) +"]";

let po = new axonPO({"testQuery": "removeCols("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The removeCols() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "removeCols("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "removeCols("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0][colName3] === 3 
      && testData[0][colName1] === undefined 
      && testData[0][colName2] === undefined, "removeCols("+ parameter +") query results contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});