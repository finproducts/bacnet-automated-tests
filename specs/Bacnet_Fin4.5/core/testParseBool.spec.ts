import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of eligible parameters
let parameterArray = [{q:'"false"', r:false}, {q:'"true"', r:true}];

//randomize
let randomIndex = Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex].q;

//the boolean value of expectedEvalResult variable for each eligible parameters within the array 
let expectedEvalResult = parameterArray[randomIndex].r;

let po = new axonPO({"testQuery": "parseBool("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The parseBool() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "parseBool("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "parseBool("+ randomParameter +") returned and object which doesn't contain an error.");

      ok(testData[0].val === expectedEvalResult, 
      "parseBool("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
    });
  });  
});