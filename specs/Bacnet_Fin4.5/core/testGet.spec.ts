import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
  var randomStr = "";
  var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i = 0; i < 5; i++ )
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString1 = createRandomString();
let randomString2 = createRandomString();
let randomString3 = createRandomString();

let stringArray = [randomString1, randomString2, randomString3];

//array passed as argument
let stringArrayStringified = "["+ [JSON.stringify(randomString1), JSON.stringify(randomString2),
JSON.stringify(randomString3)] +"]";

//random index passed as argument
let randomIndex = Math.floor(Math.random() * stringArray.length);

let po = new axonPO({"testQuery": ""+ stringArrayStringified + ".get("+ randomIndex +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The get() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, ""+ stringArrayStringified +".get("+ randomIndex +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", ""+ stringArrayStringified + ".get("+
      randomIndex +") returned an object which doesn't contain an error.");

      ok(testData[0].val === stringArray[randomIndex],"" + stringArrayStringified +".get("+
      randomIndex +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});