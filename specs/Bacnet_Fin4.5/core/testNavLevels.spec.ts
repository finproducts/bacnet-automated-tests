import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//level
let level = 'equip';

//argument passed to the axon faction
let parameter = JSON.stringify(level);

let po = new axonPO({"testQuery": "navLevels("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The navLevels()', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "navLevels("+ parameter +") query result is not undefined or null.");
      ok(typeof testData === "object" 
      && testData.type !== "error", "navLevels("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.level === 0 
        && row.id !== undefined 
        && row.dis !== undefined 
        && row.indent !== undefined 
        && row.icon16 !== undefined 
        && row.children === "✓", 
        "navLevels("+ parameter +") results contain invalid data => level: "+ row.level +" | id: "+ row.id +" | dis: "+
        row.dis +" | indent: "+ row.indent +" | icon16: "+ row.icon16 +" | children: "+ row.children);
      }
    });
  });  
});