import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//1st column name
let colName1 = 'a';

//2nd column name
let colName2 = 'b';

//grid
let grid = "[{"+ colName1 + ":1, "+ colName2 +":2}].toGrid";

//new column order inside grid
let colRemoved = 'a';

//argument passed to the axon function
let parameter = grid + "," + JSON.stringify(colRemoved);

let po = new axonPO({"testQuery": "removeCol("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The removeCol() query', () =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "removeCol("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "removeCol("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].b === 2 
      && testData[0].a === undefined, "removeCol("+ parameter +") query results contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});