import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of possible parameters
let paramArray = ['{geoCountry:"US"}', '{geoCountry:"RO"}', "100°F"];

//randomize
let randomIndex = Math.floor(Math.random()  *  paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isMetric("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isMetric() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isMetric("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isMetric("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter === '{geoCountry:"US"}' || randomParameter  ===  "100°F") {
        ok(testData[0].val === false, 
        "isMetric("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
      }
      else {
        ok(testData[0].val === true, 
        "isMetric("+ randomParameter +") query results contain invalid data => val: "+ testData[0].val);
      }
    });
  });  
});