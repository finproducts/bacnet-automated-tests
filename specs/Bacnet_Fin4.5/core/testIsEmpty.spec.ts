import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 10; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString = '"' + createRandomString() + '"';
let dictNotNull = 'read(point)';
let gridNotNull = 'readAll(point)';
let emptyString = '""';

let paramArray = [randomString, dictNotNull, emptyString, gridNotNull];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isEmpty("+ randomParameter +")", "confirmQuery":""});
po.setup(function(instance) {});

describe('The isEmpty() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isEmpty("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "isEmpty("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter !== emptyString) { 
        ok(testData[0].val === false, 
        "isEmpty("+ randomParameter +") query results contain invalid data: "+ testData[0].val);  
      }
      else if(randomParameter === emptyString) {
        ok(testData[0].val === true, 
        "isEmpty("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }     
    });
  });  
});