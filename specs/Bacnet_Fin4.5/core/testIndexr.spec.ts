import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i = 0; i < 5; i++) {
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    }
    return randomStr;
};

let randomString1 = createRandomString();
let randomString2 = createRandomString();
let randomString3 = createRandomString();
let stringAll = randomString1 + randomString2 + randomString3;

//random index
let randomNegativeOffSetIndex = Math.floor(Math.random() * stringAll.length);
let randomParameter = JSON.stringify(stringAll) +","+ JSON.stringify(randomString2) +","+ randomNegativeOffSetIndex;

let po = new axonPO({"testQuery": "indexr("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The index() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "indexr("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "indexr("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomNegativeOffSetIndex >= 5) {
        ok(testData[0].val === 5, "indexr("+ randomParameter +") query results contains invalid data: "+
        randomString2 +" is included in "+ stringAll +" starting from index "+ testData[0].val);
      }     

      else if(randomNegativeOffSetIndex <= 4) { 
        ok(JSON.stringify(testData) === "[{\"val\":null}]", 
        "indexr("+ randomParameter +") query results contains invalid data => Not found on specified index: "+ randomNegativeOffSetIndex);
      }
    });
  });  
});