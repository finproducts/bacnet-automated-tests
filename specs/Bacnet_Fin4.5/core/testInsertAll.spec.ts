import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < 5; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

//function that randomly creates a number
function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomString = createRandomString();
let randomNumber = createRandomNumber();

//array insertion
let strInNewArray = createRandomString();
let numInNewArray1 = createRandomNumber();
let numInNewArray2 = createRandomNumber();
let newArray = numInNewArray1 + "," + JSON.stringify(strInNewArray) + "," + numInNewArray2;

let paramArray = [JSON.stringify(randomString), randomNumber, true, false];

//1st random index
let randomIndex = Math.floor(Math.random() * paramArray.length);

//2nd index from random
let indexTwoFromRandom = randomIndex + 1;

//3rd index from random
let indexThreeFromRandom = indexTwoFromRandom + 1;

//argument passed to the axon function
let randomParameter = "["+ paramArray +"]" +","+ randomIndex +","+ "[" + newArray +"]";

let po = new axonPO({"testQuery": "insertAll("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The insertAll() query.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "insertAll("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "insertAll("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[randomIndex].val === numInNewArray1 
      && testData[indexTwoFromRandom].val === strInNewArray 
      && testData[indexThreeFromRandom].val === numInNewArray2, 
      "insertAll("+ randomParameter +") query results contains invalid data: "+ JSON.stringify(testData));
    });
  });  
});