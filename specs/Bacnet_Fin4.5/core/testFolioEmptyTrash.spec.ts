import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "folioEmptyTrash()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The folioEmptyTrash() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null,'folioEmptyTrash() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", "folioEmptyTrash() returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",'Running folioEmptyTrash()... Trash is empty.');
    });
  });  
});