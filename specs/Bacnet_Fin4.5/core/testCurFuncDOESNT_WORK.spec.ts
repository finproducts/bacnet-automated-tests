import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';







let po = new axonPO({"\"testQuery\"":"\"curFunc()\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testCurFuncDOESNT_WORK.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing curFunc"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'curFunc() query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'curFunc() returned an object which doesn\'t contain an error.');

ok(testData[0].ext  ===  "finStackCore"  &&  testData[0].func  ===  "✓"  &&  testData[0].name  ===  "secureEval","curFunc() query results contains valid data => ext: "  +  testData[0].ext  +  " | func: "  +  testData[0].func  +  " | name: "  +  testData[0].name);
}
);
  });  

});