import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import { ok, equal, notEqual } from "../../../helpers/nwapis";
import { waitService } from "../../../helpers/wait-service";
import axonPO from "../../../page_objects/finstack/axon.page";

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
  var randomStr = "";
  var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 5; i++)
    randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
  return randomStr;
}

let randomString = createRandomString();

function createRandomNumber() {
  var min = 0;
  var max = 100;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
}

let randomNumber1 = createRandomNumber();
let randomNumber2 = createRandomNumber();

//argument passed to the axon function
let randomParameter = "["+ randomNumber1 + "," + JSON.stringify(randomString) + "," + randomNumber2 +"].concat";

let po = new axonPO({"testQuery": randomParameter, "confirmQuery": ""});
po.setup(function(instance) {});

describe("The concat() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, randomParameter +" query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error", 
      randomParameter + " returned an object which doesn't contain an error.");

      ok(testData[0].val === randomNumber1 + randomString + randomNumber2, randomParameter +
      "query results contains valid data: "+ testData[0].val
      );
    });
  });
});
