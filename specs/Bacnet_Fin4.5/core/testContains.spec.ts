import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
	
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString1 = createRandomString();
let randomString2 = createRandomString();
let stringNotContained = createRandomString();
let randomString = randomString1 + randomString2;

//argument passed to the axon function
let paramArray = [randomString1, randomString2, stringNotContained];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let randomParameter = JSON.stringify(randomString) + "," + JSON.stringify(paramArray[randomIndex]);

let po = new axonPO({"testQuery": "contains("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance){});

describe('The contains() query.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "contains("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "contains("+ randomParameter +") returned an object which doesn't contain an error.");

      if(paramArray[randomIndex] === stringNotContained) { 
        ok(testData[0].val === false, "contains("+ randomParameter +") query results contains invalid data: "+ testData[0].val);

      } else if( paramArray[randomIndex] !== stringNotContained) {
          ok(testData[0].val === true, "contains("+ randomParameter +") query results contains valid data: "+ testData[0].val);
      }   
    });
  });  
});