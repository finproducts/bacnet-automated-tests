import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i= 0; i < 10; i++ )
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

function createRandomNumber() {
	var min = -100;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber= createRandomNumber();

//arguments passed to the axon function
let element1 = "["+ JSON.stringify(randomString) + ", " + randomNumber + ", " + true + ", " + false + "]";
let accumulator = createRandomNumber();
let randomParameter = element1 + ", " + accumulator;

let po = new axonPO({"testQuery": "count("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The count() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "count("+ randomParameter +") query result is not undefined or null.");
      ok(typeof testData === "object" 
      && testData.type !== "error", "count("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === (accumulator + 1), 
      "count("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});