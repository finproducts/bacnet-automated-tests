import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of possible parameters
let paramArray = ["1", "2015-05-23"];
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = paramArray[randomIndex];

let po = new axonPO({"testQuery": "isDate("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isDate() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isDate("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isDate("+ randomParameter +") returned an object which doesn't contain an error.");

      if(randomParameter !== "2015-05-23") { 
        ok(testData[0].val  === false, 
        "isDate("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }

      else if(randomParameter === "2015-05-23") { 
        ok(testData[0].val === true, "isDate("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      } 
    });
  });  
});