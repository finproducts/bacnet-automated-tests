import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//array of eligible arguments
let parameterArray= ["readAll(ahu)", "blocks()", "tasks()"];

//randomize
let randomIndex= Math.floor(Math.random() * parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = parameterArray[randomIndex];

let po = new axonPO({"testQuery": "colNames("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testColNames.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('The colNames() query', () => {
    po.assert(function(testData, confirmData) {

      ok(testData !== undefined 
      && testData !== null, "colNames("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "colNames("+ randomParameter +") returned and object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.val !== undefined 
        && typeof row.val === "string", 
        "colNames("+ randomParameter +") query results contains invalid data on row "+ i +" => val: "+ row.val);
      }
    });
  });  
});