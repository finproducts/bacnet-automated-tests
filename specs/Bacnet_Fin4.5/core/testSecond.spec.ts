import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//generates the current year, month and date
let today = new Date();
let hours:any = today.getHours();
let minutes:any = today.getMinutes();
let seconds:any = today.getSeconds();

if(hours < 10) {
  hours = "0" + hours;
}
if(minutes < 10) {
  minutes  =  "0"  +  minutes;
}
if(seconds < 10) {
  seconds = "0" + seconds;
}

//argument passed to the axon function
let currentTime = hours +":"+ minutes +":"+ seconds;

let po = new axonPO({"testQuery": "second("+ currentTime +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The second() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "second("+ currentTime +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "second("+ currentTime +") returned an object which doesn't contain an error.");

      ok(testData[0].val === seconds * 1000, "second("+ currentTime +") query results contain invalid data: "+ testData[0].val);
    });
  });  
});