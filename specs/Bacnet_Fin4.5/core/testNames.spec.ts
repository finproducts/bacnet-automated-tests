import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//keys of dict
let key1 = 'a';
let key2 = 'b';
let key3 = 'c';

//values of the keys
let value1 = '1';
let value2 = '2';
let value3 = '3';

//argument passed to the axon function
let parameter = "{"+ JSON.stringify(key1) + ":" + value1 + "," + JSON.stringify(key2) + ":" +
value2 + "," + JSON.stringify(key3) + ":" + value3 +"}";

let po = new axonPO({"testQuery": "names("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance){});

describe('The names() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "names("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object"
      && testData.type !== "error", "names("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(testData[0].val === "a" 
        && testData[1].val === "b" 
        && testData[2].val === "c", "names("+ parameter +") query results contain invalid data on row "+ i +" => val: "+ row.val);
      }
    });
  });  
});