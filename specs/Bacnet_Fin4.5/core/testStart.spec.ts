import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//generates the current year, month & day
let today = new Date();
let day:any = today.getDate();
let month:any = today.getMonth() + 1;
let year = today.getFullYear();

if(day < 10) {
  day = "0" + day;
}
if(month < 10) {
  month = "0" + month;
}

//current date
let currentDate = year +"-"+ month +"-"+ day;

//time range
let timeRange = 'now()..now()-7year';

//argument passed to the axon function
let parameter= timeRange;

let po = new axonPO({"testQuery": "start("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The start() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "start("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "start("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val.includes(currentDate) === true,
      "start("+ parameter +") query result contain invalid data => val: "+ testData[0].val);
    });
  });  
});