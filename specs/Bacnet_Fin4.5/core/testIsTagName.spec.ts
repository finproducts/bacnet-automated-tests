import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates an upper case alphanumeric string of 5 characters
function createRandomUpperCaseAlphaNumString() {	
    var randomStr = "";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var i = 0; i < 5; i++)
      randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let nonValidString = '""  +  createRandomUpperCaseAlphaNumString()  +  ""';

//function that randomly creates an upper case alphanumeric string of 5 characters
function createRandomValidString() {
	
    var randomStr = "";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for(var i = 0; i < 5; i++)
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let validString = '"' + createRandomValidString() + createRandomUpperCaseAlphaNumString() + '"';

//array of possible parameters
let paramArray = [nonValidString, validString];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= paramArray[randomIndex];

let po = new axonPO({"testQuery": "isTagName("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isTagName() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isTagName("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isTagName("+ randomParameter +") returned an object which doesn't contain an error.");

      if( randomParameter === nonValidString) {
        ok(testData[0].val === false, "isTagName("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
      else if( randomParameter === validString) {
        ok(testData[0].val === true, "isTagName("+ randomParameter +") query results contain invalid data: "+ testData[0].val);
      }
    });
  });  
});