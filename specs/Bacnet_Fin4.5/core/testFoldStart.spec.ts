import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//expected result
let expecRes= '__foldStart__';

let po = new axonPO({"testQuery": "foldStart()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The foldStart() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'foldStart() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", "foldStart() returned an object which doesn't contain an error.");

      ok(testData[0].val === expecRes, "foldStart() query results contains valid data => val: "+ testData[0].val);
}
);
  });  

});