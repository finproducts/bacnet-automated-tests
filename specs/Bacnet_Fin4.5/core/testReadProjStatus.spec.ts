import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "readProjStatus()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The readProjStatus() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'readProjStatus() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", 'readProjStatus() returned an object which doesn\'t contain an error.');

      let row = testData[0];

      ok(row.created !== undefined 
      && row.projDir !== undefined 
      && row.projMeta === "✓" 
      && row.projName === "demo" 
      && row.projStatus !== undefined 
      && row.recCount !== undefined 
      && row.version !== undefined, 
      "readProjStatus() query results contain invalid data => created: "+ row.created +" | projDir: "+ row.projDir +" | projMeta: "+
      row.projMeta +" | projName: "+ row.projName +" | projStatus: "+ row.projStatus +" | recCount:"+
      row.recCount +" | version: "+ row.version);
    });
  }); 
});