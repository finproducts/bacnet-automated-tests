import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "rebuildDocs()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The rebuildDocs()',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'rebuildDocs() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", 'rebuildDocs() returned an object which doesn\'t contain an error.');

      ok(testData[0].val === "ok", "rebuildDocs() query results contains valid data => val: "+ testData[0].val);
    });
  });
});