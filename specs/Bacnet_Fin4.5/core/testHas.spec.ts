import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//array of possible parameters
let paramArray = ["id","testColumnName"];

//randomize
let randomIndex = Math.floor(Math.random() * paramArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter = "readAll(point)," + JSON.stringify(paramArray[randomIndex]);

let po = new axonPO({"testQuery": "has("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The has() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "has("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "has("+ randomParameter +") returned an object which doesn't contain an error.");

      if( paramArray[randomIndex] === "id") {
        ok(testData[0].val === true,
        "has("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }

      else if(paramArray[randomIndex] === "testColumnName") {
        ok(testData[0].val === false, 
        "has("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      } 
    });
  });  
});