import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//generates the current year, month & day
let today = new Date();
let day:any = today.getDate();
let month:any = today.getMonth() + 1;
let year = today.getFullYear();

if(day < 10) {
  day = "0" + day;
}
if(month < 10) {
  month = "0" + month;
}
//argument passed to the axon function
let currentDate = year + "-" + month + "-" + day;

let po = new axonPO({"testQuery": "month("+ currentDate +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The month() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "month("+ currentDate +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "month("+ currentDate +") returned an object which doesn't contain an error.");

      ok(testData[0].val === today.getMonth() + 1, 
      "month("+ currentDate +") query results contain invalid data: "+ testData[0].val);
    });
  });  
});