import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//function that randomly creates a number
function createRandomNumber() {
	var min = 0;
	var max = 127;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

//argument that will be passed to the axon function
let randomParameter = createRandomNumber();

let po = new axonPO({"testQuery": "isAlphaNum("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The isAlphaNum query()',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "isAlphaNum("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "isAlphaNum("+ randomParameter +") returned an object which doesn't contain an error.");

      if((randomParameter >= 48 && randomParameter <= 57) 
      || (randomParameter >= 65 && randomParameter <= 90) 
      || (randomParameter >= 97 && randomParameter <= 122)) {
      
        ok(testData[0].val === true, 
        "isAlphaNum("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }
      else {
        ok(testData[0].val === false, 
        "isAlphaNum("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
      }
    });
  });  
});