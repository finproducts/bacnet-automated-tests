import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "removeMarker()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testRemoveMarker() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, 'removeMarker() query result is not undefined or null.');

      ok(typeof testData === "object" 
      && testData.type !== "error", 'removeMarker() returned an object which doesn\'t contain an error.');

      ok(JSON.stringify(testData) === "[{}]", "removeMarker() query results contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});