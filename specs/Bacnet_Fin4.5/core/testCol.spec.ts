import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//array of eligible arguments
let colArray = ["id", "mod", "disMacro", "siteRef"];
let randomIndex = Math.floor(Math.random() * colArray.length);
let parameter = colArray[randomIndex];

//the random parameter that will be passed to the axon faction
let randomParameter = "readAll(ahu), "+ JSON.stringify(parameter);

let po = new axonPO({"testQuery": "col("+ randomParameter +")", "confirmQuery":""});
po.setup(function(instance) {});

describe('The col() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "col("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "col("+ randomParameter +") returned and object which doesn't contain an error.");

      ok(testData[0].val === (parameter +" [foliod::FolioCol]"),
      "col("+ randomParameter +") query results contains invalid data: "+ testData[0].val);
    });
  });  
});