import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//1st param
let tagArray = ["equip", "ahu"];
let randomIndex = Math.floor(Math.random() * tagArray.length);
let tag = tagArray[randomIndex];

//2nd param
let colArray= ["disMacro", "floorRef", "mod", "navName"];
randomIndex = Math.floor(Math.random() * colArray.length);
let colKeep = colArray[randomIndex];

//argument passed to the axon faction
let randomParameter = "readAll("+ tag +"), ["+ JSON.stringify(colKeep) +"]";

let po = new axonPO({"testQuery": "keepCols("+ randomParameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The keepCols() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "keepCols("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "keepCols("+ randomParameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row[colKeep] !== undefined, 
        "keepCols("+ randomParameter +") query results contain invalid data on row "+ i +" => "+
        colKeep +": "+ row[colKeep]);
      }
    });
  });  
});