import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"finHaystackExcludedTags()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHaystackExcludedTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHaystackExcludedTags() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHaystackExcludedTags() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.learn  ===  "✓"  &&  
row.axSlotPath  ===  "✓"  &&  
row.axType  ===  "✓"  &&  
row.axHistoryRef  ===  "✓"  &&  
row.siteUri  ===  "✓",

"finHaystackExcludedTags() query results contains valid data on row "  +  i  
+  " => learn: "  +  row.learn  
+  " | axSlotPath: "  +  row.axSlotPath  
+  " | axType: "  +  row.axType  
+  " | axHistoryRef: "  +  row.axHistoryRef  
+  " | siteUri: "  +  row.siteUri);
}

}
);
  });  

});