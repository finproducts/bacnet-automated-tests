import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//folioSnapshots()[index]->ts.toStr after folioSnapshots()
//IMPORTANT - Have a snapshot made before the modification to run it as an argument for the function folioRestore(tsBeforeTheChange) to undo the changes and keep the test clean
let timestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

//1st argument
let pointId= 'read(point and siteRef->dis=="Laguna")->id';

//equip tag
let equipTag= 'misc';

//argument passed to the axon function
let parameter= pointId  +  ","  +  equipTag;



let po = new axonPO({"testQuery":"finCopyBoundPoints("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  + ")"});
po.setup(function(instance)
{}
)

describe("The finCopyBoundPoints() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "finCopyBoundPoints("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCopyBoundPoints("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.cur  ===  "✓"  &&  
  row.curStatus  !==  undefined  &&  
  row.curVal  !==  undefined  &&  
  row.disMacro  ===  "$equipRef $navName"  &&  
  row.equipRef  !==  undefined,
  
  "finCopyBoundPoints("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | cur: "  +  
  row.cur  +  " | curStatus: "  +  
  row.curStatus  +  " | curVal: "  +  
  row.curVal  +  " | disMacro: "  +  
  row.disMacro  +  " | equipRef: "  +  
  row.equipRef);
}


//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  timestamp  +  ")... Restored.");
}
);
  });  

});