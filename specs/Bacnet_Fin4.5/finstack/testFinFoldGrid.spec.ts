import {  by,  browser,  element,  ExpectedConditions as EC,  $,  $$} from "protractor";
import { ok, equal, notEqual } from "../../../helpers/nwapis";
import { waitService } from "../../../helpers/wait-service";
import axonPO from "../../../page_objects/finstack/axon.page";


//OK

//function that randomly creates a number
function createRandomNumber() {
  var min = -100;
  var max = 100;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
}

let randomNumber1 = createRandomNumber();
let randomNumber2 = createRandomNumber();
let randomNumber3 = createRandomNumber();

//argument passed to the axon faction
let funcWithRandomParameter = JSON.stringify([{ a: randomNumber1, b: randomNumber2, c: randomNumber3 }]) + ".toGrid.finFoldGrid(null, {useFirst})";

let po = new axonPO({  "testQuery": funcWithRandomParameter,  "confirmQuery": ""});
po.setup(function(instance) {});


describe("The finFoldGrid() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData, confirmData) {
      ok(
        testData !== undefined && testData !== null,
        funcWithRandomParameter + " query result is not undefined or null."
      );

      ok(
        typeof testData === "object" && testData.type !== "error", funcWithRandomParameter +
          " returned an object which doesn't contain an error."
      );

      ok(
        testData[0].result === randomNumber1 + randomNumber2 + randomNumber3, funcWithRandomParameter +
          " query results contains valid data: " + testData[0].result
      );
    });
  });
});
