import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 36000;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber= createRandomNumber();

//area level
let filter= 'area';

let val= randomNumber;

//2nd parameter
let filterExpression= filter  +  ">"  +  val;



let po = new axonPO({"testQuery":"readAll("  +  filter  +  ").finGridFilter("  +  filterExpression  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGridFilter() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "readAll("  +  filter  +  ").finGridFilter("  +  filterExpression  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"readAll("  +  filter  +  ").finGridFilter("  +  filterExpression  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.area  >  val,
  
  "readAll("  +  filter  +  ").finGridFilter("  +  filterExpression  +  ") query results contains valid data on row "  +  i  
  +  " => id: "  +  row.id  
  +  " | dis: "  +  row.dis  
  +  " | area: "  +  row.area);
}

}
);
  });  

});