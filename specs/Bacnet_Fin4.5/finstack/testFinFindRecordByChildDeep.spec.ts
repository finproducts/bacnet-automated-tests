import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

// 1st parameter - filter of an equip
let filterSiteAndFloorArray= ["site", "floor"];
let randomIndex= Math.floor(Math.random()  *  filterSiteAndFloorArray.length);
let filterParent= filterSiteAndFloorArray[randomIndex];

//2nd parameter - filter of a point
let filterPointArray= ["zone", "temp", "air", "sensor"];
let filterPoint= filterPointArray[randomIndex];

//arguments passed to the axon function
let parameters= filterParent  +  ", "  +  filterPoint;



let po = new axonPO({"testQuery":"finFindRecordByChildDeep("  +  parameters  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFindRecordByChildDeep() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFindRecordByChildDeep("  +  parameters  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFindRecordByChildDeep("  +  parameters  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

if( filterParent  ===  "site") 
      {
ok(row.id  !==  undefined  &&  row.dis  !==  undefined  &&  row.mod  !==  undefined,"finFindRecordByChildDeep("  +  parameters  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  row.id  +  " | dis: "  +  row.dis  +  " | geoAddr: "  +  row.geoAddr  +  " | geoCity: "  +  row.geoCity  +  " | geoCountry: "  +  row.geoCountry  +  " | geoPostalCode: "  +  row.geoPostalCode  +  " | geoState: "  +  row.geoState  +  " | mod: "  +  row.mod  +  " | tz: "  +  row.tz);
}

    else 
        if( filterParent  ===  "floor") 
      {
ok(row.id  !==  undefined  &&  row.dis  !==  undefined  &&  row.floor  ===  "✓"  &&  row.mod  !==  undefined  &&  row.siteRef  !==  undefined,"finFindRecordByChildDeep("  +  parameters  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  row.id  +  " | dis: "  +  row.dis  +  " | floor: "  +  row.floor  +  " | mod: "  +  row.mod  +  " | siteRef: "  +  row.siteRef);
}

    
      
}

}
);
  });  

});