import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//point id
let pointId= 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon faction
let parameter= pointId;



let po = new axonPO({"testQuery":"finNavletsOn("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavletsOn() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavletsOn("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavletsOn("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]","finNavletsOn("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});