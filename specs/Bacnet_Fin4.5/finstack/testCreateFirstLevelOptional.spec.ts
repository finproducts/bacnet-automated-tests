import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//folioSnapshots()[index]->ts.toStr after folioSnapshots()
//IMPORTANT - Have a snapshot made before the modification to run it as an argument for the function folioRestore(tsBeforeTheChange) to undo the changes and keep the test clean
let getLatestTimestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 

//level
let firstLevel= "site";

//argument passed to the axon function
let randomParameter= JSON.stringify(firstLevel);



let po = new axonPO({"testQuery":"createFirstLevelOptional("  +  randomParameter  +  "),",confirmQuery:"folioRestore("  +  getLatestTimestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The createFirstLevelOptional() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "createFirstLevelOptional("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"createFirstLevelOptional("  +  randomParameter  +  ") returned an object which doesnt contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  row.mod  !==  undefined  &&  row.siteRef  !==  undefined,"createFirstLevelOptional("  +  randomParameter  +  ") query results contains valid data => id: "  +  row.id  +  " | mod: "  +  row.mod  +  " | siteRef: "  +  row.siteRef);
}


//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  getLatestTimestamp  +  ")... Restored to latest timestamp.");
}
);
  });  

});