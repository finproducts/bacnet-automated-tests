import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK




let po = new axonPO({"testQuery":"finRebuildDocs()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finRebuildDocs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
	ok(testData !== undefined && testData !== null, "finRebuildDocs() query result is not undefined or null.");
	ok(typeof testData === "object" && testData.type !== "error", "finRebuildDocs() returned an object which doesn't contain an error.");
		
	    this.ok(testData[0].val === "ok", 
	    		"finRebuildDocs() query results contains valid data: "+ testData[0].val);
}
);
  });  

});