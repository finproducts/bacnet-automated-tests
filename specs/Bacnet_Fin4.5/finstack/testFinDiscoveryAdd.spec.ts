import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {snapshotTimestampForRestore as timestamp} from '../../../helpers/testingUtils';

// not ok on Fin5 but ok on Fin4.5

//folioSnapshots()[index]->ts.toStr after folioSnapshots()
//IMPORTANT - Have a snapshot made before the modification to run it as an argument for the function folioRestore(tsBeforeTheChange) to unde the changes and keep the test clean
//let timestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")'; 

//record id
let recordId= 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter= "["  +  recordId  +  "]";



let po = new axonPO({"testQuery":"finDiscoveryAdd("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finDiscoveryAdd() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finDiscoveryAdd("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finDiscoveryAdd("  +  parameter  +  ") returned an object which doesn't contain an error.");

let row= testData[0]

ok(row.dis  !==  undefined  &&  row.id  !==  undefined,
  "finDiscoveryAdd("  +  parameter  +  ") query results contains valid data => dis: "  +  
  row.dis  +  " | id: "  +  
  row.id  +  " | cur: "  +  
  row.cur  +  " | curStatus: "  +  
  row.curStatus  +  " | curTrackWrite: "  +  
  row.curTrackWrite  +  " | curVal: "  +  
  row.curVal  +  " | disMacro: "  +  
  row.disMacro  +  " | equipRef: "  +  
  row.equipRef  +  " | floorRef: "  +  
  row.floorRef  +  " | hisFunc: "  +  
  row.hisFunc  +  " | kind: "  +  
  row.kind  +  " | maxVal: "  +  
  row.maxVal  +  " | minVal: "  +  
  row.minVal  +  " | mod: "  +  
  row.mod  +  " | navName: "  +  
  row.navName  +  " | precision: "  +  
  row.precision  +  " | siteRef: "  +  
  row.siteRef  +  " | tz: "  +  
  row.tz  +  " | unit: "  +  
  row.unit  +  " | writeLevel: "  +  
  row.writeLevel);

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  timestamp  +  ")... Changes undone.");
}
);
  });  

});