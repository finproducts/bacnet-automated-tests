import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//ref id of floor 1 under City Center
let refId= 'read(floor and dis=="Floor 1" and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter= refId;



let po = new axonPO({"testQuery":"finNavDrillDown("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavDrillDown() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavDrillDown("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavDrillDown("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  10; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  testData[0].dis  ===  "Floor 1"  &&  
  testData[1].dis.includes('City Center')  ===  true  &&  
  row.dis  !==  undefined  &&  
  row.mod  !==  undefined,
  
  "finNavDrillDown("  +  parameter  +  ") query results contains valid data on row "  +  i  
  +  " => id: "  +  row.id  
  +  " | dis: "  +  row.dis  
  +  " | mod: "  +  row.mod  
  +  " | siteRef: "  +  row.siteRef);
}

}
);
  });  

});