import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//use folioSnapshot to undo the changes

//1st parameter
let level= "equip";
//2nd parameter - City Center AHU-1 CHWV

let pointId= 'read(point and siteRef->dis=="JB Tower")';


//3rd parameter - counter
function createRandomNumber() {

	var min = 1;
	var max = 5;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let getRandomCounter= createRandomNumber();

let counter= "{count:"  +  getRandomCounter  +  "}";

let randomParameter= JSON.stringify(level)  +  ","  +  pointId  +  ","  +  counter;

let folioRestoreParam= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';



let po = new axonPO({"testQuery":"finNavDuplicate("  +  randomParameter  +  ")","confirmQuery":"folioRestore("  +  folioRestoreParam  +  ")"});
po.setup(function(instance)
{}
)

describe("The finNavDuplicate() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavDuplicate("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavDuplicate("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis.includes('JB Tower')  ===  true  && testData.length  ===  getRandomCounter,
"finNavDuplicate("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => dis: "  +  row.dis);
}


//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore...DB builder restored to default.");
}
);
  });  

});