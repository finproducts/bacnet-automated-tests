import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



// OK



let po = new axonPO({"testQuery":"finFuncs()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFuncs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFuncs() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFuncs() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.leght; i++)
{
let row= testData[i]

ok(row.func  ===  "✓"  &&  row.name  !==  undefined,
"finFuncs() query results contains valid data on row "  +  i  
+  " => id: "  + row.id  
+  " | dis: "  +row.dis  
+  " | construct: " +row.construct  
+  " | ext: "  + row.ext  
+  " | mod: "  + row.mod  
+  " | name: "  + row.name
);
}

}
);
  });  

});