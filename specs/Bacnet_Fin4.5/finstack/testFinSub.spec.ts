import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let stringName= "Selenium_"  +  createRandomString();

//argument passed to the axon function
let parameter= JSON.stringify(stringName);



let po = new axonPO({"testQuery":"finSub(finSubStart("  +  parameter  +  ")","confirmQuery":"finShowWatches()"});
po.setup(function(instance)
{}
)

describe("The finSub() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSub(finSubStart("  +  parameter  +  ")) query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSub(finSubStart("  +  parameter  +  ") returned and object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finSub(finSubStart("  +  parameter  +  ")) query results contains valid data: "  +  JSON.stringify(testData));

//running finShowWatches() to check for the new watch created
let i;
let found= false
for(i  =  0; i  <  confirmData.length; i++){
if( confirmData[i].dis  ===  stringName){
found  =  true
}
    
}

ok(found  ===  true,
  "Running finShowWatches()... Confirming that the watch with the debug string "  +  stringName  +  " was successfully created.");
}
);
  });  

});