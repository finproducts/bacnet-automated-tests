import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {randomBinFileRecId} from '../../../helpers/testingUtils';


//OK



//the argument that will be passed to the axon faction
var parameter = randomBinFileRecId() +', "file"';


let po = new axonPO({"testQuery":"finReadBinTag("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finReadBinTag() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finReadBinTag("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finReadBinTag("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  ===  "fan.ioExt.BinHandle@989bb2e [ioExt::BinHandle]",
"finReadBinTag("  +  parameter  +  ") query results contains valid data => val: "  +  testData[0].val);
}
);
  });  

});