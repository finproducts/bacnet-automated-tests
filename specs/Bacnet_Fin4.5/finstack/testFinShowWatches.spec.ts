import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"finShowWatches()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finShowWatches() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finShowWatches() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finShowWatches() returned an object which doesn't contain an error.");

if( JSON.stringify(testData)  ===  "[]") 
      {
ok(JSON.stringify(testData)  ===  "[]","No watches found: "  +  JSON.stringify(testData));
}

    else 
        {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.lastPoll  !==  undefined  &&  
  row.watchCount  !==  undefined,
  
  "finShowWatches() query results contains valid data on row "  +  i  +  " => id: "  +  row.id  
  +  " | dis: "  +  row.dis  
  +  " | lastPoll: "  +  row.lastPoll  
  +  " | watchCount: "  +  row.watchCount);
}

}

      
}
);
  });  

});