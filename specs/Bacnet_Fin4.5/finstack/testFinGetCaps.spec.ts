import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"finGetCaps()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGetCaps() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGetCaps() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGetCaps() returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.caps  !==  undefined  &&  
  row.desc  !==  undefined  &&  
  row.price  !==  undefined  &&  
  row.prodPart  !==  undefined  &&  
  row.product  !==  undefined,
  
  "finGetCaps() query results contains valid data on row "  +  i  
  +  " => caps: "  +  row.caps  
  +  " | desc: "  +  row.desc  
  +  " | price: "  +  row.price  
  +  " | prodPart: "  +  row.prodPart 
   +  " | product: "  +  row.product);
}

}
);
  });  

});