import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {randomDbEquipId} from '../../../helpers/testingUtils';


//OK


let randomParameter= randomDbEquipId();



let po = new axonPO({"testQuery":"finPointSummary2("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finPointSummary2() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finPointSummary2("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finPointSummary2("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finPointSummary2("  +  randomParameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});