import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//site id
let idArray= ["@1a169921-22d9c3b8", "@1a169953-7e57d7a1", "@1a1698bd-5cb5a76e", "@1a169856-608f153d", "@1a16997f-e5e1df3e"];
let randomIdIndex= Math.floor(Math.random()  *  idArray.length);

//time zones
let timezoneArray= ["ACDT", "ADT", "CAT", "GMT", "WST"];
let randomTimezoneIndex= Math.floor(Math.random()  *  timezoneArray.length);

let id= idArray[randomIdIndex];

//argument passed to the axon function
let randomParameter= id  +  ","  +  JSON.stringify(timezoneArray[randomTimezoneIndex]);

//return to UTC
let undoChanges= id  +  ",\"UTC\"";



let po = new axonPO({"testQuery":"finRewriteHisTzJob("  +  randomParameter  +  ")","confirmQuery":"finRewriteHisTzJob("  +  undoChanges  +  ")"});
po.setup(function(instance)
{}
)

describe("The finRewriteHisTzJob() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finRewriteHisTzJob("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finRewriteHisTzJob("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].id  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finSyncHisJob  ===  "✓"  &&  
  testData[0].job  !==  undefined  &&  
  testData[0].mod  !==  undefined  &&  
  testData[0].scheduleAt  !==  undefined,
  
  "finRewriteHisTzJob("  +  randomParameter  +  ") query result contains valid data => id: "  +  testData[0].id  
  +  " | dis: "  +  testData[0].dis  
  +  " | finSyncHisJob: "  +  testData[0].finSyncHisJob  
  +  " | job: "  +  testData[0].job  
  +  " | mod: "  +  testData[0].mod  
  +  " | scheduleAt: "  +  testData[0].scheduleAt);

ok(confirmData  !==  undefined  &&  confirmData  !==  null,
  "finRewriteHisTzJob("  +  undoChanges  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error","Running finRewriteHisTzJob("  +  undoChanges  +  ")...changed to UTC.");
}
);
  });  

});