import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"finsedonaDeviceDiscoveryConfig()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finsedonaDeviceDiscoveryConfig() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finsedonaDeviceDiscoveryConfig() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finsedonaDeviceDiscoveryConfig() returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]","finsedonaDeviceDiscoveryConfig() contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});