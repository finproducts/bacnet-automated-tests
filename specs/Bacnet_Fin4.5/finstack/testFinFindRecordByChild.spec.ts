import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

// 1st parameter - filter of an equip
let filterEquipArray= ["ahu", "equip", "vav", "heating"];
let randomIndex= Math.floor(Math.random()  *  filterEquipArray.length);
let filterEquip= filterEquipArray[randomIndex];

//2nd parameter - filter of a point
let filterPointArray= ["zone", "temp", "air", "sensor"];
//let randomIndex= Math.floor(Math.random()  *  filterPointArray.length);
let filterPoint= filterPointArray[randomIndex];

//arguments passed to the axon function
let parameters= filterEquip  +  ", "  +  filterPoint;



let po = new axonPO({"testQuery":"finFindRecordByChild("  +  parameters  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFindRecordByChild() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFindRecordByChild("  +  parameters  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFindRecordByChild("  +  parameters  +  ") returned an object which doesn\'t contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  && 
  row.disMacro  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.navName  !==  undefined  &&  
  row.siteRef  !==  undefined,
  
  "finFindRecordByChild("  +  parameters  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | mod: "  +  
  row.mod  +  " | navName: "  +  
  row.navName  +  " | siteRef: "  +  
  row.siteRef);
}

}
);
  });  

});