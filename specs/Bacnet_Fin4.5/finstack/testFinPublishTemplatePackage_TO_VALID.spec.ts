import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//name of the pod
let podName= 'backupCityCenter.pod';

//stackhub username & password
let username= 'adrianm@j2inn.com';
let password= '1900Toamna';

//argument passed to the axon function
let parameter= "`"  +  podName  +  "`,"  +  JSON.stringify(username)  +  ","  +  JSON.stringify(password);



let po = new axonPO({"testQuery":"inPublishTemplatePackage("  +  parameter  +  ")","onfirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finPublishTemplatePackage() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finPublishTemplatePackage("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finPublishTemplatePackage("  +  parameter  +  ") returned an object which doesn\'t contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finPublishTemplatePackage("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});