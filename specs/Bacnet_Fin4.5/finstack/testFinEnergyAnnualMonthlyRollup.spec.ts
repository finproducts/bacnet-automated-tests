import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//point id
let pointId= 'read(point and kind=="Number" and siteRef->dis=="City Center")->id';

//folding function
let foldFunc= 'sum';

//argument passed to the axon faction - random point id 
let parameter= pointId  +  ","  +  foldFunc;



let po = new axonPO({"testQuery":"finEnergyAnnualMonthlyRollup("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finEnergyAnnualMonthlyRollup() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finEnergyAnnualMonthlyRollup("  +  parameter  +  ") query result is not undefined or null");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finEnergyAnnualMonthlyRollup("  +  parameter  +  ") returned an object which doesn\'t contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.point  !==  undefined  &&  
  row.jul  !==  undefined  &&  
  row.oct  !==  undefined  &&  
  row.feb  !==  undefined  &&  
  row.apr  !==  undefined  &&  
  row.jun  !==  undefined  &&  
  row.dec  !==  undefined  &&  
  row.may  !==  undefined  &&  
  row.aug  !==  undefined  &&  
  row.nov  !==  undefined  &&  
  row.jan  !==  undefined  &&  
  row.mar  !==  undefined  &&  
  row.sep  !==  undefined,
  
  "finEnergyAnnualMonthlyRollup("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => point: "  +  
  row.point  +  " | jan: "  +  row.jan  
  +  " | feb: "  +  row.feb  
  +  " | mar: "  +  row.mar  
  +  " | apr: "  +  row.apr  
  +  " | may: "  +  row.may  
  +  " | jun: "  +  row.jun  
  +  " | jul: "  +  row.jul  
  +  " | aug: "  +  row.aug  
  +  " | sep: "  +  row.sep 
  +  " | oct: "  +  row.oct  
  +  " | nov: "  +  row.nov  
  +  " | dec: "  +  row.dec);
}

}
);
  });  

});