import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//function that randomly creates a string
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

//equip ref: City Center Vav-14
let equipId= 'read(equip)->id';

//point ref under the equip: Airflow 
let pointId= 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter = equipId +', ['+ pointId +'], ["ahuRef", "hvac", "vav"], '+ JSON.stringify(randomString);



let po = new axonPO({"testQuery":"finEquipPointTagClone(" +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finEquipPointTagClone() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finEquipPointTagClone("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  && testData.type  !==  "error",
"finEquipPointTagClone("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].affected  >=  0,
  "finEquipPointTagClone("  +  parameter  +  ") query results contains valid data => affected: "  +  testData[0].affected);
}
);
  });  

});