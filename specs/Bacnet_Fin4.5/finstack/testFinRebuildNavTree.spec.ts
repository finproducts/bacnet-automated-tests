import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {snapshotTimestampForRestore} from '../../../helpers/testingUtils';


// not ok on Fin5 but ok on Fin4.5


//nav level
let nav= 'floor';

//parent id - City Center AHU-1
let equipId= '@1a169f42-a1d26b60';

//nav tree name
let navTreeName= 'equip';

//argument passed to the axon function
let parameter= JSON.stringify(nav)  +  ","  +  equipId  +  ","  +  JSON.stringify(navTreeName);



let po = new axonPO({"testQuery":"finRebuildNavTree("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  snapshotTimestampForRestore  +  ")"});
po.setup(function(instance)
{}
)

describe("The finRebuildNavTree() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finRebuildNavTree("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finRebuildNavTree("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finRebuildNavTree("  +  parameter  +  ") result contains valid data: "  +  JSON.stringify(testData));

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore("  +  timestamp  +  ")... Restored.");
}
);
  });  

});