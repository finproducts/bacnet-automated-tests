import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//requires a haystack connection
let connectorExtName= "haystack";

//argument passed to the axon function
let parameter= JSON.stringify(connectorExtName);




let po = new axonPO({"testQuery":"finGetConnectorTagSpecs( " +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGetConnectorTagSpecs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGetConnectorTagSpecs("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGetConnectorTagSpecs("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(testData[3].defVal  ===  ("http://host/"+ connectorExtName +"/")  &&  
row.dis  !==  undefined  &&  
row.name  !==  undefined  &&  
row.type  !==  undefined,

"finGetConnectorTagSpecs("  +  parameter  +  ") query results contains valid data on row "  +  i  
+  " => dis: "  +  row.dis  
+  " | name: "  +  row.name  
+  " | type: "  +  row.type);
}

}
);
  });  

});