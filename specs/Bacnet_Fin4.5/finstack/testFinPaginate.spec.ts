import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//random argument for paginating the results
let paramPaginateArray= ["filter", "point", "curVal", "equip", "damper"];
let randomIndex= Math.floor(Math.random()  *  paramPaginateArray.length);
let paramForPaginate= paramPaginateArray[randomIndex];

//function that randomly creates a number
function createRandomNumber() {
	var min = 0;
	var max = 10;
	var getRandomNumber = Math.floor (Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let randomNumber= createRandomNumber();

let randomPaginate= 1  +  ","  +  randomNumber;



let po = new axonPO({"testQuery":"readAll("  +  paramForPaginate  +  ").finPaginate("  +  randomPaginate  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finPaginate() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "readAll("  +  paramForPaginate  +  ").finPaginate("  +  randomPaginate  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"readAll("  +  paramForPaginate  +  ").finPaginate("  +  randomPaginate  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  testData.length  ===  (randomNumber + 1),
  
  "readAll("  +  paramForPaginate  +  ").finPaginate("  +  randomPaginate  +  ") query results contains valid data on row "  +  i  
  +  " => id: "  +  row.id  +  " | dis: "  +  row.dis);
}

}
);
  });  

});