import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of eligible parameters
let paramArray= ["filter", "point", "equip"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomSelect= paramArray[randomIndex];

//the argument  passed to the axon faction
let parameter= "readAll("  +  randomSelect  +  ")";



let po = new axonPO({"testQuery":"finNavRollup("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavRollup() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavRollup("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavRollup("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.numAlarms  >=  0  &&  
  row.numManual  >=  0  &&  
  row.numUnackAlarms  >=  0  &&  
  row.navName  !==  undefined  &&  
  row.siteRef  !==  undefined,
  
  "finNavRollup("  +  parameter  +  ") query results contains valid data on row "  +  i  
  +  " => id: "  +  row.id  
  +  " | disMacro: "  +  row.disMacro  
  +  " | floorRef: "  +  row.floorRef  
  +  " | mod: "  +  row.mod  
  +  " | navName: "  +  row.navName  
  +  " | numAlarms: "  +  row.numAlarms  
  +  " | numManual: "  +  row.numManual  
  +  " | numUnackAlarms: "  +  row.numUnackAlarms  
  +  " | siteRef: "  +  row.siteRef);
}

}
);
  });  

});