import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {nHaystackConnIdGlobalVariable} from '../../../helpers/testingUtils';


// OK


//delete Aquarium
let deleteAquarium= 'builderAppTrash("equip", [readAll(site and dis =="Aquarium")[0]->id], true)';

//the argument that will be passed to the axon faction
let parameter= nHaystackConnIdGlobalVariable;



let po = new axonPO({"testQuery":"deleteAquarium","confirmQuery":"finCreateHaystackDb("  +  parameter  +  ")"});
po.setup(function(instance)
{}
)

describe("The finCreateHaystackDb() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(confirmData  !==  undefined  &&  confirmData  !==  null,
  "finCreateHaystackDb("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"finCreateHaystackDb("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(confirmData[0].siteCount  !==  undefined  &&  
  confirmData[0].pointCount  !==  undefined  &&  
  confirmData[0].equipCount  !==  undefined,
  
  "finCreateHaystackDb("  +  parameter  +  ") query results contains valid data => siteCount: "  +  
  confirmData[0].siteCount  +  " | pointCount: "  +  
  confirmData[0].pointCount  +  " | equipCount: "  +  
  confirmData[0].equipCount);
}
);
  });  

});