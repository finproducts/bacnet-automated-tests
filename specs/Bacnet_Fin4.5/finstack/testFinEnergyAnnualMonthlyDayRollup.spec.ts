import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//point reference
let pointId= 'readAll(point)[1]->id';

//argument passed to the axon function
let parameter= pointId;



let po = new axonPO({"testQuery":"finEnergyAnnualMonthlyDayRollup("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finEnergyAnnualMonthlyDayRollup() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finEnergyAnnualMonthlyDayRollup("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finEnergyAnnualMonthlyDayRollup("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.ts  !==  undefined,
  "finEnergyAnnualMonthlyDayRollup("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => day1: "  +  
  row.day1  +  " | day2: "  +  
  row.day2  +  " | day3: "  +  
  row.day3  +  " | day4: "  +  
  row.day4  +  " | day5: "  +  
  row.day5  +  " | day6: "  +  
  row.day6  +  " | day7: "  +  
  row.day7  +  " | day8: "  +  
  row.day8  +  " | day9: "  +  
  row.day9  +  " | day10: "  +  
  row.day10  +  " | day11: "  +  
  row.day11  +  " | day12: "  +  
  row.day12  +  " | day13: "  +  
  row.day13  +  " | day14: "  +  
  row.day14  +  " | day15: "  +  
  row.day15  +  " | day16: "  +  
  row.day16  +  " | day17: "  +  
  row.day17  +  " | day18: "  +  
  row.day18  +  " | day19: "  +  
  row.day19  +  " | day20: "  +  
  row.day20  +  " | day21: "  +  
  row.day21  +  " | day22: "  +  
  row.day22  +  " | day23: "  +  
  row.day23  +  " | day24: "  +  
  row.day24  +  " | day25: "  +  
  row.day25  +  " | day26: "  +  
  row.day26  +  " | day27: "  +  
  row.day27  +  " | day28: "  +  
  row.day28  +  " | day29: "  +  
  row.day29  +  " | day30: "  +  
  row.day30  +  " | day31: "  +  
  row.day31  +  " | tz: "  +  
  row.tz);
}

}
);
  });  

});