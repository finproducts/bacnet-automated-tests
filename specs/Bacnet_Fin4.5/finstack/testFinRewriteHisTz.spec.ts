import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//site id
let siteId= 'read(site and dis=="City Center")->id';

//time zones
let timezoneArray= ["ACDT", "ADT", "CAT", "GMT", "WST"];
let randomTimezoneIndex= Math.floor(Math.random()  *  timezoneArray.length);
let timeZone= timezoneArray[randomTimezoneIndex];

//the random parameter that will be passed to the axon faction
let randomParameter= siteId  +  ","  +  JSON.stringify(timeZone);



let po = new axonPO({"testQuery":"finRewriteHisTz("  +  randomParameter  +  ")","confirmQuery":siteId.substr(0,33)});
po.setup(function(instance)
{}
)

describe("The finRewriteHisTz() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finRewriteHisTz("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finRewriteHisTz("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finRewriteHisTz("  +  randomParameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].tz  ===  timeZone  &&  
  confirmData[0].dis  ===  "City Center"  &&  
  confirmData[0].site  ===  "✓",siteId.substr(0,33)  +  " query contains valid data => dis: "  
  +  confirmData[0].dis  +  " | tz: "  +  confirmData[0].tz);
}
);
  });  

});