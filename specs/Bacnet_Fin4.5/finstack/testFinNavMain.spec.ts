import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//create navlet function first
let createNavlet= 'null, {"navletOn":"point and kind == \'Number\' ", "func":marker(), "src":"(rec, obj) => do return \'Selenium Test\'  end", "name":"seleniumTestFunc"}, {"add"}';

//argument passed to the axon function: 
let parameter= 'readAll(point and kind == "Number")[0], null';



let po = new axonPO({"testQuery":"diff("  +  createNavlet  +  ").commit()","confirmQuery":"finNavMain("  +  parameter  +  ")"});
po.setup(function(instance)
{}
)

describe("The finNavMain() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "diff("  +  createNavlet  +  ").commit() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"diff("  +  createNavlet  +  ").commit() returned an object which doesn't contain an error.");

ok(testData[0].id  !==  undefined  &&  
  testData[0].mod  !==  undefined  &&  
  testData[0].name  ===  "seleniumTestFunc"  &&  
  testData[0].navletOn  !==  undefined  &&  
  testData[0].src  !==  undefined,
  
  "diff("  +  createNavlet  +  ").commit() query results contains valid data: navlet created successfully.");

ok(confirmData  !==  undefined  &&   confirmData  !==  undefined,
  "finNavMain("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"finNavMain("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].dis  !==  undefined  &&  
  confirmData[0].image  !==  undefined  &&  
  confirmData[0].navlets  !==  undefined,
  
  "finNavMain("  +  parameter  +  ") query results contains valid data => id: "  +  confirmData[0].id  +
    " | dis: "  +  confirmData[0].dis  +  " | image: "  +  confirmData[0].image);
}
);
  });  

});