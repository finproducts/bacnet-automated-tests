import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';
import {nHaystackConnIdGlobalVariable} from '../../../helpers/testingUtils';


//OK



//argument passed to the axon function
let parameter= nHaystackConnIdGlobalVariable;



let po = new axonPO({"testQuery":"finHaystackLearn("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHaystackLearn() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHaystackLearn("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHaystackLearn("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.learn  !==  undefined,
  
  "finHaystackLearn("  +  parameter  +  ") query results contains valid data on row "  +  i  
  +  " => dis: "  +  row.dis  
  +  " | learn: "  +  row.learn);
}

}
);
  });  

});