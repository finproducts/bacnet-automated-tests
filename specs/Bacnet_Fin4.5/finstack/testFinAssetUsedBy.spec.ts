import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//finProject id
let finProjectId= 'readAll(finProject)[0]->id';

//the random parameter that will be passed to the axon faction
let parameter= finProjectId;



let po = new axonPO({"testQuery":"finAssetUsedBy("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finAssetUsedBy() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finAssetUsedBy("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finAssetUsedBy("  +  parameter  +  ") returned an object which doesnt contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finAssetUsedBy("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});