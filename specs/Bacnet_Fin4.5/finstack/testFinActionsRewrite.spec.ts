import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//1st param
let filter= 'actions';

//2nd param 
let oldExprDict= '{dis:"Manual Auto", expr: "pointAuto($self)"}';

//function that randomly creates an alphanumeric string of 10 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomTag= createRandomString();

//3rd param
let newExprDict= randomTag;

//argument  passed to the axon faction
let randomParameter= filter  +  ","  +  oldExprDict  +  ",{"  +  newExprDict  +  "}";



let po = new axonPO({"testQuery":"finActionsRewrite("  +  randomParameter  +  ")",
"confirmQuery":"ioReadZinc(readAll(actions)[0]->actions).find((row)=>row.has("  +  JSON.stringify(newExprDict)  +  "))"});
po.setup(function(instance)
{}
)

describe("The finActionsRewrite() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finActionsRewrite("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finActionsRewrite("  +  randomParameter  +  ") returned an object which doesn\'t contain an error.");

ok(testData[0].affected  !==  undefined,
  "finActionsRewrite("  +  randomParameter  +  ") query results contains valid data => affected: "  +  testData[0].affected);

ok(confirmData[0][newExprDict]  ===  "✓",
"Running ioReadZinc(readAll(actions)[0]->actions).find((row)=>row.has("  +  JSON.stringify(newExprDict)  +  ")) => "  +  newExprDict  +  ": "  +  confirmData[0][newExprDict]  + "New tag has been successfully applied.");
}
);
  });  

});