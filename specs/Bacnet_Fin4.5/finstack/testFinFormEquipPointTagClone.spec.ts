import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of eligible parameters
let parameterArray= ["@1a169f42-a1d26b60","@1a170091-acc019db","@1a169e01-383c29db","@1a17d9b2-68903543","@1a169be4-48a9601f"];

//randomize
let randomIndex= Math.floor(Math.random()  *  parameterArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= parameterArray[randomIndex];



let po = new axonPO({"testQuery":"finFormEquipPointTagClone("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormEquipPointTagClone() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormEquipPointTagClone("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormEquipPointTagClone("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.body  !==  undefined  &&  
  row.cancelButton  !==  undefined  &&  
  row.commitAction  !==  undefined  &&  
  row.commitButton  !==  undefined  &&  
  row.finForm  ===  "✓",
  
  "finFormEquipPointTagClone("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => body: "  +  
  row.body  +  " | cancelButton: "  +  
  row.cancelButton  +  " | commitAction: "  +  
  row.commitAction  +  " | commitButton: "  +  
  row.commitButton  +  " | finForm: "  +  
  row.finForm);
}

}
);
  });  

});