import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let levelParamArray= ["program" ,"alarmTarget" ,"equip"];

//randomize
let randomIndex= Math.floor(Math.random()  *  levelParamArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= JSON.stringify(levelParamArray[randomIndex]);



let po = new axonPO({"testQuery":"finNavLevels("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavLevels() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavLevels("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavLevels("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  row.dis  !==  undefined,
  "finNavLevels("  +  randomParameter  +  ") query results contains valid data on row"  +  i  
  +  " => id: "  +  row.id  
  +  " | area: "  +  row.area  
  +  " | dis: "  +  row.dis  
  +  " | geoAddr: "  +  row.geoAddr  
  +  " | geoCity: "  +  row.geoCity  
  +  " | geoCountry: "  +  row.geoCountry  
  +  " | geoPostalCode: "  +  row.geoPostalCode  
  +  " | geoState: "  +  row.geoState  
  +  " | level: "  +  row.level  
  +  " | mod: "  +  row.mod  
  +  " | tz: "  +  row.tz);
}

}
);
  });  

});