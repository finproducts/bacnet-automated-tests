import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


// not ok on Fin5 but ok on Fin4.5

//run finShowWatches() to see a list of watch id's

//array of eligible parameters
let watchIdArray= ["finShowWatches()[0]->id"];

//randomize
let randomIndex= Math.floor(Math.random()  *  watchIdArray.length);

//the random parameter that will be passed to the axon faction
let randomParameter= watchIdArray[randomIndex];



let po = new axonPO({"testQuery":"finShowPointsInWatch("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finShowPointsInWatch() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finShowPointsInWatch("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finShowPointsInWatch("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.leght; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.cur  ===  "✓"  &&  
  row.curStatus  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.tz  !==  undefined,
  
  "finShowPointsInWatch("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  row.id  
  +  " | dis: "  +  row.dis  
  +  " | cur: "  +  row.cur  
  +  " | curStatus: "  +  row.curStatus  
  +  " | disMacro: "  +  row.disMacro  
  +  " | tz: "  +  row.tz);
}

}
);
  });  

});