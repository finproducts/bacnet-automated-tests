import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates an alphanumeric string of 5 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let stringName= "Selenium_"  +  createRandomString();

//argument passed to the axon function
let parameter= JSON.stringify(stringName);



let po = new axonPO({"testQuery":"finSubStart("  +  parameter  +  ").finSubEnd()","confirmQuery":"finShowWatches()"});
po.setup(function(instance)
{}
)

describe("The finSubEnd() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSubStart("  +  parameter  +  ").finSubEnd() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSubStart("  +  parameter  +  ").finSubEnd() returned and object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finSubStart("  +  parameter  +  ").finSubEnd() query results contains valid data: "  +  JSON.stringify(testData));

let i;

let found= false

for(i  =  0; i  <  confirmData.length; i++)
{
if( confirmData[i].dis  ===  stringName) 
      {
found  =  true
}

    
}


ok(found  ===  true,"Running finShowWatches()... Confirming that the watch with the debug string"   +  stringName  +  " is present.");
}
);
  });  

});