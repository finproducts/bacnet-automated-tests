import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import axonPO from '../../../../page_objects/finstack/axon.page';





let expectedArray= ["APP6527", "70007", "7",
                     "BAC-7301C_000033", "3080225", "75"];

let serialPort= 'com14';

let broadcastAddr= 'FF';

let lowNumber= 0;

let highNumber= 4194383;

let timeout= '25sec';

let localAddr= '1';

let baudRate= 38400;

let parameter= JSON.stringify(serialPort)  +  ","  +  JSON.stringify(broadcastAddr)  +  ","  +  lowNumber  +  ","  +  highNumber  +  ","  +  timeout  +  ","  +  JSON.stringify(localAddr)  +  ","  +  baudRate;



let po = new axonPO({"\"testQuery\"":"\"bacnetMstpDeviceDiscovery(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetMstpDeviceDiscovery.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetMstpDeviceDiscovery"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetMstpDeviceDiscovery("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetMstpDeviceDiscovery("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetDeviceName  !==  undefined  &&  row.deviceId  !==  undefined  &&  row.host  !==  undefined,"bacnetMstpDeviceDiscovery("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetDeviceName : "  +  row.bacnetDeviceName  +  " | deviceId: "  +  row.deviceId  +  " | host: "  +  row.host);

if( row.bacnetDeviceName  ===  "APP6527"  &&  row.deviceId  ===  "70007"  &&  row.host  ===  "7"  ||  row.bacnetDeviceName  ===  "BAC-7301C_000033"  &&  row.deviceId  ===  "3080225"  &&  row.host  ===  "75") 
      {
validArray.push(row.bacnetDeviceName,row.deviceId,row.host);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected model names with their deviceIds: "  +  JSON.stringify(validArray));
}
);
  });  

});