import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= 'APP6527real';

let bacnetCur= 'BO94';

let parameter= 'read(point and bacnetCur=="  +  JSON.stringify(bacnetCur)  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")->id';



let po = new axonPO({"\"testQuery\"":"\"finBacnetPriorityArray(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testFinBacnetPriorityArray_BO94_APP6527real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing finBacnetPriorityArray_BO94_APP6527real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'finBacnetPriorityArray("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'finBacnetPriorityArray("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.level  !==  undefined  &&  row.levelDis  !==  undefined  &&  row.val  !==  undefined  &&  testData[testData.length-1].level  ===  16  &&  testData[testData.length-1].levelDis  ===  "Level 16","finBacnetPriorityArray("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => level: "  +  row.level  +  " | levelDis: "  +  row.levelDis  +  " | val: "  +  row.val  +  "
"  +  "Priority array for "  +  bacnetCur  +  " contains the value "  +  row.val  +  " on "  +  row.levelDis);
}

}
);
  });  

});