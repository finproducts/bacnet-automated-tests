import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["STATUS_FLAGS", "BitList(0 0 1 0)", 
                     "OBJECT_NAME", "ROOM TEMP", 
                     "OBJECT_TYPE", 0, 
                     "OBJECT_IDENTIFIER", "AI4"];

let bacnetConn= 'read(bacnetConn and dis=="APP6527real")';

let bacnetObject= 'AI4';

let parameter= bacnetConn  +  ","  +  JSON.stringify(bacnetObject);



let po = new axonPO({"\"testQuery\"":"\"bacnetReadObject(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetReadObject_AI4_APP6527real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadObject_AI4_APP6527real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetReadObject("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetReadObject("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetValueType  !==  undefined  &&  row.objectType  ===  "ANALOG_INPUT"  &&  row.propertyName  !==  undefined  &&  row.value  !==  undefined,"bacnetReadObject("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetValueType: "  +  row.bacnetValueType  +  " | objectType: "  +  row.objectType  +  " | propertyName: "  +  row.propertyName  +  " | value: "  +  row.value);

if( row.propertyName  ===  "STATUS_FLAGS"  &&  row.value  ===  "BitList(0 0 1 0)"  ||  row.propertyName  ===  "OBJECT_NAME"  &&  row.value  ===  "ROOM TEMP"  ||  row.propertyName  ===  "OBJECT_TYPE"  &&  row.value  ===  0  ||  row.propertyName  ===  "OBJECT_IDENTIFIER"  &&  row.value  ===  "AI4") 
      {
validArray.push(row.propertyName,row.value);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected elements: "  +  JSON.stringify(validArray));
}
);
  });  

});