import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let bacnetConn= 'read(bacnetConn and dis=="APP6527real")';

let objArray= ["AI4", "AO1", "BI10", "BO5"];

let objIndex= Math.floor(Math.random()  *  objArray.length);

let bacnetObject= objArray[objIndex];

let propArray= ["OBJECT_NAME", "OBJECT_TYPE", "OBJECT_IDENTIFIER"];

let propIndex= Math.floor(Math.random()  *  propArray.length);

let objectProperty= propArray[propIndex];

let parameter= bacnetConn  +  ","  +  JSON.stringify(bacnetObject)  +  ","  +  JSON.stringify(objectProperty);



let po = new axonPO({"\"testQuery\"":"\"bacnetReadObjectProperty(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetReadObjectProperty_BACNET_OBJECTS_APP6527real.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadObjectProperty_BACNET_OBJECTS_APP6527real"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetReadObjectProperty("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetReadObjectProperty("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(testData[0].bacnetValueType  !==  undefined  &&  testData[0].objectType  !==  undefined  &&  testData[0].propertyName  ===  objectProperty  &&  testData[0].value  !==  undefined,"bacnetReadObjectProperty("  +  parameter  +  ") query contains valid data => bacnetValueType: "  +  testData[0].bacnetValueType  +  " | objectType: "  +  testData[0].objectType  +  " | propertyName: "  +  testData[0].propertyName  +  " | value: "  +  testData[0].value);
}
);
  });  

});