import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import axonPO from '../../../../page_objects/finstack/axon.page';





let expectedArray= ["1", //Local (doesn\'t have any use in the BACnet setup config for automated testing)
                     "2", //ReadSim
                     "3", //WriteSim
                     "4", //BacnetAddedDevice
                     "21", //BAC-KMC_1_21
                     "60076", //EasyIODevice
                     "169276", //Local connectors device
                       ];

let broadcastAdr= '255.255.255.255:47806';

let bindAdr= '192.168.1.22:47806';

let instanceNrStart= 0;

let instanceNrEnd= 4194303;

let networkNr= '0xffff';

let timeout= '8sec';

let parameter= JSON.stringify(broadcastAdr)  +  ","  +  instanceNrStart  +  ","  +  instanceNrEnd  +  ","  +  networkNr  +  ","  +  timeout  +  ","  +  JSON.stringify(bindAdr);



let po = new axonPO({"\"testQuery\"":"\"bacnetDeviceDiscovery(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testBacnetDeviceDiscovery_forAsusDevice.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetDeviceDiscovery_forAsusDevice"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'bacnetDeviceDiscovery("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'bacnetDeviceDiscovery("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetDeviceName  !==  undefined  &&  row.bacnetLocalAddress  ===  bindAdr  &&  row.deviceId  !==  undefined  &&  row.host.includes(':47806')  ===  false  &&  row.modelName  !==  undefined,"bacnetDeviceDiscovery("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => deviceId: "  +  row.deviceId  +  " | host: "  +  row.host  +  " | modelName: "  +  row.modelName  +  " | objectname: "  +  row.objectName  +  " | bacnetLocalAddress: "  +  row.bacnetLocalAddress  +  " | dnet: "  +  row.dnet  +  " | dAdr: "  +  row.dAdr);

if( row.deviceId  ===  "1"  ||  row.deviceId  ===  "2"  ||  row.deviceId  ===  "3"  ||  row.deviceId  ===  "4"  ||  row.deviceId  ===  "21"  ||  row.deviceId  ===  "60076"  ||  row.deviceId  ===  "169276") 
      {
validArray.push(row.deviceId);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),"Query result contains the expected model names with their deviceIds: "  +  JSON.stringify(validArray));
}
);
  });  

});