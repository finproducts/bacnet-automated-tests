import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
for each type of point: analog, boolean & enum
*/


//bacnet connector: BAC-KMC_1_21
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//Objects tested
let analogObject = "av2";
let booleanObject = "bv2";
let enumObject = "msv2";

//creating random names for all objects
function createRandomString() {

  var randomStr = "";
  var charSequence = "abcdefghijklmnopqrstuvwxyz";
  for (var i = 0; i < 10; i++)
    randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
  return randomStr;
};

let analogObjectName = createRandomString();
let booleanObjectName = createRandomString();
let enumObjectName = createRandomString();

//creating random number for analog object
function createRandomNumber() {

  var min = 1;
  var max = 100;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
};

let analogObjectValue = createRandomNumber();

//selecting a random state for enum object
let arrayEnumVal = [1, 2, 3];

let indexEnum = Math.floor(Math.random() * arrayEnumVal.length);

let enumObjectValue = arrayEnumVal[indexEnum];

let arrayBoolVal = [0, 1];

let indexBool = Math.floor(Math.random() * arrayBoolVal.length);

let booleanObjectValue = arrayBoolVal[indexBool];

let parameter = bacnetConn + ", {" + analogObject + ":{present_value:[" + analogObjectValue + "], object_name:[" + JSON.stringify(analogObjectName) + "]}," + booleanObject + ":{present_value:[" + booleanObjectValue + "], object_name:[" + JSON.stringify(booleanObjectName) + "]}," + enumObject + ":{present_value:[" + enumObjectValue + "], object_name:[" + JSON.stringify(enumObjectName) + "]}}";

let validation = bacnetConn + ", {" + analogObject + ":{present_value, object_name}, " + booleanObject + ":{present_value, object_name}, " + enumObject + ":{present_value, object_name}}";



let po = new axonPO({ "testQuery": "bacnetWritePropertyMultiple(" + parameter + ")", "confirmQuery": "bacnetReadPropertyMultiple(" + validation + ")" });
po.setup(function (instance) { }
)

describe("The bacnetWritePropertyMultiple_BAC-KMC_1_21() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetWritePropertyMultiple(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetWritePropertyMultiple(" + parameter + ") returned an object which doesn't contain an error.");

      let i;

      for (i = 0; i < testData.length; i++) {
        let rowTestData = testData[i]

        ok(rowTestData.bacnetValueType !== undefined &&
          rowTestData.objectId !== undefined &&
          rowTestData.propertyName !== undefined &&
          rowTestData.value !== undefined,

          "bacnetWritePropertyMultiple(" + parameter + ") query results contains valid data on row " + i + " => bacnetValueType: " +
          rowTestData.bacnetValueType + " | objectId: " +
          rowTestData.objectId + " | propertyName: " +
          rowTestData.propertyName + " | value: " +
          rowTestData.value);
      }


      let j;

      for (j = 0; j < confirmData.length; j++) {
        let rowConfirmData = confirmData[j]

        ok(confirmData[0].bacnetValueType === "CharString" &&
          confirmData[0].objectId === analogObject.toUpperCase() &&
          confirmData[0].propertyName === "OBJECT_NAME" &&
          confirmData[0].value === analogObjectName &&

          confirmData[1].bacnetValueType === "Real" &&
          confirmData[1].objectId === analogObject.toUpperCase() &&
          confirmData[1].propertyName === "PRESENT_VALUE" &&
          confirmData[1].value === analogObjectValue &&

          confirmData[2].bacnetValueType === "CharString" &&
          confirmData[2].objectId === enumObject.toUpperCase() &&
          confirmData[2].propertyName === "OBJECT_NAME" &&
          confirmData[2].value === enumObjectName &&

          confirmData[3].bacnetValueType === "UnsignedInteger" &&
          confirmData[3].objectId === enumObject.toUpperCase() &&
          confirmData[3].propertyName === "PRESENT_VALUE" &&
          confirmData[3].value === enumObjectValue &&

          confirmData[4].bacnetValueType === "CharString" &&
          confirmData[4].objectId === booleanObject.toUpperCase() &&
          confirmData[4].propertyName === "OBJECT_NAME" &&
          confirmData[4].value === booleanObjectName &&

          confirmData[5].bacnetValueType === "Enumeration" &&
          confirmData[5].objectId === booleanObject.toUpperCase() &&
          confirmData[5].propertyName === "PRESENT_VALUE" &&
          confirmData[5].value === booleanObjectValue,

          "Confirming... bacnetReadPropertyMultiple(" + validation + ") query contains valid data on row " + j + " => bacnetValueType: " +
          rowConfirmData.bacnetValueType + " | objectId: " +
          rowConfirmData.objectId + " | propertyName: " +
          rowConfirmData.propertyName + " | value: " +
          rowConfirmData.value);
      }

    }
    );
  });

});