import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//OK

/*
 * Point must be in watch!!!
 * BAC-KMC_1_21 connector has COV enabled
 * Use bacnetPing(read(bacnetConn and dis=="BAC-KMC_1_21")) to change the connStatus of the connector to "ok"
 * Point targeted: AV2
 * After re-adding the connector in DB Builder, drag the point in the nav tree again.
*/


//bacnet point targeted
let bacnetPoint = "AV2";

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//new value for object property
function createRandomNumber() {

  var min = 1;
  var max = 100;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
};

let newValue = createRandomNumber();

let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", \"present_value\", " + JSON.stringify(newValue);



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal" });
po.setup(function (instance) { }
)

describe("The testConnAppDetails_COV_AV2_BAC-KMC_1_21_part2() query",async () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed",async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

     await ok(confirmData[0].val === newValue,
        "read(point and bacnetCur==" + JSON.stringify(bacnetPoint) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->curVal query contains valid data => val: " + confirmData[0].val + "" + "COV worked properly.New value is store in curVal.");
    }
    );
  });

});