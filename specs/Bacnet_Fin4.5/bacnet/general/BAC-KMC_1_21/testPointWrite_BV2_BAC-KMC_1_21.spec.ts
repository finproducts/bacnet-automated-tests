import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
  bacnet connector used is "BAC-KMC_1_21" & BV2 point (bacnetCur is the name of the point on the device & is unchangeable) 
  the point must be present in DB Builder
  the function writes the new value of the point on the device
  the point from DB Builder will have the new value stored on the property tags "writeVal" & "writeDef": read(point and bacnetCur=="BV2" and bacnetConnRef->dis=="BAC-KMC_1_21")->writeVal
 */


//bacnet connector dis
let connDis = "BAC-KMC_1_21";

//bacnetCur of the point used
let pointBacnetCur = "BV2";

//new value for object property: PRESENT_VALUE
let paramArray = [true, false];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let newValue = paramArray[randomIndex];

//argument passed to the axon function
let parameter = "read(point and bacnetCur==" + JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id," + newValue + ", 16, \"su\"";

let validation = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")," + JSON.stringify(pointBacnetCur) + ",\"PRESENT_VALUE\"";



let po = new axonPO({ "testQuery": "pointWrite(" + parameter + ")", "confirmQuery": "bacnetReadObjectProperty(" + validation + ")" });
po.setup(function (instance) { }
)

describe("The pointWrite_BV2_BAC-KMC_1_21() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "pointWrite(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "pointWrite(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].val === newValue, "pointWrite(" + parameter + ") query contains valid data => val: " + testData[0].val);

      if (newValue === true) {
        ok(confirmData[0].bacnetValueType === "Enumeration" &&
          confirmData[0].objectType === "BINARY_VALUE" &&
          confirmData[0].propertyName === "PRESENT_VALUE" &&
          confirmData[0].value === 1,

          "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
          confirmData[0].bacnetValueType + " | objectType: " +
          confirmData[0].objectType + " | propertyName: " +
          confirmData[0].propertyName + " | value: " +
          confirmData[0].value);
      }

      else
        if (newValue === false) {
          ok(confirmData[0].bacnetValueType === "Enumeration" &&
            confirmData[0].objectType === "BINARY_VALUE" &&
            confirmData[0].propertyName === "PRESENT_VALUE" &&
            confirmData[0].value === 0,

            "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
            confirmData[0].bacnetValueType + " | objectType: " +
            confirmData[0].objectType + " | propertyName: " +
            confirmData[0].propertyName + " | value: " +
            confirmData[0].value);
        }



    }
    );
  });

});