import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted: "BAC-KMC_1_21"

//BACnet conn read
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//schedule targeted
let bacnetObject = "SCH1";

//schedule
let readScheduleId = "read(bacnetCur==" + JSON.stringify(bacnetObject) + ")->id";

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + readScheduleId;



let po = new axonPO({ "testQuery": "bacnetScheduleRead(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetScheduleRead_SCH1_BAC-KMC_1_21() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetScheduleRead(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetScheduleRead(" + parameter + ") returned an object which doesn't contain an error.");

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.ts !== false && row.val === 1 &&
          testData.length === 11,

          "bacnetScheduleRead(" + parameter + ") query contains valid data on row " + i + " => ts: " +
          row.ts + " | val: " +
          row.val);
      }

    }
    );
  });

});