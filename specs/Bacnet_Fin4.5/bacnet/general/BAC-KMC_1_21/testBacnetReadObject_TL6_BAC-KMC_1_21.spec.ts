import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//bacnet object
let bacnetObject = "TL6";

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);



let po = new axonPO({ "testQuery": "bacnetReadObject(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadObject_TL6_BAC-KMC_1_21() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetReadObject(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadObject(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetValueType !== undefined &&
          row.objectType === "TREND_LOG" &&
          row.propertyName !== undefined,

          "bacnetReadObject(" + parameter + ") query contains valid data on row " + i + " => bacnetValueType: " +
          row.bacnetValueType + " | objectType: " +
          row.objectType + " | propertyName: " +
          row.propertyName + " | value: " +
          row.value);

        if (row.propertyName === "STATUS_FLAGS" &&
          row.value === "BitList(0 0 0 0)" || row.propertyName === "OBJECT_NAME" &&
          row.value === "TL_06" || row.propertyName === "OBJECT_TYPE" &&
          row.value === 20 || row.propertyName === "OBJECT_IDENTIFIER" &&
          row.value === "TL6" || row.propertyName === "LOG_DEVICE_OBJECT_PROPERTY" &&
          row.value === "AV4194303" || row.propertyName === "RECORD_COUNT" &&
          row.value === 1) {
          validArray.push(row.propertyName, row.value);
        }


      }

      ok(validArray.includes("STATUS_FLAGS") === true
        && validArray.includes("BitList(0 0 0 0)") === true
        && validArray.includes("OBJECT_NAME") === true
        && validArray.includes("TL_06") === true
        && validArray.includes("OBJECT_TYPE") === true
        && validArray.includes(20) === true
        && validArray.includes("OBJECT_IDENTIFIER") === true
        && validArray.includes("TL6") === true
        && validArray.includes("LOG_DEVICE_OBJECT_PROPERTY") === true
        && validArray.includes("AV4194303") === true
        && validArray.includes("RECORD_COUNT") === true
        && validArray.includes(1) === true,
        "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});