import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//bacnet object
let bacnetObject = "SCH1";

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);



let po = new axonPO({ "testQuery": "bacnetReadObject(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadObject_SCH1_BAC-KMC_1_21() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetReadObject(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadObject(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetValueType !== undefined &&
          row.objectType === "SCHEDULE" &&
          row.propertyName !== undefined,

          "bacnetReadObject(" + parameter + ") query contains valid data on row " + i + " => bacnetValueType: " +
          row.bacnetValueType + " | objectType: " +
          row.objectType + " | propertyName: " +
          row.propertyName + " | value: " +
          row.value);

        if (row.propertyName === "STATUS_FLAGS" &&
          row.value === "BitList(0 0 0 0)" || row.propertyName === "OBJECT_NAME" &&
          row.value === "SCH_01" || row.propertyName === "OBJECT_TYPE" &&
          row.value === 17 || row.propertyName === "OBJECT_IDENTIFIER" &&
          row.value === "SCH1" || row.propertyName === "PRESENT_VALUE" &&
          row.value !== undefined) {
          validArray.push(row.propertyName, row.value);
        }


      }
      ok(validArray.includes("STATUS_FLAGS") === true
        && validArray.includes("BitList(0 0 0 0)") === true
        && validArray.includes("OBJECT_NAME") === true
        && validArray.includes("SCH_01") === true
        && validArray.includes("OBJECT_TYPE") === true
        && validArray.includes(17) === true
        && validArray.includes("OBJECT_IDENTIFIER") === true
        && validArray.includes("SCH1") === true
        && validArray.includes("PRESENT_VALUE") === true,

        "Query result contains the expected elements: " + JSON.stringify(validArray));


    }
    );
  });

});