import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

// bacnet device targeted: "BAC-KMC_1_21"
let bacnetConn = 'read(bacnetConn and dis=="BAC-KMC_1_21")';

//bacnet object
let bacnetObject = "SCH2";

//bacnet object property
let objectProperty = "object_name";

//new value for object property
function createRandomString() {

  var randomStr = "";
  var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 10; i++)
    randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
  return randomStr;
};

let newValue = createRandomString();

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + JSON.stringify(objectProperty) + "," + JSON.stringify(newValue);

//validation
let validation = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + JSON.stringify(objectProperty);



let po = new axonPO({ "testQuery": "bacnetWriteObjectProperty(" + parameter + ")", "confirmQuery": "bacnetReadObjectProperty(" + validation + ")" });
po.setup(function (instance) { }
)

describe("The bacnetWriteObjectProperty_SCH2_BAC-KMC_1_21() query", () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetWriteObjectProperty(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetWriteObjectProperty(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].status === true,
        "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

      ok(confirmData[0].bacnetValueType !== undefined &&
        confirmData[0].objectType === "SCHEDULE" &&
        confirmData[0].propertyName === objectProperty.toUpperCase() &&
        confirmData[0].value === newValue,

        "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: " +
        confirmData[0].bacnetValueType + " | objectType: " +
        confirmData[0].objectType + " | propertyName: " +
        confirmData[0].propertyName + " | value: " +
        confirmData[0].value);
    }
    );
  });

});