import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

//OK


/*
  bacnet connector used is "BAC-KMC_1_21" & AV2 point (bacnetCur is the name of the point on the device & is unchangeable) 
  the point must be present in DB Builder
  the point should have the value "null" on the level indicated by the "bacnetWriteLevel" tag (default is 16)
 */


//bacnet connector dis
let connDis = "BAC-KMC_1_21";

let pointBacnetCur = "AV2";

let nullValue = null;

let parameter = "read(point and bacnetCur==" + JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id," + nullValue + ", 16, \"su\"";

let validation = "read(point and bacnetCur==" + JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id";



let po = new axonPO({ "testQuery": "pointWrite(" + parameter + ")", "confirmQuery": "finBacnetPriorityArray(" + validation + ")" });
po.setup(function (instance) { }
)

describe("The pointWrite_AV2_NULL_BAC-KMC_1_21() query", async () => {
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "pointWrite(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "pointWrite(" + parameter + ") returned an object which doesn't contain an error.");

     await ok(testData[0].val === nullValue, "pointWrite(" + parameter + ") query contains valid data => val: " + testData[0].val);

      let i;

      for (i = 0; i < confirmData.length; i++) {
               let row = confirmData[i]

       await ok(row.level !== undefined &&
          row.levelDis !== undefined &&
          row.val !== undefined &&
          confirmData[15].level === 16 &&
          confirmData[15].levelDis === "Level 16" &&
          confirmData[15].val === JSON.stringify(null),

          "finBacnetPriorityArray(" + validation + ") query results contains valid data on row " + i + " => level: " +
          row.level + " | levelDis: " +
          row.levelDis + " | val: " +
          row.val);
      }

    }
    );
  });

});