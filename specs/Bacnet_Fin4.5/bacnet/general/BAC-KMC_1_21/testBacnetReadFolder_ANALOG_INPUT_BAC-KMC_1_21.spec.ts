import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';





//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = ["AI1", "AI2", "AI3", "AI4", "AI5", "AI6", "AI7", "AI8"];

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`ANALOG_INPUT`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_ANALOG_INPUT_BAC-KMC_1_21() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
 await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null, 
        "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error", 
      "bacnetLearn("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray = []

let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

       await ok(row.bacnetCur !== undefined && 
          row.dis !== undefined && 
          row.kind === "Number" && 
          row.point === "✓", 
          
          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " + 
          row.bacnetCur + " | dis: " + 
          row.dis + " | is: " + 
          row.kind + " | point: " + 
          row.point);

        if (row.bacnetCur === "AI1" ||
         row.bacnetCur === "AI2" || 
         row.bacnetCur === "AI3" || 
         row.bacnetCur === "AI4" || 
         row.bacnetCur === "AI5" || 
         row.bacnetCur === "AI6" || 
         row.bacnetCur === "AI7" || 
         row.bacnetCur === "AI8") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});