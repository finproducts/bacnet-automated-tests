import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';

//OK


/* General info:
 * 
 * Schedule BAC-KMC_1_21 SCH1 [bool / val:true]
 * Tuesday & Thursday from 8:00 to 10:00
 * 
 * drag a SCHEDULE Object from the connector in DB Builder - this will create the FIN Stack point and bind the object to the point
 * make the point writable and schedulable
 * add a connTuning on the connector that has the tag "writeSchedule"
 * create a schedule and add the point to it
 * 
 * "scheduleHis(read(point and bacnetCur=="scheduleObjectName" and bacnetConnRef->dis=="connName")->id, thisWeek() +1day, {timeline})" displays the schedule as a grid
 * 
 */


//BACnet conn read
let bacnetConnDis = "BAC-KMC_1_21";

//BACnet object
let scheduleObject = "SCH1";

//schedule displayed as a grid with values (Schedule WriteSim SCH3 is the name of the schedule)
let writeSchedule = "scheduleHis(read(point and bacnetCur==" +
  JSON.stringify(scheduleObject) + " and bacnetConnRef->dis==" +
  JSON.stringify(bacnetConnDis) + ")->id, thisWeek() +1day, {timeline})";

//argument passed to the axon function
let parameter = "read(bacnetConn and dis==" +
  JSON.stringify(bacnetConnDis) + ")->id, " +
  JSON.stringify(scheduleObject) + ", " + writeSchedule;

//validation query (should return the same results as the "writeSchedule" grid passed as a 3rd argument)
let validation = "read(bacnetConn and dis==" +
  JSON.stringify(bacnetConnDis) + "), read(bacnetCur==" +
  JSON.stringify(scheduleObject) + ")->id";



let po = new axonPO({ "testQuery": "bacnetScheduleWrite(" + parameter + ")", "confirmQuery": "bacnetScheduleRead(" + validation + ")" });
po.setup(function (instance) { }
)

describe('The bacnetScheduleWrite_SCH1_bool_BAC-KMC_1_21() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "bacnetScheduleWrite(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetScheduleWrite(" + parameter + ") returned an object which doesn't contain an error.");

     await ok(testData[0].val === true,
        "bacnetScheduleWrite(" + parameter + ") query returns valid data: " + testData[0].val);

      let i;

      for (i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]

       await ok(row.ts.includes("2019") === true && row.val === true,
          "bacnetScheduleRead(" + validation + ") query contains valid data on row " + i + " => ts: " + row.ts + " | val: " + row.val);
      }

    }
    );
  });

});