import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//OK

//bacnet device targeted: "BAC-KMC_1_21"

//expected array
let expectedArray = [//"TL1", "TL2", "TL3",
  "TL4", "TL5", "TL6"];

//bacnet conn dis
let connDis = "BAC-KMC_1_21";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`TREND_LOG`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_TREND_LOG_BAC-KMC_1_21() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
   await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

     await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

       await ok(row.bacnetHis !== undefined && 
          row.dis !== undefined && 
          row.kind === "Number" && 
          row.point === "✓", 
          
          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetHis: " + 
          row.bacnetHis + " | dis: " + 
          row.dis + " | is: " + 
          row.kind + " | point: " + 
          row.point);

        if (row.bacnetHis === "TL4" || 
        row.bacnetHis === "TL5" || 
        row.bacnetHis === "TL6") {
          validArray.push(row.bacnetHis);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});