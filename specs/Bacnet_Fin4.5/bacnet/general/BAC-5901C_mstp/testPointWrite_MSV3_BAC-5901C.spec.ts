import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= "BAC-5901C";

let pointBacnetCur= "MSV3";

let paramArray= ["AUTO", "ON"];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let newValue= paramArray[randomIndex];

let parameter = "read(point and bacnetCur==" + JSON.stringify(pointBacnetCur) + " and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")->id," + JSON.stringify(newValue) + ", 16, \"su\"";

let validation = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")," + JSON.stringify(pointBacnetCur) + ",\"PRESENT_VALUE\"";



let po = new axonPO({ "testQuery": "pointWrite(" + parameter + ")", "confirmQuery": "bacnetReadObjectProperty(" + validation + ")" });
po.setup(function (instance) { }
)

describe("testPointWrite_MSV3_BAC-5901C.js",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Testing pointWrite_MSV3_BAC-5901C", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "pointWrite("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"pointWrite("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  ===  newValue,
  "pointWrite("  +  parameter  +  ") query contains valid data => val: "  +  testData[0].val);

if( newValue  ===  "AUTO") 
      {
ok(confirmData[0].bacnetValueType  ===  "UnsignedInteger"  &&  
confirmData[0].objectType  ===  "MULTI_STATE_VALUE"  &&  
confirmData[0].propertyName  ===  "PRESENT_VALUE"  &&  
confirmData[0].value  ===  1,

"bacnetReadObjectProperty("  +  validation  +  ") query contains valid data => bacnetValueType: "  +  
confirmData[0].bacnetValueType  +  " | objectType: "  +  
confirmData[0].objectType  +  " | propertyName: "  +  
confirmData[0].propertyName  +  " | value: "  +  
confirmData[0].value);
}

    else 
        if( newValue  ===  "OFF") 
      {
ok(confirmData[0].bacnetValueType  ===  "UnsignedInteger"  &&  
confirmData[0].objectType  ===  "MULTI_STATE_VALUE"  &&  
confirmData[0].propertyName  ===  "PRESENT_VALUE"  &&  
confirmData[0].value  ===  2,

"bacnetReadObjectProperty("  +  validation  +  ") query contains valid data => bacnetValueType: "  +  
confirmData[0].bacnetValueType  +  " | objectType: "  +  
confirmData[0].objectType  +  " | propertyName: "  +  
confirmData[0].propertyName  +  " | value: "  +  
confirmData[0].value);
}

    
      
}
);
  });  

});