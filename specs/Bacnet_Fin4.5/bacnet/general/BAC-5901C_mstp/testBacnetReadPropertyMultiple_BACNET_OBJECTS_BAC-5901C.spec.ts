import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let expectedArray= ["OBJECT_NAME", "OBJECT_TYPE", "OBJECT_IDENTIFIER"];

let bacnetConn= 'read(bacnetConn and dis=="BAC-5901C")';

let objArray= ["AI1", "AO7", "AV1", "BI7", "BO1", "BV1", "LP1", "MSV1", "SCH1", "TL1"];

let objIndex= Math.floor(Math.random()  *  objArray.length);

let bacnetObject= objArray[objIndex];

let objProps= 'object_name, object_type, object_identifier';

let parameter= bacnetConn  +  ", {"  +  JSON.stringify(bacnetObject)  +  ":{"  +  objProps  +  ", false_property1}},{\"FALSE_OBJECT\":{"  +  objProps  +  ", false_property2}}";



let po = new axonPO({ "testQuery": "bacnetReadPropertyMultiple(" + parameter + ")", "confirmQuery": "" });
po.setup(function(instance)
{}
)

describe('testBacnetReadPropertyMultiple_BACNET_OBJECTS_BAC-5901C.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing bacnetReadPropertyMultiple_BACNET_OBJECTS_BAC-5901C"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,
  "bacnetReadPropertyMultiple("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"bacnetReadPropertyMultiple("  +  parameter  +  ") returned an object which doesn't contain an error.");

let validArray= []

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.bacnetValueType  !==  undefined  &&  
   row.objectId  ===  bacnetObject  &&  
   row.propertyName  !==  undefined  &&  
   row.value  !==  undefined  &&  testData.length  ===  3,
   "bacnetReadPropertyMultiple("  +  parameter  +  ") query contains valid data on row "  +  i  +  " => bacnetValueType: "  +  
   row.bacnetValueType  +  " | objectId: "  +  
   row.objectId  +  " | propertyName: "  +  
   row.propertyName  +  " | value: "  + 
    row.value);

if( row.propertyName  ===  "OBJECT_IDENTIFIER"  ||  
    row.propertyName  ===  "OBJECT_TYPE"  || 
    row.propertyName  ===  "OBJECT_NAME") 
      {
validArray.push(row.propertyName);
}

    
}


ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),
"Query result contains the expected properties: "  +  JSON.stringify(validArray));
}
);
  });  

});