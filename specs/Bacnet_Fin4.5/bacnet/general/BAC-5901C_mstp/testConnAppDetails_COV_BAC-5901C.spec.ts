import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';





let connDis= "BAC-5901C";

let bacnetConn= "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

let parameter = "\"bacnet\", read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";



let po = new axonPO({ "testQuery": "bacnetPing(" + bacnetConn + ")", "confirmQuery": "connAppDetails(" + parameter + ")" });
po.setup(function (instance) { }
)

describe("testConnAppDetails_COV_BAC-5901C.js",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Testing connAppDetails_COV_BAC-5901C", () => {
    po.assert(function(testData,confirmData){
ok(confirmData  !==  undefined  &&  confirmData  !==  null,
   "connAppDetails("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"connAppDetails("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(confirmData[0].val.includes('isCOVEnabled:      true')  ===  true  
&&  confirmData[0].val.includes('supportsCOV:       true')  ===  true 
&&  confirmData[0].val.includes('connPollFreq:  null')  ===  true 
&&  confirmData[0].val.includes("deviceName:        "  +  connDis)  ===  true  
&&  confirmData[0].val.includes('defWriteDelay:     20sec')  ===  true,

"connAppDetails(" + parameter + ") query contains valid data => val: " +
        confirmData[0].val + "" + " " + "" + connDis + " connector has COV enabled.");
}
);
  });  

});