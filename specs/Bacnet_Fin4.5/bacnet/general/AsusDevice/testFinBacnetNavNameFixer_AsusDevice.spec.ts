import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted: "AsusDevice"
let connDis = "AsusDevice";

//filter
let filter = "point and bacnetCur and bacnetConnRef->dis==" + JSON.stringify(connDis);

//property name you want to use as a navName
let propName = 'test';

//argument passed to the axon function
let parameter = JSON.stringify(filter) + "," + JSON.stringify(propName);



let po = new axonPO({ "testQuery": "finBacnetNavNameFixer(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finBacnetNavNameFixer_AsusDevice() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finBacnetNavNameFixer(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finBacnetNavNameFixer(" + parameter + ") returned an object which doesn't contain an error.");

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetCur !== undefined &&
          row.dis.includes(connDis) === true &&
          row.newName === "<bacnetObj does not exist>" &&
          row.oldName !== undefined,

          "finBacnetNavNameFixer(" + parameter + ") query contains valid data => bacnetCur: " +
          row.bacnetCur + " | dis: " +
          row.dis + " | newName: " +
          row.newName + " | oldName: " +
          row.oldName);
      }

    }
    );
  });

});