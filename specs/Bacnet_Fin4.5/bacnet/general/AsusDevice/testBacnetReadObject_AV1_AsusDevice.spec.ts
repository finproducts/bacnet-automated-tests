import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//expected array
let expectedArray =
// [2,"BitList(0 0 0 0)","MAT","OBJECT_NAME","OBJECT_TYPE","STATUS_FLAGS"];
["STATUS_FLAGS", "BitList(0 0 0 0)",
  "OBJECT_NAME", "MAT",
  "OBJECT_TYPE", 2,
  "OBJECT_IDENTIFIER", "AV1"];

  //bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="AsusDevice")';

//bacnet object
let bacnetObject = "AV1";

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);



let po = new axonPO({ "testQuery": "bacnetReadObject(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadObject_AV1_AsusDevice() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
    await po.assert(async function (testData, confirmData) {
     await ok(testData !== undefined && testData !== null,
        "bacnetReadObject(" + parameter + ") query result is not undefined or null.");

      await ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadObject(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

       await ok(row.bacnetValueType !== undefined && row.objectType === "ANALOG_VALUE" && row.propertyName !== undefined, "bacnetReadObject(" + parameter + ") query contains valid data on row " + i + " => bacnetValueType: " + row.bacnetValueType + " | objectType: " + row.objectType + " | propertyName: " + row.propertyName + " | value: " + row.value);

        if (row.propertyName === "STATUS_FLAGS" && 
            row.value === "BitList(0 0 0 0)" || row.propertyName === "OBJECT_NAME" &&
            row.value === "MAT" || row.propertyName === "OBJECT_TYPE" && row.value === 2 || row.propertyName === "OBJECT_IDENTIFIER" && 
            row.value === "AV1") {
          validArray.push(row.propertyName, row.value);
        }


      }
      console.log(validArray.sort());
      console.log(expectedArray.sort());

      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});