import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
drag into DB Builder & path: AsusDevice(site) ==> Floor 1 ==> Equip 1 ==> MAT (bacnetCur AV1) 
curStatus should have the value "ok" from "stale" after running bacnetSyncCur() 
point tags affected: curStatus, curVal, curErr
*/


//bacnet connector dis
let connDis = "AsusDevice";

let parameter = "read(bacnetCur and kind==\"Number\" and bacnetCur==\"AV1\" and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")";



let po = new axonPO({ "testQuery": "bacnetSyncCur(" + parameter + ")", "confirmQuery": parameter });
po.setup(function (instance) { }
)

describe('The bacnetSyncCur_number_AV1_AsusDevice() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetSyncCur(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetSyncCur(" + parameter + ") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "bacnetSyncCur(" + parameter + ") query returns valid data: " + JSON.stringify(testData));

      let row = confirmData[0]

      ok(row.dis === connDis + " Equip 1 MAT" &&
        row.id !== undefined &&
        row.bacnetCur === "AV1" &&
        row.cur === "✓" &&
        row.bacnetConnRefDis === connDis &&
        row.kind === "Number" &&
        row.siteRefDis === connDis &&
        row.is === "ANALOG_VALUE" &&
        row.curStatus === "ok" &&
        row.point === "✓" &&
        row.curErr === undefined &&
        row.writable === "✓" &&
        row.floorRefDis === "Floor 1" &&
        row.navName === "MAT" &&
        row.bacnetWrite === "AV1" &&
        row.equipRefDis.includes(connDis) === true &&
        row.cmd === "✓", parameter +

        " query contains valid data => dis: " +
        row.dis + " | id: " +
        row.id + " | bacnetCur: " +
        row.bacnetCur + " | cur: " +
        row.cur + " | bacnetConnRefDis: " +
        row.bacnetConnRefDis + " | kind: " +
        row.kind + " | siteRefDis: " +
        row.siteRefDis + " | is: " +
        row.is + " | curStatus: " +
        row.curStatus + " | curVal: " +
        row.curVal + " | point: " +
        row.point + " | writable: " +
        row.writable + " | floorRefDis: " +
        row.floorRefDis + " | navName: " +
        row.navName + " | bacnetWrite: " +
        row.bacnetWrite + " | equipRefDis: " +
        row.equipRefDis + " | cmd: " +
        row.cmd);
    }
    );
  });

});