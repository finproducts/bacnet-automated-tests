import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//"AsusDevice" bacnet device
let bacnetConn = 'read(bacnetConn and dis=="AsusDevice")';

//bacnet object
let objArray = ["AV1", "AV4", "BV3", "BV6"];
let objIndex = Math.floor(Math.random() * objArray.length);
let bacnetObject = objArray[objIndex];

//bacnet object property
let propArray = ["OBJECT_NAME", "OBJECT_TYPE", "OBJECT_IDENTIFIER"];
let propIndex = Math.floor(Math.random() * propArray.length);
let objectProperty = propArray[propIndex];

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + JSON.stringify(objectProperty);



let po = new axonPO({ "testQuery": "bacnetReadObjectProperty(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadObjectProperty_BACNET_OBJECTS_AsusDevice() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
   await po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetReadObjectProperty(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadObjectProperty(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].bacnetValueType !== undefined &&
        testData[0].objectType !== undefined &&
        testData[0].propertyName === objectProperty &&
        testData[0].value !== undefined,

        "bacnetReadObjectProperty(" + parameter + ") query contains valid data => bacnetValueType: " +
        testData[0].bacnetValueType + " | objectType: " +
        testData[0].objectType + " | propertyName: " +
        testData[0].propertyName + " | value: " +
        testData[0].value);
    }
    );
  });

});