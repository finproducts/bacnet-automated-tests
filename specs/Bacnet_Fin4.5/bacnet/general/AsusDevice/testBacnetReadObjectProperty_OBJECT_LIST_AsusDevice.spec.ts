import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//expected array
let expectedArray = ["TL5", "BV6", "AV1", "TL2", "BV3", "DEV50047", "TL7", "AV4"];

//bacnet filter
let bacnetConn = 'read(bacnetConn and dis=="AsusDevice")';

//DEV property of the bacnet connector
let dev = "DEV50047";

let parameter = bacnetConn + "," + JSON.stringify(dev) + ", \"OBJECT_LIST\"";



let po = new axonPO({ "testQuery": "bacnetReadObjectProperty(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadObjectProperty_OBJECT_LIST_AsusDevice() query', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', async () => {
   await po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetReadObjectProperty(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetReadObjectProperty(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetValueType === "ObjectId" &&
          row.objectType === "DEVICE" &&
          row.propertyName === "OBJECT_LIST" &&
          row.value !== undefined &&
          testData.length === 8,

          "bacnetReadObjectProperty(" + parameter + ") query results contains correct data => bacnetValueType: " +
          row.bacnetValueType + " | objectType: " +
          row.objectType + " | propertyName: " +
          row.propertyName + " | value: " +
          row.value);

        if (row.value === "TL5" ||
          row.value === "BV6" ||
          row.value === "AV1" ||
          row.value === "TL2" ||
          row.value === "BV3" ||
          row.value === "DEV50047" ||
          row.value === "TL7" ||
          row.value === "AV4") {
          validArray.push(row.value);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});