import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted: "AsusDevice"
let connDis = "AsusDevice";

//bacnet conn id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//selected dicts
let selectedDicts = "[{}]";

//argument passed to the axon function
let parameter = bacnetConnId + "," + selectedDicts;



let po = new axonPO({ "testQuery": "finFormReadWrite(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finFormReadWrite_AsusDevice() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finFormReadWrite(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finFormReadWrite(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined &&
        testData[0].cancelButton === "$cancelButton" &&
        testData[0].commitAction === "backgroundFinFormReadWriteApply($filter,$readWrite)" &&
        testData[0].commitButton === "$applyButton" &&
        testData[0].dis === "Read/Write Bacnet Property" &&
        testData[0].finForm === "✓" &&
        testData[0].helpDoc !== undefined &&
        testData[0].name === "finReadWriteForm",

        "finFormReadWrite(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});