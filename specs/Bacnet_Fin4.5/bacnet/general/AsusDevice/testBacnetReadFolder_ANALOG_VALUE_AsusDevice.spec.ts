import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

//bacnet device targeted: "AsusDevice"

//expected array
let expectedArray = ["AV1", "AV4"];

let connDis = "AsusDevice";

let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

let folder = "`ANALOG_VALUE`";

let parameter = bacnetConn + "," + folder;



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_ANALOG_VALUE_AsusDevice() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
    await po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetCur !== undefined &&
          row.kind === "Number" &&
          row.point === "✓" &&
          row.bacnetWrite !== undefined &&
          row.bacnetWriteLevel !== undefined,

          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " +
          row.bacnetCur + " | dis: " +
          row.dis + " | is: " +
          row.kind + " | point: " +
          row.point + " | bacnetWrite: " +
          row.bacnetWrite + " | bacnetWriteLevel: " +
          row.bacnetWriteLevel);

        if (row.bacnetCur === "AV1" || row.bacnetCur === "AV4") {
          validArray.push(row.bacnetCur);
        }


      }


      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
        "Query result contains the expected elements: " + JSON.stringify(validArray));
    }
    );
  });

});