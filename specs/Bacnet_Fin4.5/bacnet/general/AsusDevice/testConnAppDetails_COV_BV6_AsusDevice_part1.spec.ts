import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../helpers/nwapis';
import { waitService } from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';


//OK

/*
AsusDevice connector has COV enabled 
Use bacnetPing(read(bacnetConn and dis=="AsusDevice")) to change the connStatus of the connector to "ok"
Point targeted: BV6
After reading the connector in DB Builder, drag the point in the nav tree again.
*/


//bacnet point targeted
let bacnetPoint = "BV6";

let connDis = "AsusDevice";

let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

let parameter = "\"bacnet\", read(point and bacnetCur=="+ JSON.stringify(bacnetPoint) +" and bacnetConnRef->dis=="+ JSON.stringify(connDis) +")->id";



let po = new axonPO({ "testQuery": "bacnetPing(" + bacnetConn + ")", "confirmQuery": "connAppDetails(" + parameter + ")" });
po.setup(function (instance) { }
)

describe('The testConnAppDetails_COV_BV6_AsusDevice_part1() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(confirmData !== undefined && confirmData !== null,
        "connAppDetails(" + parameter + ") query result is not undefined or null.");

      ok(typeof confirmData === "object" && confirmData.type !== "error",
        "connAppDetails(" + parameter + ") returned an object which doesn't contain an error.");

      ok(confirmData[0].val.includes("isCov: true") === true
        && confirmData[0].val.includes("bacnetCur:      " + bacnetPoint) === true
        && confirmData[0].val.includes("conn:           " + connDis) === true
        && confirmData[0].val.includes("isPollingDegraded: false") === true
        && confirmData[0].val.includes("watchStatus:  isCov") === true,
        "connAppDetails(" + parameter + ") query contains valid data => val: " + confirmData[0].val
        + "\n" + " " + "\n" + "Point " + bacnetPoint + " from " + connDis + " is in COV.");

    }
    );
  });

});