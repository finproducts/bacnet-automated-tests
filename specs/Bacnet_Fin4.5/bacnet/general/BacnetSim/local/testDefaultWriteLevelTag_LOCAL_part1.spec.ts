import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';





let connDis= "Local";

let defWriteLevel= 5;

let bacnetConnId= "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")->id";

let floorId= "read(floor and siteRef->dis=="  +  JSON.stringify(connDis)  +  ")->id";

let addPoint= "AV1";

let parameter= bacnetConnId  +  ","  +  floorId  +  ", [{bacnetCur:"  +  JSON.stringify(addPoint)  +  "}]";


let po = new axonPO({"testQuery": "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ").toRecList.map(r => diff(r, {defaultWriteLevel:"  +  defWriteLevel  +  "})).commit","confirmQuery": "finBacnetAddPointsApply("  +  parameter  +  ")"});

po.setup(function(instance)
{}
)



describe('testDefaultWriteLevelTag_LOCAL_part1.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing defaultWriteLevelTag part 1"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  
   testData  !==  undefined,
   "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ").toRecList.map(r => diff(r, {defaultWriteLevel:"  +  defWriteLevel  +  "})).commit query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  
   testData.type  !==  "error",
   "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ").toRecList.map(r => diff(r, {defaultWriteLevel:"  +  defWriteLevel  +  "})).commit returned an object which doesn't contain an error.");

let row= testData[0]

ok(row.id  !==  undefined  &&  
  row.bacnetConn  ===  "✓"  &&  
  row.bacnetDeviceName  ===  connDis  &&  
  row.bacnetDeviceStatus  ===  "OPERATIONAL"  &&  
  row.defaultWriteLevel  ===  defWriteLevel &&
 row.dis  ===  connDis  &&  row.uri  ===  "bacnet://10.10.200.241:47806/1?dnet=2&dadr=01000000",
  "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ").toRecList.map(r => diff(r, {defaultWriteLevel:"  + 
   defWriteLevel  +  "})).commit query contains valid data => id: "  +  
   row.id  +  " | bacnetConn: "  +  
   row.bacnetConn  +  " | bacnetDeviceName: "  +  
   row.bacnetDeviceName  +  " | bacnetDeviceStatus: "  +  
   row.bacnetDeviceStatus  +  " | defaultWriteLevel: "  +  
   row.defaultWriteLevel  +  " | dis: "  +  
   row.dis  +  " | uri: "  +  row.uri);


}
);
  });  

});