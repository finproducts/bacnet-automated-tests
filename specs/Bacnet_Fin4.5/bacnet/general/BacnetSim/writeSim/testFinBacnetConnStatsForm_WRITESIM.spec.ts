import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';
import { async } from 'q';


//NOT ok


let connDis = "WriteSim";

let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

let selectedDicts = '[{path: "/192.168.251.30", vendorName: "Siemens Industry Inc., Bldg Tech", port: 47808, description: "Apogee TC16.Floor.AHU", mod: dateTime(2017-12-19, 12:47:04.734, "UTC"), location: "78.Bucharest", bacnetDeviceStatus: "OPERATIONAL", objectName: "AHU01", iconTable: `fan://frescoRes/img/tableUnknown.png`, connTag: "bacnetConn", dis: "AHU01", bacnetVersion: "1.7", disMacro: "$bacnetDeviceName", uri: `bacnet://192.168.251.30/7003`, connStatus: "down", model: "bacnetExt::BacnetModel", connState: "closed", icon24: `http://localhost:85/pod/bacnetExt/res/img/icon24.png`, depth: 0, bacnetConn, bacnetDeviceName: "AHU01", vendorId: 7, scheme: "bacnet", ext: "bacnet", id: @21cbc6c0-921f1471}]';

let parameter = bacnetConnId + "," + selectedDicts;



let po = new axonPO({ "testQuery": "finBacnetConnStatsForm(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The FinBacnetConnStatsForm_WRITESIM', async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"',async () => {
    await po.assert(async function (testData, confirmData) {
      
     await ok(testData !== undefined && testData !== null, 
        "finBacnetConnStatsForm(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && 
      testData.type !== "error", "finBacnetConnStatsForm(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined && 
        testData[0].cancelButton === "$cancelButton" && 
        testData[0].dis === "BACnet Connector Stats" && 
        testData[0].finForm === "✓" && 
        testData[0].name === "bacnetConnStatsView", 
        
        "finBacnetConnStatsForm(" + parameter + ") query result contains valid data => body: " + 
        testData[0].body + " | cancelButton: " + 
        testData[0].cancelButton + " | commitAction: " + 
        testData[0].commitAction + " | commitButton: " + 
        testData[0].commitButton + " | dis: " + 
        testData[0].dis + " | finForm: " + 
        testData[0].finForm + " | helpDoc: " + 
        testData[0].helpDoc + " | name: " + 
        testData[0].name);
    }
    );
  });

});