import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

/*
this test will undo also the changes performed on the test "testFinRenameBacnetConnsCommit_WRITESIM.js"
*/

//connector targeted
let connDis = "WriteSim";

//bacnet connector id
let bacnetConnId = "read(bacnetConn and bacnetDeviceName==" + JSON.stringify(connDis) + ")->id";

//argument passed to the axon function
let parameter = bacnetConnId;



let po = new axonPO({
  "testQuery": "finRenameBacnetConnsForm(" + parameter + ")",
  "confirmQuery": "finRenameBacnetConnsCommit([{newName:\"WriteSim\", id: read(bacnetConn and dis==\"Nameless\")->id}])"
});
po.setup(function (instance) { }
)

describe('The finRenameBacnetConnsForm_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finRenameBacnetConnsForm(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finRenameBacnetConnsForm(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined &&
        testData[0].cancelButton === "$cancelButton" &&
        testData[0].commitAction.includes('finRenameBacnetConnsCommit') === true &&
        testData[0].commitButton === "$applyButton" &&
        testData[0].dis === "Rename Bacnet Connectors" &&
        testData[0].finForm === "✓" &&
        testData[0].helpDoc !== undefined &&
        testData[0].name === "renameBacnetConns",

        "finRenameBacnetConnsForm(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});