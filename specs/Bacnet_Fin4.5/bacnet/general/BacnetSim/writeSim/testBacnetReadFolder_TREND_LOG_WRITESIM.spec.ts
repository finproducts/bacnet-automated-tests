import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


// OK

let expectedArray = ["TL1","TL2", "TL3"];

let bacnetConn = 'read(bacnetConn and dis=="WriteSim")';

let folder = "`TREND_LOG`";

let parameter = bacnetConn + "," + folder;


let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", " confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetReadFolder_TREND_LOG_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null, "bacnetLearn(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" &&
        testData.type !== "error", "bacnetLearn(" + parameter + "), returned an object which doesn't contain an error.");

      let tracker = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]
        ok(row.bacnetHis !== undefined &&
          row.dis !== undefined &&
         // row.kind  === "Number "+"\nBool "+"\nStr"  && 
          row.point === "✓", "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetHis: " +
          row.bacnetHis + " | dis: " +
          row.dis + " | is: " +
         // row.is + " | kind: " +
          row.kind + " | point: " +
          row.point);

        if (row.bacnetHis === "TL1" || row.bacnetHis === "TL2" || row.bacnetHis === "TL3") {
          tracker.push(row.bacnetHis);
        }

      }

      ok(JSON.stringify(tracker.sort()) === JSON.stringify(expectedArray.sort()),
        "Query result contains the expected elements: " + JSON.stringify(tracker.sort()));
    }
    );
  });

});