import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK


let connDis = "WriteSim";

let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";



let po = new axonPO({ "testQuery": "finBacnetClearCache(" + bacnetConn + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finBacnetClearCache() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finBacnetClearCache(" + bacnetConn + ") query result is not undefined or null.");

      ok(typeof testData === "object" &&
        testData.type !== "error",
        "finBacnetClearCache(" + bacnetConn + ") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) ===  "[{\"val\":null}]",
        "finBacnetClearCache(" + bacnetConn + ") query contains valid data: " + JSON.stringify(testData) + ". Cache cleared successfully.");
    }
    );
  });

});