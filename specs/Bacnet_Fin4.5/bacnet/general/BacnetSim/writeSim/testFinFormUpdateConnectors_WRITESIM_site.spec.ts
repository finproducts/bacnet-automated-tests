import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted "WriteSim"
let siteDis = "WriteSim";

//bacnet conn id
let siteConnId = "read(bacnetConn and dis=="  +  JSON.stringify(siteDis)  +  ")->id";

//selected dicts
let selectedDicts = '[{children, dis: "WriteSim", geoState: "AL", mod: dateTime(2017-11-03, 11:54:37.103, "UTC"), geoCountry: "US", level: 0, tz: "Athens", site, icon16: `fan://equipExt/res/img/site.png`, indent: 0, id: @218f157d-bf5f0818, treePath: `equip:/WriteSim`}]';

//argument passed to the axon function
let parameter = siteConnId + "," + selectedDicts;



let po = new axonPO({ "testQuery": "finFormUpdateConnectors(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finFormUpdateConnectors_WRITESIM_site() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finFormUpdateConnectors(parameter)
      ok(testData !== undefined && testData !== null,
        "finFormUpdateConnectors(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finFormUpdateConnectors(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined &&
        testData[0].commitAction.includes("finApplyUpdateConnectors") === true &&
        testData[0].commitButton === "$applyButton" &&
        testData[0].cancelButton === "$cancelButton" &&
        testData[0].dis === "Update Connectors" &&
        testData[0].finForm === "✓",

        "finFormUpdateConnectors(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});