import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

//OK


//bacnet connector targeted: "WriteSim"
let connDis = "WriteSim";

//bacnet conn id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//selected dicts
let selectedDicts = '[{mod: dateTime(2017-11-06, 09:00:19.552, "UTC"), bacnetDeviceStatus: "OPERATIONAL", bacnetLocalAddress: "192.168.1.69:47806", addToList, bacnetConn, iconTable: `fan://frescoRes/img/tableUnknown.png`, connErr: "sys::IOErr: java.net.BindException: Cannot assign requested address: Cannot bind", dis: "WriteSim", depth: 0, folderPath: "/BACnet conn ip: 192.168.1.99/ -- Network 2", disMacro: "$bacnetDeviceName", changeBacnet, bacnetVersion: "1.12", model: "bacnetExt::BacnetModel", connState: "closed", icon24: `http://localhost:85/pod/bacnetExt/res/img/icon24.png`, connStatus: "fault", bacnetDeviceName: "WriteSim", ipDevice, ext: "bacnet", connTag: "bacnetConn", uri: `bacnet://192.168.1.99:47806/3?dnet=2&dadr=03000000`, organize, id: @218f0ef6-d2f045db, path: "/192.168.1.99:47806/2"}]';

//argument passed to the axon function
let parameter = bacnetConnId + "," + selectedDicts;


let po = new axonPO({ "testQuery": "finFormSyncHis(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finFormSyncHis_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finFormSyncHis(parameter)
      ok(testData !== undefined && testData !== null,
        "finFormSyncHis(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finFormSyncHis(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined &&
        testData[0].cancelButton === "$cancelButton" &&
        testData[0].commitAction.includes('finSyncHisPointsJob') === true &&
        testData[0].commitButton === "$applyButton" &&
        testData[0].dis === "Create History Sync Jobs" &&
        testData[0].finForm === "✓" &&
        testData[0].name === "syncHisForm",

        "finFormSyncHis(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});