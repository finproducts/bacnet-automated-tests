import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//site connector targeted: "WriteSim"
let siteDis = "WriteSim";

//bacnet conn id
let siteConnId = '"@" + read(site and dis==' + JSON.stringify(siteDis) + ')->id.toStr';

//argument passed to the axon function
let parameter = siteConnId;



let po = new axonPO({ "testQuery": "finSelectedConnectorTypes(["+ parameter + "])", "confirmQuery": "" });
po.setup(function (instance) { }

)

describe('The finSelectedConnectorTypes_WRITESIM_site() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    
    po.assert(function (testData, confirmData) {
console.log("finSelectedConnectorTypes([@"+ parameter + "])");
      //testing finSelectedConnectorTypes(parameter)
      ok(testData !== undefined && testData !== null,
        "finSelectedConnectorTypes(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finSelectedConnectorTypes(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].type === "bacnet",
        "finSelectedConnectorTypes(" + parameter + ") query result contains valid data => type: " +
        testData[0].type);

        console.log(parameter)
    }
    );
  });

});