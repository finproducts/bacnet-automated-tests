import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


// OK


let bacnetConn = 'read(bacnetConn and dis=="WriteSim")';

let bacnetObject = "SCH2";

let readScheduleId = "read(bacnetCur==" + JSON.stringify(bacnetObject) + ")->id";

let parameter = bacnetConn + "," + JSON.stringify(bacnetObject) + "," + readScheduleId;



let po = new axonPO({ "testQuery": "bacnetScheduleRead(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The bacnetScheduleRead_SCH2_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined &&
        testData !== null, "bacnetScheduleRead(" + parameter + ") query result is not undefined or null.");
    
      ok(typeof testData === "object" &&
        testData.type !== "error", "bacnetScheduleRead(" + parameter + ") returned an object which doesn't contain an error.");

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.ts !== undefined &&
          row.val !== undefined &&
          testData[0].val === 18.5 &&
          testData[1].val === 15.5 &&
          testData[2].val === 18.5 &&
          testData[3].val === 15.5 &&
          testData[4].val === 18.5 &&
          testData[5].val === 15.5 &&
          testData[6].val === 18.5 &&
          testData[7].val === 15.5 &&
          testData[8].val === 18.5 &&
          testData[9].val === 15.5 &&
          testData[10].val === 15.5 &&
          testData[11].val === 15.5, 
          
          "bacnetScheduleRead(" + parameter + ") query returns valid data => ts: " + row.ts + " | val: " + row.val);
      }

    }
    );
  });

});