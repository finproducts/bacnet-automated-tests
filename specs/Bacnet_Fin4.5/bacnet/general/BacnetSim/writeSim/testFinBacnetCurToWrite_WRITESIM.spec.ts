import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//Not OK


let expectedArray = ["AV1", "BV1", "MSV1"];

let connDis = "WriteSim";

let parameter = "readAll(point and bacnetCur and bacnetConnRef->dis==" + JSON.stringify(connDis) + ")";



let po = new axonPO({ "testQuery": "finBacnetCurToWrite(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finBacnetCurToWrite_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null, 
        "finBacnetCurToWrite("  +  parameter  +  ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error", 
      "finBacnetCurToWrite(" + parameter + ") returned an object which doesn't contain an error.");

      if (JSON.stringify(testData) === "[]") {
        ok(JSON.stringify(testData) === "[]", 
        "finBacnetCurToWrite(" + parameter + ") query contains valid data: " + JSON.stringify(testData) + " " + "No bacnet points present in DB Builder.");
      }

      else
        if (JSON.stringify(testData) !== "[]" && testData.type !== "error") {
          let tracker = []

          let i;

          for (i = 0; i < testData.length; i++) {
            let row = testData[i]

            ok(row.dis.includes(connDis) === true && 
            row.id !== undefined && 
            row.bacnetCur !== undefined && 
            row.bacnetWrite !== undefined, 
            
            "finBacnetCurToWrite(" + parameter + ") query results contains valid data on row " + i + " => dis: " + 
            row.dis + " | id: " + 
            row.id + " | bacnetCur: " + 
            row.bacnetCur + " | bacnetWrite: " + 
            row.bacnetWrite + " | bacnetWriteBeforeSet: " + 
            row.bacnetWriteBeforeSet);

            if (row.bacnetCur === "AV1" && 
            row.bacnetWrite === "AV1" && 
            row.bacnetWriteBeforeSet === "AV1" || row.bacnetCur === "BV1" && 
            row.bacnetWrite === "BV1" && 
            row.bacnetWriteBeforeSet === "BV1" || row.bacnetCur === "MSV1" && 
            row.bacnetWrite === "MSV1" && 
            row.bacnetWriteBeforeSet === "MSV1") {
              if (tracker.indexOf(row.bacnetCur) === -1) {
                tracker.push(row.bacnetCur);
              }


            }


          }


          ok(JSON.stringify(tracker.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains points from each category: " + JSON.stringify(tracker.sort()));
        }



    }
    );
  });

});