import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//NOT OK

//bacnet device targeted: "WriteSim"
//point targeted by the trend logs: "AI1"

//expected array
let expectedArray = ["TL1", "TL2", "TL3"];
let expectedArray2 = ["AI1","TL1"];


//bacnet connector dis
let connDis = "WriteSim";

//bacnet conn read
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//bacnet object
let folder = "`TREND_LOG`";

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;

//checking the trend logs to see if they point to the same bacnet point: AI1
let validation = 'bacnetReadPropertyMultiple(read(bacnetConn and dis==' + JSON.stringify(connDis) + '),{"TL1":{log_device_object_property}, "TL2":{log_device_object_property},"TL3":{log_device_object_property}})';



let po = new axonPO({ "testQuery": "bacnetLearn(" + parameter + ")", "confirmQuery": validation });
po.setup(function (instance) { }
)

describe('The SamePointRefTrendLogs_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "bacnetLearn(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "bacnetLearn(" + parameter + ") returned an object which doesn't contain an error.");

      let validArray = []

      let i;

      for (i = 0; i < testData.length; i++) {
        let row = testData[i]

        ok(row.bacnetHis !== undefined &&
          row.dis !== undefined &&
          // row.is === "TREND_LOG" &&
          // row.kind === "Number" &&
          row.point === "✓",

          "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetHis: " +
          row.bacnetHis + " | dis: " +
          row.dis + " | is: " +
          row.is + " | kind: " +
          row.kind + " | point: " +
          row.point);

        if (row.bacnetHis === "TL1" || row.bacnetHis === "TL2" || row.bacnetHis === "TL3") {
          validArray.push(row.bacnetHis);
        }


      }

      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
        "Query result contains the expected elements: " + JSON.stringify(validArray));

      let validArray2 = []

      let j;

      for (j = 0; j < confirmData.length; j++) {
        let row = confirmData[j]

        if (row.objectId === "TL1" && row.value === "AI1" )
          // row.objectId === "TL2" && row.value === "AI1" ||
          // row.objectId === "TL3" && row.value === "AI1") 
          {
          validArray2.push(row.objectId, row.value);
        }


      }

      ok(JSON.stringify(validArray2.sort()) === JSON.stringify(expectedArray2.sort()),
        "Query result contains the expected elements: " + JSON.stringify(validArray2));
    }
    );
  });

});