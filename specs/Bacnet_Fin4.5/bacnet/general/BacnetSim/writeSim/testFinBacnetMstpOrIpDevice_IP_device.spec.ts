import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK


//bacnet connector targeted => "WriteSim"
let bacnetDis = "WriteSim";

//bacnet connector id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(bacnetDis) + ")->id";

//argument passed to the axon function
let parameter = "[" + bacnetConnId + "]";



let po = new axonPO({ "testQuery": "finBacnetMstpOrIpDevice(" + parameter + ")", "confirmQuery": "read(bacnetConn and dis==" + JSON.stringify(bacnetDis) + ")" });
po.setup(function (instance) { }
)

describe('The finBacnetMstpOrIpDevice_IP_device() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finBacnetMstpOrIpDevice(parameter)
      ok(testData !== undefined && testData !== null,
        "finBacnetMstpOrIpDevice(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finBacnetMstpOrIpDevice(" + parameter + ") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
        "finBacnetMstpOrIpDevice(" + parameter + ") query contains valid data: " + JSON.stringify(testData));

      //validation
      ok(confirmData[0].ipDevice === "✓" &&
        confirmData[0].bacnetDeviceName === bacnetDis &&
        confirmData[0].dis === bacnetDis &&
        confirmData[0].uri.includes("bacnet://") === true,

        "read(bacnetConn and dis==" + JSON.stringify(bacnetDis) + ") query contains valid data => ipDevice: " +
        confirmData[0].ipDevice + " | bacnetDeviceName: " +
        confirmData[0].bacnetDeviceName + " | dis: " +
        confirmData[0].dis + " | uri: " +
        confirmData[0].uri + "" + "ipDevice marker tag has been successfully added.");
    }
    );
  });

});