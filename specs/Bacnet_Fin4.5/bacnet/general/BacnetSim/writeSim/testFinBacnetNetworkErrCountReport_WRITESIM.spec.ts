import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//host ip
let hostIp = "192.168.1.99:47806";

//network number
let network = "2";

//bacnet device dis
let bacnetDis = "WriteSim";

//argument passed to the axon function
let parameter = "{host:" + JSON.stringify(hostIp) + ", net:" + JSON.stringify(network) + ", conns:[read(bacnetConn and dis==" + JSON.stringify(bacnetDis) + ")]}";



let po = new axonPO({ "testQuery": "finBacnetNetworkErrCountReport(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finBacnetNetworkErrCountReport_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finBacnetNetworkErrCountReport(parameter)
      ok(testData !== undefined && testData !== null,
        "finBacnetNetworkErrCountReport(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finBacnetNetworkErrCountReport(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].conn === 1 &&
        testData[0].networkNum === network &&
        testData[0].network === (network + hostIp) &&
        testData[0].totalPoints !== undefined &&
        testData[0].errors === 0,

        "finBacnetNetworkErrCountReport(" + parameter + ") query result contains valid data => conn: " +
        testData[0].conn + " | networkNum: " +
        testData[0].networkNum + " | network: " +
        testData[0].network + " | totalPoints: " +
        testData[0].totalPoints + " | errors: " +
        testData[0].errors);
    }
    );
  });

});