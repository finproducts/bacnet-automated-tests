import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//The connector targeted must have "changeBacnet" marker tag on it

//"WriteSim" connector targeted!
let connDis = "WriteSim";

//setting to be changed
let setting = "ip";

//bacnet connector id
let connId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//argument passed to the axon function
let parameter = JSON.stringify(setting) + ", [" + connId + "]";



let po = new axonPO({ "testQuery": "diff(readById(read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id), {changeBacnet}).commit", "confirmQuery": "finFormChangeBacnetCommon(" + parameter + ")" });
po.setup(function (instance) { }
)

describe('The finFormChangeBacnetCommon_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {

      //testing finFormChangeBacnetCommon(parameter)
      ok(confirmData !== undefined && confirmData !== null,
        "finFormChangeBacnetCommon(" + parameter + ") query result is not undefined or null.");

      ok(typeof confirmData === "object" &&
        confirmData.type !== "error", "finFormChangeBacnetCommon(" + parameter + ") returned an object which doesn't contain an error.");

      ok(confirmData[0].body !== undefined && 
        confirmData[0].commitAction.includes("finChangeBacnetCommonCommit") === true && 
        confirmData[0].cancelButton === "$cancelButton" && 
        confirmData[0].commitButton === "$applyButton" && 
        confirmData[0].finForm === "✓" && 
        confirmData[0].dis === "Change BACnet Settings" &&
         confirmData[0].helpDoc !== undefined, 
         
         "finFormChangeBacnetCommon(" + parameter + ") query result contains valid data => commitAction: " + 
         confirmData[0].commitAction + " | cancelButton: " + 
         confirmData[0].cancelButton + " | commitButton: " + 
         confirmData[0].commitButton + " | finForm: " + 
         confirmData[0].finForm + " | name: " + 
         confirmData[0].name + " | body: " + 
         confirmData[0].body + " | helpDoc: " + 
         confirmData[0].helpDoc);
    }
    );
  });

});