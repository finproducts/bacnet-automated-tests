import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';




//bacnet connector targeted "WriteSim"
let connDis = "WriteSim";

//bacnet connector filter
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")";

//argument passed to the axon function
let parameter = bacnetConn;



let po = new axonPO({ "testQuery": "finOrganizeConnectorsForm(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finOrganizeConnectorsForm_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      
      //testing finOrganizeConnectorsForm()
      ok(testData !== undefined && testData !== null,
        "finOrganizeConnectorsForm(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finOrganizeConnectorsForm(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body.includes('IP 10.10.200.99') === true &&
        testData[0].cancelButton === "$cancelButton" &&
        testData[0].commitAction.includes("finOrganizeConnectorsCommit") === true &&
        testData[0].commitButton === "$applyButton" &&
        testData[0].dis === "Organize BACnet Connectors into Folders" &&
        testData[0].finForm === "✓" &&
        testData[0].helpDoc !== undefined &&
        testData[0].name === "organizeConnectorsForm",

        "finOrganizeConnectorsForm(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});