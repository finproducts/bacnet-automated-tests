import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//OK

//bacnet connector targeted: "WriteSim"
let connDis = "WriteSim";

//bacnet connector id
let bacnetConnId = "read(bacnetConn and dis==" + JSON.stringify(connDis) + ")->id";

//argument passed to the axon function
let parameter = bacnetConnId;



let po = new axonPO({ "testQuery": "finFormChangeBacnetSettings(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) { }
)

describe('The finFormChangeBacnetSettings_WRITESIM() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('"Returns correct data when executed"', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
        "finFormChangeBacnetSettings(" + parameter + ") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
        "finFormChangeBacnetSettings(" + parameter + ") returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined &&
        testData[0].cancelButton === "$cancelButton" &&
        testData[0].commitAction.includes('finFormChangeBacnetCommon') === true &&
        testData[0].commitButton === "$applyButton" &&
        testData[0].dis === "Change BACnet Settings" &&
        testData[0].finForm === "✓" &&
        testData[0].helpDoc !== undefined &&
        testData[0].name === "finFormChangeBacnetSettings",

        "finFormChangeBacnetSettings(" + parameter + ") query result contains valid data => body: " +
        testData[0].body + " | cancelButton: " +
        testData[0].cancelButton + " | commitAction: " +
        testData[0].commitAction + " | commitButton: " +
        testData[0].commitButton + " | dis: " +
        testData[0].dis + " | finForm: " +
        testData[0].finForm + " | helpDoc: " +
        testData[0].helpDoc + " | name: " +
        testData[0].name);
    }
    );
  });

});