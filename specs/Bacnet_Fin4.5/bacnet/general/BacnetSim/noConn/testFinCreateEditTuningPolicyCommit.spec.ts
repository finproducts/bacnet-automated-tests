import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//conn tuning name generator
function createRandomString() {
	
  var randomStr = "";
  var charSequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for(var i = 0; i < 10; i++)
    randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
  return randomStr; 
};

//conn tuning name
let connTuningName= createRandomString();

//polling enable checkbox
let pollEnable= false;

//minimum enable checkbox
let minEnable = false;

//maximum enable checkbox
let maxEnable = false;

//edit enable checkbox
let edit= false;

let minWrite = 5;
let maxWrite = 90;
let poll = '5s';
let schedule = false;

//argument passed to the axon function
let parameter= "{name:"+ JSON.stringify(connTuningName) + ", pollEnab:"+ pollEnable +", minEnab:"+
minEnable +", maxEnab:"+ maxEnable +", edit:"+ edit +", minWrite:"+ minWrite +", maxWrite:"+
maxWrite +", poll:"+ JSON.stringify(poll) +", schedule:"+ schedule +"}, 0";

let po = new axonPO({"testQuery": "finCreateEditTuningPolicyCommit("+ parameter +")",
"confirmQuery": "read(connTuning and dis=="+ JSON.stringify(connTuningName) +")"});
po.setup(function(instance) {});

describe('The finCreateEditTuningPolicyCommit() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finCreateEditTuningPolicyCommit("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finCreateEditTuningPolicyCommit("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === 1,
      "finCreateEditTuningPolicyCommit("+ parameter +") query contains valid data => val: "+ testData[0].val);

      ok(confirmData[0].id !== undefined 
      && confirmData[0].dis === connTuningName 
      && confirmData[0].connTuning === "✓" 
      && confirmData[0].mod !== undefined,
      "read(connTuning and dis=="+ JSON.stringify(connTuningName) +") query contains valid data => id: "+
      confirmData[0].id +" | dis: "+ confirmData[0].dis +" | connTuning: "+ confirmData[0].connTuning +" | mod: "+
      confirmData[0].mod);
    });
  });  
});