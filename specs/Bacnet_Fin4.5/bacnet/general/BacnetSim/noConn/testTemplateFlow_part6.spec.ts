import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



/*
 * Applying Bacnet TemplateOn from connector AHU01
 * 
 */

//connector name: AHU01 (hardcoded)
let connName = 'AHU01';

//filter expression
let parameter = "bacnetConn and dis=="+ JSON.stringify(connName);

let po = new axonPO({"testQuery": "finFormBacnetTemplateOnApply("+ JSON.stringify(parameter) +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finFormBacnetTemplateOnApply() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  //testing finFormBacnetTemplateOnApply(JSON.stringify(parameter))
  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormBacnetTemplateOnApply("+ JSON.stringify(parameter) +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "finFormBacnetTemplateOnApply("+ JSON.stringify(parameter) +") returned an object which doesn't contain an error.");

      ok(testData[0].val === null, 
      "finFormBacnetTemplateOnApply("+ JSON.stringify(parameter) +") query result contains invalid data: "+ testData[0].val);
    });
  });  
});