import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "finFormBacnetImport()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinFormBacnetImport() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  //testing finFormBacnetImport()
  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormBacnetImport() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finFormBacnetImport() returned an object which doesn't contain an error.");

      ok(testData[0].body !== undefined 
      && testData[0].cancelButton === "$cancelButton" 
      && testData[0].commitAction === "if(isList(null)) finFormBacnetImport(1, null) else finFormBacnetImport(1, $filt)" 
      && testData[0].commitButton === "$applyButton" 
      && testData[0].dis === "Add Bacnet Temp Points" 
      && testData[0].finForm === "✓" 
      && testData[0].name === "finFromBacnetImport",
      "finFormBacnetImport() query result doesn't contain valid data => body: "+
      testData[0].body +" | cancelButton: "+ testData[0].cancelButton +" | commitAction: "+
      testData[0].commitAction +" | commitButton: "+ testData[0].commitButton +" | dis: "+
      testData[0].dis +" | finForm: "+ testData[0].finForm +" | name: "+ testData[0].name);
    });
  });  
});