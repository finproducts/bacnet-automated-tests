import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



let bacnetConnDis = 'BACnet';
let filter = "bacnetConn and (dis==\"" + bacnetConnDis + "\")";
let parameter= JSON.stringify(filter);

let po = new axonPO({"testQuery": "finFormBacnetTemplateOnApply("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The finFormBacnetTemplateOnApply() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined  
      && testData !== null, "finFormBacnetTemplateOnApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "finFormBacnetTemplateOnApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"val":null}]', 
      "finFormBacnetTemplateOnApply("+ parameter +") query result contains invalid data: "+ JSON.stringify(testData));
    });
  });  
});