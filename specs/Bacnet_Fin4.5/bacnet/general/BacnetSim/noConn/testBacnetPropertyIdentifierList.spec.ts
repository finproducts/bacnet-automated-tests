import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let po = new axonPO({"testQuery": "bacnetPropertyIdentifierList()", "confirmQuery": ""});
po.setup(function(instance){});

describe('The bacnetPropertyIdentifierList() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "bacnetPropertyIdentifierList() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "bacnetPropertyIdentifierList() returned an object which doesn't contain an error.");

      let i;

      for(i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.name !== undefined
        && row.value !== undefined 
        && testData.length === 352, 
        "bacnetPropertyIdentifierList() query contains invalid data on row "+ i +" => name: "+
        row.name + " | value: "+ row.value);
      }
    });
  });  
});