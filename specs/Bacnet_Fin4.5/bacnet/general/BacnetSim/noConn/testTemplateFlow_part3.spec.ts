import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * Enabling the .pod
 */

//name of the .pod
let name = 'vav';

//argument passed to the axon function
let parameter = "`" + name + "`.toStr,false,true";

let po = new axonPO({"testQuery": "finSettingsAction("+ parameter +")", "confirmQuery": "exts()"});
po.setup(function(instance) {});

describe('The finSettingsAction() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finSettingsAction("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finSettingsAction("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].dis === null 
      && testData[0].id !== undefined 
      && testData[0].ext === name 
      && testData[0].mod !== undefined, "finSettingsAction("+ parameter +") query result contains invalid data => dis: "+
      testData[0].dis +" | id: "+ testData[0].id +" | ext: "+ testData[0].ext + " | mod: "+ testData[0].mod);


      for(let i = 0; i < confirmData.length; i++) {
        let row = confirmData[i];

        if(row.dis === name 
          && row.enabled === "✓" 
          && row.extStatus === "ok" 
          && row.name === name) {
            console.log(name +".pod has been successfully enabled.");
        } 
      }
    });
  });  
});