import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

/*
 it refers to the default values of the fields from the Device Discovery UI panel
 defaultValue | dis | (maxLength) | name
 */

let expectedArray= ["255.255.255.255", "Broadcast Address", 15, "broadcastAddress",
                    "0.0.0.0", "Local Address", 15, "localAddress",
                    0, "Instance Number Start", 4194303, "instanceLow",
                    4194303, "Instance Number End", 4194303, "instanceHi",
                    65535, "Network number", 65535, "networkNo",
                    8000, "Timeout", "discoveryTimeout"];

let filter = 'bacnet';
let parameter = JSON.stringify(filter);

let po = new axonPO({"testQuery": "finGetDiscoveryConfiguration("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinGetDiscoveryConfiguration() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finGetDiscoveryConfiguration("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finGetDiscoveryConfiguration("+ parameter +") returned an object which doesn't contain an error.");

      let tracker= [];

      for(let i = 0; i <  testData.length; i++) {
        let row= testData[i];
        ok(row.dis !== undefined 
        && row.name !== undefined,
        "finGetDiscoveryConfiguration("+ parameter +") query result contains valid data on row "+ i +" => defaultValue: "+
        row.defaultValue +" | dis: "+ row.dis +" | maxLength: "+ row.maxLength +" | name: "+ row.name);

        if( row.defaultValue === "255.255.255.255" 
        && row.dis === "Broadcast Address" 
        && row.maxLength === 15 
        && row.name === "broadcastAddress" || row.defaultValue === "0.0.0.0" 
        && row.dis === "Local Address" 
        && row.maxLength === 15 
        && row.name === "localAddress" || row.defaultValue === 0 
        && row.dis === "Instance Number Start" 
        && row.maxLength === 4194303 
        && row.name === "instanceLow" || row.defaultValue === 4194303 
        && row.dis === "Instance Number End" 
        && row.maxLength === 4194303 
        && row.name === "instanceHi" || row.defaultValue === 65535 
        && row.dis === "Network number" 
        && row.maxLength === 65535 
        && row.name === "networkNo" || row.defaultValue === 8000 
        && row.dis === "Timeout" 
        && row.name === "discoveryTimeout") {
          tracker.push(row.defaultValue,row.dis,row.name);

          if( row.maxLength  !==  undefined) {
            tracker.push(row.maxLength);
          }
        }    
      }
      ok(JSON.stringify(tracker.sort()) === JSON.stringify(expectedArray.sort()), 
      "Query result contains the expected values: "+ JSON.stringify(tracker.sort()));
    });
  });  
});