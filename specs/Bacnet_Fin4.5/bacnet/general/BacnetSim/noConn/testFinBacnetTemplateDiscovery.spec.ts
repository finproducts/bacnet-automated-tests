import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//address //addport
let broadcastAdress= '255.255.255.255:47806';

//argument passed to the axon function
let parameter= JSON.stringify(broadcastAdress);

let po = new axonPO({"testQuery": "finBacnetTemplateDiscovery("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finBacnetTemplateDiscovery() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finBacnetTemplateDiscovery("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "finBacnetTemplateDiscovery("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.deviceId !== undefined 
        && row.host !== undefined 
        && row.scheme === "bacnet" 
        //&& row.device === "✓" 
        && row.bacnetDeviceName !== undefined,
       // && row.id !== undefined
        "finBacnetTemplateDiscovery("+ parameter +") query contains valid data on row "+ i +" => id: "+
        row.id +" | dNet: "+ row.dNet +" | dAdr: "+ row.dAdr +" | hopCount: "+ row.hopCount +" | device: "+
        row.device +" | scheme: "+ row.scheme +" | host: "+ row.host +" | deviceId: "+
        row.deviceId +" | device: "+ row.device +" | bacnetDeviceName: "+ row.bacnetDeviceName +" | id: "+ row.id);
      }
    });
  });  
});