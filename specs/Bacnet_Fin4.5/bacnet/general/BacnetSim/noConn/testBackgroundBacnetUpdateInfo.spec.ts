import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

//tags
let filter = 'point and bacnetCur';

//argument passed to the axon function
let parameter = JSON.stringify(filter);

let po = new axonPO({"testQuery": "backgroundBacnetUpdateInfo("+ parameter +")", "confirmQuery": "jobStatusAll()"});
po.setup(function(instance) {});

describe('The testBackgroundBacnetUpdateInfo() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined  && testData !== null, "backgroundBacnetUpdateInfo("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object"  
      && testData.type !== "error",
      "backgroundBacnetUpdateInfo("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined, 
      "backgroundBacnetUpdateInfo("+ parameter +") query result contains valid data => val: "+  testData[0].val);

      let row = confirmData.length - 1;

      ok(confirmData[row].handle === testData[0].val 
      && confirmData[row].jobStatus === "doneOk" 
      && confirmData[row].dis === "finFormUpdateBacnetTemplateInfo..." 
      && confirmData[row].progressMsg === "Complete", 
      "jobStatusAll() query results contains valid data on backgroundBacnetUpdateInfo => handle: "+
      confirmData[row].handle +" | jobStatus: "+ confirmData[row].jobStatus +" | dis: "+
      confirmData[row].dis +" | progress: "+ confirmData[row].progress +" | progressMsg: "+
      confirmData[row].progressMsg +" | started: "+ confirmData[row].started +" | runtime: "+ confirmData[row].runtime);
    });
  });  
});