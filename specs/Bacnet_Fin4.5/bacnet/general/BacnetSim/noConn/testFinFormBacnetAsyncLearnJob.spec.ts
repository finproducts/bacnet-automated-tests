import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';




//array of possible arguments
let paramArray = ["", "bacnetConn"];
let randomIndex = Math.floor(Math.random() * paramArray.length);
let selectedParam = paramArray[randomIndex];

//argument passed to the axon function
let randomParameter = JSON.stringify(selectedParam);

let po = new axonPO({"testQuery": "finFormBacnetAsyncLearnJob("+ randomParameter +")", "confirmQuery": "jobStatusAll()"});
po.setup(function(instance) {});

describe('The testFinFormBacnetAsyncLearnJob() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormBacnetAsyncLearnJob("+ randomParameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "finFormBacnetAsyncLearnJob("+ randomParameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val !== undefined, 
      "finFormBacnetAsyncLearnJob("+ randomParameter +") contains invalid data => val: "+ testData[0].val);

      let rowIndex, i, row;
      let found = false;
     
      for(i = 0; i < confirmData.length; i++) {
        row = confirmData[i];
        if(row.handle === testData[0].val 
        && row.jobStatus !== undefined 
        && row.dis.includes('finFormBacnetAsyncLearnJob') === true 
        && row.progressMsg !== undefined) { 
          found = true;
          rowIndex = i;
        }
      }
      equal(found, true, "1...Running jobStatusAll()... finFormBacnetAsyncLearnJob job was successfully found on row "+
      i +" => handle: "+ row.handle +" | jobStatus: "+ row.jobStatus +" | dis: "+ row.dis +" | progress: "+
      row.progress +" | progressMsg: "+ row.progressMsg +" | started: "+ row.started +" | runtime: "+ row.runtime);
    });
  });  
});