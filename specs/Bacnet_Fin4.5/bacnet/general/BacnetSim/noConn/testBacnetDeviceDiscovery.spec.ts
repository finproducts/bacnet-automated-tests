import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

/*
 * run several times for the entire list with devices to be correctly generated 
 * check the deviceId of the discovered devices to check if any modifications occurred in the discovery list
 */

let expectedArray = [ '50047', '50048', '1', '21', '1', '2', '3', '4' ]
/*["1", //Local (doesn't have any use in the BACnet setup config for automated testing)
                    "2", //ReadSim
                    "3", //WriteSim
                    "4", //BacnetAddedDevice
                    "21", //BAC-KMC_1_21
                    //"3080225", //BAC-7301C_000033 
                    //"70007", //APP6527 
                    // "60076", //EasyIODevice
                    "50047", //AsusDevice
                    "50048", //AsusDevicePoll

                    ];*/


/*
 broadcast address (255.255.255.255 is the universal broadcast address)
 47808 is the universal BACnet port
 internal BACnet port is 47806
*/
let broadcastAdr= '255.255.255.255:47806';

/*
 bind address is the bacnetLocalAddress (property tag) that will be added on the connector (localhost + bacnet port used)
 0.0.0.0:bacnetPort is the universal address for multiple IPs
 using localhost:bacnetPort & 0.0.0.0:sameBacnetPort could trigger issues - use the same bacnetLocalAddress for all your connectors
 */
let bindAdr = '10.10.200.225:47806';

//instance number start
let instanceNrStart = 0;

//instance number end
let instanceNrEnd = 4194303;

//network number (hexadecimal)
let networkNr = '0xffff';

//timeout
let timeout = '8sec';

//argument passed to the axon function
let parameter = JSON.stringify(broadcastAdr) +  "," + instanceNrStart + "," + instanceNrEnd +
"," + networkNr + "," + timeout + "," + JSON.stringify(bindAdr);

let po = new axonPO({"testQuery": "bacnetDeviceDiscovery("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The bacnetDeviceDiscovery() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== null, "bacnetDeviceDiscovery("+ parameter +") query result is undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error",
      "bacnetDeviceDiscovery("+ parameter +") returned an object which doesn't contain an error.");

      let validArray = [];
      
      console.log(parameter);
      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.bacnetDeviceName !== undefined 
        && row.bacnetLocalAddress === bindAdr
        && row.deviceId !== undefined
        && row.host.includes(':47806') === true
        && row.modelName !== undefined, "bacnetDeviceDiscovery("+ parameter +") query contains valid data on row "+ 
        i +" => deviceId: "+ row.deviceId +" | host: "+ row.host +" | modelName: "+ row.modelName +" | objectname: "+
        row.objectName +" | bacnetLocalAddress: "+ row.bacnetLocalAddress +" | dnet: "+ row.dnet +" | dAdr: "+ row.dAdr);

        if( row.deviceId === "1" 
        || row.deviceId === "2" 
        || row.deviceId === "3" 
        || row.deviceId === "4"
        || row.deviceId === "21"
        || row.deviceId === "3080225"
        || row.deviceId === "50047"
        || row.deviceId === "50048"
        || row.deviceId === "70007"
        || row.deviceId === "1") {
          validArray.push(row.deviceId);
        }   
      }

      console.log(validArray);
      console.log(expectedArray);
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      

      "Query result doesn't contain the expected model names with their deviceIds: "+ JSON.stringify(validArray));
    });
  });  
});