import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//argument passed to the axon function
let parameter = 'read(bacnetConn)->id';

let po = new axonPO({"testQuery": "finFormBacnetAsyncLearnJob2("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinFormBacnetAsyncLearnJob2() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormBacnetAsyncLearnJob2("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finFormBacnetAsyncLearnJob2("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].size !== undefined 
      && typeof testData[0].size === "number", 
      "finFormBacnetAsyncLearnJob2("+ parameter +") query result contains invalid data => size: "+ testData[0].size);
    });
  });  
});