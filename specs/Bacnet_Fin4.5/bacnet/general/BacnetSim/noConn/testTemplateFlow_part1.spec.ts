import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * Creating the template package into a .pod
 */

//path to the .pod
let path = '`io/vavTemplate/`';

//name of the .pod
let name = 'vav';

//version
let version = '1.0';

//argument passed to the axon function
let parameter = path + ", {name:"+ JSON.stringify(name) + ", version:"+ JSON.stringify(version) +"}";


let po = new axonPO({"testQuery": "finCreateTemplatePackage("+ parameter +")",
"confirmQuery": "exts().find((x)=>x->dis=="+ JSON.stringify(name) +")"});
po.setup(function(instance) {});

describe('The finCreateTemplatePackage() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finCreateTemplatePackage("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finCreateTemplatePackage("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].templatePod === name + ".pod", 
      "finCreateTemplatePackage("+ parameter +") query result contains invalid data: "+ testData[0].templatePod);

      ok(confirmData[0].dis === name 
      && confirmData[0].name === name  
      && confirmData[0].version === version, 
      "exts().find((x)=>x->dis=="+ JSON.stringify(name) +") query result contains invalid data => dis: "+
      confirmData[0].dis +" | name: "+ confirmData[0].name +" | version: "+ confirmData[0].version);
    });
  });  
});