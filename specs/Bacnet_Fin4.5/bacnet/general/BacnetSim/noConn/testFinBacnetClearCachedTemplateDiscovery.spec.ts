import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * the function clears the cache created when running finBacnetTemplateDiscovery()
 * after running it, the waiting time for the finBacnetTemplateDiscovery() results should be visibly increased
 */


//address
let broadcastAdress = '255.255.255.255:47806';

//argument passed to the axon function
let parameter = JSON.stringify(broadcastAdress);

let po = new axonPO({"testQuery": "finBacnetClearCachedTemplateDiscovery()", "confirmQuery":
"finBacnetTemplateDiscovery("+ parameter +")"});

po.setup(function(instance) {});

describe('The finBacnetClearCachedTemplateDiscovery() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== undefined, "finBacnetClearCachedTemplateDiscovery() query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finBacnetClearCachedTemplateDiscovery() returned an object which doesn't contain an error.");

      ok(testData[0].val ===  true, "finBacnetClearCachedTemplateDiscovery() query contains valid => val: "+
      testData[0].val +". Cache cleared successfully.");


      console.log(confirmData);
      for(let i = 0; i < confirmData.length; i++) {
        let row = confirmData[i]; 
        ok(row.deviceId !== undefined
          && row.host !== undefined
          && row.scheme === "bacnet"
         //&& row.device === "✓"
         && row.bacnetDeviceName !== undefined,
        //&& row.id !== undefined
        "finBacnetTemplateDiscovery("+ parameter +") query contains valid data on row "+ i +" => id: "+
        row.id +" | dNet: "+ row.dNet +" | dAdr: "+ row.dAdr +" | hopCount: "+ row.hopCount +" | device: "+
        row.device +" | scheme: "+ row.scheme +" | host: "+ row.host +" | deviceId: "+ row.deviceId +" | device: "+
        row.device +" | bacnetDeviceName: "+ row.bacnetDeviceName +" | id: "+ row.id);
      }
    });
  });  
});