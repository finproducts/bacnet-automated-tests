import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//file system node module
let fs = require('fs');

//read the discovery id saved at step 1 (testFinDiscoveryFlowPart1) in finDiscoveryId.txt file 
let txtFile = '.finDiscoveryId.txt';
let discoveryId = fs.readFileSync(txtFile,'utf8');

//connector name
let connName = 'DXR_Radu';

let po = new axonPO({"testQuery": "finDiscoveryStatusCheck("+ discoveryId +")",
"confirmQuery": "finDiscoveryAdd([finDiscoveryStatusCheck("+ discoveryId +").find(row =>row->bacnetDeviceName=="+
JSON.stringify(connName) +")->id])"});

po.setup(function(instance){});

describe('testFinDiscoveryFlowPart2.js',() => {
  beforeEach(async () => {
    await po.open();
  });

  //testing finDiscoveryStatusCheck(discoveryId)
  it('Returns the correct results when executed', () => {
    po.assert(function(testData,confirmData) {
      console.log("testData:");
      console.log(testData);
      console.log("testData!");
      
    
      ok(testData !== undefined 
      && testData !== undefined, "finDiscoveryStatusCheck("+ discoveryId +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finDiscoveryStatusCheck("+ discoveryId +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined 
        && row.connTagName.includes('bacnetConn') === true 
        && row.disMacro === "$bacnetDeviceName" 
        && row.finDiscoveryRef !== undefined 
        && row.mod !== undefined 
        && row.path !== undefined 
        && row.uri !== undefined,
        "finDiscoveryStatusCheck("+ discoveryId +") query result contains valid data on row "+ i +" => id: "+
        row.id +" | bacnetDeviceName: "+ row.bacnetDeviceName +" | connTagName: "+ row.connTagName +" | dis: "+
        row.dis +" | disMacro: "+ row.disMacro +" | finDiscoveryRef: "+ row.finDiscoveryRef +" | mod: "+
        row.mod +" | path: "+ row.path +" | uri: "+ row.uri);
      }

      let row= confirmData[0];
      ok(row.id !== undefined 
      && row.bacnetConn === "✓" 
      && row.path !== undefined 
      && row.disMacro === "$bacnetDeviceName" 
      && row.bacnetDeviceName === connName 
      && row.uri !== undefined 
      && row.dis === connName 
      && row.mod !== undefined, 
      "finDiscoveryAdd([finDiscoveryStatusCheck("+ discoveryId +").find(row =>row->bacnetDeviceName=="+
      JSON.stringify(connName) +")->id]) query result contains valid data => id: "+ row.id +" | bacnetConn: "+
      row.bacnetConn +" | path: "+ row.path +" | disMacro: "+ row.disMacro +" | bacnetDeviceName: "+
      row.bacnetDeviceName +" | uri: "+ row.uri +" | dis: "+ row.dis +" | mod: "+ row.mod + "\n"+
      "Connector successfully added: "  +  connName);
    });
  });  
});