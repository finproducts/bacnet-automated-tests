import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//array of expected elements: name & shortName
let expectedArray= ["ANALOG_INPUT", "AI",
                    "ANALOG_OUTPUT", "AO",
                    "ANALOG_VALUE", "AV",
                    "BINARY_INPUT", "BI",
                    "BINARY_OUTPUT", "BO",
                    "BINARY_VALUE", "BV",
                    "CALENDAR", "6",
                    "COMMAND", "CMD",
                    "DEVICE", "DEV",
                    "EVENT_ENROLLMENT", "EE",
                    "FILE", "10",
                    "GROUP", "11",
                    "LOOP", "LP",
                    "MULTI_STATE_INPUT", "MSI",
                    "MULTI_STATE_OUTPUT", "MSO",
                    "NOTIFICATION_CLASS", "NC",
                    "PROGRAM", "PRG",
                    "SCHEDULE", "SCH",
                    "AVERAGING", "AVG",
                    "MULTI_STATE_VALUE", "MSV",
                    "TREND_LOG", "TL",
                    "LIFE_SAFETY_POINT", "21",
                    "LIFE_SAFETY_ZONE", "22",
                    "ACCUMULATOR", "ACC",
                    "PULSE_CONVERTER", "PC",
                    "EVENT_LOG", "25",
                    "GLOBAL_GROUP", "26",
                    "TREND_LOG_MULTIPLE", "27",
                    "LOAD_CONTROL", "LC",
                    "STRUCTURED_VIEW", "29",
                    "ACCESS_DOOR", "30",
                    "ACCESS_CREDENTIAL", "32",
                    "ACCESS_POINT", "33",
                    "ACCESS_RIGHTS", "34",
                    "ACCESS_USER", "35",
                    "ACCESS_ZONE", "36",
                    "CREDENTIAL_DATA_INPUT", "37",
                    "NETWORK_SECURITY", "38",
                    "BITSTRING_VALUE", "39",
                    "CHARACTERSTRING_VALUE", "40",
                    "DATE_PATTERN_VALUE", "41",
                    "DATE_VALUE", "42",
                    "DATETIME_PATTERN_VALUE", "43",
                    "DATETIME_VALUE", "44",
                    "INTEGER_VALUE", "45",
                    "LARGE_ANALOG_VALUE", "46",
                    "OCTETSTRING_VALUE", "47",
                    "POSITIVE_INTEGER_VALUE", "48",
                    "TIME_PATTERN_VALUE", "49",
                    "TIME_VALUE", "50",
                    "NOTIFICATION_FORWARDER", "51",
                    "ALERT_ENROLLMENT", "52",
                    "CHANNEL", "53",
                    "LIGHTING_OUTPUT", "54",
                    "ASHRAE_RESERVED", "127",
                    "VENDOR_PROPRIETARY", "128"];

let po = new axonPO({"testQuery": "bacnetObjectTypeList()", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testBacnetObjectTypeList.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Testing bacnetObjectTypeList() query', () => {
    po.assert(function(testData, confirmData) {

      //testing bacnetObjectTypeList()
      ok(testData !== undefined  
      && testData !== null, "bacnetObjectTypeList() query result is not undefined or null.");

      ok(typeof testData === "object"
      && testData.type !== "error", "bacnetObjectTypeList() returned an object which doesn't contain an error.");

      let validArray= [];
      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.name !== undefined 
        && row.shortName !== undefined
        && row.value !== undefined, 
        "bacnetObjectTypeList() query contains valid data on row "+ i +" => name: "+ row.name +" | shortName: "+
        row.shortName +" | value: "+ row.value);

        if(row.name === "ANALOG_INPUT" && row.shortName === "AI" 
        || row.name === "ANALOG_OUTPUT" && row.shortName === "AO"
        || row.name === "ANALOG_VALUE" && row.shortName === "AV"
        || row.name === "BINARY_INPUT" && row.shortName === "BI"
        || row.name === "BINARY_OUTPUT" && row.shortName === "BO"
        || row.name === "BINARY_VALUE" && row.shortName === "BV"
        || row.name === "CALENDAR" && row.shortName === "6"
        || row.name === "COMMAND" && row.shortName === "CMD"
        || row.name === "DEVICE" && row.shortName === "DEV"
        || row.name === "EVENT_ENROLLMENT" && row.shortName === "EE"
        || row.name === "FILE" && row.shortName === "10"
        || row.name === "GROUP" && row.shortName === "11"
        || row.name === "LOOP" && row.shortName === "LP"
        || row.name === "MULTI_STATE_INPUT" && row.shortName === "MSI" 
        || row.name === "MULTI_STATE_OUTPUT" && row.shortName === "MSO"
        || row.name === "NOTIFICATION_CLASS" && row.shortName === "NC" 
        || row.name === "PROGRAM" && row.shortName === "PRG"
        || row.name === "SCHEDULE" && row.shortName === "SCH" 
        || row.name === "AVERAGING" && row.shortName === "AVG"
        || row.name === "MULTI_STATE_VALUE" && row.shortName === "MSV"
        || row.name === "TREND_LOG" && row.shortName === "TL"
        || row.name === "LIFE_SAFETY_POINT" && row.shortName === "21"
        || row.name === "LIFE_SAFETY_ZONE" && row.shortName === "22"
        || row.name === "ACCUMULATOR" && row.shortName === "ACC"
        || row.name === "PULSE_CONVERTER" && row.shortName === "PC"
        || row.name === "EVENT_LOG" && row.shortName === "25"
        || row.name === "GLOBAL_GROUP" && row.shortName === "26"
        || row.name === "TREND_LOG_MULTIPLE" && row.shortName === "27" 
        || row.name === "LOAD_CONTROL" && row.shortName === "LC"
        || row.name === "STRUCTURED_VIEW" && row.shortName === "29"
        || row.name === "ACCESS_DOOR" && row.shortName === "30"
        || row.name === "ACCESS_CREDENTIAL" && row.shortName === "32"
        || row.name === "ACCESS_POINT" && row.shortName === "33"
        || row.name === "ACCESS_RIGHTS" && row.shortName === "34"
        || row.name === "ACCESS_USER" && row.shortName === "35"
        || row.name === "ACCESS_ZONE" && row.shortName === "36"
        || row.name === "CREDENTIAL_DATA_INPUT" && row.shortName === "37"
        || row.name === "NETWORK_SECURITY" && row.shortName === "38"
        || row.name === "BITSTRING_VALUE" && row.shortName === "39"
        || row.name === "CHARACTERSTRING_VALUE" && row.shortName === "40" 
        || row.name === "DATE_PATTERN_VALUE" && row.shortName === "41"
        || row.name === "DATE_VALUE" && row.shortName === "42"
        || row.name === "DATETIME_PATTERN_VALUE" && row.shortName === "43"
        || row.name === "DATETIME_VALUE" && row.shortName === "44"
        || row.name === "INTEGER_VALUE" && row.shortName === "45"
        || row.name === "LARGE_ANALOG_VALUE" && row.shortName === "46"
        || row.name === "OCTETSTRING_VALUE" && row.shortName === "47"
        || row.name === "POSITIVE_INTEGER_VALUE" && row.shortName === "48"
        || row.name === "TIME_PATTERN_VALUE" && row.shortName === "49"
        || row.name === "TIME_VALUE" && row.shortName === "50"
        || row.name === "NOTIFICATION_FORWARDER" && row.shortName === "51"
        || row.name === "ALERT_ENROLLMENT" && row.shortName === "52"
        || row.name === "CHANNEL" && row.shortName === "53"
        || row.name === "LIGHTING_OUTPUT" && row.shortName === "54"
        || row.name === "ASHRAE_RESERVED" && row.shortName === "127"
        || row.name === "VENDOR_PROPRIETARY" && row.shortName === "128") {
      
          validArray.push(row.name, row.shortName);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      "Query result doesn't contain the expected elements: "+ JSON.stringify(validArray));
    });
  });  
});