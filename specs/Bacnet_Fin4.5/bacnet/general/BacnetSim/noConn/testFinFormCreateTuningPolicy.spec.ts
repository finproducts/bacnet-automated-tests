import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



let po = new axonPO({"testQuery": "finFormCreateTuningPolicy(0)", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinFormCreateTuningPolicy() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected result when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finFormCreateTuningPolicy(0) query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finFormCreateTuningPolicy(0) returned an object which doesn't contain an error.");

      ok(testData[0].body.includes('Dis Name for Tuning Policy') === true 
      && testData[0].body.includes('Enable Min Write Time') === true 
      && testData[0].body.includes('Min Write time in seconds') === true 
      && testData[0].body.includes('Enable Max Write Time') === true 
      && testData[0].body.includes('Max Write time in Minutes') === true 
      && testData[0].body.includes('Schedule Write') === true 
      && testData[0].body.includes('Enable Poll Time') === true 
      && testData[0].body.includes('Poll Time') === true 
      && testData[0].cancelButton === "$cancelButton" 
      && testData[0].commitAction.includes('finCreateEditTuningPolicyCommit') === true 
      && testData[0].commitButton === "$applyButton" 
      && testData[0].dis === "Create a Tuning Policy" 
      && testData[0].finForm === "✓" 
      && testData[0].helpDoc !== undefined 
      && testData[0].name === "tuningPolicy2", 
      "finFormCreateTuningPolicy(0) query result contains valid data => body: "+ testData[0].body +" | cancelButton: "+
      testData[0].cancelButton +" | commitAction: "+ testData[0].commitAction +" | commitButton: "+ 
      testData[0].commitButton +" | dis: "+ testData[0].dis +" | finForm: "+ testData[0].finForm +" | helpDoc: "+
      testData[0].helpDoc +" | name: "+ testData[0].name);
    });
  });  
});