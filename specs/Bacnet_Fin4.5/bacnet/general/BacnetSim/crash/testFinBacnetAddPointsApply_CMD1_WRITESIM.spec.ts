import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';





let connDis= 'WriteSim';

let bacnetConnId= 'read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")->id';

let floorId= 'read(floor and siteRef->dis=="  +  JSON.stringify(connDis)  +  ")->id';

let addPoint= 'CMD1';

let parameter= bacnetConnId  +  ","  +  floorId  +  ", [{bacnetCur:"  +  JSON.stringify(addPoint)  +  "}]";



let po = new axonPO({"\"testQuery\"":"\"finBacnetAddPointsApply(\"  +  parameter  +  \")\"","\"confirmQuery\"":"\"read(point and bacnetCur==\"  +  JSON.stringify(addPoint)  +  \" and siteRef->dis==\"  +  JSON.stringify(connDis)  +  \")\""});
po.setup(function(instance)
{}
)

describe('testFinBacnetAddPointsApply_CMD1_WRITESIM.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing finBacnetAddPointsApply_CMD1_WRITESIM"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'finBacnetAddPointsApply("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'finBacnetAddPointsApply("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

ok(testData[0].val  ===  undefined,"finBacnetAddPointsApply("  +  parameter  +  ") query contains valid data => val: "  +  testData[0].val);

ok(confirmData[0].dis  ===  "WriteSim WriteSim COMMAND CMD_1"  &&  confirmData[0].bacnetCur  ===  addPoint  &&  confirmData[0].bacnetConnRefDis  ===  connDis  &&  confirmData[0].point  ===  "✓"  &&  confirmData[0].cur  ===  "✓"  &&  confirmData[0].siteRefDis  ===  connDis,"read(point and bacnetCur=="  +  JSON.stringify(addPoint)  +  " and siteRef->dis=="  +  JSON.stringify(connDis)  +  ") query contains valid data => dis: "  +  confirmData[0].dis  +  " | bacnetCur: "  +  confirmData[0].bacnetCur  +  " | bacnetConnRefDis: "  +  confirmData[0].bacnetConnRefDis  +  " | point: "  +  confirmData[0].point  +  " | cur: "  +  confirmData[0].cur  +  " | siteRefDis: "  +  confirmData[0].siteRefDis);
}
);
  });  

});