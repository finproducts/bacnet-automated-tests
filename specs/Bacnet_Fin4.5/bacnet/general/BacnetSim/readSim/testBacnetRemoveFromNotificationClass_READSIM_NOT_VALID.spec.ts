import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';

/*
 * Bacnet connector targeted: "ReadSim" 
 * Notification class targeted: NC1
 * Removes the local device corresponding to the connector from the provided notification class, to disable receiving alarm/event notifications.  
 * Low level function for debugging mostly. 
 */

//point targeted
let notificationClass = 'NC1';

//bacnet connector dis
let connDis = 'ReadSim';

//argument passed to the axon function
let parameter = "read(bacnetConn and dis=="+ JSON.stringify(connDis) + ")" + "," + JSON.stringify(notificationClass);
 
let po = new axonPO({"testQuery": "bacnetRemoveFromNotificationClass("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetRemoveFromNotificationClass_READSIM_NOT_VALID() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null, "bacnetRemoveFromNotificationClass("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetRemoveFromNotificationClass("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === false,
      "bacnetRemoveFromNotificationClass("+ parameter +") query doesn't contain valid data: "+ testData[0].val);
    });
  });  
});