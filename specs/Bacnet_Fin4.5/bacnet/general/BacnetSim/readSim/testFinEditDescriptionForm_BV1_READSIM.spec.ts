import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//bacnet connector targeted "ReadSim" & "BV1" point (boolean)

//bacnet connector name
let connDis= 'ReadSim';

//bacnetCur
let bacnetPoint= 'BV1';

//argument passed to the axon function
let parameter = "read(point and bacnetCur=="+ JSON.stringify(bacnetPoint) +" and bacnetConnRef->dis=="+ 
JSON.stringify(connDis) +")->id";

let po = new axonPO({"testQuery": "finEditDescriptionForm(" + parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinEditDescriptionForm_BV1_READSIM() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined 
      && testData !== undefined, "finEditDescriptionForm("+ parameter +") query result is not undefined or null.");
 
      ok(typeof testData === "object" 
      && testData.type !== "error", "finEditDescriptionForm("+  parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].body.includes('Binary Value 1') === true  
      && testData[0].cancelButton === "$cancelButton" 
      && testData[0].commitButton === "$commitButton"  
      && testData[0].dis === "Edit Description" 
      && testData[0].finForm === "✓" 
      && testData[0].name === "finEditDescriptionForm", 
      "finEditDescriptionForm("+ parameter +") query result contains valid data => body: "+
      testData[0].body +" | cancelButton: "+ testData[0].cancelButton +" | commitAction: "+
      testData[0].commitAction +" | commitButton: "+ testData[0].commitButton +" | dis: "+
      testData[0].dis +" | finForm: "+ testData[0].finForm +" | helpDoc: "+
      testData[0].helpDoc +" | name: "+  testData[0].name);
    });
  });  
});