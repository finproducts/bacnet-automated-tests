import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



/*
Point must be in watch!!!
ReadSim connector has POLLING enabled (by disabling COV) 
Use bacnetPing(read(bacnetConn and dis=="ReadSim")) to change the connStatus of the connector to "ok"
Point targeted: AV3
After re-adding the connector in DB Builder, drag the point in the nav tree again.
*/

//bacnet point targeted
let bacnetPoint = "AV3";

//bacnet conn dis
let connDis = "ReadSim";

//read bacnet connector expression
let bacnetConn = "read(bacnetConn and dis==" + JSON.stringify(connDis) +")";

//new value for object property
function createRandomNumber() {

	var min = 1;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let newValue= createRandomNumber();

let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", \"present_value\", "+ JSON.stringify(newValue);


let po = new axonPO({"testQuery": "bacnetWriteObjectProperty("+ parameter +")",
"confirmQuery": "read(point and bacnetCur=="+ JSON.stringify(bacnetPoint) +" and bacnetConnRef->dis=="+
JSON.stringify(connDis) +")->curVal"});

po.setup(function(instance) {});

describe('The testConnAppDetails_POLLING_AV3_part2() query',async () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', async () => {
    await po.assert(async function(testData, confirmData) {
      await ok(testData[0].status === true,
      "bacnetWriteObjectProperty("+ parameter +") query doesn't contain valid data => status "+
       testData[0].status +". New value:"+ await newValue);
       console.log(await newValue)
     

      

     await ok(confirmData[0].val === newValue,
      "read(point and bacnetCur=="+ JSON.stringify(bacnetPoint) +" and bacnetConnRef->dis=="+ 
      JSON.stringify(connDis) +")->curVal query contains invalid data => val: "+ 
      confirmData[0].val +"\n"+ "Polling worked properly. New value is store in curVal.");
    });
  });  
});