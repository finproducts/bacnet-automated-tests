import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let expectedArray = ["ACC1", "ACC2", "ACC3"];
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';
let folder = '`ACCUMULATOR`';
let parameter = bacnetConn + "," + folder;

let po = new axonPO({"testQuery": "bacnetLearn("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The testBacnetReadFolder_ACCUMULATOR() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined && testData !== undefined,
      "bacnetLearn("+ parameter +") query result is not undefined or null.");
      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetLearn("+ parameter +") returned an object which doesn't contain an error.");
      let validArray= []

      for(let i  =  0; i  <  testData.length; i++) {
        let row= testData[i]

        ok(row.bacnetCur !== undefined && row.bacnetWrite !== null &&
        row.bacnetWriteLevel!== undefined && row.dis !== undefined &&
        row.kind === "Number" && row.point === "✓" &&
        row.writable === "✓", "bacnetLearn("+ parameter +") query contains valid data on row "+
        i +" => bacnetCur: "+ row.bacnetCur + " | bacnetWrite: "+ row.bacnetWrite +" | bacnetWriteLevel:"+
        row.bacnetWriteLevel +" | dis: "+ row.dis +" | is: "+ row.is +" | kind: "+ row.kind +" | point: "+
        row.point +" | writable: "+ row.writable);

        if( row.bacnetCur === "ACC1" || row.bacnetCur === "ACC2" || row.bacnetCur === "ACC3") {
          validArray.push(row.bacnetCur);
        }   
      }
      ok(JSON.stringify(validArray.sort())  ===  JSON.stringify(expectedArray.sort()),
      "Query result contains the expected elements: " + JSON.stringify(validArray));
    });
  });  
});