import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["STATUS_FLAGS", "BitList(0 0 0 0)", "OBJECT_NAME", "MO_1",
"OBJECT_TYPE", 14, "OBJECT_IDENTIFIER", "MSO1", "PRESENT_VALUE", 1];

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//bacnet object
let bacnetObject = 'MSO1';

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);

let po = new axonPO({"testQuery": "bacnetReadObject("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetReadObject_MSO1() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null, "bacnetReadObject("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetReadObject("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.bacnetValueType !== undefined &&
        row.objectType === "MULTI_STATE_OUTPUT" &&
        row.propertyName !== undefined, "bacnetReadObject("+ parameter +") query contains valid data on row "+
        i +" => bacnetValueType: "+ row.bacnetValueType +" | objectType: "+ 
        row.objectType +" | propertyName: "+ row.propertyName +" | value: "+ row.value);

        if( row.propertyName === "STATUS_FLAGS" && row.value === "BitList(0 0 0 0)" 
        || row.propertyName === "OBJECT_NAME" && row.value === "MO_1"
        || row.propertyName === "OBJECT_TYPE" && row.value === 14 
        || row.propertyName === "OBJECT_IDENTIFIER" && row.value === "MSO1"
        || row.propertyName === "PRESENT_VALUE" && row.value === 1) {
          validArray.push(row.propertyName,row.value);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), 
      "Query result doesn't contain the expected elements: "+ JSON.stringify(validArray));
    });
  });  
});