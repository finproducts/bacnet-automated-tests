import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//generates current date & time
let today = new Date();

let hours = today.getHours();

let minutes = today.getMinutes();

let seconds = today.getSeconds();

let year = today.getFullYear();

let month = today.getMonth() + 1;

if( hours < 10) { 
    hours = "0" + hours
}

if( minutes  <  10) {
minutes = "0" + minutes
}

if( seconds  <  10) {
seconds = "0" + seconds
}

if( month  <  10) {
month = "0" + month
}

//connector targeted: "ReadSim"
let connDis = "ReadSim";

//bacnet conn id
let connId ="read(bacnetConn and dis=="+ JSON.stringify(connDis) +")->id.toStr";

//argument passed to the axon function
let parameter = connId;

let po = new axonPO({"testQuery": "finSyncHisPointsJob(["+ parameter +"])", "confirmQuery":"jobStatusAll()"});
po.setup(function(instance) {});

describe('The testFinSyncHisPointsJob_READSIM() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
        ok(testData !== undefined 
        && testData !== null, "finSyncHisPointsJob("+ parameter +") query result is not undefined or null.");

        ok(typeof testData === "object" 
        && testData.type !== "error", "finSyncHisPointsJob("+ parameter +") returned an object which doesn't contain an error.");

        ok(testData[0].val === null, 
        "finSyncHisPointsJob("+ parameter +") query result contains invalid data => val: "+ testData[0].val);

        let row = confirmData.length - 1

        ok(confirmData[row].handle !== undefined 
        && confirmData[row].jobStatus === "doneOk" 
        && confirmData[row].dis === "FIN Energy kw to kwh"
        && confirmData[row].progressMsg === "Complete" 
        && confirmData[row].started.includes(year) === true 
        && confirmData[row].started.includes(month) === true,
        "jobStatusAll() query results contains invalid data on backgroundBacnetUpdateInfo => handle: "+
        confirmData[row].handle +" | jobStatus: "+ confirmData[row].jobStatus +" | dis: "+
        confirmData[row].dis +" | progress: "+ confirmData[row].progress +" | progressMsg: "+
        confirmData[row].progressMsg +" | started: "+ confirmData[row].started +" | runtime: "+ confirmData[row].runtime);
    });
  });  
});