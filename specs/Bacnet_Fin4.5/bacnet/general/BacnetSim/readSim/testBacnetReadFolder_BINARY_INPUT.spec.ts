import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["BI1", "BI2", "BI3"];

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//bacnet object
let folder = '`BINARY_INPUT`';

//argument passed to the axon function
let parameter = bacnetConn  +  ","  +  folder;

let po = new axonPO({"testQuery": "bacnetLearn("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance){})

describe('The testBacnetReadFolder_BINARY_INPUT() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined &&
      testData !== undefined, "bacnetLearn("+ parameter +") query result is not undefined or null.");
      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetLearn("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i  =  0; i  <  testData.length; i++) {
        let row= testData[i];
        ok(row.bacnetCur !== undefined &&
        row.dis !== null &&
        //row.is === "BINARY_INPUT" &&
        row.kind === "Bool" &&
        row.point === "✓" &&
        row.enum === "INACTIVE,ACTIVE", "bacnetLearn("+ parameter +") query contains valid data on row "+
        i +" => bacnetCur: "+ row.bacnetCur +" | dis: "+ row.dis +" | is: "+ row.is +" | kind: "+
        row.kind +" | point: "+ row.point +" | enum: "+ row.enum);

        if( row.bacnetCur === "BI1" || row.bacnetCur === "BI2" || row.bacnetCur === "BI3") {
          validArray.push(row.bacnetCur);
        }
      }
    ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
    "Query result contains the expected elements: "+  JSON.stringify(validArray));
    });
  });  
});