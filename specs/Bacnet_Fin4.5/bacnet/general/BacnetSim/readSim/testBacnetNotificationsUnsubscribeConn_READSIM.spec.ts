import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let expectedArray= ["AV1", "BV1"];
let connDis= 'ReadSim';
let bacnetConn= "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")";

let po = new axonPO({"testQuery": "bacnetNotificationsUnsubscribeConn("+ bacnetConn +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The bacnetNotificationsUnsubscribeConn() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined && testData !== null,
      "bacnetNotificationsUnsubscribeConn("+ bacnetConn +") query result is not undefined or null.");
      ok(typeof testData === "object" && testData.type !== "error",
      "bacnetNotificationsUnsubscribeConn("+ bacnetConn +") returned an object which doesn't contain an error.");

      let validArray= []

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i]
        ok(row.localDevId.includes('DEV') === true &&
        row.notificationId.includes('NC') === true &&
        row.pointId !== undefined && row.result === true,
        "bacnetNotificationsUnsubscribeConn("+ bacnetConn +") query contains valid data on row "+ i
        +" => localDevId: "+ row.localDevId +" | notificationId: "+ row.notificationId
        +" | pointId: "+ row.pointId +" | result: "+ row.result);

        if(row.pointId === "AV1" && row.notificationId === "NC1" 
        || row.pointId === "BV1" && row.notificationId === "NC1") {
          validArray.push(row.pointId);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      "Query result contains the expected objects with alarms: "+ JSON.stringify(validArray));
    });
  });  
});