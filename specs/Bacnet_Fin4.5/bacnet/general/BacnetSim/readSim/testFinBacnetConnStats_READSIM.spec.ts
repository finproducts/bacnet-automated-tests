import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//bacnet device dis
let bacnetDis= "ReadSim";

//argument passed to the axon function
let parameter ="read(bacnetConn and dis=="+ JSON.stringify(bacnetDis) +")";

let po = new axonPO({"testQuery": "finBacnetConnStats("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testFinBacnetConnStats_READSIM() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined
      && testData !== null, "finBacnetConnStats("+ parameter +") query result is not undefined or null.");
      
      ok(typeof testData === "object"
      && testData.type !==  "error", "finBacnetConnStats("+ parameter +") returned an object which doesn't contain an error.");

      let row= testData[0];

      ok(row.defWriteDelay === 20000 
      && row.remoteDevice === "DEV2" 
      && row.deviceName === "ReadSim"
      && row.deviceStatus === "OPERATIONAL" 
      && row.supportsCOV === false 
      && row.covFallBack === 0 
      && row.connType === "IP" 
      && row.segmentation === "SEGMENTED_BOTH" 
      && row.maxADPUSize === 1476 
      && row.protocolVersion === 1 
      && row.protocolRevision === 12 
      && row.writeMaxRetry === 1 
      && row.supportsWPM === true 
      //&& row.crtWriteDelay === undefined
      && row.covSubcribed === 0 
      && row.vendorName === "Siemens" 
      && row.localDevice.includes("DEV") === true 
      && row.modelName === "BACnet2004" 
      && row.connStat === "✓" 
      && row.writeIncRetry === 3 
      && row.isCOVEnabled === false 
      && row.supportsRPM === true, "finBacnetConnStats("+ parameter +") query contains invalid data => connErrCount: "+
      row.connErrCount +" | defWriteDelay: "+ row.defWriteDelay +" | remoteDevice: "+ row.remoteDevice +" | deviceName: "+
      row.deviceName +" | deviceStatus: "+ row.deviceStatus +" | supportsCOV: "+ row.supportsCOV +" | covFallBack: "+
      row.covFallBack +" | connType: "+ row.connType +" | segmentation: "+  row.segmentation +" | maxADPUSize: "+
      row.maxADPUSize +" | protocolVersion: "+ row.protocolVersion +" | protocolRevision: "+
      row.protocolRevision +" | writeMaxRetry: "+ row.writeMaxRetry +" | supportsWPM: "+ row.supportsWPM +" | crtWriteDelay: "+
      row.crtWriteDelay +" | covSubcribed: "+ row.covSubcribed +" | vendorName: "+ row.vendorName +" | localDevice: "+
      row.localDevice +" | modelName: "+ row.modelName +" | connStat: "+ row.connStat +" | writeIncRetry: "+
      row.writeIncRetry  +" | isCOVEnabled: "+ row.isCOVEnabled +" | supportsRPM: "+ row.supportsRPM);
    });
  });  
});