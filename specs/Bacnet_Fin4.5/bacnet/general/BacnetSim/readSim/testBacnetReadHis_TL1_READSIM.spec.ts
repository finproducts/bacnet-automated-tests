import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
bacnet connector targeted: "ReadSim"

convert to UTC timezone for the correct results
var toValidTz = '.map(r => {"ts":r["ts"].toTimeZone("UTC"), "val":r["val"]})';
history points array

TL1 mapped to AI1 (numeric) | Data: REAL
*/

let pointArray = ["TL1"];
let randomPointIndex = Math.floor(Math.random() * pointArray.length);
let randomPoint = pointArray[randomPointIndex];

//bacnet connector
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//time range
let timeRange = 'now()-10year..now()';

//argument passed to the axon function  
let parameter = bacnetConn + "," + JSON.stringify(randomPoint) + "," + timeRange;

let po = new axonPO({"testQuery": "bacnetReadHis("+ parameter +").map(r => {\"ts\":r[\"ts\"].toTimeZone(\"UTC\"), \"val\":r[\"val\"]})",
"confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetReadHis_TL1_READSIM() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null, "bacnetReadHis("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "bacnetReadHis("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(testData[0].ts.includes('2013-01-01') === true &&
        row.ts !== undefined &&
        typeof row.val === "number" &&
        row.val === (i+1) && testData.length === 50, 
        "bacnetReadHis("+ parameter +") query contains invalid data on row "+ i +" => ts: "+
        row.ts +" | val: "+ row.val);
      }
    });
  });  
});