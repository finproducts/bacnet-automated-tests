import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let bacnetCur = 'AV1';
let connDis = 'ReadSim';

let pointExpression = "read(point and bacnetCur =="+ JSON.stringify(bacnetCur)
+" and bacnetConnRef->dis=="+ JSON.stringify(connDis) +")";

let parameter = "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")" + "," + pointExpression;

let undoChanges = "bacnetNotificationsSubscribePoint(read(bacnetConn and dis=="
+ JSON.stringify(connDis) +"), read(point and bacnetCur =="+ JSON.stringify(bacnetCur)
+" and bacnetConnRef->dis==" + JSON.stringify(connDis) +"))";

let po = new axonPO({"testQuery": "bacnetNotificationsUnsubscribePoint("+ parameter +")","confirmQuery": "undoChanges"});
po.setup(function(instance) {})

describe('The testBacnetNotificationsUnsubscribePoint_AV1() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined && testData !== null,
      "bacnetNotificationsUnsubscribePoint("+ parameter +") query result is not undefined or null.");
      ok(typeof testData === "object" && testData.type !== "error",
      "bacnetNotificationsUnsubscribePoint("+ parameter +") returned an object which doesn't contain an error.");

      for(let i  =  0; i  <  testData.length; i++) {
        let row= testData[i];  
        ok(row.localDevId.includes('DEV') === true  &&  row.notificationId  ===  "NC1"
        && row.pointId === bacnetCur && row.result === true,
        "bacnetNotificationsUnsubscribePoint("+ parameter +") query contains valid data on row "+
        i +" => localDevId: "+ row.localDevId +" | notificationId: "+ row.notificationId +" | pointId: "+
        row.pointId +" | result: "+ row.result);
      }
    });
  });  
});