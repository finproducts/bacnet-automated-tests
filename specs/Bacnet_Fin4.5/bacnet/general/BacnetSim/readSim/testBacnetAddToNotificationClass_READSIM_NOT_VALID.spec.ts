import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * Bacnet connector targeted: "ReadSim" 
 * Notification class targeted: NC1
 * Adds the connector to the provided notification class to enable receiving alarm/event notifications. 
 * Low level function for debugging mostly. 
 */

//point targeted
let notificationClass = 'NC1';

//bacnet connector dis
let connDis = 'ReadSim';

//argument passed to the axon function
let parameter = "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")" + "," + JSON.stringify(notificationClass);


let po = new axonPO({"testQuery": "bacnetAddToNotificationClass("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The bacnetAddToNotificationClass() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct results when executed', () => {
    //console.log(po);
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined  
      && testData !== undefined, "bacnetAddToNotificationClass("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "bacnetAddToNotificationClass("+
      parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].val === true, "bacnetAddToNotificationClass("+
      parameter +") query contains valid data: "+  testData[0].val);
    });
  });  
});