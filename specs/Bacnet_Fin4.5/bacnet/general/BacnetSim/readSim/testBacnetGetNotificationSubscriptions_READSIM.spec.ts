import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';



/*
 * Bacnet connector targeted: "ReadSim" 
 * Returns all the Notification Classes this current connector receives notifications from. 
 */

let connDis = 'ReadSim';
let bacnetConn = "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")";
let po = new axonPO({"testQuery": "bacnetGetNotificationSubscriptions("+ bacnetConn +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The bacnetGetNotificationSubscriptions() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns correct data when executed', () => {
    po.assert(function(testData, confirmData) {

      ok(testData !== undefined 
      && testData !== null, "bacnetGetNotificationSubscriptions("+ bacnetConn +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", 
      "bacnetGetNotificationSubscriptions("+ bacnetConn +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.localDevice.startsWith('DEV') === true &&
        row.notificationClass.startsWith('NC') === true &&
        row.point.startsWith('@') === true,
        "bacnetGetNotificationSubscriptions("+ bacnetConn +") query contains valid data on row "+ i +" => localDevice:" +
        row.localDevice +" | notificationClass: " + row.notificationClass + " | point: " + row.point);
      }
    });
  });  
});