import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


/*
 * To maintain test data clean, delete and re-add the point in DB Builder (under ReadSim -> Floor 1 -> His) before each test run
 */

//connector targeted
let bacnetConnDis= "ReadSim";

//navName of the point (must not be changed!)
let pointNav= "MI_1";

//argument passed to the axon function
let parameter= "read(point and bacnetConnRef->dis=="+ JSON.stringify(bacnetConnDis) +" and navName=="+ JSON.stringify(pointNav) +")->id";

//function validation
let validation= "read(point and bacnetConnRef->dis=="+ JSON.stringify(bacnetConnDis) +" and navName=="+ JSON.stringify(pointNav) +")->id,now()-10year..now()";

let po = new axonPO({"testQuery": "bacnetSyncHis("+ parameter +")", "confirmQuery": "hisRead("+ validation +")"});
po.setup(function(instance) {});

describe('The testBacnetSyncHis_MSI1_READSIM() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "bacnetSyncHis("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "bacnetSyncHis("+ parameter +") returned an object which doesn't contain an error.");
      
      let i;
      ok(testData[0].val === 150, "bacnetSyncHis("+ parameter +") query contains invalid data on row "+ 
      i +" => val: "+ testData[0].val);

      
      for(i = 0; i < confirmData.length; i++) {
        let row= confirmData[i];
        ok(confirmData[0].ts.includes('2013-01-01') === true 
        && row.ts !== undefined 
        && typeof row.v0 === "string" 
        && confirmData[1].v0 === "6" 
        && confirmData[2].v0 === "9"
        && confirmData[3].v0 === "12"  
        && confirmData[4].v0 === "15"
        && confirmData[5].v0 === "18" 
        && confirmData[confirmData.length-1].v0 === "450" 
        && confirmData.length === 150, "hisRead("+ validation +") query contains invalid data on row "+ 
        i +" => ts: "+ row.ts +" | v0: "+ row.v0);
      }
    });
  });  
});