import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';





let connDis = 'ReadSim';

let bacnetConn = "read(bacnetConn and dis=="+ JSON.stringify(connDis) +")";



let po = new axonPO({"testQuery": "bacnetGetUnhandledAlarms("+ bacnetConn +")", "confirmQuery": ""});
po.setup(function(instance) {});


describe('The bacnetGetUnhandledAlarms() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "bacnetGetUnhandledAlarms("+ bacnetConn +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "bacnetGetUnhandledAlarms("+
      bacnetConn +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[]", 
      "bacnetGetUnhandledAlarms("+ bacnetConn +") query contains valid data: "+ JSON.stringify(testData));
    });
  });  
});