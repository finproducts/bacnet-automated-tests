import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array 
let expectedArray = ["STATUS_FLAGS", "BitList(0 0 0 0)",
"OBJECT_NAME", "AI_1", "OBJECT_TYPE", 0, "OBJECT_IDENTIFIER", "AI1", "PRESENT_VALUE", 35];

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//bacnet object
let bacnetObject = 'AI1';

//argument passed to the axon function
let parameter = bacnetConn + "," + JSON.stringify(bacnetObject);

let po = new axonPO({"testQuery":"bacnetReadObject("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The testBacnetReadObject_AI1() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData, confirmData) {
      ok(testData !== undefined && testData !== null,
      "bacnetReadObject("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error",
      "bacnetReadObject("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i]

        ok(row.bacnetValueType !== undefined &&
        row.objectType === "ANALOG_INPUT" &&
        row.propertyName !== undefined &&
        row.value !== undefined,
        "bacnetReadObject("+ parameter +") query contains valid data on row "+ i +" => bacnetValueType: "+
        row.bacnetValueType  +" | objectType: "+ row.objectType +" | propertyName: "+
        row.propertyName +" | value: "+  row.value);

        if(row.propertyName === "STATUS_FLAGS" &&
        row.value === "BitList(0 0 0 0)" || row.propertyName === "OBJECT_NAME" &&
        row.value === "AI_1" || row.propertyName === "OBJECT_TYPE" &&
        row.value === 0 || row.propertyName === "OBJECT_IDENTIFIER" &&
        row.value === "AI1" || row.propertyName === "PRESENT_VALUE" && row.value  ===  35) { 
      
          validArray.push(row.propertyName,row.value);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      "Query result contains the expected elements: "+ JSON.stringify(validArray));
    });
  });  
});