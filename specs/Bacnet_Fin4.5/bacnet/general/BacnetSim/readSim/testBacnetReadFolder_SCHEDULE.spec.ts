import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["SCH1", "SCH2", "SCH3"];

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

// bacnet object
let folder = '`SCHEDULE`';

//argument pased to the axon function
let parameter = bacnetConn + "," + folder;

let po = new axonPO({"testQuery": "bacnetLearn("+ parameter +")", "confirmQuery":""});
po.setup(function(instance) {});

describe('The testBacnetReadFolder_SCHEDULE() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {

      ok(testData !== undefined &&
      testData !== null, "bacnetLearn("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetLearn("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {

        let row = testData[i];

        ok(row.bacnetCur !== undefined &&
        row.dis !== undefined &&
        //row.is === "SCHEDULE" &&
        row.kind !== undefined &&
        row.point === "✓", "bacnetLearn("+ parameter +") query contains valid data on row "+ i +" => bacnetCur: "+
        row.bacnetCur +" | dis:"+  row.dis + " | is: "+ row.is +" | kind: "+ row.kind +" | point: "+ row.point);

        if( row.bacnetCur === "SCH1" || row.bacnetCur === "SCH2" || row.bacnetCur === "SCH3") {
          validArray.push(row.bacnetCur);
        }
      }
    ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
    "Query result contains the expected elements: " + JSON.stringify(validArray));
    });
  });  
});