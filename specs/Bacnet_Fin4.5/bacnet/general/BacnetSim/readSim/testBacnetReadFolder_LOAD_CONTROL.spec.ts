import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../../../../helpers/nwapis';
import {waitService} from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


//expected array
let expectedArray = ["LC1", "LC2", "LC3"];

//bacnet conn read
let bacnetConn = 'read(bacnetConn and dis=="ReadSim")';

//bacnet object
let folder = '`LOAD_CONTROL`';

//argument passed to the axon function
let parameter = bacnetConn + "," + folder;


let po = new axonPO({"testQuery": "bacnetLearn("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {})

describe('The testBacnetReadFolder_LOAD_CONTROL() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the correct data when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined &&
      testData !== null, "bacnetLearn("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" &&
      testData.type !== "error", "bacnetLearn("+ parameter +") returned an object which doesn't contain an error.");

      let validArray= [];

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        
        ok(row.bacnetCur !== undefined &&
        row.dis !== undefined &&
        //row.is === "LOAD_CONTROL" &&
        row.kind === "Str" &&
        row.point === "✓" &&
        row.enum === "shed-inactive,shed-request-pending,shed-compliant,shed-non-compliant",
        "bacnetLearn("+ parameter +") query contains valid data on row "+ i +" => bacnetCur: "+
        row.bacnetCur  +" | dis: "+ row.dis +" | is: "+ row.is +" | kind: "+ row.kind +" | point: "+
        row.point +" | enum: "+ row.enum);

        if( row.bacnetCur === "LC1" || row.bacnetCur === "LC2" || row.bacnetCur === "LC3") {
          validArray.push(row.bacnetCur);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
      "Query result contains the expected elements: "+ JSON.stringify(validArray));
    });
  });  
});