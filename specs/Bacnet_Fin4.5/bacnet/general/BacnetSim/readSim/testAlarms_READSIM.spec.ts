import { by, browser, element, ExpectedConditions as EC, $, $$ } from 'protractor';
import { ok, equal, notEqual } from '../../../../../../helpers/nwapis';
import { waitService } from '../../../../../../helpers/wait-service';
import axonPO from '../../../../../../page_objects/finstack/axon.page';


let expectedArray = ["AV1"];
let level = 'site';
let connDis = 'ReadSim';
let filter = "finSubEquipFilter(read("+ level +" and dis=="+ JSON.stringify(connDis) +"))";
let timeRange = "thisWeek";
let parameter = filter + ", "+ timeRange +", null, null";

let po = new axonPO({ "testQuery": "alarms(" + parameter + ")", "confirmQuery": "" });
po.setup(function (instance) {})

describe('The testAlarms() query', () => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns correct data when executed', () => {
    po.assert(function (testData, confirmData) {
      ok(testData !== undefined && testData !== null,
      "alarms("+  parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" && testData.type !== "error",
      "alarms("  +  parameter  +  ") returned an object which doesn't contain an error.");

      let validArray = [];

      for (let i = 0; i < testData.length; i++) {
        let row = testData[i]
        ok(row.id !== undefined &&
        row.ackRequired === false &&
        row.alarm === "✓" &&
        row.alarmStatus === "alarm" &&
        //row.alarmText === "From Alarm Summary!" &&
        row.bacnetConnRefDis === connDis &&
        row.equipRefDis.includes(connDis) === true &&
        row.eventObjectIdentifier !== undefined &&
        row.initiatingDeviceIdentifier === "DEV2" &&
        row.notifyType === "ALARM" &&
        row.pointRefDis.includes(connDis) === true &&
        row.siteRefDis === connDis &&
        row.toState !== undefined,
        "alarms(" + parameter + ") query contains valid data on row " + i + " => id: " + row.id + " | ackRequired: " + row.ackRequired + " | alarm: " + row.alarm + " | alarmStatus: " + row.alarmStatus + " | alarmText: " + row.alarmText + " | bacnetConnRefDis: " + row.bacnetConnRefDis + " | equipRefDis: " + row.equipRefDis + " | eventObjectIdentifier: " + row.eventObjectIdentifier + " | initiatingDeviceIdentifier: " + row.initiatingDeviceIdentifier + " | notifyType: " + row.notifyType + " | pointRefDis: " + row.pointRefDis + " | siteRefDis: " + row.siteRefDis + " | toState: " + row.toState + " | ts: " + row.ts);

        if (row.id.includes('bacnet::AV1') === true &&
        row.eventObjectIdentifier === "AV1" &&
        row.pointRefDis === "ReadSim Equip 1 AV_1" ||
        row.id.includes('bacnet::BV1::OFFNORMAL') === true &&
        row.eventObjectIdentifier === "BV1" &&
        row.pointRefDis === "ReadSim Equip 1 BV_1") {
          validArray.push(row.eventObjectIdentifier);
        }
      }
      ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()),
       "Query result contains the expected objects with alarms: " + JSON.stringify(validArray));
    });
  });
});