import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let connDis = 'APP6527';

let bacnetCur = 'AI4';

let parameter = 'read(bacnetCur and kind==\"Number\" and bacnetCur=="  +  JSON.stringify(bacnetCur)' +
    '  +  " and bacnetConnRef->dis=="  +  JSON.stringify(connDis)  +  ")';

let po = new axonPO({"testQuery": "bacnetSyncCur("  +  parameter  +  ")", "confirmQuery": "parameter"});
po.setup(function (instance) {})

describe('testBacnetSyncCur_number_AI4_APP6527.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetSyncCur_number_AI4_APP6527", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetSyncCur("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "bacnetSyncCur("  +  parameter  +  ") returned an object which doesn\'t contain a" +
            'n error.');

        ok(JSON.stringify(testData) === '[{\"val\":null}]', "bacnetSyncCur(" + parameter + ") query returns valid data: " + JSON.stringify(testData));

        let row = confirmData[0]

        ok(row.dis === connDis + " Equip 1 ROOM TEMP" && row.id !== undefined && row.bacnetCur === bacnetCur && row.cur === "✓" && row.bacnetConnRefDis === connDis && row.kind === "Number" && row.siteRefDis === connDis && row.is === "ANALOG_INPUT" && row.unit === "°F" && row.point === "✓" && row.floorRefDis === "Floor 1" && row.navName === "ROOM TEMP" && row.equipRefDis.includes(connDis) === false && row.sensor === "✓", parameter + " query contains valid data => dis: " + row.dis + " | id: " + row.id + " | bacnetCur: " + row.bacnetCur + " | cur: " + row.cur + " | bacnetConnRefDis: " + row.bacnetConnRefDis + " | kind: " + row.kind + " | siteRefDis: " + row.siteRefDis + " | is: " + row.is + " | curStatus: " + row.curStatus + " | curVal: " + row.curVal + " | point: " + row.point + " | writable: " + row.writable + " | floorRefDis: " + row.floorRefDis + " | navName: " + row.navName + " | bacnetWrite: " + row.bacnetWrite + " | equipRefDis: " + row.equipRefDis + " | sensor: " + row.sensor);
      });
  });

});