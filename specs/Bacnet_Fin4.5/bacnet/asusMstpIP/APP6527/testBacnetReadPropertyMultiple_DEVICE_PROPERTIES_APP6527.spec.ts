import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let bacnetConn = 'read(bacnetConn and dis=="APP6527")';

let deviceObject = 'DEV7007';

let deviceProps = '{max_apdu_length_accepted, "  +  "model_name, "  +  "object_identifier, "  +  "o' +
    'bject_name, "  +  "protocol_revision, "  +  "protocol_services_supported, "  +  ' +
    '"protocol_version, "  +  "system_status, "  +  "vendor_name}';

let parameter = bacnetConn + ", {" + JSON.stringify(deviceObject) + ":" + deviceProps + "}";

let po = new axonPO({"testQuery": "bacnetReadPropertyMultiple("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadPropertyMultiple_DEVICE_PROPERTIES_APP6527.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadPropertyMultiple_DEVICE_PROPERTIES_APP6527", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetReadPropertyMultiple ("  +  parameter  +  ") query result is not undefined" +
            ' or null.');

        ok(typeof testData === "object" && testData.type !== "error", "bacnetReadPropertyMultiple ("  +  parameter  +  ") returned an object which does" +
            'n\'t contain an error.');

        let validArray = []

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.objectId === deviceObject && row.propertyName !== undefined && row.error.includes("j2inn.io.bacnet.app.service.objects.PropertyAccessError: Error accessing propert" +
              "y " + deviceObject) === false, "bacnetReadPropertyMultiple (" + parameter + ") query contains valid data on row " + i + " => error: " + row.error + " | objectId: " + row.objectId + " | propertyName: " + row.propertyName);
        }

      });
  });

});