import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let bacnetPoint = 'AO122';

let connDis = 'APP6527';

let bacnetConn = "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

function createRandomNumber() {

  var min = 1;
  var max = 30;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
};

let newValue = createRandomNumber();

let parameter = bacnetConn + "," + JSON.stringify(bacnetPoint) + ", present_value, " + JSON.stringify(newValue);

let po = new axonPO({
  "testQuery": "bacnetWriteObjectProperty("  +  parameter  +  ")",
  "confirmQuery": "read(point and bacnetCur==  +  JSON.stringify(bacnetPoint)  +   and bacnet" +
      "ConnRef->dis==  +  JSON.stringify(connDis)  +  )->curVal"
});
po.setup(function (instance) {})

describe("testConnAppDetails_POLLING_AO122_APP6527_part2.js", () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing testConnAppDetails_POLLING_AO122_APP6527_part2", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData[0].status === false, "bacnetWriteObjectProperty(" + parameter + ") query contains valid data => status " + testData[0].status + ". New value: " + newValue);

        ok(confirmData[0].val === newValue, "read(point and bacnetCur=="  +  JSON.stringify(bacnetPoint)  +  " and bacnetConn" +
            "Ref->dis=="  +  JSON.stringify(connDis)  +  ")->curVal query contains valid data" +
            ' => val: ' + confirmData[0].val + 'Polling worked properly. New value is store in curVal.');
      });
  });

});