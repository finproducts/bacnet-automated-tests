/**
 * Testing axon implementation
 * 
 */

import {
    by,
    browser,
    element,
    ExpectedConditions as EC,
    $,
    $$
  } from 'protractor';
  import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
  import {waitService} from '../../../../../helpers/wait-service';
  import axonPO from '../../../../../page_objects/finstack/axon.page';
  
    
  let parameter = "readAll(site)";
  
  let po = new axonPO({"testQuery": parameter, "confirmQuery": ""});
  
  po.setup(function (instance) {})
  
  describe('testAxon.js', () => {
    beforeEach(async() => {
      await po.open();
    });
  
    it("testingReadall", () => {
      po
        .assert(function (testData, confirmData) {
          ok(testData !== undefined && testData !== null, "readAll(site)query result is not undefined or null.");
  
          ok(typeof testData === "object" && testData.type !== "error", "readAll(site ) returned an object which does not contain an error.");
         // console.log(JSON.stringify(testData));  
          
  
          let i;
  
          for (i = 0; i < confirmData.length; i++) {
            let row = confirmData[i]
  
            ok(row.id !== undefined 
                && row.dis == "Laguna"
                    || row.dis== "City Center"
                    || row.dis== "Malibu"
                    || row.dis== "JB Tower"
                    || row.dis== "Pearl Tower"
                    
           
            , "it works " );
          }
  
        });
    });
  
  });