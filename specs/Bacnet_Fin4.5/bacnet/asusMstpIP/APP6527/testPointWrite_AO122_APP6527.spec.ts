import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {ok, equal, notEqual} from "../../../../../helpers/nwapis";
import {waitService} from "../../../../../helpers/wait-service";
import axonPO from "../../../../../page_objects/finstack/axon.page";

let connDis = "APP6527";

let pointBacnetCur = "AO122";

function createRandomNumber() {

  var min = 1;
  var max = 30;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
};

let newValue = createRandomNumber();

let parameter = "read(point and bacnetCur=="  +  JSON.stringify(pointBacnetCur)  +  " and bacnetC" +
    "onnRef->dis=="  +  JSON.stringify(connDis)  +  ")->id,"  +  newValue  +  ", 16, " +
    "su";

let validation = "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  "),"  +  JSON.stringify(pointBacnetCur)  + ",PRESENT_VALUE";

let po = new axonPO({"testQuery": "pointWrite("  +  parameter  +  ")", "confirmQuery": "bacnetReadObjectProperty(  +  validation  +  )"});
po.setup(function (instance) {})

describe("testPointWrite_AO122_APP6527.js", () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing pointWrite_AO122_APP6527", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "pointWrite("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "pointWrite("  +  parameter  +  ") returned an object which doesnt contain an e" +
            "rror.");

        ok(testData[0].val === newValue, "pointWrite(" + parameter + ") query contains valid data => val: " + testData[0].val);

        ok(confirmData[0].bacnetValueType === "Real" && confirmData[0].objectType === "ANALOG_OUTPUT" && confirmData[0].propertyName === "PRESENT_VALUE" && confirmData[0].value === newValue, "bacnetReadObjectProperty(" + validation + ") query contains valid data => bacnetValueType: "  +  confirmData[0].bacnetValue +
            "Type"  +   "| objectType : "  +  confirmData[0].objectType  +  " | propertyName: " + confirmData[0].propertyName + " | value: " + confirmData[0].value);
      });
  });
});