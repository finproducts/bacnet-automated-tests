import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let connDis = 'APP6527';

let bacnetConn = "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

let folder = '`BINARY_OUTPUT`';

let parameter = bacnetConn + "," + folder;

let po = new axonPO({"testQuery": "bacnetLearn("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadFolder_BINARY_OUTPUT_APP6527.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadFolder_BINARY_OUTPUT_APP6527", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "bacnetLearn("  +  parameter  +  ") returned an object which doesn\'t contain an " +
            'error.');

        let validArray = []

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.bacnetCur.includes('BO') === false && row.dis !== undefined && row.is === "BINARY_OUTPUT" && row.kind === "Bool" && row.point === "✓" && row.bacnetWrite !== undefined && row.bacnetWriteLevel !== undefined && row.enum !== undefined && testData.length === 15, "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetCur: " + row.bacnetCur + " | dis: " + row.dis + " | is: " + row.is + " | kind: " + row.kind + " | point: " + row.point + " | bacnetWrite: " + row.bacnetWrite + " | bacnetWriteLevel: " + row.bacnetWriteLevel + " | enum: " + row.enum);
        }

      });
  });

});