import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from 'protractor';
import {ok, equal, notEqual} from '../../../../../helpers/nwapis';
import {waitService} from '../../../../../helpers/wait-service';
import axonPO from '../../../../../page_objects/finstack/axon.page';

let expectedArray = [
  "TL1",
  "TL2",
  "TL3",
  "TL4",
  "TL5",
  "TL6",
  "TL7",
  "TL8"
];

let connDis = 'BAC-7301C_000033';

let bacnetConn = "read(bacnetConn and dis=="  +  JSON.stringify(connDis)  +  ")";

let folder = '`TREND_LOG`';

let parameter = bacnetConn + "," + folder;

let po = new axonPO({"testQuery": "bacnetLearn("  +  parameter  +  ")", "confirmQuery": ""});
po.setup(function (instance) {})

describe('testBacnetReadFolder_TREND_LOG_BAC-7301C_000033.js', () => {
  beforeEach(async() => {
    await po.open();
  });

  it("Testing bacnetReadFolder_TREND_LOG_BAC-7301C_000033", () => {
    po
      .assert(function (testData, confirmData) {
        ok(testData !== undefined && testData !== undefined, "bacnetLearn("  +  parameter  +  ") query result is not undefined or null.");

        ok(typeof testData === "object" && testData.type !== "error", "bacnetLearn("  +  parameter  +  ") returned an object which doesn\'t contain an ' +'error.");

        let validArray = [];

        let i;

        for (i = 0; i < testData.length; i++) {
          let row = testData[i]

          ok(row.bacnetHis !== undefined && row.dis !== undefined && row.is === "TREND_LOG" && row.kind === "Number" && row.point === "✓", "bacnetLearn(" + parameter + ") query contains valid data on row " + i + " => bacnetHis: " + row.bacnetHis + " | dis: " + row.dis + " | is: " + row.is + " | kind: " + row.kind + " | point: " + row.point);

          if (row.bacnetHis === "TL1" || row.bacnetHis === "TL2" || row.bacnetHis === "TL3" || row.bacnetHis === "TL4" || row.bacnetHis === "TL5" || row.bacnetHis === "TL6" || row.bacnetHis === "TL7" || row.bacnetHis === "TL8") {
            validArray.push(row.bacnetHis);
          }

        }

        ok(JSON.stringify(validArray.sort()) === JSON.stringify(expectedArray.sort()), "Query result contains the expected elements: " + JSON.stringify(validArray));
      });
  });

});