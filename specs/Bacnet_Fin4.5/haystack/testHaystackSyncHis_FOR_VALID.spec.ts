import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//argument passed to the axon function
let parameter = 'readAll(haystackHis), null';

let po = new axonPO({"testQuery": "haystackSyncHis("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The haystackSyncHis() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "haystackSyncHis("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "haystackSyncHis("+ parameter +") returned an object which doesn't contain an error.");

      if(JSON.stringify(testData) === "[]") {
        ok(JSON.stringify(testData) === "[]",
        "haystackSyncHis("+ parameter +") query result contain invalid data: "+ JSON.stringify(testData));
      }
      else {
        ok(testData[0].val.includes('sys::Err: Not a haystackConn:') === true, 
        "haystackSyncHis("+ parameter +") query result contain invalid data => val: "+ testData[0].val);
      }     
    });
  });  
});