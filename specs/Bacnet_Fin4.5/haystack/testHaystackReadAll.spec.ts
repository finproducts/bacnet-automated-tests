import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//filter
let filter = 'equip';

//the argument that will be passed to the axon faction
let parameter = haystackConnId +", "+ filter;

let po = new axonPO({"testQuery": "haystackReadAll("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The haystackReadAll() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "haystackReadAll("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "haystackReadAll("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined 
        && row.axSlotPath.includes(filter) === true 
        && row.axType === "nhaystack:HEquip" 
        && row.dis !== undefined 
        && row.navName !== true 
        && row.siteRef === "@S.Aquarium", 
        "haystackReadAll("+ parameter +") query results contain invalid data => id: "+ row.id +" | axType: "+
        row.axType +" | dis: "+ row.dis +" | navName: "+ row.navName +" | siteRef: "+ row.siteRef +" | axSlotPath: "+ row.axSlotPath);
      }
    });
  });  
});