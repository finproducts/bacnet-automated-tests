import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//filter
let filter = 'equip';

//column contained by the filter
let columnFilter = 'dis';

//argument passed to the axon faction
let parameter = haystackConnId +","+ JSON.stringify(filter) +","+ JSON.stringify(columnFilter);

let po = new axonPO({"testQuery": "finNHaystackReadUnique("+  parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackReadUnique.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it(' Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !==  null, "finNHaystackReadUnique("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackReadUnique("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row= testData[i];
        ok(row.count !== undefined 
        && row[columnFilter] !== undefined, 
        "finNHaystackReadUnique("+ parameter +") query results contain invalid data on row "+ i +" => count: "+ row.count 
        +" | "+ columnFilter +": "+ row[columnFilter]);
      }
    });
  });  
});