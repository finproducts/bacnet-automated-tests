import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//argument passed to the axon faction
let parameter = haystackConnId;

let po = new axonPO({"testQuery": "haystackPing("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testHaystackPing() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "haystackPing("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "haystackPing("+ parameter +") returned an object which doesn't contain an error.");

      let row = testData[0];
      ok(row.val !== undefined, "haystackPing("+ parameter +") query results contain invalid data => val: "+ row.val);
    });
  });  
});