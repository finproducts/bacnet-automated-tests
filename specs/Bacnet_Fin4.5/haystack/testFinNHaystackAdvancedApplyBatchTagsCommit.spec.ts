import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Form function part of Advanced String Editor

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//target filter
let targetFilter= "point";

//search tag
let searchTag= "navName";

//search function
let searchFunc= "contains";

//search string value
let searchVal= "SupplyAirTemp";

//tags to apply or remove
let tags= "navNameFormat:\"SAT\"";

//the argument that will be passed to the axon faction
let parameter= haystackConnId + "," + JSON.stringify(targetFilter) + "," + JSON.stringify(tags) + "," + JSON.stringify(searchTag) + ","  +  JSON.stringify(searchFunc)  +  ","  +  JSON.stringify(searchVal);



let po = new axonPO({"testQuery":"finNHaystackAdvancedApplyBatchTagsCommit("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackAdvancedApplyBatchTagsCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackAdvancedApplyBatchTagsCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackAdvancedApplyBatchTagsCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  ===  "$okButton"  &&  
  testData[0].finForm  ===  "✓"  && 
  testData[0].dis  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finNHaystackAdvancedApplyBatchTagsCommit("  +  parameter  +  ") query results contains valid data => body: "  +  testData[0].body  
  +  " | cancelButton: "  +  testData[0].cancelButton  
  +  " | form: "  +  testData[0].finForm  
  +  " | dis: "  +  testData[0].dis  
  +  " | name: "  +  testData[0].name);
}
);
  });  

});