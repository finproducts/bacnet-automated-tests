import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//Commit action for NHaystack Sync Equips

//haystack conn filter
let haystackConnFilter = 'haystackConn and dis=="Aquarium"';

//equips filter
let equipFilter = 'equip';

//update navName checkbox 
let updateNavName = 'false';

//argument passed to the axon faction
let parameter = JSON.stringify(haystackConnFilter) +","+ JSON.stringify(equipFilter) +","+ updateNavName;

let po = new axonPO({"testQuery": "finNHaystackSyncNiagaraEquipApply("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackSyncNiagaraEquipApply() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackSyncNiagaraEquipApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "finNHaystackSyncNiagaraEquipApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === "[{\"val\":null}]",
      "finNHaystackSyncNiagaraEquipApply("+ parameter +") query results contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});