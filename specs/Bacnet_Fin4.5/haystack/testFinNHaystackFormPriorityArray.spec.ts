import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK


//Haystack Priority Array

//Aquarium point id
let aquariumPointId = 'read(point and siteRef->dis=="Aquarium")->id';

//argument passed to the axon faction
let parameter = aquariumPointId;



let po = new axonPO({"testQuery":"finNHaystackFormPriorityArray("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackFormPriorityArray() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackFormPriorityArray("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackFormPriorityArray("  +  parameter  +  ") returned an object which doesn't contain an error.");

let row= testData[0]

ok(row.body  !==  undefined  &&  
  row.cancelButton  ===  "$okButton"  &&  
  row.dis  ===  "Haystack Priority Array"  &&  
  row.finForm  ===  "✓"  &&  
  row.name  ===  "foo",
  
  "finNHaystackFormPriorityArray("  +  parameter  +  ") query results contains valid data => body: "  +  row.body  
  +  " | cancelButton: "  +  row.cancelButton  
  +  " | dis: "  +  row.dis  
  +  " | finForm: "  +  row.finForm  
  +  " | name: "  +  row.name);
}
);
  });  

});