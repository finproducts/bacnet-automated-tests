import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK


//Commit function for Nhaystack Add Floor To Equip

//conn dict
let connDict= '[{dis:"Aquarium", moduleName:"nhaystack", connState:"open", uri:`http://localhost/haystack/`,'
+' id:@1f6519d7-01ce80a6, moduleVersion:"1.2.5.18", mod:parseDateTime("2016-09-09T08:04:11.523Z UTC"),'
+' tz:"Athens", haystackSlot:false, productName:"Niagara AX", equipFilter:"", haystackConn, connStatus:"ok",'
+' pointFilter:"", username:"admin", productVersion:"3.8.38"}]';


//name prefix
let namePrefix= "Floor";

//suffix / name
let suffix= "Test";

//argument passed to the axon function
let parameter= connDict  +  ","  +  JSON.stringify(namePrefix)  +  ","  +  JSON.stringify(suffix);



let po = new axonPO({"testQuery":"finAddFloorToEquipCommit("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finAddFloorToEquipCommit_DOESNT_WORK() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finAddFloorToEquipCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finAddFloorToEquipCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finAddFloorToEquipCommit("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});