import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//IMPORTANT - need to run a niagara job first : finNHaystackUniqueEquipTypes
//more info - https://finproducts.atlassian.net/wiki/display/FINStack/Find+Unique+Equip+Types

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//the argument that will be passed to the axon faction
let parameter = haystackConnId;

let po = new axonPO({"testQuery": "finNHaystackUniqueEquipTypes("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackUniqueEquipTypes() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      &&  testData !== undefined, "finNHaystackUniqueEquipTypes("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "finNHaystackUniqueEquipTypes("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];
        ok(row.id !== undefined 
        && row.equipIds === "@S.Aquarium.AHU_1,@S.Aquarium.AHU_2,@S.Aquarium.AHU_3,@S.Aquarium.AHU_4" 
        && row.numPoints >= 0 
        && row.numEquips === 4 
        && row.similarTypes >= 0 
        && row.equipType >= 0,
        "finNHaystackUniqueEquipTypes("+ parameter +") query results contain invalid data on row "+ i +" => id: "+ row.id 
        +" | equipIds: "+ row.equipIds +" | numPoints: "+ row.numPoints +" | numEquips: "+ row.numEquips 
        +" | similarTypes: "+ row.similarTypes +" | equipType: "+ row.equipType);
      }
    });
  });  
});