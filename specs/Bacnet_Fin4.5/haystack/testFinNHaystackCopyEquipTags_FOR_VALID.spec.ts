import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//Commit action for Clone Equip-Point Tags

//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//equip that will be copied
let copyEquip ="@S.Aquarium.AHU_1";

//target filter
let targetFilter = "test";

//the argument that will be passed to the axon faction
let parameter = haystackConnId +", "+ copyEquip +","+ JSON.stringify(targetFilter);



let po = new axonPO({"testQuery":"finNHaystackCopyEquipTags("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackCopyEquipTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackCopyEquipTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackCopyEquipTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[]",
"finNHaystackCopyEquipTags("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});