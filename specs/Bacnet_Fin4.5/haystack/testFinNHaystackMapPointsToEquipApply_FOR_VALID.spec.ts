import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//Commit function for Nhaystack Map Points To Equip

//conn dict
let connDict = '[{dis:"Aquarium", moduleName:"nhaystack", connState:"open", uri:`http://localhost/haystack/`,'
+' id:@1f6519d7-01ce80a6, moduleVersion:"1.2.5.18", mod:parseDateTime("2016-09-09T08:04:11.523Z UTC"),'
+' tz:"Athens", haystackSlot:false, productName:"Niagara AX", equipFilter:"", haystackConn, connStatus:"ok",'
+' pointFilter:"", username:"admin", productVersion:"3.8.38"}]';

let filter1 = '';
let filter2 = '';

//argument passed to the axon function
let parameter = connDict + "," + JSON.stringify(filter1) +","+ JSON.stringify(filter2);

let po = new axonPO({"testQuery": "finNHaystackMapPointsToEquipApply("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The finNHaystackMapPointsToEquipApply() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackMapPointsToEquipApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackMapPointsToEquipApply("+ parameter +") returned an object which doesn't contain an error.");

      ok(JSON.stringify(testData) === '[{"val":null}]', 
      "finNHaystackMapPointsToEquipApply("+ parameter +") query result contain invalid data: "+ JSON.stringify(testData));
    });
  });  
});