import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK


//Commit action for NHaystack Apply Batch Tags


//haystack filter
let haystack= '[{uri:`http://localhost/haystack/`, haystackSlot:false, productName:"Niagara AX",'
+' equipFilter:"", dis:"Aquarium", productVersion:"3.8.38", id:@1f77b19f-2faf9ef1,'
+' haystackConn, moduleName:"nhaystack", pointFilter:"", connState:"open", moduleVersion:"1.2.5.18",'
+' username:"admin", mod:parseDateTime("2016-09-23T09:26:45.908Z UTC"), tz:"Athens", connStatus:"ok"}]';

//tag add or remove
let tag= 'selenium';

//argument passed to the axon faction
let parameter= haystack  +  ","  +  JSON.stringify(tag);



let po = new axonPO({"testQuery":"finCommitNHaystackApplyBatchTags_DOESNT_WORK("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finCommitNHaystackApplyBatchTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCommitNHaystackApplyBatchTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCommitNHaystackApplyBatchTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCommitNHaystackApplyBatchTags("  +  parameter  +  ") query results contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});