import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//Commit action for Advanced String Editor

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//target filter
let targetFilter= 'point';

//tags to apply or remove
let tags = 'navNameFormat:"SAT"';

//search string value
let searchVal = "[@C.Logic.HousingUnit.AirHandler.SupplyAirTemp]";

let parameter = haystackConnId +", "+ JSON.stringify(targetFilter) +","+ JSON.stringify(tags) +","+ searchVal;



let po = new axonPO({"testQuery":"finNHaystackApplyBatchTags("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackApplyBatchTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNHaystackApplyBatchTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNHaystackApplyBatchTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].rowsChanged  ===  1,
  "finNHaystackApplyBatchTags("  +  parameter  +  ") query results contains valid data => rowsChanged: "  +  testData[0].rowsChanged);
}
);
  });  

});