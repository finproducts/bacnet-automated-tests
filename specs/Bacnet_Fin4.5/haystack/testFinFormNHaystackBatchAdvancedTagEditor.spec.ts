import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Haystack Batch Advanced Tag Editor


let po = new axonPO({"testQuery":"finFormNHaystackBatchAdvancedTagEditor()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormNHaystackBatchAdvancedTagEditor() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormNHaystackBatchAdvancedTagEditor() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormNHaystackBatchAdvancedTagEditor() returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].dis  ===  "Batch Advanced Haystack Tag Editor"  &&  
  testData[0].name  ===  "finFormNHaystackBatchAdvancedTagEditor"  &&  
  testData[0].cancelButton  ===  "$cancelButton"  &&  
  testData[0].commitAction.includes('finFormNHaystackBatchAdvancedTagEditor')  ===  true  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].commitButton  ===  "$applyButton",
  
  "finFormNHaystackBatchAdvancedTagEditor() query results contains valid data => body: "  +  testData[0].body  
  +  " | cancelButton: "  +  testData[0].cancelButton  
  +  " | commitAction: "  +  testData[0].commitAction  
  +  " | commitButton: "  +  testData[0].commitButton  
  +  " | dis: "  +  testData[0].dis  
  +  " | finForm: "  +  testData[0].finForm  
  +  " | name: "  +  testData[0].name);
}
);
  });  

});