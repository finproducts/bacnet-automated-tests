import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//the argument that will be passed to the axon faction
let parameter = haystackConnId;

let po = new axonPO({"testQuery": "finNHaystackShowWatches("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackShowWatches()',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData)  {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackShowWatches("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "finNHaystackShowWatches("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].id !== undefined 
      && testData[0].lastPoll !== undefined 
      && testData[0].watchCount >= 0, 
      "finNHaystackShowWatches("+ parameter +") query results contain invalid data => id: "+ testData[0].id 
      +" | lastPoll: "+ testData[0].lastPoll +" | watchCount: "+ testData[0].watchCount);
    });
  });  
});