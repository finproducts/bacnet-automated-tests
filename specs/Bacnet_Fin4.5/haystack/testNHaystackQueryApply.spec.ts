import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';

//Commit action for NHaystack Query

//haystack conn id: "Aquarium"
let haystackConn = 'haystackConn and dis=="Aquarium"';

//filter
let targetFilter = 'point';

//search tag
let searchTag = 'dis';

//search function
let searchFunc = 'contains';

//search string value
let searchVal = 'Bool';

//the argument that will be passed to the axon faction
let parameter = JSON.stringify(haystackConn) +", "+ JSON.stringify(targetFilter) +","+
JSON.stringify(searchTag) +","+ JSON.stringify(searchFunc) +","+ JSON.stringify(searchVal);

let po = new axonPO({"testQuery": "nHaystackQueryApply("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('The NHaystackQueryApply() query',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "nHaystackQueryApply("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !==  "error", "nHaystackQueryApply("+ parameter +") returned an object which doesn't contain an error.");

      for(let i = 0; i < testData.length; i++) {
        let row = testData[i];

        ok(row.id !== undefined 
        && row.axSlotPath !== undefined 
        && row.axType.includes('Boolean') === true 
        && row.connRefDis === "Aquarium" 
        && row.cur === "✓" 
        && row.dis !== undefined 
        && row.kind === "Bool" 
        && row.navName !== undefined, 
        "nHaystackQueryApply("+ parameter +") query results contain invalid data => id: "+ testData[0].id +" | axSlotPath: "+
        row.axSlotPath +" | axType: "+ row.axType +" | connRefDis: "+ row.connRefDis +" | cur: "+ row.cur +" | dis: "+
        row.dis +" | kind: "+ row.kind +" | navName: "+ row.navName);
      }
    });
  });  
});