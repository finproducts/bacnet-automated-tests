import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//haystack conn id: "Aquarium"
let haystackConnId = 'read(haystackConn and dis=="Aquarium")->id';

//filter
let filter = 'equip';

//the argument that will be passed to the axon faction
let parameter= haystackConnId + "," + JSON.stringify(filter);

let po = new axonPO({"testQuery": "finNHaystackReadSize("+ parameter +")", "confirmQuery": ""});
po.setup(function(instance) {});

describe('testFinNHaystackReadSize() query',() => {
  beforeEach(async () => {
    await po.open();
  });

  it('Returns the expected results when executed', () => {
    po.assert(function(testData,confirmData) {
      ok(testData !== undefined 
      && testData !== null, "finNHaystackReadSize("+ parameter +") query result is not undefined or null.");

      ok(typeof testData === "object" 
      && testData.type !== "error", "finNHaystackReadSize("+ parameter +") returned an object which doesn't contain an error.");

      ok(testData[0].size !== undefined, 
      "finNHaystackReadSize("+ parameter +") query results contain invalid data => size: "+ testData[0].size);
    });
  });  
});