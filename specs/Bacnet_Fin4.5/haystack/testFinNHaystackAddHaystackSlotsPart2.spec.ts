import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Commit action for Add Haystack Slot

//use in conjunction with finNHaystackDeleteHaystackSlot()

//haystack conn id: "Aquarium"
let haystackConnId= 'read(haystackConn and dis=="Aquarium")->id';

//point navName on which the haystack slot will be added: TestHaystackSlot
let pointName= "SAT";

//the argument that will be passed to the axon faction
let parameter= haystackConnId  +  ", point and navName=="  +  JSON.stringify(pointName);



let po = new axonPO({"testQuery":"haystackRead("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNHaystackAddHaystackSlotsPart2() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "haystackRead("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"haystackRead("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].axAnnotated  ===  "✓",
"haystackRead("  +  parameter  +  ") query results contains valid data => axAnnotated: "  +  testData[0].axAnnotated);
}
);
  });  

});