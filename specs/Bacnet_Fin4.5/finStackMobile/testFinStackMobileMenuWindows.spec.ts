import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Windows 7 - platform: Windows
let parameter= 'null, null, null, {platformOs:"Windows Server 2008 R2 7", userAgent:"Mozilla 5.0 (Windows NT 6.1; WOW64)"  +  "AppleWebKit/537.36 (KHTML, like Gecko) Chrome 47.0.2526.111 Safari 537.36"}';



let po = new axonPO({"testQuery":"finStackMobileMenu("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finStackMobileMenu_Windows 7() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finStackMobileMenu("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finStackMobileMenu("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.badgeType  ===  "normal"  &&  row.badgeValue  >=  0  &&  row.displayName  !==  undefined  &&  row.hasBadge  !==  undefined  &&  row.iconType  !==  undefined  &&  row.minUiTarget  !==  undefined  &&  row.ui  !==  undefined,"finStackMobileMenu("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => action: "  +  row.action  +  " | appId: "  +  row.appId  +  " | badgeType: "  +  row.badgeType  +  " | classes: "  +  row.classes  +  " | displayName: "  +  row.displayName  +  " | icon: "  +  row.icon  +  " | minUiTarget: "  +  row.minUiTarget);
}

}
);
  });  

});