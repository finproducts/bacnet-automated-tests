import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Blackberry Z30 - platform: Blackberry
let parameter= 'null, null, null, {platformOs:"BlackBerry", userAgent:"Mozilla 5.0 (BB10; Touch)"  +  "AppleWebKit 537.10+ (KHTML, like Gecko) Version 10.0.9.2372 Mobile Safari 537.10+"}';



let po = new axonPO({"testQuery":"finStackMobileMenu("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finStackMobileMenu_Blackberry() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finStackMobileMenu("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finStackMobileMenu("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(testData[0].appId  ===  "alarms"  &&  
testData[1].appId  ===  "graphics"  &&  
testData[2].appId  ===  "trends"  &&  
testData[3].appId  ===  "notes"  &&  
testData[4].appId  ===  "points"  &&  
testData[5].appId  ===  "summary"  &&  
row.displayName  !==  undefined  &&  
row.appId  !==  undefined  &&  
row.badgeType  ===  "normal"  &&  
row.badgeValue  >=  0  &&  
row.displayName  !==  undefined  &&  
row.hasBadge  !==  undefined  &&  
row.iconType  !==  undefined  &&  
row.minUiTarget  !==  undefined  &&  
row.ui  !==  undefined,

"finStackMobileMenu("  +  parameter  +  ") query results contains valid data on row "  +  i  
+  " => action: "  +  row.action  
+  " | appId: "  +  row.appId 
+  " | badgeType: "  +  row.badgeType  
+  " | classes: "  +  row.classes  
+  " | displayName: "  +  row.displayName  
+  " | icon: "  +  row.icon  
+  " | minUiTarget: "  +  row.minUiTarget);
}

}
);
  });  

});