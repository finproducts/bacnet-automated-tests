import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//summary name
let summaryName= "Selenium Summary";

//filter
let filter= "equip";

//run on
let runOn= "ahu";

//summary full name (validation)
let summaryFullName= runOn.charAt(0).toUpperCase()  +  runOn.slice(1)  +  " "  +  summaryName;

//argument passed to the axon function
let parameter= JSON.stringify(summaryName)  +  ", ["  +  JSON.stringify(runOn)  +  "], [],"  +  JSON.stringify(filter)  +  ", []";



let po = new axonPO({"testQuery":"finFormCreateSummaryApply("  +  parameter  +  ")","confirmQuery":"read(summaryOn and dis=="  +  JSON.stringify(summaryFullName)  +  ")"});
po.setup(function(instance)
{}
)

describe("The finFormCreateSummaryApply() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormCreateSummaryApply("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormCreateSummaryApply("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]","finFormCreateSummaryApply("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].dis  ===  summaryFullName  &&  
  confirmData[0].equipFilter.includes(runOn)  ===  true  &&  
  confirmData[0].mod  !==  undefined  &&  
  confirmData[0].pointFilter  !==  undefined  &&  
  confirmData[0].summaryOn  ===  "ahu",
  
  "read(summaryOn and dis=="  +  JSON.stringify(summaryFullName)  +  ") query results contains valid data => id: "  +  
  confirmData[0].id  +  " | dis: "  +  
  confirmData[0].dis  +  " | equipFilter: "  +  
  confirmData[0].equipFilter  +  " | mod: "  +  
  confirmData[0].mod  +  " | pointFilter: "  +  
  confirmData[0].pointFilter  +  " | summaryOn: "  +  
  confirmData[0].summaryOn  +  ""  +  "New summary was successfully created: "  +  
  confirmData[0].dis);
}
);
  });  

});