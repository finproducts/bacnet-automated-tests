import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//City Center point id
let pointId= 'id==@1eeb0a1b-e62962c7';

//2nd parameter
let paramArray= ["true", "false"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let useUnique= paramArray[randomIndex];

//argument passed to the axon function
let parameter= JSON.stringify(pointId)  +  ","  +  useUnique;



let po = new axonPO({"testQuery":"finSaveRecordsSeparate("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finSaveRecordsSeparate() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSaveRecordsSeparate("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSaveRecordsSeparate("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finSaveRecordsSeparate("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});