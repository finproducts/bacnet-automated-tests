import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

//After each running of the test, run to folioRestore !

//trio file uri: contains the axon functions created for testing purpose of serialization an deserialization
let trioUri = "`io/serializationAndDeserialization.trio`";

//argument passed to the axon function
let parameter= trioUri;

//confirming query
let paramArray= ["hardcodedObj", "mirror", "internalObjValidation"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let funcName= paramArray[randomIndex];



let po = new axonPO({"testQuery":"finPostTrioFuncs("  +  parameter  +  ")","confirmQuery":"finPostTrioFuncs("  +  parameter  +  ")"});
po.setup(function(instance)
{}
)

describe("The finPostTrioFuncs() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
//testing finPostTrioFuncs(parameter)
ok(testData  !==  undefined  &&  testData  !==  undefined,'finPostTrioFuncs("  +  parameter  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'finPostTrioFuncs("  +  parameter  +  ") returned an object which doesn\'t contain an error.');

//testData testing
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.name  !==  undefined  &&  row.insert  ===  "✓",
"finPostTrioFuncs("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => name: "  +  row.name  +  " | insert: "  +  row.insert);
}


console.log('Validating the second test scenario:');

//confirmData testing
let j;

for(j  =  0; j  <  confirmData.length; j++)
{
let row= confirmData[j]

ok(row.name  !==  undefined  &&  row.update  ===  "✓",
"finPostTrioFuncs("  +  parameter  +  ") query result contains valid data on row "  +  j  +  " => name: "  +  row.name  +  " | update: "  +  row.update);
}

}
);
  });  

});