import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["point", "schedule"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll("  +  randomParameter  +  ")[0]->id";



let po = new axonPO({"testQuery":"finReadCloneEvents("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finReadCloneEvents() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finReadCloneEvents("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finReadCloneEvents("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( JSON.stringify(testData)  ===  "[]") 
      {
ok(JSON.stringify(testData)  ===  "[]",
"finReadCloneEvents("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData)  +  ". No cloned events present.");
}

    
}
);
  });  

});