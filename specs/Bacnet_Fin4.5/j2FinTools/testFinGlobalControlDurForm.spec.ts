import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';

//OK


//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 50;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();
let value= createRandomNumber();
let units= createRandomNumber();

//array of possible point kind
let kindArray= ["bool", "number", "enum", "str"];
let randomIndexKind= Math.floor(Math.random()  *  kindArray.length);
let kind= kindArray[randomIndexKind];

//array of possible actions
let actionsArray= ["{add}", "{update}", "{delete}"];
let randomIndexActions= Math.floor(Math.random()  *  actionsArray.length);
let action= actionsArray[randomIndexActions];

//argument passed to the axon function
let parameter= "readAll(point)["  +  index  +  "]->id, "  +  action  +  ", "  +  value  +  ", "  +  units  +  ", "  +  JSON.stringify(kind);



let po = new axonPO({"testQuery":"finGlobalControlDurForm("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGlobalControlDurForm() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlDurForm("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlDurForm("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].width  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finGlobalControlDurForm("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | width: "  +  
  testData[0].width  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});