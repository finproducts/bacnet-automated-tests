import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["point", "floor", "site"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "read("  +  randomParameter  +  ")";



let po = new axonPO({"testQuery":"finNavMetaDeleteFailForm("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finNavMetaDeleteFailForm() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavMetaDeleteFailForm("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavMetaDeleteFailForm("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].name  !==  undefined,
  
  "finNavMetaDeleteFailForm("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});