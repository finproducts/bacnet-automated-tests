import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//1st param (id==@1a169f42-97348ed4 -> City Center AHU-1 CHWV)
let paramArray= ["point", "id==@1a169f42-97348ed4"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let filter= paramArray[randomIndex];

//zinc file name
let zincName= "point";

//argument passed to the axon function
let parameter= JSON.stringify(filter)  +  ","  +  JSON.stringify(zincName);



let po = new axonPO({"testQuery":"finSaveRecords("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finSaveRecords() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSaveRecords("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSaveRecords("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].size  !==  undefined,"finSaveRecords("  +  parameter  +  ") query result contains valid data => size: "  +  testData[0].size);
}
);
  });  

});