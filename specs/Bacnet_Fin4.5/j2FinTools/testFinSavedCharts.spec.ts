import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//The function takes the id of any level on which the charts are created.

//1nd param => level
let paramArray= ["site", "equip"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let level= paramArray[randomIndex];

//number generator
function createRandomNumber() {

	var min = 0;
	var max = 3;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

//level index
let index= createRandomNumber();

//argument passed to the axon function
let parameter= "readAll("  +  level  +  ")["  +  index  +  "]->id";



let po = new axonPO({"testQuery":"finSavedCharts("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finSavedCharts() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSavedCharts("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSavedCharts("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.chartOn  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.savedChart  !==  undefined,
  
  "finSavedCharts("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | chartOn: "  +  
  row.chartOn  +  " | mod: "  +  
  row.mod  +  " | savedChart: "  +  
  row.savedChart);
}

}
);
  });  

});