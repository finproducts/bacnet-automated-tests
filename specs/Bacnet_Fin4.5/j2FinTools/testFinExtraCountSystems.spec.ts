import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let equip= 'read(ahu)';

let parameter= equip;



let po = new axonPO({"testQuery":"finExtraCountSystems("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finExtraCountSystems() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finExtraCountSystems("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finExtraCountSystems("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.count  >=  0  &&  row.text  !==  undefined,
  "finExtraCountSystems("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => count: "  +  
  row.count  +  " | text: "  +  row.text);
}

}
);
  });  

});