import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["ahu", "point", "vav"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= JSON.stringify(randomParameter);



let po = new axonPO({"testQuery":"finBatchDeleteTagProvider("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finBatchDeleteTagProvider() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchDeleteTagProvider("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchDeleteTagProvider("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.count  !==  undefined  &&  
  row.kind  !==  undefined  &&  
  row.name  !==  undefined,
  
  "finBatchDeleteTagProvider("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => count: "  +  
  row.count  +  " | kind: "  +  row.kind  +  " | name: "  +  row.name);
}

}
);
  });  

});