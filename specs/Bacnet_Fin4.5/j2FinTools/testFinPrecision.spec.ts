import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//precision
function myToPrecision(value, precision) {
	
    var scalar = Math.pow(10, precision);
    return Math.trunc(scalar * value)/scalar;
};

//number generator
function createRandomNumber() {

	var min = 0;
	var max = 9;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomPrecision= Math.floor(createRandomNumber());
let randomNumber= createRandomNumber();

//argument passed to the axon function
let randomParameter= randomNumber  +  ", "  +  randomPrecision;



let po = new axonPO({"testQuery":"finPrecision("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finPrecision() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finPrecision("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finPrecision("  +  randomParameter  +  ") returned an object which doesn\'t contain an error.");

ok(testData[0].val  ===  myToPrecision(randomNumber,randomPrecision),
"finPrecision("  +  randomParameter  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});