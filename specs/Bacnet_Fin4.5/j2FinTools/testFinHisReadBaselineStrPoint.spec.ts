import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';




//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 3;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//array of possible arguments
let paramArray= ["hour", "minute", "second"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let timeUnit= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "hisRead(readAll(point)["  +  index  +  "]->id, now()-1"  +  timeUnit  +  "..now()), readAll(point)["  +  index  +  "]->navName";



let po = new axonPO({"testQuery":"finHisReadBaselineStrPoint("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHisReadBaselineStrPoint() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHisReadBaselineStrPoint("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHisReadBaselineStrPoint("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.ts  !==  undefined,
  "finHisReadBaselineStrPoint(" +  parameter + ") query result contains valid data on row " +  i +  " => ts: " + row.ts + " | v0: "  +  row.v0);
}

}
);
  });  

});