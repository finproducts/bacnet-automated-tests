import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//folio restore
let folioRestoreParam= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

//string generator
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let newTagToApply= createRandomString();

let parameter= '"ahu",'  +  JSON.stringify(newTagToApply);



let po = new axonPO({"testQuery":"finCommitApplyBatchTags("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  folioRestoreParam  +  ")"});
po.setup(function(instance)
{}
)

describe("The finCommitApplyBatchTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCommitApplyBatchTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCommitApplyBatchTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.siteRef  !==  undefined  &&  
  row[newTagToApply]  ===  "✓",
  
  "finCommitApplyBatchTags("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | mod: "  +  
  row.mod  +  " | navName: "  + 
  row.navName  +  " | siteRef: "  +  
  row.siteRef  +  " | "  +  
  newTagToApply  +  ": "  +  
  row[newTagToApply]);
}

//running folioRestore  
//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore...DB builder restored to default.");
}
);
  });  

});