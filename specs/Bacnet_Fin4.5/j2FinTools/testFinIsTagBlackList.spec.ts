import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//run finTagBlackList() to check if a tag is blacklisted

//array of possible arguments
let paramArray= ["ahu", "point"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= JSON.stringify(randomParameter);



let po = new axonPO({"testQuery":"finIsTagBlackList("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finIsTagBlackList() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finIsTagBlackList("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finIsTagBlackList("  +  parameter  +  ") returned an object which doesn\'t contain an error.");

if( randomParameter  ===  "ahu") 
      {
ok(testData[0].val  ===  false,
  "finIsTagBlackList("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val  +  ". Tag is not blacklisted.");
}

    else 
        if( randomParameter  ===  "point") 
      {
ok(testData[0].val  === true,
  "finIsTagBlackList("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val  +  ". Tag is blacklisted.");
}

    
      
}
);
  });  

});