import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["haystack", "bacnet"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let conn= paramArray[randomIndex];

//argument passed to the axon function
let parameter= JSON.stringify(conn);



let po = new axonPO({"testQuery":"finValidConnectors("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finValidConnectors() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finValidConnectors ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finValidConnectors ("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( conn  ===  "haystack") 
      {
ok(testData[0].id  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].connState  !==  undefined  &&  
  testData[0].connStatus  !==  undefined  &&  
  testData[0].haystackConn  ===  "✓"  &&  
  testData[0].uri.includes('haystack')  ===  true,
  "finValidConnectors ("  +  parameter  +  ") query result contains valid data => id: "  +  
  testData[0].id  +  " | dis: "  +  
  testData[0].dis  +  " | connState: "  +  
  testData[0].connState  +  " | connStatus: "  +  
  testData[0].connStatus  +  " | haystackConn: "  +  
  testData[0].haystackConn  +  " | uri: "  +  
  testData[0].uri);
}

    else 
        if( conn  ===  "bacnet") 
      {
ok(testData[0].id  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].connState  !==  undefined  &&  
  testData[0].connStatus  !==  undefined  &&  
  testData[0].bacnetConn  ===  "✓"  &&  
  testData[0].uri.includes('bacnet')  ===  true,
  
  "finValidConnectors ("  +  parameter  +  ") query result contains valid data => id: "  +  
  testData[0].id  +  " | dis: "  +  
  testData[0].dis  +  " | connState: "  +  
  testData[0].connState  +  " | connStatus: "  +  
  testData[0].connStatus  +  " | bacnetConn: "  +  
  testData[0].haystackConn  +  " | uri: "  +  
  testData[0].uri);
}

    
      
}
);
  });  

});