import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a string
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let debug= createRandomString();

let name= createRandomString();

let parameter= JSON.stringify(debug)  +  ","  +  JSON.stringify(name);



let po = new axonPO({"testQuery":"finConnDebugForm("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finConnDebugForm() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finConnDebugForm("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finConnDebugForm("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body.includes(debug)  ===  true  &&  
testData[0].body.includes(name)  ===  true  &&  
testData[0].commitAction  !==  undefined  &&  
testData[0].commitButton  !==  undefined  &&  
testData[0].dis  !==  undefined  &&  
testData[0].finForm  ===  "✓"  &&  
testData[0].helpDoc  !==  undefined  &&  
testData[0].name  !==  undefined,

"finConnDebugForm("  +  parameter  +  ") query result contains valid data => body: "  +  
testData[0].body  +  " | commitAction: "  +  
testData[0].commitAction  +  " | commitButton: "  +  
testData[0].commitButton  +  " | dis: "  +  
testData[0].dis  +  " | finForm: "  +  
testData[0].finForm  +  " | helpDoc: "  +  
testData[0].helpDoc  +  " | name: "  +  
testData[0].name);
}
);
  });  

});