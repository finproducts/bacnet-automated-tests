import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//function that randomly creates a string of 10 characters
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let randomString= createRandomString();

//argument passed to the axon function
let randomParameter= JSON.stringify(randomString)  +  ', false, ""';

//undo changes
let undoChanges= JSON.stringify(randomString)  +  ', true, ""';



let po = new axonPO({"testQuery":"finNavMetaCommit("  +  randomParameter  +  ")","confirmQuery":"finNavMetaCommit("  +  undoChanges  +  ")"});
po.setup(function(instance)
{}
)

describe("The finNavMetaCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finNavMetaCommit("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finNavMetaCommit("  +  randomParameter  +  ") returned an object which doesn\'t contains an error.");

ok(testData[0].val  ===  (randomString +" was created successfully"),
"finNavMetaCommit("  +  randomParameter  +  ") query results contains valid data => val: "  +  testData[0].val);

ok(confirmData[0].val  ===  (randomString +" was successfully removed"),
"finNavMetaCommit("  +  undoChanges  +  ") query results contain valid data => val: "  +  confirmData[0].val);
}
);
  });  

});