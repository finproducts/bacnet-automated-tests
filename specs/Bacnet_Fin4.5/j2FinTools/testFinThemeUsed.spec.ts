import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery":"finThemeUsed()","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finThemeUsed() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finThemeUsed() query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finThemeUsed() returned an object which doesn't contain an error.");

ok(testData[0].accentColor  !==  undefined  &&  
  testData[0].accentColorStroke  !==  undefined  &&  
  testData[0].buttonDown  !==  undefined  &&  
  testData[0].buttonMouseOver  !==  undefined  &&  
  testData[0].highlight  !==  undefined  &&  
  testData[0].logo  !==  undefined  &&  
  testData[0].textBGStroke  !==  undefined  &&  
  testData[0].windowHeader  !==  undefined,
  
  "finThemeUsed() query results contains valid data => accentColor: "  +  
  testData[0].accentColor  +  " | accentColorStroke: "  +  
  testData[0].accentColorStroke  +  " | buttonDown: "  +  
  testData[0].buttonDown  +  " | buttonMouseOver: "  +  
  testData[0].buttonMouseOver  +  " | highlight: "  +  
  testData[0].highlight  +  " | logo: "  +  
  testData[0].logo  +  " | textBGStroke: "  +  
  testData[0].textBGStroke  +  " | windowHeader: "  +  
  testData[0].windowHeader);
}
);
  });  

});