import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//Aquarium floor id
let selectedId= 'read(floor and siteRef->dis=="Aquarium")->id';

//connector type
let connType= "haystack";

//existing connectors
let existingConnectors= "read(haystackConn)->id";

//valid connectors
let validConnector= "";

//argument passed to the axon function
let parameter= "["  +  selectedId  +  "], ["  +  JSON.stringify(connType)  +  "], ["  +  existingConnectors  +  "], ["  +  validConnector  +  "]";



let po = new axonPO({"testQuery":"finApplyUpdateConnectors("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finApplyUpdateConnectors() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finApplyUpdateConnectors("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finApplyUpdateConnectors("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finApplyUpdateConnectors("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});