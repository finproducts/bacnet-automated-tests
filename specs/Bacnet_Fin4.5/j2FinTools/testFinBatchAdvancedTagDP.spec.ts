import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//1st param
let paramArray= ["true", "false"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let num= paramArray[randomIndex];

//2nd param
let sec= "contains";

//3rd param
let xtra= 0;

//argument passed to the axon function
let parameter= "["  +  num  +  "][0], "  +  JSON.stringify(sec)  +  ","  +  xtra;



let po = new axonPO({"testQuery":"finBatchAdvancedTagDP("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finBatchAdvancedTagDP() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
  	//testing finBatchAdvancedTagDP(parameter)
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchAdvancedTagDP("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchAdvancedTagDP("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.contains  !==  undefined  &&  
  row.filter  !==  undefined  &&  
  row.selected  !==  undefined  &&  
  row.tagsToAdd  !==  undefined,
  
  "finBatchAdvancedTagDP("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => contains: "  +  
  row.contains  +  " | filter: "  +  
  row.filter  +  " | selected: "  +  
  row.selected  +  " | tagsToAdd: "  +  
  row.tagsToAdd);
}

}
);
  });  

});