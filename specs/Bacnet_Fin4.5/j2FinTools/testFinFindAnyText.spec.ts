import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let tag= 'equip';

let textParamArray= ["City Center" ,"Tower", "Laguna"];

let randomIndex= Math.floor(Math.random()  *  textParamArray.length);

let text= textParamArray[randomIndex];



let po = new axonPO({"testQuery":"readAll("  +  tag  +  ").finFindAnyText("  +  JSON.stringify(text)  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFindAnyText() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "readAll("  +  tag  +  ").finFindAnyText("  +  JSON.stringify(text)  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"readAll("  +  tag  +  ").finFindAnyText("  +  JSON.stringify(text)  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.disMacro  ===  "$siteRef $navName"  &&  
  row.floorRef  !==  undefined  &&  
  row.navName  !==  undefined  &&  
  row.siteRefDis  !==  undefined  &&  
  row.mod  !==  undefined,
  
  "readAll("  +  tag  +  ").finFindAnyText("  +  JSON.stringify(text)  +  ") query results contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | navName: "  +  
  row.navName  +  " | siteRefDis: "  +  
  row.siteRefDis  +  " | mod: "  +  
  row.mod);
}

}
);
  });  

});