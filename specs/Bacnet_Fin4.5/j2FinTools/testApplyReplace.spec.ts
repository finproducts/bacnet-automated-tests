import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//to undo changes: applyReplace("point and navName==\"SELENIUM\" and siteRef->dis==\"City Center\"","navName","SELENIUM","CHWV",[])

//filter => CHWV points from City Center
let filter= 'point and navName==\"CHWV\" and siteRef->dis==\"City Center\"';

//tag
let tag= "navName";

//text to be replaced
let searchText= "CHWV";

//new text
let replaceText= "SELENIUM";

//argument passed to the axon function
let parameter= JSON.stringify(filter)  +  ","  +  JSON.stringify(tag)  +  ","  +  JSON.stringify(searchText)  +  ","  +  JSON.stringify(replaceText)  +  ",[]";



let po = new axonPO({"testQuery":"applyReplace("  +  parameter  +  ")","confirmQuery":"point and navName=="  +  JSON.stringify(replaceText)});
po.setup(function(instance)
{}
)

describe("The applyReplace() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "applyReplace("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"applyReplace("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.cool  ===  "✓"  &&  
  row.cur  ===  "✓"  &&  
  testData.length  ===  4  &&  
  row.curVal  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.equipRef  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.siteRef  !==  undefined  &&  
  row.navName  ===  replaceText,
  
  "applyReplace("  +  parameter  +  ") query result contains valid data => id: "  +  
  row.id  +  " | cool: "  +  
  row.cool  +  " | cur: "  +  
  row.cur  +  " | curVal: "  +  
  row.curVal  +  " | disMacro: "  +  
  row.disMacro  +  " | equipRef: "  +  
  row.equipRef  +  " | floorRef: "  +  
  row.floorRef  +  " | siteRef: "  +  
  row.siteRef  +  " | navName: "  +  
  row.navName  +  ""  +  "Navname has been successfully changed. New value: "  +  replaceText);
}


for(i  =  0; i  <  confirmData.length; i++)
{
let row= confirmData[i]

ok(row.dis.includes(replaceText)  ===  true  &&  
confirmData.length  ===  4  &&  
row.id  !==  undefined  &&  
row.cool  ===  "✓"  &&  
row.cur  ===  "✓"  &&  
row.curVal  !==  undefined  &&  
row.disMacro  !==  undefined  &&  
row.equipRef  !==  undefined  &&  
row.floorRef  !==  undefined  &&  
row.siteRef  !==  undefined  &&  
row.navName  ===  replaceText,

"point and navName=="  +  JSON.stringify(replaceText)  +  " query result contains valid data => id: "  +  
row.id  +  " | cool: "  +  
row.cool  +  " | cur: "  +  
row.cur  +  " | curVal: "  +  
row.curVal  +  " | disMacro: "  +  
row.disMacro  +  " | equipRef: "  +  
row.equipRef  +  " | floorRef: "  +  
row.floorRef  +  " | siteRef: "  +  
row.siteRef  +  " | navName: "  +  
row.navName);
}

}
);
  });  

});