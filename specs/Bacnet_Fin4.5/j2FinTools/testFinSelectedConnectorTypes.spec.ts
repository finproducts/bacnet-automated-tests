import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 5;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//array of possible arguments
let paramArray= ["Aquarium", "BACnet"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let connName= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "[readAll(point and siteRef ->dis =="  +  JSON.stringify(connName)  +  ")["  +  index  +  "]->id]";



let po = new axonPO({"testQuery":"finSelectedConnectorTypes("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finSelectedConnectorTypes() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finSelectedConnectorTypes ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finSelectedConnectorTypes ("  +  parameter  +  ") returned an object which doesn\'t contain an error.");

if( connName  ===  "Aquarium") 
      {
ok(testData[0].type  ===  "haystack",
"finSelectedConnectorTypes ("  +  parameter  +  ") query result contains valid data => type: "  +  testData[0].type);
}

    else 
        if( connName  ===  "BACnet") 
      {
ok(testData[0].type  ===  "bacnet",
"finSelectedConnectorTypes ("  +  parameter  +  ") query result contains valid data => type: "  +  testData[0].type);
}

    
      
}
);
  });  

});