import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK

//number generator
function createRandomNumber() {

	var min = 0;
	var max = 5;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//records with history from haystack connector: Aquarium
let hisRecs= 'readAll(point and haystackConnRef->dis == "Aquarium")';

//start time
let startTime= '03/21/2015 05:45 am';

//end time
let endTime= '04/21/2015 06:30 pm';

//rewrite history function
let func= 'v=>v+1';

//parse time format
let formatTime= 'MM/DD/YYYY kk:mm aa';

//argument passed to the axon function
let parameter= hisRecs +"[" + index + "],"+ JSON.stringify(startTime)+","+JSON.stringify(endTime)+"," + func + ","+JSON.stringify(formatTime);



let po = new axonPO({"testQuery":"finHisRewriteSpan("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finHisRewriteSpan() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finHisRewriteSpan("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finHisRewriteSpan("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val.includes("histories rewritten")  ===  true,
"finHisRewriteSpan("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val);
}
);
  });  

});