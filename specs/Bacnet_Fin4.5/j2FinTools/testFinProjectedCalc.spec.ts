import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["today", "thisWeek", "thisMonth", "thisYear"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll(point),"  +  randomParameter;



let po = new axonPO({"testQuery":"finProjectedCalc("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finProjectedCalc() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finProjectedCalc ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finProjectedCalc ("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.curUse  !==  undefined  &&  
  row.estProject  !==  undefined  &&  
  row.estTotal  !==  undefined,
  
  "finProjectedCalc ("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | curUse: "  +  
  row.curUse  +  " | estProject: "  +  
  row.estProject  +  " | estTotal: "  +  
  row.estTotal);
}

}
);
  });  

});