import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//set alarm on digest time
let alarmOn= "true";

//alarm time => hour & minutes
let alarmTime= "time(0, 0)";

//set note on digest time
let noteOn= "true";

//note time => hour & minutes
let noteTime= "time(10, 0)";

//argument passed to the axon function
let parameter= "{on:"  +  alarmOn  +  ", time:"  +  alarmTime  +  "}, {on:"  +  noteOn  +  ", time:"  +  noteTime  +  "}";



let po = new axonPO({"testQuery":"finDigestTimeCommit("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finDigestTimeCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finDigestTimeCommit("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finDigestTimeCommit("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finDigestTimeCommit("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));
}
);
  });  

});