import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["equip", "site"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll("  +  randomParameter  +  ")";



let po = new axonPO({"testQuery":"finGetUriFromRef("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGetUriFromRef() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGetUriFromRef ("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGetUriFromRef ("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( randomParameter  ===  "equip") 
      {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.id  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.siteRef  !==  undefined,
  
  "finGetUriFromRef ("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | id: "  +  
  row.id  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | mod: "  +  
  row.mod  +  " | siteRef: "  + 
  row.siteRef);
}

}

    else 
        if( randomParameter  ===  "site") 
      {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  row.idDis  !==  undefined  &&  row.dis  !==  undefined  &&  row.mod  !==  undefined,"finGetUriFromRef ("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  row.id  +  " | idDis: "  +  row.idDis  +  " | dis: "  +  row.dis  +  " | mod: "  +  row.mod  +  " | imageRef: "  +  row.imageRef  +  " | imageUri: "  +  row.imageUri);
}

}

    
      
}
);
  });  

});