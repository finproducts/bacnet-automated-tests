import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//folioSnapshots()[index]->ts.toStr after folioSnapshots()
//IMPORTANT - Have a snapshot made before the modification to run it as an argument for the function.... 
//....folioRestore(tsBeforeTheChange) to unde the changes and keep the test clean
let getSnapshotTimestamp= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

//argument passed to the axon function
let timestamp= getSnapshotTimestamp;

//array of possible arguments
let paramArray1= ["point"];
let randomIndex1= Math.floor(Math.random()  *  paramArray1.length);
let filter= paramArray1[randomIndex1];

//array of possible arguments
let paramArray2= ["point", "seleniumTestTag"];
let randomIndex2= Math.floor(Math.random()  *  paramArray2.length);
let tags= paramArray2[randomIndex2];

//argument passed to the axon function
let parameter= JSON.stringify(filter)  +  ", ["  +  JSON.stringify(tags)  +  "]";



let po = new axonPO({"testQuery":"finBatchDeleteTagsApply("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finBatchDeleteTagsApply() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchDeleteTagsApply("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchDeleteTagsApply("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( tags  ===  "seleniumTestTag") 
      {
ok(testData[0].body.includes("0 recs were modified")  ===  true  &&  
testData[0].cancelButton  !==  undefined  &&  
testData[0].dis  !==  undefined  && 
 testData[0].finForm  ===  "✓"  &&  
 testData[0].name  !==  undefined,
 
 "finBatchDeleteTagsApply("  +  parameter  +  ") query result contains valid data => body: "  +  
 testData[0].body  +  " | cancelButton: "  +  
 testData[0].cancelButton  +  " | dis: "  +  
 testData[0].dis  +  " | finForm: "  +  
 testData[0].finForm  +  " | name: "  +  
 testData[0].name);
}

    else 
        if( tags  ===  "point") 
      {
ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].name  !==  undefined,
  
  "finBatchDeleteTagsApply("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | name: "  +  
  testData[0].name);
}

    
//undo changes
//ok(confirmData[0].val === null, 
 // "Running folioRestore("+ timestamp +")... Restored.");
}
);
  });  

});