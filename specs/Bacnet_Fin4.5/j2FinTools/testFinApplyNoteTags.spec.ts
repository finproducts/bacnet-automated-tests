import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK on Fin5 but OK in Fin4.5


//function that randomly creates a string
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let newTag= createRandomString();

//argument passed to the axon function
let parameter= "[read(noteTags)->id], ["  +  JSON.stringify(newTag)  +  "]";



let po = new axonPO({"testQuery":"finApplyNoteTags("  +  parameter  +  ")","confirmQuery":"read(noteTags)"});
po.setup(function(instance)
{}
)

describe("The finApplyNoteTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finApplyNoteTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finApplyNoteTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finApplyNoteTags("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData)  +  ""  +  "New tag added: "  +  JSON.stringify(newTag));

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].mod  !==  undefined  &&  
  confirmData[0].noteTags  !==  undefined  &&  
  confirmData[0][newTag]  ===  "✓",
  
  "read(noteTags) query result contains valid data => id: "  +  
  confirmData[0].id  +  " | mod: "  +  
  confirmData[0].mod  +  " | noteTags: "  +  
  confirmData[0].noteTags  +  " | "  +  
  newTag  +  ": "  +  
  confirmData[0][newTag]  +  ""  +  " CONFIRMED - The new tag "  +  JSON.stringify(newTag)  +  " was successfully added.");
}
);
  });  

});