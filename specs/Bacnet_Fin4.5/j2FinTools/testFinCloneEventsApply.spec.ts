import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//source id of the first event: VAV RMT CONTROL
let sourceId= "readAll(schedule)[0]->id";

//events
let events= "";

//targets => City Center Vav-04 Room Setpoint
let targets= 'read(point and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter= sourceId  +  ", ["  +  events  +  "], ["  +  targets  +  "]";



let po = new axonPO({"testQuery":"finCloneEventsApply("  +  parameter  +  ")","confirmQuery":"readAll(schedule)[2]"});
po.setup(function(instance)
{}
)

describe("The finCloneEventsApply() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCloneEventsApply("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCloneEventsApply("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCloneEventsApply("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].dis  !==  undefined,
  "readAll(schedule)[0] query contains valid data. Changes applied successfully: "  +  confirmData[0].dis);
}
);
  });  

});