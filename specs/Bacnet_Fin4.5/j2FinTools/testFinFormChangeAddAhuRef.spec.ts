import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//City Center Vav-01 id
let vav01CityCenterId= 'read(vav and navName=="Vav-01" and siteRef->dis=="City Center")->id';

//argument passed to the axon function
let parameter= vav01CityCenterId  +  ',[{id: @1a169da0-ef590b20, dis: "City Center Vav-01",'
+' floorRef: @1a16992d-9cbd4882, heating, children, hvac, vav, equip, navName: "Vav-01",'
+' treePath: `equip:/City Center/Floor 1/Vav-01`, siteRef: @1a169921-22d9c3b8, finProjectRef: @1b93666d-9b8797fc,'
+' ahuRef: @1a169f42-a1d26b60, indent: 0, description: "Cafe Lounge", icon16: `fan://equipExt/res/img/equipHvac.png`,'
+' mod: dateTime(2016-03-15, 23:11:09.149, "UTC"), level: 0, cityCenterAHU1, disMacro: "\$siteRef \$navName"}]';



let po = new axonPO({"testQuery":"finFormChangeAddAhuRef("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finFormChangeAddAhuRef() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finFormChangeAddAhuRef("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finFormChangeAddAhuRef("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].body  !==  undefined  &&  
  testData[0].cancelButton  !==  undefined  &&  
  testData[0].commitAction  !==  undefined  &&  
  testData[0].commitButton  !==  undefined  &&  
  testData[0].dis  !==  undefined  &&  
  testData[0].finForm  ===  "✓"  &&  
  testData[0].helpDoc  !==  undefined  &&  
  testData[0].name  !==  undefined,
  
  "finFormChangeAddAhuRef("  +  parameter  +  ") query result contains valid data => body: "  +  
  testData[0].body  +  " | cancelButton: "  +  
  testData[0].cancelButton  +  " | commitAction: "  +  
  testData[0].commitAction  +  " | commitButton: "  +  
  testData[0].commitButton  +  " | dis: "  +  
  testData[0].dis  +  " | finForm: "  +  
  testData[0].finForm  +  " | helpDoc: "  +  
  testData[0].helpDoc  +  " | name: "  +  
  testData[0].name);
}
);
  });  

});