import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';

//OK


//folio restore
let folioRestoreParam= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

//1st param =>> first id is a valid id of a point
let dicts= '{"@1a16f0f0-aed42306":"@1cdd2fdf-52ad7hfb"}';

//2nd param
let keepName= "true";

//3rd param
let keepMarkers= "true";

//4th param
let type= "point";

//argument passed to the axon function
let parameter= "["  +  dicts  +  "], "  +  keepName  +  ", "  +  keepMarkers  +  ","  +  JSON.stringify(type);



let po = new axonPO({"testQuery":"finBatchUpdateRecords("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  folioRestoreParam  +  ")"});
po.setup(function(instance)
{}
)

describe("The finBatchUpdateRecords() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchUpdateRecords("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchUpdateRecords("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finBatchUpdateRecords("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]",'Running folioRestore...DB builder restored to default.');
}
);
  });  

});