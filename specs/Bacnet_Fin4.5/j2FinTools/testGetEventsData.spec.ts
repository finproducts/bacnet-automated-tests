import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["weekly", "whole"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let randomParameter= paramArray[randomIndex];

//argument passed to the axon function
let parameter= "readAll(point)[0]->id, "  +  JSON.stringify(randomParameter);



let po = new axonPO({"testQuery":"getEventsData("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The getEventsData() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "getEventsData("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"getEventsData("  +  parameter  +  ") returned an object which doesn't contain an error.");

if( randomParameter  ===  "weekly") 
      {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  row.label  !==  undefined,
  "getEventsData("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | label: "  +  row.label);
}

}

    else 
        if( randomParameter  ===  "whole") 
      {
let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dis  !==  undefined  &&  
  row.label  !==  undefined  &&  
  row.selected  !==  undefined,
  
  "getEventsData("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => dis: "  +  
  row.dis  +  " | label: "  +  row.label  +  " | selected: "  +  row.selected);
}

}

    
      
}
);
  });  

});