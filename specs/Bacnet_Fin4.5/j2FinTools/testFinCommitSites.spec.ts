import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//string generator
function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let geoPostalCode= 12345;

let dis= "Site"  +  createRandomString();

let geoCountry= "United States";

let area= 10000;

let geoAddr= "123 Main St";

let tz= "Los_Angeles";

let geoCity= "Springfield";

let geoState= "CA";

let parameter= "[{geoPostalCode:"  +  geoPostalCode  +  ", dis:"  +  JSON.stringify(dis)  +  ", geoCountry:"  +  JSON.stringify(geoCountry)  +  ", area:"  +  area  +  ", geoAddr:"  +  JSON.stringify(geoAddr)  +  ", tz:"  +  JSON.stringify(tz)  +  ", geoCity:"  +  JSON.stringify(geoCity)  +  ", geoState:"  +  JSON.stringify(geoState)  +  "}]";



let po = new axonPO({"testQuery":"finCommitSites("  +  parameter  +  ")","confirmQuery":"readAll(site and dis=="  +  JSON.stringify(dis)  +  ")"});
po.setup(function(instance)
{}
)

describe("The finCommitSites() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCommitSites("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCommitSites("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCommitSites("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].dis  ===  dis  &&  
  confirmData[0].area  ===  area  &&  
  confirmData[0].geoAddr  ===  geoAddr  &&  
  confirmData[0].geoCity  ===  geoCity  &&  
  confirmData[0].geoCountry  ===  geoCountry  &&  
  confirmData[0].geoPostalCode  ===  geoPostalCode  &&  
  confirmData[0].geoState  ===  geoState  &&  
  confirmData[0].tz  ===  tz,
  
  "readAll(site and dis=="  +  JSON.stringify(dis)  +  ") contains valid data => id: "  +  
  confirmData[0].id  +  " | dis: "  +  
  confirmData[0].dis  +  " | area: "  +  
  confirmData[0].area  +  " | geoAddr: "  +  
  confirmData[0].geoAddr  +  " | geoCity: "  +  
  confirmData[0].geoCity  +  " | geoCountry: "  +  
  confirmData[0].geoCountry  +  " | geoPostalCode: "  +  
  confirmData[0].geoPostalCode  +  " | geoState: "  +  
  confirmData[0].geoState  +  " | tz: "  +  
  confirmData[0].tz);
}
);
  });  

});