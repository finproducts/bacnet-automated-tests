import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 2;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index1= createRandomNumber();

if( index1  >  0) 
      {
var index2= index1  -  1
}

    else 
        if( index1  ===  0) 
      {
var index2= index1  +  3
};

//argument passed to the axon function
let parameter= "[readAll(point)["+ index1 +"]->id, readAll(point)["+ index2 +"]->id]";



let po = new axonPO({"testQuery":"finGlobalControlActions("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The finGlobalControlActions() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finGlobalControlActions("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finGlobalControlActions("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.dur  !==  undefined  &&  
  row.name  !==  undefined  &&  
  row.value  !==  undefined,
  
  "finGlobalControlActions("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => dur: "  +  
  row.dur  +  " | name: "  +  
  row.name  +  " | value: "  +  
  row.value);
}

}
);
  });  

});