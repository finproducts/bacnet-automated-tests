import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//use folioSnapshot to undo the changes
let folioRestoreParam= 'parseDateTime(folioSnapshots()[-1]->ts.toStr, "YYYY-MM-DD\'T\'hh:mm:ssz zzzz")';

//record type
let paramArray= ["site"];

let randomIndex= Math.floor(Math.random()  *  paramArray.length);

let record= paramArray[randomIndex];

function createRandomString() {
	
    var randomStr ="";
    var charSequence = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 10; i++ )
        randomStr += charSequence.charAt(Math.floor(Math.random() * charSequence.length));
    return randomStr;
};

let markerTag= createRandomString();

let propertyTag= createRandomString();

function createRandomNumber() {

	var min = 1;
	var max = 100;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let value= createRandomNumber();

let randomParameter= "readAll("  +  record  +  "), {"  +  markerTag  +  ","  +  propertyTag  +  ":"  +  value  +  "}";



let po = new axonPO({"testQuery":"finCommit("  +  randomParameter  +  ")","confirmQuery":"readAll(site)"});
po.setup(function(instance)
{}
)

describe("The finCommit() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCommit("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCommit("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  !==  undefined,
  "finCommit("  +  randomParameter  +  ") query results contains valid data => val: "  +  testData[0].val  +  " records were affected.");

let i;

for(i  =  0; i  <  confirmData.length; i++)
{
let row= confirmData[i]

ok(row[markerTag]  ===  "✓"  &&  
row[propertyTag]  ===  value,

"readAll(site) query contains valid data in accordance with the new changes on row "  +  i  +  " => "  +  
markerTag  +  ":"  +  row[markerTag]  +  " | "  +  propertyTag  +  ":"  +  row[propertyTag]);
}

}
);
  });  

});