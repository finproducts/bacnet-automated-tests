import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {snapshotTimestampForRestore as timestamp} from '../../../helpers/testingUtils';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//tags 
let paramArray= ["", "point", "hvac"];
let randomIndex= Math.floor(Math.random()  *  paramArray.length);
let tag= paramArray[randomIndex];

let parameter= "["  +  JSON.stringify(tag)  +  "]";



let po = new axonPO({"testQuery":"finConflictingTags("  +  parameter  +  ")","confirmQuery":"folioRestore("  +  timestamp  +  ")"});
po.setup(function(instance)
{}
)

describe("The finConflictingTags() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finConflictingTags("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finConflictingTags("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(testData[0].val  !==  undefined,
  "finConflictingTags("  +  parameter  +  ") query result contains valid data => val: "  +  testData[0].val);

//ok(JSON.stringify(confirmData)  ===  "[{\"val\":null}]","Running folioRestore...DB builder restored to default.");
}
);
  });  

});