import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not OK


//Must create a new empty schedule entitled "Selenium Test Schedule" (must be boolean)!

//schedule id acting as a template => "Summer School" (is boolean)
let scheduleTemplateId= 'read(schedule and dis=="Summer School")->id';

//events copied to the new schedule from the schedule acting as template
let events= '["mon", "tue", "wed", "thu", "fri", "sat", "sun", "New Years", "Thanksgiving Day", "Presidents Day", "Labor Day", "Christmas", "Good Friday", "Memorial Day", "Independence Day"]';

//new schedule id => "Selenium Test Schedule"(must be boolean)
let targetSchedule= 'read(schedule and dis=="Selenium Test Schedule")';

//argument passed to the axon function
let parameter = scheduleTemplateId +","+ events +',"readAll(" + (['+ targetSchedule +'->id]).map(t=>"id==@"+t).concat(" or ") + ")"';



let po = new axonPO({"testQuery":"finBatchCloneScheduleApply("  +  parameter  +  ")","confirmQuery":"targetSchedule"});
po.setup(function(instance)
{}
)

describe("The finBatchCloneScheduleApply() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finBatchCloneScheduleApply("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finBatchCloneScheduleApply("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finBatchCloneScheduleApply("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].schedule.includes("sun,mon,tue,wed,thu,fri,sat")  ===  true  &&  
confirmData[0].schedule.includes("Thanksgiving Day")  ===  true  &&  
confirmData[0].schedule.includes('Presidents Day')  ===  true  &&  
confirmData[0].schedule.includes('Labor Day')  ===  true  &&  
confirmData[0].schedule.includes('Christmas')  ===  true  &&  
confirmData[0].schedule.includes('Good Friday')  ===  true  &&  
confirmData[0].schedule.includes('Memorial Day')  ===  true  &&  
confirmData[0].schedule.includes('Independence Day')  ===  true,

targetSchedule +" contains valid data => dis: "+ confirmData[0].dis +" | schedule: "+ confirmData[0].schedule +"\n"
+"The schedule Summer School has successfully been cloned to Selenium Test Schedule.");
}
);
  });  

});