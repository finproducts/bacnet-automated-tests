import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//not ok on Fin5 but ok on Fin4.5

//floor plan query name
let queryName= 'seleniumTest2';

//create floor plan query
let parameter= '[readAll(equip and siteRef->dis=="City Center")[0]->id], '  +  JSON.stringify(queryName);

//floor id: City Center Floor 2 
let floorId= 'read(floor and dis=="Floor 2" and siteRef->dis=="City Center")->id';

//tag
let tag= queryName  +  "Order";

//argument passed to the axon function
let argument= floorId  +  ","  +  JSON.stringify(tag);



let po = new axonPO({"testQuery":"finCreateFloorPlanQuery("  +  parameter  +  ")","confirmQuery":"finFloorPlan("  +  argument  +  ")"});
po.setup(function(instance)
{}
)

describe("The finFloorPlan() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "finCreateFloorPlanQuery("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"finCreateFloorPlanQuery("  +  parameter  +  ") returned an object which doesn't contain an error.");

ok(JSON.stringify(testData)  ===  "[{\"val\":null}]",
"finCreateFloorPlanQuery("  +  parameter  +  ") query result contains valid data: "  +  JSON.stringify(testData));

ok(confirmData[0].id  !==  undefined  &&  
  confirmData[0].dis  ===  "City Center Vav-11"  &&  
  confirmData[0].finProjectRef  !==  undefined  &&  
  confirmData[0].disMacro  !==  undefined  &&  
  confirmData[0].navName  !==  undefined  &&  
  confirmData[0].siteRef  !==  undefined,
  
  "finFloorPlan("  +  argument  +  ") has been successfully applied => id: "  +  
  confirmData[0].id  +  " | dis: "  +  
  confirmData[0].dis  +  " | finProjectRef: "  +  
  confirmData[0].finProjectRef  +  " | disMacro: "  +  
  confirmData[0].disMacro  +  " | navName: "  +  
  confirmData[0].disMacro  +  " | siteRef: "  +  
  confirmData[0].siteRef);
}
);
  });  

});