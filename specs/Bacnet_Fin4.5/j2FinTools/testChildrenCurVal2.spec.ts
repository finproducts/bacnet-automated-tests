import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 10;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index1= createRandomNumber();

let index2= createRandomNumber();

let index3= createRandomNumber();

let parameter= "[readAll(equip)["  +  index1  +  "], readAll(equip)["  +  index2  +  "], readAll(equip)["  +  index3  +  "]]";



let po = new axonPO({"testQuery":"childrenCurVal2("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The childrenCurVal2() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "childrenCurVal2("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"childrenCurVal2("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.floorRef  !==  undefined  && 
  row.navName  !==  undefined  &&  
  row.siteRef  !==  undefined,
  
  "childrenCurVal2("  +  parameter  +  ") query result contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | navName: "  +  
  row.navName  +  " | siteRef: "  +  
  row.siteRef);
}

}
);
  });  

});