import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK



let po = new axonPO({"testQuery": "finCreateNoteTopicCommit(\"SELENIUM_TEST_TOPIC_FOR_EDIT\", [], {tags: [], logic: \"and\"}, {tags: [], logic: \"and\"},null)",
"confirmQuery" : "finEditTopicsForm()"});
po.setup(function(instance)
{}
)

describe("The finEditTopicsForm() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(confirmData  !==  undefined  &&  confirmData  !==  null,
  "finEditTopicsForm() query result is not undefined or null.");

ok(typeof confirmData  ===  "object"  &&  confirmData.type  !==  "error",
"finEditTopicsForm() returned an object which doesn't contain an error.");

ok(confirmData[0].body  !==  undefined  &&  
  confirmData[0].cancelButton  !==  undefined  &&  
  confirmData[0].commitAction  !==  undefined  &&  
  confirmData[0].commitButton  !==  undefined  &&  
  confirmData[0].dis  !==  undefined  &&  
  confirmData[0].finForm  ===  "✓"  &&  
  confirmData[0].helpDoc  !==  undefined  && 
  confirmData[0].name  !==  undefined,
  
  "finEditTopicsForm() query result contains valid data => body: "  +  
  confirmData[0].body  +  " | cancelButton: "  +  
  confirmData[0].cancelButton  +  " | commitAction: "  +  
  confirmData[0].commitAction  +  " | commitButton: "  +  
  confirmData[0].commitButton  +  " | dis: "  +  
  confirmData[0].dis  +  " | finForm: "  +  
  confirmData[0].finForm  +  " | helpDoc: "  +  
  confirmData[0].helpDoc  +  " | name: "  +  
  confirmData[0].name);
}
);
  });  

});