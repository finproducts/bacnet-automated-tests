import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';







let po = new axonPO({"\"testQuery\"":"\"hardcodedObj()\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testDecoding.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing decoding"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'hardcodedObj() query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'hardcodedObj() returned an object which doesn\'t contain an error.');

ok(testData[0].name  ===  "Jason"  &&  testData[0].phones[0].type  ===  "cell"  &&  testData[0].phones[0].value  ===  "123-456-7890"  &&  testData[0].phones[1].type  ===  "home"  &&  testData[0].phones[1].value  ===  "123-456-7890"  &&  testData[0].title  ===  "Founder"  &&  testData[0].vehicle.type  ===  "Tesla"  &&  testData[0].vehicle.color  ===  "Silver"  &&  testData[0].vehicle.topSpeed  ===  250  &&  testData[0].vehicle.unit  ===  "mph",'hardcodedObj() returns valid data: "  +  "
"  +  JSON.stringify(testData)  +  "
"  +  "The JS object returned contains the same keys, values, data types & structure with the axon object contained by the axon function hardcodedObj()');
}
);
  });  

});