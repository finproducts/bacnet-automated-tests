import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';





let obj= '{name: "Jason", phones: [{type: "cell", value: "123-456-7890"}, {type: "home", value: "123-456-7890"}], title: "Founder", vehicle: {type: "Tesla", color: "Silver", topSpeed: 250mph}}';



let po = new axonPO({"\"testQuery\"":"\"internalObjValidation(\"  +  obj  +  \")\"","\"confirmQuery\"":"\"\""});
po.setup(function(instance)
{}
)

describe('testEncoding.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing encoding"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  undefined,'internalObjValidation("  +  obj  +  ") query result is not undefined or null.');

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",'internalObjValidation("  +  obj  +  ") returned an object which doesn\'t contain an error.');

ok(testData[0].val  ===  "PASSED - The object has been correctly encoded.","internalObjValidation("  +  obj  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});