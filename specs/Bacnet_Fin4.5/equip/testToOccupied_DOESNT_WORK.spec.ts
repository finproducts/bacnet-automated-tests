import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';

//OK


//point id
let pointId= 'read(point and siteRef->dis=="City Center")->id';

let parameter= pointId;



let po = new axonPO({"testQuery":"toOccupied("  +  parameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe('testToOccupied_DOESNT_WORK.js',() =>{
  beforeEach(async () => {
    await po.open();
  });

  it('"Testing toOccupied"', () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "toOccupied("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"toOccupied("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis.includes('Vav')  ===  true  &&  
  row.cur  ===  "✓"  &&  
  row.curStatus  ===  "ok"  &&  
  row.curTracksWrite  ===  "✓"  &&  
  row.equipRefDis.includes('Vav')  ===  true  &&  
  row.floorRefDis.includes('Floor')  ===  true  &&  
  row.occupied  ===  "✓"  &&  
  row.hisFunc  ===  "hisBoolean"  &&  
  row.kind  ===  "Bool",
  
  "toOccupied("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | cur: "  +  
  row.cur  +  " | curStatus: "  +  
  row.curStatus  +  " | curTracksWrite: "  +  
  row.curTracksWrite  +  " | curVal: "  +  
  row.curVal  +  " | disMacro: "  +  
  row.disMacro  +  " | equipRef: "  +  
  row.equipRef  +  " | floorRef: "  +  
  row.floorRef  +  " | hisFunc: "  +  
  row.hisFunc  +  " | kind: "  +  
  row.kind  +  " | mod: "  +  
  row.mod  +  " | navName: "  +  
  row.navName  +  " | siteRef: "  +  
  row.siteRef);
}

}
);
  });  

});