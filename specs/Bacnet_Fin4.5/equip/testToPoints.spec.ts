import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of possible arguments
let paramArray= ["site", "equip"];

//randomize
let randomIndex= Math.floor(Math.random()  *  paramArray.length);

//argument passed to the axon faction
let randomParameter= "readAll("  +  paramArray[randomIndex]  +  ")";



let po = new axonPO({"testQuery":"toPoints("  +  randomParameter  +  ').toGrid.keepCols(["id","dis","cur","equipRef","disMacro","floorRef"])',"confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The toPoints() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "toPoints("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"toPoints("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  10; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.cur  ===  "✓"  &&  
  row.equipRef  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.floorRef  !==  undefined,
  
  "toPoints("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRef: "  +  
  row.floorRef  +  " | equipRef: "  +  
  row.equipRef  +  " | cur: "  +  
  row.cur);
}

}
);
  });  

});