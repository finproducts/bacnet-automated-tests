import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK but
//row.siteRefDis  ===  "JB Tower"    in 4.5
//row.siteRefDis  ===  "City Center"  in 5

//equip id
let equipId= "read(vav)->id";

//argument passed to the axon faction
let parameter= equipId;



let po = new axonPO({"testQuery":'equipsToRuleTargets('+ parameter + ').toGrid.keepCols(["id","dis","siteRef","disMacro","floorRef", "mod","navName"])',"confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The equipsToRuleTargets() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{  
  
ok(testData  !==  undefined  &&  testData  !==  null,
  "equipsToRuleTargets("  +  parameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"equipsToRuleTargets("  +  parameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i];


ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.disMacro  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.navName  !==  undefined &&  
  row.siteRefDis  ===  "City Center" 
  ,
  
  "equipsToRuleTargets("  +  parameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | disMacro: "  +  
  row.disMacro  +  " | floorRefDis: "  +  
  row.floorRefDis  +  " | mod: "  +  
  row.mod  +  " | navName: "  + 
  row.navName  +  " | siteRefDis: "  + 
  row.siteRefDis);

}

}
);
  });  

});