import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 10;
	var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
	return getRandomNumber;	
};

let index= createRandomNumber();

//argument passed to the axon faction
let randomParameter= "readById(readAll(equip)["  +  index  +  "]->id)";



let po = new axonPO({"testQuery":"equipToPoints("  +  randomParameter  +  ').toGrid.keepCols(["id","dis","cur","curStatus",kind"])',"confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The equipToPoints() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "equipToPoints("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"equipToPoints("  +  randomParameter  +  ") returned an object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.cur  ===  "✓"  &&  
  //row.equipRefDis  !==  undefined  &&  
  row.kind  ===  "number",
  
  "equipToPoints("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | cur: "  +  
  row.cur  +  " | curStatus: "  +  
  row.Status  +  " | equipRefDis: "  +  
  row.equipRefDis  +  " | kind: "  +  
  row.kind);
}

}
);
  });  

});