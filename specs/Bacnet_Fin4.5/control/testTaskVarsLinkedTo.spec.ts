import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//array of variables from program ahuFDD @target JB Tower AHU-2
let varArray= ["sf", "mat", "rat", "oad"];
let randomIndexVar= Math.floor(Math.random()  *  varArray.length);
let randomVar= varArray[randomIndexVar];

//program id: "ahuFDD"
let programId= 'read(program and name=="ahuFDD")->id';

//target id: "JB Tower AHU-2"
let targetId= 'read(equip and navName=="AHU-2" and siteRef->dis=="JB Tower")->id';

//argument passed to the axon function
let randomParameter= programId  +  ", "  +  targetId  +  ", "  +  JSON.stringify(randomVar);



let po = new axonPO({"testQuery":"taskVarsLinkedTo("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The taskVarsLinkedTo() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "taskVarsLinkedTo("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"taskVarsLinkedTo("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let row= testData[0]

//testing taskVarsLinkedTo(randomParameter)
ok(row.programRef  !==  undefined  &&  
  row.programRefDis  ===  "ahuFDD"  &&  
  row.targetRef  !==  undefined  &&  
  row.targetRefDis  ===  "JB Tower AHU-2"  &&  
  row.name  ===  randomVar,
  
  "taskVarsLinkedTo("  +  randomParameter  +  ") query results contains valid data => programRef: "  +  
  row.programRef  +  " | programRefDis: "  +  
  row.programRefDis  +  " | targetRef: "  +  
  row.targetRef  +  " | targetRefDis: "  +  
  row.targetRefDis  +  " | name: "  +  
  row.name);
}
);
  });  

});