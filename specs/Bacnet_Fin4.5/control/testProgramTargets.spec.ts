import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


let programName= "overHeatedZone";

let programId= "read(program and name=="  +  JSON.stringify(programName)  +  ")->id";

let randomParameter= programId;



let po = new axonPO({"testQuery":"programTargets("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The programTargets() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "programTargets("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"programTargets("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  !==  undefined  &&  
  row.floorRef  !==  undefined  &&  
  row.mod  !==  undefined  &&  
  row.navName  !==  undefined  &&  
  row.siteRef  !==  undefined,
  
  "programTargets("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
  row.id  +  " | dis: "  +  
  row.dis  +  " | floorRef: "  +  
  row.floorRef  +  " | mod: "  +  
  row.mod  +  " | navName: "  +  
  row.navName  +  " | siteRef: "  +  
  row.siteRef);
}

}
);
  });  

});