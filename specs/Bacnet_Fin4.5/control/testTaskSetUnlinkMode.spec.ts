import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK


//array of variables from program ahuFDD @target JB Tower AHU-2
let varArray= ["sf", "mat", "rat", "oad", "osat"];
let randomIndexVar= Math.floor(Math.random()  *  varArray.length);
let randomVar= varArray[randomIndexVar];

//value array for variables
let valArray= [true, false];
let randomIndexVal= Math.floor(Math.random()  *  valArray.length);
let randomVal= valArray[randomIndexVal];

//argument passed to the axon function
let randomParameter= 'read(program and name=="ahuFDD")->id, read(equip and navName=="AHU-2" and siteRef->dis=="JB Tower")->id, {' +  JSON.stringify(randomVar)  +  ":"  +  randomVal  +  "}";



let po = new axonPO({"testQuery":"taskSetUnlinkMode("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The taskSetUnlinkMode() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "taskSetUnlinkMode("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"taskSetUnlinkMode("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

ok(testData[0].val  ===  "taskSetUnlinkMode",
"taskSetUnlinkMode("  +  randomParameter  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});