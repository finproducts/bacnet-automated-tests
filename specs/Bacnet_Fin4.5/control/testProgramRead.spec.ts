import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';



//OK

let programName= "overHeatedZone";

let programId= "read(program and name=="  +  JSON.stringify(programName)  +  ")->id";

let randomParameter= programId;



let po = new axonPO({"testQuery":"programRead("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The programRead() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "programRead("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"programRead("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

let i;

for(i  =  0; i  <  testData.length; i++)
{
let row= testData[i]

ok(row.id  !==  undefined  &&  
  row.dis  ===  programName  &&  
  row.mod  !==  undefined  &&  
  row.name  ===  programName  && 
   row.program  !==  undefined  &&  
   row.programOn  !==  undefined  &&  
   row.programStatus  !==  undefined  &&  
   row.programVars  !==  undefined,
   
   "programRead("  +  randomParameter  +  ") query results contains valid data on row "  +  i  +  " => id: "  +  
   row.id  +  " | dis: "  +  
   row.dis  +  " | mod: "  +  
   row.mod  +  " | name: "  +  
   row.name  +  " | program: "  +  
   row.program  +  " | programOn: "  +  
   row.programOn  +  " | programStatus: "  +  
   row.programStatus  +  " | programVars: "  +  
   row.programVars);
}

}
);
  });  

});