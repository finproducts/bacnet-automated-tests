import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {ok, equal, notEqual} from '../../../helpers/nwapis';
import {waitService} from '../../../helpers/wait-service';
import axonPO from '../../../page_objects/finstack/axon.page';


//OK

//function that randomly creates a number
function createRandomNumber() {

	var min = 0;
	var max = 100;
	var getRandomNumber = Math.random() * (max - min) + min;
	return getRandomNumber;	
};

let randomNumber= createRandomNumber();

//array of variables from program ahuFDD @target JB Tower AHU-2
let varArray= ["sf", "mat", "rat", "oad", "osat"];
let randomIndexVar= Math.floor(Math.random()  *  varArray.length);
let randomVar= varArray[randomIndexVar];

//value array for variables
let valArray= [true, false];
let randomIndexVal= Math.floor(Math.random()  *  valArray.length);
let randomVal= valArray[randomIndexVal];

let paramValue;
if( randomVar  ===  "sf") 
      {
paramValue  =  randomVal
}

    else 
        if( randomVar  !==  "sf") 
      {
paramValue  =  randomNumber
}

    
      ;

let randomParameter= 'read(program and name=="ahuFDD")->id, read(equip and navName=="AHU-2" and siteRef->dis=="JB Tower")->id, {'  +  JSON.stringify(randomVar)  +  ":"  +  paramValue  +  "}";

let po = new axonPO({"testQuery":"taskSetVars("  +  randomParameter  +  ")","confirmQuery":""});
po.setup(function(instance)
{}
)

describe("The taskSetVars() query",() =>{
  beforeEach(async () => {
    await po.open();
  });

  it("Returns correct data when executed", () => {
    po.assert(function(testData,confirmData)
{
ok(testData  !==  undefined  &&  testData  !==  null,
  "taskSetVars("  +  randomParameter  +  ") query result is not undefined or null.");

ok(typeof testData  ===  "object"  &&  testData.type  !==  "error",
"taskSetVars("  +  randomParameter  +  ") returned and object which doesn't contain an error.");

ok(testData[0].val  ===  "taskSetVars",
"taskSetVars("  +  randomParameter  +  ") query results contains valid data: "  +  testData[0].val);
}
);
  });  

});