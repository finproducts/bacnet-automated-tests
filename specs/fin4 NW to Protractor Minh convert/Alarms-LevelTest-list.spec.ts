import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

let selectors =
  "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(n) > td:nth-child(4) > span";

describe("Alarms-LevelTest-list.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("navigation to CC", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.front > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1900);
  });
  it("list ", async () => {
    click("#innerApp > section > header > div > button:nth-child(2)");

    waitService.waitForMilliseconds(4000);
  });
  it("verify only CC", async () => {
    waitService.waitForMilliseconds(1000);

    containsText(selectors, "City Center");

    notContainsText(selectors, "JB");

    notContainsText(selectors, "Laguna");

    notContainsText(selectors, "Malibu");

    notContainsText(selectors, "Pearl");

    waitService.waitForMilliseconds(1900);
  });
  it("navigation to CC >> floor 1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1900);
  });
  it("list 1", async () => {
    click("#innerApp > section > header > div > button:nth-child(2)");
  });
  it("verify CC-Floor 1", async () => {
    containsText(selectors, "City Center");

    notContainsText(selectors, "JB");

    notContainsText(selectors, "City Center Vav-11");

    notContainsText(selectors, "Laguna");

    notContainsText(selectors, "Malibu");

    notContainsText(selectors, "Pearl");

    waitService.waitForMilliseconds(1900);
  });
  it("navigation to CC >> floor 1 >> VAV-01", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1900);
  });
  it("list 2", async () => {
    click("#innerApp > section > header > div > button:nth-child(2)");
  });
  it("verify CC-Floor 1-VAV-01", async () => {
    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(2) > td:nth-child(4) > span",
      "City Center Vav-01"
    );

    notContainsText(selectors, "City Center Vav-02");

    waitService.waitForMilliseconds(1900);
  });
});
