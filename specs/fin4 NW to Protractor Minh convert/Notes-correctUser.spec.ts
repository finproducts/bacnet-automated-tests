import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let notesButton = po.notesButton;

let noteTitleMob = "noteMobile" + new Date().valueOf();

let noteTitleMain = "noteMain" + new Date().valueOf();

describe("Notes-correctUser.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > section > ul > div.horizontal.middle.divider-item.no-margin > span:nth-child(2)",
      "logged as End User"
    );


 
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to notes1", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(notesButton);

    waitService.waitForMilliseconds(5000);
  });
  it("Assert the note is assigned to the correct user", async () => {
    containsText(
      "#innerApp > section > ul > li:nth-child(1) > div > div.vertical.flex > div.horizontal.middle.wrap.margin.extra.l-side > span.dim-text.italic",
      "End User"
    );
  });
  it("Reassign to system integrator", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "#innerApp > section > ul > li:nth-child(1) > div > div.horizontal.no-flex.middle.margin.large.l-side > button:nth-child(2)"
    );

    waitForElementPresent(
      "body > app > div.modal.vertical.middle.center > div > section > ul > li",
      4000
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > section > ul > li"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > section > ul > li > span.icon-checkmark"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );
  });
  it("log in with system integrator", async () => {
    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > section > ul > div.horizontal.middle.divider-item.no-margin > span:nth-child(2)",
      "logged as System integrator"
    );

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to notes", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click("body > app > section > section > ul > li:nth-child(6)");

    waitService.waitForMilliseconds(500);
  });
  it("Assert the note is assigned to the correct user2", async () => {
    containsText(
      "#innerApp > section > ul > li:nth-child(1) > div > div.vertical.flex > div.horizontal.middle.wrap.margin.extra.l-side > span.dim-text.italic",
      "System Integrator"
    );
  });
});
