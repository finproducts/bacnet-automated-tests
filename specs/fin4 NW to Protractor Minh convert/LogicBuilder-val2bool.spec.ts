import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-val2bool.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1) > a > div.vertical.flex.non-auto > h4"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1) > a"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(10)"
    );

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(500);

    frame(0);

    waitService.waitForMilliseconds(500);
  });
  it("Drag the blocks to stage", async () => {
    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.boolean.variable > b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(
      function(simple) {
        app.set("blocks.1.x", 900);

        app.set("blocks.1.y", 200);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.boolean.variable > b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(
      function(simple) {
        app.set("blocks.2.x", 900);

        app.set("blocks.2.y", 400);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.number.variable > b.number.get.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(
      function(simple) {
        app.set("blocks.3.x", 100);

        app.set("blocks.3.y", 400);
      },
      [],
      function(result) {}
    );

    click("#panel > section.blocklibrary > div > div:nth-child(3) > div");

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div.category.open > div.contents > div:nth-child(1)",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    execute(
      function(simple) {
        app.set("blocks.4.x", 540);

        app.set("blocks.4.y", 104);
      },
      [],
      function(result) {}
    );

    click("#panel > section.blocklibrary > div > div:nth-child(6) > div");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div:nth-child(6) > div.contents > div:nth-child(5)",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(
      function(simple) {
        app.set("blocks.5.x", 300);

        app.set("blocks.5.y", 300);
      },
      [],
      function(result) {}
    );
  });
  it("Link the blocks", async () => {
    click(
      "#stage > div.scroller > div > div.get.number.block > div > div.output > div > b"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div.cmpGt.block > div > div.input > div:nth-child(1) > b"
    );

    click(
      '#stage > div.scroller > div > div.cmpGt.block > div > div.input > div:nth-child(2) > div > input[type="text"]'
    );

    waitService.waitForMilliseconds(100);

    keys("75");

    waitService.waitForMilliseconds(100);

    click(
      "#stage > div.scroller > div > div.cmpGt.block > div > div.output > div > b"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div.if.block > div.data.sockets > div.input > div > b"
    );

    waitService.waitForMilliseconds(1);

    click(
      "#stage > div.scroller > div > div.if.block > div.exec.sockets > div.out > div:nth-child(1) > b"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div:nth-child(2) > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div.if.block > div.exec.sockets > div.out > div:nth-child(2) > b"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div:nth-child(3) > div.exec.sockets > div.in > div > b"
    );

    click(
      "#stage > div.scroller > div > div:nth-child(3) > div.data.sockets > div.input > div > div > label"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div.__start__.block > div > div > div > b"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#stage > div.scroller > div > div.if.block > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(5000);
  });
});
