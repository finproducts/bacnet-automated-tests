import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let settingsButton = po.settingsButton;

let root =
  "body > app > section > div > div.app-nav.vertical > nav > div:nth-child";

let button =
  "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.back > ul > li:nth-last-child(1)";

let OK = "#controlBar > button:nth-child(2)";

let CANCEL = "#controlBar > button:nth-child(1)";

let icon = "WU-HAZY";

let iconClass = "wi-wu-hazy";

let displayName = "ActionAlert";

let dis = displayName.toUpperCase();

let alert = "alert('The button launched an alert sucessfully')";

describe("SettingsHeader-AddButton-Action.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("go to settings > header", async () => {
    cssClassPresent(
      settingsButton +
        "> ul > li:nth-child(3) > span.icon-toggles.mnh.non-interactive",
      "icon-toggles"
    );

    click(settingsButton + " > ul > li:nth-child(4)");

    waitService.waitForMilliseconds(1000);
  });
  it("header menu assertions", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1) > header > h4",
      "— Edit Header —"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Home"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-item-icon.icon-home",
      "icon-home"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Disable Header"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-item-icon.icon-cross",
      "icon-cross"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Button"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-item-icon.icon-plus",
      "icon-plus"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Reorder Buttons"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-item-icon.icon-list",
      "icon-list"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > div > h6",
      "Buttons"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Add new button click", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Assert new button options submenu", async () => {
    containsText(root + "(1) > header > h4", "— Add Button —");

    cssClassPresent(
      root + "(2) > a > div > div > span.nav-item-icon.icon-return",
      "icon-return"
    );

    containsText(
      root +
        "(2) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Back"
    );

    containsText(root + "(3)> div > h6", "New");

    cssClassPresent(
      root + "(4) > a > div > div > span.nav-item-icon.icon-pencil",
      "icon-pencil"
    );

    containsText(
      root +
        "(4) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Display Name"
    );

    cssClassPresent(
      root + "(5) > a > div > div > span.nav-item-icon.icon-photo",
      "icon-photo"
    );

    containsText(
      root +
        "(5) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Icon"
    );

    cssClassPresent(
      root + "(6) > a > div > div > span.nav-item-icon.icon-tag",
      "icon-tag"
    );

    containsText(
      root +
        "(6) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Security Tags"
    );

    cssClassPresent(
      root + "(7) > a > div > div > span.nav-item-icon.icon-information",
      "icon-information"
    );

    containsText(
      root +
        "(7) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Badge"
    );

    cssClassPresent(
      root + "(8) > a > div > div > span.nav-item-icon.icon-web",
      "icon-web"
    );

    containsText(
      root +
        "(8) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Link"
    );

    cssClassPresent(
      root + "(9) > a > div > div > span.nav-item-icon.icon-plus",
      "icon-plus"
    );

    containsText(
      root +
        "(9) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Submenu Button"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Set Display Name", async () => {
    click(root + "(4)");

    containsText("#title", "Edit Button Name");

    containsText("#content > div > label", "Display Name");

    clearValue("#content > div > input");

    setValue("#content > div > input", displayName);

    click(OK);

    waitService.waitForMilliseconds(500);

    containsText(root + "(3)", displayName);

    waitService.waitForMilliseconds(1000);
  });
  it("Set Icon", async () => {
    click(root + "(5)");

    waitService.waitForMilliseconds(500);

    containsText("#title", "Edit Button Icon");

    containsText("#content > div > label", "Icon");

    click("#content > div > div");

    setValue('#content > div > div > div > input[type="text"]', icon);

    containsText(
      "#ractive-multiselect-dropdown-container > ul > li:nth-child(2) > div > span.label-item",
      icon
    );

    cssClassPresent(
      "#ractive-multiselect-dropdown-container > ul > li:nth-child(2) > div > span.wi-wu-hazy.item-icon",
      iconClass
    );

    click("#ractive-multiselect-dropdown-container > ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    cssClassPresent(
      "#content > div > div > div > span > div > span.wi-wu-hazy.item-icon",
      iconClass
    );

    containsText(
      "#content > div > div > div > span > div > span.label-item",
      icon
    );

    waitService.waitForMilliseconds(1000);

    click(OK);

    waitService.waitForMilliseconds(1000);
  });
  it("Set Graphic", async () => {
    let dropdown = "#content > div.form-item-holder.combobox > select";

    click(root + "(8)");

    containsText("#title", "Edit Button Link");

    containsText("#content > div > label", "Link");

    elementPresent("#content > div.form-item-holder.textinput > input");

    containsText("#content > div.form-item-holder.combobox", "Link Type");

    elementPresent("#content > div.form-item-holder.combobox > select");

    click(dropdown);

    containsText(dropdown + " > option:nth-child(1)", "action");

    containsText(dropdown + " > option:nth-child(2)", "hash");

    containsText(dropdown + " > option:nth-child(3)", "url");

    containsText(dropdown + " > option:nth-child(4)", "Graphic");

    waitService.waitForMilliseconds(500);

    clearValue("#content > div.form-item-holder.textinput > input");

    setValue("#content > div.form-item-holder.textinput > input", alert);

    waitService.waitForMilliseconds(1000);

    click(OK);

    waitService.waitForMilliseconds(1000);
  });
  it("The button is on the bar and clicking it launches the alert", async () => {
    waitService.waitForMilliseconds(1000);

    refresh();

    waitService.waitForMilliseconds(4000);

    click("body > app > section > header > button:nth-last-child(2)");

    elementPresent(button);

    cssClassPresent(button + "> span", iconClass);

    containsText(button + "> div > h6", dis);

    click(button);

    waitService.waitForMilliseconds(3000);

    getAlertText(function(result) {
      console.log(result.value);
    });

    acceptAlert();

    waitService.waitForMilliseconds(1000);

    end();
  });
});
