import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let settingsButton = po.settingsButton;

let root =
  "body > app > section > div > div.app-nav.vertical > nav > div:nth-child";

describe("SettingsHeader-AddThenRemoveButton.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("go to settings > header", async () => {
    cssClassPresent(
      settingsButton +
        "> ul > li:nth-child(3) > span.icon-toggles.mnh.non-interactive",
      "icon-toggles"
    );

    click(settingsButton + " > ul > li:nth-child(4)");

    waitService.waitForMilliseconds(1000);
  });
  it("header menu assertions", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1) > header > h4",
      "— Edit Header —"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Home"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-item-icon.icon-home",
      "icon-home"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Disable Header"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-item-icon.icon-cross",
      "icon-cross"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Button"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-item-icon.icon-plus",
      "icon-plus"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Reorder Buttons"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-item-icon.icon-list",
      "icon-list"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > div > h6",
      "Buttons"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Add new button click", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Assert new button options submenu", async () => {
    containsText(root + "(1) > header > h4", "— Add Button —");

    cssClassPresent(
      root + "(2) > a > div > div > span.nav-item-icon.icon-return",
      "icon-return"
    );

    containsText(
      root +
        "(2) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Back"
    );

    containsText(root + "(3)> div > h6", "New");

    cssClassPresent(
      root + "(4) > a > div > div > span.nav-item-icon.icon-pencil",
      "icon-pencil"
    );

    containsText(
      root +
        "(4) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Display Name"
    );

    cssClassPresent(
      root + "(5) > a > div > div > span.nav-item-icon.icon-photo",
      "icon-photo"
    );

    containsText(
      root +
        "(5) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Icon"
    );

    cssClassPresent(
      root + "(6) > a > div > div > span.nav-item-icon.icon-tag",
      "icon-tag"
    );

    containsText(
      root +
        "(6) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Security Tags"
    );

    cssClassPresent(
      root + "(7) > a > div > div > span.nav-item-icon.icon-information",
      "icon-information"
    );

    containsText(
      root +
        "(7) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Badge"
    );

    cssClassPresent(
      root + "(8) > a > div > div > span.nav-item-icon.icon-web",
      "icon-web"
    );

    containsText(
      root +
        "(8) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Set Link"
    );

    cssClassPresent(
      root + "(9) > a > div > div > span.nav-item-icon.icon-plus",
      "icon-plus"
    );

    containsText(
      root +
        "(9) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Submenu Button"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Click Back Button assert the new button is created and has the same name in button list", async () => {
    getText(root + "(3)> div > h6", function(result) {
      click(root + "(2) > a > div > div > span.nav-item-icon.icon-return");

      waitService.waitForMilliseconds(3000);

      getText(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
        function(result2) {
          console.log(result.value);

          console.log(result2.value);

          equal(result.value, result2.value);
        }
      );
    });

    waitService.waitForMilliseconds(1000);

    refresh();

    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);

    click(settingsButton + " > ul > li:nth-child(4)");

    waitService.waitForMilliseconds(1000);
  });
  it("verify button is present in the top bar", async () => {
    click("body > app > section > header > button:nth-last-child(2)");

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.back > ul > li:nth-last-child(1)"
    );

    getText(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.back > ul > li:nth-last-child(1)> div > h6",
      function(result) {
        getText(
          "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
          function(result2) {
            console.log(result.value);

            console.log(result2.value);

            equal(result.value, result2.value.toUpperCase());
          }
        );
      }
    );

    cssClassPresent(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.back > ul > li:nth-child(7) > span",
      "icon-plus"
    );
  });
  it("remove", async () => {
    let newButton =
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1)";

    click(newButton);

    waitService.waitForMilliseconds(500);

    containsText(newButton + "> ul > li:nth-child(1) > label", "Edit");

    cssClassPresent(newButton + "> ul > li:nth-child(1) > i", "icon-pencil");

    containsText(newButton + "> ul > li:nth-child(2) > label", "Remove");

    cssClassPresent(newButton + "> ul > li:nth-child(2) > i", "icon-trash");

    click(newButton + "> ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    driver,
      getAlertText(function(result) {
        console.log(result.value);
      }),
      driver,
      pause(1000);

    acceptAlert();

    waitService.waitForMilliseconds(1000);
  });
  it("verify.removed", async () => {
    refresh();

    waitService.waitForMilliseconds(5000);

    click("body > app > section > header > button:nth-last-child(1)");

    notContainsText(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.back > ul > li:nth-last-child(1)",
      "NEW"
    );

    waitService.waitForMilliseconds(1000);

    end();
  });
});
