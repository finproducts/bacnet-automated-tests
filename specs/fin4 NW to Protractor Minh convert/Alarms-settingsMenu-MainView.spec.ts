import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-settingsMenu-MainView.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(500);
  });
  it("Switch to main view", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");
  });
  it("filter menu - period", async () => {
    click("#innerApp > section > header > button:nth-child(4)");

    waitService.waitForMilliseconds(1000);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Settings button", async () => {
    waitService.waitForMilliseconds(1000);

    moveToElement(
      "#innerApp > section > header > button:nth-last-child(1)",
      1,
      1
    );

    click(" #innerApp > section > header > button:nth-last-child(1)");
  });
  it("Settings menu", async () => {
    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(2) > div",
      "sound selector"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(2) > div"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div"
    );

    waitService.waitForMilliseconds(15000);

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(1)",
      "9"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(2)",
      "18"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(3)",
      "36"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(4)",
      "56"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(2)"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select",
      "18",
      "changed to 18"
    );

    containsText("#innerApp > section > div > span", "1 - 18 of");

    elementPresent("#innerApp > section > div > button:nth-child(3)");

    click("#innerApp > section > div > button:nth-child(3)");

    containsText("#innerApp > section > div > span", "19 - 36 of");

    cssClassPresent(
      "#innerApp > section > div > button:nth-child(1) > span",
      "icon-chevron-left"
    );

    click("#innerApp > section > div > button:nth-child(1)");

    elementPresent(
      "#innerApp > section > ul > li:nth-child(18)",
      "18 alarms per page"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(3)"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div",
      "36",
      "changed to 36"
    );

    waitService.waitForMilliseconds(5000);

    elementPresent(
      "#innerApp > section > ul > li:nth-child(36)",
      "36 alarms per page"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(4)"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div",
      "56",
      "changed to 56"
    );

    waitService.waitForMilliseconds(5000);

    containsText("#innerApp > section > div > span", "1 -");

    elementPresent("#innerApp > section > div > button:nth-child(3)");

    click("#innerApp > section > div > button:nth-child(3)");

    containsText("#innerApp > section > div > span", "37 - ", "switched page");

    cssClassPresent(
      "#innerApp > section > div > button:nth-child(1) > span",
      "icon-chevron-left"
    );

    click("#innerApp > section > div > button:nth-child(1)");

    elementPresent(
      "#innerApp > section > ul > li:nth-child(36)",
      "36 alarms per page"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(37)",
      "56 alarms per page"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(4)"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div",
      "56",
      "changed to 56"
    );

    waitService.waitForMilliseconds(5000);

    elementPresent(
      "#innerApp > section > ul > li:nth-child(37)",
      "56 alarms per page"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div > select > option:nth-child(1)"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div",
      "9",
      "changed to 9"
    );

    waitService.waitForMilliseconds(5000);

    elementPresent(
      "#innerApp > section > ul > li:nth-child(9)",
      "9 alarms per page"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Download", async () => {
    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5)"
    );

    end();
  });
});
