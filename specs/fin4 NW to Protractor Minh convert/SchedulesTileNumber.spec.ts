import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let booleanScheduleTrue = "boolScheduleTrue" + new Date().valueOf();

let booleanScheduleFalse = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleNull = "boolScheduleNull" + new Date().valueOf();

let numberSchedule = "numSchedule" + new Date().valueOf();

let stringSchedule = "strSchedule" + new Date().valueOf();

let enumSchedule = "enumSchedule" + new Date().valueOf();

describe("SchedulesTileNumber.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("Create New Schedule Form", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      numberSchedule
    );

    click("#content > div.form-item-holder.combobox > select");

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)",
      "Boolean"
    );

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)",
      "Number"
    );

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)",
      "String"
    );

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)",
      "Enum"
    );

    waitService.waitForMilliseconds(2000);
  });
  it("New Numeric Schedule-DefaultValue", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      numberSchedule
    );

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > input", "set default value ");

    click("#content > div > input");

    setValue("#content > div > input", "69.69");

    waitService.waitForMilliseconds(600);

    value("#content > div > input", "69.69");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Numeric Schedule created succesfully"
    );

    click("#controlBar > button");

    refresh();

    waitService.waitForMilliseconds(1000);
  });
  it("Assert Schedule Program Tile number equals the number from view all schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click("body > app > section > section > ul > li:nth-child(8)");

    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(4000);
  });
  it("compare", async () => {
    waitService.waitForMilliseconds(4000);

    getText(
      "body > app > section > section > ul > li:nth-child(8) > div >span:nth-last-child(1)",
      function(result) {
        getText(
          "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.num-indicator",
          function(result2) {
            console.log(result.value);

            console.log(result2.value);

            equal(result.value, result2.value);
          }
        );
      }
    );
  });
});
