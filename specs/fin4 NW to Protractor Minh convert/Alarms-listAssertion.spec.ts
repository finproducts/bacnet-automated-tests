import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-listAssertion.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    moveToElement(
      "body > app > section > header > button.link-button.pna.launcher-open-btn",
      1,
      1
    );

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(500);
  });
  it("Switch to main view", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");
  });
  it("list ", async () => {
    click("#innerApp > section > header > button:nth-child(2)");
  });
  it("assert list headers", async () => {
    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(2) > span",
      "STATUS"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(3) > span.single-line-text",
      "DATE/TIME"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(4) > span",
      "NAME"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(5) > span",
      "SOURCE"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(6) > span",
      "STATE"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(7) > span",
      "PRIORITY"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(8) > span",
      "MESSAGE"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(9) > span",
      "ACK"
    );

    containsText(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(1) > th:nth-child(10) > span",
      "ACK BY"
    );
  });
  it("info button & form", async () => {
    click(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(2) > td:nth-child(1) > button"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > h5"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > header > h5",
      "CITY CENTER VAV"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > thead > tr > th:nth-child(1)",
      "PROPERTY"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > thead > tr > th:nth-child(2)",
      "VALUE"
    );

    moveToElement(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(1) > td:nth-child(1)",
      3,
      3
    );

    elementPresent(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(2) > td:nth-child(2) > div > span"
    );

    cssClassPresent(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(2) > td:nth-child(2) > div > span",
      "icon bell red"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(1) > td:nth-child(1) > span",
      "dis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(1) > td:nth-child(1) > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(2) > td:nth-child(1) > span",
      "id"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(2) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(3) > td:nth-child(1) > span",
      "alarm"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(3) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(4) > td:nth-child(1) > span",
      "alarmStatus"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(4) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(5) > td:nth-child(1) > span",
      "alarmText"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(5) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(6) > td:nth-child(1) > span",
      "equipRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(6) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(7) > td:nth-child(1) > span",
      "equipRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(7) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(8) > td:nth-child(1) > span",
      "floorRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(8) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(9) > td:nth-child(1) > span",
      "floorRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(9) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(10) > td:nth-child(1) > span",
      "instructions"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(10) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(11)> td:nth-child(1) > span",
      "periods"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(11) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(12) > td:nth-child(1) > span",
      "priority"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(12) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(13) > td:nth-child(1) > span",
      "siteRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(13) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(14) > td:nth-child(1) > span",
      "siteRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(14) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(15) > td:nth-child(1) > span",
      "targetRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(15) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(16) > td:nth-child(1) > span",
      "targetRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(16) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(17) > td:nth-child(1) > span",
      "ts"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(17) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(18) > td:nth-child(1) > span",
      "tsTimezone"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(18) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(19) > td:nth-child(1) > span",
      "tsOffset"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(18) > td.mono.grow-double.horizontal.middle > span"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Acknowledge", async () => {
    click(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(2) > td:nth-child(9) > button"
    );

    elementPresent(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table > tr:nth-child(2) > td:nth-child(9) > div > span"
    );
  });
  it("pause to be moved as the functions are created", async () => {
    waitService.waitForMilliseconds(500);
  });
});
