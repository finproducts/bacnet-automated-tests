import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

describe("SchedulesMiscellaneous.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("Go to Miscellaneous", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Backup", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click("#content > div.form-item-holder.textinput > input");

    clearValue("#content > div.form-item-holder.textinput > input");

    setValue(
      "#content > div.form-item-holder.textinput > input",
      "SchedulesBackup"
    );

    click("#content > div:nth-child(5) > div > div > input");

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(1)"
    );

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(1)"
    );

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(1)"
    );

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(3)"
    );

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(4)"
    );

    keys(driver, Keys, CONTROL);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Restore", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > a"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click("#content > div:nth-child(2) > div > div");

    waitService.waitForMilliseconds(500);

    click("#content > div:nth-child(3) > div > div > input");

    waitService.waitForMilliseconds(500);

    click("#content > div:nth-child(2) > div > div");

    waitService.waitForMilliseconds(500);

    click("#content > div:nth-child(3) > div > div > input");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Organize", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) "
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    setValue("#content > div.form-item-holder.textinput > input", "newPath");

    click("#controlBar > button:nth-child(2)");

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) "
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(5000);
  });
});
