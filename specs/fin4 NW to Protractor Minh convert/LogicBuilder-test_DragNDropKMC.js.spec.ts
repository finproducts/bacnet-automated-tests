import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("LogicBuilder-test_DragNDropKMC.js.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add a variable and drag it to stage in KMC", async () => {
    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".variables > center > span > span:nth-child(2) > label");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      ".variables .scroller div  b.variable.get.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .get.variable.block",
      "The variable getter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 50);

        app.set("blocks.1.y", 50);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      ".variables .scroller .variable  b.variable.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.variable.block",
      "The variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 100);

        app.set("blocks.2.y", 100);
      },
      [],
      function(result) {}
    );
  });
  it("Add a routine and drag it to stage in KMC", async () => {
    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(500);

    click(".variables > center > span > span:nth-child(3) > label");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [".variables .scroller .function > b", ".blocks"]);

    waitService.waitForMilliseconds(1000);

    elementPresent(".blocks .function.block", "The routine block is on stage");

    execute(
      function(simple) {
        app.set("blocks.3.x", 200);

        app.set("blocks.3.y", 200);
      },
      [],
      function(result) {}
    );
  });
  it("Add a macroblock and drag it to stage in KMC", async () => {
    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);

    click(".variables > center > span > span:nth-child(4) > label");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [".variables .scroller .macro  b", ".blocks"]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block",
      "The macro block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.4.x", 300);

        app.set("blocks.4.y", 300);
      },
      [],
      function(result) {}
    );
  });
});
