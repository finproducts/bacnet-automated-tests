import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-addVarTest.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    click();

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(10)"
    );

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);
  });
  it("Add Num", async () => {
    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    keys("numVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    setValue("#content > div.form-item-holder.numericstepper > input", "100");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    moveToElement("#controlBar > button", 1, 1);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);
  });
  it("Add Bool", async () => {
    frame(null);

    waitService.waitForMilliseconds(3000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(3000);

    click("#content > div.form-item-holder.textinput > input");

    keys("boolVar");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    moveToElement("#controlBar > button", 1, 1);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);
  });
  it("Add Str", async () => {
    frame(null);

    waitService.waitForMilliseconds(3000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    keys("strVar");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    setValue(
      "#content > div.form-item-holder.textinput > input",
      "stringValue"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1500);

    moveToElement("#controlBar > button", 1, 1);

    click("#controlBar > button");

    waitService.waitForMilliseconds(4000);
  });
  it("Add Date", async () => {
    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    keys("dateVar");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > select > option:nth-child(2)");

    click("#content > div:nth-child(3) > select > option:nth-child(2)");

    click("#content > div:nth-child(4) > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(2000);

    moveToElement("#controlBar > button", 1, 1);

    waitService.waitForMilliseconds(3000);

    moveToElement("#controlBar > button", 1, 1);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);
  });
  it("Add time", async () => {
    frame(null);

    waitService.waitForMilliseconds(3000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(1000);

    keys("timeVar");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(5)"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(1) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(2) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(3) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(4) > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(3000);

    moveToElement("#controlBar > button", 1, 1);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);
  });
  it("Add Date&Time", async () => {
    frame(null);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    setValue(
      "#content > div.form-item-holder.textinput > input",
      "dateAndTimeVar"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(6)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(2000);

    click("#content > div:nth-child(2) > select > option:nth-child(2)");

    click("#content > div:nth-child(3) > select > option:nth-child(2)");

    click("#content > div:nth-child(4) > select > option:nth-child(2)");

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(1) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(2) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(3) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(4) > select > option:nth-child(2)"
    );

    click("#controlBar > button:nth-child(2)");

    moveToElement("#controlBar > button", 1, 1);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
});
