import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let settingsButton = po.settingsButton;

let root =
  "body > app > section > div > div.app-nav.vertical > nav > div:nth-child";

let OK = "#controlBar > button:nth-child(2)";

let host = "smtp.gmail.com";

let username = "mailexttest@gmail.com";

let password = "P@avelmailtest";

let fromAddress = "mailexttest@gmail.com";

let port = "465";

let subject = "Test Email";

let message = "This Email sent via automated test";

let URL = "mail.google.com";

let signin =
  "body > nav > div > a.gmail-nav__nav-link.gmail-nav__nav-link__sign-in";

let query = 'exts().findAll myRow => myRow.dis.contains("Email")';

describe("Settings-ext.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("go to settings > ext", async () => {
    click(settingsButton + " > ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Assertions", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1) > header > h4",
      "— Settings —"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div",
      "Refresh"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-item-icon.icon-retweet",
      "icon-retweet"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > div > h6",
      "Ext Settings"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("email extension assertion", async () => {
    attributeContains(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11) > a > div > div > img  ",
      "src",
      "/pod/emailExt/res/img/icon24.png"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Email"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11) > a > div > div > span.num-indicator.error",
      "error"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("click email", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11) > ul > li"
    );

    waitService.waitForMilliseconds(2000);
  });
  it("go to back to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("go to settings > proj", async () => {
    click(settingsButton + " > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(1000);
  });
  it("click on email smtp settings", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6)"
    );

    waitService.waitForMilliseconds(500);

    containsText("#title", "Email SMTP Settings");

    containsText("#content > div:nth-child(1) > label", "Host");

    elementPresent("#content > div:nth-child(1) > input");

    containsText("#content > div:nth-child(2) > label", "Username");

    elementPresent("#content > div:nth-child(2) > input");

    containsText("#content > div:nth-child(3) > label", "Password");

    elementPresent("#content > div:nth-child(3) > input");

    containsText("#content > div:nth-child(4) > label", "From Address");

    elementPresent("#content > div:nth-child(4) > input");

    containsText(
      "#content > div.form-item-holder.numericstepper > label",
      "Port"
    );

    elementPresent("#content > div.form-item-holder.numericstepper > input");

    containsText("#content > div:nth-child(6) > div > label", "Use SSL");

    elementPresent("#content > div:nth-child(6) > div > div");

    containsText(
      "#content > div:nth-child(7) > div > label",
      "Test SMTP Configuration"
    );

    elementPresent("#content > div:nth-child(7) > div > div");

    waitService.waitForMilliseconds(1000);
  });
  it("Fill the details", async () => {
    clearValue("#content > div:nth-child(1) > input");

    setValue("#content > div:nth-child(1) > input", host);

    clearValue("#content > div:nth-child(2) > input");

    setValue("#content > div:nth-child(2) > input", username);

    clearValue("#content > div:nth-child(3) > input");

    setValue("#content > div:nth-child(3) > input", password);

    clearValue("#content > div:nth-child(4) > input");

    setValue("#content > div:nth-child(4) > input", fromAddress);

    clearValue("#content > div.form-item-holder.numericstepper > input");

    setValue("#content > div.form-item-holder.numericstepper > input", port);

    click("#content > div:nth-child(6) > div > div");

    click("#content > div:nth-child(7) > div > div");

    click(OK);

    waitService.waitForMilliseconds(1000);
  });
  it("test SMTP", async () => {
    containsText("#title", "Test SMTP Configuration");

    containsText(
      "#content > div.form-item-holder.label > label.itemEditor",
      host
    );

    containsText("#content > div:nth-child(2) > label", "To");

    clearValue("#content > div:nth-child(2) > input");

    setValue("#content > div:nth-child(2) > input", fromAddress);

    containsText("#content > div:nth-child(3) > label", "Subject");

    clearValue("#content > div:nth-child(3) > input");

    setValue("#content > div:nth-child(3) > input", subject);

    containsText("#content > div.form-item-holder.textarea > label", "Message");

    clearValue("#content > div.form-item-holder.textarea > textarea");

    setValue("#content > div.form-item-holder.textarea > textarea", message);

    click(OK);

    waitService.waitForMilliseconds(15000);

    containsText("#content > div > textarea", "*** SUCCESS! ***");

    click("#controlBar > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("Query", async () => {
    finEval(query, function(output) {
      console.log(output);

      if (
        output.length > 0 &&
        output[0].name == "email" &&
        output[0].dis == "Email" &&
        output[0].extStatus == "ok" &&
        output[0].enabled == "✓" &&
        output[0].licensed == "✓" &&
        output[0].version == "2.1.15" &&
        output[0].settings == "emailExt::EmailSettings"
      ) {
        console.log("OK");
      } else {
        console.log("error");
      }
    });

    waitService.waitForMilliseconds(1000);
  });
  it("Login to gmail", async () => {
    url("https://mail.google.com");

    clearValue("#identifierId");

    setValue("#identifierId", username);

    click("#identifierNext > content > span");

    waitService.waitForMilliseconds(2000);

    clearValue("._input");

    setValue(
      '#password > div:nth-child(1) > div > div:nth-child(1) > input[type="password"]',
      password
    );

    click("#passwordNext > content > span");

    waitService.waitForMilliseconds(5000);

    containsText(
      'div > div > div > div > div > div > div > div > div > table[cellpadding="0"] > tbody > tr > td:nth-child(6) > div > div >div > span:nth-child(2)',
      message
    );

    waitService.waitForMilliseconds(2000);
  });
});
