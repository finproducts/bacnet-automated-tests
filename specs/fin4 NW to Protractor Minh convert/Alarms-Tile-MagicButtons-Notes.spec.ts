import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectors =
  "#innerApp > section > ul > li:nth-child(1n+0) > div.horizontal.middle.margin.small.v-sides > h1";

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-Tile-MagicButtons-Notes.js", () => {
  it("go to City Center Floor-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("Magic buttons", async () => {
    click(
      "#innerApp > section > ul > li:nth-child(1) > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Notes button", async () => {
    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2) > i",
      "icon-conversation",
      "Magic button has correct icon"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2) > label",
      "Notes"
    );
  });
  it("Click on Notes", async () => {
    click(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(4000);

    containsText(
      "#innerApp > header > h4",
      "— Notes —",
      "Notes app opened succesfully"
    );

    containsText(
      "#innerApp > section > ul > li > div > div.vertical.flex > h6",
      "CITY CENTER VAV-01",
      "Corect note location"
    );
  });
  it("pause to be moved as the functions are created", async () => {
    waitService.waitForMilliseconds(500);
  });
});
