import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let selectorLibrary= require('./po.js');

let logicBuilderButton= po.logicBuilderButton;

let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let programName= "test"  +  new Date().valueOf();



describe('LogicBuilder-alarmBit.js',() =>{
  
  it('go to City Center VAV-1', async () => 
    
    {
waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)');

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('body > app > section > header > button.link-button.pna.launcher-open-btn');

waitService.waitForMilliseconds(500);

click(logicBuilderButton);

waitService.waitForMilliseconds(500);

click(logicBuilderButton  +  "> ul > li:nth-child(2)");
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys(programName);

waitService.waitForMilliseconds(500);

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.list > select > option:nth-child(4)');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.list > select > option:nth-child(10)');

waitService.waitForMilliseconds(500);

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a');

waitService.waitForMilliseconds(1000);

frame(0);

waitService.waitForMilliseconds(3000);
}

    
  );  
  it('Setting the alarm', async () => 
    
    {
waitService.waitForMilliseconds(1000);

setValue('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]','alarm');

waitService.waitForMilliseconds(500);

dnd('#panel > section.blocklibrary > div > div.category > div.contents > div',600,400);

execute(dragAndDrop,['#panel > section.blocklibrary > div > div.category > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.set('blocks.1.x',600);

app.set('blocks.1.y',400);
}
,[],function(result)
{}
);

waitService.waitForMilliseconds(1000);

click('#stage > div.scroller > div > div.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div.alarmBlock.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div.alarmBlock.block > header > div');

waitService.waitForMilliseconds(1000);

click('#stage > div.scroller > div > div:nth-child(2) > header > div');

waitService.waitForMilliseconds(1000);

click('#stage > div.scroller > div > div:nth-child(2) > header > div');

click('#stage > div.scroller > div > div.alarmBlock.block > header > div');

waitService.waitForMilliseconds(1000);

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div:nth-child(1) > div > input','alarmText');

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div:nth-child(2) > div > textarea','alarmInstructions 
 Call Bob');

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div:nth-child(3) > div > textarea',69);

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div.add > input[type="text"]','testMarker');

keys(driver,Keys,ENTER);

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div.add > input[type="text"]','testMarker1');

keys(driver,Keys,ENTER);

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div.add > input[type="text"]','testMarker2');

keys(driver,Keys,ENTER);

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});