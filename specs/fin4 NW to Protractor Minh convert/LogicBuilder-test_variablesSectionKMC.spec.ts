import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;



describe('LogicBuilder-test_variablesSectionKMC.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Change Language KMC', async () => 
    
    {
click('#panel > section.controls > div.icons > i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?');

click('body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div > div > span:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('.language','KMC');

waitService.waitForMilliseconds(1500);
}

    
  );  
  it('Check interface', async () => 
    
    {
elementPresent('.variables > center .variables-tabs','Variable tabs are present');

containsText('.variables > center > span > u > b > span > label','objects','The first category is objects');

containsText('.variables > center > span > span:nth-child(2) > label','locals','The second category is locals');

containsText('.variables > center > span > span:nth-child(3) > label','routines','The third category is routines');

containsText('.variables > center > span > span:nth-child(4) > label','macros','The fourth category is macros');

elementPresent('.variables .add.panel-button .variable.symbol','Add variable button is present');

elementPresent('.variables .add.panel-button .routine.symbol','Add routine button is present');

elementPresent('.variables .add.panel-button .macro.symbol','Add macro button is present');
}

    
  );  
  it('Verify Tool Tip Add Variable', async () => 
    
    {
moveToElement('.add.panel-button .variable.symbol',1,1);

waitService.waitForMilliseconds(500);

containsText(('.ractive-tooltip'),'Add a new variable');
}

    
  );  
  it('Add Variable', async () => 
    
    {
click('.add.panel-button .variable.symbol');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

elementPresent('.variables .scroller > div > span');

elementPresent('.variables .scroller > div.variable > b.variable.get.block');

elementPresent('.variables .scroller > div.variable > b.variable.set.block');
}

    
  );  
  it('Verify Tool Tip Edit Variable', async () => 
    
    {
click('.variables .scroller > div.variable > i');

waitService.waitForMilliseconds(200);

moveToElement('.variables .scroller > div.variable > span',2,2);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.variable > i.pencil.icon',200);

moveToElement('.variables .scroller > div.variable > i.pencil.icon',1,1);

containsText(('.ractive-tooltip'),'Rename variable');
}

    
  );  
  it('Verify Tool Tip Delete Variable', async () => 
    
    {
moveToElement('.variables .scroller > div.variable > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.variable > i.cross.icon',200);

moveToElement('.variables .scroller > div.variable > i.cross.icon',1,1);

containsText(('.ractive-tooltip'),'Delete variable');
}

    
  );  
  it('Verify Tool Tip for Get', async () => 
    
    {
moveToElement('.variables .scroller > div.variable > b.variable.get.block',2,2);

containsText(('.ractive-tooltip'),'Drag to the stage to add a get block for this variable');
}

    
  );  
  it('Verify Tool Tip for Set', async () => 
    
    {
moveToElement('.variables .scroller > div.variable > b.variable.set.block',2,2);

containsText(('.ractive-tooltip'),'Drag to the stage to add a set block for this variable');
}

    
  );  
  it('Rename Variable', async () => 
    
    {
moveToElement('.variables .scroller > div.variable.variable > span',0,0);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.variable.variable > i.pencil.icon',200);

click('.variables .scroller > div.variable.variable > i.pencil.icon');

keys([driver.Keys.CONTROL, 'a']);

keys(driver,Keys,CONTROL);

keys(driver,Keys,DELETE);

keys(driver,Keys,DELETE);

keys('testVariable');

keys(driver,Keys,ENTER);

keys(driver,Keys,ENTER);

equal(await $(('.variables .scroller > div.variable.variable > span')).getText(),'testVariable');
}

    
  );  
  it('Verify Tool Tip Add Function', async () => 
    
    {
moveToElement('.add.panel-button .routine.symbol',1,1);

waitService.waitForMilliseconds(500);

containsText(('.ractive-tooltip'),'Add a new routine');
}

    
  );  
  it('Add Function', async () => 
    
    {
click('.add.panel-button .routine.symbol');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

elementPresent('.variables .scroller > div.function > span > span');
}

    
  );  
  it('Verify Tool Tip for Opening a Function', async () => 
    
    {
moveToElement('.variables .scroller > div.function > span',1,1);

containsText(('.ractive-tooltip'),'Click to open this routine.');
}

    
  );  
  it('Verify Tool Tip Edit Function', async () => 
    
    {
moveToElement('.variables .scroller > div.function > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.function > i.pencil.icon',200);

moveToElement('.variables .scroller > div.function > i.pencil.icon',1,1);

containsText(('.ractive-tooltip'),'Rename routine');
}

    
  );  
  it('Verify Tool Tip Delete Function', async () => 
    
    {
moveToElement('.variables .scroller > div.function > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.function > i.cross.icon',200);

moveToElement('.variables .scroller > div.function > i.cross.icon',1,1);

containsText(('.ractive-tooltip'),'Delete routine');
}

    
  );  
  it('Verify Tool Tip for Call()', async () => 
    
    {
moveToElement('.variables .scroller > div.function > b',2,2);

containsText(('.ractive-tooltip'),'Drag to the stage to use this routine');
}

    
  );  
  it('Rename Function', async () => 
    
    {
moveToElement('.variables .scroller > div.function > span',0,0);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.function > i.pencil.icon',200);

click('.variables .scroller > div.function > i.pencil.icon');

keys([driver.Keys.CONTROL, 'a']);

keys(driver,Keys,CONTROL);

keys(driver,Keys,DELETE);

keys(driver,Keys,DELETE);

keys('testFunction');

keys(driver,Keys,ENTER);

waitService.waitForMilliseconds(1000);

equal(await $(('.variables .scroller > div.function > span')).getText(),'testFunction');
}

    
  );  
  it('Edit Function', async () => 
    
    {
moveToElement('.variables .scroller > div.function > span',1,1);

mouseButtonClick(0);

waitService.waitForMilliseconds(500);

visible('#stage > div.breadcrumb.banner');

execute(function(simple)
{
return app.get('blocks').length;
}
,[],function(result)
{
console.log(result.value);

expect(result.value).to.equal(2);
}
);

click('#stage > div.breadcrumb.banner > b');

waitService.waitForMilliseconds(400);

element('#stage > div.breadcrumb.banner');

to;

not;

be;

present;
}

    
  );  
  it('Verify Tool Tip Add MacroBlock ', async () => 
    
    {
moveToElement('.add.panel-button .macro.symbol',1,1);

waitService.waitForMilliseconds(500);

containsText(('.ractive-tooltip'),'Add a new macro block');
}

    
  );  
  it('Add MacroBlock ', async () => 
    
    {
click('.add.panel-button .macro.symbol');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

elementPresent('.variables .scroller > div.macro > span > span');
}

    
  );  
  it('Verify Tool Tip for Macro Block', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > span',1,1);

containsText('.ractive-tooltip','Click to edit this macro block's code');
}

    
  );  
  it('Verify Tool Tip Edit Macro Block', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.macro > i.pencil.icon',200);

moveToElement('.variables .scroller > div.macro > i.pencil.icon',1,1);

containsText(('.ractive-tooltip'),'Rename macro block');
}

    
  );  
  it('Verify Tool Tip Delete Macro Block', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.macro > i.cross.icon',200);

moveToElement('.variables .scroller > div.macro > i.cross.icon',1,1);

containsText(('.ractive-tooltip'),'Delete macro block');
}

    
  );  
  it('Verify Tool tip for block(dragable)', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > b',2,2);

containsText(('.ractive-tooltip'),'Drag to the stage to use this macro block');
}

    
  );  
  it('Rename macro', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > span',0,0);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.macro  > i.pencil.icon',200);

click('.variables .scroller > div.macro  > i.pencil.icon');

keys([driver.Keys.CONTROL, 'a']);

keys(driver,Keys,CONTROL);

keys(driver,Keys,DELETE);

keys(driver,Keys,DELETE);

keys('testMacro');

keys(driver,Keys,ENTER);

keys(driver,Keys,ENTER);

equal(await $(('.variables .scroller > div.macro > span')).getText(),'testMacro');
}

    
  );  
  it('Edit Macro Block', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > span',1,1);

mouseButtonClick(0);

waitService.waitForMilliseconds(500);

elementPresent('.macro.modal > div > div.window');

click('body > app > div.macro.modal > div > div.window > section > div.panel > div > div.tac.mtm > button > i');

waitService.waitForMilliseconds(300);

elementPresent('.macro.modal table tr:nth-child(3)');

click('.macro.modal table tr:nth-child(4) td:nth-child(1) input');

waitService.waitForMilliseconds(300);

keys([driver.Keys.CONTROL,'a',driver.Keys.CONTROL,driver.Keys.DELETE, driver.Keys.DELETE]);

keys('teava');

value('.macro.modal table tr:nth-child(4) td:nth-child(1) input','teava');

click('.macro.modal table tr:nth-child(4) td:nth-child(2) div label');

waitService.waitForMilliseconds(500);

elementPresent('.dropdown.open');

click('.macro.modal table > tbody > tr:nth-child(4) > td:nth-child(2) > div > select > option:nth-child(2)');

waitService.waitForMilliseconds(300);

containsText('.macro.modal table tbody tr:nth-child(4) td:nth-child(2) div label','output');

click('.macro.modal table tr:nth-child(4) td:nth-child(3) div label');

waitService.waitForMilliseconds(500);

click('.macro.modal table tbody tr:nth-child(4) td:nth-child(3) div select option:nth-child(2)');

waitService.waitForMilliseconds(300);

containsText('.macro.modal table tbody tr:nth-child(4) td:nth-child(3) div label','integer');

click('.macro.modal table > tbody > tr:nth-child(4) > td:nth-child(5) > i');

waitService.waitForMilliseconds(300);

elementNotPresent('.macro.modal table tr:nth-child(4)');

click('body > app > div.macro.modal > div > div.close > i');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Delete Macro', async () => 
    
    {
moveToElement('.variables .scroller > div.macro > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.macro > i.pencil.icon',200);

click('.variables .scroller > div.macro > i.cross.icon');

waitService.waitForMilliseconds(500);

element('.variables .scroller > div.macro');

to;

not;

be;

present;
}

    
  );  
  it('Delete Function', async () => 
    
    {
click('#panel > section.variables > center > span > span:nth-child(3) > label');

waitService.waitForMilliseconds(500);

moveToElement('.variables .scroller > div.function > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.function > i.cross.icon',200);

click('.variables .scroller > div.function > i.cross.icon');

waitService.waitForMilliseconds(500);

element('.variables .scroller > div.function');

to;

not;

be;

present;
}

    
  );  
  it('Use Search', async () => 
    
    {
click('#panel > section.variables > center > span > span:nth-child(2) > label');

waitService.waitForMilliseconds(500);

click('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]');

keys('TestVariable');

waitService.waitForMilliseconds(500);

elementPresent('.variable.variable');

keys([driver.Keys.CONTROL,'a',driver.Keys.CONTROL,driver.Keys.DELETE,driver.Keys.DELETE]);

waitService.waitForMilliseconds(200);

click('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]');

keys('asd');

waitService.waitForMilliseconds(500);

elementNotPresent('#panel > section.variables > div > div:nth-child(2)');

keys([driver.Keys.CONTROL,'a',driver.Keys.CONTROL,driver.Keys.DELETE,driver.Keys.DELETE]);

waitService.waitForMilliseconds(200);
}

    
  );  
  it('Delete Variable', async () => 
    
    {
moveToElement('.variables .scroller > div.variable.variable > span',1,1);

waitService.waitForMilliseconds(200);

waitForElementVisible('.variables .scroller > div.variable.variable > i.cross.icon',200);

click('.variables .scroller > div.variable.variable > i.cross.icon');

waitService.waitForMilliseconds(500);

element('.variables .scroller > div.variable.variable');

to;

not;

be;

present;
}

    
  );  
  
  
});