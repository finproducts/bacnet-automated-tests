import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let settingsButton = po.settingsButton;

describe("SettingsGeneral.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("settings menu", async () => {
    containsText(
      settingsButton + " > ul > li:nth-child(1) > span.italic.non-interactive",
      "Close"
    );

    cssClassPresent(
      settingsButton +
        " > ul > li:nth-child(1) > span.icon-arrow-thin-left.mnh.non-interactive",
      "icon-arrow-thin-left"
    );

    containsText(
      settingsButton + " > ul > li:nth-child(2) > span.italic.non-interactive",
      "Ext"
    );

    cssClassPresent(
      settingsButton +
        "> ul > li:nth-child(2) > span.icon-gear.mnh.non-interactive",
      "icon-gear"
    );

    containsText(
      settingsButton + " > ul > li:nth-child(3) > span.italic.non-interactive",
      "Proj"
    );

    cssClassPresent(
      settingsButton +
        "> ul > li:nth-child(3) > span.icon-toggles.mnh.non-interactive",
      "icon-toggles"
    );

    containsText(
      settingsButton + " > ul > li:nth-child(4) > span.italic.non-interactive",
      "Header"
    );

    cssClassPresent(
      settingsButton +
        " > ul > li:nth-child(4) > span.icon-driver.mnh.non-interactive",
      "icon-driver"
    );

    containsText(
      settingsButton + " > ul > li:nth-child(5) > span.italic.non-interactive",
      "Alarm"
    );

    cssClassPresent(
      settingsButton +
        "> ul > li:nth-child(5) > span.icon-alarm.mnh.non-interactive",
      "icon-alarm"
    );

    waitService.waitForMilliseconds(2000);
  });
  it("Settings-close", async () => {
    click(settingsButton + " > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);

    containsText(
      "body > app > section > section > ul > li:nth-child(26) > div > span.non-interactive.single-line-text.item-name",
      "Settings"
    );

    waitService.waitForMilliseconds(1000);

    click(settingsButton);

    waitService.waitForMilliseconds(500);
  });
  it("Settings-Alarm", async () => {
    elementPresent(settingsButton + " > ul > li:nth-child(5)");

    moveToElement(settingsButton + " > ul > li:nth-child(5)", 1, 1);

    waitService.waitForMilliseconds(1000);

    click(settingsButton + " > ul > li:nth-child(5)");

    elementPresent("#title");

    containsText("#title", "Alarm Settings");

    waitService.waitForMilliseconds(1000);
  });
  it("Clear Value-input 0", async () => {
    click("#content > div.form-item-holder.numericstepper > input");

    clearValue("#content > div.form-item-holder.numericstepper > input");

    setValue("#content > div.form-item-holder.numericstepper > input", 0);

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1500);
  });
  it("check the value is 3", async () => {
    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(1000);

    click(settingsButton + " > ul > li:nth-child(5)");

    valueContains(
      "#content > div.form-item-holder.numericstepper > input",
      3,
      "value is set as the minimum 3"
    );

    getText("#content > div.form-item-holder.numericstepper > input", function(
      result
    ) {
      console.log(result.value);

      containsText(
        "#content > div.form-item-holder.numericstepper > input",
        result.value
      );
    });

    waitService.waitForMilliseconds(1000);
  });
});
