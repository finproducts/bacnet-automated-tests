import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let trendName = "trend" + new Date().valueOf();

describe("TEST-finstackMenuTest.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );
  });
  it("FINSTACK LOGO", async () => {
    elementPresent(
      "body > app > section > header > div.horizontal.middle.pnh.app-logo-container > img"
    );
  });
  it("menu assertion", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "body > app > section > section > ul > div.horizontal.middle.divider-item.no-margin > h6",
      "END USER APPS",
      "END USER APPS divider"
    );

    containsText(
      "body > app > section > section > ul > div.horizontal.middle.divider-item.no-margin > span:nth-child(2)",
      "su",
      "SU"
    );

    containsText(
      "body > app > section > section > ul > div.horizontal.middle.divider-item.no-margin > span.bold.link-item.label-item",
      "Logout",
      "Logout"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(3)",
      "Alarms",
      "Alarms"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(4)",
      "Graphics",
      "Graphics"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(5)",
      "Historian",
      "Historian"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(6)",
      "Notes",
      "Notes"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(7)",
      "Point Graphics",
      "Point Graphics"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(8)",
      "Schedules",
      "Schedules"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(9)",
      "Summary",
      "Summary"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(10)",
      "Weather",
      "Weather"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(11)",
      "Favorites",
      "Favorites"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(12)",
      "O&M Manuals",
      "O&M Manuals"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(13)",
      "Overrides",
      "Overrides"
    );
  });
  it("SYSTEM INTEGRATOR APPS", async () => {
    containsText(
      "body > app > section > section > ul > div:nth-child(14) > h6",
      "SYSTEM INTEGRATOR APPS",
      "SYSTEM INTEGRATOR APPS divider"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(15)",
      "Connectors",
      "Connectors"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(16)",
      "DB Builder",
      "DB Builder"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(17)",
      "Graphics Builder",
      "Graphics Builder"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(18)",
      "Logic Builder",
      "Logic Builder"
    );

    containsText(
      "body > app > section > section > ul > li:nth-child(19)",
      "Summary Builder",
      "Summary Builder"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("ADVANCED APPS", async () => {
    containsText(
      "body > app > section > section > ul > div:nth-child(20)",
      "ADVANCED APPS",
      "ADVANCED APPS divider"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(21)",
      "Debug",
      "Debug"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(22)",
      "Folio",
      "Folio"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(23)",
      "Funcs",
      "Funcs"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(24)",
      "Host",
      "Host"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(25)",
      "Jobs",
      "Jobs"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(26)",
      "Settings",
      "Settings"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(27)",
      "StackHub",
      "StackHub"
    );

    containsText(
      "body > app > section > section > ul > div:nth-child(28)",
      "Users",
      "Users"
    );
  });
  it("Action Buttons", async () => {
    containsText(
      "body > app > section > div > div.tabbar.horizontal.middle.center.no-flex > button:nth-child(1) > label",
      "Navigation",
      "Navigation"
    );

    containsText(
      "body > app > section > div > div.tabbar.horizontal.middle.center.no-flex > button:nth-child(2) > label",
      "Mini App",
      "Mini App"
    );

    containsText(
      "body > app > section > div > div.tabbar.horizontal.middle.center.no-flex > button:nth-child(3) > label",
      "Menu",
      "Menu"
    );

    getValue(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(9) > a > div > div > span"
    );

    waitService.waitForMilliseconds(500);
  });
});
