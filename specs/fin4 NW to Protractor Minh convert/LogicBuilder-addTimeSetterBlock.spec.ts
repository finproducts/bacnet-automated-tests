import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-addTimeSetterBlock.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);
  });
  it("Add variables", async () => {
    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.textinput > input");

    keys("timeVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(5)"
    );

    click("#controlBar > button:nth-child(2)");

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(1) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(2) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(3) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(4) > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(300);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(300);

    click("#controlBar > button");

    frame(0);

    waitService.waitForMilliseconds(9000);
  });
  it("Drag the Time Set block on stage", async () => {
    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div > b.time.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#stage > div.scroller > div > div.set.time.block",
      "The time setter block is on stage"
    );

    waitService.waitForMilliseconds(1000);

    execute(
      function(simple) {
        app.set("blocks.1.x", 110);

        app.set("blocks.1.y", 304);

        app.set("blocks.2.x", 120);

        app.set("blocks.2.y", 324);
      },
      [],
      function(result) {}
    );

    waitService.waitForMilliseconds(300);

    click("#stage > div.scroller > div > div.set.time.block");

    waitService.waitForMilliseconds(300);

    waitService.waitForMilliseconds(1000);

    keys([driver.Keys.CONTROL, "a"]);

    waitService.waitForMilliseconds(1000);

    keys([driver.Keys.CONTROL, "c"]);

    waitService.waitForMilliseconds(1000);

    keys([driver.Keys.CONTROL, "v"]);

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(1000);
  });
});
