import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-liveValues.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(3)");

    waitService.waitForMilliseconds(1000);
  });
  it("Edit  AHUFDD", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Live Values", async () => {
    frame(0);

    elementPresent(
      "#panel > section.controls > div.icons > i.feed.icon",
      "live values button is present"
    );

    waitService.waitForMilliseconds(1000);

    click("#panel > section.controls > div.icons > i.feed.icon");

    waitService.waitForMilliseconds(2000);

    elementPresent(
      "#panel > section.controls > div.icons > i.feed.icon.active",
      "live values is lit up"
    );

    click("#panel > section.controls > div.icons > i.feed.icon.active");

    waitService.waitForMilliseconds(2000);

    click("#panel > section.controls > div.icons > i.feed.icon");

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#stage > div.scroller > div > div.get.boolean.block > div.livedata",
      "live data"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Live Values Numeric", async () => {
    elementPresent(
      "#stage > div.scroller > div > div:nth-child(13) > div.livedata"
    );

    getText(
      "#stage > div.scroller > div > div:nth-child(13) > div.livedata",
      function(result) {
        console.log(result.value);

        console.assert(!isNaN(result.value), "should be a number");
      }
    );
  });
});
