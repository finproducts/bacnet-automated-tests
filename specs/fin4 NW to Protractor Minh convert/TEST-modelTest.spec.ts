import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let globalVariable = 100;

let selectorLibrary = require("./po.js");

let graphicsButton = po.graphicsButton;

describe("TEST-modelTest.js", () => {
  it("go to City Center vav-01", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("go to Graphics", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(graphicsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("Graphics Title Assertion", async () => {
    containsText("#innerApp > header > h4", "— Graphics —");

    waitService.waitForMilliseconds(1000);
  });
});
