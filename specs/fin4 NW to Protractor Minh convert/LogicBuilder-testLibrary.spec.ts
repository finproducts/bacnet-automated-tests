import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let commands= require('../fixtures/execute/libraryVars.js').commands;

let command= require('../fixtures/execute/libraryVars.js').command;

let controlFlow= require('../fixtures/execute/libraryVars.js').controlFlow;

let expression= require('../fixtures/execute/libraryVars.js').expression;

let language= require('../fixtures/execute/libraryVars.js').language;

let logical= require('../fixtures/execute/libraryVars.js').logical;

let math= require('../fixtures/execute/libraryVars.js').math;

let strings= require('../fixtures/execute/libraryVars.js').strings;

let time= require('../fixtures/execute/libraryVars.js').time;

let weather= require('../fixtures/execute/libraryVars.js').weather;

let status= require('../fixtures/execute/libraryVars.js').status;

let trigonometry= require('../fixtures/execute/libraryVars.js').trigonometry;

let value= require('../fixtures/execute/libraryVars.js').value;

let logic= require('../fixtures/execute/libraryVars.js').logic;

let ui= require('../fixtures/execute/libraryVars.js').ui;

let utilities= require('../fixtures/execute/libraryVars.js').utilities;



describe('LogicBuilder-testLibrary.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Change Language JavaScript', async () => 
    
    {
click('#panel > section.controls > div.icons > i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?');

click('body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div > div > span:nth-child(1)');

waitService.waitForMilliseconds(500);

containsText(('.language'),'JavaScript');
}

    
  );  
  it('Library categories JS', async () => 
    
    {
execute(controlFlow,[],function(result)
{
elementPresent(result.value,'Control Flow');
}
);

execute(language,[],function(result)
{
elementPresent(result.value,'Language');
}
);

execute(math,[],function(result)
{
elementPresent(result.value,'Math');
}
);

execute(logic,[],function(result)
{
elementPresent(result.value,'Logic');
}
);

execute(ui,[],function(result)
{
elementPresent(result.value,'Ui');
}
);

execute(utilities,[],function(result)
{
elementPresent(result.value,'Utilities');
}
);
}

    
  );  
  it('Change Language KMC', async () => 
    
    {
click('#panel > section.controls > div.icons > i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?');

click('body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div > div > span:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText(('.language'),'KMC');
}

    
  );  
  it('Library categories KMC', async () => 
    
    {
execute(controlFlow,[],function(result)
{
elementPresent(result.value,'Control Flow');
}
);

execute(language,[],function(result)
{
elementPresent(result.value,'Language');
}
);

execute(math,[],function(result)
{
elementPresent(result.value,'Math');
}
);

execute(logical,[],function(result)
{
elementPresent(result.value,'Logical');
}
);

execute(time,[],function(result)
{
elementPresent(result.value,'Time');
}
);

execute(command,[],function(result)
{
elementPresent(result.value,'Command');
}
);

execute(status,[],function(result)
{
elementPresent(result.value,'Status');
}
);

execute(trigonometry,[],function(result)
{
elementPresent(result.value,'Trigonometry');
}
);

execute(value,[],function(result)
{
elementPresent(result.value,'Value');
}
);
}

    
  );  
  it('Change Language bLine', async () => 
    
    {
click('#panel > section.controls > div.icons > i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program');

containsText('body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?');

click('body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div > div > span:nth-child(6)');

waitService.waitForMilliseconds(500);

containsText(('.language'),'bLine');
}

    
  );  
  it('Library categories bLine', async () => 
    
    {
execute(controlFlow,[],function(result)
{
elementPresent(result.value,'Control Flow');
}
);

execute(language,[],function(result)
{
elementPresent(result.value,'Language');
}
);

execute(math,[],function(result)
{
elementPresent(result.value,'Math');
}
);

execute(logical,[],function(result)
{
elementPresent(result.value,'Logical');
}
);

execute(time,[],function(result)
{
elementPresent(result.value,'Time');
}
);

execute(commands,[],function(result)
{
elementPresent(result.value,'Commands');
}
);

execute(expression,[],function(result)
{
elementPresent(result.value,'Expression');
}
);

execute(strings,[],function(result)
{
elementPresent(result.value,'Strings');
}
);

execute(weather,[],function(result)
{
elementPresent(result.value,'Weather');
}
);
}

    
  );  
  it('Use Search in bLine library', async () => 
    
    {
click('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

keys('if');

waitService.waitForMilliseconds(300);

elementPresent('.blocklibrary > div > div.category > div.contents > div');

clearValue('.blocklibrary .filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Drag cmpEq block to the stage', async () => 
    
    {
setValue('.blocklibrary .filters > div > input[type="text"]','cmpeq');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .cmpEq.block','The cmpEq block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',50);

app.set('blocks.1.y',50);
}
,[],function(result)
{}
);

elementPresent('.blocks .cmpEq.block > header > div.ractive-select','Dropdown to choose from block family is present for cmpEq block');

clearValue('.blocklibrary .filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Switch between compare blocks family', async () => 
    
    {
click('.blocks .cmpEq.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(2)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(2) > li:nth-child(2)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .cmpGt.block','Block changed to cmpGt after selecting the option from the dropdown list');

click('.blocks .cmpGt.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(2)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(2) > li:nth-child(3)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .cmpGtEq.block','Block changed to cmpGtEq after selecting the option from the dropdown list');

click('.blocks .cmpGtEq.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(2)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(2) > li:nth-child(4)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .cmpLt.block','Block changed to cmpLt after selecting the option from the dropdown list');

click('.blocks .cmpLt.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(2)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(2) > li:nth-child(5)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .cmpLtEq.block','Block changed to cmpLtEq after selecting the option from the dropdown list');

click('.blocks .cmpLtEq.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(2)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(2) > li:nth-child(6)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .cmpNotEq.block','Block changed to cmpNotEq after selecting the option from the dropdown list');
}

    
  );  
  it('Drag add block to the stage', async () => 
    
    {
setValue('.blocklibrary > div > div.filters > div > input[type="text"]','add');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div:nth-child(3) > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .add.block','The add block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',150);

app.set('blocks.2.y',150);
}
,[],function(result)
{}
);

elementPresent('.blocks .add.block > header > div.ractive-select','Dropdown to choose from block family is present for add block');

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Switch between add blocks family', async () => 
    
    {
click('.blocks .add.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(3)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(3) > li:nth-child(2)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .mult.block','Block changed to 'mult' after selecting the option from the dropdown list');

click('.blocks .mult.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(3)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(3) > li:nth-child(3)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .sub.block','Block changed to 'sub' after selecting the option from the dropdown list');

click('.blocks .sub.block > header > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul:nth-child(3)','open','Dropdown is open after clicking the block name');

click('#ractive-select-dropdown-container > ul:nth-child(3) > li:nth-child(4)');

waitService.waitForMilliseconds(500);

elementPresent('.blocks .div.block ','Block changed to 'div' after selecting the option from the dropdown list');
}

    
  );  
  
  
});