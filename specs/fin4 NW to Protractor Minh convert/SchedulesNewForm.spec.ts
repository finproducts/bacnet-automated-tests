import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let booleanScheduleTrue = "boolScheduleTrue" + new Date().valueOf();

let booleanScheduleFalse = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleNull = "boolScheduleNull" + new Date().valueOf();

let numberSchedule = "numSchedule" + new Date().valueOf();

let stringSchedule = "strSchedule" + new Date().valueOf();

let enumSchedule = "enumSchedule" + new Date().valueOf();

describe("SchedulesNewForm.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("Create New Schedule Form", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleTrue
    );

    click("#content > div.form-item-holder.combobox > select");

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)",
      "Boolean"
    );

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)",
      "Number"
    );

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)",
      "String"
    );

    elementPresent(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)",
      "Enum"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("New Boolean Schedule-DefaultTrue", async () => {
    click("#content > div.form-item-holder.checkbox > div");

    click("#content > div.form-item-holder.checkbox > div");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True-False-Null option");

    click("#content > div > select > option:nth-child(1)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#content > div > select > option:nth-child(1)");

    click("#content > div > select > option:nth-child(2)");

    click("#content > div > select > option:nth-child(3)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With TRUE Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("New Boolean Schedule-DefaultFalse", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalse
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True-False-Null option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#content > div > select > option:nth-child(4)");

    click("#content > div > select > option:nth-child(5)");

    click("#content > div > select > option:nth-child(6)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With FALSE Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("New Boolean Schedule-DefaultNull", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleNull
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True-False-Null option");

    click("#content > div > select > option:nth-child(3)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#content > div > select > option:nth-child(7)");

    click("#content > div > select > option:nth-child(8)");

    click("#content > div > select > option:nth-child(9)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With NULL Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("New Numeric Schedule-DefaultValue", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      numberSchedule
    );

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > input", "set default value ");

    click("#content > div > input");

    setValue("#content > div > input", "69.69");

    waitService.waitForMilliseconds(600);

    value("#content > div > input", "69.69");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(600);

    click("#content > div > select > option:nth-child(1)");

    click("#content > div > select > option:nth-child(2)");

    click("#content > div > select > option:nth-child(3)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Numeric Schedule created succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("New String Schedule", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      stringSchedule
    );

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)"
    );

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > input", "set default value ");

    click("#content > div > input");

    setValue("#content > div > input", "stringDefaultValue");

    waitService.waitForMilliseconds(600);

    value("#content > div > input", "stringDefaultValue");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(600);

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "String Schedule created succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("New Enum Schedule", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue("#content > div.form-item-holder.textinput > input", enumSchedule);

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#content > div:nth-child(1) > select",
      "set default value "
    );

    click("#content > div:nth-child(1) > select");

    click("#content > div:nth-child(1) > select > option");

    click("#content > div:nth-child(2) > select");

    click("#content > div:nth-child(2) > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(600);

    click("#controlBar > button:nth-child(2)");

    elementPresent("#controlBar > button", "enum Schedule created succesfully");

    click("#controlBar > button");

    waitService.waitForMilliseconds(10000);
  });
});
