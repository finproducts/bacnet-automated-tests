import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let booleanScheduleTrue = "boolScheduleTrue" + new Date().valueOf();

let booleanScheduleFalse = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleNull = "boolScheduleNull" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let numberSchedule = "numSchedule" + new Date().valueOf();

let stringSchedule = "strSchedule" + new Date().valueOf();

let enumSchedule = "enumSchedule" + new Date().valueOf();

describe("SchedulesOptionsRange-TimedMainView.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("New Boolean Schedule-DefaultFalse", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalse
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#content > div > select > option:nth-child(4)");

    click("#content > div > select > option:nth-child(5)");

    click("#content > div > select > option:nth-child(6)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With True Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("View All schedules-Click on the created schedule", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)",
      "All schedules"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Edit schedule-Pencil", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(5)",
      "schedule is open"
    );

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");

    waitService.waitForMilliseconds(500);

    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(5)"
    );

    elementPresent("#content > div:nth-child(1) > input", "Edit Schedule form");

    click("#content > div:nth-child(1) > input");

    clearValue("#content > div:nth-child(1) > input");

    setValue("#content > div:nth-child(1) > input", "editedBooleanSChedule");

    waitService.waitForMilliseconds(500);

    elementPresent("#content > div:nth-child(4) > input", "Tag Editor");

    click("#content > div:nth-child(4) > input");

    setValue(
      "#content > div:nth-child(4) > input",
      "newTag, newOtherTag, tagThree"
    );

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add interval", async () => {
    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal.new-weekly > li:nth-child(1) > div > div.weekly-event-view",
      "Add interval window is open"
    );

    click(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal.new-weekly > li:nth-child(1) > div > div.weekly-event-view"
    );

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal > li:nth-child(1) > div > div.handle.top > button",
      1,
      1
    );

    mouseButtonDown(0);

    moveToElement(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal > li:nth-child(1) > div > div.weekly-event-view",
      2,
      300
    );

    mouseButtonUp();

    waitService.waitForMilliseconds(500);

    click("#scheduleView > div > div > div:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side",
      "success prompt"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("events -new event-range", async () => {
    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > ul > li:nth-child(2)"
    );

    elementPresent(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > ul > li.padded.normal.all-sides.selected",
      "EVENTS"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(2) > input",
      "events window"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(2) > input"
    );

    setValue(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(2) > input",
      "trueEventRange"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li.padded.normal.all-sides.flex.text-center"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li:nth-child(2)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li:nth-child(3)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li.padded.normal.all-sides.flex.text-center"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(6) > ul > li.padded.normal.all-sides.flex.text-center.selected"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.horizontal.middle.center.padded.all-sides.normal > div > label"
    );

    containsText(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(1) > div > span.tall-text.theme-color.margin.small.r-side",
      "Every",
      "Event will occur every year"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(1) > div"
    );

    waitService.waitForMilliseconds(300);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > ul > ul:nth-child(4) > li:nth-child(4)",
      "start date form"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-left.date-action-btn.margin.small.r-side"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > ul > ul:nth-child(4) > li:nth-child(4)"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > ul > ul:nth-child(4) > li.date-item.horizontal.middle.center.flex.non-auto.mono.selected",
      "Nov 22"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(2) > div"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-up.date-action-btn.margin.small.l-side",
      "end date format"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-up.date-action-btn.margin.small.l-side"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span:nth-child(6)",
      "2018",
      "2018"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-right.date-action-btn.margin.small.l-side"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-right.date-action-btn.margin.small.l-side"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-right.date-action-btn.margin.small.l-side"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-right.date-action-btn.margin.small.l-side"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span.icon-chevron-right.date-action-btn.margin.small.l-side"
    );

    waitService.waitForMilliseconds(100);

    waitService.waitForMilliseconds(100);

    containsText(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div > span:nth-child(2)",
      "May",
      "May"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > ul > ul:nth-child(3) > li:nth-child(2)"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > ul > ul:nth-child(3) > li.date-item.horizontal.middle.center.flex.non-auto.mono.selected",
      "13"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    containsText(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(1) > div > span:nth-child(2)",
      "Nov 22",
      "Start date is November 22n2 2018"
    );

    containsText(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(2) > div > span:nth-child(2)",
      "May 13",
      "End date is May 13 2018"
    );

    waitService.waitForMilliseconds(5000);

    click(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > div > input[type="number"]:nth-child(1)'
    );

    clearValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > div > input[type="number"]:nth-child(1)'
    );

    setValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > div > input[type="number"]:nth-child(1)',
      "9"
    );

    click(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > div > input[type="number"]:nth-child(3)'
    );

    clearValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > div > input[type="number"]:nth-child(3)'
    );

    setValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > div > input[type="number"]:nth-child(3)',
      "41"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > ul > li:nth-child(2)"
    );

    elementPresent(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(1) > div > ul > li.padded.small.all-sides.selected",
      "9:41 PM"
    );

    click(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > div > input[type="number"]:nth-child(1)'
    );

    clearValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > div > input[type="number"]:nth-child(1)'
    );

    setValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > div > input[type="number"]:nth-child(1)',
      "11"
    );

    click(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > div > input[type="number"]:nth-child(3)'
    );

    clearValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > div > input[type="number"]:nth-child(3)'
    );

    setValue(
      '#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > div > input[type="number"]:nth-child(3)',
      "06"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > ul > li:nth-child(1)"
    );

    elementPresent(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div.vertical.padded.small.v-sides > div:nth-child(2) > div > ul > li.padded.small.all-sides.selected",
      "11:06 AM"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);
  });
});
