import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

describe("LogicBuilder-test_variablesSectionbLine.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Change Language bLine", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(6)"
    );

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");

    waitService.waitForMilliseconds(500);
  });
  it("Check Alarm Routine", async () => {
    click(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(".__start__.block");
  });
  it("Add Variable in bLine", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent("#panel > section.variables > div.scroller > div > span");

    elementPresent(
      "#panel > section.variables > div.scroller > div.variable > b.boolean.get.block"
    );

    elementPresent(
      "#panel > section.variables > div.scroller > div.variable > b.boolean.set.block"
    );
  });
  it("Add Function in bLine", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#panel > section.variables > div.scroller > div.function > span > span"
    );
  });
  it("Add MacroBlock in bLine", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);
  });
  it("Edit Macro Block in bLine", async () => {
    moveToElement(".variables .scroller .macro span", 1, 1);

    mouseButtonClick(0);

    click(".variables .scroller .macro span");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller .macro", "viewing");

    elementPresent(".__start__.block");

    elementPresent(".bLine.banner.macro");

    click(".bLine.banner.macro i");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".bLine.banner.macro i", "active");

    visible(".slide-panel.open");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(1)");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(2)");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(3)");

    elementPresent(
      ".bLine.banner.macro > div > div > div.tac.mtm > button:nth-child(1)"
    );

    elementPresent(
      ".bLine.banner.macro > div > div > div.tac.mtm > button:nth-child(4)"
    );

    elementPresent(
      ".bLine.banner.macro > div > div > div.tac.mtm > button:nth-child(7)"
    );

    click(".bLine.banner.macro .tac.mtm button");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(4)");

    cssClassPresent(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b",
      "get"
    );

    clearValue(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(2) input"
    );

    setValue(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(2) input",
      "testMacro"
    );

    waitService.waitForMilliseconds(500);

    value(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(2) input",
      "testMacro"
    );

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(3) > div"
    );

    waitService.waitForMilliseconds(500);

    visible(".dropdown.open");

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(3) > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(3) div",
      "output"
    );

    cssClassPresent(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b",
      "set"
    );

    click(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(4) div"
    );

    waitService.waitForMilliseconds(500);

    visible(".dropdown.open");

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(4) > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(4) div",
      "number"
    );

    cssClassPresent(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b",
      "number"
    );

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(6) > i"
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".bLine.banner.macro table tbody tr:nth-child(4)");

    moveToElement(
      ".bLine.banner.macro table > tbody > tr:nth-child(1) > td:nth-child(1) > b",
      10,
      5
    );

    mouseButtonDown(0);

    waitService.waitForMilliseconds(200);

    moveToElement("#stage > div.scroller > div", 300, 300);

    waitService.waitForMilliseconds(200);

    mouseButtonUp(0);

    waitService.waitForMilliseconds(200);

    click(".bLine.banner.macro i");

    elementNotPresent(".slide-panel.open");
  });
});
