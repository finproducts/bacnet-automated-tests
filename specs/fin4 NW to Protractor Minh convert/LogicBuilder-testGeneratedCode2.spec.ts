import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let fs= require('fs');

let bLineCode= fs.readFileSync('./tests/fixtures/programs/bLineCode.txt').toString();

let programName= "test"  +  new Date().valueOf();

let selectorLibrary= require('./po.js');

let logicBuilderButton= po.logicBuilderButton;



describe('LogicBuilder-testGeneratedCode2.js',() =>{
  
  it('go to City Center VAV-1', async () => 
    
    {
waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)');

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('body > app > section > header > button.link-button.pna.launcher-open-btn');

waitService.waitForMilliseconds(500);

click(logicBuilderButton);

waitService.waitForMilliseconds(500);

click(logicBuilderButton  +  "> ul > li:nth-child(2)");

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys("test"  +  new Date().valueOf());

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a');

waitService.waitForMilliseconds(1000);

frame(0);

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Add variables and drag them to stage', async () => 
    
    {
execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['#panel > section.variables > div > div.number.variable > b.number.set.block', '.scroller .blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(2)','The numeric n1 setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',443);

app.set('blocks.1.y',228);
}
,[],function(result)
{}
);

execute(dragAndDrop,['#panel > section.variables > div.scroller > div:nth-child(2) > b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(3)','The numeric n2 setter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',670);

app.set('blocks.2.y',293);
}
,[],function(result)
{}
);

waitService.waitForMilliseconds(1000);

execute(dragAndDrop,['#panel > section.variables > div.scroller > div:nth-child(3) > b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(4)','The numeric n3 setter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',856);

app.set('blocks.3.y',410);
}
,[],function(result)
{}
);
}

    
  );  
  it('Link blocks', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(2) div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div > div > div > input[type="number"]','123');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(2) div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(3) div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div:nth-child(3) > div.data.sockets > div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div:nth-child(3) > div.data.sockets > div.input > div > div > div > input[type="number"]','999');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(3) div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(4) div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div:nth-child(4) > div.data.sockets >  div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div:nth-child(4) > div.data.sockets > div.input > div > div > div > input[type="number"]','8769678');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('verify generated code', async () => 
    
    {
waitService.waitForMilliseconds(1000);

execute(function(simple)
{
return app.generateCode();
}
,[],function(result)
{
console.log(result.value);

waitService.waitForMilliseconds(5000);

expect(result.value);

to;

equal(bLineCode);
}
);
}

    
  );  
  
  
});