import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("LogicBuilder-kmcTestProgram.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add year block", async () => {
    click(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "year"
    );

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div.category > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.1.x", 100);

      app.set("blocks.1.y", 300);
    }, []);

    waitService.waitForMilliseconds(1000);
  });
  it("Add and edit add block", async () => {
    click(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    clearValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "add"
    );

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div.category > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.2.x", 400);

      app.set("blocks.2.y", 300);
    }, []);

    waitService.waitForMilliseconds(1000);

    click(
      '#stage > div > div > div.add.block > div > div.input > div:nth-child(2) > div > div > input[type="number"]'
    );

    setValue(
      '#stage > div > div > div.add.block > div > div.input > div:nth-child(2) > div > div > input[type="number"]',
      "10"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Add Variable", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(2) > i.variable.symbol"
    );

    elementPresent("#panel > section.variables > div.scroller > div");

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div > b.variable.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.3.x", 600);

      app.set("blocks.3.y", 250);
    }, []);

    waitService.waitForMilliseconds(1000);
  });
  it("Link year to add", async () => {
    click("#stage > div > div > div.year.block > div > div.output > div > b");

    waitService.waitForMilliseconds(1000);

    click(
      "#stage > div > div > div.add.block > div > div.input > div:nth-child(1) > b"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Link add to set variable", async () => {
    click("#stage > div > div > div.add.block > div > div.output > div > b");

    waitService.waitForMilliseconds(1000);

    click(
      "#stage > div > div > div.set.variable.block > div.data.sockets > div.input > div > b"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Link set variable to start", async () => {
    click(
      "#stage > div > div > div.set.variable.block > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(1000);

    click("#stage > div > div > div.__start__.block > div > div > div > b");

    waitService.waitForMilliseconds(300);
  });
  it("Save & Export Program", async () => {
    click("#panel > section.controls > div.icons > i.save.icon");

    click("#panel > section.controls > div.icons > i.export.icon");

    click("body > app > div.export.modal > div > div.close > i");

    waitService.waitForMilliseconds(1000);
  });
});
