import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let selectorLibrary= require('./po.js');

let logicBuilderButton= po.logicBuilderButton;

let programName= "test"  +  new Date().valueOf();



describe('LogicBuilder-MainAlarmMainSwitch.js',() =>{
  
  it('go to City Center VAV-1', async () => 
    
    {
waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)');

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('body > app > section > header > button.link-button.pna.launcher-open-btn');

waitService.waitForMilliseconds(500);

click(logicBuilderButton);

waitService.waitForMilliseconds(500);

click(logicBuilderButton  +  "> ul > li:nth-child(2)");

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
setValue('#content > div.form-item-holder.textinput > input','testTag');

waitService.waitForMilliseconds(500);

keys(programName);

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a');

waitService.waitForMilliseconds(1000);

frame(0);

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Setting the alarm', async () => 
    
    {
click('#stage > div.tab.mobile > span:nth-child(3)');

waitService.waitForMilliseconds(500);

setValue('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]','alarm');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add a macroblock and drag it to stage in bLine', async () => 
    
    {
click('.add.panel-button .macro');

waitService.waitForMilliseconds(500);

moveToElement('.variables .scroller .macro > span > span',1,1);

waitService.waitForMilliseconds(500);

click('.variables .scroller .macro > i.pencil.icon');

waitService.waitForMilliseconds(500);

keys([driver.Keys.CONTROL,'a', driver.Keys.CONTROL, driver.Keys.DELETE]);

waitService.waitForMilliseconds(500);

keys('min-max');

waitService.waitForMilliseconds(500);

keys(driver,Keys,ENTER);

containsText('.variables .scroller .macro > span > span','min-max');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller .macro  b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .macro.commandWithDataOutputs.block','The macro block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',406);

app.set('blocks.1.y',268);
}
,[],function(result)
{}
);
}

    
  );  
  it('Open macroblock by clicking the icon on the block', async () => 
    
    {
click('.blocks .macro.commandWithDataOutputs.block > header > i');

waitService.waitForMilliseconds(500);

cssClassPresent('.variables .scroller .macro','viewing','Macroblock is open for editing');

elementPresent('.bLine.banner.macro','Macroblock banner is present inside');

click('i.cog.icon');

waitService.waitForMilliseconds(500);

cssClassPresent('.bLine.banner.macro > div','open','macroblock settings window is open after clicking the settings icon');
}

    
  );  
  it('Edit ports for macroblock', async () => 
    
    {
click('#stage > div.bLine.banner.macro > div > div > div.tac.mtm > button');

waitService.waitForMilliseconds(500);

elementPresent('.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4)','A fourth port is added');

click('.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label','output','Fourth port is now set to 'output'');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label','number','Data type changed to number for first port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label','number','Data type changed to number for second port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','number','Data type changed to number for third port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > label','number','Data type changed to number for fourth port');

clearValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]','min');

waitService.waitForMilliseconds(500);

value('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]','min','The name for the third port is 'min'');

clearValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]','max');

waitService.waitForMilliseconds(500);

value('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]','max','The name for the fourth port is 'max'');

click('#panel > section.variables > div.scroller > div.macro.viewing > span > span');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(3)');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(3)');

waitService.waitForMilliseconds(5000);
}

    
  );  
  
  
});