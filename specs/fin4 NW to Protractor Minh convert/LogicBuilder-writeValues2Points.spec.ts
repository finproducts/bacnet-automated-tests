import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-writeValues2Points.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1) > a > div.vertical.flex.non-auto > h4"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1) > a"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(10)"
    );

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);
  });
});
