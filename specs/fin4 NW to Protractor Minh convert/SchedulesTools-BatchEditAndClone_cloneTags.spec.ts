import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let booleanScheduleTrue = "boolScheduleTrue" + new Date().valueOf();

let booleanScheduleFalse = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleNull = "boolScheduleNull" + new Date().valueOf();

let numberSchedule = "numSchedule" + new Date().valueOf();

let stringSchedule = "strSchedule" + new Date().valueOf();

let enumSchedule = "enumSchedule" + new Date().valueOf();

describe("SchedulesTools-BatchEditAndClone_cloneTags.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)",
      "schedules menu"
    );
  });
  it("New Numeric Schedule-DefaultValue", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      numberSchedule
    );

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > input", "set default value ");

    click("#content > div > input");

    setValue("#content > div > input", "69.69");

    waitService.waitForMilliseconds(600);

    value("#content > div > input", "69.69");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(600);

    click("#content > div > select > option:nth-child(1)");

    click("#content > div > select > option:nth-child(2)");

    click("#content > div > select > option:nth-child(3)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Numeric Schedule created succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("View All schedules-Click on the created schedule", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)",
      "All schedules"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Tools Form", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12) > ul > li:nth-child(2)"
    );

    waitForElementPresent("#title", 2000, "Tools form launched");

    waitService.waitForMilliseconds(1000);
  });
  it("Assert tools", async () => {
    elementPresent(
      "#content > div > div > div > ul > li:nth-child(1) > div",
      "Add points to the schedule"
    );

    elementPresent(
      "#content > div > div > div > ul > li:nth-child(2) > div",
      "Batch clone schedule"
    );

    elementPresent(
      "#content > div > div > div > ul > li:nth-child(3) > div",
      "Batch edit and clone"
    );

    elementPresent(
      "#content > div > div > div > ul > li:nth-child(4) > div",
      "Clone events"
    );

    elementPresent(
      "#content > div > div > div > ul > li:nth-child(5) > div",
      "Delete events"
    );

    elementPresent(
      "#content > div > div > div > ul > li:nth-child(6) > div",
      "Duplicate schdedule day"
    );

    elementPresent(
      "#content > div > div > div > ul > li:nth-child(7) > div",
      "Excel Row Paster"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Batch edit and clone", async () => {
    click(
      "#content > div > div > div > ul > li:nth-child(3) > div > button > span"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#content > div > div > div > ul > li.no-flex.is-open.j2-tree > ul > li > div"
    );

    elementPresent(
      "#content > div > div > div > ul > li.no-flex.is-open.j2-tree > ul > li > div",
      "batch edit and clone is selected"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Clone selected tags from - VAV Rmt Control - to below filter", async () => {
    click(
      "#content > div.form-item-holder.list > select > option:nth-child(1)"
    );
  });
  it("Filter edit", async () => {
    click("#content > div.form-item-holder.filter > div > button");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div:nth-child(4) > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div:nth-child(4) > div > section > div > div.vertical.padded.normal.all-sides > div > select"
    );

    waitService.waitForMilliseconds(200);

    click(
      "body > app > div:nth-child(4) > div > section > div > div.vertical.padded.normal.all-sides > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div:nth-child(4) > div > section > div > div.vertical.flex.tree-container > div > ul > li:nth-child(5) > div"
    );

    waitService.waitForMilliseconds(500);

    click("body > app > div:nth-child(4) > div > footer > button:nth-child(3)");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div:nth-child(4) > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div:nth-child(4) > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(600);
  });
});
