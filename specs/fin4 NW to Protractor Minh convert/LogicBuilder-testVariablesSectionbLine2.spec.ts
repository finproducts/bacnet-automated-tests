import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-testVariablesSectionbLine2.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);

    elementPresent("#stage > div.tab.mobile > span:nth-child(2)");

    elementPresent("#stage > div.tab.mobile > span:nth-child(3)");
  });
  it("Check Alarm Routine", async () => {
    click("#stage > div.tab.mobile > span:nth-child(3)");

    waitService.waitForMilliseconds(100);

    cssClassPresent("#stage > div.tab.mobile > span:nth-child(3)", "tablinks");

    cssClassPresent("#stage > div.tab.mobile > span:nth-child(3)", "active");

    elementPresent(".__start__.block");
  });
  it("Add Variable in bLine", async () => {
    frame(null);

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.textinput > input");

    keys("boolVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(100);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(100);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(100);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);

    frame(0);

    elementPresent("#stage > div.tab.mobile > span:nth-child(2)");

    waitService.waitForMilliseconds(5000);

    elementPresent(
      "#panel > section.variables > div.scroller > div > b.boolean.get.block"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#panel > section.variables > div.scroller > div > b.boolean.set.block"
    );
  });
  it("Add Function in bLine", async () => {
    click("#stage > div.tab.mobile > span:nth-child(1)");

    waitService.waitForMilliseconds(1000);

    elementPresent("#stage > div.tab.mobile > span:nth-child(4)");
  });
  it("Add MacroBlock in bLine", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#panel > section.variables > div.scroller > div.macro > span > span"
    );
  });
  it("Edit Macro Block in bLine", async () => {
    moveToElement(".variables .scroller .macro span", 1, 1);

    mouseButtonClick(0);

    click(".variables .scroller .macro span");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller .macro", "viewing");

    elementPresent(".__start__.block");

    elementPresent(".bLine.banner.macro");

    click(".bLine.banner.macro i");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".bLine.banner.macro i", "active");

    visible(".slide-panel.open");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(1)");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(2)");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(3)");

    elementPresent(
      ".bLine.banner.macro > div > div > div.tac.mtm > button:nth-child(1)"
    );

    elementPresent(
      ".bLine.banner.macro > div > div > div.tac.mtm > button:nth-child(4)"
    );

    elementPresent(
      ".bLine.banner.macro > div > div > div.tac.mtm > button:nth-child(7)"
    );

    click(".bLine.banner.macro .tac.mtm button");

    elementPresent(".bLine.banner.macro table tbody tr:nth-child(4)");

    cssClassPresent(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b",
      "get"
    );

    clearValue(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(2) input"
    );

    setValue(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(2) input",
      "testMacro"
    );

    waitService.waitForMilliseconds(500);

    value(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(2) input",
      "testMacro"
    );

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(3) > div"
    );

    waitService.waitForMilliseconds(500);

    visible(".dropdown.open");

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(3) > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(3) div",
      "output"
    );

    cssClassPresent(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b",
      "set"
    );

    click(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(4) div"
    );

    waitService.waitForMilliseconds(500);

    visible(".dropdown.open");

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(4) > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table tbody tr:nth-child(4) td:nth-child(4) div",
      "number"
    );

    cssClassPresent(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b",
      "number"
    );

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(6) > i"
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".bLine.banner.macro table tbody tr:nth-child(4)");

    moveToElement(
      ".bLine.banner.macro table > tbody > tr:nth-child(1) > td:nth-child(1) > b",
      10,
      5
    );

    mouseButtonDown(0);

    waitService.waitForMilliseconds(200);

    moveToElement("#stage > div.scroller > div", 300, 300);

    waitService.waitForMilliseconds(200);

    mouseButtonUp(0);

    waitService.waitForMilliseconds(200);

    click(".bLine.banner.macro i");

    elementNotPresent(".slide-panel.open");
  });
});
