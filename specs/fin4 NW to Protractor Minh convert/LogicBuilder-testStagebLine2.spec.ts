import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let selectorLibrary= require('./po.js');

let logicBuilderButton= po.logicBuilderButton;

let mainRoutine= ('#stage > div.tab.mobile > span:nth-child(1)');

let alarmRoutine= ('#stage > div.tab.mobile > span:nth-child(3)');

let programName= "test"  +  new Date().valueOf();



describe('LogicBuilder-testStagebLine2.js',() =>{
  
  it('go to City Center VAV-1', async () => 
    
    {
waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)');

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('body > app > section > header > button.link-button.pna.launcher-open-btn');

waitService.waitForMilliseconds(500);

click(logicBuilderButton);

waitService.waitForMilliseconds(500);

click(logicBuilderButton  +  "> ul > li:nth-child(2)");

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys(programName);

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)');

waitService.waitForMilliseconds(1000);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a');

waitService.waitForMilliseconds(1000);

frame(0);

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Check main and alarm routine', async () => 
    
    {
waitService.waitForMilliseconds(500);

containsText('#stage > div.tab.mobile > span.tablinks.active','main','The main routine is first');

containsText('#stage > div.tab.mobile > span:nth-child(3)','alarm','The alarm routine is second');
}

    
  );  
  it('Check the settings banner', async () => 
    
    {
elementPresent('.bLine.banner','The banner is present');

elementPresent('i.cog.icon','Banner settings icon is present');

click('i.cog.icon');

waitService.waitForMilliseconds(500);

cssClassPresent('.bLine.banner > div','open','Routine settings window opens when the settings icon is clicked');

elementPresent('.bLine.banner > div > div:nth-child(2)',''Interval' option is present');

elementPresent('.bLine.banner > div > div:nth-child(2) > div > label','Radio button to select 'Interval' option');

elementPresent('.bLine.banner > div > div:nth-child(2) > div > div','Dropdown list for 'Interval' option is present');

click('.bLine.banner > div > div:nth-child(2) > div > div > label');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-select-dropdown-container > ul','open','The drodown list is open when clicked');

containsText('#ractive-select-dropdown-container > ul > li:nth-child(1)','slow','First option is 'slow'');

containsText('#ractive-select-dropdown-container > ul > li:nth-child(2)','normal','Second option is 'normal'');

containsText('#ractive-select-dropdown-container > ul > li:nth-child(3)','fast','Third option is 'fast'');

click('#ractive-select-dropdown-container > ul > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('.bLine.banner > div > div:nth-child(2) > div > div > label','normal','Interval changed to normal when clicked');

click('.bLine.banner > div > div:nth-child(2) > div > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul > li:nth-child(3)');

waitService.waitForMilliseconds(500);

containsText('.bLine.banner > div > div:nth-child(2) > div > div > label','fast','Interval changed to fast when clicked');

click('.bLine.banner > div > div:nth-child(2) > div > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

containsText('.bLine.banner > div > div:nth-child(2) > div > div > label','slow','Interval changed back to slow when clicked');

elementPresent('.bLine.banner > div > div:nth-child(3)',''On event' option is present');

elementPresent('.bLine.banner > div > div:nth-child(3) > label','Radio button to select 'On event' option');

click('.bLine.banner > div > div:nth-child(3) > label');

waitService.waitForMilliseconds(500);

elementPresent('.bLine.banner.on.event','The banner changed to 'On event' when the option was clicked');

elementPresent('.bLine.banner > div > div:nth-child(4)',''Manual' option is present');

elementPresent('.bLine.banner > div > div:nth-child(4) > label','Radio button to select 'Manual' option');

click('.bLine.banner > div > div:nth-child(4) > label');

waitService.waitForMilliseconds(500);

elementPresent('.bLine.banner.manual','The banner changed to 'Manual' when the option was clicked');

click('.bLine.banner > div > div:nth-child(2) > div > label');

waitService.waitForMilliseconds(500);

elementPresent('.bLine.banner.interval','The banner changed to 'Interval' when the option was clicked');

click('i.cog.icon');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.bLine.banner > div','open','Routine settings window is closed when the icon is clicked');

click(alarmRoutine);

waitService.waitForMilliseconds(500);

click(mainRoutine);

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add variables, routines and macro blocks', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(100);

click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full');

waitService.waitForMilliseconds(100);

click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)');

waitService.waitForMilliseconds(100);

click('#content > div.form-item-holder.textinput > input');

keys('boolVar');

waitService.waitForMilliseconds(100);

click('#content > div:nth-child(2) > div > input');

waitService.waitForMilliseconds(100);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(100);

click('#content > div.form-item-holder.combobox > select > option:nth-child(2)');

waitService.waitForMilliseconds(100);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(100);

click('#content > div.form-item-holder.combobox > select > option:nth-child(1)');

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(400);

click('#controlBar > button');

waitService.waitForMilliseconds(2000);

frame(0);

waitService.waitForMilliseconds(1000);

elementPresent('#panel > section.variables > div.scroller > div > span');

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

frame(0);

click('#stage > div.tab.mobile > span:nth-child(1)');

waitService.waitForMilliseconds(500);

click('.add.panel-button .macro');

waitService.waitForMilliseconds(500);

elementPresent('.variables .scroller .macro','A macroblock is added');
}

    
  );  
  it('Drag the variables and macroblocks to the stage', async () => 
    
    {
execute(function(simple)
{
app.set('blocks.0.x',40);

app.set('blocks.0.y',26);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .boolean.variable  b.boolean.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.boolean.block','The boolean setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',200);

app.set('blocks.1.y',70);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .boolean.variable  b.boolean.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.boolean.block','The boolean getter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',35);

app.set('blocks.2.y',136);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .number.variable  b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.3.x',384);

app.set('blocks.3.y',90);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .number.variable  b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.4.x',222);

app.set('blocks.4.y',187);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .string.variable  b.string.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.5.x',705);

app.set('blocks.5.y',174);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .string.variable  b.string.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.6.x',571);

app.set('blocks.6.y',285);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .date.variable  b.date.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.7.x',901);

app.set('blocks.7.y',228);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .date.variable  b.date.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.8.x',754);

app.set('blocks.8.y',310);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .time.variable  b.time.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.9.x',1100);

app.set('blocks.9.y',304);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .time.variable  b.time.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.10.x',958);

app.set('blocks.10.y',369);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .datetime.variable  b.datetime.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.11.x',1306);

app.set('blocks.11.y',328);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .datetime.variable  b.datetime.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.12.x',1190);

app.set('blocks.12.y',418);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .macro b', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.13.x',1522);

app.set('blocks.13.y',385);
}
,[],function(result)
{}
);

setValue('.blocklibrary .filters > div > input[type="text"]','if');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary .scroller > div > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.14.x',540);

app.set('blocks.14.y',104);
}
,[],function(result)
{}
);

clearValue('.blocklibrary .filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

click('.blocks');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Check blocks format', async () => 
    
    {
elementPresent('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b','Boolean setter block has exec input port');

elementPresent('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b','Boolean setter block has exec output port');

elementPresent('.blocks .set.boolean.block > div.data.sockets > div > div > b','Boolean setter block has data input port');

elementPresent('#stage > div.scroller > div > div.set.boolean.block > div.data.sockets > div.input > div > div > label','Boolean setter block has checkbox');

elementPresent('.blocks .get.boolean.block > div > div.output > div > b','Boolean getter block has data output port');

containsText('.blocks .set.boolean.block > header','set boolVar','Boolean setter block has the correct name');

containsText('.blocks .get.boolean.block > div > div.output > div','boolVar','Boolean getter block has the correct name');

cssProperty('.blocks .set.boolean.block','background-color','rgba(39, 174, 96, 1)','Boolean setter block has the correct color');

cssProperty('.blocks .get.boolean.block','background-color','rgba(39, 174, 96, 1)','Boolean getter block has the correct color');

elementPresent('.blocks .set.number.block > div.exec.sockets > div.in > div > b','Number setter block has exec input port');

elementPresent('.blocks .set.number.block > div.exec.sockets > div.out > div > b','Number setter block has exec output port');

elementPresent('.blocks .set.number.block > div.data.sockets > div > div > b','Number setter block has data input port');

elementPresent('#stage > div.scroller > div > div.set.number.block > div.data.sockets > div.input > div > div > div','Number setter block has numeric input');

elementPresent('.blocks .set.number.block > div.data.sockets > div > div > div > div','Number setter block has stepper for the input');

elementPresent('.blocks .get.number.block > div > div.output > div > b','Number getter block has data output port');

containsText('.blocks .set.number.block > header','set number','Number setter block has the correct name');

containsText('.blocks .get.number.block > div > div.output > div','number','Number getter block has the correct name');

cssProperty('.blocks .set.number.block','background-color','rgba(142, 68, 173, 1)','Number setter block has the correct color');

cssProperty('.blocks .get.number.block','background-color','rgba(142, 68, 173, 1)','Number getter block has the correct color');

elementPresent('.blocks .set.string.block > div.exec.sockets > div.in > div > b','String setter block has exec input port');

elementPresent('.blocks .set.string.block > div.exec.sockets > div.out > div > b','String setter block has exec output port');

elementPresent('.blocks .set.string.block > div.data.sockets > div > div > b','String setter block has data input port');

elementPresent('#stage > div.scroller > div > div.set.string.block > div.data.sockets > div.input > div > div','String setter block has text input');

elementPresent('.blocks .get.string.block > div > div.output > div > b','String getter block has data output port');

containsText('.blocks .set.string.block > header','set string','String setter block has the correct name');

containsText('.blocks .get.string.block > div > div.output > div','string','String getter block has the correct name');

cssProperty('.blocks .set.string.block','background-color','rgba(243, 156, 18, 1)','String setter block has the correct color');

cssProperty('.blocks .get.string.block','background-color','rgba(243, 156, 18, 1)','String getter block has the correct color');

elementPresent('.blocks .set.date.block > div.exec.sockets > div.in > div > b','Date setter block has exec input port');

elementPresent('.blocks .set.date.block > div.exec.sockets > div.out > div > b','Date setter block has exec output port');

elementPresent('.blocks .set.date.block > div.data.sockets > div > div > b','Date setter block has data input port');

elementPresent('#stage > div.scroller > div > div.set.date.block > div.data.sockets > div.input > div > div > div > i','Date setter block has timepicker');

elementPresent('.blocks .get.date.block > div > div.output > div > b','Date getter block has data output port');

containsText('#stage > div.scroller > div > div.set.date.block > header','set date','Date setter block has the correct name');

containsText('.blocks .get.date.block > div > div.output > div','date','Date getter block has the correct name');

cssProperty('.blocks .set.date.block','background-color','rgba(26, 188, 156, 1)','Date setter block has the correct color');

cssProperty('.blocks .get.date.block','background-color','rgba(26, 188, 156, 1)','Date getter block has the correct color');

elementPresent('.blocks .set.time.block > div.exec.sockets > div.in > div > b','Time setter block has exec input port');

elementPresent('.blocks .set.time.block > div.exec.sockets > div.out > div > b','Time setter block has exec output port');

elementPresent('.blocks .set.time.block > div.data.sockets > div > div > b','Time setter block has data input port');

elementPresent('#stage > div.scroller > div > div.set.time.block > div.data.sockets > div.input > div > div > div > i','Time setter block has timepicker');

elementPresent('.blocks .get.time.block > div > div.output > div > b','Time getter block has data output port');

containsText('.blocks .set.time.block > header','set time','Time setter block has the correct name');

containsText('.blocks .get.time.block > div > div.output > div','time','Time getter block has the correct name');

cssProperty('.blocks .set.time.block','background-color','rgba(26, 188, 156, 1)','Time setter block has the correct color');

cssProperty('.blocks .get.time.block','background-color','rgba(26, 188, 156, 1)','Time getter block has the correct color');

elementPresent('.blocks .set.datetime.block > div.exec.sockets > div.in > div > b','DateTime setter block has exec input port');

elementPresent('.blocks .set.datetime.block > div.exec.sockets > div.out > div > b','DateTime setter block has exec output port');

elementPresent('.blocks .set.datetime.block > div.data.sockets > div > div > b','DateTime setter block has data input port');

elementPresent('#stage > div.scroller > div > div.set.datetime.block > div.data.sockets > div.input > div > div > div > i','DateTime setter block has timepicker');

elementPresent('.blocks .get.datetime.block > div > div.output > div > b','DateTime getter block has data output port');

containsText('.blocks .set.datetime.block > header','set dateTime','Datetime setter block has the correct name');

containsText('.blocks .get.datetime.block > div > div.output > div','dateTime','Datetime getter block has the correct name');

cssProperty('.blocks .set.datetime.block','background-color','rgba(26, 188, 156, 1)','Datetime setter block has the correct color');

cssProperty('.blocks .get.datetime.block','background-color','rgba(26, 188, 156, 1)','Datetime getter block has the correct color');

elementPresent('.macro.commandWithDataOutputs.block > div.exec.sockets > div.in > div > b','Macroblock has exec input port');

elementPresent('.macro.commandWithDataOutputs.block > div.exec.sockets > div.out > div > b','Macroblock has exec output port');

elementPresent('.macro.commandWithDataOutputs.block > div.data.sockets > div.input > div:nth-child(1) > b','Macroblock has one data input port');

elementPresent('#stage > div.scroller > div > div.macro.commandWithDataOutputs.block > div.data.sockets > div.input > div:nth-child(1) > div > input[type="text"]','Macroblock has one text input');

elementPresent('.macro.commandWithDataOutputs.block > div.data.sockets > div.input > div:nth-child(2) > b','Macroblock has data a second input port');

elementPresent('#stage > div.scroller > div > div.macro.commandWithDataOutputs.block > div.data.sockets > div.input > div:nth-child(2) > div > input[type="text"]','Macroblock has a second text input');

elementPresent('.macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div.macro-output-dropdown','Macroblock has dropdown output');

containsText('.blocks .macro.commandWithDataOutputs.block > header','unnamedMacro1','Macroblock has the correct name');

cssProperty('.blocks .macro.commandWithDataOutputs.block','background-color','rgba(52, 73, 94, 1)','Macroblock has the correct color');

elementPresent('.if.block > div.exec.sockets > div.in > div > b','If block has exec input port');

elementPresent('.if.block > div.exec.sockets > div.out > div:nth-child(1) > b','If block has exec then output port');

elementPresent('.if.block > div.exec.sockets > div.out > div:nth-child(2) > b','If block has exec else output port');

elementPresent('.if.block > div.exec.sockets > div.out > div:nth-child(3) > b','If block has exec next output port');

elementPresent('.if.block > div.data.sockets > div > div > b','If block has data input port');

elementPresent('#stage > div.scroller > div > div.if.block > div.data.sockets > div.input > div > div > label','If block has checkbox');

containsText('.blocks .if.block > header','if','IF block has the correct name');

cssProperty('.blocks .if.block','background-color','rgba(93, 93, 93, 1)','IF block has the correct color');
}

    
  );  
  it('Link the blocks in the stage', async () => 
    
    {
click('.__start__ > div > div > div > b');

waitService.waitForMilliseconds(300);

click('#stage > div.scroller > div > div.set.boolean.block > div.data.sockets > div.input > div > b');

waitService.waitForMilliseconds(1000);

waitService.waitForMilliseconds(1000);

click('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.date.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.date.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.time.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.time.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.datetime.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.datetime.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.boolean.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.string.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.date.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.date.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.time.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.time.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.datetime.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.datetime.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(2000);

elementNotPresent('.blocks .set.number.block > div.data.sockets > div > div > div > input[type="number"','Number setter numeric input is not present after linking with a getter');

elementNotPresent('.blocks .set.string.block > div.data.sockets > div > div > input','String setter text input is not present after linking with a getter');

elementNotPresent('.blocks .set.date.block > div.data.sockets > div > div > div.datepicker','Date setter timepicker is not present after linking with a getter');

elementNotPresent('.blocks .set.time.block > div.data.sockets > div > div > div.datepicker','Time setter timepicker is not present after linking with a getter');

elementNotPresent('.blocks .set.datetime.block > div.data.sockets > div > div > div.datepicker','DateTime setter timepicker is not present after linking with a getter');
}

    
  );  
  it('Delete blocks', async () => 
    
    {
click('.__start__');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementPresent('.__start__','routine start block is still present after trying to delete it');

click('.blocks .if.block');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementNotPresent('.blocks .if.block','If block is deleted');

elementNotPresent('#stage > div.scroller > svg > path:nth-child(13)','Link to IF block is deleted');

click('.blocks .get.string.block');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementNotPresent('.blocks .get.string.block','String getter block is deleted');

elementNotPresent('#stage > div.scroller > svg > path:nth-child(12)','Link to String getter block is deleted');

elementPresent('#stage > div.scroller > div > div.set.string.block > div.data.sockets > div.input > div > div > input[type="text"]','String setter text input is present after deleting linked getter');

click('#stage > div.scroller > div > div.set.time.block');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementNotPresent('#stage > div.scroller > div > div.set.time.block','Time setter block is deleted');

elementNotPresent('#stage > div.scroller > svg > path:nth-child(10)','Link to Time setter block is deleted');

click('.macro.commandWithDataOutputs.block');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementNotPresent('.macro.commandWithDataOutputs.block','Macroblock is deleted');

elementNotPresent('#stage > div.scroller > svg > path.exec.datetime.variable','Link to Macroblock is deleted');
}

    
  );  
  it('Delete links', async () => 
    
    {
moveToElement('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b',1,1);

waitService.waitForMilliseconds(300);

mouseButtonClick(2);

waitService.waitForMilliseconds(1000);

click('body > app > ul');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.blocks .set.boolean.block > div.exec.sockets > div.out > div','linked');

cssClassNotPresent('.blocks .set.number.block > div.exec.sockets > div.in > div','linked');

elementNotPresent('#stage > div.scroller > svg > path.exec.boolean.variable');

moveToElement('.blocks .get.date.block > div > div.output > div > b',1,1);

waitService.waitForMilliseconds(300);

mouseButtonClick(2);

waitService.waitForMilliseconds(1000);

click('body > app > ul');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.blocks .set.date.block > div.data.sockets > div > div','linked');

cssClassNotPresent('.blocks .get.date.block > div > div.output > div','linked');

elementNotPresent('#stage > div.scroller > svg > path.data.date');

elementPresent('#stage > div.scroller > div > div.set.date.block > div.data.sockets > div.input > div > div > div > i','Date setter timepicker is present after deleting link with a getter');
}

    
  );  
  it('Check time picker for date time blocks', async () => 
    
    {
click('.blocks .set.date.block > div.data.sockets > div > div > div');

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div','Time picker is open');

click('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.header > div.year');

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.header > div.year.active','Years option is selected');

elementPresent('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.editor > div','The view switched from calendar to years');

click('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.header > div.date');

waitService.waitForMilliseconds(500);

elementNotPresent('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.header > div.year.active','Years option is no longer selected after clicking on the day');

elementNotPresent('.blocks .set.date.block  > div.data.sockets > div > div > div > div > div > div.editor > div.years','Years view is no longer on screen');

cssClassPresent('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.header > div.date.active','active','Date option is selected');

elementPresent('#stage > div.scroller > div > div.set.dropdownOpen.date.block > div.data.sockets > div.input > div > div > div > div > div > div.editor','The calendar view is on screen instead of Years');
}

    
  );  
  it('Try continuous linking', async () => 
    
    {
cssClassNotPresent('.blocks .set.boolean.block > div.exec.sockets > div.out > div','linked');

cssClassNotPresent('.blocks .set.number.block > div.exec.sockets > div.in > div','linked');

cssClassNotPresent('.blocks .set.number.block > div.exec.sockets > div.out > div','linked');

cssClassNotPresent('.blocks .set.string.block > div.exec.sockets > div.in > div','linked');

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('.blocks .set.boolean.block > div.exec.sockets > div.out > div','linked');

cssClassPresent('.blocks .set.number.block > div.exec.sockets > div.in > div','linked');

elementPresent('#stage > div.scroller > div > div.set.number.block > div.exec.sockets > div.out > div','element present');

cssClassPresent('#stage > div.scroller > div > div.set.number.block > div.exec.sockets > div.out > div','linked');

cssClassPresent('.blocks .set.string.block > div.exec.sockets > div.in > div','linked');
}

    
  );  
  it('Switch between routines', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('#stage > div.tab.mobile > span:nth-child(3)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.tab.mobile > span:nth-child(3)','alarm','The routine changed to alarm');

click('#stage > div.tab.mobile > span:nth-child(4)');

waitService.waitForMilliseconds(500);

containsText('.bLine.banner.interval > span','unnamedRoutine1 routine — interval (normal)','The routine changed to the second main routine');

click('#stage > div.tab.mobile > span:nth-child(3)');

waitService.waitForMilliseconds(500);

click('.variables .scroller > div:nth-child(3) > i');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Select blocks', async () => 
    
    {
click('.blocks');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block');

waitService.waitForMilliseconds(300);

cssClassPresent('.blocks .set.boolean.block','selected','Boolean setter is selected when clicked');

elementPresent('.blocks .set.boolean.block > div.top-icons.no-select > div > i','Comments icon is present for boolean setter');

keys(driver,Keys,SHIFT);

waitService.waitForMilliseconds(300);

click('.blocks .set.number.block');

waitService.waitForMilliseconds(300);

click('.blocks .set.string.block');

waitService.waitForMilliseconds(300);

click('.blocks .set.date.block');

waitService.waitForMilliseconds(300);

cssClassPresent('.blocks .set.number.block','selected','Number setter is selected when clicked while holding CTRL key');

elementPresent('.blocks .set.number.block > div.top-icons.no-select > div > i','Comments icon is present for number setter');

cssClassPresent('.blocks .set.string.block','selected','String setter is selected when clicked while holding CTRL key');

elementPresent('.blocks .set.string.block > div.top-icons.no-select > div > i','Comments icon is present for string setter');

cssClassPresent('.blocks .set.date.block','selected','Date setter is selected when clicked while holding CTRL key');

elementPresent('.blocks .set.date.block > div.top-icons.no-select > div > i','Comments icon is present for date setter');

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(300);
}

    
  );  
  it('Move Blocks', async () => 
    
    {
cssProperty('.blocks .__start__','left','40px');

cssProperty('.blocks .__start__','top','26px');

moveToElement('.__start__',1,1);

waitService.waitForMilliseconds(500);

mouseButtonDown(0);

waitService.waitForMilliseconds(500);

moveTo(null,50,50);

waitService.waitForMilliseconds(500);

mouseButtonUp(0);

waitService.waitForMilliseconds(500);

cssProperty('.blocks .__start__','left','40px');

cssProperty('.blocks .__start__','top','26px');
}

    
  );  
  it('Set input values for numeric setter by typing', async () => 
    
    {
click('#stage > div.scroller > div > div.set.number.block');

waitService.waitForMilliseconds(300);

moveToElement('#stage > div.scroller > div > div.set.number.block > div.data.sockets > div.input > div > b',1,1);

waitService.waitForMilliseconds(300);

mouseButtonClick(2);

waitService.waitForMilliseconds(1000);

click('body > app > ul');

waitService.waitForMilliseconds(1000);

elementPresent('#stage > div.scroller > div > div.set.number.block > div.data.sockets > div.input > div > div > div > input[type='number']','Set number input is present on the page.');
}

    
  );  
  
  
});