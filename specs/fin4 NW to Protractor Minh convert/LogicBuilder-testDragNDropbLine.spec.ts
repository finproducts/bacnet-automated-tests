import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-testDragNDropbLine.js", () => {
  it("Go to Block Programming bLine", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(6)");

    waitService.waitForMilliseconds(1500);
  });
  it("Add a variable and drag it to stage in bLine", async () => {
    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      ".variables .scroller .boolean.variable  b.boolean.get.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .get.boolean.block",
      "The boolean getter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 50);

        app.set("blocks.1.y", 50);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      ".variables .scroller .boolean.variable  b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.boolean.block",
      "The boolean setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 100);

        app.set("blocks.2.y", 100);
      },
      [],
      function(result) {}
    );
  });
  it("Add a macroblock and drag it to stage in bLine", async () => {
    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [".variables .scroller .macro  b", ".blocks"]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block",
      "The macro block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.3.x", 200);

        app.set("blocks.3.y", 200);
      },
      [],
      function(result) {}
    );
  });
});
