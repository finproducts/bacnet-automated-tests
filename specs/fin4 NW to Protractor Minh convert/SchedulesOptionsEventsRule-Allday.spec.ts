import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let booleanScheduleTrue = "boolScheduleTrue" + new Date().valueOf();

let booleanScheduleFalse = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleNull = "boolScheduleNull" + new Date().valueOf();

let numberSchedule = "numSchedule" + new Date().valueOf();

let stringSchedule = "strSchedule" + new Date().valueOf();

let enumSchedule = "enumSchedule" + new Date().valueOf();

describe("SchedulesOptionsEventsRule-Allday.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("New Boolean Schedule-DefaultFalse", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalse
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#content > div > select > option:nth-child(4)");

    click("#content > div > select > option:nth-child(5)");

    click("#content > div > select > option:nth-child(6)");

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With True Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("View All schedules-Click on the created schedule", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)",
      "All schedules"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Edit schedule-Pencil", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(5)",
      "schedule is open"
    );

    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(5)"
    );

    elementPresent("#content > div:nth-child(1) > input", "Edit Schedule form");

    click("#content > div:nth-child(1) > input");

    clearValue("#content > div:nth-child(1) > input");

    setValue("#content > div:nth-child(1) > input", "editedBooleanSChedule");

    waitService.waitForMilliseconds(500);

    elementPresent("#content > div:nth-child(4) > input", "Tag Editor");

    click("#content > div:nth-child(4) > input");

    setValue(
      "#content > div:nth-child(4) > input",
      "newTag, newOtherTag, tagThree"
    );

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add interval", async () => {
    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal.new-weekly > li:nth-child(1) > div > div.weekly-event-view",
      "Add interval window is open"
    );

    click(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal.new-weekly > li:nth-child(1) > div > div.weekly-event-view"
    );

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal > li:nth-child(1) > div > div.handle.top > button",
      1,
      1
    );

    mouseButtonDown(0);

    moveToElement(
      "#scheduleView > div > section > div.schedule-view > ul.weekly-events-container.horizontal > li:nth-child(1) > div > div.weekly-event-view",
      2,
      300
    );

    mouseButtonUp();

    waitService.waitForMilliseconds(500);

    click("#scheduleView > div > div > div:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side",
      "success prompt"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(10000000);
  });
  it("events -new event-rule", async () => {
    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > ul > li:nth-child(2)"
    );

    elementPresent(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > ul > li.padded.normal.all-sides.selected",
      "EVENTS"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#scheduleView > div > section > div.padded.normal.all-sides.vertical > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(2) > input",
      "events window"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(2) > input"
    );

    setValue(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(2) > input",
      "trueEventRule"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li.padded.normal.all-sides.flex.text-center"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li:nth-child(2)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li:nth-child(3)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(4) > ul > li.padded.normal.all-sides.flex.text-center"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(6) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(1) > select"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(1) > select > option:nth-child(4)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(2) > select"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(2) > select > option:nth-child(4)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(3) > select"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div:nth-child(7) > div.vertical.padded.small.v-sides > div:nth-child(3) > select > option:nth-child(10)"
    );

    click(
      "#barAppContainer > div.modal-overlay.schedule-event > div > div.horizontal.middle.center.padded.all-sides.normal > div > label"
    );

    elementPresent("#allDayEventToggle", "AllDayEventON");

    waitService.waitForMilliseconds(500);
  });
});
