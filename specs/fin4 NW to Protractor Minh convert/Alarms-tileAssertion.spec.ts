import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-tileAssertion.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    moveToElement(
      "body > app > section > header > button.link-button.pna.launcher-open-btn",
      1,
      1
    );

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(500);
  });
  it("Assert Alarm tile", async () => {
    elementPresent("#innerApp > section > ul > li:nth-child(1)", "Alarm Tile");

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > header > button:nth-child(1)",
      "info button"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > header > h6",
      "Title"
    );

    getText(
      "#innerApp > section > ul > li:nth-child(1) > div.vertical.properties-box.margin.normal.t-side > div:nth-child(1) > span.flex.grow-double.non-auto.text-right"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > header > button:nth-child(3)",
      "Magic Bubbles launcher"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > div.horizontal.middle.margin.small.v-sides > span",
      "bell icon "
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > div.horizontal.middle.margin.small.v-sides > h1",
      "Location"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > span",
      "Date time and location line"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > div.vertical.properties-box.margin.normal.t-side > div:nth-child(1)",
      "Last In Alarm row"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > div.vertical.properties-box.margin.normal.t-side > div:nth-child(2)",
      "Priority row"
    );

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > div.margin.normal.t-side.flex > em",
      "instructions box"
    );
  });
  it("info button & form", async () => {
    waitService.waitForMilliseconds(5000);

    click(
      "#innerApp > section > ul > li:nth-child(1) > header > button:nth-child(1)"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > h5"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > header > h5",
      "CITY CENTER VAV"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > thead > tr > th:nth-child(1)",
      "PROPERTY"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > thead > tr > th:nth-child(2)",
      "VALUE"
    );

    moveToElement(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(1) > td:nth-child(1)",
      3,
      3
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(1) > td:nth-child(1) > span",
      "dis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(1) > td:nth-child(1) > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(2) > td:nth-child(1) > span",
      "id"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(2) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(3) > td:nth-child(1) > span",
      "alarm"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(3) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(4) > td:nth-child(1) > span",
      "alarmStatus"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(4) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(5) > td:nth-child(1) > span",
      "alarmText"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(5) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(6) > td:nth-child(1) > span",
      "equipRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(6) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(7) > td:nth-child(1) > span",
      "equipRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(7) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(8) > td:nth-child(1) > span",
      "floorRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(8) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(9) > td:nth-child(1) > span",
      "floorRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(9) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(10) > td:nth-child(1) > span",
      "instructions"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(10) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(11)> td:nth-child(1) > span",
      "periods"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(11) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(12) > td:nth-child(1) > span",
      "priority"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(12) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(13) > td:nth-child(1) > span",
      "siteRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(13) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(14) > td:nth-child(1) > span",
      "siteRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(14) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(15) > td:nth-child(1) > span",
      "targetRef"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(15) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(16) > td:nth-child(1) > span",
      "targetRefDis"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(16) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(17) > td:nth-child(1) > span",
      "ts"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(17) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(18) > td:nth-child(1) > span",
      "tsTimezone"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(18) > td.mono.grow-double.horizontal.middle > span"
    );

    containsText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(19) > td:nth-child(1) > span",
      "tsOffset"
    );

    getText(
      "body > app > div.modal.vertical.middle.center > div > section > table > tbody > tr:nth-child(18) > td.mono.grow-double.horizontal.middle > span"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button"
    );
  });
  it("Magic buttons", async () => {
    click(
      "#innerApp > section > ul > li:nth-child(1) > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal > div > div.root.action.button.vertical.middle.center",
      "Magic button - Graphic"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(7)",
      "Magic button - Edit Description"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(6)",
      "Magic button - Point Graphics"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(5)",
      "Magic button - Graphics"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(4)",
      "Magic button - Schedules"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(3)",
      "Magic button - Add Note"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2)",
      "Magic button - Overrides"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1)",
      "Magic button - Parent Graphic"
    );

    back();
  });
  it("pause to be moved as the functions are created", async () => {
    waitService.waitForMilliseconds(500);
  });
});
