import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let stageTestProgramKMC= require('../fixtures/programs/stageTestProgramKMC');

let bp245KMC= require('../fixtures/programs/BP-245KMC');

let expect= require('chai').expect;

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('LogicBuilder-test_stage_KMC.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Load Program', async () => 
    
    {
loadProgram(bp245KMC);

waitService.waitForMilliseconds(500);

loadProgram(stageTestProgramKMC);

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Verify elements', async () => 
    
    {
containsText('#panel > section.variables > div.scroller > small','5 variables, 1 routine 1 macro block');

elementPresent('#panel > section.variables > div.scroller > div.function');

elementPresent('#panel > section.variables > div.scroller > div.macro');

elementPresent('#panel > section.variables > div.scroller > div:nth-child(3)');

elementPresent('#panel > section.variables > div.scroller > div:nth-child(4)');

elementPresent('#panel > section.variables > div.scroller > div:nth-child(5)');

elementPresent('#panel > section.variables > div.scroller > div:nth-child(6)');

elementPresent('#panel > section.variables > div.scroller > div:nth-child(7)');
}

    
  );  
  it('Verify blocks number in stage', async () => 
    
    {
execute(function(simple)
{
return app.get('blocks').length;
}
,[],function(result)
{
console.log(result.value);

expect(result.value).to.equal(12);
}
);

elementPresent('#bd362f631-4e9a-4987-a20f-1ebae26a8072');

elementPresent('#b10fbb994-fefd-4c5a-a988-7b2c6f8a537c');

elementPresent('#b83e5f5c0-427e-46f8-821c-2e9dc0204a3e');

elementPresent('#b6d0954ee-9bbc-4b67-b46e-2ccf55291cd6');

elementPresent('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07');

elementPresent('#bc637414f-b72c-459f-a36a-989caece80a0');

elementPresent('#b32f9011f-e57b-473e-aaa6-212a12484585');

elementPresent('#bd18396aa-637e-4a69-89dd-5efc3607c0d6');

elementPresent('#bf7944348-0664-46e8-a011-afa22e2a5551');

elementPresent('#ba0c8f7a7-df3e-492f-85ae-49683ceca2e1');

elementPresent('#b6be3e1f6-976d-4e2c-a5d7-eca432f28b64');

elementPresent('#b46608e4a-534e-4485-9c7e-2b4f63f0eff4');
}

    
  );  
  it('Select Blocks with keyboard shortcut', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('#bd362f631-4e9a-4987-a20f-1ebae26a8072');

waitService.waitForMilliseconds(1000);

keys([driver.Keys.CONTROL, 'a', driver.Keys.CONTROL]);

element('.comments');

to;

be;

visible;
}

    
  );  
  it('Copy-Paste Blocks with keyboard shortcuts', async () => 
    
    {
keys([driver.Keys.CONTROL, 'c', driver.Keys.CONTROL]);

keys([driver.Keys.CONTROL, 'v', driver.Keys.CONTROL]);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
return app.get('blocks').length;
}
,[],function(result)
{
console.log(result.value);

expect(result.value).to.equal(23);
}
);
}

    
  );  
  it('Use Undo with keyboard shortcut', async () => 
    
    {
keys([driver.Keys.CONTROL, 'z', driver.Keys.CONTROL]);

execute(function(simple)
{
return app.get('blocks').length;
}
,[],function(result)
{
console.log(result.value);

expect(result.value).to.equal(12);
}
);
}

    
  );  
  it('Use Redo with keyboard shortcut', async () => 
    
    {
keys([driver.Keys.CONTROL, 'y', driver.Keys.CONTROL]);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
return app.get('blocks').length;
}
,[],function(result)
{
console.log(result.value);

expect(result.value).to.equal(23);
}
);

keys([driver.Keys.CONTROL, 'z', driver.Keys.CONTROL]);
}

    
  );  
  it('Delete Blocks', async () => 
    
    {
click('#bc637414f-b72c-459f-a36a-989caece80a0');

waitService.waitForMilliseconds(500);

keys([driver.Keys.DELETE, driver.Keys.DELETE]);

waitService.waitForMilliseconds(500);

elementNotPresent('#bc637414f-b72c-459f-a36a-989caece80a0');
}

    
  );  
  it('Delete Exec Links', async () => 
    
    {
moveToElement('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.exec.sockets > div.in > div > b',2,2);

waitService.waitForMilliseconds(200);

mouseButtonClick(2);

waitForElementVisible('body > app > ul',200);

waitService.waitForMilliseconds(200);

click('body > app > ul > li');

waitService.waitForMilliseconds(500);

cssClassNotPresent('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.exec.sockets > div.in > div','linked');

cssClassNotPresent('#b32f9011f-e57b-473e-aaa6-212a12484585 > div.exec.sockets > div.out > div','linked');

click('#stage');

waitService.waitForMilliseconds(200);
}

    
  );  
  it('Delete In/Out Links', async () => 
    
    {
moveToElement('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div.socket.no-select.linked.string.variable > b',2,2);

waitService.waitForMilliseconds(200);

mouseButtonClick(2);

waitService.waitForMilliseconds(500);

visible('body > app > ul');

waitService.waitForMilliseconds(300);

click('body > app > ul > li');

waitService.waitForMilliseconds(500);

cssClassNotPresent('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div.socket.no-select.string.variable','linked');

cssClassNotPresent('#ba0c8f7a7-df3e-492f-85ae-49683ceca2e1 > div > div.output > div','linked');

click('#stage');
}

    
  );  
  it('Move Blocks', async () => 
    
    {
let stagex= 314

let stagey= 3

moveToElement('#bd362f631-4e9a-4987-a20f-1ebae26a8072',1,1);

mouseButtonDown(0);

moveToElement('.blocks',738,202);

mouseButtonUp(0);

waitService.waitForMilliseconds(500);

getLocation('#bd362f631-4e9a-4987-a20f-1ebae26a8072',function(result)
{
equal(result.value.x,738  +  stagex);

equal(result.value.y,202  +  stagey);
}
);
}

    
  );  
  it('Set input values by typing', async () => 
    
    {
setValue('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.data.sockets > div > div > div > input[type="number"]','20');

waitService.waitForMilliseconds(500);

click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07');

waitService.waitForMilliseconds(200);

element('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.data.sockets > div > div > div > input[type="number"]');

to;

have;

value;

that;

equals(20);
}

    
  );  
  it('Set input values using steppers', async () => 
    
    {
moveToElement('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07',1,1);

mouseButtonDown(0);

moveToElement('.blocks',796,245);

mouseButtonUp(0);

waitService.waitForMilliseconds(500);

click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.data.sockets > div > div > div > div > div.inc');

waitService.waitForMilliseconds(1000);

click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07');

waitService.waitForMilliseconds(200);

element('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.data.sockets > div > div > div > input[type="number"]');

to;

have;

value;

that;

equals(21);
}

    
  );  
  it('Set property values for SET blocks', async () => 
    
    {
click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07');

waitService.waitForMilliseconds(500);

visible('.property');

click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.property');

waitService.waitForMilliseconds(500);

visible('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.property > div');
}

    
  );  
  it('Set priority values for SET blocks by typing', async () => 
    
    {
visible('.priority');

click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.priority');

waitService.waitForMilliseconds(500);

visible('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.priority > div');

clearValue('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.priority > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.priority > div > div > input[type="number"]','10');

waitService.waitForMilliseconds(500);

containsText('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.data.sockets','10');
}

    
  );  
  it('Set priority values for SET blocks using steppers', async () => 
    
    {
visible('.priority');

visible('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.priority > div');

click('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.top-icons.no-select > div.priority > div > div > div > div.inc');

waitService.waitForMilliseconds(500);

containsText('#b20432b57-3ee8-4cdb-9021-2f91e9e37d07 > div.data.sockets','11');
}

    
  );  
  it('Set property values for GET blocks', async () => 
    
    {
click('#b6d0954ee-9bbc-4b67-b46e-2ccf55291cd6');

waitService.waitForMilliseconds(500);

visible('.property');

click('#b6d0954ee-9bbc-4b67-b46e-2ccf55291cd6 > div.top-icons.no-select > div.property');

waitService.waitForMilliseconds(500);

visible('#b6d0954ee-9bbc-4b67-b46e-2ccf55291cd6 > div.top-icons.no-select > div.property > div');
}

    
  );  
  it('Use Color Picker', async () => 
    
    {
click('#bf7944348-0664-46e8-a011-afa22e2a5551');

waitService.waitForMilliseconds(200);

mouseButtonDown(0);

moveToElement('.blocks',1154,515);

waitService.waitForMilliseconds(200);

mouseButtonUp(0);

click('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > input');

waitService.waitForMilliseconds(500);

keys('#fff000');

waitService.waitForMilliseconds(500);

elementPresent('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > div');

click('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > div');

waitService.waitForMilliseconds(500);

visible('.dropdown.open');

value('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > div > div > div > div.inputs > input','fff000');

click('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > div > div > div > div.pickers > div.slPicker');

moveToElement('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > div > div > div > div.pickers > div.slPicker',40,40);

waitService.waitForMilliseconds(500);

mouseButtonClick(0);

waitService.waitForMilliseconds(500);

value('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div > div > div > div > div.inputs > input','#00cfff');
}

    
  );  
  it('Create exec links', async () => 
    
    {
click('#b32f9011f-e57b-473e-aaa6-212a12484585 > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#b32f9011f-e57b-473e-aaa6-212a12484585 > div.exec.sockets > div.out > div','linked');

cssClassPresent('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.exec.sockets > div.in > div','linked');
}

    
  );  
  it('Create input/output links', async () => 
    
    {
click('#ba0c8f7a7-df3e-492f-85ae-49683ceca2e1 > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div.socket.no-select.string.variable > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#bf7944348-0664-46e8-a011-afa22e2a5551 > div.data.sockets > div > div.socket.no-select.linked.string.variable','linked');

cssClassPresent('#ba0c8f7a7-df3e-492f-85ae-49683ceca2e1 > div > div.output > div','linked');
}

    
  );  
  it('Change Variable Type and verify in stage', async () => 
    
    {
click('#panel > section.variables > div.scroller > div:nth-child(3) > i');

cssClassPresent('#panel > section.variables > div.scroller > div:nth-child(3)','integer');

elementPresent('#b83e5f5c0-427e-46f8-821c-2e9dc0204a3e.set.integer.block');
}

    
  );  
  it('Change data type for macro block output', async () => 
    
    {
click('#panel > section.variables > div.scroller > div.macro > span');

waitService.waitForMilliseconds(500);

click('.macro.modal table tbody tr:nth-child(3) td:nth-child(3) div');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(4)');

waitService.waitForMilliseconds(500);

containsText('.macro.modal table tbody tr:nth-child(3) td:nth-child(3) div','string');

click('body > app > div.macro.modal > div > div.close > i');

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});