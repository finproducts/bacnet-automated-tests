import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let trendName = "trend" + new Date().valueOf();

describe("SchedulesMenu.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("SchedulesMenu", async () => {
    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1)",
      "Schedules Menu"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)",
      "View All Schedules"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)",
      "Create New Schedules"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)",
      "Refresh"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5)",
      "Legacy Schedules"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6)",
      "Miscellaneous"
    );

    waitService.waitForMilliseconds(1500);
  });
  it("Schedules Menu-View all schedules", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(2000);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)",
      "Back"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a",
      "Create New Schedule"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a",
      "Refresh"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a",
      "Legacy Schedules"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > a",
      "Miscellaneous"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(7)",
      "--ALL SCHEDULES--"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(9)",
      "Normal Work Week"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10)",
      "Sport Weekend"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11)",
      "Summer School"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)",
      "Vav Rmt Control"
    );

    waitService.waitForMilliseconds(1500);
  });
  it("Back", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(1500);

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(7) > div > h6",
      "City Center AHU-1",
      "Back to schedules menu"
    );
  });
  it("Refresh", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a"
    );

    waitForElementPresent(
      "body > app > section > div > div.app-nav.vertical",
      "refreshed"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Legacy Schedules", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > a"
    );

    waitForElementPresent("#FINStack", "Legacy Schedules");

    waitService.waitForMilliseconds(500);
  });
});
