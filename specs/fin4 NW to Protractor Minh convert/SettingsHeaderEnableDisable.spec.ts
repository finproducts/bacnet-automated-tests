import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let settingsButton = po.settingsButton;

describe("SettingsHeaderEnableDisable.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("header", async () => {
    cssClassPresent(
      settingsButton +
        "> ul > li:nth-child(3) > span.icon-toggles.mnh.non-interactive",
      "icon-toggles"
    );

    click(settingsButton + " > ul > li:nth-child(4)");

    waitService.waitForMilliseconds(1000);
  });
  it("header menu assertions", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1) > header > h4",
      "— Edit Header —"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Home"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2) > a > div > div > span.nav-item-icon.icon-home",
      "icon-home"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Disable Header"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-item-icon.icon-cross",
      "icon-cross"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Add Button"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-item-icon.icon-plus",
      "icon-plus"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Reorder Buttons"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-item-icon.icon-list",
      "icon-list"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6) > div > h6",
      "Buttons"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Disable Header button", async () => {
    cssClassPresent(
      "body > app > section > header > button:nth-last-child(2) > span",
      "icon-retweet"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(20000000);

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Disable Header"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-item-icon.icon-cross",
      "icon-cross"
    );

    refresh();

    cssClassNotPresent(
      "body > app > section > header > button:nth-last-child(2) > span",
      "icon-retweet",
      "header disabled"
    );

    waitService.waitForMilliseconds(500000000);
  });
  it("go  back to settings", async () => {
    waitService.waitForMilliseconds(5000);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(1500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it(" go back to header", async () => {
    click(settingsButton + " > ul > li:nth-child(4)");

    waitService.waitForMilliseconds(2000);
  });
  it("Enable Header button", async () => {
    cssClassPresent(
      "body > app > section > header > button:nth-child(4) > span",
      "icon-retweet"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Enable Header"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3) > a > div > div > span.nav-item-icon.icon-checkmark",
      "icon-checkmark"
    );

    refresh();

    waitService.waitForMilliseconds(5000);

    cssClassPresent(
      "body > app > section > header > button:nth-child(4) > span",
      "icon-retweet",
      "header enabled"
    );

    waitService.waitForMilliseconds(1000);
  });
});
