import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "JSGB" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let graphicsBuilderButton = po.graphicsBuilderButton;

describe("LogicBuilder-JSFromGB-AssertionTest.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Graphics Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton);

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton + "> ul > li:nth-child(3)");

    waitService.waitForMilliseconds(3000);
  });
  it("New Graphic Form", async () => {
    click("#content > div.form-item-holder.textinput > input");

    keys(programName);

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(300);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    waitForElementPresent(
      "body > app > div.modal.form-stack.vertical.middle.center > div",
      2000
    );

    moveToElement("#controlBar > button", 1, 1);

    waitService.waitForMilliseconds(5000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Open The Graphic", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Drag a button", async () => {
    frame(0);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#container > ul:nth-child(6) > li:nth-child(2) > iron-icon",
      "#main-content > section > div"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#projects-container > div > div > button",
      "Button is dragged on the stage"
    );
  });
  it("Right-click on button", async () => {
    moveToElement("#projects-container > div > div > button", 2, 2);

    mouseButtonClick(2);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1)",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    click(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scriptArea > div.ace_scroller > div",
      "Script editor form launched succesfully"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Script Editor Form", async () => {
    frame(1);

    waitService.waitForMilliseconds(5000);

    click("#editorModeSelector > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("Sidepanel-Assertions", async () => {
    frame(0);

    containsText(
      "#panel > section.controls > div.program-name > span.language",
      "JavaScript",
      'JavaScript"  +  " "  +  " language is displayed'
    );

    elementPresent(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input",
      "Title is Unnamed Program"
    );

    elementPresent(
      "#panel > section.controls > div.icons > div > i",
      "Alignment button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.undo.icon.disabled",
      "undo button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.redo.icon.disabled",
      "redo button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.enum.icon",
      "edit enum button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.collapse.icon.fr",
      "collapse button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(2) > i.point.symbol",
      "add new  virtual point"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol",
      "Add Variable button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(4) > i.function.symbol",
      "Add new function button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(5) > i.macro.symbol.mrxs",
      "Add new macro button is present"
    );

    elementPresent(
      "#panel > section.variables > div.filters > div",
      "search bar is present"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(2)",
      "LIBRARY BLOCKS"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(2)",
      "control flow"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(3)",
      "language"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(4)",
      "math"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(5)",
      "application"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(6)",
      "array"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(7)",
      "lodash"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(8)",
      "logic"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(9)",
      "string"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(10)",
      "ui"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(11)",
      "utilities"
    );

    elementPresent(
      "#stage > div.scroller > div > div",
      "Start block is present"
    );

    waitService.waitForMilliseconds(1000);
  });
});
