import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "JSGB" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let graphicsBuilderButton = po.graphicsBuilderButton;

describe("LogicBuilder-JSFromGB-TestProgram.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton);

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton + "> ul > li:nth-child(3)");

    waitService.waitForMilliseconds(3000);
  });
  it("New Graphic Form", async () => {
    click("#content > div.form-item-holder.textinput > input");

    keys(programName);

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(300);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    waitForElementPresent(
      "body > app > div.modal.form-stack.vertical.middle.center > div",
      2000
    );

    moveToElement("#controlBar > button", 1, 1);

    waitService.waitForMilliseconds(5000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Open The Graphic", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Drag a button", async () => {
    frame(0);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#container > ul:nth-child(6) > li:nth-child(2) > iron-icon",
      "#main-content > section > div"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#projects-container > div > div > button",
      "Button is dragged on the stage"
    );
  });
  it("Right-click on button", async () => {
    moveToElement("#projects-container > div > div > button", 2, 2);

    mouseButtonClick(2);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1)",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    click(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scriptArea > div.ace_scroller > div",
      "Script editor form launched succesfully"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Script Editor Form", async () => {
    frame(1);

    waitService.waitForMilliseconds(5000);

    click("#editorModeSelector > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("Rename JS PROGRAM", async () => {
    frame(0);

    click(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input"
    );

    clearValue(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input"
    );

    setValue(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input",
      programName
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Add variable1 ", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol"
    );

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div:nth-child(1) > b.variable.get.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.1.x", 100);

      app.set("blocks.1.y", 300);
    }, []);

    waitService.waitForMilliseconds(1000);
  });
  it("Add and edit add block", async () => {
    click(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    clearValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "add"
    );

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div:nth-child(2) > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.2.x", 400);

      app.set("blocks.2.y", 300);
    }, []);

    waitService.waitForMilliseconds(1000);

    click(
      '#stage > div > div > div.add.block > div > div.input > div:nth-child(2) > div > input[type="text"]'
    );

    setValue(
      '#stage > div > div > div.add.block > div > div.input > div:nth-child(2) > div > input[type="text"]',
      "10"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Add Variable2", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol"
    );

    elementPresent("#panel > section.variables > div.scroller > div");

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div:nth-child(2) > b.variable.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.3.x", 600);

      app.set("blocks.3.y", 250);
    }, []);

    waitService.waitForMilliseconds(1000);
  });
  it("Link variable1 to add", async () => {
    click(
      "#stage > div > div > div.get.variable.block > div > div.output > div > b"
    );

    waitService.waitForMilliseconds(1000);

    click("#stage > div > div > div.add.block > div > div.input > div > b");

    waitService.waitForMilliseconds(1000);
  });
  it("Link add to set variable2", async () => {
    click("#stage > div > div > div.add.block > div > div.output > div > b");

    waitService.waitForMilliseconds(1000);

    click(
      "#stage > div > div > div.set.variable.block > div.data.sockets > div.input > div > b"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Link set variable2 to start", async () => {
    click(
      "#stage > div > div > div.set.variable.block > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(1000);

    click("#stage > div > div > div.__start__.block > div > div > div > b");

    waitService.waitForMilliseconds(1000);
  });
});
