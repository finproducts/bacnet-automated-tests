import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let historianButton = po.historianButton;

let Enum = "Trend" + new Date().valueOf();

let trendSAName = "trend" + new Date().valueOf();

describe("HistorianTestEnum.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Historian", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(historianButton);

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)",
      "historian is opened"
    );
  });
  it("new trend", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    containsText("#equipList > li:nth-child(1) > a > h4", "City Center AHU-1");

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(3) > div > div.switch");

    click("#relativeSelector");

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);
  });
  it("cogwheel option and save", async () => {
    click("#fooAppContainer > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input"
    );

    waitService.waitForMilliseconds(900);

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input",
      Enum
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div.padded.large.all-sides.horizontal.middle.center > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(3000);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div.custom-folder-input-container.horizontal.middle.padded.large.all-sides > input"
    );

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div.custom-folder-input-container.horizontal.middle.padded.large.all-sides > input",
      "testPath"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );
  });
  it("Go to Favorites ", async () => {
    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click("body > app > section > section > ul > li:nth-child(5) > div");

    waitService.waitForMilliseconds(1000);

    click(
      "#trendsView > div > section > div.vertical.flex > div > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div > div > div.amChartsLegend.amcharts-legend-div > svg > g > g > g > rect"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div > div > div.amChartsLegend.amcharts-legend-div > svg > g > g > g > rect",
      "Enum"
    );

    waitService.waitForMilliseconds(1000);
  });
});
