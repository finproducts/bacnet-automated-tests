import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;



describe('LogicBuilder-test_menuSection.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(3000);

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('All menu buttons are present', async () => 
    
    {
waitService.waitForMilliseconds(500);

elementPresent('i.file.icon');

elementPresent('i.save.icon');

elementPresent('i.open.icon');

elementPresent('i.export.icon');

elementPresent('i.import.icon');

elementPresent('i.undo.icon');

elementPresent('i.redo.icon');

elementPresent('i.alignment.icon');

elementPresent('i.enum.icon');

elementPresent('i.collapse.icon.fr');
}

    
  );  
  it('Change Language JavaScript', async () => 
    
    {
click('i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with languages appears on screen');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present');

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div.window > div > span:nth-child(1)');

waitService.waitForMilliseconds(500);

containsText('span.language','JavaScript','Language changed to JavaScript');
}

    
  );  
  it('Change Language PPCL', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with languages appears on screen');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present');

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div.window > div > span:nth-child(3)');

waitService.waitForMilliseconds(500);

containsText('span.language','PPCL','Language changed to PPCL');
}

    
  );  
  it('Change Language Ruby', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with saving warning appears on screeen');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present');

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div.window > div > span:nth-child(4)');

waitService.waitForMilliseconds(500);

containsText('span.language','Ruby','Language changed to Ruby');
}

    
  );  
  it('Change Language axon', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with saving warning appears on screeen');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present');

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div.window > div > span:nth-child(5)');

waitService.waitForMilliseconds(500);

containsText('span.language','axon','Language changed to axon');
}

    
  );  
  it('Change Language bLine', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with saving warning appears on screeen');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present');

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('body > app > div.newProgram.modal > div > div.window > div > span:nth-child(6)');

waitService.waitForMilliseconds(500);

containsText('span.language','bLine','Language changed to bLine');
}

    
  );  
  it('Change Language KMC', async () => 
    
    {
waitService.waitForMilliseconds(500);

click('i.file.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with languages appears on screen');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Creating a new program will discard your current program','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present');

click('button.mls.danger.button');

click('body > app > div.newProgram.modal > div > div.window > div > span:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#panel > section.controls > div.program-name > span.language','KMC','Language changed to KMC');
}

    
  );  
  it('Verify Load notification', async () => 
    
    {
click('#panel > section.controls > div.icons > label');

waitService.waitForMilliseconds(1000);

visible('.unsavedWorkWarning.modal > div > div.window','A pop-up appears when clicking 'Load' button');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)','You have unsaved work! Loading a new program will discard your current program.','Warning appears about unsaved changes');

containsText('.unsavedWorkWarning.modal > div > div.window > div > p.mts','Do you wish to proceed?',''Do you wish to proceed?' message is present',''Do you wish to proceed?' message is present');

elementPresent('.unsavedWorkWarning.modal > div > div.close > i','A close button is present');

click('.unsavedWorkWarning.modal > div > div.window > footer > button');

waitService.waitForMilliseconds(500);

elementNotPresent('.window','The warning window closed after clicking that button');
}

    
  );  
  it('Verify Tool Tip New Program', async () => 
    
    {
moveToElement('i.file.icon',2,2);

waitService.waitForMilliseconds(200);

elementPresent('.ractive-tooltip');

containsText('.ractive-tooltip','Create a new program','Create a new program' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Save', async () => 
    
    {
moveToElement('i.save.icon',2,2);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Save current program',''Save current program' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Load', async () => 
    
    {
moveToElement('.open',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Load saved program',''Load saved program' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Import', async () => 
    
    {
moveToElement('i.import.icon',0,0);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Import program from code',''Import program from code' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Export', async () => 
    
    {
moveToElement('i.export.icon',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Export current program to code',''Export current program to code' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Alignment', async () => 
    
    {
moveToElement('i.alignment.icon',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Align blocks',''Align blocks' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Enum Editor', async () => 
    
    {
moveToElement('i.enum.icon',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Open Enum editor',''Open Enum editor' tool tip is present');
}

    
  );  
  it('Use Colapse and Verify Tool Tip', async () => 
    
    {
moveToElement('i.collapse.icon.fr',2,2);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Collapse side panel');
}

    
  );  
  it('Verify Tool Tip Expand', async () => 
    
    {
click('#panel > section.controls > div.icons > i.collapse.icon.fr');

waitService.waitForMilliseconds(1000);

moveToElement('#stage > div > div > div',0,0);

waitService.waitForMilliseconds(1000);

moveToElement('i.collapse.icon.fr',2,5);

waitService.waitForMilliseconds(1500);

elementPresent('i.collapse.icon.fr');

click('i.collapse.icon.fr');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Use Undo from menu button', async () => 
    
    {
moveToElement('.__start__.block',5,5);

waitService.waitForMilliseconds(500);

mouseButtonDown(0);

waitService.waitForMilliseconds(500);

moveTo(null,50,50);

waitService.waitForMilliseconds(500);

mouseButtonUp(0);

waitService.waitForMilliseconds(500);

moveToElement('i.undo.icon',5,5);

waitService.waitForMilliseconds(200);

containsText('.ractive-tooltip','Undo last action');

waitService.waitForMilliseconds(500);

click('i.undo.icon');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Use Redo from menu button', async () => 
    
    {
moveToElement('i.redo.icon',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Redo last action');

waitService.waitForMilliseconds(500);

click('i.redo.icon');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Open Import window', async () => 
    
    {
click('i.import.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with languages appears on screen');

elementPresent('.controls');

elementPresent('.mls','Import Button');

containsText('.window','IMPORT PROGRAM INTO BLOCKS');

click('.import.modal > div > div.close > i');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Check languages in import window', async () => 
    
    {
click('i.import.icon');

waitService.waitForMilliseconds(500);

click('.import.modal > div > div.window > div > div.controls > div > label');

waitService.waitForMilliseconds(500);

elementPresent('.dropdown.open');

click('#ractive-select-dropdown-container > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

containsText('.import.modal > div > div.window > div > div.controls > div > label','JavaScript','JavaScript is the selected language');

click('.import.modal > div > div.window > div > div.controls > div > label');

waitService.waitForMilliseconds(300);

elementPresent('.dropdown.open');

click('#ractive-select-dropdown-container > ul > li:nth-child(2)');

waitService.waitForMilliseconds(300);

click('.import.modal > div > div.window > div > div.controls > div > label');

waitService.waitForMilliseconds(300);

click('#ractive-select-dropdown-container > ul > li:nth-child(1)');

waitService.waitForMilliseconds(300);

containsText('.import.modal > div > div.window > div > div.controls > div > label','JavaScript');

click('.import.modal > div > div.window > div > div.CodeMirror.cm-s-default > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre');

waitService.waitForMilliseconds(300);

keys(driver,Keys,ENTER);

keys(driver,Keys,ENTER);

keys('booL = booL');

keys(driver,Keys,ENTER);

keys('numM = numM');

keys(driver,Keys,ENTER);

keys('strinG = strinG');

elementPresent('.mls','Import Button Present');

click('.import.modal > div > div.window > div > div.controls > button');

visible('#stage');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Open Export window', async () => 
    
    {
waitService.waitForMilliseconds(200);

click('i.export.icon');

waitService.waitForMilliseconds(500);

elementPresent('.window','Window with languages appears on screen');

waitService.waitForMilliseconds(200);

containsText('.window','GENERATED JAVASCRIPT CODE');
}

    
  );  
  
  
});