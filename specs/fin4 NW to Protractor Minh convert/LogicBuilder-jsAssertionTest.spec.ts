import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("LogicBuilder-jsAssertionTest.js", () => {
  it("Go to Block Programming JavaScript", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(1)");

    waitService.waitForMilliseconds(500);
  });
  it("Sidepanel-Assertions", async () => {
    containsText(
      "#panel > section.controls > div.program-name > span.language",
      "JavaScript",
      'JavaScript"  +  " "  +  " language is displayed'
    );

    elementPresent(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input",
      "Title is Unnamed Program"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.file.icon",
      "New Program button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > label > i",
      "Open file button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.save.icon",
      "Save button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.import.icon",
      "Import program from code button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.export.icon",
      "Export current program button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > div > i",
      "Alignment button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.undo.icon.disabled",
      "undo button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.redo.icon.disabled",
      "redo button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.enum.icon",
      "edit enum button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.collapse.icon.fr",
      "collapse button is present"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#panel > section.variables > header > div:nth-child(2) > i.point.symbol",
      "add new  virtual point"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol",
      "Add Variable button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(4) > i.function.symbol",
      "Add new function button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(5) > i.macro.symbol.mrxs",
      "Add new macro button is present"
    );

    elementPresent(
      "#panel > section.variables > div.filters > div",
      "search bar is present"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(2)",
      "LIBRARY BLOCKS"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(2)",
      "control flow"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(3)",
      "language"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(4)",
      "math"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(5)",
      "application"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(6)",
      "array"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(7)",
      "lodash"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(8)",
      "logic"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(9)",
      "string"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(10)",
      "ui"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(11)",
      "utilities"
    );

    elementPresent(
      "#stage > div.scroller > div > div",
      "Start block is present"
    );
  });
});
