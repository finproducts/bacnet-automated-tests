import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-generalAssertion.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(500);
  });
  it("assert alarms", async () => {
    containsText(
      "#innerApp > header > h4",
      "— Alarms —",
      "Title of the sidemenu is Alarms"
    );

    elementPresent(
      "#innerApp > header > button:nth-child(3) > span",
      "Expand button is present"
    );

    elementPresent(
      "#innerApp > section > header > button.button-default.ack-btn.margin.small.r-side.active",
      "Tiles Button"
    );

    elementPresent(
      "#innerApp > section > header > button:nth-child(2)",
      "List Button"
    );

    elementPresent(
      "#innerApp > section > header > button:nth-child(4)",
      "Acknowledge All"
    );

    elementPresent(
      "#innerApp > section > header > button:nth-child(4)",
      "Filter Button"
    );

    elementPresent(
      "#innerApp > section > header > button:nth-child(5)",
      "settings Button"
    );
  });
  it("top buttons launch test", async () => {
    click("#innerApp > header > button:nth-child(3) > span");

    waitForElementPresent("#innerApp > section > div", 2000, "Main view");

    click("#innerApp > section > header > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "#innerApp > section > div.can-scroll.margin.small.t-side > table",
      1000,
      "table view"
    );

    click("#innerApp > section > header > button:nth-child(4)");

    waitForElementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div",
      1000,
      "filter form"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open"
    );

    waitForElementNotPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open",
      1000,
      "filter form closed"
    );

    click("#innerApp > section > header > button:nth-child(5)");

    waitForElementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div",
      1000,
      "settings form opened"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open"
    );

    waitForElementNotPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div",
      1000,
      "settings form closed"
    );

    click("#innerApp > section > header > button:nth-child(1)");

    waitForElementPresent(
      "#innerApp > section > ul > li:nth-child(1)",
      1000,
      "back to tiles"
    );
  });
});
