import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "jsProgram" + new Date().valueOf();

describe("LogicBuilder-kmcPerformanceTest.js", () => {
  it("Go to Block Programming JS", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add variables", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(2) > i.variable.symbol"
    );

    click(
      "#panel > section.variables > header > div:nth-child(2) > i.variable.symbol"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Drag the number set block on stage", async () => {
    for (let n = 2; n <= 101; n++) {
      execute(dragAndDrop, [
        "#panel > section.variables > div.scroller > div:nth-child(1) > b.variable.set.block",
        ".blocks"
      ]);
    }

    waitService.waitForMilliseconds(1000);

    execute(
      function(simple) {
        let numberOfBlocks = app.get("blocks").length;

        for (let n = 1; n < numberOfBlocks; n++) {
          app.set('blocks."  +  n  +  ".x', 210 + n * 100),
            app.set('blocks."  +  n  +  ".y', 404 + n * 100);
        }
      },
      [],
      function(result) {}
    );
  });
  it("Linking blocks gate2gate", async () => {
    let lx;

    for (lx = 2; lx <= 100; lx++) {
      waitService.waitForMilliseconds(300);

      click(
        '#stage > div.scroller > div > div:nth-child("  +  lx  +  ") > div.exec.sockets > div.out > div > b'
      );

      waitService.waitForMilliseconds(300);

      click(
        '#stage > div.scroller > div > div:nth-child("  +  (lx + 1)  +  ") > div.exec.sockets > div.in > div > b'
      );
    }

    waitService.waitForMilliseconds(500);
  });
  it("Save Program", async () => {
    moveToElement("#panel > section.controls > div.icons > i.save.icon", 1, 1);

    waitService.waitForMilliseconds(500);

    click("#panel > section.controls > div.icons > i.save.icon");

    waitService.waitForMilliseconds(1000);
  });
});
