import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let schedulesButton = po.schedulesButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let booleanScheduleTrue = "boolScheduleTrue" + new Date().valueOf();

let booleanScheduleFalseCAMPUS = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleFalseCC = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleFalseFloor1 = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleFalseAHU1 = "boolScheduleFalse" + new Date().valueOf();

let booleanScheduleNull = "boolScheduleNull" + new Date().valueOf();

let numberSchedule = "numSchedule" + new Date().valueOf();

let stringSchedule = "strSchedule" + new Date().valueOf();

let enumSchedule = "enumSchedule" + new Date().valueOf();

describe("SchedulesLevel.js", () => {
  it("LOGIN", async () => {});
  it("go to Schedules", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(schedulesButton);

    waitService.waitForMilliseconds(1500);
  });
  it("New Boolean Schedule-DefaultFalse", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalseCAMPUS
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With True Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Verify Schedule is created on Campus", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      booleanScheduleFalseCAMPUS
    );

    waitService.waitForMilliseconds(500);
  });
  it("Navigate to CC", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(2000);

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCAMPUS
    );

    waitService.waitForMilliseconds(500);
  });
  it("New Boolean Schedule-DefaultFalse on CC", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalseCC
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With True Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Verify Schedule is created on CC", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      booleanScheduleFalseCAMPUS
    );

    waitService.waitForMilliseconds(500);
  });
  it("Navigate to Floor 1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(2000);

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCAMPUS
    );

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCC
    );

    waitService.waitForMilliseconds(500);
  });
  it("New Boolean Schedule-DefaultFalse on floor 1", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalseFloor1
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With True Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Verify Schedule is created on floor1", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      booleanScheduleFalseFloor1
    );

    waitService.waitForMilliseconds(500);
  });
  it("Navigate to AHU1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(2000);

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCAMPUS
    );

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCC
    );

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseFloor1
    );

    waitService.waitForMilliseconds(500);
  });
  it("New Boolean Schedule-DefaultFalse on AHU 1", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "#content > div.form-item-holder.textinput > input",
      "New Schedule Form"
    );

    setValue(
      "#content > div.form-item-holder.textinput > input",
      booleanScheduleFalseAHU1
    );

    click("#content > div.form-item-holder.combobox > select");

    click("#controlBar > button:nth-child(2)");

    elementPresent("#content > div > select", "True option");

    click("#content > div > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    elementPresent(
      "#controlBar > button",
      "Boolean With True Default Value Created Succesfully"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Verify Schedule is created on Ahu1", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      booleanScheduleFalseAHU1
    );

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCAMPUS
    );

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseCC
    );

    notEqual(
      await $(
        "body > app > section > div > div.app-nav.vertical > nav > div:nth-last-child(1) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive"
      ).getText(),
      booleanScheduleFalseFloor1
    );

    waitService.waitForMilliseconds(500);
  });
});
