import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-filterMenu-MainView.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("Switch to main view", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");
  });
  it("filter button", async () => {
    elementPresent(
      "#innerApp > section > header > button:nth-last-child(2)",
      "filter Button"
    );

    click("#innerApp > section > header > button:nth-last-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("filter menu-search", async () => {
    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > header:nth-child(1) > h6",
      "— APPLY FILTER —"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div.horizontal.middle.margin.normal.t-side > input"
    );

    setValue(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div.horizontal.middle.margin.normal.t-side > input",
      "vav-03"
    );

    click("#innerApp > section > header > button:nth-child(4)");

    waitService.waitForMilliseconds(2000);

    containsText(
      "#innerApp > section > ul > li > div.horizontal.middle.margin.small.v-sides > h1",
      "City Center Vav-03",
      "Found vav-03, search works "
    );

    elementNotPresent(
      "#innerApp > section > ul > li:nth-child(2)",
      "only the found item is present"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("filter menu - period", async () => {
    click("#innerApp > section > header > button:nth-child(4)");

    waitService.waitForMilliseconds(1000);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(1)",
      "Today"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(2)",
      "This Week"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(3)",
      "This Month"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(4)",
      "This Year"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(5)",
      "Yesterday"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(6)",
      "Last Week"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(7)",
      "Last Month"
    );

    containsText(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(8)",
      "Last Year"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("filter menu - priority", async () => {
    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div.horizontal.middle.margin.normal.t-side > input"
    );

    clearValue(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div.horizontal.middle.margin.normal.t-side > input"
    );

    waitService.waitForMilliseconds(1000);

    click(
      '#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(2) > input[type="text"]'
    );

    clearValue(
      '#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(2) > input[type="text"]'
    );

    setValue(
      '#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(2) > input[type="text"]',
      100
    );

    click("#innerApp > section > header > button:nth-child(4)");

    elementNotPresent(
      "#innerApp > section > ul > li:nth-child(1)",
      "No alarms with 100 priority, filter by priority works"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("sliders", async () => {
    click("#innerApp > section > header > button:nth-child(4)");

    clearValue(
      '#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(2) > input[type="text"]'
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(1) > div > select > option:nth-child(4)"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(1) > div > label",
      "Acked"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(2) > div > label",
      "Un-Acked"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(3) > div > label",
      "In Alarm"
    );

    elementPresent(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(4) > div > label",
      "Not In Alarm"
    );

    setValue(
      '#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(3) > div:nth-child(2) > input[type="text"]',
      107
    );

    waitService.waitForMilliseconds(10000);
  });
  it("Acked Slider", async () => {
    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(2) > div > label"
    );

    waitService.waitForMilliseconds(300);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(3) > div > label"
    );

    waitService.waitForMilliseconds(300);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(4) > div > label"
    );

    click("#innerApp > section > header > button:nth-child(4)");

    elementNotPresent(
      "#innerApp > section > ul > li:nth-child(1) > button",
      "no acknowledge button"
    );

    containsText(
      "#innerApp > section > ul > li:nth-child(1) > div.vertical.middle.center.margin.normal.t-side > span:nth-child(1)",
      "— Acknowledged —",
      "Acknowledged"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Un-Acked Slider", async () => {
    click("#innerApp > section > header > button:nth-child(4)");

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(1) > div > label"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(2) > div > label"
    );

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open"
    );

    waitService.waitForMilliseconds(4000);

    elementPresent(
      "#innerApp > section > ul > li:nth-child(1) > button",
      "Acknowledge button"
    );

    elementNotPresent(
      "#innerApp > section > ul > li:nth-child(1) > div.vertical.middle.center.margin.normal.t-side > span:nth-child(1)",
      "alarm is not acknowledged"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("In alarm slider", async () => {
    click("#innerApp > section > header > button:nth-last-child(2)");

    waitService.waitForMilliseconds(200);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(2) > div > label"
    );

    waitService.waitForMilliseconds(200);

    click(
      "#innerApp > section > header > button.link-button.dd-element.horizontal.middle.open > div > section > div:nth-child(5) > div:nth-child(3) > div > label"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Not in alarm slider", async () => {
    waitService.waitForMilliseconds(1000);
  });
});
