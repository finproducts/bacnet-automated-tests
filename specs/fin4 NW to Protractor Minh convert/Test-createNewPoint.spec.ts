import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let query =
  "diff(null,{dis:”pointName”,cur,cmd,point,equipRef:@201b63cb-a755131d,siteRef:@201b6286-6eca8931,floorRef:@201b6334-e3007481,kind:'Enum'},{add}).commit";

describe("Test-createNewPoint.js", () => {
  it("go to City Center VAV-1", async () => {
    finEval(query, function(output) {
      console.log(output);
    });
  });
});
