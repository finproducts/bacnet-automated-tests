import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-newProgramForm.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    click("#content > div.form-item-holder.textarea > textarea");

    keys("summary");

    waitService.waitForMilliseconds(500);

    click("#content > div.form-item-holder.filter > div > input");

    waitService.waitForMilliseconds(500);

    element("#content > div.form-item-holder.filter > div > input");

    to;

    have;

    value;

    that;

    equals("equip and hotWaterReheat and hvac and vav");

    waitService.waitForMilliseconds(4000);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(10)"
    );

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(6) > div > div > input");

    click("#content > div:nth-child(7) > div > div > input");

    click("#content > div:nth-child(8) > div > div > input");

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(2000);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(3000);
  });
  it("bline program", async () => {
    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)"
    );

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(5)"
    );

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(2000);
  });
  it("Add Absolute point variables ", async () => {
    moveToElement(
      "#content > div.form-item-holder.ractive > div > div > ul > li:nth-child(2) > div",
      1,
      1
    );

    click(
      "#content > div.form-item-holder.ractive > div > div > ul > li:nth-child(2) > div"
    );
  });
  it("OK/cancel", async () => {
    click("#controlBar > button:nth-child(2)");
  });
});
