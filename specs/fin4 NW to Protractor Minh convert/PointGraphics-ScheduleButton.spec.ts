import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let pointGraphicsButton = po.pointGraphicsButton;

let trendName = "trend" + new Date().valueOf();

describe("PointGraphics-ScheduleButton.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to point graphics", async () => {
    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(pointGraphicsButton);

    waitService.waitForMilliseconds(500);
  });
  it("Check Schedule button", async () => {
    elementPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button:nth-child(2)",
      "Schedule Button"
    );

    moveToElement(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button:nth-child(2)",
      1,
      1
    );

    click(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(1500);
  });
  it("Schedule details", async () => {
    elementPresent("#schedule3");

    containsText(
      "#schedule3 > div > div:nth-child(1) > span.italic.flex.non-auto.margin.small.v-sides",
      "Schedule Name:"
    );

    containsText(
      "#schedule3 > div > div:nth-child(2) > span.italic.flex.non-auto.margin.small.v-sides",
      "Schedule Value:"
    );

    containsText(
      "#schedule3 > div > div:nth-child(3) > span.italic.flex.non-auto.margin.small.v-sides",
      "Next Update:"
    );

    containsText(
      "#schedule3 > div > div:nth-child(4) > span.italic.flex.non-auto.margin.small.v-sides",
      "Next Value:"
    );

    waitService.waitForMilliseconds(1500);
  });
});
