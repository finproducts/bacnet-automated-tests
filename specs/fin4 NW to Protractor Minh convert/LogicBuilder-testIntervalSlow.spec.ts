import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-testIntervalSlow.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Create bLine Program", async () => {
    newProg(programName);
  });
  it("Add number variable", async () => {
    frame(0);

    click("#stage > div.scroller > div > div > div > div > div > b");

    waitService.waitForMilliseconds(1000);

    click("body");

    moveToElement("body", 300, 600);

    mouseButtonClick(0);

    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(3) > div > input"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    moveToElement("#content > div.form-item-holder.textinput > input", 1, 1);

    setValue("#content > div.form-item-holder.textinput > input", "interval");

    click("#content > div:nth-child(2) > div > input");

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(2000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(2000);
  });
  it("Move interval getter to stage", async () => {
    frame(0);

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div > b.number.get.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.2.x", 100);

      app.set("blocks.2.y", 300);
    }, []);

    waitService.waitForMilliseconds(1000);
  });
  it("Add add block to stage ", async () => {
    frame(0);

    click(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "add"
    );

    waitService.waitForMilliseconds(2000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div:nth-child(3) > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.3.x", 250);

      app.set("blocks.3.y", 700);
    }, []);

    waitService.waitForMilliseconds(1000);
  });
  it("Add new port to add block", async () => {
    frame(0);

    click(
      "#stage > div.scroller > div > div.add.block > div > div.input > div.add"
    );

    setValue(
      '#stage > div.scroller > div > div.add.block > div > div.input > div:nth-child(2) > div > div > input[type="number"]',
      1
    );

    waitService.waitForMilliseconds(1000);
  });
  it("link get to add", async () => {
    frame(0);

    click(
      "#stage > div.scroller > div > div.get.number.block > div > div.output > div > b"
    );

    click(
      "#stage > div.scroller > div > div.add.block > div > div.input > div:nth-child(1) > b"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("link add to set", async () => {
    frame(0);

    link(
      "#stage > div.scroller > div > div.add.block > div > div.output > div > b",
      "#stage > div.scroller > div > div.set.number.block > div.data.sockets > div.input > div > b"
    );
  });
  it("Routine options", async () => {
    click("#stage > div.bLine.banner.mobile.interval > i");

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#stage > div.bLine.banner.mobile.interval > div > div:nth-child(2) > div > div",
      1,
      1
    );

    click(
      "#stage > div.bLine.banner.mobile.interval > div > div:nth-child(2) > div > div"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mls > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);
  });
  it("save program", async () => {
    frame(0);

    click("#panel > section.controls > div.icons > i.save.icon");
  });
  it("pause", async () => {
    waitService.waitForMilliseconds(15000);

    frame(null);
  });
  it("get value", async () => {
    frame(null);

    getText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(13) > a > div > div > span.num-indicator.normal",
      function(result) {
        expect(
          result.value < 5,
          "error, not fast enough value is only: " + result.value
        ).to.equal(false);
      }
    );

    waitService.waitForMilliseconds(4000);
  });
});
