import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let pointGraphicsButton = po.pointGraphicsButton;

let trendName = "trend" + new Date().valueOf();

describe("PointGraphics.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);
  });
  it("go to point graphics", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(pointGraphicsButton);

    waitService.waitForMilliseconds(1500);
  });
  it("pointGraphics options-Click on image", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#point-actions > div > div > div > button:nth-child(1)");

    elementPresent("#point-actions > div > div > div > button:nth-child(2)");

    elementPresent("#point-actions > div > div > div > button:nth-child(3)");

    elementPresent("#point-actions > div > div > div > button:nth-child(4)");

    elementPresent("#point-actions > div > div > div > button:nth-child(5)");

    elementPresent("#point-actions > div > div > div > button:nth-child(6)");
  });
  it("pointGraphics options-Click on emergency set", async () => {
    click("#point-actions > div > div > div > button:nth-child(1)");

    elementPresent(
      "#point-actions > div > div > header",
      "emergency set form is open"
    );

    click("#action-value");

    clearValue("#action-value");

    setValue("#action-value", "69");

    value("#action-value", "69", "value is 69");

    click("#point-actions > div > div > footer > button.rounded.focus");
  });
  it("pointGraphics options-Click on emergency Auto", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    waitService.waitForMilliseconds(500);

    click("#point-actions > div > div > div > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("pointGraphics options-Click on manual set", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    click("#point-actions > div > div > div > button:nth-child(3)");

    waitService.waitForMilliseconds(1000);

    click("#action-value");

    clearValue("#action-value");

    setValue("#action-value", "69");

    value("#action-value", "69", "value is 69");

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(1000);

    click("#duration-hours");

    clearValue("#duration-hours");

    setValue("#duration-hours", "10");

    value("#duration-hours", "10", "10 hours");

    waitService.waitForMilliseconds(1000);

    click("#duration-minutes");

    waitService.waitForMilliseconds(500);

    clearValue("#duration-minutes");

    waitService.waitForMilliseconds(500);

    setValue("#duration-minutes", "45");

    waitService.waitForMilliseconds(500);

    value("#duration-minutes", "45", "45 minutes");

    waitService.waitForMilliseconds(1000);

    click("#duration-seconds");

    waitService.waitForMilliseconds(500);

    clearValue("#duration-seconds");

    waitService.waitForMilliseconds(500);

    setValue("#duration-seconds", "50");

    waitService.waitForMilliseconds(500);

    value("#duration-seconds", "50", "50 seconds");

    waitService.waitForMilliseconds(500);

    click(
      "#point-actions > div > div > div > form > div:nth-child(3) > div:nth-child(2) > label.foreverToggleLabel"
    );

    waitService.waitForMilliseconds(500);

    click("#point-actions > div > div > footer > button.rounded.focus");

    waitService.waitForMilliseconds(500);
  });
  it("pointGraphics options-Click on manual auto", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    click("#point-actions > div > div > div > button:nth-child(4)");

    waitService.waitForMilliseconds(500);
  });
  it("pointGraphics options-Click on set default", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    click("#point-actions > div > div > div > button:nth-child(5)");

    click("#action-value");

    clearValue("#action-value");

    setValue("#action-value", "69.69");

    value("#action-value", "69.69", "default value is 69.69");

    waitService.waitForMilliseconds(500);

    click("#point-actions > div > div > footer > button.rounded.focus");

    waitService.waitForMilliseconds(500);
  });
  it("pointGraphics options-Click on set null", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    click("#point-actions > div > div > div > button:nth-child(6)");

    waitService.waitForMilliseconds(500);
  });
  it("pointGraphics chart and tooltip", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button"
    );

    waitService.waitForMilliseconds(500);

    elementPresent("#trend0 > div > div > svg", "chart shown");

    waitService.waitForMilliseconds(3000);

    moveToElement(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.padded.normal.all-sides.point-details.vertical.flex.non-auto > div > div > div > svg > g:nth-child(2) > path.amcharts-plot-area",
      40,
      40
    );

    waitService.waitForMilliseconds(7000000);

    elementPresent(".amcharts-balloon-div", "tooltip present");
  });
  it("pointGraphics chart-download", async () => {
    click(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list"
    );

    elementPresent(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list.open > ul > li:nth-child(1)",
      "csv option"
    );

    elementPresent(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list.open > ul > li:nth-child(2)",
      "json option"
    );

    elementPresent(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list.open > ul > li:nth-child(3)",
      "pdf option"
    );

    click(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list.open > ul > li:nth-child(1)"
    );

    elementPresent("body > a", "downloaded as CSV");

    click(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list"
    );

    click(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list.open > ul > li:nth-child(2)"
    );

    elementPresent("body > a", "downloaded as JSON");

    click(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list"
    );

    click(
      "#equipList > li:nth-child(1) > div.extra-details-box > div.horizontal.middle.center.dd-list.open > ul > li:nth-child(3)"
    );

    elementPresent("body > a", "downloaded as PDF");

    waitService.waitForMilliseconds(1000);
  });
  it("pointGraphics chart-magic bubbles", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.point-thumb.margin.normal.r-side.no-flex"
    );

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1)",
      5000
    );

    click(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1)"
    );

    waitForElementPresent(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.middle-button.flex.tall-text",
      5000,
      "History"
    );

    waitService.waitForMilliseconds(1000);
  });
});
