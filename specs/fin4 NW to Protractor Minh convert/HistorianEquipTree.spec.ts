import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let historianButton = po.historianButton;

let trendFromCampus = "Trend" + new Date().valueOf();

let trendFromCC = "TrendCC" + new Date().valueOf();

let trendFromFloor1 = "TrendFloor1" + new Date().valueOf();

let trendFromAhu1 = "ZTrendAhu" + new Date().valueOf();

let trendSAName = "Trend" + new Date().valueOf();

describe("HistorianEquipTree.js", () => {
  it("Login", async () => {
    waitService.waitForMilliseconds(1000);
  });
  it("go to Historian", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(historianButton);

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)",
      "historian is opened"
    );
  });
  it("trend from Campus", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("cogwheel option and save", async () => {
    waitService.waitForMilliseconds(3000);

    click("#barAppContainer > ul > li:nth-child(1) > div");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input"
    );

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input",
      trendFromCampus
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Go to saved ", async () => {
    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(5000);

    click("body > app > section > section > ul > li:nth-child(5) > div");

    waitService.waitForMilliseconds(1000);

    containsText(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)",
      trendFromCampus
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Navigate to floor 4 AHU-4 Check the trend does not appear on the incorrect level", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.front > ul > li:nth-child(3) > div.pna.horizontal.middle"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.front > ul > li:nth-child(3) > ul > li:nth-child(4) > div"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.front > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div.horizontal.flex.face.front > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(2)"
      ).getText(),
      trendFromCC
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Navigate  to CC ", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCampus
    );

    waitService.waitForMilliseconds(1000);
  });
  it("trend from CC", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("cogwheel option and save 1", async () => {
    waitService.waitForMilliseconds(5000);

    click("#fooAppContainer > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input"
    );

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input",
      trendFromCC
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Go to saved 1", async () => {
    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(5000);

    click("body > app > section > section > ul > li:nth-child(5) > div");

    waitService.waitForMilliseconds(1000);

    containsText(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)",
      trendFromCC
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Navigate  to Floor 1 ", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCampus
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCC
    );
  });
  it("trend from Floor 1", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("cogwheel option and save 2", async () => {
    waitService.waitForMilliseconds(5000);

    click("#barAppContainer > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input"
    );

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input",
      trendFromFloor1
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Go to saved 2", async () => {
    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(5000);

    click("body > app > section > section > ul > li:nth-child(5) > div");

    waitService.waitForMilliseconds(1000);

    containsText(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)",
      trendFromFloor1
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Navigate to AHU-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCampus
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCC
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromFloor1
    );

    waitService.waitForMilliseconds(1000);
  });
  it("trend from AHU1", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("cogwheel option and save 3", async () => {
    waitService.waitForMilliseconds(5000);

    click("#barAppContainer > ul > li:nth-first-child(1)");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input"
    );

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input",
      trendFromAhu1
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Go to saved 3", async () => {
    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(5000);

    click("body > app > section > section > ul > li:nth-child(5) > div");

    waitService.waitForMilliseconds(1000);

    containsText(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)",
      trendFromAhu1
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCampus
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromCC
    );

    notEqual(
      await $(
        "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-last-child(1)"
      ).getText(),
      trendFromFloor1
    );

    waitService.waitForMilliseconds(1000);

    end();
  });
});
