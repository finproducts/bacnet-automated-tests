import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-performanceTestLinks.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(10)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);
  });
  it("Add variables", async () => {
    frame(null);

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.textinput > input");

    keys("boolVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(100);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(100);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(100);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button");

    frame(0);

    waitService.waitForMilliseconds(6000);
  });
  it("Drag the number set block on stage", async () => {
    for (let n = 2; n <= 201; n++) {
      execute(dragAndDrop, [
        "#panel > section.variables > div.scroller > div.number.variable > b.number.set.block",
        ".blocks"
      ]);
    }

    waitService.waitForMilliseconds(1000);

    execute(
      function(simple) {
        let numberOfBlocks = app.get("blocks").length;

        for (let n = 1; n < numberOfBlocks; n++) {
          app.set('blocks."  +  n  +  ".x', 210 + n * 100),
            app.set('blocks."  +  n  +  ".y', 404 + n * 100);
        }
      },
      [],
      function(result) {}
    );
  });
  it("Linking blocks gate2gate", async () => {
    let lx;

    for (lx = 2; lx <= 100; lx++) {
      waitService.waitForMilliseconds(300);

      click(
        '#stage > div.scroller > div > div:nth-child("  +  lx  +  ") > div.exec.sockets > div.out > div > b'
      );

      waitService.waitForMilliseconds(300);

      click(
        '#stage > div.scroller > div > div:nth-child("  +  (lx + 1)  +  ") > div.exec.sockets > div.in > div > b'
      );
    }

    waitService.waitForMilliseconds(500);
  });
  it("Save Program", async () => {
    moveToElement("#panel > section.controls > div.icons > i.save.icon", 1, 1);

    waitService.waitForMilliseconds(500);

    click("#panel > section.controls > div.icons > i.save.icon");

    waitService.waitForMilliseconds(1000);
  });
  it("Switch Between Main And Alarm Repeatedly", async () => {
    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    click("#stage > div.tab.mobile > span:nth-child(3)");

    click("#stage > div.tab.mobile > span:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
});
