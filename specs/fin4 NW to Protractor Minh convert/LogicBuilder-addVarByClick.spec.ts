import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-addVarByClick.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(2)");
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(1000);

    keys(programName);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(3000);
  });
  it("Click on start, click on stage to launch add var form Point", async () => {
    frame(0);

    click("#stage > div.scroller > div > div > div > div > div > b");

    waitService.waitForMilliseconds(3000);

    click(".blocks");

    moveToElement(".blocks", 300, 300);

    waitService.waitForMilliseconds(1000);

    mouseButtonClick(0);

    frame(null);

    waitService.waitForMilliseconds(1000);

    moveToElement(
      "#content > div.form-item-holder.group > div > div:nth-child(2) > div > input",
      1,
      1
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(2) > div > input"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#content > div > select");

    click("#content > div > select > option:nth-child(3)");

    click("#controlBar > button:nth-child(2)");

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Click on start, click on stage to launch add var form Tag", async () => {
    frame(0);

    click("#stage > div.scroller > div > div > div > div > div > b");

    waitService.waitForMilliseconds(3000);

    click("body");

    moveToElement("body", 300, 300);

    mouseButtonClick(0);

    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(1) > div > input"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(2000);

    setValue("#content > div.form-item-holder.textinput > input", "tagNum");

    click("#controlBar > button:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Click on start, click on stage to launch add var form var", async () => {
    frame(0);

    click("#stage > div.scroller > div > div > div > div > div > b");

    waitService.waitForMilliseconds(1000);

    click("body");

    moveToElement("body", 340, 400);

    mouseButtonClick(0);

    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(3) > div > input"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    moveToElement("#content > div.form-item-holder.textinput > input", 1, 1);

    setValue("#content > div.form-item-holder.textinput > input", "numVar");

    click("#content > div:nth-child(2) > div > input");

    click("#content > div:nth-child(3) > div > input");

    click("#content > div:nth-child(4) > div > input");

    click("#content > div.form-item-holder.combobox > select");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
});
