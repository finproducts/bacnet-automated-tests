import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let pointGraphicsButton = po.pointGraphicsButton;

let trendName = "trend" + new Date().valueOf();

let queryNUM =
  'readAll(point and  kind == "Number" and equipRef==@1eeaf2cd-f2e9b66d).sort((a,b) => if(a->navName > b->navName) 1 else -1)[0..0]';

let OK = "TEST PASS";

let error = "ERROR!";

let input = "" + createRandomNumber();

function createRandomNumber() {
  var min = 0;
  var max = 100;
  var getRandomNumber = Math.floor(Math.random() * (max - min) + min);
  return getRandomNumber;
}

describe("PointGraphics-setNum-Folio.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to point graphics", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(pointGraphicsButton);

    waitService.waitForMilliseconds(1500);
  });
  it("pointGraphics options-Click on image", async () => {
    click(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#point-actions > div > div > div > button:nth-child(1)");

    elementPresent("#point-actions > div > div > div > button:nth-child(2)");

    elementPresent("#point-actions > div > div > div > button:nth-child(3)");

    elementPresent("#point-actions > div > div > div > button:nth-child(4)");

    elementPresent("#point-actions > div > div > div > button:nth-child(5)");

    elementPresent("#point-actions > div > div > div > button:nth-child(6)");
  });
  it("pointGraphics options-Click on emergency set", async () => {
    click("#point-actions > div > div > div > button:nth-child(1)");

    elementPresent(
      "#point-actions > div > div > header",
      "emergency set form is open"
    );

    click("#action-value");

    clearValue("#action-value");

    setValue("#action-value", input);

    value("#action-value", input);

    click("#point-actions > div > div > footer > button.rounded.focus");

    console.log(input);
  });
  it("QueryCheck", async () => {
    finEval(queryNUM, function(output) {
      console.log(output);

      if (
        output.length > 0 &&
        output[0].cmd == "✓" &&
        output[0].navName == "CHWV" &&
        output[0].kind == "Number" &&
        output[0].point == "✓" &&
        output[0].unit == "%" &&
        output[0].writable == "✓" &&
        output[0].curVal == input &&
        output[0].writeVal == input &&
        output[0].curVal == output[0].writeVal
      ) {
        console.log(OK);
      } else {
        console.log(error);
      }
    });

    waitService.waitForMilliseconds(1000);
  });
});
