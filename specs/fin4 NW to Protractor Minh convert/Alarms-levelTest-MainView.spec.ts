import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectors =
  "#innerApp > section > ul > li:nth-child(1n+0) > div.horizontal.middle.margin.small.v-sides > h1";

let selectorLibrary = require("./po.js");

let alarmsButton = po.alarmsButton;

describe("Alarms-levelTest-MainView.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);
  });
  it("go to Alarms", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(alarmsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("Switch to main view", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");

    waitService.waitForMilliseconds(2999);

    frame(null);
  });
  it("navigation to CC", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1900);
  });
  it("verify only CC", async () => {
    frame(0);

    containsText(selectors, "City Center");

    notContainsText(selectors, "JB");

    notContainsText(selectors, "Laguna");

    notContainsText(selectors, "Malibu");

    notContainsText(selectors, "Pearl");

    waitService.waitForMilliseconds(1900);
  });
  it("Switch to main view 1", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");

    waitService.waitForMilliseconds(2999);

    frame(null);
  });
  it("navigation to CC >> floor 1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1900);
  });
  it("verify CC-Floor 1", async () => {
    frame(0);

    containsText(selectors, "City Center");

    notContainsText(selectors, "JB");

    notContainsText(selectors, "City Center Vav-11");

    notContainsText(selectors, "Laguna");

    notContainsText(selectors, "Malibu");

    notContainsText(selectors, "Pearl");

    waitService.waitForMilliseconds(1900);
  });
  it("Switch to main view 2", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");

    waitService.waitForMilliseconds(2999);

    frame(null);
  });
  it("navigation to CC >> floor 1 >> VAV-01", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1900);
  });
  it("Switch to main view 3", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");

    waitService.waitForMilliseconds(2999);

    frame(null);
  });
  it("verify CC-Floor 1-VAV-01", async () => {
    frame(0);

    waitService.waitForMilliseconds(1000);

    containsText(
      "#innerApp > section > ul > li > div.horizontal.middle.margin.small.v-sides > h1",
      "City Center Vav-01"
    );

    notContainsText(
      "#innerApp > section > ul > li > div.horizontal.middle.margin.small.v-sides > h1",
      "City Center Vav-02"
    );

    waitService.waitForMilliseconds(1900);
  });
});
