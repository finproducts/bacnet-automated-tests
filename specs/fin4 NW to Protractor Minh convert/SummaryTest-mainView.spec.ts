import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let summaryButton = po.summaryButton;

describe("SummaryTest-mainView.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to Summary", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(summaryButton);

    waitService.waitForMilliseconds(1500);
  });
  it("Main View", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");
  });
  it("Summary menu", async () => {
    elementPresent("#innerApp > header", "summary app is open");

    click("#innerApp > section > ul > li > div");

    waitService.waitForMilliseconds(2000);

    click("#fooAppContainer > header > button:nth-child(1) > span");

    waitService.waitForMilliseconds(2000);

    elementPresent("#innerApp > header > h4", "Opened summary app");

    waitService.waitForMilliseconds(500);
  });
  it("summary expand", async () => {
    click("#innerApp > header > button:nth-child(3)");

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent("#innerApp > header > h4");

    waitService.waitForMilliseconds(1000);
  });
  it("click on the prompt button-4 square thingy=must find a better name", async () => {
    click("#innerApp > section > ul > li > div > button");

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > h6",
      "Opened the form"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.vertical.middle.center.non-modal > div > footer > button:nth-child(4)"
    );

    elementNotPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > h6",
      "form is closed"
    );

    frame(null);

    waitService.waitForMilliseconds(1000);

    click("#innerApp > section > ul > li > div > button");

    waitService.waitForMilliseconds(500);
  });
  it("summary form", async () => {
    frame(null);

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(1)",
      "column properties is there"
    );

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(2)",
      "compare button"
    );

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > footer > button.button-default.margin.small.r-side.horizontal.middle",
      "live button is present"
    );

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > footer > button.button-default.margin.small.l-side.horizontal.middle",
      "export button"
    );

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > h6",
      "closed"
    );

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(6)",
      "min"
    );

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(7)",
      "max"
    );

    waitService.waitForMilliseconds(500);
  });
  it("summary min-back-max", async () => {
    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(6)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > button",
      "summmary is expanded again"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(7)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(7) > span",
      "contract"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(7)"
    );

    elementPresent("#innerApp > section > ul > li > div", "contracted");

    waitService.waitForMilliseconds(1500);
  });
  it("filter-show", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(1)"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button.link-button.dd-element.horizontal.middle.open > div > section > ul > li:nth-child(1) > button.pill.margin.normal.h-sides.selected"
    );

    waitService.waitForMilliseconds(2000);

    cssClassNotPresent(
      "body > app > div.vertical.middle.center.non-modal > div > div.vertical.flex.non-auto.grid-container > table > tbody > tr:nth-child(6) > td:nth-child(1)",
      "button-default alt",
      "name button is hidden"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button.link-button.dd-element.horizontal.middle.open > div > section > footer > button"
    );
  });
  it("summary export", async () => {
    click(
      "body > app > div.vertical.middle.center.non-modal > div > footer > button.button-default.margin.small.r-side.horizontal.middle"
    );
  });
  it("summary live", async () => {
    click(
      "body > app > div.vertical.middle.center.non-modal > div > footer > button.button-default.margin.small.l-side.horizontal.middle"
    );

    waitService.waitForMilliseconds(200);

    elementNotPresent(
      "body > app > div.vertical.middle.center.non-modal > div > footer > button.button-default.margin.small.l-side.horizontal.middle.focused.toggled",
      "live values"
    );

    waitService.waitForMilliseconds(500);
  });
  it("summary window", async () => {
    click(
      "body > app > div.vertical.middle.center.non-modal > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal > div > div.root.action.button.vertical.middle.center",
      "magic buttons"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > div > table > thead"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);
  });
  it("Summary compare", async () => {
    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button.link-button.dd-element.horizontal.middle.open > div > section > ul > li:nth-child(1) > button"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button.link-button.dd-element.horizontal.middle.open > div > section > ul > li:nth-child(3) > button"
    );

    click(
      "body > app > div.vertical.middle.center.non-modal > div > header > button.link-button.dd-element.horizontal.middle.open > div > section > footer > button"
    );

    getText(
      "body > app > div.vertical.middle.center.non-modal > div > div.vertical.flex.non-auto.grid-container > table > tbody > tr:nth-child(1) > td.flex.non-auto.horizontal.middle > span",
      function(result) {
        console.log(result.value);

        containsText(
          "body > app > div.vertical.middle.center.non-modal > div > div.vertical.flex.non-auto.grid-container > table > tbody > tr:nth-child(1) > td.flex.non-auto.horizontal.middle > span",
          result.value
        );
      }
    );
  });
  it("magicbuttons", async () => {
    elementPresent(
      "body > app > div.modal > div > div.root.action.button.vertical.middle.center"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1)",
      "cc1"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2)",
      "Notes"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(3)",
      "Normal Work Week-schedule"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(4)",
      "Point Graphics"
    );

    elementPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(5)",
      "Cafe lounge"
    );
  });
});
