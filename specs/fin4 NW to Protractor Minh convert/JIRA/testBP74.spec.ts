import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp74bLine = require("../../fixtures/programs/BP-74bLine");

let bp74KMC = require("../../fixtures/programs/BP-74KMC");

describe("testBP74.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Load KMC program for BP-74", async () => {
    loadProgram(bp74KMC);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#bf635d400-e4b9-47a1-bca9-dd3c03cd1a84",
      "Check that the variable block is present"
    );

    elementPresent(
      "#b83d9e8bc-73c4-4b83-9fca-6c2e055fe4f3",
      "Check that the integer variable block is present"
    );

    elementPresent(
      "#b9c6be4dc-8dae-42fd-9236-3fcab640c997",
      "Check that the real variable block is present"
    );

    elementPresent(
      "#b5ef8cb00-0e6d-4437-abc3-f9b2c4ad5b3e",
      "Check that the string variable block is present"
    );
  });
  it("Check inputs for KMC variables", async () => {
    elementPresent(
      "#bf635d400-e4b9-47a1-bca9-dd3c03cd1a84 > div.data.sockets > div > div > input",
      "Check if the input for variable is present"
    );

    elementPresent(
      '#b83d9e8bc-73c4-4b83-9fca-6c2e055fe4f3 > div.data.sockets > div > div > div > input[type="number"]',
      "Check if the input for integer variable is present"
    );

    elementPresent(
      '#b9c6be4dc-8dae-42fd-9236-3fcab640c997 > div.data.sockets > div > div > div > input[type="number"]',
      "Check if the input for real variable is present"
    );

    elementPresent(
      "#b5ef8cb00-0e6d-4437-abc3-f9b2c4ad5b3e > div.data.sockets > div > div > input",
      "Check if the input for string variable is present"
    );
  });
  it("Load bLine program for BP-74", async () => {
    loadProgram(bp74bLine);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#b888c358f-ae7d-4b38-9b61-d87fb86baad9",
      "Check that the boolean variable block is present"
    );

    elementPresent(
      "#bdadaeead-cf6b-47c6-9509-c767da9241f9",
      "Check that the number variable block is present"
    );

    elementPresent(
      "#ba65b65fb-1b65-42fe-a263-98accd6f4cef",
      "Check that the string variable block is present"
    );
  });
  it("Check inputs for bLine variables", async () => {
    elementPresent(
      "#b888c358f-ae7d-4b38-9b61-d87fb86baad9 > div.data.sockets > div > div > label",
      "Check if the input (checkbox) for boolean variable is present"
    );

    elementPresent(
      "#ba65b65fb-1b65-42fe-a263-98accd6f4cef > div.data.sockets > div > div > input",
      "Check if the input for string variable is present"
    );
  });
});
