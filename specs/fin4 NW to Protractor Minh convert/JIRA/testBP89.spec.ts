import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

describe("testBP89.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Verifying bug BP-89", async () => {
    refresh();

    waitService.waitForMilliseconds(500);

    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click("#panel > section.controls > div.icons > i.export.icon");

    waitService.waitForMilliseconds(200);

    click(
      "body > app > div.export.modal > div > div > div > div > div.CodeMirror-scroll > div.CodeMirror-sizer"
    );

    keys("test");

    setValue(
      "body > app > div.export.modal > div > div > div > div > div.CodeMirror-scroll > div.CodeMirror-sizer",
      "teeest"
    );

    click("body > app > div.export.modal > div > div.close > i");

    waitService.waitForMilliseconds(500);

    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    elementPresent("#panel > section.variables > div.scroller > div");

    waitService.waitForMilliseconds(500);
  });
});
