import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP157.js", () => {
  it("open a bLine program centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      console.log(result.value);

      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      console.log(result.value);

      console.log(result.value + " > ul > li:nth-child(2)");

      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarm, [], function(result) {
      console.log(result.value);

      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarmSubMenu, [], function(result) {
      console.log(result.value);

      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .blocks");
  });
  it("add macro block 1", async () => {
    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(200);

    elementPresent(".variables .macro");
  });
  it("enter macro block 1", async () => {
    click(".variables .macro .name");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller div:nth-child(3)", "viewing");

    elementNotPresent("b.macro.block");
  });
  it("delete port from macro block 1", async () => {
    click(".bLine.banner.macro i.cog.icon");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro table tbody tr:nth-child(1) td:nth-child(6) i");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro table tbody tr:nth-child(1) td:nth-child(6) i");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro table tbody tr:nth-child(1) td:nth-child(6) i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(".bLine.banner.macro table tbody tr:nth-child(1)");
  });
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Change language to bLine", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");
  });
  it("add macro block 2", async () => {
    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(200);

    elementPresent(".variables .macro");
  });
  it("enter macro block 2", async () => {
    click(".variables .macro .name");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller div:nth-child(3)", "viewing");

    elementNotPresent("b.macro.block");
  });
  it("delete port from macro block 2", async () => {
    click(".bLine.banner.macro i.cog.icon");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro table tbody tr:nth-child(1) td:nth-child(6) i");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro table tbody tr:nth-child(1) td:nth-child(6) i");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro table tbody tr:nth-child(1) td:nth-child(6) i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(".bLine.banner.macro table tbody tr:nth-child(1)");
  });
});
