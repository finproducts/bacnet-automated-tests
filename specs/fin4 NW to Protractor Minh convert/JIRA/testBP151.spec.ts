import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

describe("testBP151.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(5000);

    waitForElementPresent(".blocks", 1000);
  });
  it("add function in JavaScript", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(1)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "JavaScript");

    click(".add.panel-button .function");

    waitService.waitForMilliseconds(500);
  });
  it("delete function in JavaScript", async () => {
    moveToElement(".variables .function .name", 2, 2);

    click(".cross.icon");

    waitService.waitForMilliseconds(500);

    elementNotPresent(".variables.function");
  });
  it("add function in KMC", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(2)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "KMC");

    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(500);
  });
  it("delete function in KMC", async () => {
    moveToElement(".variables .function .name", 2, 2);

    click(".cross.icon");

    waitService.waitForMilliseconds(500);

    elementNotPresent(".variables.function");
  });
  it("add routine in bLine", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");

    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(500);
  });
  it("delete function in bLine", async () => {
    moveToElement(".variables .function:nth-child(3) .name", 2, 2);

    click(".cross.icon");

    waitService.waitForMilliseconds(500);

    elementNotPresent(".variables.function:nth-child(3");
  });
});
