import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP100.js", () => {
  it("open a bLine program centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(6000);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(200000);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(
        result.value +
          "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(2)"
      );

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarm, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarmSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(5000000);

    elementPresent(".scroller .blocks");
  });
  it("modify size of section", async () => {
    getCssProperty(".blocklibrary", "height", function(result) {
      moveToElement(".blocklibrary header", 2, 2);

      mouseButtonDown(0);

      cssClassPresent(".blocklibrary", "dragging");

      moveTo(null, 100, 100);

      waitService.waitForMilliseconds(500);

      mouseButtonUp(0);

      element(".blocklibrary");

      to;

      have;

      css("height");

      which;

      does;

      not;

      equal(result.value);

      waitService.waitForMilliseconds(200000);
    });
  });
});
