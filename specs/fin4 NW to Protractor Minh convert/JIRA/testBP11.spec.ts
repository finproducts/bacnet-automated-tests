import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp11= require('../../fixtures/programs/BP-11');

let bp245KMC= require('../../fixtures/programs/BP-245KMC');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP11.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Verifying bug BP-11', async () => 
    
    {
loadProgram(bp11);

waitService.waitForMilliseconds(500);

moveToElement('#b10660aea-528e-45d4-9d36-39d81bf5a778 > div > div > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementPresent('body > app > ul');

containsText('body > app > ul > li','Delete link to denumire');

moveToElement('#b6e48997f-b8fe-427e-8f00-0ac4f2c3c9fb > div.exec.sockets > div.in > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementPresent('body > app > ul');

containsText('body > app > ul > li','Delete link to denumire next');

moveToElement('#bfd676f80-e707-4a14-80fb-01e77b638450 > div.exec.sockets > div.out > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementPresent('body > app > ul');

containsText('body > app > ul > li','Delete link to oFunctie');

moveToElement('#b6e48997f-b8fe-427e-8f00-0ac4f2c3c9fb > div.exec.sockets > div.out > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementPresent('body > app > ul');

containsText('body > app > ul > li','Delete link to codulet input 1');

moveToElement('#bd59b3fc7-6a28-4549-b957-f71f99ee7330 > div > div.in > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementPresent('body > app > ul');

containsText('body > app > ul > li','Delete link to codulet next');

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});