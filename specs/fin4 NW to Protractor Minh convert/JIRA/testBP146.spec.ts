import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

describe("testBP146.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("add variable function and macro block in JavaScript", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(1)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "JavaScript");

    click(".add.panel-button .function");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);
  });
  it("delete characters for function on JavaScript", async () => {
    moveToElement(".variables .function .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .function .name").getText(), "unnamedFunctio");
  });
  it("rename function on JavaScript", async () => {
    moveToElement(".variables .function .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .function .name").getText(),
      "unnamedFunctioTest"
    );
  });
  it("delete characters for variable on JavaScript", async () => {
    moveToElement(".variables .variable .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .variable .name").getText(), "unnamedVariabl");
  });
  it("rename variable on JavaScript", async () => {
    moveToElement(".variables .variable .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .variable .name").getText(),
      "unnamedVariablTest"
    );
  });
  it("delete characters for macro block on JavaScript", async () => {
    moveToElement(".variables .macro .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .macro .name").getText(), "unnamedMacr");
  });
  it("rename macro block on JavaScript", async () => {
    moveToElement(".variables .macro .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .macro .name").getText(), "unnamedMacrTest");
  });
  it("add variable function and macro block in KMC", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(2)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "KMC");

    click(".add.panel-button .function");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);
  });
  it("delete characters for function on KMC", async () => {
    moveToElement(".variables .function .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .function .name").getText(), "unnamedFunctio");
  });
  it("rename function on KMC", async () => {
    moveToElement(".variables .function .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .function .name").getText(),
      "unnamedFunctioTest"
    );
  });
  it("delete characters for variable on KMC", async () => {
    moveToElement(".variables .variable .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .variable .name").getText(), "unnamedVariabl");
  });
  it("rename variable on KMC", async () => {
    moveToElement(".variables .variable .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .variable .name").getText(),
      "unnamedVariablTest"
    );
  });
  it("delete characters for macro block on KMC", async () => {
    moveToElement(".variables .macro .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .macro .name").getText(), "unnamedMacr");
  });
  it("rename macro block on KMC", async () => {
    moveToElement(".variables .macro .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .macro .name").getText(), "unnamedMacrTest");
  });
  it("add variable function and macro block in bLine", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");

    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);
  });
  it("delete characters for routine on bLine", async () => {
    moveToElement(".variables .function:nth-child(3) .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .function:nth-child(3) .name").getText(),
      "unnamedRoutin"
    );
  });
  it("rename routine on bLine", async () => {
    moveToElement(".variables .function:nth-child(3) .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .function:nth-child(3) .name").getText(),
      "unnamedRoutinTest"
    );
  });
  it("delete characters for variable on bLine", async () => {
    moveToElement(".variables .variable .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .variable .name").getText(), "unnamedVariabl");
  });
  it("rename variable on bLine", async () => {
    moveToElement(".variables .variable .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(
      await $(".variables .variable .name").getText(),
      "unnamedVariablTest"
    );
  });
  it("delete characters for macro block on bLine", async () => {
    moveToElement(".variables .macro .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, BACK_SPACE);

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .macro .name").getText(), "unnamedMacr");
  });
  it("rename macro block on bLine", async () => {
    moveToElement(".variables .macro .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    equal(await $(".variables .macro .name").getText(), "unnamedMacrTest");
  });
});
