import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let chillerAlarm = require("../../fixtures/execute/logicBuilder.js")
  .chillerAlarm;

let chillerAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .chillerAlarmSubMenu;

let booleanBlock = require("../../fixtures/execute/logicBuilder.js")
  .booleanBlock;

describe("testBP155.js", () => {
  it("open chillerAlarm", async () => {
    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(chillerAlarm, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(chillerAlarmSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(3000);

      click(
        "body > app > section > div > div.frame-content > section > button"
      );

      waitService.waitForMilliseconds(500);

      frame(0);

      waitService.waitForMilliseconds(500);

      containsText(
        "#panel > section.controls > div.program-name > span:nth-child(1)",
        "chillerAlarm",
        "Chiller Alarm program is open"
      );
    });
  });
  it("Check boolean blocks for checkboxes", async () => {
    getText(".variables > div > div:nth-child(2) > span > span", function(
      result
    ) {
      if (result.value == "main") {
        click(".variables > div > div:nth-child(2) > span > span");

        waitService.waitForMilliseconds(1500);
      } else {
        containsText(
          ".variables > div > div:nth-child(3) > span > span",
          "main",
          "Main routine is second"
        );

        click(".variables > div > div:nth-child(3) > span > span");

        waitService.waitForMilliseconds(1500);
      }
    });

    execute(booleanBlock, [], function(result) {
      console.log(result.value);

      result.value.forEach(function(sel) {
        elementPresent(sel + " > div.data.sockets > div.input > div > label");
      });
    });
  });
});
