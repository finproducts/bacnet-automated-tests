import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let test= require('../../fixtures/execute/logicBuilder.js').test;

let testSubMenu= require('../../fixtures/execute/logicBuilder.js').testSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;

let addVariables= require('../../fixtures/execute/programData.js').addVariables;

let addVariablesSubMenu= require('../../fixtures/execute/programData.js').addVariablesSubMenu;



describe('testBP256.js',() =>{
  
  it('go to City Center AHU-1 ', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center','Browsed to City Center');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','Floor 1','Browsed to Floor 1');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center AHU-1','Browsed to City Center AHU-1');
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);
}

    
  );  
  it('open create bLine program option', async () => 
    
    {
execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1) > label");

waitService.waitForMilliseconds(1000);
}
);

elementPresent('.form-window.modal-panel.generic-panel','Check the window to create new programs is on screen');

containsText('.form-window.modal-panel.generic-panel #title','Create bLine Program',''Create bLine Program' has the correct title');
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys('test');

waitService.waitForMilliseconds(300);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#title','Success','Check that the program was successfully created');

click('#controlBar > button');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add number variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','number');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(1)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

setValue('#content > div.form-item-holder.numericstepper > input','1');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(12) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','number','Check that the 'number' variable is added');
}

    
  );  
  it('Add boolean variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','boolean');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(12) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','bool','Check that the 'boolean' variable is added');
}

    
  );  
  it('Add string variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','string');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(3)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

setValue('#content > div.form-item-holder.textinput > input','random');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(14) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','string','Check that the 'string' variable is added');
}

    
  );  
  it('Add date variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','date');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(4)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(13) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','date','Check that the 'date' variable is added');
}

    
  );  
  it('Add time variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','time');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(5)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(16) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','time','Check that the 'date' variable is added');
}

    
  );  
  it('Add dateTime variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','dateTime');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(6)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Open program', async () => 
    
    {
execute(edit,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(2500);
}
);

frame(0);

waitService.waitForMilliseconds(500);

containsText('#panel > section.variables > div.scroller > div.function.viewing > span > span','main','Check that the main routine is first and open');

containsText('#panel > section.variables > div.scroller > div:nth-child(2) > span > span','alarm','Check that the alarm routine is second and not open');
}

    
  );  
  it('Drag number setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .number.variable b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block','Number setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',238);

app.set('blocks.1.y',174);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag number getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .number.variable b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block','Number getter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',479);

app.set('blocks.2.y',202);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag boolean setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .boolean.variable  b.boolean.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.boolean.block','Boolean setter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',238);

app.set('blocks.3.y',295);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag boolean getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .boolean.variable b.boolean.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.boolean.block','Boolean getter block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',479);

app.set('blocks.4.y',273);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag string setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .string.variable b.string.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.string.block','String setter block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',238);

app.set('blocks.5.y',401);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag string getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .string.variable b.string.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.string.block','String getter block is on stage');

execute(function(simple)
{
app.set('blocks.6.x',479);

app.set('blocks.6.y',370);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag date setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .date.variable b.date.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.date.block','Date setter block is on stage');

execute(function(simple)
{
app.set('blocks.7.x',238);

app.set('blocks.7.y',507);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag date getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .date.variable b.date.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.date.block','Date getter block is on stage');

execute(function(simple)
{
app.set('blocks.8.x',479);

app.set('blocks.8.y',464);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag time setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .time.variable  b.time.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.time.block','Time setter block is on stage');

execute(function(simple)
{
app.set('blocks.9.x',238);

app.set('blocks.9.y',605);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag time getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .time.variable b.time.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.time.block','Time getter block is on stage');

execute(function(simple)
{
app.set('blocks.10.x',479);

app.set('blocks.10.y',544);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag dateTime setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .datetime.variable b.datetime.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.datetime.block','DateTime setter block is on stage');

execute(function(simple)
{
app.set('blocks.11.x',238);

app.set('blocks.11.y',752);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag dateTime getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .datetime.variable b.datetime.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.datetime.block','DateTime getter block is on stage');

execute(function(simple)
{
app.set('blocks.12.x',479);

app.set('blocks.12.y',631);
}
,[],function(result)
{}
);
}

    
  );  
  it('Check link suggestion for start block', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for number setter block INPUT', async () => 
    
    {
click('.blocks .set.number.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for number setter block OUTPUT', async () => 
    
    {
click('.blocks .set.number.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for number getter data port', async () => 
    
    {
click('.blocks .get.number.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','MATH BLOCKS','Math Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','ABS','ABS block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','ABS block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','+','Add block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Add block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','AVG','AVG block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','AVG block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','÷','DIVIDE block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','DIVIDE block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','ISEVEN','IsEven block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','IsEven block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','ISODD','IsOdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','IsOdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.label-item','LIMIT','Limit block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.icon-media-stop.item-icon','Limit block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','MAX','MAX block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','MAX block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.label-item','MIN','MIN block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.icon-media-stop.item-icon','MIN block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','MODULO','Modulo block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','Modulo block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','×','Multiply block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','Multiply block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.label-item','NEG','NEG block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.icon-media-stop.item-icon','NEG block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RANDOM','Random block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Random block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','RESET','Reset block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Reset block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.label-item','SINEWAVE','SineWave block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.icon-media-stop.item-icon','SineWave block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','-','Subtract block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.icon-media-stop.item-icon','Subtract block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','THRESHOLD','Threshold block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.icon-media-stop.item-icon','Threshold block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','BOOLLATCH','BoolLatch block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.icon-media-stop.item-icon','BoolLatch block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','DELAYEDOUTPUT','DelayedOutput block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.icon-media-stop.item-icon','DelayedOutput block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','STAGING','Staging block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.icon-media-stop.item-icon','Staging block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(23) > div > span','TIME BLOCKS','Time Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(24) > div > span.label-item','DATETIMEADD','DateTimeAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(24) > div > span.icon-media-stop.item-icon','DateTimeAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(25) > div > span.label-item','DATETIMESUB','DateTimeSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(25) > div > span.icon-media-stop.item-icon','DateTimeSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(26) > div > span','LOGICAL BLOCKS','Logical Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(27) > div > span.label-item','INRANGE','InRange block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(27) > div > span.icon-media-stop.item-icon','InRange block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(28) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(29) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(29) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(30) > div > span.label-item','PID','Pid block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(30) > div > span.icon-media-stop.item-icon','Pid block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(31) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(32) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(32) > div > span.wi-moon-full.item-icon','Number block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for boolean setter block INPUT', async () => 
    
    {
click('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for boolean setter block OUTPUT', async () => 
    
    {
click('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for boolean getter data port', async () => 
    
    {
click('.blocks .get.boolean.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','LOGICAL BLOCKS','Logical Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','ALL','All block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','All block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','AND','And block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','And block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','ANY','Any block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Any block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','NOT','Not block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Not block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','OR','Or block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','Or block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','XOR','XOr block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','XOr block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','BOOLLATCH','BoolLatch block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','BoolLatch block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.label-item','DELAYEDOUTPUT','DelayedOutput block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.icon-media-stop.item-icon','DelayedOutput block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','STAGING','Staging block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','Staging block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.label-item','CONDDELAY','CondDelay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.icon-media-stop.item-icon','CondDelay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','COUNT','Count block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Count block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.label-item','PID','Pid block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.icon-media-stop.item-icon','Pid block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span','MATH BLOCKS','Math Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','THRESHOLD','Threshold block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.icon-media-stop.item-icon','Threshold block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for string setter block INPUT', async () => 
    
    {
click('.blocks .set.string.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for string setter block OUTPUT', async () => 
    
    {
click('.blocks .set.string.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for string getter data port', async () => 
    
    {
click('.blocks .get.string.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span','STRINGS BLOCKS','Strings Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','STRCAPITALIZE','StrCapitalize block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','StrCapitalize block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','STRCONCAT','StrConcat block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','StrConcat block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.label-item','STRCONTAINS','StrContains block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.icon-media-stop.item-icon','StrContains block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','STRDECAPITALIZE','StrDeCapitalize block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','StrDeCapitalize block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.label-item','STRENDSWITH','StrEndsWith block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.icon-media-stop.item-icon','StrEndsWith block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','STRFORMAT','StrFormat block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','StrFormat block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','STRLOWER','StrLower block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','StrLower block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.label-item','STRMACRO','StrMacro block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.icon-media-stop.item-icon','StrMacro block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','STRREPLACE','StrReplace block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','StrReplace block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','STRSIZE','StrSize block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','StrSize block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.label-item','STRSTARTSWITH','StrStartsWith block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.icon-media-stop.item-icon','StrStartsWith block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','STRUPPER','StrUpper block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.icon-media-stop.item-icon','StrUpper block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for date setter block INPUT', async () => 
    
    {
click('.blocks .set.date.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for date setter block OUTPUT', async () => 
    
    {
click('.blocks .set.date.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for date getter data port', async () => 
    
    {
click('.blocks .get.date.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','TIME BLOCKS','Time Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Date block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','DATETIMEADD','DateTimeAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','DateTimeAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','DATETIMESUB','DateTimeSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','DateTimeSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','DATETONUMBER','DateToNumber block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','DateToNumber block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','DATETOSTRING','DateToString block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','DateToString block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','DTTODTADD','DateToDateAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','DateToDateAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.label-item','DTTODTSUB','DateToDateSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.icon-media-stop.item-icon','DateToDateSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','Time block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.wi-moon-full.item-icon','Date block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for time setter block INPUT', async () => 
    
    {
click('.blocks .set.time.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for time setter block OUTPUT', async () => 
    
    {
click('.blocks .set.time.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for time getter data port', async () => 
    
    {
click('.blocks .get.time.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','TIME BLOCKS','Time Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','TIMETOMINUTES','TimeToMinutes block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','TimeToMinutes block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','TIMETOTIMEADD','TimeToTimeAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','TimeToTimeAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','TIMETOTIMESUB','TimeToTimeSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','TimeToTimeSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.wi-moon-full.item-icon','Time block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for datetime setter block INPUT', async () => 
    
    {
execute('scrollTo(0,800)');

click('.blocks .set.datetime.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for datetime setter block OUTPUT', async () => 
    
    {
click('.blocks .set.datetime.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','COMMANDS BLOCKS','Commands Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','EMAIL','Email block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Email block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','SETDELAY','Set Delay block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Set Delay block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','SETEXTERNAL','Set External block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','Set External block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','WAIT','Wait block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','Wait block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span','CONTROL FLOW BLOCKS','Control Flow Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','IF','If block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','If block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','JOBRUN','JobRun block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','JobRun block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','LEADLAGSTAGESELECT','LeadLagStageSelect block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','LeadLagStageSelect block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span','BLOCKS','Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RETURN','Return block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Return block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','SET','Set block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Set block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.wi-moon-full.item-icon','Number block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','BOOLEAN','Boolean block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.wi-moon-full.item-icon','Boolean block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','STRING','String block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.wi-moon-full.item-icon','String block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.wi-moon-full.item-icon','Date block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.wi-moon-full.item-icon','Time block has a point icon');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for datetime getter data port', async () => 
    
    {
click('.blocks .get.datetime.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','TIME BLOCKS','Time Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','DATE','Date block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','Date block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','DATETIMEADD','DateTimeAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','DateTimeAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','DATETIMESUB','DateTimeSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','DateTimeSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','DATETONUMBER','DateToNumber block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','DateToNumber block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','DATETOSTRING','DateToString block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','DateToString block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','DTTODTADD','DateToDateAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','DateToDateAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.label-item','DTTODTSUB','DateToDateSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.icon-media-stop.item-icon','DateToDateSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','TIME','Time block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','Time block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','DATETIME','DateTime block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.wi-moon-full.item-icon','DateTime block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for add block input', async () => 
    
    {
waitService.waitForMilliseconds(500);

setValue('.blocklibrary .filters > div > input[type="text"]','add');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary .scroller > div:nth-child(2) > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .add.block','Add block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',435);

app.set('blocks.1.y',56);
}
,[],function(result)
{}
);

click('.blocks .add.block > div > div.input > div.socket.no-select.number.or.gridcol.obj > b');

waitService.waitForMilliseconds(400);

moveTo(null,-20,-20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','MATH BLOCKS','Math Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','ABS','ABS block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','ABS block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','+','Add block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Add block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','AVG','AVG block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','AVG block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','÷','DIVIDE block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','DIVIDE block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','LIMIT','Limit block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','Limit block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','MAX','Max block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','Max block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.label-item','MIN','Min block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.icon-media-stop.item-icon','Min block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','MODULO','Modulo block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','Modulo block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.label-item','×','Multiply block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.icon-media-stop.item-icon','Multiply block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','NEG','Neg block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','Neg block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','RANDOM','Random block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','Random block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.label-item','RESET','Reset block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.icon-media-stop.item-icon','Reset block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','SINEWAVE','Sinewave block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Sinewave block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','-','Subtract block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Subtract block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','COUNT','Count block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.icon-media-stop.item-icon','Count block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','GRIDSELECTCOL','GridSelectCol block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.icon-media-stop.item-icon','GridSelectCol block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.label-item','PID','Pid block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span.icon-media-stop.item-icon','Pid block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span','TIME BLOCKS','Time Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','DATETONUMBER','DateToNumber block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.icon-media-stop.item-icon','DateToNumber block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','DTTODTADD','DateToDateAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.icon-media-stop.item-icon','DateToDateAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(23) > div > span.label-item','DTTODTSUB','DateToDateSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(23) > div > span.icon-media-stop.item-icon','DateToDateSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(24) > div > span.label-item','TIMETOMINUTES','TimeToMinutes block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(24) > div > span.icon-media-stop.item-icon','TimeToMinutes block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(25) > div > span.label-item','TIMETOTIMEADD','TimeToTimeAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(25) > div > span.icon-media-stop.item-icon','TimeToTimeAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(26) > div > span.label-item','TIMETOTIMESUB','TimeToTimeSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(26) > div > span.icon-media-stop.item-icon','TimeToTimeSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(27) > div > span','LANGUAGE BLOCKS','Logical Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(28) > div > span.label-item','STAGING','Staging block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(28) > div > span.icon-media-stop.item-icon','Staging block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(29) > div > span','STRINGS BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(30) > div > span.label-item','STRSIZE','StrSize block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(30) > div > span.icon-media-stop.item-icon','StrSize block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(31) > div > span','WEATHER BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(32) > div > span.label-item','WEATHERINFO','WeatherInfo block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(32) > div > span.icon-media-stop.item-icon','WeatherInfo block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(33) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(34) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(34) > div > span.wi-moon-full.item-icon','Number block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('Check link suggestion for add block output', async () => 
    
    {
click('.blocks .add.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(400);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

elementPresent('#content > div.form-item-holder.ractive > div > div > input[type="text"]','An input to select blocks is present');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

cssClassPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open','open','Dropdown is open to select blocks');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(1) > div > span','MATH BLOCKS','Math Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item','ABS','ABS block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.icon-media-stop.item-icon','ABS block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.label-item','+','Add block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(3) > div > span.icon-media-stop.item-icon','Add block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.label-item','AVG','AVG block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(4) > div > span.icon-media-stop.item-icon','AVG block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.label-item','÷','DIVIDE block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(5) > div > span.icon-media-stop.item-icon','DIVIDE block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.label-item','ISEVEN','IsEven block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(6) > div > span.icon-media-stop.item-icon','IsEven block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.label-item','ISODD','IsOdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(7) > div > span.icon-media-stop.item-icon','IsOdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.label-item','LIMIT','Limit block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(8) > div > span.icon-media-stop.item-icon','Limit block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.label-item','MAX','MAX block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(9) > div > span.icon-media-stop.item-icon','MAX block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.label-item','MIN','MIN block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(10) > div > span.icon-media-stop.item-icon','MIN block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.label-item','MODULO','Modulo block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(11) > div > span.icon-media-stop.item-icon','Modulo block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.label-item','×','Multiply block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(12) > div > span.icon-media-stop.item-icon','Multiply block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.label-item','NEG','NEG block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(13) > div > span.icon-media-stop.item-icon','NEG block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.label-item','RANDOM','Random block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(14) > div > span.icon-media-stop.item-icon','Random block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.label-item','RESET','Reset block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(15) > div > span.icon-media-stop.item-icon','Reset block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.label-item','SINEWAVE','SineWave block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(16) > div > span.icon-media-stop.item-icon','SineWave block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.label-item','-','Subtract block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(17) > div > span.icon-media-stop.item-icon','Subtract block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item','THRESHOLD','Threshold block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.icon-media-stop.item-icon','Threshold block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(19) > div > span','LANGUAGE BLOCKS','Language Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.label-item','BOOLLATCH','BoolLatch block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(20) > div > span.icon-media-stop.item-icon','BoolLatch block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.label-item','DELAYEDOUTPUT','DelayedOutput block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(21) > div > span.icon-media-stop.item-icon','DelayedOutput block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.label-item','STAGING','Staging block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(22) > div > span.icon-media-stop.item-icon','Staging block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(23) > div > span','TIME BLOCKS','Time Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(24) > div > span.label-item','DATETIMEADD','DateTimeAdd block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(24) > div > span.icon-media-stop.item-icon','DateTimeAdd block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(25) > div > span.label-item','DATETIMESUB','DateTimeSub block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(25) > div > span.icon-media-stop.item-icon','DateTimeSub block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(26) > div > span','LOGICAL BLOCKS','Logical Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(27) > div > span.label-item','INRANGE','InRange block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(27) > div > span.icon-media-stop.item-icon','InRange block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(28) > div > span','EXPRESSION BLOCKS','Expression Blocks category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(29) > div > span.label-item','LEADLAG','LeadLag block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(29) > div > span.icon-media-stop.item-icon','LeadLag block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(30) > div > span.label-item','PID','Pid block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(30) > div > span.icon-media-stop.item-icon','Pid block has a checkbox');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(31) > div > span','CURRENT VARIABLES','Current Variables category is present in the dropdown');

containsText('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(32) > div > span.label-item','NUMBER','Number block is present for this category');

elementPresent('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(32) > div > span.wi-moon-full.item-icon','Number block has a point icon');

click('#content > div.form-item-holder.ractive > label');

waitService.waitForMilliseconds(500);

containsText('#content > div.form-item-holder.group > div > div:nth-child(1) > div > label','Tag','Add Tag option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(1) > div > input','Radio button to select Tag');

containsText('#content > div.form-item-holder.group > div > div:nth-child(2) > div > label','Point','Add Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(2) > div > input','Radio button to select Point');

containsText('#content > div.form-item-holder.group > div > div:nth-child(3) > div > label','Variable','Add Variable option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(3) > div > input','Radio button to select Variable');

containsText('#content > div.form-item-holder.group > div > div:nth-child(4) > div > label','Absolute Point','Add Absolute Point option is available');

elementPresent('#content > div.form-item-holder.group > div > div:nth-child(4) > div > input','Radio button to select Absolute Point');

click('#controlBar > button:nth-child(1)');

waitService.waitForMilliseconds(500);

frame(0);

waitService.waitForMilliseconds(400);
}

    
  );  
  it('delete program', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(200);

click('body > app > div.desktop-content > div.app-nav.vertical > nav > div:nth-child(1) > a > div > label');

waitService.waitForMilliseconds(500);

execute(test,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

    
  );  
  
  
});