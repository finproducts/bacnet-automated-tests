import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP211.js", () => {
  it("Go to Block Programming bLine", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(6)");

    waitService.waitForMilliseconds(500);
  });
  it("Drag blocks to the stage", async () => {
    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "weatherconditions"
    );

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .weatherConditions.block",
      "Weather conditions block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 44);

        app.set("blocks.1.y", 16);
      },
      [],
      function(result) {}
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(500);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "weatherinfo"
    );

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .weatherInfo.block",
      "Weather info block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 47);

        app.set("blocks.2.y", 109);
      },
      [],
      function(result) {}
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(500);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "weathersunrise"
    );

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .weatherSunrise.block",
      "Weather sunrise block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.3.x", 286);

        app.set("blocks.3.y", 128);
      },
      [],
      function(result) {}
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(500);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "weathersunset"
    );

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .weatherSunset.block",
      "Weather sunset block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.4.x", 308);

        app.set("blocks.4.y", 221);
      },
      [],
      function(result) {}
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(500);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "datetonumber"
    );

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .dateToNumber.block",
      "dateToNumber  block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.5.x", 270);

        app.set("blocks.5.y", 17);
      },
      [],
      function(result) {}
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(500);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "datetostring"
    );

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .dateToString.block",
      "dateToString block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.6.x", 64);

        app.set("blocks.6.y", 210);
      },
      [],
      function(result) {}
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(500);
  });
  it("Check input for weatherConditions", async () => {
    elementNotPresent(
      ".blocks .weatherConditions.block > div.data.sockets > div.input > div > b"
    );

    click(
      ".blocks .weatherConditions.block > div > div.input > div > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)",
      "CURRENT",
      "Current option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)",
      "TODAY",
      "TODAY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(3)",
      "TOMORROW",
      "TOMORROW option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(4)",
      "TWODAY",
      "TWODAY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(5)",
      "THREEDAY",
      "THREEDAY option is present in the dropdown"
    );

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for weatherInfo", async () => {
    elementNotPresent(
      ".blocks .weatherInfo.block > div.data.sockets > div.input > div > b"
    );

    click(".blocks .weatherInfo.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)",
      "CURRENTHUMIDITY",
      "CURRENTHUMIDITY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)",
      "CURRENTTEMP",
      "CURRENTTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(3)",
      "TODAYMAXTEMP",
      "TODAYMAXTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(4)",
      "TODAYMINTEMP",
      "TODAYMINTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(5)",
      "TOMORROWMAXTEMP",
      "TOMORROWMAXTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(6)",
      "TOMORROWMINTEMP",
      "TOMORROWMINTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(7)",
      "TWODAYMAXTEMP",
      "TWODAYMAXTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(8)",
      "TWODAYMINTEMP",
      "TWODAYMINTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(9)",
      "THREEDAYMAXTEMP",
      "THREEDAYMAXTEMP option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(10)",
      "THREEDAYMINTEMP",
      "THREEDAYMINTEMP option is present in the dropdown"
    );

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for weatherSunrise", async () => {
    elementNotPresent(
      ".blocks .weatherSunrise.block > div.data.sockets > div.input > div > b"
    );

    click(
      ".blocks .weatherSunrise.block > div > div.input > div > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)",
      "TODAY",
      "TODAY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)",
      "TOMORROW",
      "TOMORROW option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(3)",
      "TWODAY",
      "TWODAY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(4)",
      "THREEDAY",
      "THREEDAY option is present in the dropdown"
    );

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for weatherSunset", async () => {
    elementNotPresent(
      ".blocks .weatherSunset.block > div.data.sockets > div.input > div > b"
    );

    click(".blocks .weatherSunset.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)",
      "TODAY",
      "TODAY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)",
      "TOMORROW",
      "TOMORROW option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(3)",
      "TWODAY",
      "TWODAY option is present in the dropdown"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(4)",
      "THREEDAY",
      "THREEDAY option is present in the dropdown"
    );

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for dateToNumber", async () => {
    click(
      ".blocks .dateToNumber.block > div > div.input > div.socket.no-select.input > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)",
      "YEAR"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)",
      "MONTH"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(3)",
      "WEEK"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(4)",
      "DAY"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(5)",
      "HOUR"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(6)",
      "MIN"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(7)",
      "SEC"
    );

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for dateToString", async () => {
    click(
      ".blocks .dateToString.block > div > div.input > div.socket.no-select.input > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)",
      "MONTH"
    );

    containsText(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)",
      "WEEKDAY"
    );

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
});
