import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;



describe('testBP192.js',() =>{
  
  it('Go to Block Programming KMC', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(2)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add variable', async () => 
    
    {
click('.add.panel-button .variable.symbol');

waitService.waitForMilliseconds(500);

elementPresent('.scroller .variable.variable','Check that the variable is added');
}

    
  );  
  it('Drag variable and rem block to the stage', async () => 
    
    {
execute(dragAndDrop,['.scroller .variable > b.variable.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

setValue('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]','rem');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['#panel > section.blocklibrary > div.scroller > div > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.1.x',50);

app.set('blocks.1.y',50);
}
,[],function(result)
{}
);

execute(function(simple)
{
app.set('blocks.2.x',200);

app.set('blocks.2.y',200);
}
,[],function(result)
{}
);

waitService.waitForMilliseconds(500);

elementPresent('.blocks .rem.block','Check the block for the rem appears on the stage');

elementPresent('.blocks .set.variable.block','Check the block for the set variable appears on the stage');
}

    
  );  
  it('Check linkable attribute for inputs', async () => 
    
    {
elementPresent('.blocks .set.variable.block > div.data.sockets > div > div > b','Check that the variable block has a port for linking');

elementNotPresent('.blocks .rem.block > div.data.sockets > div > div > b','Check that the rem block doesn\'t have a port for linking');
}

    
  );  
  
  
});