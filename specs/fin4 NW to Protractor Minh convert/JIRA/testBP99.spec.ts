import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

describe("testBP99.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("BP-99", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div.window > div > span:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#panel > section.variables > header > div:nth-child(3)");

    waitService.waitForMilliseconds(500);

    click("#panel > section.variables > header > div:nth-child(4)");

    waitService.waitForMilliseconds(500);

    click("#panel > section.controls > div.icons > i.export.icon");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.export.modal > div > div.window > div > div > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div:nth-child(1) > pre > span > span"
    );

    waitService.waitForMilliseconds(500);

    keys([driver.Keys.CONTROL, "a", driver.Keys.CONTROL]);

    waitService.waitForMilliseconds(300);

    keys([driver.Keys.CONTROL, "c", driver.Keys.CONTROL]);

    waitService.waitForMilliseconds(300);

    click("body > app > div.export.modal > div > div.close > i");

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#panel > section.variables > div.scroller > div.function > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    click(
      "#panel > section.variables > div.scroller > div.function > i.cross.icon"
    );

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#panel > section.variables > div.scroller > div.macro",
      5,
      5
    );

    waitService.waitForMilliseconds(500);

    click(
      "#panel > section.variables > div.scroller > div.macro > i.cross.icon"
    );

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#panel > section.variables > div.scroller > div.variable > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    click(
      "#panel > section.variables > div.scroller > div.variable > i.cross.icon"
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      "#panel > section.variables > div.scroller > div.function"
    );

    elementNotPresent("#panel > section.variables > div.scroller > div.macro");

    click("#panel > section.controls > div.icons > i.import.icon");

    waitService.waitForMilliseconds(500);

    click(".import.modal > div > div.window > div > div.controls > div");

    waitService.waitForMilliseconds(500);

    click(
      ".import.modal > div > div.window > div > div.controls > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      ".import.modal > div > div.window > div > div.CodeMirror.cm-s-default > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre"
    );

    keys([driver.Keys.CONTROL, "v", driver.Keys.CONTROL]);

    waitService.waitForMilliseconds(500);

    click(".import.modal > div > div.window > div > div.controls > button");

    waitService.waitForMilliseconds(500);

    elementPresent("#panel > section.variables > div.scroller > div.function");

    elementPresent("#panel > section.variables > div.scroller > div.macro");

    elementPresent("#panel > section.variables > div.scroller > div.variable");
  });
});
