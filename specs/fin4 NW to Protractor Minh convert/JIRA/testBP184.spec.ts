import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp184 = require("../../fixtures/programs/BP184");

let bp245KMC = require("../../fixtures/programs/BP-245KMC");

describe("testBP184.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(5000);

    waitForElementPresent(".blocks", 1000);
  });
  it("load bLine program", async () => {
    loadProgram(bp245KMC);

    waitService.waitForMilliseconds(500);

    loadProgram(bp184);

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");

    elementPresent("#b163c371c-b2d6-4111-b474-3343d9dc0cf3");
  });
  it("check macro name", async () => {
    containsText(
      "#b163c371c-b2d6-4111-b474-3343d9dc0cf3 > header",
      "unnamedMacro1"
    );
  });
  it("rename macro 1", async () => {
    moveToElement(".variables .macro > span > span", 2, 2);

    waitForElementVisible(".pencil.icon", 200);

    click(".variables .macro > i.pencil.icon");

    waitService.waitForMilliseconds(500);

    keys([driver.Keys.CONTROL, "a", driver.Keys.CONTROL, driver.Keys.DELETE]);

    waitService.waitForMilliseconds(500);

    keys("t");

    waitService.waitForMilliseconds(50);

    keys("e");

    waitService.waitForMilliseconds(50);

    keys("s");

    waitService.waitForMilliseconds(50);

    keys("t");

    waitService.waitForMilliseconds(50);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(50);
  });
  it("check macro renamed 1", async () => {
    containsText("#b163c371c-b2d6-4111-b474-3343d9dc0cf3 > header", "test");
  });
  it("rename macro 2", async () => {
    moveToElement(".variables .macro > span > span", 2, 2);

    waitForElementVisible(".pencil.icon", 200);

    click(".variables .macro > i.pencil.icon");

    waitService.waitForMilliseconds(500);

    keys([driver.Keys.CONTROL, "a", driver.Keys.CONTROL, driver.Keys.DELETE]);

    waitService.waitForMilliseconds(500);

    keys("t");

    waitService.waitForMilliseconds(50);

    keys("e");

    waitService.waitForMilliseconds(50);

    keys("s");

    waitService.waitForMilliseconds(50);

    keys("t");

    waitService.waitForMilliseconds(50);

    keys("2");

    waitService.waitForMilliseconds(50);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(50);
  });
  it("check macro renamed 2", async () => {
    containsText("#b163c371c-b2d6-4111-b474-3343d9dc0cf3 > header", "test2");
  });
});
