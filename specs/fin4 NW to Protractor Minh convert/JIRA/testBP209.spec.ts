import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let weatherTime = require("../../fixtures/programs/weatherTime");

let optionOne =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)";

let optionTwo =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)";

let optionThree =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(3)";

let optionFour =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(4)";

let optionFive =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(5)";

let optionSix =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(6)";

let optionSeven =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(7)";

let optionEight =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(8)";

let optionNine =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(9)";

let optionTen =
  "#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(10)";

describe("testBP209.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Load program for BP-209", async () => {
    loadProgram(weatherTime);

    waitService.waitForMilliseconds(1000);

    loadProgram(weatherTime);

    waitService.waitForMilliseconds(1000);

    refresh();

    waitService.waitForMilliseconds(200);

    click(
      "body > app > div.restoreUnsaved.modal > div > div.unsaved.alert.window > div.tar > button.mls"
    );

    waitService.waitForMilliseconds(1500);

    elementPresent(
      ".weatherConditions.block",
      "Check that weatherConditions block is present"
    );

    elementPresent(
      ".weatherInfo.block",
      "Check that weatherInfo block is present"
    );

    elementPresent(
      ".weatherSunrise.block",
      "Check that weatherSunrise block is present"
    );

    elementPresent(
      ".weatherSunset.block",
      "Check that weatherSunset block is present"
    );

    elementPresent(
      ".dateToNumber.block",
      "Check that dateToNumber block is present"
    );

    elementPresent(
      ".dateToString.block",
      "Check that dateToString block is present"
    );
  });
  it("Check enums for weatherConditions", async () => {
    click(".weatherConditions.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);

    elementPresent(optionOne, "First enum is present in the list");

    elementPresent(optionTwo, "Second enum is present in the list");

    elementPresent(optionThree, "Third enum is present in the list");

    elementPresent(optionFour, "Fourth enum is present in the list");

    elementPresent(optionFive, "Fifth enum is present in the list");

    containsText(optionOne, "CURRENT", "Option 1 is CURRENT");

    containsText(optionTwo, "TODAY", "Option 2 is TODAY");

    containsText(optionThree, "TOMORROW", "Option 3 is TOMORROW");

    containsText(optionFour, "TWODAY", "Option 4 is TWODAY");

    containsText(optionFive, "THREEDAY", "Option 5 is THREEDAY");

    click(".weatherConditions.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for weatherInfo", async () => {
    click(".weatherInfo.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);

    elementPresent(optionOne, "First enum is present in the list");

    elementPresent(optionTwo, "Second enum is present in the list");

    elementPresent(optionThree, "Third enum is present in the list");

    elementPresent(optionFour, "Fourth enum is present in the list");

    elementPresent(optionFive, "Fifth enum is present in the list");

    elementPresent(optionSix, "Sixth enum is present in the list");

    elementPresent(optionSeven, "Seventh enum is present in the list");

    elementPresent(optionEight, "Eighth enum is present in the list");

    elementPresent(optionNine, "Ninth enum is present in the list");

    elementPresent(optionTen, "Tenth enum is present in the list");

    containsText(optionOne, "CURRENTHUMIDITY", "Option 1 is CURRENTHUMIDITY");

    containsText(optionTwo, "CURRENTTEMP", "Option 2 is CURRENTTEMP");

    containsText(optionThree, "TODAYMAXTEMP", "Option 3 is TODAYMAXTEMP");

    containsText(optionFour, "TODAYMINTEMP", "Option 4 is TODAYMINTEMP");

    containsText(optionFive, "TOMORROWMAXTEMP", "Option 5 is TOMORROWMAXTEMP");

    containsText(optionSix, "TOMORROWMINTEMP", "Option 6 is TOMORROWMINTEMP");

    containsText(optionSeven, "TWODAYMAXTEMP", "Option 7 is TWODAYMAXTEMP");

    containsText(optionEight, "TWODAYMINTEMP", "Option 8 is TWODAYMINTEMP");

    containsText(optionNine, "THREEDAYMAXTEMP", "Option 9 is THREEDAYMAXTEMP");

    containsText(optionTen, "THREEDAYMINTEMP", "Option 10 is THREEDAYMINTEMP");

    click(".weatherInfo.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for weatherSunrise", async () => {
    click(".weatherSunrise.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);

    elementPresent(optionOne, "First enum is present in the list");

    elementPresent(optionTwo, "Second enum is present in the list");

    elementPresent(optionThree, "Third enum is present in the list");

    elementPresent(optionFour, "Fourth enum is present in the list");

    containsText(optionOne, "TODAY", "Option 1 is TODAY");

    containsText(optionTwo, "TOMORROW", "Option 2 is TOMORROW");

    containsText(optionThree, "TWODAY", "Option 3 is TWODAY");

    containsText(optionFour, "THREEDAY", "Option 4 is THREEDAY");

    click(".weatherSunrise.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for weatherSunset", async () => {
    click(".weatherSunset.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);

    elementPresent(optionOne, "First enum is present in the list");

    elementPresent(optionTwo, "Second enum is present in the list");

    elementPresent(optionThree, "Third enum is present in the list");

    elementPresent(optionFour, "Fourth enum is present in the list");

    containsText(optionOne, "TODAY", "Option 1 is TODAY");

    containsText(optionTwo, "TOMORROW", "Option 1 is TOMORROW");

    containsText(optionThree, "TWODAY", "Option 2 is TWODAY");

    containsText(optionFour, "THREEDAY", "Option 3 is THREEDAY");

    click(".weatherSunset.block > div > div.input > div > div > label");

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for dateToString", async () => {
    click(
      ".dateToString.block > div > div.input > div.socket.no-select.input > div > label"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(optionOne, "First enum is present in the list");

    elementPresent(optionTwo, "Second enum is present in the list");

    containsText(optionOne, "MONTH", "Option 1 is MONTH");

    containsText(optionTwo, "WEEKDAY", "Option 2 is WEEKDAY");

    click(
      ".dateToString.block > div > div.input > div.socket.no-select.input > div"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check enums for dateToNumber", async () => {
    click(
      ".dateToNumber.block > div > div.input > div.socket.no-select.input > div > label"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(optionOne, "First enum is present in the list");

    elementPresent(optionTwo, "Second enum is present in the list");

    elementPresent(optionThree, "Third enum is present in the list");

    elementPresent(optionFour, "Fourth enum is present in the list");

    elementPresent(optionFive, "Fifth enum is present in the list");

    elementPresent(optionSix, "Sixth enum is present in the list");

    elementPresent(optionSeven, "Seventh enum is present in the list");

    containsText(optionOne, "YEAR", "Option 1 is YEAR");

    containsText(optionTwo, "MONTH", "Option 2 is MONTH");

    containsText(optionThree, "WEEK", "Option 3 is WEEK");

    containsText(optionFour, "DAY", "Option 4 is DAY");

    containsText(optionFive, "HOUR", "Option 5 is HOUR");

    containsText(optionSix, "MIN", "Option 6 is MIN");

    containsText(optionSeven, "SEC", "Option 7 is SEC");

    click(
      ".dateToNumber.block > div > div.input > div.socket.no-select.input > div > label"
    );

    waitService.waitForMilliseconds(500);
  });
});
