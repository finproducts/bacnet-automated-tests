import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp195 = require("../../fixtures/programs/BP195");

describe("testBP195.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("load bLine program", async () => {
    loadProgram(bp195);

    containsText(".language", "bLine");

    waitService.waitForMilliseconds(1010);

    elementPresent("#b6a2fbc13-57a4-48f4-82a5-ce40f8e12b8f");
  });
  it("duplicate macro in stage", async () => {
    elementNotPresent(".scroller .blocks .macro.block:nth-child(3)");

    click("#b6a2fbc13-57a4-48f4-82a5-ce40f8e12b8f");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    keys("c");

    waitService.waitForMilliseconds(500);

    keys("v");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(1500);

    elementPresent(".scroller .blocks .macro.block:nth-child(3)");
  });
  it("go to macro 1", async () => {
    click(".variables .macro > span > span");

    waitService.waitForMilliseconds(500);

    containsText(
      "#bf13e0f61-e063-4c07-abcb-327575d67d66 > header",
      "macro start"
    );
  });
  it("add new port", async () => {
    click(".bLine.banner.macro .cog.icon");

    waitService.waitForMilliseconds(500);

    click(".bLine.banner.macro > div > div > div.tac.mtm > button");

    elementPresent(
      ".bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4)"
    );
  });
  it("change output data type to number", async () => {
    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label"
    );

    waitService.waitForMilliseconds(500);

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label",
      "number"
    );

    click(".bLine.banner.macro .cog.icon");

    waitService.waitForMilliseconds(500);
  });
  it("return to main routine 1", async () => {
    click("#panel > section.variables > div.scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    containsText(
      "#b45aa2124-fc7a-400c-94ff-757071d6e20b > header",
      "routine start"
    );
  });
  it("check changes to macro", async () => {
    elementPresent(
      "#b6a2fbc13-57a4-48f4-82a5-ce40f8e12b8f > div.data.sockets > div.input > div:nth-child(3)"
    );

    cssClassPresent(
      "#b6a2fbc13-57a4-48f4-82a5-ce40f8e12b8f > div.data.sockets > div.output > div > div",
      "number"
    );

    elementPresent(
      "#b6a2fbc13-57a4-48f4-82a5-ce40f8e12b8f > div.data.sockets > div.input > div:nth-child(3) > input"
    );
  });
});
