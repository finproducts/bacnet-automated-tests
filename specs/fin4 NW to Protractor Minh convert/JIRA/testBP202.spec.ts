import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let test = require("../../fixtures/execute/logicBuilder.js").test;

let testSubMenu = require("../../fixtures/execute/logicBuilder.js").testSubMenu;

let edit = require("../../fixtures/execute/programData.js").edit;

let back = require("../../fixtures/execute/programData.js").back;

describe("testBP202.js", () => {
  it("go to bLine", async () => {
    waitService.waitForMilliseconds(5000);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });
  });
  it("open create bLine program option", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(1)"
    );

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "Check the window to create new programs is on screen"
    );

    containsText(
      ".form-window.modal-panel.generic-panel #title",
      "Create Program"
    );
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys("1test");

    waitService.waitForMilliseconds(300);

    click("#content > div.form-item-holder.filter > div > input");

    keys("vav");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
});
