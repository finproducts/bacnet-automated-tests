import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp183 = require("../../fixtures/programs/BP183");

let bp245KMC = require("../../fixtures/programs/BP-245KMC");

describe("testBP183.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(5000);

    waitForElementPresent(".blocks", 1000);
  });
  it("load bLine program", async () => {
    loadProgram(bp245KMC);

    waitService.waitForMilliseconds(500);

    loadProgram(bp183);

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");

    elementPresent("#bfa0a299b-ecca-423e-bb20-99a58eb91906");
  });
  it("check variables present", async () => {
    elementPresent(".variables .scroller  div:nth-child(3) ");

    elementPresent(".variables .scroller  div:nth-child(4) ");

    elementPresent(".variables .scroller  div:nth-child(5) ");

    elementPresent(".variables .scroller  div:nth-child(6) ");

    elementPresent(".variables .scroller  div:nth-child(7) ");

    elementPresent(".variables .scroller  div:nth-child(8) ");

    elementPresent(".variables .scroller  div:nth-child(9) ");
  });
  it("check macro dropdown list for boolean", async () => {
    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label",
      "Select a variable"
    );

    click(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(2)",
      "bool1"
    );

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(3)",
      "bool2"
    );

    elementNotPresent(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(4)"
    );
  });
  it("select a bool variable", async () => {
    click(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label",
      "bool2"
    );
  });
  it("go to macro 1", async () => {
    click(".variables .macro > span > span");

    waitService.waitForMilliseconds(500);

    containsText(
      "#bbede9ee5-1dee-4516-9cfe-73e9c5658916 > header",
      "macro start"
    );
  });
  it("change output data type to number", async () => {
    click(".bLine.banner.macro .cog.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label"
    );

    waitService.waitForMilliseconds(500);

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label",
      "number"
    );

    click(".bLine.banner.macro .cog.icon");

    waitService.waitForMilliseconds(500);
  });
  it("return to main routine 1", async () => {
    click("#panel > section.variables > div.scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    containsText(
      "#b9aa14b92-45f9-4723-bb92-f5aa2b516b20 > header",
      "routine start"
    );
  });
  it("check macro dropdown list for number", async () => {
    click(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(2)",
      "number1"
    );

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(3)",
      "number2"
    );

    elementNotPresent(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(4)"
    );
  });
  it("select a number variable", async () => {
    click(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label",
      "number2"
    );
  });
  it("go to macro 2", async () => {
    click(".variables .macro > span > span");

    waitService.waitForMilliseconds(500);

    containsText(
      "#bbede9ee5-1dee-4516-9cfe-73e9c5658916 > header",
      "macro start"
    );
  });
  it("change output data type to string", async () => {
    click(".bLine.banner.macro .cog.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label"
    );

    waitService.waitForMilliseconds(500);

    click(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      ".bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label",
      "string"
    );

    click(".bLine.banner.macro .cog.icon");

    waitService.waitForMilliseconds(500);
  });
  it("return to main routine 2", async () => {
    click("#panel > section.variables > div.scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    containsText(
      "#b9aa14b92-45f9-4723-bb92-f5aa2b516b20 > header",
      "routine start"
    );
  });
  it("check macro dropdown list for string", async () => {
    click(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(2)",
      "string1"
    );

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(3)",
      "string2"
    );

    elementNotPresent(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(4)"
    );
  });
  it("select a string variable", async () => {
    click(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#bfa0a299b-ecca-423e-bb20-99a58eb91906 > div.data.sockets > div.output > div > div > label",
      "string2"
    );
  });
});
