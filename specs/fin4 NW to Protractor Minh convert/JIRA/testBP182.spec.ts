import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP182.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add the macro block and drag it to stage", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      ".variables .scroller div  b.macro.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block",
      "The macro block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 466);

        app.set("blocks.1.y", 24);
      },
      [],
      function(result) {}
    );

    click(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div.ractive-select.pill.macro-output-dropdown.get.variable.block > label"
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      "#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.variable.block > li:nth-child(2)",
      "Check that there is no variables in the output dropdown list"
    );
  });
  it("add variables", async () => {
    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(500);
  });
  it("check macro dropdown list", async () => {
    click(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div.ractive-select.pill.macro-output-dropdown.get.variable.block > label"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.variable.block > li:nth-child(2)",
      "Check that there is a variable in the output dropdown list"
    );

    elementPresent(
      "#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.variable.block > li:nth-child(3)",
      "Check that there is a second variable in the output dropdown list"
    );

    elementPresent(
      "#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.variable.block > li:nth-child(4)",
      "Check that there is a third variable in the output dropdown list"
    );

    elementPresent(
      "#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.variable.block > li:nth-child(4)",
      "Check that there is a fourth variable in the output dropdown list"
    );
  });
});
