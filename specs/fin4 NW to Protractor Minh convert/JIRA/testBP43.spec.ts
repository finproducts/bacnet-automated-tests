import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

describe("testBP43.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Verifying feature BP-43", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(6)"
    );

    waitService.waitForMilliseconds(400);

    elementPresent("#stage > div.bLine.banner");

    waitService.waitForMilliseconds(300);

    click("#stage > div.bLine.banner > i");

    waitService.waitForMilliseconds(200);

    elementPresent("#stage > div.bLine.banner > div");

    waitService.waitForMilliseconds(500);

    click("#stage > div.bLine.banner > div > div:nth-child(2) > div > div");

    waitService.waitForMilliseconds(1000);

    elementPresent(".dropdown.open.mls");

    containsText(
      ".bLine.banner.interval > div > div:nth-child(2) > div > div > select > option:nth-child(1)",
      "slow"
    );

    containsText(
      ".bLine.banner.interval > div > div:nth-child(2) > div > div > select > option:nth-child(2)",
      "normal"
    );

    containsText(
      ".bLine.banner.interval > div > div:nth-child(2) > div > div > select > option:nth-child(3)",
      "fast"
    );

    elementPresent(".bLine.banner div div:nth-child(3) label span");

    click(".bLine.banner div div:nth-child(3) label span");

    waitService.waitForMilliseconds(200);

    containsText(".bLine.banner span", "on event");

    elementPresent(".bLine.banner div div:nth-child(4) label span");

    click(".bLine.banner div div:nth-child(4) label span");

    waitService.waitForMilliseconds(200);

    containsText(".bLine.banner span", "manual");
  });
});
