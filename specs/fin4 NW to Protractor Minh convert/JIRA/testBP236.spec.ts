import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP236.js", () => {
  it("Go to Block Programming bLine", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(5000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(6)");

    waitService.waitForMilliseconds(500);
  });
  it("Add variables and drag them to stage", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".scroller .boolean.variable:nth-child(3)",
      "First variable is present"
    );

    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".scroller .boolean.variable:nth-child(4)",
      "Second variable is present"
    );

    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".scroller .boolean.variable:nth-child(5)",
      "Third variable is present"
    );

    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".scroller .boolean.variable:nth-child(6)",
      "Fourth variable is present"
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div:nth-child(3) > b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.boolean.block:nth-child(2)",
      "First variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 466);

        app.set("blocks.1.y", 24);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div:nth-child(4) > b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.boolean.block:nth-child(3)",
      "Second variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 303);

        app.set("blocks.2.y", 314);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div:nth-child(5) > b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.boolean.block:nth-child(4)",
      "Third variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.3.x", 548);

        app.set("blocks.3.y", 432);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div:nth-child(6) > b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.boolean.block:nth-child(5)",
      "Fourth variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.4.x", 765);

        app.set("blocks.4.y", 617);
      },
      [],
      function(result) {}
    );
  });
  it("Select blocks and align them", async () => {
    keys(driver, Keys, SHIFT);

    waitService.waitForMilliseconds(300);

    click(".blocks .set.boolean.block:nth-child(2)");

    waitService.waitForMilliseconds(300);

    click(".blocks .set.boolean.block:nth-child(3)");

    waitService.waitForMilliseconds(300);

    click(".blocks .set.boolean.block:nth-child(4)");

    waitService.waitForMilliseconds(300);

    click(".blocks .set.boolean.block:nth-child(5)");

    waitService.waitForMilliseconds(300);

    click(".ractive-dropdown .alignment.icon");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.controls > div.icons > div > div",
      "open",
      "Align dropdown is open"
    );

    click(".icons > div > div > i.align-left.icon");

    waitService.waitForMilliseconds(500);

    cssProperty(
      ".blocks .set.boolean.block:nth-child(2)",
      "left",
      "303px",
      "First block is aligned to left"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(3)",
      "left",
      "303px",
      "Second block is aligned to left"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(4)",
      "left",
      "303px",
      "Third block is aligned to left"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(5)",
      "left",
      "303px",
      "Fourth block is aligned to left"
    );

    click("i.undo.icon");

    waitService.waitForMilliseconds(500);

    click(".ractive-dropdown .alignment.icon");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.controls > div.icons > div > div",
      "open",
      "Align dropdown is open"
    );

    click(".icons > div > div > i.align-right.icon");

    waitService.waitForMilliseconds(500);

    cssProperty(
      ".blocks .set.boolean.block:nth-child(2)",
      "left",
      "617px",
      "First block is aligned to right"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(3)",
      "left",
      "617px",
      "Second block is aligned to right"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(4)",
      "left",
      "617px",
      "Third block is aligned to right"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(5)",
      "left",
      "617px",
      "Fourth block is aligned to right"
    );

    click("i.undo.icon");

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    click(".ractive-dropdown .alignment.icon");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.controls > div.icons > div > div",
      "open",
      "Align dropdown is open"
    );

    click(".icons > div > div > i.align-top.icon");

    waitService.waitForMilliseconds(500);

    cssProperty(
      ".blocks .set.boolean.block:nth-child(2)",
      "top",
      "24px",
      "First block is aligned to top"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(3)",
      "top",
      "24px",
      "Second block is aligned to top"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(4)",
      "top",
      "24px",
      "Third block is aligned to top"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(5)",
      "top",
      "24px",
      "Fourth block is aligned to top"
    );

    click("i.undo.icon");

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    click(".ractive-dropdown .alignment.icon");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.controls > div.icons > div > div",
      "open",
      "Align dropdown is open"
    );

    click(".icons > div > div > i.align-bottom.icon");

    waitService.waitForMilliseconds(500);

    cssProperty(
      ".blocks .set.boolean.block:nth-child(2)",
      "top",
      "617px",
      "First block is aligned to bottom"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(3)",
      "top",
      "617px",
      "Second block is aligned to bottom"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(4)",
      "top",
      "617px",
      "Third block is aligned to bottom"
    );

    cssProperty(
      ".blocks .set.boolean.block:nth-child(5)",
      "top",
      "617px",
      "Fourth block is aligned to bottom"
    );

    click("i.undo.icon");

    waitService.waitForMilliseconds(500);
  });
});
