import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp104= require('../../fixtures/programs/BP-104');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP104.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('BP-104', async () => 
    
    {
click('#panel > section.controls > div.icons > i.import.icon');

waitService.waitForMilliseconds(500);

click('.import.modal > div > div.window > div > div.controls > div');

waitService.waitForMilliseconds(500);

click('.import.modal > div > div.window > div > div.controls > div > select > option:nth-child(2)');

click('.import.modal > div > div.window > div > div.CodeMirror.cm-s-default > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre');

keys('REAL abc[10]');

waitService.waitForMilliseconds(300);

click('.import.modal > div > div.window > div > div.controls > button');

waitService.waitForMilliseconds(500);

acceptAlert();

waitService.waitForMilliseconds(500);

elementPresent('#panel > section.variables > div.scroller > div');

containsText('#panel > section.variables > div.scroller > div > span','abc[10]');

loadProgram(bp104);

waitService.waitForMilliseconds(500);

elementPresent('#panel > section.variables > div.scroller > div');

containsText('#panel > section.variables > div.scroller > div > span','abc[10]');

elementPresent('#b7ac913b2-69d0-4a66-aba2-9354a7700488');

elementPresent('#b356c232e-a840-4b7c-8507-294e31688fd1');

elementPresent('#b356c232e-a840-4b7c-8507-294e31688fd1 > div.data.sockets > div.input > div');

containsText('#b356c232e-a840-4b7c-8507-294e31688fd1 > div.data.sockets > div.input > div ','index');

elementPresent('#b7ac913b2-69d0-4a66-aba2-9354a7700488 > div.data.sockets > div > div.socket.no-select.real.integer');

containsText('#b7ac913b2-69d0-4a66-aba2-9354a7700488 > div.data.sockets > div > div.socket.no-select.real.integer ','index');

elementPresent('#b7ac913b2-69d0-4a66-aba2-9354a7700488 > div.data.sockets > div > div.socket.no-select.real.variable');

containsText('#b7ac913b2-69d0-4a66-aba2-9354a7700488 > div.data.sockets > div > div.socket.no-select.real.variable','to');
}

    
  );  
  
  
});