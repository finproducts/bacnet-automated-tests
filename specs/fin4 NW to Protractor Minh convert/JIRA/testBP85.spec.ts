import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP85.js", () => {
  it("open centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarm, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarmSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .blocks");
  });
  it("rename alarm variable in centralPlantAlarm", async () => {
    containsText(".variable.boolean .name", "alarm");

    moveToElement(".boolean.variable .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".variable.boolean .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".variable.boolean .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".variable.boolean .name", "alarm");
  });
  it("Add routines and rename them in centralPlantAlarm", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".function:nth-child(3)", "Check if the routine is added");

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");

    moveToElement(".function:nth-child(3) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");
  });
  it("Add macro blocks and rename them in centralPlantAlarm", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".macro", "Check if the macro block is added");

    containsText(".macro .name", "unnamedMacro1");

    moveToElement(".macro .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".macro .name", "renaming", "Check the renaming is active");

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".macro .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".macro .name", "unnamedMacro1");
  });
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Change language to bLine", async () => {
    click(".file.icon");

    waitService.waitForMilliseconds(500);

    click(".tar .mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div.window > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(1)",
      "Check if main routine is present"
    );

    elementPresent(
      ".function:nth-child(2)",
      "Check if alarm routine is present"
    );

    elementNotPresent(
      ".variable.boolean",
      "Check that there is no variable present"
    );
  });
  it("Add variables and rename them in bLine", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".variable.boolean", "Check if the variable is added");

    containsText(".variable.boolean .name", "unnamedVariable1");

    moveToElement(".boolean.variable .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".variable.boolean .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".variable.boolean .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".variable.boolean .name", "unnamedVariable1");
  });
  it("Add routines and rename them in bLine", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".function:nth-child(3)", "Check if the routine is added");

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");

    moveToElement(".function:nth-child(3) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");
  });
  it("Add macro blocks and rename them in bLine", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".macro", "Check if the macro block is added");

    containsText(".macro .name", "unnamedMacro1");

    moveToElement(".macro .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".macro .name", "renaming", "Check the renaming is active");

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".macro .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".macro .name", "unnamedMacro1");
  });
  it("Change language to KMC", async () => {
    click(".file.icon");

    waitService.waitForMilliseconds(500);

    click(".tar .mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div.window > div > span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add variables and rename them in KMC", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".variable.variable", "Check if the variable is added");

    containsText(".variable.variable .name", "unnamedVariable1");

    moveToElement(".variable.variable .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".variable.variable .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".variable.variable .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".variable.variable .name", "unnamedVariable1");
  });
  it("Add routines and rename them in KMC", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".function:nth-child(1)", "Check if the routine is added");

    containsText(".function:nth-child(1) .name", "unnamedRoutine1");

    moveToElement(".function:nth-child(1) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".function:nth-child(1) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".function:nth-child(1) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".function:nth-child(1) .name", "unnamedRoutine1");
  });
  it("Add macro blocks and rename them in KMC", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".macro", "Check if the macro block is added");

    containsText(".macro .name", "unnamedMacro1");

    moveToElement(".macro .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".macro .name", "renaming", "Check the renaming is active");

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".macro .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".macro .name", "unnamedMacro1");
  });
});
