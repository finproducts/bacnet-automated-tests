import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP214.js", () => {
  it("open centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarm, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarmSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .blocks");
  });
  it("Try to delete routines in centralPlantAlarm", async () => {
    moveToElement(
      "#panel > section.variables > div.scroller > div.function.viewing > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");
  });
  it("Add new routines and try again to delete the main and alarm routine in centralPlantAlarm", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(3)",
      "Check if the new added routine is present"
    );

    moveToElement(
      "#panel > section.variables > div.scroller > div.function.viewing > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(3) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementPresent(".cross.icon");
  });
  it("Delete added routines and try again in centralPlantAlarm", async () => {
    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(3) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementPresent(".cross.icon");

    click(".cross.icon");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".function:nth-child(3)",
      "Check if the new added routine is deleted"
    );

    moveToElement(
      "#panel > section.variables > div.scroller > div.function.viewing > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");
  });
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Change language to bLine", async () => {
    click(".file.icon");

    waitService.waitForMilliseconds(500);

    click(".tar .mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div.window > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(1)",
      "Check if main routine is present"
    );

    elementPresent(
      ".function:nth-child(2)",
      "Check if alarm routine is present"
    );
  });
  it("Try to delete routines", async () => {
    moveToElement(
      "#panel > section.variables > div.scroller > div.function.viewing > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");
  });
  it("Add new routines and try again to delete the main and alarm routine", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(3)",
      "Check if the new added routine is present"
    );

    moveToElement(
      "#panel > section.variables > div.scroller > div.function.viewing > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(3) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementPresent(".cross.icon");
  });
  it("Delete added routines and try again", async () => {
    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(3) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementPresent(".cross.icon");

    click(".cross.icon");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".function:nth-child(3)",
      "Check if the new added routine is deleted"
    );

    moveToElement(
      "#panel > section.variables > div.scroller > div.function.viewing > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");

    moveToElement(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      2,
      2
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent(".cross.icon");
  });
});
