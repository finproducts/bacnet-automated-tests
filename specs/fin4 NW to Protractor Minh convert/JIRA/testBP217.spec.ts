import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp217 = require("../../fixtures/programs/BP-217");

describe("testBP217.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Load KMC program for BP-217", async () => {
    loadProgram(bp217);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e",
      "Check that the minMaxAvg block is present"
    );
  });
  it("Check output ports for minMaxAvg block", async () => {
    elementPresent(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(1) > div",
      "Dropdown present for min output port"
    );

    elementPresent(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(2) > div",
      "Dropdown present for max output port"
    );

    elementPresent(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(3) > div",
      "Dropdown present for avg output port"
    );
  });
  it("Check variables displayed in avg dropdown", async () => {
    click(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(3) > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(3) > div > select > option:nth-child(2)",
      "integer",
      "Check the first option is integer"
    );

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(3) > div > select > option:nth-child(3)",
      "real",
      "Check the second option is real"
    );

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(3) > div > select > option:nth-child(4)",
      "variable",
      "Check the third option is variable"
    );
  });
  it("Check variables displayed in max dropdown", async () => {
    click(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(2) > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(2) > div > select > option:nth-child(2)",
      "integer",
      "Check the first option is integer"
    );

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(2) > div > select > option:nth-child(3)",
      "real",
      "Check the second option is real"
    );

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(2) > div > select > option:nth-child(4)",
      "variable",
      "Check the third option is variable"
    );
  });
  it("Check variables displayed in min dropdown", async () => {
    click(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(1) > div > label"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(1) > div > select > option:nth-child(2)",
      "integer",
      "Check the first option is integer"
    );

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(1) > div > select > option:nth-child(3)",
      "real",
      "Check the second option is real"
    );

    containsText(
      "#b373d22a2-d768-4700-9fb6-594c5a105c8e > div.data.sockets > div.output > div:nth-child(1) > div > select > option:nth-child(4)",
      "variable",
      "Check the third option is variable"
    );
  });
});
