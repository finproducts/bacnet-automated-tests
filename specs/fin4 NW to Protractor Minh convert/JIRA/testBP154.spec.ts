import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP154.js", () => {
  it("open a bLine program centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      console.log(result.value);

      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(2)"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(9) > a > div > div > div.drag.hitarea.full"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(9) > ul > li:nth-child(1)"
    );

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller.blocks");
  });
  it("add a macro block 1", async () => {
    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);

    elementPresent(".variables .macro");
  });
  it("enter macro block 1", async () => {
    click(".variables .macro .name");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller div:nth-child(3)", "viewing");

    elementNotPresent("b.macro.block");
  });
  it("switch from a macro block 1", async () => {
    click(".variables .function:nth-child(1) .name");

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(5000);

    cssClassNotPresent(".variables .scroller div:nth-child(3)", "viewing");

    cssClassPresent(".variables .scroller div:nth-child(1)", "viewing");

    elementPresent("b.macro.block");
  });
  it("add variables function macro block 1", async () => {
    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(200);

    elementPresent(".variables .macro");

    elementPresent(".variables .variable");

    elementPresent(".variables .function:nth-child(3)");
  });
  it("enter macro block 2", async () => {
    click(".variables .scroller div:nth-child(4) span");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller div:nth-child(4)", "viewing");

    elementNotPresent(".variables .scroller div:nth-child(4) b.macro.block");
  });
  it("edit a macro block 1", async () => {
    click(".bLine.banner.mobile.macro i.cog.icon");

    waitService.waitForMilliseconds(200);

    click(".bLine.banner.mobile.macro > div > div > div.tac.mtm > button");

    waitService.waitForMilliseconds(200);

    clearValue(
      '.bLine.banner.mobile.macro table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]'
    );

    setValue(
      '.bLine.banner.mobile.macro table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]',
      "test"
    );

    click(".bLine.banner.mobile.macro i.cog.icon.active");
  });
  it("switch from a macro block 2", async () => {
    click(".variables .function:nth-child(1) .name");

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(".variables .scroller div:nth-child(3)", "viewing");

    cssClassPresent(".variables .scroller div:nth-child(1)", "viewing");

    elementPresent("b.macro.block");
  });
  it("add variables function macro block 2", async () => {
    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(200);

    elementPresent(".variables .macro");

    elementPresent(".variables .variable");

    elementPresent(".variables .function:nth-child(3)");
  });
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("change language to bLine", async () => {
    click(".controls i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");
  });
  it("add a macro block 2", async () => {
    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(500);

    elementPresent(".variables .macro");
  });
  it("enter macro block 3", async () => {
    click(".variables .macro .name");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller div:nth-child(3)", "viewing");

    elementNotPresent("b.macro.block");
  });
  it("switch from a macro block 3", async () => {
    click(".variables .function:nth-child(1) .name");

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(".variables .scroller div:nth-child(3)", "viewing");

    cssClassPresent(".variables .scroller div:nth-child(1)", "viewing");

    elementPresent("b.macro.block");
  });
  it("add variables function macro block 3", async () => {
    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(200);

    elementPresent(".variables .macro");

    elementPresent(".variables .variable");

    elementPresent(".variables .function:nth-child(3)");
  });
  it("enter macro block 4", async () => {
    click(".variables .scroller div:nth-child(4) span");

    waitService.waitForMilliseconds(500);

    cssClassPresent(".variables .scroller div:nth-child(4)", "viewing");

    elementNotPresent(".variables .scroller div:nth-child(4) b.macro.block");
  });
  it("edit a macro block 2", async () => {
    click(".bLine.banner.macro i.cog.icon");

    waitService.waitForMilliseconds(200);

    click(".bLine.banner.macro > div > div > div.tac.mtm > button");

    waitService.waitForMilliseconds(200);

    clearValue(
      '.bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]'
    );

    setValue(
      '.bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]',
      "test"
    );

    click(".bLine.banner.macro .cog.icon.active");
  });
  it("switch from a macro block 4", async () => {
    click(".variables .function:nth-child(1) .name");

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(".variables .scroller div:nth-child(4)", "viewing");

    cssClassPresent(".variables .scroller div:nth-child(1)", "viewing");

    elementPresent("b.macro.block");
  });
  it("add variables function macro block 4", async () => {
    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .variable");

    waitService.waitForMilliseconds(200);

    click(".add.panel-button .macro");

    waitService.waitForMilliseconds(200);

    elementPresent(".variables .macro");

    elementPresent(".variables .variable");

    elementPresent(".variables .function:nth-child(3)");
  });
});
