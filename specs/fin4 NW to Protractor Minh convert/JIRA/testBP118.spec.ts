import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let fs= require('fs');

let bp118= fs.readFileSync('./tests/fixtures/programs/BP118.txt').toString();



describe('testBP118.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add variables and drag them to stage', async () => 
    
    {
execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

setValue('.blocklibrary .filters div > input[type="text"]','if');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary .scroller > div > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .if.block','The if block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',386);

app.set('blocks.1.y',206);
}
,[],function(result)
{}
);

clearValue('.blocklibrary .filters div > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('.blocklibrary .filters div > input[type="text"]','cmpeq');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary .scroller > div > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .cmpEq.block','The numeric setter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',170);

app.set('blocks.2.y',362);
}
,[],function(result)
{}
);

clearValue('.blocklibrary .filters div > input[type="text"]');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller div:nth-child(3)  b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block:nth-child(4)','The numeric n1 getter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',20);

app.set('blocks.3.y',412);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller div:nth-child(4)  b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block:nth-child(5)','The numeric n2 getter block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',30);

app.set('blocks.4.y',482);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller div:nth-child(5)  b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block','The numeric setter block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',428);

app.set('blocks.5.y',393);
}
,[],function(result)
{}
);
}

    
  );  
  it('Link blocks', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div.set.number.block > div.data.sockets > div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div.set.number.block > div.data.sockets > div.input > div > div > div > input[type="number"]','15');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpEq.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(4) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpEq.block > div > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(5) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpEq.block > div > div.input > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('verify generated code', async () => 
    
    {
waitService.waitForMilliseconds(1000);

execute(function(simple)
{
return app.generateCode();
}
,[],function(result)
{
console.log(result.value);

waitService.waitForMilliseconds(5000);

expect(result.value);

to;

equal(bp118);
}
);
}

    
  );  
  
  
});