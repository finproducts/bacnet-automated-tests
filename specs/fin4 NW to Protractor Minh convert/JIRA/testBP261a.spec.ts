import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let test= require('../../fixtures/execute/logicBuilder.js').test;

let testSubMenu= require('../../fixtures/execute/logicBuilder.js').testSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;

let addVariables= require('../../fixtures/execute/programData.js').addVariables;

let addVariablesSubMenu= require('../../fixtures/execute/programData.js').addVariablesSubMenu;



describe('testBP261a.js',() =>{
  
  it('go to City Center VAV-01 ', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','Floor 1');

click('#equipList > li:nth-child(2) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center Vav-01');
}

    
  );  
  it('go to bLine', async () => 
    
    {
waitService.waitForMilliseconds(1500);

cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);
}

    
  );  
  it('open create bLine program option', async () => 
    
    {
execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1) > label");

waitService.waitForMilliseconds(1000);
}
);

elementPresent('.form-window.modal-panel.generic-panel','Check the window to create new programs is on screen');

containsText('.form-window.modal-panel.generic-panel #title','Create bLine Program');
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys('test');

waitService.waitForMilliseconds(300);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#title','Success','Check that the program was successfully created');

click('#controlBar > button');

waitService.waitForMilliseconds(500);

elementNotPresent('#notifications > div','Check that a warning is not displayed that a program with the same name exits, if it doensn't');
}

    
  );  
  it('delete program', async () => 
    
    {
refresh();

waitService.waitForMilliseconds(3000);

frame(null);

waitService.waitForMilliseconds(500);

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);

execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(2) > label");

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

    
  );  
  
  
});