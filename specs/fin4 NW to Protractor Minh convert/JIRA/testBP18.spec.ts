import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

describe("testBP18.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Change language to JavaScript", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change variable type in JavaScript", async () => {
    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    moveToElement(
      "#panel > section.variables > div.scroller > div > span",
      2,
      2
    );

    click("#panel > section.variables > div.scroller > div > div");

    waitService.waitForMilliseconds(200);

    click(
      "#panel > section.variables > div.scroller > div > div > div > table > tr:nth-child(1) > td:nth-child(2) > div"
    );

    waitService.waitForMilliseconds(200);

    click("#ractive-select-dropdown-container > ul > li:nth-child(2)");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "variable"
    );
  });
  it("Change language to KMC", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change variable type in KMC", async () => {
    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "variable"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "integer"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent("#panel > section.variables > div.scroller > div", "real");

    waitService.waitForMilliseconds(500);

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "string"
    );
  });
  it("Change language to PPCL", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change variable type in PPCL", async () => {
    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "boolean"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "number"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "string"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change language to Ruby", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change variable type in Ruby", async () => {
    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "boolean"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "number"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "string"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change language to axon", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(5)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change variable type in axon", async () => {
    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "boolean"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "number"
    );

    click("#panel > section.variables > div.scroller > div > i");

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div",
      "string"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change language to bLine", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    elementPresent(".window");

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p:nth-child(1)",
      "You have unsaved work! Creating a new program will discard your current program"
    );

    containsText(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > div > p.mts",
      "Do you wish to proceed?"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(6)"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Change variable type in bLine", async () => {
    click("#panel > section.variables > header > div:nth-child(2)");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div.boolean.variable",
      "boolean"
    );

    click(
      "#panel > section.variables > div.scroller > div.boolean.variable > i"
    );

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div.number.variable",
      "number"
    );

    click(
      "#panel > section.variables > div.scroller > div.number.variable > i"
    );

    waitService.waitForMilliseconds(200);

    cssClassPresent(
      "#panel > section.variables > div.scroller > div.string.variable",
      "string"
    );

    waitService.waitForMilliseconds(500);
  });
});
