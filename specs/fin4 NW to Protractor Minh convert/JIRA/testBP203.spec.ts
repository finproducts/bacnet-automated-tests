import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP203.js", () => {
  it("open centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarm, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarmSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .blocks");
  });
  it("Add new routine and rename it", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".function:nth-child(3)", "Check if the routine is added");

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");

    moveToElement(".function:nth-child(3) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("main");

    waitService.waitForMilliseconds(700);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");
  });
  it("Add two macroBlocks and rename one", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".macro", "Check if the macro block is added");

    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".macro:nth-child(5)",
      "Check if the second macro block is added"
    );

    containsText(".macro:nth-child(4) .name", "unnamedMacro1");

    containsText(".macro:nth-child(5) .name", "unnamedMacro2");

    moveToElement(".macro:nth-child(5) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".macro:nth-child(5) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("unnamedMacro1");

    waitService.waitForMilliseconds(1500);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".macro:nth-child(5) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".macro:nth-child(5) .name", "unnamedMacro2");
  });
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Change language to bLine", async () => {
    click(".file.icon");

    waitService.waitForMilliseconds(500);

    click(".tar .mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div.window > div > span:nth-child(6)");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(1)",
      "Check if main routine is present"
    );

    elementPresent(
      ".function:nth-child(2)",
      "Check if alarm routine is present"
    );

    elementNotPresent(
      ".variable.boolean",
      "Check that there is no variable present"
    );
  });
  it("Add two variables and rename one in bLine", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".variable.boolean:nth-child(3)",
      "Check if the variable is added"
    );

    elementPresent(
      ".variable.boolean:nth-child(4)",
      "Check if the variable is added"
    );

    containsText(".variable.boolean:nth-child(3) .name", "unnamedVariable1");

    containsText(".variable.boolean:nth-child(4) .name", "unnamedVariable2");

    moveToElement(".boolean.variable:nth-child(4) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".variable.boolean:nth-child(4) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("unnamedVariable1");

    waitService.waitForMilliseconds(1500);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".variable.boolean:nth-child(4) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".variable.boolean:nth-child(4) .name", "unnamedVariable2");
  });
  it("Add routine and rename it in bLine", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".function:nth-child(3)", "Check if the routine is added");

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");

    moveToElement(".function:nth-child(3) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("main");

    waitService.waitForMilliseconds(700);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".function:nth-child(3) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".function:nth-child(3) .name", "unnamedRoutine1");
  });
  it("Add two macroBlocks and rename one in bLine", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".macro", "Check if the macro block is added");

    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".macro:nth-child(5)",
      "Check if the second macro block is added"
    );

    containsText(".macro:nth-child(4) .name", "unnamedMacro1");

    containsText(".macro:nth-child(5) .name", "unnamedMacro2");

    moveToElement(".macro:nth-child(5) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".macro:nth-child(5) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("unnamedMacro1");

    waitService.waitForMilliseconds(1500);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".macro:nth-child(5) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".macro:nth-child(5) .name", "unnamedMacro2");
  });
  it("Change language to KMC", async () => {
    click(".file.icon");

    waitService.waitForMilliseconds(500);

    click(".tar .mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".newProgram.modal > div > div.window > div > span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add two variables and rename one in KMC", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".variable.variable:nth-child(1)",
      "Check if the first variable is added"
    );

    elementPresent(
      ".variable.variable:nth-child(2)",
      "Check if the second variable is added"
    );

    containsText(".variable.variable:nth-child(1) .name", "unnamedVariable1");

    containsText(".variable.variable:nth-child(2) .name", "unnamedVariable2");

    moveToElement(".variable.variable:nth-child(2) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".variable.variable:nth-child(2) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("unnamedVariable1");

    waitService.waitForMilliseconds(1500);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".variable.variable:nth-child(2) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".variable.variable:nth-child(2) .name", "unnamedVariable2");
  });
  it("Add two routines and rename one in KMC", async () => {
    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(1)",
      "Check if the first routine is added"
    );

    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".function:nth-child(2)",
      "Check if the second routine is added"
    );

    containsText(".function:nth-child(1) .name", "unnamedRoutine1");

    moveToElement(".function:nth-child(1) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".function:nth-child(1) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("unnamedRoutine2");

    waitService.waitForMilliseconds(1500);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".function:nth-child(1) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".function:nth-child(1) .name", "unnamedRoutine1");
  });
  it("Add two macroBlocks and rename one in KMC", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".macro", "Check if the macro block is added");

    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".macro:nth-child(4)",
      "Check if the second macro block is added"
    );

    containsText(".macro:nth-child(3) .name", "unnamedMacro1");

    containsText(".macro:nth-child(4) .name", "unnamedMacro2");

    moveToElement(".macro:nth-child(4) .name", 2, 2);

    waitService.waitForMilliseconds(500);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".macro:nth-child(4) .name",
      "renaming",
      "Check the renaming is active"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("a");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(200);

    keys("unnamedMacro1");

    waitService.waitForMilliseconds(1500);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    cssClassNotPresent(
      ".macro:nth-child(4) .name",
      "renaming",
      "Check the renaming is inactive"
    );

    containsText(".macro:nth-child(4) .name", "unnamedMacro2");
  });
});
