import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let test = require("../../fixtures/execute/logicBuilder.js").test;

let testSubMenu = require("../../fixtures/execute/logicBuilder.js").testSubMenu;

let edit = require("../../fixtures/execute/programData.js").edit;

let back = require("../../fixtures/execute/programData.js").back;

describe("testBP227.js", () => {
  it("go to City Center AHU-1 ", async () => {
    waitService.waitForMilliseconds(1500);

    waitService.waitForMilliseconds(1000);

    containsText("header > h4", "Site");

    click("#equipList > li:nth-child(1) > a > h4");

    containsText("header > h4", "Floor 1");

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    containsText("header > h4", "City Center AHU-1");
  });
  it("go to bLine", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });
  });
  it("open create bLine program option", async () => {
    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1) > label");

      waitService.waitForMilliseconds(1000);
    });

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "Check the window to create new programs is on screen"
    );

    containsText(
      ".form-window.modal-panel.generic-panel #title",
      "Create bLine Program"
    );
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys("test");

    waitService.waitForMilliseconds(300);

    click("#content > div:nth-child(6) > div > div");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    containsText(
      "#content > div.form-item-holder.combobox > label",
      "Select Tag Parent:",
      "Check that the window switched to choosing Tags"
    );

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(1)"
    );

    waitService.waitForMilliseconds(200);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(200);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(200);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(200);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(5)"
    );

    waitService.waitForMilliseconds(200);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Open program", async () => {
    refresh();

    waitService.waitForMilliseconds(2000);

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(1000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent("#panel");

    elementPresent(
      "#panel .variables .marker.variable:nth-child(3)",
      "Check the ahu tag is present in the section"
    );

    containsText(
      "#panel .variables .marker.variable:nth-child(3) span",
      "ahu",
      "Check the ahu tag has the correct name"
    );

    elementPresent(
      "#panel .variables .marker.variable:nth-child(4)",
      "Check the equip tag is present in the section"
    );

    containsText(
      "#panel .variables .marker.variable:nth-child(4) span",
      "equip",
      "Check the equip tag has the correct name"
    );

    elementPresent(
      "#panel .variables .marker.variable:nth-child(5)",
      "Check the hvac tag is present in the section"
    );

    containsText(
      "#panel .variables .marker.variable:nth-child(5) span",
      "hvac",
      "Check the hvac tag has the correct name"
    );

    elementPresent(
      "#panel .variables .string.variable:nth-child(6)",
      "Check the navName tag is present in the section"
    );

    containsText(
      "#panel .variables .string.variable:nth-child(6) span",
      "navName",
      "Check the navName tag has the correct name"
    );

    elementPresent(
      "#panel .variables .marker.variable:nth-child(7)",
      "Check the vavZone tag is present in the section"
    );

    containsText(
      "#panel .variables .marker.variable:nth-child(7) span",
      "vavZone",
      "Check the vavZone tag has the correct name"
    );

    waitService.waitForMilliseconds(500);
  });
  it("delete program", async () => {
    refresh();

    waitService.waitForMilliseconds(2000);

    frame(null);

    waitService.waitForMilliseconds(500);

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(4)");

      waitService.waitForMilliseconds(1000);

      click("#controlBar > button:nth-child(2)");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      element(result.value);

      to;

      not;

      be;

      present;
    });
  });
});
