import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();




describe('testBP132.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('BP-132', async () => 
    
    {
click('#panel > section.controls > div.icons > i.import.icon');

waitService.waitForMilliseconds(500);

click('.import.modal > div > div.window > div > div.controls > div > label');

waitService.waitForMilliseconds(500);

click('.import.modal > div > div.window > div > div.controls > div > select > option:nth-child(1)');

waitService.waitForMilliseconds(500);

click('.import.modal > div > div.window > div > div.CodeMirror.cm-s-default > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre');

keys([driver.Keys.CONTROL, 'a', driver.Keys.CONTROL, driver.Keys.DELETE]);

waitService.waitForMilliseconds(300);

keys('var SVG = getByName("SVG");');

keys(driver,Keys,ENTER);

keys('SVG.fill = 'rgba(255, 0, 0, 0.52)';');

keys(driver,Keys,ENTER);

keys('SVG.style.borderStyle = 'solid';');

keys(driver,Keys,ENTER);

keys('SVG.style.borderRadius = '16px';');

keys(driver,Keys,ENTER);

keys('SVG.style.borderWidth = '2px';');

keys(driver,Keys,ENTER);

keys('SVG.style.borderColor = 'rgb(241, 0, 0)';');

keys(driver,Keys,ENTER);

waitService.waitForMilliseconds(500);

click('body > app > div.import.modal > div > div.window > div > div.controls > button');

waitService.waitForMilliseconds(500);

elementPresent('#panel > section.variables > div.scroller > div');

containsText('#panel > section.variables > div.scroller > div > span','SVG');

elementPresent('#stage > div > div > div:nth-child(1)');

containsText('#stage > div > div > div:nth-child(1) > header','getByName');

elementPresent('#stage > div > div > div:nth-child(2)');

containsText('#stage > div > div > div:nth-child(2) > header','propertySet');

elementPresent('#stage > div > div > div:nth-child(3)');

containsText('#stage > div > div > div:nth-child(3) > header','propertySet');

elementPresent('#stage > div > div > div:nth-child(4)');

containsText('#stage > div > div > div:nth-child(4) > header','propertySet');

elementPresent('#stage > div > div > div:nth-child(5)');

containsText('#stage > div > div > div:nth-child(5) > header','propertySet');

elementPresent('#stage > div > div > div:nth-child(6)');

containsText('#stage > div > div > div:nth-child(6) > header','propertySet');

elementPresent('#stage > div > div > div:nth-child(7)');

containsText('#stage > div > div > div:nth-child(7) > header','set SVG');

elementPresent('#stage > div > div > div:nth-child(8)');

containsText('#stage > div > div > div:nth-child(8) > header','program start');
}

    
  );  
  
  
});