import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let ahuFDD = require("../../fixtures/execute/logicBuilder.js").ahuFDD;

let ahuFDDSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .ahuFDDsubMenu;

describe("testBP244.js", () => {
  it("open ahUFDD", async () => {
    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(ahuFDD, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(ahuFDDSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);
  });
  it("Add a macro block", async () => {
    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .macro", "A macro was added");
  });
  it("Switch between the macro block and the main routine a few times", async () => {
    click(".scroller .macro i.macro.symbol");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(2)",
      "No other block except for the start block is present"
    );

    click(".scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(17)",
      "No extra block is present"
    );

    click(".scroller .macro i.macro.symbol");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(2)",
      "No other block except for the start block is present"
    );

    click(".scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(17)",
      "No extra block is present"
    );

    click(".scroller .macro i.macro.symbol");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(2)",
      "No other block except for the start block is present"
    );

    click(".scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(17)",
      "No extra block is present"
    );

    click(".scroller .macro i.macro.symbol");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(2)",
      "No other block except for the start block is present"
    );

    click(".scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(17)",
      "No extra block is present"
    );

    click(".scroller .macro i.macro.symbol");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(2)",
      "No other block except for the start block is present"
    );

    click(".scroller > div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".blocks > div:nth-child(17)",
      "No extra block is present"
    );
  });
});
