import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let stringSetBlock = ".blocks .set.string.block";

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let colorPickerTest = require("../../fixtures/execute/logicBuilder.js")
  .colorPickerTest;

let colorPickerTestSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .colorPickerTestSubMenu;

let edit = require("../../fixtures/execute/programData.js").edit;

let back = require("../../fixtures/execute/programData.js").back;

let addVariables = require("../../fixtures/execute/programData.js")
  .addVariables;

let addVariablesSubMenu = require("../../fixtures/execute/programData.js")
  .addVariablesSubMenu;

describe("testBP160.js", () => {
  it("Go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });
  });
  it("Open Make new program option", async () => {
    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1) > label");

      waitService.waitForMilliseconds(1000);
    });

    containsText(
      ".form-window.modal-panel.generic-panel #title",
      "Create Program",
      'The window to create a new program is on screen and has the title "Create Program"'
    );

    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys("colorPickerTest");

    waitService.waitForMilliseconds(300);

    click("#content > div.form-item-holder.filter > div > input");

    waitService.waitForMilliseconds(500);

    keys("vav");

    waitService.waitForMilliseconds(300);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    containsText(
      "#title",
      "Success",
      "Check that the program was successfully created"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Open program", async () => {
    getText(
      ".app-nav.vertical > nav > div:nth-child(6) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive",
      function(result) {
        console.log(result.value);

        if (result.value == "Edit") {
          execute(edit, [], function(result) {
            click(result.value);

            waitService.waitForMilliseconds(3000);

            frame(0);

            waitService.waitForMilliseconds(200);

            containsText(
              "#panel > section.controls > div.program-name > span:nth-child(1)",
              "colorPickerTest",
              "colorPickerTest program is open"
            );

            frame(null);

            waitService.waitForMilliseconds(200);
          });
        } else {
          refresh();

          waitService.waitForMilliseconds(5000);

          frame(null);

          waitService.waitForMilliseconds(500);

          click(
            ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
          );

          waitService.waitForMilliseconds(500);

          execute(logicBuilder, [], function(result) {
            click(result.value);

            waitService.waitForMilliseconds(1500);
          });

          execute(logicBuilderSubMenu, [], function(result) {
            click(result.value + " > ul > li:nth-child(2) > label");

            waitService.waitForMilliseconds(1000);
          });

          execute(colorPickerTest, [], function(result) {
            click(result.value);

            waitService.waitForMilliseconds(1000);
          });

          execute(testSubMenu, [], function(result) {
            click(result.value + " > ul > li:nth-child(1)");

            waitService.waitForMilliseconds(1000);

            frame(0);

            waitService.waitForMilliseconds(200);

            containsText(
              "#panel > section.controls > div.program-name > span:nth-child(1)",
              "colorPickerTest",
              "colorPickerTest program is open"
            );

            frame(null);

            waitService.waitForMilliseconds(200);
          });

          execute(testSubMenu, [], function(result) {
            click(result.value + " > ul > li:nth-child(3)");

            waitService.waitForMilliseconds(1000);
          });
        }
      }
    );
  });
  it("Add string variable", async () => {
    execute(addVariables, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(700);
    });

    execute(addVariablesSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(3) > label");

      waitService.waitForMilliseconds(700);
    });

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "The window to add variables is on screen"
    );

    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(400);

    keys("string");

    waitService.waitForMilliseconds(500);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    setValue("#content > div.form-item-holder.textinput > input", "color");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(700);

    containsText("#title", "Success", "The variable was successfully added");

    click("#controlBar > button");

    waitService.waitForMilliseconds(10000);
  });
  it("Drag the variables to the stage using suggestion tool", async () => {
    frame(0);

    waitService.waitForMilliseconds(200);

    click(".__start__.block > div > div > div > b");

    waitService.waitForMilliseconds(200);

    moveTo(null, 20, 20);

    waitService.waitForMilliseconds(400);

    mouseButtonClick(0);

    waitService.waitForMilliseconds(800);

    frame(null);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".modal-panel",
      "Add Points to bLine Program form appears on screen"
    );

    click(
      '#content > div.form-item-holder.ractive > div > div > input[type="text"]'
    );

    waitService.waitForMilliseconds(500);

    click(
      "#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item"
    );

    waitService.waitForMilliseconds(200);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(2500);

    frame(0);

    waitService.waitForMilliseconds(400);

    elementPresent(stringSetBlock, "String Setter block is on stage");
  });
  it("Set color code for  string1 input value", async () => {
    setValue(
      stringSetBlock + " > div.data.sockets > div > div > input",
      "#000fff"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      stringSetBlock + " > div.data.sockets > div > div > div",
      "The color picker is present"
    );

    cssClassPresent(
      stringSetBlock + " > div.data.sockets > div > div > div",
      "colorpicker-button",
      "The color picker has the correct class"
    );
  });
  it("Delete color code for  string1 input value", async () => {
    click(stringSetBlock + " > div.data.sockets > div > div > input");

    keys(driver, Keys, BACK_SPACE);

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      stringSetBlock + " > div.data.sockets > div > div > div",
      "The color picker disappeared after deleting one character"
    );

    keys([driver.Keys.CONTROL, "a", driver.Keys.CONTROL, driver.Keys.DELETE]);

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      stringSetBlock + " > div.data.sockets > div > div > div",
      "The color picker is still gone"
    );

    cssProperty(
      stringSetBlock + " > div.data.sockets > div > div > input",
      "width",
      "138.734px",
      "The input has the normal size"
    );
  });
  it("Delete program", async () => {
    frame(null);

    waitService.waitForMilliseconds(200);

    getText(
      ".app-nav.vertical > nav > div:nth-child(6) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive",
      function(result) {
        console.log(result.value);

        if (result.value == "Edit") {
          click(
            "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1) > a > div > label"
          );

          waitService.waitForMilliseconds(2500);

          execute(colorPickerTest, [], function(result) {
            click(result.value);

            waitService.waitForMilliseconds(1000);
          });

          execute(colorPickerTestSubMenu, [], function(result) {
            click(result.value + " > ul > li:nth-child(4)");

            waitService.waitForMilliseconds(1000);

            click("#controlBar > button:nth-child(2)");

            waitService.waitForMilliseconds(1000);
          });

          execute(colorPickerTest, [], function(result) {
            element(result.value);

            to;

            not;

            be;

            present;
          });
        } else {
          refresh();

          waitService.waitForMilliseconds(5000);

          frame(null);

          waitService.waitForMilliseconds(500);

          click(
            ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
          );

          waitService.waitForMilliseconds(500);

          execute(logicBuilder, [], function(result) {
            click(result.value);

            waitService.waitForMilliseconds(1500);
          });

          execute(logicBuilderSubMenu, [], function(result) {
            click(result.value + " > ul > li:nth-child(2) > label");

            waitService.waitForMilliseconds(1000);
          });

          execute(colorPickerTest, [], function(result) {
            click(result.value);

            waitService.waitForMilliseconds(1000);
          });

          execute(colorPickerTestSubMenu, [], function(result) {
            click(result.value + " > ul > li:nth-child(4)");

            waitService.waitForMilliseconds(1000);

            click("#controlBar > button:nth-child(2)");

            waitService.waitForMilliseconds(1000);
          });

          execute(colorPickerTest, [], function(result) {
            element(result.value);

            to;

            not;

            be;

            present;
          });
        }
      }
    );
  });
});
