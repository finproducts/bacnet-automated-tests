import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let test= require('../../fixtures/execute/logicBuilder.js').test;

let testSubMenu= require('../../fixtures/execute/logicBuilder.js').testSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;



describe('testBP163b.js',() =>{
  
  it('go to Laguna RTU-1', async () => 
    
    {
waitService.waitForMilliseconds(7000);

frame(null);

waitService.waitForMilliseconds(500);

moveToElement('#barAppContainer > section > ul > li:nth-child(3)',1,2);

mouseButtonClick(0);

waitService.waitForMilliseconds(1000);

containsText('header > h4','Laguna');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','Floor 1');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','Laguna RTU-1');
}

    
  );  
  it('go to bLine', async () => 
    
    {
waitService.waitForMilliseconds(1500);

cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);
}

    
  );  
  it('open create bLine program option', async () => 
    
    {
execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1) > label");

waitService.waitForMilliseconds(1000);
}
);

elementPresent('.form-window.modal-panel.generic-panel','Check the window to create new programs is on screen');

containsText('.form-window.modal-panel.generic-panel #title','Create bLine Program');
}

    
  );  
  it('check  Create bLine Program window', async () => 
    
    {
elementPresent('#title','Check that the window has a title');

elementPresent('#content > div.form-item-holder.textinput > label','Check that the name input has a label');

elementPresent('#content > div.form-item-holder.textinput > input','Check that the name input is present');

elementPresent('#content > div.form-item-holder.textarea > label','Check that the summary has a label');

elementPresent('#content > div.form-item-holder.textarea > textarea','Check that the summary has a text area');

elementPresent('#content > div.form-item-holder.filter > label','Check that 'Program On' has a label');

elementPresent('#content > div.form-item-holder.filter > div > input','Check that 'Program On' has an input');

elementPresent('#content > div.form-item-holder.filter > div > button','Check that 'Program On' has a filter button');

elementPresent('#content > div.form-item-holder.list > label','Check that Points list has a label');

elementPresent('#content > div.form-item-holder.list > select','Check there's a list with the points from the equip');

elementPresent('#content > div.form-item-holder.separator > div > label','Check there'a label to add more than points');

elementPresent('#content > div:nth-child(6) > div > label','Check that Tags option is available');

elementPresent('#content > div:nth-child(6) > div > div','Check that Tags has a checkbox');

elementPresent('#content > div:nth-child(7) > div > label','Check that Project Variable option is available');

elementPresent('#content > div:nth-child(7) > div > div','Check that Project Variable has a checkbox');

elementPresent('#content > div:nth-child(8) > div > label','Check that Absolute Variables option is available');

elementPresent('#content > div:nth-child(8) > div > div','Check that Absolute Variables has a checkbox');

elementPresent('#controlBar > button:nth-child(2)','Check that Okay button is present');

elementPresent('#controlBar > button:nth-child(1)','Check that Cancel button is present');
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys('test');

waitService.waitForMilliseconds(300);

click('#content > div.form-item-holder.list > select > option:nth-child(18)');

waitService.waitForMilliseconds(300);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1500);

click('#controlBar > button');

waitService.waitForMilliseconds(1500);
}

    
  );  
  it('Open program', async () => 
    
    {
execute(edit,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(2000);
}
);

frame(0);

waitService.waitForMilliseconds(500);

elementPresent('#panel');

elementPresent('#panel .variables .number.variable','Check the zoneTempSp variable is present in the section');

containsText('#panel .variables .number.variable span','zoneTempSp','Check the zoneTempSp variable has the correct name');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('delete program', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(500);

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);

execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(2) > label");

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

    
  );  
  
  
});