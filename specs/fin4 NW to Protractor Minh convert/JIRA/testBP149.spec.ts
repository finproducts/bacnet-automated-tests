import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

describe("testBP149.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("colapse menu", async () => {
    getCssProperty(".blocklibrary", "width", function(result) {
      click(".controls i.file.icon");

      waitService.waitForMilliseconds(500);

      click(
        ".unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
      );

      waitService.waitForMilliseconds(500);

      click(".newProgram.modal > div > div > div > span:nth-child(1)");

      waitService.waitForMilliseconds(500);

      click(".collapse.icon.fr");

      waitService.waitForMilliseconds(500);

      element(".blocklibrary");

      to;

      have;

      css("width");

      which;

      does;

      not;

      equal(result.value);
    });
  });
});
