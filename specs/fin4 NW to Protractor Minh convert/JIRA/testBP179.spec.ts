import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let centralPlantAlarm = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarm;

let centralPlantAlarmSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .centralPlantAlarmSubMenu;

describe("testBP179.js", () => {
  it("open a bLine program centralPlantAlarm", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      console.log(result.value);

      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      console.log(result.value);

      console.log(result.value + " > ul > li:nth-child(2)");

      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarm, [], function(result) {
      console.log(result.value);

      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(centralPlantAlarmSubMenu, [], function(result) {
      console.log(result.value);

      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .blocks");
  });
  it("save program", async () => {
    waitService.waitForMilliseconds(500);

    moveToElement(".__start__.block", 2, 2);

    waitService.waitForMilliseconds(200);

    mouseButtonDown(0);

    moveTo(null, 100, 100);

    mouseButtonUp(0);

    waitService.waitForMilliseconds(2500);

    cssClassNotPresent(".controls .icons .save.icon", "disabled");

    waitService.waitForMilliseconds(500);

    click("#panel > section.controls > div.icons > i.save.icon");

    waitService.waitForMilliseconds(1000);

    cssClassPresent(".controls .icons .save.icon", "disabled");
  });
  it("refresh driver", async () => {
    refresh();

    waitService.waitForMilliseconds(2000);

    elementNotPresent(".error.banner");
  });
});
