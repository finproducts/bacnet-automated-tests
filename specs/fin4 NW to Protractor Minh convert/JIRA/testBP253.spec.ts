import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP253.js", () => {
  it("Go to Block Programming bLine", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(6)");

    waitService.waitForMilliseconds(500);
  });
  it("Add macro block", async () => {
    click(".add.panel-button .macro.symbol.mrxs");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .macro", "Check that the macro is added");

    click(".scroller .macro .macro.symbol");

    waitService.waitForMilliseconds(500);
  });
  it("Drag macro block ports to the stage", async () => {
    click("i.cog.icon");

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      "#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(1) > b",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(1) > b",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(1) > b",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .get.boolean.block:nth-child(2)",
      "Check the block for the first port appears on the stage"
    );

    elementPresent(
      ".blocks .get.boolean.block:nth-child(3)",
      "Check the block for the second port appears on the stage"
    );

    elementPresent(
      ".blocks .set.boolean.block:nth-child(4)",
      "Check the block for the third port appears on the stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 50);

        app.set("blocks.1.y", 50);
      },
      [],
      function(result) {}
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 100);

        app.set("blocks.2.y", 50);
      },
      [],
      function(result) {}
    );

    execute(
      function(simple) {
        app.set("blocks.3.x", 150);

        app.set("blocks.3.y", 50);
      },
      [],
      function(result) {}
    );

    click("i.cog.icon");

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      "#panel > section.variables > div.scroller > div:nth-child(4)",
      "Check that no extra variables were added in the variables section"
    );
  });
});
