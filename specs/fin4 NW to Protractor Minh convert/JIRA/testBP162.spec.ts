import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let test = require("../../fixtures/execute/logicBuilder.js").test;

let testSubMenu = require("../../fixtures/execute/logicBuilder.js").testSubMenu;

describe("testBP162.js", () => {
  it("go to bLine", async () => {
    waitService.waitForMilliseconds(5000);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });
  });
  it("open create bLine program option", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "Check the window to create new programs is on screen"
    );

    containsText(
      ".form-window.modal-panel.generic-panel #title",
      "Create Program"
    );
  });
  it("check  Create bLine Program window", async () => {
    elementPresent("#title");

    elementPresent("#content > div.form-item-holder.textinput > label");

    elementPresent("#content > div.form-item-holder.textinput > input");

    elementPresent("#content > div.form-item-holder.textarea > label");

    elementPresent("#content > div.form-item-holder.textarea > textarea");

    elementPresent("#content > div.form-item-holder.filter > label");

    elementPresent("#content > div.form-item-holder.filter > div > input");

    elementPresent("#content > div.form-item-holder.filter > div > button");

    elementPresent("#content > div.form-item-holder.ractive > label");

    elementPresent("#content > div.form-item-holder.ractive > div");

    elementPresent("#content > div.form-item-holder.separator > div > label");

    elementPresent("#content > div:nth-child(6) > div > label");

    elementPresent("#content > div:nth-child(6) > div > div");

    elementPresent("#content > div:nth-child(7) > div > label");

    elementPresent("#content > div:nth-child(7) > div > div");

    elementPresent("#controlBar > button:nth-child(2)");

    elementPresent("#controlBar > button:nth-child(1)");
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys("test");

    waitService.waitForMilliseconds(500);

    click("#content > div.form-item-holder.filter > div > input");

    keys("vav");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );
  });
  it("go back", async () => {
    click(
      ".desktop-content > div.app-nav.vertical > nav > div:nth-child(1) > a"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Open program", async () => {
    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent("#panel");

    elementPresent(
      "#panel > section.variables > div.scroller > div:nth-child(2)"
    );

    containsText(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span",
      "alarm"
    );
  });
  it("Check alarm routine", async () => {
    click(
      "#panel > section.variables > div.scroller > div:nth-child(2) > span"
    );

    waitService.waitForMilliseconds(500);

    elementNotPresent("#stage > div.bLine.banner.mobile.interval > i.cog.icon");

    cssClassPresent("#stage > div.bLine.banner.mobile", "alarm");
  });
  it("delete program", async () => {
    frame(null);

    waitService.waitForMilliseconds(500);

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(4)");

      waitService.waitForMilliseconds(1000);

      click("#controlBar > button:nth-child(2)");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      element(result.value);

      to;

      not;

      be;

      present;
    });
  });
});
