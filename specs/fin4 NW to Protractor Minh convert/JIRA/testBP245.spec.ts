import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp245bLine = require("../../fixtures/programs/BP-245bLine");

let bp245KMC = require("../../fixtures/programs/BP-245KMC");

describe("testBP245.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("Load KMC program for BP-245", async () => {
    loadProgram(bp245KMC);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#b4ae180ec-c6c0-4fdf-8883-b15d3ab725fc",
      "Check that the macro block is present"
    );
  });
  it("Add more inputs to the macro block in KMC", async () => {
    click("#panel .variables .scroller div span");

    waitService.waitForMilliseconds(500);

    click(".tac.mtm button");

    waitService.waitForMilliseconds(500);

    click(".tac.mtm button");

    waitService.waitForMilliseconds(500);

    click(".close .cross.icon");

    waitService.waitForMilliseconds(500);
  });
  it("Check inputs for KMC", async () => {
    elementPresent(
      "#b4ae180ec-c6c0-4fdf-8883-b15d3ab725fc > div.data.sockets > div.input > div:nth-child(3) > input",
      "Check that the input for the first added port exists"
    );

    elementPresent(
      "#b4ae180ec-c6c0-4fdf-8883-b15d3ab725fc > div.data.sockets > div.input > div:nth-child(4) > input",
      "Check that the input for the second added port exists"
    );
  });
  it("Load bLine program for BP-245", async () => {
    loadProgram(bp245bLine);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#bfcf7a50f-2266-42f7-a37d-699b828d2f55",
      "Check that the macro block is present"
    );
  });
  it("Add more inputs to the macro block in bLine", async () => {
    click("#panel .variables .scroller > div.macro > span");

    waitService.waitForMilliseconds(500);

    click(".cog.icon");

    waitService.waitForMilliseconds(500);

    click(".tac.mtm button");

    waitService.waitForMilliseconds(500);

    click(".tac.mtm button");

    waitService.waitForMilliseconds(500);

    click(".cog.icon");

    waitService.waitForMilliseconds(500);
  });
  it("Check inputs for bLine", async () => {
    click("#panel .variables .scroller div:nth-child(1) > i");

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#bfcf7a50f-2266-42f7-a37d-699b828d2f55 > div.data.sockets > div.input > div:nth-child(3) > input",
      "Check that the input for the first added port exists"
    );

    elementPresent(
      "#bfcf7a50f-2266-42f7-a37d-699b828d2f55 > div.data.sockets > div.input > div:nth-child(4) > input",
      "Check that the input for the second added port exists"
    );
  });
});
