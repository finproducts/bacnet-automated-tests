import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let bp191= require('../../fixtures/programs/BP-191');

let bp245KMC= require('../../fixtures/programs/BP-245KMC');



describe('testBP191.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Load bLine program for BP-191', async () => 
    
    {
loadProgram(bp245KMC);

waitService.waitForMilliseconds(500);

loadProgram(bp191);

waitService.waitForMilliseconds(10000);

elementPresent('.boolean.variable','Check that the boolean variable is present in the section');

elementPresent('.number.variable','Check that the number variable is present in the section');

elementPresent('.string.variable','Check that the string variable is present in the section');

elementPresent('.date.variable','Check that the date variable is present in the section');

elementPresent('.datetime.variable','Check that the dateTime variable is present in the section');

elementPresent('.time.variable','Check that the time variable is present in the section');

waitService.waitForMilliseconds(1000);

elementPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745','Check that the macro block is present');
}

    
  );  
  it('Change macroblock output to number', async () => 
    
    {
click('.macro .name');

waitService.waitForMilliseconds(500);

click('.cog.icon');

waitService.waitForMilliseconds(500);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(200);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(2)');

waitService.waitForMilliseconds(200);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','number','Check that the number data type is selected');

click('.cog.icon');
}

    
  );  
  it('Verify number output in main routine', async () => 
    
    {
click('.function:nth-child(1) .routine.symbol');

waitService.waitForMilliseconds(500);

containsText('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div > label','Select a variable','Check that the output doesn\'t display a debug id but 'Select a variable'');

cssClassPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div','number','Check that the output displays number variables');
}

    
  );  
  it('Change macroblock output to string', async () => 
    
    {
click('.macro .name');

waitService.waitForMilliseconds(500);

click('.cog.icon');

waitService.waitForMilliseconds(500);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(200);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(3)');

waitService.waitForMilliseconds(200);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','string','Check that the number data type is selected');

click('.cog.icon');
}

    
  );  
  it('Verify string output in main routine', async () => 
    
    {
click('.function:nth-child(1) .routine.symbol');

waitService.waitForMilliseconds(500);

containsText('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div > label','Select a variable','Check that the output doesn\'t display a debug id but 'Select a variable'');

cssClassPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div','string','Check that the output displays string variables');
}

    
  );  
  it('Change macroblock output to date', async () => 
    
    {
click('.macro .name');

waitService.waitForMilliseconds(500);

click('.cog.icon');

waitService.waitForMilliseconds(500);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(200);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(4)');

waitService.waitForMilliseconds(200);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','date','Check that the number data type is selected');

click('.cog.icon');
}

    
  );  
  it('Verify date output in main routine', async () => 
    
    {
click('.function:nth-child(1) .routine.symbol');

waitService.waitForMilliseconds(500);

containsText('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div > label','Select a variable','Check that the output doesn\'t display a debug id but 'Select a variable'');

cssClassPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div','date','Check that the output displays date variables');
}

    
  );  
  it('Change macroblock output to dateTime', async () => 
    
    {
click('.macro .name');

waitService.waitForMilliseconds(500);

click('.cog.icon');

waitService.waitForMilliseconds(500);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(200);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(5)');

waitService.waitForMilliseconds(200);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','datetime','Check that the number data type is selected');

click('.cog.icon');
}

    
  );  
  it('Verify dateTime output in main routine', async () => 
    
    {
click('.function:nth-child(1) .routine.symbol');

waitService.waitForMilliseconds(500);

containsText('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div > label','Select a variable','Check that the output doesn\'t display a debug id but 'Select a variable'');

cssClassPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div','datetime','Check that the output displays dateTime variables');
}

    
  );  
  it('Change macroblock output to time', async () => 
    
    {
click('.macro .name');

waitService.waitForMilliseconds(500);

click('.cog.icon');

waitService.waitForMilliseconds(500);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(200);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(6)');

waitService.waitForMilliseconds(200);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','time','Check that the number data type is selected');

click('.cog.icon');
}

    
  );  
  it('Verify time output in main routine', async () => 
    
    {
click('.function:nth-child(1) .routine.symbol');

waitService.waitForMilliseconds(500);

containsText('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div > label','Select a variable','Check that the output doesn\'t display a debug id but 'Select a variable'');

cssClassPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div','time','Check that the output displays time variables');
}

    
  );  
  it('Change macroblock output to boolean', async () => 
    
    {
click('.macro .name');

waitService.waitForMilliseconds(500);

click('.cog.icon');

waitService.waitForMilliseconds(500);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(200);

click('.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(4) > div > select > option:nth-child(1)');

waitService.waitForMilliseconds(200);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','boolean','Check that the number data type is selected');

click('.cog.icon');
}

    
  );  
  it('Verify boolean output in main routine', async () => 
    
    {
click('.function:nth-child(1) .routine.symbol');

waitService.waitForMilliseconds(500);

containsText('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div > label','Select a variable','Check that the output doesn\'t display a debug id but 'Select a variable'');

cssClassPresent('#b686aa4cd-0181-496c-9c36-fd35ec3e6745 > div.data.sockets > div.output > div > div','boolean','Check that the output displays boolean variables');
}

    
  );  
  
  
});