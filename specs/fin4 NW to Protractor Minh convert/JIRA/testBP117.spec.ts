import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let bp117 = require("../../fixtures/programs/BP117");

let bp245KMC = require("../../fixtures/programs/BP-245KMC");

describe("testBP117.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("load bLine program", async () => {
    loadProgram(bp117);

    waitService.waitForMilliseconds(500);

    containsText(".language", "bLine");

    waitService.waitForMilliseconds(10000000000);

    elementPresent("#b67825ca9-bf76-4ccb-b671-e2b3cc3a9d95");

    elementPresent("#b955bc146-39bb-43e3-8dd5-94fc0d291a7c");
  });
  it("verify block in library", async () => {
    click(
      '#panel > section.blocklibrary > div.filters > div > input[type="text"]'
    );

    keys("callRoutine");

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div"
    );

    containsText(
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      "CallRoutine"
    );
  });
  it("verify routines in dropdown list", async () => {
    click(
      "#b955bc146-39bb-43e3-8dd5-94fc0d291a7c > div > div.ractive-select > label"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#b955bc146-39bb-43e3-8dd5-94fc0d291a7c > div > div.ractive-select > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    containsText(
      "#b955bc146-39bb-43e3-8dd5-94fc0d291a7c > div > div.ractive-select > label",
      "unnamedRoutine1"
    );
  });
});
