import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP216.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(1000);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(1000);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("Check new added KMC blocks", async () => {
    setValue('.blocklibrary .filters > div > input[type="text"]', "oneshot");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "OneShot",
      "OneShot block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "limiter");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Limiter",
      "Limiter block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "compare");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div:nth-child(1)",
      "Compare",
      "Compare block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "equaltodiff"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "EqualToDiff",
      "EqualToDiff block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "greaterthandiff"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "GreaterThanDiff",
      "GreaterThanDiff block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "lessthandiff"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "LessThanDiff",
      "LessThanDiff block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "within");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Within",
      "Within block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "switch");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Switch",
      "Switch block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "anticycle");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "AntiCycle",
      "AntiCycle block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "counter");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Counter",
      "Counter block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "delayonmake"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "DelayOnMake",
      "DelayOnMake block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue(
      '.blocklibrary .filters > div > input[type="text"]',
      "delayonbreak"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "DelayOnBreak",
      "DelayOnBreak block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "select");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Select",
      "Select block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "reset");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Reset",
      "Reset block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "slope");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "Slope",
      "Slope block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);

    setValue('.blocklibrary .filters > div > input[type="text"]', "minmaxavg");

    waitService.waitForMilliseconds(1000);

    containsText(
      ".blocklibrary .scroller > div > div.contents > div",
      "MinMaxAvg",
      "MinMaxAvg block is present in the libary"
    );

    clearValue('.blocklibrary .filters > div > input[type="text"]');

    waitService.waitForMilliseconds(1000);
  });
});
