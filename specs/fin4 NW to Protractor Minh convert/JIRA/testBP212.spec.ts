import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let test = require("../../fixtures/execute/logicBuilder.js").test;

let testSubMenu = require("../../fixtures/execute/logicBuilder.js").testSubMenu;

let edit = require("../../fixtures/execute/programData.js").edit;

let back = require("../../fixtures/execute/programData.js").back;

describe("testBP212.js", () => {
  it("go to City Center VAV-01 ", async () => {
    waitService.waitForMilliseconds(1500);

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    containsText("header > h4", "City Center");

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    containsText("header > h4", "Floor 1");

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    containsText("header > h4", "City Center AHU-1");
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });
  });
  it("open make new program option", async () => {
    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1) > label");

      waitService.waitForMilliseconds(1000);
    });

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "Check the window to create new programs is on screen"
    );

    containsText(
      ".form-window.modal-panel.generic-panel #title",
      "Create Program"
    );
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys("test");

    waitService.waitForMilliseconds(300);

    click(".form-item-holder.list > select > option:nth-child(49)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    containsText(
      "#title",
      "Success",
      "Check that the program was successfully created"
    );

    click("#controlBar > button");

    waitService.waitForMilliseconds(500);
  });
  it("Open program", async () => {
    execute(edit, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(2000);
    });

    click(".desktop-content > div.frame-content > section > button");

    waitService.waitForMilliseconds(500);

    frame(0);

    waitService.waitForMilliseconds(500);

    containsText(
      ".variables .scroller .grid.variable > span",
      "cityCenterVavProgram",
      "cityCenterVavProgram bundle is presend in the variables section"
    );
  });
  it("Link IT block to start block", async () => {
    click(".__start__ > div > div > div > b");

    waitService.waitForMilliseconds(500);

    moveTo(null, 100, 100);

    waitService.waitForMilliseconds(500);

    mouseButtonClick(0);

    waitService.waitForMilliseconds(500);

    frame(null);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "Suggestion window appears"
    );

    click('.form-item-holder.ractive > div > div > input[type="text"]');

    waitService.waitForMilliseconds(500);

    containsText(
      "#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18) > div > span.label-item",
      "IT",
      "it variable is present in the dropdown list in the suggestion window"
    );

    click(
      "#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(18)"
    );

    waitService.waitForMilliseconds(500);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".blocks .set.string.block",
      "it variable appears as a string block in the stage"
    );

    containsText(
      "#panel > section.variables > div.scroller > div.string.variable > span",
      "it",
      "it variable also appears in the variables section, also as string"
    );

    cssClassPresent(
      ".__start__ > div > div > div",
      "linked",
      "Start block is linked with it block"
    );

    cssClassPresent(
      ".blocks .set.string.block > div.exec.sockets > div.in > div",
      "linked",
      "it block is linked with start block"
    );

    setValue(
      ".blocks .set.string.block > div.data.sockets > div > div > input",
      "test"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Save program and reopen", async () => {
    waitService.waitForMilliseconds(1000);

    click("i.save.icon");

    waitService.waitForMilliseconds(2000);

    cssClassPresent("i.save.icon", "disabled");

    url("http://localhost:8085/");

    waitService.waitForMilliseconds(3000);

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(500);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(2000);
    });

    frame(null);

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".modal-panel.alert-panel",
      "No warning appears when reopening this saved program"
    );

    frame(0);

    waitService.waitForMilliseconds(500);

    elementNotPresent(
      ".error.banner",
      "No error banner is present in the app when reopening this saved program"
    );
  });
  it("delete program", async () => {
    refresh();

    waitService.waitForMilliseconds(3000);

    frame(null);

    waitService.waitForMilliseconds(500);

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(4)");

      waitService.waitForMilliseconds(1000);

      click("#controlBar > button:nth-child(2)");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      element(result.value);

      to;

      not;

      be;

      present;
    });
  });
});
