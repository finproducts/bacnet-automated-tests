import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp2= require('../../fixtures/programs/BP-2');

let bp245KMC= require('../../fixtures/programs/BP-245KMC');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP2.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Verify bug BP-2', async () => 
    
    {
loadProgram(bp2);

waitService.waitForMilliseconds(500);

click('#bdb1625ed-b509-4951-b5a6-64c3b82e6302');

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

waitService.waitForMilliseconds(400);

element('#bf7f471fa-31e3-4d3a-9a98-72eddd85e666 > div.data.sockets > div > div > div > input[type="number"]');

to;

have;

value;

not;

equals('true');
}

    
  );  
  
  
});