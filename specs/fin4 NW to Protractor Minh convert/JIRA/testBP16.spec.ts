import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp16= require('../../fixtures/programs/BP-16');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP16.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Verify bug BP-16', async () => 
    
    {
loadProgram(bp16);

click('#b1985e8ba-7d76-42b7-91e0-b646478d50b8 > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(200);

click('#bcb9c4e32-f0a1-4858-8c36-330e9793a069 > div > div > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#bcb9c4e32-f0a1-4858-8c36-330e9793a069 > div > div > div','linked');

click('#b9b637688-d3e9-4b33-bd49-d1183d7fb4ae > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(200);

click('#b1985e8ba-7d76-42b7-91e0-b646478d50b8 > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#b1985e8ba-7d76-42b7-91e0-b646478d50b8 > div.exec.sockets > div.out > div','linked');

click('#stage > div > div > div.set.boolean.block');

waitService.waitForMilliseconds(1000000);

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

waitService.waitForMilliseconds(1000);

elementNotPresent('#stage > div > div > div.set.boolean.block');

click('#stage > div > div > div.set.number.block');

waitService.waitForMilliseconds(200);

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

waitService.waitForMilliseconds(1000);

elementNotPresent('#stage > div > div > div.set.number.block');

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});