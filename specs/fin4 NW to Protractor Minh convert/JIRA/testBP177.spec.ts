import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP177.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add variable and macroblock", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".scroller .variable.variable",
      "Check that the variable is added"
    );

    click(".add.panel-button .macro.symbol.mrxs");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .macro", "Check that the macro is added");
  });
  it("Drag variable and macroblock to the stage", async () => {
    execute(dragAndDrop, [".scroller .macro > b", ".blocks"]);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      ".scroller .variable > b.variable.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    execute(
      function(simple) {
        app.set("blocks.1.x", 50);

        app.set("blocks.1.y", 50);
      },
      [],
      function(result) {}
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 200);

        app.set("blocks.2.y", 200);
      },
      [],
      function(result) {}
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block",
      "Check the block for the macroblock appears on the stage"
    );

    elementPresent(
      ".blocks .set.variable.block",
      "Check the block for the set variable appears on the stage"
    );
  });
  it("Check priority and property", async () => {
    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div:nth-child(1)",
      "Check the priority icon for macro block"
    );

    click(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div:nth-child(1) > i"
    );

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div:nth-child(1) > div",
      "open",
      "Check that a dropdown list opens when clicking on the macro block priority"
    );

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div:nth-child(2)",
      "Check the property icon for macro block"
    );

    click(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div:nth-child(2) > i"
    );

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      ".blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div > div:nth-child(2) > div",
      "open",
      "Check that a dropdown list opens when clicking on the macro block property"
    );

    waitService.waitForMilliseconds(500);

    click(".blocks .set.variable.block");

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".blocks .set.variable.block > div.top-icons.no-select > div.priority",
      "Check the priority icon for the variable"
    );

    click(
      ".blocks .set.variable.block > div.top-icons.no-select > div.priority"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".blocks .set.variable.block > div.top-icons.no-select > div.priority > div",
      "Check that a dropdown list opens when clickingon the variable priority"
    );
  });
});
