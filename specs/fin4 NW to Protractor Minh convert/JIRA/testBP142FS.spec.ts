import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let ahuFDD = require("../../fixtures/execute/logicBuilder.js").ahuFDD;

let ahuFDDSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .ahuFDDsubMenu;

describe("testBP142FS.js", () => {
  it("Open ahUFDD and go to Alarm routine", async () => {
    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(ahuFDD, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(ahuFDDSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(1)");

      waitService.waitForMilliseconds(3000);

      click(
        "body > app > section > div > div.frame-content > section > button"
      );

      waitService.waitForMilliseconds(500);

      frame(0);

      waitService.waitForMilliseconds(500);

      containsText(
        "#panel > section.controls > div.program-name > span:nth-child(1)",
        "ahuFDD",
        "ahuFDD program is open"
      );
    });

    getText(
      "#panel > section.variables > div > div:nth-child(3) > span > span",
      function(result) {
        if (result.value == "alarm") {
          click(".variables > div > div:nth-child(3) > span > span");

          waitService.waitForMilliseconds(1500);
        } else {
          containsText(
            ".variables > div > div:nth-child(2) > span > span",
            "alarm",
            "Alarm routine is first"
          );

          click(".variables > div > div:nth-child(2) > span > span");

          waitService.waitForMilliseconds(1500);
        }
      }
    );
  });
  it("BP-248 - multiline input", async () => {
    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(2) > textarea",
      "Check that the alarmBlock has a textarea for instructions where you can write on multiple lines"
    );
  });
  it("BP-257 Add marker tag", async () => {
    click(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label"
    );

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(400);

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label",
      "marker",
      "marker option was selected from the dropdown list"
    );

    click(".alarmBlock.block > div.data.sockets > div.input > div.add > input");

    keys("marker");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(6)",
      "Check that the marker tag is added"
    );
  });
  it("BP-270 BP-257 Add boolean tag", async () => {
    click(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label"
    );

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(400);

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label",
      "boolean",
      "boolean option was selected from the dropdown list"
    );

    click(".alarmBlock.block > div.data.sockets > div.input > div.add > input");

    keys("boolean");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(7)",
      "Check that the boolean tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(7) > b",
      "Check that the boolean tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(7)",
      "boolean",
      "Check the boolean tag has the correct name"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(7) > label",
      "Check that boolean tag has a checkbox"
    );
  });
  it("BP-257 Add number tag", async () => {
    click(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label"
    );

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(400);

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label",
      "number",
      "number option was selected from the dropdown list"
    );

    click(".alarmBlock.block > div.data.sockets > div.input > div.add > input");

    keys("number");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(8)",
      "Check that the number tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(8) > b",
      "Check that the number tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(8)",
      "number",
      "Check the number tag has the correct name"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(8) > input",
      "Check the number tag has an input"
    );
  });
  it("BP-257 Add uri tag", async () => {
    click(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label"
    );

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(400);

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label",
      "uri",
      "uri option was selected from the dropdown list"
    );

    click(".alarmBlock.block > div.data.sockets > div.input > div.add > input");

    keys("uri");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(9)",
      "Check that the uri tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(9) > b",
      "Check that the uri tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(9)",
      "uri",
      "Check the uri tag has the correct name"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div.socket.no-select.uri > input",
      "Check the uri tag has an input"
    );
  });
  it("BP-257 Add string tag", async () => {
    moveToElement(".alarmBlock.block", 1, 1);

    waitService.waitForMilliseconds(200);

    mouseButtonDown(0);

    waitService.waitForMilliseconds(200);

    moveTo(null, 0, -100);

    waitService.waitForMilliseconds(200);

    mouseButtonUp(0);

    waitService.waitForMilliseconds(200);

    click(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label"
    );

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(5)"
    );

    waitService.waitForMilliseconds(400);

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div.add > div > label",
      "string",
      "String option was selected from the dropdown list"
    );

    click(".alarmBlock.block > div.data.sockets > div.input > div.add > input");

    keys("string");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(10)",
      "Check that the string tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(10) > b",
      "Check that the string tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(10)",
      "string",
      "Check the string tag has the correct name"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(10) > input",
      "Check the string tag has an input"
    );
  });
  it("BP-269 Check inputs color for added tags", async () => {
    elementPresent(
      ".alarmBlock.block > div.data.sockets > div.input > div.socket.no-select.marker > i.tag.icon",
      "Check that marker tag has an icon and no port"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(7) > b",
      "border-color",
      "rgb(121, 255, 169)",
      "Check that boolean port is green"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(8) > b",
      "border-color",
      "rgb(212, 129, 246)",
      "Check that number port is violet"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div.input > div.socket.no-select.uri > b",
      "border-color",
      "rgb(255, 255, 255)",
      "Check that uri port is gray"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div.input > div:nth-child(10) > b",
      "border-color",
      "rgb(255, 218, 128)",
      "Check that string port is yellow-ish"
    );
  });
});
