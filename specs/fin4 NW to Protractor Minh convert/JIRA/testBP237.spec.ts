import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP237.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add variables and drag them to stage", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .variable.variable", "A variable is added");

    click(".add.panel-button .routine.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .function", "A routine is added");

    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .macro", "A macroblock is added");

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.variable > b.variable.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.variable.block",
      "Variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 346);

        app.set("blocks.1.y", 301);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.function > b",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(".blocks .function.block", "Routine block is on stage");

    execute(
      function(simple) {
        app.set("blocks.2.x", 601);

        app.set("blocks.2.y", 263);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.macro > b",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block",
      "Macroblock is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.3.x", 915);

        app.set("blocks.3.y", 267);
      },
      [],
      function(result) {}
    );
  });
  it("Link blocks", async () => {
    click(".blocks .__start__.block > div > div > div > b");

    waitService.waitForMilliseconds(400);

    click(".blocks .set.variable.block > div.exec.sockets > div.in > div > b");

    waitService.waitForMilliseconds(400);

    click(".blocks .set.variable.block > div.exec.sockets > div.out > div > b");

    waitService.waitForMilliseconds(400);

    click(".blocks .function.block > div > div.in > div > b");

    waitService.waitForMilliseconds(400);

    click(".blocks .function.block > div > div.out > div > b");

    waitService.waitForMilliseconds(400);

    click(
      ".blocks .macro.commandWithDataOutputs.block > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(400);

    elementPresent("#stage > div > svg > path:nth-child(1)");

    elementPresent("#stage > div > svg > path:nth-child(2)");

    elementPresent("#stage > div > svg > path:nth-child(3)");
  });
  it("Select blocks and multiply them", async () => {
    keys(driver, Keys, SHIFT);

    waitService.waitForMilliseconds(300);

    click(".blocks .set.variable.block");

    waitService.waitForMilliseconds(300);

    click(".blocks .function.block");

    waitService.waitForMilliseconds(300);

    click(".blocks .macro.commandWithDataOutputs.block");

    waitService.waitForMilliseconds(300);

    keys(driver, Keys, SHIFT);

    waitService.waitForMilliseconds(300);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(7)",
      "The blocks multiplied the first time"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(10)",
      "The blocks multiplied the second time"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(13)",
      "The blocks multiplied the third time"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(16)",
      "The blocks multiplied the fourth time"
    );
  });
  it("Delete all routine blocks", async () => {
    moveToElement(".blocks", 580, 200);

    waitService.waitForMilliseconds(500);

    mouseButtonDown(0);

    waitService.waitForMilliseconds(500);

    moveToElement(".blocks", 850, 370);

    waitService.waitForMilliseconds(500);

    mouseButtonUp(0);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(500);

    elementNotPresent(".blocks .function.block:nth-child(3)");

    elementNotPresent(".blocks .function.block:nth-child(6)");

    elementNotPresent(".blocks .function.block:nth-child(9)");

    elementNotPresent(".blocks .function.block:nth-child(12)");

    elementNotPresent(".blocks .function.block:nth-child(15)");

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(5) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(7) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(9) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(11) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );
  });
  it("Go to Block Programming KMC B", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Add variables and drag them to stage B", async () => {
    click(".add.panel-button .variable.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .variable.variable", "A variable is added");

    click(".add.panel-button .macro.symbol");

    waitService.waitForMilliseconds(500);

    elementPresent(".scroller .macro", "A macroblock is added");

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.variable > b.variable.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .set.variable.block",
      "Variable setter block is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.1.x", 402);

        app.set("blocks.1.y", 251);
      },
      [],
      function(result) {}
    );

    execute(dragAndDrop, [
      "#panel > section.variables > div.scroller > div.macro > b",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      ".blocks .macro.commandWithDataOutputs.block",
      "Macroblock is on stage"
    );

    execute(
      function(simple) {
        app.set("blocks.2.x", 639);

        app.set("blocks.2.y", 296);
      },
      [],
      function(result) {}
    );
  });
  it("Link blocks B", async () => {
    click(".blocks .set.variable.block > div.exec.sockets > div.out > div > b");

    waitService.waitForMilliseconds(400);

    click(
      ".blocks .macro.commandWithDataOutputs.block > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(400);

    elementPresent("#stage > div > svg > path:nth-child(1)");
  });
  it("Select blocks and multiply them B", async () => {
    keys(driver, Keys, SHIFT);

    waitService.waitForMilliseconds(300);

    click(".blocks .set.variable.block");

    waitService.waitForMilliseconds(300);

    click(".blocks .macro.commandWithDataOutputs.block");

    waitService.waitForMilliseconds(300);

    keys(driver, Keys, SHIFT);

    waitService.waitForMilliseconds(300);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(5)",
      "The blocks multiplied the first time"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(7)",
      "The blocks multiplied the second time"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(9)",
      "The blocks multiplied the third time"
    );

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    keys("c");

    waitService.waitForMilliseconds(200);

    keys("v");

    waitService.waitForMilliseconds(200);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(200);

    elementPresent(
      ".blocks div:nth-child(11)",
      "The blocks multiplied the fourth time"
    );
  });
  it("Delete all routine blocks B", async () => {
    moveToElement(".blocks", 350, 220);

    waitService.waitForMilliseconds(500);

    mouseButtonDown(0);

    waitService.waitForMilliseconds(500);

    moveToElement(".blocks", 450, 400);

    waitService.waitForMilliseconds(500);

    mouseButtonUp(0);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, DELETE);

    waitService.waitForMilliseconds(500);

    elementNotPresent(".blocks .function.block:nth-child(2)");

    elementNotPresent(".blocks .function.block:nth-child(3)");

    elementNotPresent(".blocks .function.block:nth-child(4)");

    elementNotPresent(".blocks .function.block:nth-child(5)");

    elementNotPresent(".blocks .function.block:nth-child(6)");

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(4) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(5) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );

    containsText(
      ".blocks .macro.commandWithDataOutputs.block:nth-child(6) > div.data.sockets > div.input > div:nth-child(1)",
      "input1"
    );
  });
});
