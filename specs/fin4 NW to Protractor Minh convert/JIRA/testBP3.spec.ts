import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp3= require('../../fixtures/programs/BP-3');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP3.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Verify bug BP-3', async () => 
    
    {
loadProgram(bp3);

waitService.waitForMilliseconds(500);

click('#b7af23455-b57a-44db-be06-412292ad1528');

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

elementNotPresent('#b7af23455-b57a-44db-be06-412292ad1528');

click('#b0372959e-a183-4116-adb4-437cf9aa8d39');

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

elementNotPresent('#b0372959e-a183-4116-adb4-437cf9aa8d39');

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});