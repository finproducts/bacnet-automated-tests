import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP142BP.js", () => {
  it("Go to Block Programming bLine", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(5000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(6)");

    waitService.waitForMilliseconds(500);
  });
  it("Drag alarm block to the stage", async () => {
    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "alarm"
    );

    waitService.waitForMilliseconds(800);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(".blocks .alarmBlock.block");

    execute(
      function(simple) {
        app.set("blocks.1.x", 50);

        app.set("blocks.1.y", 50);
      },
      [],
      function(result) {}
    );
  });
  it("BP-248 - multiline input", async () => {
    elementPresent(
      "#stage > div.scroller > div > div.alarmBlock.block > div.data.sockets > div.input > div:nth-child(2) > div > textarea",
      "Check that the alarmBlock has a textarea for instructions where you can write on multiple lines"
    );
  });
  it("BP-257 Add marker tag", async () => {
    click(".alarmBlock.block > div.data.sockets > div > div.add > div > label");

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(400);

    click(".alarmBlock.block > div.data.sockets > div > div.add > input");

    keys("marker");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div.socket.no-select.marker",
      "Check that the marker tag is added"
    );
  });
  it("BP-270 BP-257 Add boolean tag", async () => {
    click(".alarmBlock.block > div.data.sockets > div > div.add > div > label");

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(400);

    click(".alarmBlock.block > div.data.sockets > div > div.add > input");

    keys("boolean");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(6)",
      "Check that the boolean tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(6) > b",
      "Check that the boolean tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(6)",
      "boolean",
      "Check the boolean tag has the correct name"
    );

    elementPresent(
      "#stage > div.scroller > div > div.alarmBlock.block > div.data.sockets > div.input > div.socket.no-select.boolean > div > label",
      "Check that boolean tag has a checkbox"
    );
  });
  it("BP-257 Add number tag", async () => {
    click(".alarmBlock.block > div.data.sockets > div > div.add > div > label");

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(400);

    click(".alarmBlock.block > div.data.sockets > div > div.add > input");

    keys("number");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(7)",
      "Check that the number tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(7) > b",
      "Check that the number tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(7)",
      "number",
      "Check the number tag has the correct name"
    );

    elementPresent(
      '#stage > div.scroller > div > div.alarmBlock.block > div.data.sockets > div.input > div.add > input[type="text"]',
      "Check the number tag has an input"
    );
  });
  it("BP-257 Add uri tag", async () => {
    click(".alarmBlock.block > div.data.sockets > div > div.add > div > label");

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(400);

    click(".alarmBlock.block > div.data.sockets > div > div.add > input");

    keys("uri");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(8)",
      "Check that the uri tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(8) > b",
      "Check that the uri tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(8)",
      "uri",
      "Check the uri tag has the correct name"
    );

    elementPresent(
      "#stage > div.scroller > div > div.alarmBlock.block > div.data.sockets > div.input > div.socket.no-select.uri > div > input",
      "Check the uri tag has an input"
    );
  });
  it("BP-257 Add string tag", async () => {
    click(".alarmBlock.block > div.data.sockets > div > div.add > div > label");

    waitService.waitForMilliseconds(400);

    click(
      "#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(5)"
    );

    waitService.waitForMilliseconds(400);

    click(".alarmBlock.block > div.data.sockets > div > div.add > input");

    keys("string");

    waitService.waitForMilliseconds(400);

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(9)",
      "Check that the string tag is added"
    );

    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(9) > b",
      "Check that the string tag has a linking port"
    );

    containsText(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(9)",
      "string",
      "Check the string tag has the correct name"
    );

    elementPresent(
      "#stage > div.scroller > div > div.alarmBlock.block > div.data.sockets > div.input > div:nth-child(9) > div > input",
      "Check the string tag has an input"
    );
  });
  it("BP-269 Check inputs color for added tags", async () => {
    elementPresent(
      ".alarmBlock.block > div.data.sockets > div > div.socket.no-select.marker > i.tag.icon",
      "Check that marker tag has an icon and no port"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div > div.socket.no-select.boolean > b",
      "border-color",
      "rgb(121, 255, 169)",
      "Check that boolean port is green"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(7) > b",
      "border-color",
      "rgb(212, 129, 246)",
      "Check that number port is violet"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div > div.socket.no-select.uri > b",
      "border-color",
      "rgb(255, 255, 255)",
      "Check that uri port is gray"
    );

    cssProperty(
      ".alarmBlock.block > div.data.sockets > div > div:nth-child(9) > b",
      "border-color",
      "rgb(255, 218, 128)",
      "Check that string port is yellow-ish"
    );
  });
});
