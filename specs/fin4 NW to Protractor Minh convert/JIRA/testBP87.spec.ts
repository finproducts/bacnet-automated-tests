import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp87= require('../../fixtures/programs/BP-87');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP87.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Verifying bug BP-87', async () => 
    
    {
loadProgram(bp87);

waitService.waitForMilliseconds(500);

moveToElement('#b92d6b14d-42bf-4808-9675-a728230d4d5d > div.exec.sockets > div.in > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#b92d6b14d-42bf-4808-9675-a728230d4d5d > div.data.sockets > div > div > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#ba7b93bd4-06d4-4977-ba5c-6f24136b6fe1 > div.exec.sockets > div.in > div > b',7,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#ba7b93bd4-06d4-4977-ba5c-6f24136b6fe1 > div.data.sockets > div.input > div:nth-child(1) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#ba7b93bd4-06d4-4977-ba5c-6f24136b6fe1 > div.data.sockets > div.input > div:nth-child(2) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#ba7b93bd4-06d4-4977-ba5c-6f24136b6fe1 > div.data.sockets > div.input > div:nth-child(3) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#ba7b93bd4-06d4-4977-ba5c-6f24136b6fe1 > div.data.sockets > div.input > div:nth-child(4) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#b00a2ef97-4e7f-4df5-828c-7bfc661d9049 > div > div.input > div:nth-child(1) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#b00a2ef97-4e7f-4df5-828c-7bfc661d9049 > div > div.input > div:nth-child(2) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#b00a2ef97-4e7f-4df5-828c-7bfc661d9049 > div > div.input > div:nth-child(3) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#b00a2ef97-4e7f-4df5-828c-7bfc661d9049 > div > div.input > div:nth-child(4) > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementNotPresent('body > app > ul > li:nth-child(2)');

moveToElement('#ba6754521-959d-4b16-a6c9-c47a127250ec > div.data.sockets > div.output > div > b',4,4);

mouseButtonClick(2);

waitService.waitForMilliseconds(200);

elementPresent('body > app > ul > li:nth-child(2)');

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});