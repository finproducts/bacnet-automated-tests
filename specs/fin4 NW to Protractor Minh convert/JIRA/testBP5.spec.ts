import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let bp5= require('../../fixtures/programs/BP-5');

let app= {};

app.get  =  [object Object];

app.load  =  [object Object];



describe('testBP5.js',() =>{
  
  it('Go to Block Programming', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(5000);

waitForElementPresent('.blocks',1000);
}

    
  );  
  it('Verify bug BP-5', async () => 
    
    {
loadProgram(bp5);

waitService.waitForMilliseconds(500);

click('#bf9d84c6a-d775-4195-bfbb-437c890654a3 > div > div > div > b');

click('#bee6bb5ac-3c4f-4b3b-ab97-874fbe60db3b > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#bee6bb5ac-3c4f-4b3b-ab97-874fbe60db3b > div.exec.sockets > div.in > div','linked');

click('#bee6bb5ac-3c4f-4b3b-ab97-874fbe60db3b > div.exec.sockets > div.out > div > b');

click('#b8e8cc7ae-54be-4428-b0be-d02814c6348b > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#b8e8cc7ae-54be-4428-b0be-d02814c6348b > div.exec.sockets > div.in > div','linked');

click('#b9e46badd-8a86-4cfe-8c17-e5c4c7772d69 > div > div.output > div > b');

click('#bee6bb5ac-3c4f-4b3b-ab97-874fbe60db3b > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

cssClassPresent('#b9e46badd-8a86-4cfe-8c17-e5c4c7772d69 > div > div.output > div','linked');

waitService.waitForMilliseconds(1000);

moveToElement('#stage > div > div > div:nth-child(3)',2,1);

mouseButtonClick(0);

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

waitService.waitForMilliseconds(1000);

elementNotPresent('#b8e8cc7ae-54be-4428-b0be-d02814c6348b','block B not present');

waitService.waitForMilliseconds(500);

cssClassPresent('#b9e46badd-8a86-4cfe-8c17-e5c4c7772d69 > div > div.output > div','linked');

click('#b9e46badd-8a86-4cfe-8c17-e5c4c7772d69');

keys([driver.Keys.DELETE,driver.Keys.DELETE]);

elementNotPresent('#b9e46badd-8a86-4cfe-8c17-e5c4c7772d69');

waitService.waitForMilliseconds(500);
}

    
  );  
  
  
});