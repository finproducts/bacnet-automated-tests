import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

describe("testBP96.js", () => {
  it("Go to Block Programming", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);
  });
  it("rename function on JavaScript", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .function");

    waitService.waitForMilliseconds(500);

    moveToElement(".variables .function .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, SPACE);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    containsText(".variables .function .name", "unnamedFunction1Test");

    waitService.waitForMilliseconds(200);
  });
  it("rename function on KMC", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(500);

    moveToElement(".variables .function .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, SPACE);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    containsText(".variables .function .name", "unnamedRoutine1Test");

    waitService.waitForMilliseconds(200);
  });
  it("rename routine on bLine", async () => {
    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button.mls.danger.button"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.newProgram.modal > div > div > div > span:nth-child(6)"
    );

    waitService.waitForMilliseconds(500);

    click(".add.panel-button .routine");

    waitService.waitForMilliseconds(500);

    moveToElement(".variables .function:nth-child(3) .name", 2, 2);

    click(".pencil.icon");

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, SPACE);

    keys("T");

    keys("e");

    keys("s");

    keys("t");

    keys(driver, Keys, ENTER);

    waitService.waitForMilliseconds(200);

    containsText(
      ".variables .function:nth-child(3) .name",
      "unnamedRoutine1Test"
    );

    waitService.waitForMilliseconds(200);
  });
});
