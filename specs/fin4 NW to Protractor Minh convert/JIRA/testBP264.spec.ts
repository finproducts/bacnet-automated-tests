import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let test= require('../../fixtures/execute/logicBuilder.js').test;

let testSubMenu= require('../../fixtures/execute/logicBuilder.js').testSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;



describe('testBP264.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:8085/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(5000);

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add variables and drag them to stage', async () => 
    
    {
execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller div  b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(2)','The numeric setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',466);

app.set('blocks.1.y',24);
}
,[],function(result)
{}
);
}

    
  );  
  it('Copy number block', async () => 
    
    {
moveToElement('.blocks .set.number.block:nth-child(2)',1,2);

mouseButtonClick(0);

waitService.waitForMilliseconds(300);

keys([driver.Keys.CONTROL]);

waitService.waitForMilliseconds(500);

keys('c');

waitService.waitForMilliseconds(500);

keys('v');

waitService.waitForMilliseconds(500);

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > div > div:nth-child(3)','A copied blocked appears on stage');

elementPresent('#stage > div.scroller > div > div:nth-child(3) > div.data.sockets > div.input > div > div > div > input[type="number"]','The copied numeric block as an input');

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

keys('c');

waitService.waitForMilliseconds(500);

keys('v');

waitService.waitForMilliseconds(500);

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > div > div:nth-child(4)','A copied blocked appears on stage');

elementPresent('#stage > div.scroller > div > div:nth-child(4) > div.data.sockets > div.input > div > div > div > input[type="number"]','The copied numeric block as an input');

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

keys('c');

waitService.waitForMilliseconds(500);

keys('v');

waitService.waitForMilliseconds(500);

keys(driver,Keys,CONTROL);

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > div > div:nth-child(5)','A copied blocked appears on stage');

elementPresent('#stage > div.scroller > div > div:nth-child(5) > div.data.sockets > div.input > div > div > div > input[type="number"]','The copied numeric block as an input');
}

    
  );  
  
  
});