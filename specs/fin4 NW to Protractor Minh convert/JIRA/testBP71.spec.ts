import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let dateSetBlock= ('.blocks .set.date.block');

let timeSetBlock= ('.blocks .set.time.block');

let dateTimeSetBlock= ('.blocks .set.datetime.block');

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let timeDateTest= require('../../fixtures/execute/logicBuilder.js').timeDateTest;

let timeDateTestSubMenu= require('../../fixtures/execute/logicBuilder.js').timeDateTestSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;

let addVariables= require('../../fixtures/execute/programData.js').addVariables;

let addVariablesSubMenu= require('../../fixtures/execute/programData.js').addVariablesSubMenu;



describe('testBP71.js',() =>{
  
  it('Go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);
}

    
  );  
  it('Open Make new program option', async () => 
    
    {
execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1) > label");

waitService.waitForMilliseconds(1000);
}
);

containsText('.form-window.modal-panel.generic-panel #title','Create Program','The window to create a new program is on screen and has the title "Create Program"');

click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys('timeDateTest');

waitService.waitForMilliseconds(300);

click('#content > div.form-item-holder.filter > div > input');

waitService.waitForMilliseconds(500);

keys('vav');

waitService.waitForMilliseconds(300);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#title','Success','Check that the program was successfully created');

click('#controlBar > button');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Open program', async () => 
    
    {
getText('.app-nav.vertical > nav > div:nth-child(6) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive',function(result)
{
console.log(result.value);

if( result.value  ==  "Edit") 
      {
execute(edit,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(3000);

frame(0);

waitService.waitForMilliseconds(200);

containsText('#panel > section.controls > div.program-name > span:nth-child(1)','timeDateTest','timeDateTest program is open');

frame(null);

waitService.waitForMilliseconds(200);
}
);
}

    else 
        {
refresh();

waitService.waitForMilliseconds(5000);

frame(null);

waitService.waitForMilliseconds(500);

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);

execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(2) > label");

waitService.waitForMilliseconds(1000);
}
);

execute(timeDateTest,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1)");

waitService.waitForMilliseconds(1000);

frame(0);

waitService.waitForMilliseconds(200);

containsText('#panel > section.controls > div.program-name > span:nth-child(1)','timeDateTest','timeDateTest program is open');

frame(null);

waitService.waitForMilliseconds(200);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(3)");

waitService.waitForMilliseconds(1000);
}
);
}

      
}
);
}

    
  );  
  it('Add datetime variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(700);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(3) > label");

waitService.waitForMilliseconds(700);
}
);

elementPresent('.form-window.modal-panel.generic-panel','The window to add variables is on screen');

click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(400);

keys('time');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(5)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(700);

containsText('#title','Success','The variable was successfully added');

click('#controlBar > button');

waitService.waitForMilliseconds(500);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(700);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(700);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(3) > label");

waitService.waitForMilliseconds(700);
}
);

elementPresent('.form-window.modal-panel.generic-panel','The window to add variables is on screen');

click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(400);

keys('date');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(4)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(700);

containsText('#title','Success','The variable was successfully added');

click('#controlBar > button');

waitService.waitForMilliseconds(500);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(700);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(700);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(3) > label");

waitService.waitForMilliseconds(700);
}
);

elementPresent('.form-window.modal-panel.generic-panel','The window to add variables is on screen');

click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(400);

keys('dateTime');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(6)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(700);

containsText('#title','Success','The variable was successfully added');

click('#controlBar > button');

waitService.waitForMilliseconds(10000);

frame(0);

waitService.waitForMilliseconds(500);

elementPresent('.variables .time.variable','Time variable was added in the scroller');

elementPresent('.variables .date.variable','Date variable was added in the scroller');

elementPresent('.variables .datetime.variable','DateTime variable was added in the scroller');
}

    
  );  
  it('Drag the variables to the stage using suggestion tool', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(200);

moveTo(null,20,20);

waitService.waitForMilliseconds(400);

mouseButtonClick(0);

waitService.waitForMilliseconds(800);

frame(null);

waitService.waitForMilliseconds(200);

elementPresent('.modal-panel','Add Points to bLine Program form appears on screen');

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

click('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item');

waitService.waitForMilliseconds(200);

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

click('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item');

waitService.waitForMilliseconds(200);

click('#content > div.form-item-holder.ractive > div > div > input[type="text"]');

waitService.waitForMilliseconds(500);

click('#ractive-multiselect-dropdown-container > ul.dropdown.open > li:nth-child(2) > div > span.label-item');

waitService.waitForMilliseconds(200);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(2500);

frame(0);

waitService.waitForMilliseconds(400);

elementPresent(timeSetBlock,'Time Setter block is on stage');

elementPresent(dateSetBlock,'Date Setter block is on stage');

elementPresent(dateTimeSetBlock,'DateTime Setter block is on stage');

moveToElement(dateTimeSetBlock,1,1);

waitService.waitForMilliseconds(200);

mouseButtonDown(0);

waitService.waitForMilliseconds(200);

moveTo(null,100,400);

mouseButtonUp(0);

moveToElement(dateSetBlock,1,1);

waitService.waitForMilliseconds(200);

mouseButtonDown(0);

waitService.waitForMilliseconds(200);

moveTo(null,100,200);

waitService.waitForMilliseconds(200);

mouseButtonUp(0);

waitService.waitForMilliseconds(200);
}

    
  );  
  it('Check picker for time block', async () => 
    
    {
click(timeSetBlock  +  " > div.data.sockets > div.input > div > div");

waitService.waitForMilliseconds(200);

cssClassPresent(timeSetBlock  +  " > div.data.sockets > div.input > div > div > div",'open','Check that time picker is open');

elementPresent(timeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div",'Check that the time is present in the picker');

elementPresent(timeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div",'Check that the clock is present in the picker');

elementPresent(timeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div > div.hours",'Check there's an option to edit the hour');

elementPresent(timeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div > div.minutes",'Check there's an option to edit the minutes');

elementPresent(timeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div > div.meridiem",'Check there's an option to switch between AM and PM');

click(timeSetBlock  +  " > div.data.sockets > div.input > div > div");

waitService.waitForMilliseconds(200);
}

    
  );  
  it('Check picker for date block', async () => 
    
    {
console.log(dateSetBlock  +  " > div.data.sockets > div.input > div > div");

click(dateSetBlock  +  " > div.data.sockets > div.input > div > div");

waitService.waitForMilliseconds(200);

cssClassPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div",'open','Check that the date picker is open');

elementPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.year",'Check that the Year option is available');

elementPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.date.active > span",'Check that the week day is displayed');

elementPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.date.active > div",'Check that the Month and Date is displayed');

elementNotPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.time",'Check that the Time option is NOT available for the Date block');

elementPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor",'Check that a calendar is present');

elementPresent(dateSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div.monthyear",'Check that the month and year is present at the top of the calendar');

click(dateSetBlock  +  " > div.data.sockets > div.input > div > div");

waitService.waitForMilliseconds(200);
}

    
  );  
  it('Check picker for dateTime block', async () => 
    
    {
click(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div");

waitService.waitForMilliseconds(200);

cssClassPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div",'open','Check that the date picker is open');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.year",'Check that the Year option is available');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.date.active > span",'Check that the week day is displayed');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.date.active > div",'Check that the Month and Date is displayed');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.time",'Check that the Time option is available for the DateTime block');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor",'Check that a calendar is present');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div.monthyear",'Check that the month and year is present at the top of the calendar');

click(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.header > div.time");

waitService.waitForMilliseconds(500);

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div.clock",'The content switched from calendar to clock');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div > div.hours",'Check there's an option to edit the hour');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div > div.minutes",'Check there's an option to edit the minutes');

elementPresent(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div > div > div > div.editor > div > div.meridiem",'Check there's an option to switch between AM and PM');

click(dateTimeSetBlock  +  " > div.data.sockets > div.input > div > div");

waitService.waitForMilliseconds(200);
}

    
  );  
  it('Delete program', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(200);

getText('.app-nav.vertical > nav > div:nth-child(6) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive',function(result)
{
console.log(result.value);

if( result.value  ==  "Edit") 
      {
click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1) > a > div > label');

waitService.waitForMilliseconds(1500);

execute(timeDateTest,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(timeDateTestSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(timeDateTest,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

    else 
        {
refresh();

waitService.waitForMilliseconds(5000);

frame(null);

waitService.waitForMilliseconds(500);

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);

execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(2) > label");

waitService.waitForMilliseconds(1000);
}
);

execute(timeDateTest,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(timeDateTestSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(timeDateTest,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

      
}
);
}

    
  );  
  
  
});