import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let logicBuilder = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilder;

let logicBuilderSubMenu = require("../../fixtures/execute/logicBuilder.js")
  .logicBuilderSubMenu;

let test = require("../../fixtures/execute/logicBuilder.js").test;

let testSubMenu = require("../../fixtures/execute/logicBuilder.js").testSubMenu;

let edit = require("../../fixtures/execute/programData.js").edit;

let back = require("../../fixtures/execute/programData.js").back;

describe("testBP267.js", () => {
  it("go to City Center AHU-1 ", async () => {
    waitService.waitForMilliseconds(5000);

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1) > a > div.vertical.flex.non-auto > h4"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > header > h4",
      "City Center"
    );

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1) > a"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > header > h4",
      "Floor 1"
    );

    click(
      "body > app > section > div > div.main-content.can-scroll > div.app-view.vertical.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    containsText("header > h4", "City Center AHU-1");
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    cssClassPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is closed"
    );

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    cssClassNotPresent(
      ".app-nav.vertical",
      "hidden",
      "Check that the menu is open"
    );

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });
  });
  it("open create bLine program option", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      ".form-window.modal-panel.generic-panel",
      "Check the window to create new programs is on screen"
    );

    containsText(
      ".form-window.modal-panel.generic-panel #title",
      "Create Program"
    );
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys("test");

    waitService.waitForMilliseconds(500);

    click(
      "#content > div.form-item-holder.list > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );
  });
  it("Open program and drag variable to the stage", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    frame(0);

    waitService.waitForMilliseconds(500);

    execute(dragAndDrop, [
      ".scroller .boolean.variable  b.boolean.set.block",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(".blocks .set.boolean.block");

    execute(
      function(simple) {
        app.set("blocks.1.x", 350);

        app.set("blocks.1.y", 250);
      },
      [],
      function(result) {}
    );
  });
  it("Link boolean block and save program", async () => {
    click(".__start__.block > div > div > div > b");

    waitService.waitForMilliseconds(200);

    click(".blocks .set.boolean.block > div.exec.sockets > div.in > div > b");

    waitService.waitForMilliseconds(800);

    elementPresent(
      "#stage > div > svg > path",
      "Check that a link is created between start block and boolean set block "
    );

    getValue(
      '#stage > div.scroller > div > div.set.boolean.block > div.data.sockets > div.input > div > div > label > input[type="checkbox"]',
      function(result) {
        if (result.value == "on") {
          click(
            "#stage > div.scroller > div > div.set.boolean.block > div.data.sockets > div.input > div > div > label"
          );
        }
      }
    );

    waitService.waitForMilliseconds(5000000);

    cssClassNotPresent(
      "#panel > section.controls > div.icons > i.save.icon",
      "disabled",
      "Check that the save icon is now enabled"
    );

    click("#panel > section.controls > div.icons > i.save.icon");

    waitService.waitForMilliseconds(500);

    cssClassPresent(
      "#panel > section.controls > div.icons > i.save.icon",
      "disabled",
      "Check that the save icon was clicked and is now inactive"
    );
  });
  it("refresh driver and check for errors", async () => {
    refresh();

    waitService.waitForMilliseconds(3500);

    elementNotPresent(
      "body > app > div.alert.modal > div > div",
      "Check that an error is not thrown when refreshing the program"
    );

    elementNotPresent(
      "#stage > div.error.banner",
      "Check also that an error banner is not present"
    );
  });
  it("delete program", async () => {
    frame(null);

    waitService.waitForMilliseconds(500);

    click(
      ".desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    execute(logicBuilder, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1500);
    });

    execute(logicBuilderSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(2) > label");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      click(result.value);

      waitService.waitForMilliseconds(1000);
    });

    execute(testSubMenu, [], function(result) {
      click(result.value + " > ul > li:nth-child(4)");

      waitService.waitForMilliseconds(1000);

      click("#controlBar > button:nth-child(2)");

      waitService.waitForMilliseconds(1000);
    });

    execute(test, [], function(result) {
      element(result.value);

      to;

      not;

      be;

      present;
    });
  });
});
