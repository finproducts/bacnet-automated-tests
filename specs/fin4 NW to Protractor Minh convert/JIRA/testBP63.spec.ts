import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let test= require('../../fixtures/execute/logicBuilder.js').test;

let testSubMenu= require('../../fixtures/execute/logicBuilder.js').testSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;

let addVariables= require('../../fixtures/execute/programData.js').addVariables;

let addVariablesSubMenu= require('../../fixtures/execute/programData.js').addVariablesSubMenu;



describe('testBP63.js',() =>{
  
  it('go to City Center AHU-1 ', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center','Browsed to City Center');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','Floor 1','Browsed to Floor 1');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center AHU-1','Browsed to City Center AHU-1');
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);
}

    
  );  
  it('open create bLine program option', async () => 
    
    {
execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1) > label");

waitService.waitForMilliseconds(1000);
}
);

elementPresent('.form-window.modal-panel.generic-panel','Check the window to create new programs is on screen');

containsText('.form-window.modal-panel.generic-panel #title','Create bLine Program',''Create bLine Program' has the correct title');
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys('test');

waitService.waitForMilliseconds(300);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#title','Success','Check that the program was successfully created');

click('#controlBar > button');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add number variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','number');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(1)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

setValue('#content > div.form-item-holder.numericstepper > input','1');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(12) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','number','Check that the 'number' variable is added');
}

    
  );  
  it('Add boolean variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','boolean');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(12) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','bool','Check that the 'boolean' variable is added');
}

    
  );  
  it('Add string variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','string');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(3)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

setValue('#content > div.form-item-holder.textinput > input','random');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(14) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','string','Check that the 'string' variable is added');
}

    
  );  
  it('Open program', async () => 
    
    {
execute(edit,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(2500);
}
);

frame(0);

waitService.waitForMilliseconds(500);

containsText('#panel > section.variables > div.scroller > div.function.viewing > span > span','main','Check that the main routine is first and open');

containsText('#panel > section.variables > div.scroller > div:nth-child(2) > span > span','alarm','Check that the alarm routine is second and not open');
}

    
  );  
  it('Drag number setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .number.variable b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block','Number setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',238);

app.set('blocks.1.y',174);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag number getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .number.variable b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block','Number getter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',479);

app.set('blocks.2.y',202);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag boolean setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .boolean.variable  b.boolean.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.boolean.block','Boolean setter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',238);

app.set('blocks.3.y',295);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag boolean getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .boolean.variable b.boolean.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.boolean.block','Boolean getter block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',479);

app.set('blocks.4.y',273);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag string setter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .string.variable b.string.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.string.block','String setter block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',238);

app.set('blocks.5.y',401);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag string getter block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .string.variable b.string.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.string.block','String getter block is on stage');

execute(function(simple)
{
app.set('blocks.6.x',479);

app.set('blocks.6.y',370);
}
,[],function(result)
{}
);
}

    
  );  
  it('Link blocks to check color', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.number.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.number.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.string.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .get.boolean.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.boolean.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .get.number.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.number.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .get.string.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.string.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(400);

cssClassPresent('#stage > div.scroller > svg > path:nth-child(1)','exec','Exec link between program start block and boolean block');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(2)','exec','Exec link between boolean block and number block');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(3)','exec','Exec link between number block and dateTime block');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(4)','boolean','Data link between boolean getter and setter');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(5)','number','Data link between number getter and setter');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(6)','string','Data link between string getter and setter');

cssProperty('#stage > div.scroller > svg > path:nth-child(1)','stroke','rgb(204, 204, 204)','Correct color for link between program start block and boolean block');

cssProperty('#stage > div.scroller > svg > path:nth-child(2)','stroke','rgb(204, 204, 204)','Correct color for link between boolean block and number block');

cssProperty('#stage > div.scroller > svg > path:nth-child(3)','stroke','rgb(204, 204, 204)','Correct color for link between number block and string block');

cssProperty('#stage > div.scroller > svg > path:nth-child(4)','stroke','rgb(67, 255, 134)','Correct color for link between boolean getter and setter');

cssProperty('#stage > div.scroller > svg > path:nth-child(5)','stroke','rgb(195, 79, 242)','Correct color for link between number getter and setter');

cssProperty('#stage > div.scroller > svg > path:nth-child(6)','stroke','rgb(255, 203, 77)','Correct color for link between string getter and setter');

cssProperty('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b','background-color','rgba(0, 0, 0, 0)','The boolean setter input port has the correct color');

cssProperty('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b','background-color','rgba(0, 0, 0, 0)','The boolean setter output port has the correct color');

cssProperty('.blocks .set.number.block > div.exec.sockets > div.in > div > b','background-color','rgba(0, 0, 0, 0)','The number setter input port has the correct color');

cssProperty('.blocks .set.number.block > div.exec.sockets > div.out > div > b','background-color','rgba(0, 0, 0, 0)','The number setter output port has the correct color');

cssProperty('.blocks .set.string.block > div.exec.sockets > div.in > div > b','background-color','rgba(0, 0, 0, 0)','The string setter input port has the correct color');

cssProperty('.blocks .get.boolean.block > div > div.output > div > b','background-color','rgba(121, 255, 169, 1)','The boolean getter data port has the correct color');

cssProperty('.blocks .set.boolean.block > div.data.sockets > div > div > b','background-color','rgba(121, 255, 169, 1)','The boolean setter data port has the correct color');

cssProperty('.blocks .get.number.block > div > div.output > div > b','background-color','rgba(212, 129, 246, 1)','The number getter data port has the correct color');

cssProperty('.blocks .set.number.block > div.data.sockets > div > div > b','background-color','rgba(212, 129, 246, 1)','The number setter data port has the correct color');

cssProperty('.blocks .get.string.block > div > div.output > div > b','background-color','rgba(255, 218, 128, 1)','The string getter data port has the correct color');

cssProperty('.blocks .set.string.block > div.data.sockets > div > div > b','background-color','rgba(255, 218, 128, 1)','The string setter data port has the correct color');
}

    
  );  
  it('delete program', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(200);

click('body > app > div.desktop-content > div.app-nav.vertical > nav > div:nth-child(1) > a > div > label');

waitService.waitForMilliseconds(500);

execute(test,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

    
  );  
  
  
});