import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../../../helpers/nwapis";
import { waitService } from "../../../../helpers/wait-service";
import Fin4PO from "../../../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("testBP232.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:8085/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Drag div block to the stage", async () => {
    waitService.waitForMilliseconds(1000);

    setValue(
      '#panel > section.blocklibrary > div.filters > div > input[type="text"]',
      "div"
    );

    waitService.waitForMilliseconds(800);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div.scroller > div > div.contents > div:nth-child(1)",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(".blocks .divide.block");
  });
  it("Move block", async () => {
    execute(
      function(simple) {
        app.set("blocks.1.x", 300);

        app.set("blocks.1.y", 300);
      },
      [],
      function(result) {}
    );
  });
  it("Write negative numbers in inputs for div block", async () => {
    clearValue(
      '.divide.block .data.sockets div.input > div:nth-child(1) > div > input[type="number"]'
    );

    waitService.waitForMilliseconds(200);

    click(
      '.divide.block .data.sockets div.input > div:nth-child(1) > div > input[type="number"]'
    );

    waitService.waitForMilliseconds(200);

    keys("-20");

    waitService.waitForMilliseconds(500);

    value(
      '.divide.block .data.sockets div.input > div:nth-child(1) > div > input[type="number"]',
      "-20"
    );
  });
});
