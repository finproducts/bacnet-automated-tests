import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let ahuFDD= require('../../fixtures/execute/logicBuilder.js').ahuFDD;

let ahuFDDSubMenu= require('../../fixtures/execute/logicBuilder.js').ahuFDDsubMenu;



describe('testBP215.js',() =>{
  
  it('open ahuFDD', async () => 
    
    {
cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);

execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(2) > label");

waitService.waitForMilliseconds(1000);
}
);

execute(ahuFDD,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(ahuFDDSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1)");

waitService.waitForMilliseconds(2000);
}
);

frame(0);

waitService.waitForMilliseconds(500);

elementPresent('.scroller .blocks');
}

    
  );  
  it('Switch to Alarm routine and check the alarm block', async () => 
    
    {
click('#panel .variables .scroller .function .routine.symbol');

waitService.waitForMilliseconds(400);

click('#panel > section.variables > div.scroller > div:nth-child(2) > i');

waitService.waitForMilliseconds(1500);

value('.blocks .alarmBlock div.data.sockets div div:nth-child(1) input','102');

value('.blocks .alarmBlock div.data.sockets div div:nth-child(2) input','1. Check wiring on actuator damper
2. Grease up dampers
3. Verify programming is correct');

value('.blocks .alarmBlock div.data.sockets div div:nth-child(3) input','5');

value('.blocks .alarmBlock div.data.sockets div div:nth-child(4) input','Econ Stuck Open');
}

    
  );  
  
  
});