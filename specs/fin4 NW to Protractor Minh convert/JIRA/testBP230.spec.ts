import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../../../helpers/nwapis';
import {waitService} from '../../../../helpers/wait-service';
import Fin4PO from '../../../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let logicBuilder= require('../../fixtures/execute/logicBuilder.js').logicBuilder;

let logicBuilderSubMenu= require('../../fixtures/execute/logicBuilder.js').logicBuilderSubMenu;

let test= require('../../fixtures/execute/logicBuilder.js').test;

let testSubMenu= require('../../fixtures/execute/logicBuilder.js').testSubMenu;

let edit= require('../../fixtures/execute/programData.js').edit;

let back= require('../../fixtures/execute/programData.js').back;

let addVariables= require('../../fixtures/execute/programData.js').addVariables;

let addVariablesSubMenu= require('../../fixtures/execute/programData.js').addVariablesSubMenu;



describe('testBP230.js',() =>{
  
  it('go to City Center AHU-1 ', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center','Browsed to City Center');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','Floor 1','Browsed to Floor 1');

click('#equipList > li:nth-child(1) > a > h4');

waitService.waitForMilliseconds(1000);

containsText('header > h4','City Center AHU-1','Browsed to City Center AHU-1');
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

cssClassPresent('.app-nav.vertical','hidden','Check that the menu is closed');

click('.desktop-content > div.tabbar.horizontal.middle.center > button:nth-child(3)');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.app-nav.vertical','hidden','Check that the menu is open');

execute(logicBuilder,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1500);
}
);
}

    
  );  
  it('open create bLine program option', async () => 
    
    {
execute(logicBuilderSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(1) > label");

waitService.waitForMilliseconds(1000);
}
);

elementPresent('.form-window.modal-panel.generic-panel','Check the window to create new programs is on screen');

containsText('.form-window.modal-panel.generic-panel #title','Create bLine Program',''Create bLine Program' has the correct title');
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys('test');

waitService.waitForMilliseconds(300);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#title','Success','Check that the program was successfully created');

click('#controlBar > button');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add date variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','date');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(4)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(12) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','date','Check that the 'date' variable is added');
}

    
  );  
  it('Add time variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','time');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(5)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);

containsText('.desktop-content > div.app-nav.vertical > nav > div:nth-child(13) > a > div > label > span.nav-label.flex.italic.margin.normal.l-side.non-auto.single-line-text.non-interactive','time','Check that the 'date' variable is added');
}

    
  );  
  it('Add dateTime variables', async () => 
    
    {
execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariables,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(addVariablesSubMenu,[],function(result)
{
click(result.value  +  " ul > li:nth-child(3) > label ");

waitService.waitForMilliseconds(1000);
}
);

setValue('#content > div.form-item-holder.textinput > input','dateTime');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.combobox > select > option:nth-child(6)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#controlBar > button');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Open program', async () => 
    
    {
execute(edit,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(2500);
}
);

frame(0);

waitService.waitForMilliseconds(500);

containsText('#panel > section.variables > div.scroller > div.function.viewing > span > span','main','Check that the main routine is first and open');

containsText('#panel > section.variables > div.scroller > div:nth-child(2) > span > span','alarm','Check that the alarm routine is second and not open');
}

    
  );  
  it('Drag date SET block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .date.variable b.date.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.date.block','Date setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',277);

app.set('blocks.1.y',113);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag date GET block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .date.variable b.date.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.date.block','Date getter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',135);

app.set('blocks.2.y',203);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag time SET block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .time.variable  b.time.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.time.block','Time setter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',501);

app.set('blocks.3.y',147);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag time GET block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .time.variable b.time.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.time.block','Time getter block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',348);

app.set('blocks.4.y',220);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag dateTime SET block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .datetime.variable b.datetime.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.datetime.block','DateTime setter block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',710);

app.set('blocks.5.y',191);
}
,[],function(result)
{}
);
}

    
  );  
  it('Drag dateTime GET block to the stage', async () => 
    
    {
execute(dragAndDrop,['.variables .scroller .datetime.variable b.datetime.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.datetime.block','DateTime getter block is on stage');

execute(function(simple)
{
app.set('blocks.6.x',570);

app.set('blocks.6.y',276);
}
,[],function(result)
{}
);
}

    
  );  
  it('Check date block functionality', async () => 
    
    {
click('.blocks .set.date.block > div.data.sockets > div > div > div');

waitService.waitForMilliseconds(500);

cssClassPresent('.blocks .set.date.block > div.data.sockets > div > div > div > div','open','Time picker is open');

elementNotPresent('.blocks .set.date.block > div.data.sockets > div > div > div > div > div > div.header > div.time','Check that a time option is NOT present');

click('.blocks .set.date.block  > div.data.sockets > div > div > div > div > div > div.header > div.year');

waitService.waitForMilliseconds(500);

cssClassPresent('.blocks .set.date.block  > div.data.sockets > div > div > div > div > div > div.header > div.year','active','Years option is selected');

elementPresent('.blocks .set.date.block  > div.data.sockets > div > div > div > div > div > div.editor > div.years','The view switched from calendar to years');

click('.blocks .set.date.block > div.data.sockets > div > div > div > div > div > div.header > div.date > span');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.blocks .set.date.block  > div.data.sockets > div > div > div > div > div > div.header > div.year','active','Years option is no longer selected after clicking on the day');

elementNotPresent('.blocks .set.date.block  > div.data.sockets > div > div > div > div > div > div.editor > div.years','Years view is no longer on screen');

cssClassPresent('.blocks .set.date.block > div.data.sockets > div > div > div > div > div > div.header > div.date','active','Date option is selected');

elementPresent('.blocks .set.date.block > div.data.sockets > div > div > div > div > div > div.editor > div.days > div.dates','The calendar view is on screen instead of Years');
}

    
  );  
  it('Check dateTime block functionality', async () => 
    
    {
click('.blocks .set.datetime.block > div.data.sockets > div > div > div');

waitService.waitForMilliseconds(500);

cssClassPresent('.blocks .set.datetime.block > div.data.sockets > div > div > div > div','open','Time picker is open');

elementPresent('.blocks .set.datetime.block > div.data.sockets > div > div > div > div > div > div.header > div.time','Check that a time option is present');

click('.blocks .set.datetime.block  > div.data.sockets > div > div > div > div > div > div.header > div.year');

waitService.waitForMilliseconds(500);

cssClassPresent('.blocks .set.datetime.block  > div.data.sockets > div > div > div > div > div > div.header > div.year','active','Years option is selected');

elementPresent('.blocks .set.datetime.block  > div.data.sockets > div > div > div > div > div > div.editor > div.years','The view switched from calendar to years');

click('.blocks .set.datetime.block > div.data.sockets > div > div > div > div > div > div.header > div.date > span');

waitService.waitForMilliseconds(500);

cssClassNotPresent('.blocks .set.datetime.block  > div.data.sockets > div > div > div > div > div > div.header > div.year','active','Years option is no longer selected after clicking on the day');

elementNotPresent('.blocks .set.datetime.block  > div.data.sockets > div > div > div > div > div > div.editor > div.years','Years view is no longer on screen');

cssClassPresent('.blocks .set.datetime.block > div.data.sockets > div > div > div > div > div > div.header > div.date','active','Date option is selected');

elementPresent('.blocks .set.datetime.block > div.data.sockets > div > div > div > div > div > div.editor > div.days > div.dates','The calendar view is on screen instead of Years');
}

    
  );  
  it('Link blocks to check color', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.date.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.date.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.time.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.time.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.datetime.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .get.date.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.date.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .get.time.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.time.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .get.datetime.block > div > div.output > div > b');

waitService.waitForMilliseconds(400);

click('.blocks .set.datetime.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(400);

cssClassPresent('#stage > div.scroller > svg > path:nth-child(1)','exec','Exec link between program start block and date block');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(2)','exec','Exec link between date block and time block');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(3)','exec','Exec link between time block and dateTime block');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(4)','date','Data link between date getter and setter');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(5)','time','Data link between time getter and setter');

cssClassPresent('#stage > div.scroller > svg > path:nth-child(6)','datetime','Data link between dateTime getter and setter');

cssProperty('#stage > div.scroller > svg > path:nth-child(1)','stroke','rgb(204, 204, 204)','Correct color for link between program start block and date block');

cssProperty('#stage > div.scroller > svg > path:nth-child(2)','stroke','rgb(204, 204, 204)','Correct color for link between date block and time block');

cssProperty('#stage > div.scroller > svg > path:nth-child(3)','stroke','rgb(204, 204, 204)','Correct color for link between time block and dateTime block');

cssProperty('#stage > div.scroller > svg > path:nth-child(4)','stroke','rgb(77, 255, 220)','Correct color for link between date getter and setter');

cssProperty('#stage > div.scroller > svg > path:nth-child(5)','stroke','rgb(77, 255, 220)','Correct color for link between time getter and setter');

cssProperty('#stage > div.scroller > svg > path:nth-child(6)','stroke','rgb(77, 255, 220)','Correct color for link between dateTime getter and setter');

cssProperty('.blocks .set.date.block > div.exec.sockets > div.in > div > b','background-color','rgba(0, 0, 0, 0)','The date setter input port has the correct color');

cssProperty('.blocks .set.date.block > div.exec.sockets > div.out > div > b','background-color','rgba(0, 0, 0, 0)','The date setter output port has the correct color');

cssProperty('.blocks .set.time.block > div.exec.sockets > div.in > div > b','background-color','rgba(0, 0, 0, 0)','The time setter input port has the correct color');

cssProperty('.blocks .set.time.block > div.exec.sockets > div.out > div > b','background-color','rgba(0, 0, 0, 0)','The time setter output port has the correct color');

cssProperty('.blocks .set.datetime.block > div.exec.sockets > div.in > div > b','background-color','rgba(0, 0, 0, 0)','The dateTime setter input port has the correct color');

cssProperty('.blocks .get.date.block > div > div.output > div > b','background-color','rgba(128, 255, 230, 1)','The date getter data port has the correct color');

cssProperty('.blocks .set.date.block > div.data.sockets > div > div > b','background-color','rgba(128, 255, 230, 1)','The date setter data port has the correct color');

cssProperty('.blocks .get.time.block > div > div.output > div > b','background-color','rgba(128, 255, 230, 1)','The time getter data port has the correct color');

cssProperty('.blocks .set.time.block > div.data.sockets > div > div > b','background-color','rgba(128, 255, 230, 1)','The time setter data port has the correct color');

cssProperty('.blocks .get.datetime.block > div > div.output > div > b','background-color','rgba(128, 255, 230, 1)','The dateTime getter data port has the correct color');

cssProperty('.blocks .set.datetime.block > div.data.sockets > div > div > b','background-color','rgba(128, 255, 230, 1)','The dateTime setter data port has the correct color');
}

    
  );  
  it('delete program', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(200);

click('body > app > div.desktop-content > div.app-nav.vertical > nav > div:nth-child(1) > a > div > label');

waitService.waitForMilliseconds(500);

execute(test,[],function(result)
{
click(result.value);

waitService.waitForMilliseconds(1000);
}
);

execute(testSubMenu,[],function(result)
{
click(result.value  +  " > ul > li:nth-child(4)");

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);
}
);

execute(test,[],function(result)
{
element(result.value);

to;

not;

be;

present;
}
);
}

    
  );  
  
  
});