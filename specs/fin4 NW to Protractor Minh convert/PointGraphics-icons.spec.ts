import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let pointGraphicsButton = po.pointGraphicsButton;

let query =
  "readAll(point and equipRef==@1eeaf2cd-f2e9b66d).sort((a,b) => if(a->navName > b->navName) 1 else -1)[0..2]";

let id = "City Center AHU-1";

describe("PointGraphics-icons.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to point graphics", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(pointGraphicsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("Assert CHVW-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > div > h4",
      "CHWV"
    );

    attributeContains(
      "#equipList > li:nth-child(1) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/image3.png",
      "Heater"
    );

    attributeContains(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > span:nth-child(2)"
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert EF Boolean Output", async () => {
    containsText(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > div > h4",
      "EF"
    );

    attributeContains(
      "#equipList > li:nth-child(2) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/fan.png",
      "Fan"
    );

    attributeContains(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_bool.png",
      "Boolean Output"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button",
      "action-boolean",
      "boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/([ONOFF])/g);
  });
  it("Assert EF Filter Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(3) > div > div.vertical.flex.non-auto > div > h4",
      "Filter"
    );

    attributeContains(
      "#equipList > li:nth-child(3) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/filter.png",
      "Heater"
    );

    attributeContains(
      "#equipList > li:nth-child(3) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(3) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(3) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(3) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert MAD-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(4) > div > div.vertical.flex.non-auto > div > h4",
      "MAD"
    );

    attributeContains(
      "#equipList > li:nth-child(4) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damper.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(4) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(4) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert MAT - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(5) > div > div.vertical.flex.non-auto > div > h4",
      "MAT"
    );

    attributeContains(
      "#equipList > li:nth-child(5) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/averaging.png",
      "MAT Image"
    );

    attributeContains(
      "#equipList > li:nth-child(5) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(5) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(5) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(5) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Assert OAD-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(6) > div > div.vertical.flex.non-auto > div > h4",
      "OAD"
    );

    attributeContains(
      "#equipList > li:nth-child(6) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damper.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(6) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(6) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert RAD-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(7) > div > div.vertical.flex.non-auto > div > h4",
      "RAD"
    );

    attributeContains(
      "#equipList > li:nth-child(7) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damper.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(7) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(7) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert RAT - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(8) > div > div.vertical.flex.non-auto > div > h4",
      "RAT"
    );

    attributeContains(
      "#equipList > li:nth-child(8) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/temp.png",
      "Temperature Sensor"
    );

    attributeContains(
      "#equipList > li:nth-child(8) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(8) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(8) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(8) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Assert SAF - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(9) > div > div.vertical.flex.non-auto > div > h4",
      "SAF"
    );

    attributeContains(
      "#equipList > li:nth-child(9) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/halo.png",
      "Circular "
    );

    attributeContains(
      "#equipList > li:nth-child(9) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(9) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(9) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(9) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\scfm/g);
  });
  it("Assert SAP - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(10) > div > div.vertical.flex.non-auto > div > h4",
      "SAP"
    );

    attributeContains(
      "#equipList > li:nth-child(10) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/dp.png",
      "DP"
    );

    attributeContains(
      "#equipList > li:nth-child(10) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(10) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(10) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(10) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sinHg/g);
  });
  it("Assert SAT - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(11) > div > div.vertical.flex.non-auto > div > h4",
      "SAT"
    );

    attributeContains(
      "#equipList > li:nth-child(11) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/temp.png",
      "Temperature Sensor"
    );

    attributeContains(
      "#equipList > li:nth-child(11) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(11) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(11) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(11) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);

    waitService.waitForMilliseconds(1000);
  });
  it("Assert SF Boolean Output", async () => {
    containsText(
      "#equipList > li:nth-child(12) > div > div.vertical.flex.non-auto > div > h4",
      "SF"
    );

    attributeContains(
      "#equipList > li:nth-child(12) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/fan.png",
      "Fan"
    );

    attributeContains(
      "#equipList > li:nth-child(12) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_bool.png",
      "Boolean Output"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > div > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(12) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/([ONOFF])/g);
  });
  it("Assert Total VAVs-Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(13) > div > div.vertical.flex.non-auto > div > h4",
      "Total Vavs"
    );

    attributeContains(
      "#equipList > li:nth-child(13) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/sigma.png",
      "Sigma Total"
    );

    attributeContains(
      "#equipList > li:nth-child(13) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(13) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(13) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(13) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/10.00/g);
  });
  it("Assert VAV Avg Damper", async () => {
    containsText(
      "#equipList > li:nth-child(14) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(14) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damperavg.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(14) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(14) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(14) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(14) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s/g);
  });
  it("Assert VAV Avg Delta Airflow", async () => {
    containsText(
      "#equipList > li:nth-child(15) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Delta Airflow"
    );

    attributeContains(
      "#equipList > li:nth-child(15) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/airflowdeltaavg.png",
      "Delta AVG "
    );

    attributeContains(
      "#equipList > li:nth-child(15) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(15) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(15) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(15) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\scfm/g);
  });
  it("Assert VAV Avg Delta Temp", async () => {
    containsText(
      "#equipList > li:nth-child(16) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Delta Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(16) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempdeltaavg.png",
      "Delta Temp AVG"
    );

    attributeContains(
      "#equipList > li:nth-child(16) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(16) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(16) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(16) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sΔ°F/g);
  });
  it("Assert VAV Avg Room Temp", async () => {
    containsText(
      "#equipList > li:nth-child(17) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Room Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(17) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      '"/pod/finStackMobileExt/assets/images/points/rmtavg.png"',
      "Room Temp AVG"
    );

    attributeContains(
      "#equipList > li:nth-child(17) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(17) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(17) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(17) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Assert VAV Max Damper", async () => {
    containsText(
      "#equipList > li:nth-child(18) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(18) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/dampermax.png",
      "Max Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(18) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(18) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(18) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(18) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert VAV Max Delta Airflow", async () => {
    containsText(
      "#equipList > li:nth-child(19) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Delta Airflow"
    );

    attributeContains(
      "#equipList > li:nth-child(19) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/airflowdeltamax.png",
      "Max Delta  Airflow "
    );

    attributeContains(
      "#equipList > li:nth-child(19) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(19) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(19) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(19) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\scfm/g);
  });
  it("Assert VAV Max Delta Temp", async () => {
    containsText(
      "#equipList > li:nth-child(20) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Delta Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(20) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempdeltamax.png",
      "Delta Temp Max"
    );

    attributeContains(
      "#equipList > li:nth-child(20) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(20) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(20) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(20) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sΔ°F/g);
  });
  it("Assert VAV Max Room Temp", async () => {
    containsText(
      "#equipList > li:nth-child(21) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Room Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(21) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/rmtmax.png",
      "Room Temp Max"
    );

    attributeContains(
      "#equipList > li:nth-child(21) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(21) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(21) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(21) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Assert VAV Min Damper", async () => {
    containsText(
      "#equipList > li:nth-child(22) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Min Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(22) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/dampermin.png",
      "Min Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(22) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(22) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(22) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(22) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert VAV Min Delta Temp", async () => {
    containsText(
      "#equipList > li:nth-child(23) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Min Delta Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(23) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempdeltamin.png",
      "Delta Temp Min"
    );

    attributeContains(
      "#equipList > li:nth-child(23) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(23) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(23) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(23) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sΔ°F/g);
  });
  it("Assert VAV Min Room Temp", async () => {
    containsText(
      "#equipList > li:nth-child(24) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Min Room Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(24) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/rmtmin.png",
      "Room Temp Min"
    );

    attributeContains(
      "#equipList > li:nth-child(24) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(24) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(24) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(24) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Assert ZoneTempSp ", async () => {
    containsText(
      "#equipList > li:nth-child(29) > div > div.vertical.flex.non-auto > div > h4",
      "ZoneTempSp"
    );

    attributeContains(
      "#equipList > li:nth-child(29) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempsp.png",
      "Room Temp Min"
    );

    attributeContains(
      "#equipList > li:nth-child(29) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/logical_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(29) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(29) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(29) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Switch to main view", async () => {
    waitService.waitForMilliseconds(3000);

    moveToElement(
      "#equipDrawer > div.app-equip.active-app > header > button:nth-child(3))",
      1,
      1
    );

    waitService.waitForMilliseconds(1999);

    click(
      "#equipDrawer > div.app-equip.active-app > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement(
      "#equipDrawer > div.app-equip.active-app > header > button:nth-child(3)",
      1,
      1
    );

    click(
      "#equipDrawer > div.app-equip.active-app > header > button:nth-child(3)"
    );

    elementPresent("#content");

    waitService.waitForMilliseconds(3000);
  });
  it("Main View Assert CHVW-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > div > h4",
      "CHWV"
    );

    attributeContains(
      "#equipList > li:nth-child(1) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/image3.png",
      "Heater"
    );

    attributeContains(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert EF Boolean Output", async () => {
    containsText(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > div > h4",
      "EF"
    );

    attributeContains(
      "#equipList > li:nth-child(2) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/fan.png",
      "Fan"
    );

    attributeContains(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_bool.png",
      "Boolean Output"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button",
      "action-boolean",
      "boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/([ONOFF])/g);
  });
  it("Main View Assert EF Filter Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(3) > div > div.vertical.flex.non-auto > div > h4",
      "Filter"
    );

    attributeContains(
      "#equipList > li:nth-child(3) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/filter.png",
      "Heater"
    );

    attributeContains(
      "#equipList > li:nth-child(3) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(3) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(3) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(3) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert MAD-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(4) > div > div.vertical.flex.non-auto > div > h4",
      "MAD"
    );

    attributeContains(
      "#equipList > li:nth-child(4) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damper.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(4) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(4) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(4) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert MAT - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(5) > div > div.vertical.flex.non-auto > div > h4",
      "MAT"
    );

    attributeContains(
      "#equipList > li:nth-child(5) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/averaging.png",
      "MAT Image"
    );

    attributeContains(
      "#equipList > li:nth-child(5) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(5) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(5) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(5) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Main View Assert OAD-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(6) > div > div.vertical.flex.non-auto > div > h4",
      "OAD"
    );

    attributeContains(
      "#equipList > li:nth-child(6) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damper.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(6) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(6) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(6) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert RAD-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(7) > div > div.vertical.flex.non-auto > div > h4",
      "RAD"
    );

    attributeContains(
      "#equipList > li:nth-child(7) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damper.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(7) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(7) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(7) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert RAT - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(8) > div > div.vertical.flex.non-auto > div > h4",
      "RAT"
    );

    attributeContains(
      "#equipList > li:nth-child(8) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/temp.png",
      "Temperature Sensor"
    );

    attributeContains(
      "#equipList > li:nth-child(8) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(8) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(8) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(8) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Main View Assert SAF - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(9) > div > div.vertical.flex.non-auto > div > h4",
      "SAF"
    );

    attributeContains(
      "#equipList > li:nth-child(9) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/halo.png",
      "Circular "
    );

    attributeContains(
      "#equipList > li:nth-child(9) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(9) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(9) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(9) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\scfm/g);
  });
  it("Main View Assert SAP - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(10) > div > div.vertical.flex.non-auto > div > h4",
      "SAP"
    );

    attributeContains(
      "#equipList > li:nth-child(10) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/dp.png",
      "DP"
    );

    attributeContains(
      "#equipList > li:nth-child(10) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(10) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(10) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(10) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sinHg/g);
  });
  it("Main View Assert SAT - Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(11) > div > div.vertical.flex.non-auto > div > h4",
      "SAT"
    );

    attributeContains(
      "#equipList > li:nth-child(11) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/temp.png",
      "Temperature Sensor"
    );

    attributeContains(
      "#equipList > li:nth-child(11) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(11) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(11) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(11) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);

    waitService.waitForMilliseconds(1000);
  });
  it("Main View Assert SF Boolean Output", async () => {
    containsText(
      "#equipList > li:nth-child(12) > div > div.vertical.flex.non-auto > div > h4",
      "SF"
    );

    attributeContains(
      "#equipList > li:nth-child(12) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/fan.png",
      "Fan"
    );

    attributeContains(
      "#equipList > li:nth-child(12) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_bool.png",
      "Boolean Output"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > div > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(12) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(12) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/([ONOFF])/g);
  });
  it("Main View Assert Total VAVs-Numeric Input", async () => {
    containsText(
      "#equipList > li:nth-child(13) > div > div.vertical.flex.non-auto > div > h4",
      "Total Vavs"
    );

    attributeContains(
      "#equipList > li:nth-child(13) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/sigma.png",
      "Sigma Total"
    );

    attributeContains(
      "#equipList > li:nth-child(13) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(13) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(13) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(13) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/10.00/g);
  });
  it("Main View Assert VAV Avg Damper", async () => {
    containsText(
      "#equipList > li:nth-child(14) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(14) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/damperavg.png",
      "Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(14) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(14) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(14) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(14) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s/g);
  });
  it("Main View Assert VAV Avg Delta Airflow", async () => {
    containsText(
      "#equipList > li:nth-child(15) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Delta Airflow"
    );

    attributeContains(
      "#equipList > li:nth-child(15) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/airflowdeltaavg.png",
      "Delta AVG "
    );

    attributeContains(
      "#equipList > li:nth-child(15) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(15) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(15) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(15) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\scfm/g);
  });
  it("Main View Assert VAV Avg Delta Temp", async () => {
    containsText(
      "#equipList > li:nth-child(16) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Delta Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(16) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempdeltaavg.png",
      "Delta Temp AVG"
    );

    attributeContains(
      "#equipList > li:nth-child(16) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(16) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(16) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(16) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sΔ°F/g);
  });
  it("Main View Assert VAV Avg Room Temp", async () => {
    containsText(
      "#equipList > li:nth-child(17) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Avg Room Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(17) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      '"/pod/finStackMobileExt/assets/images/points/rmtavg.png"',
      "Room Temp AVG"
    );

    attributeContains(
      "#equipList > li:nth-child(17) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(17) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(17) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(17) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Main View Assert VAV Max Damper", async () => {
    containsText(
      "#equipList > li:nth-child(18) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(18) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/dampermax.png",
      "Max Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(18) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(18) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(18) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(18) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert VAV Max Delta Airflow", async () => {
    containsText(
      "#equipList > li:nth-child(19) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Delta Airflow"
    );

    attributeContains(
      "#equipList > li:nth-child(19) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/airflowdeltamax.png",
      "Max Delta  Airflow "
    );

    attributeContains(
      "#equipList > li:nth-child(19) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(19) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(19) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(19) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\scfm/g);
  });
  it("Main View Assert VAV Max Delta Temp", async () => {
    containsText(
      "#equipList > li:nth-child(20) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Delta Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(20) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempdeltamax.png",
      "Delta Temp Max"
    );

    attributeContains(
      "#equipList > li:nth-child(20) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(20) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(20) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(20) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sΔ°F/g);
  });
  it("Main View Assert VAV Max Room Temp", async () => {
    containsText(
      "#equipList > li:nth-child(21) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Max Room Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(21) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/rmtmax.png",
      "Room Temp Max"
    );

    attributeContains(
      "#equipList > li:nth-child(21) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(21) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(21) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(21) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Main View Assert VAV Min Damper", async () => {
    containsText(
      "#equipList > li:nth-child(22) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Min Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(22) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/dampermin.png",
      "Min Damper"
    );

    attributeContains(
      "#equipList > li:nth-child(22) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(22) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(22) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(22) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Main View Assert VAV Min Delta Temp", async () => {
    containsText(
      "#equipList > li:nth-child(23) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Min Delta Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(23) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempdeltamin.png",
      "Delta Temp Min"
    );

    attributeContains(
      "#equipList > li:nth-child(23) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(23) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(23) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(23) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\sΔ°F/g);
  });
  it("Main View Assert VAV Min Room Temp", async () => {
    containsText(
      "#equipList > li:nth-child(24) > div > div.vertical.flex.non-auto > div > h4",
      "VAV Min Room Temp"
    );

    attributeContains(
      "#equipList > li:nth-child(24) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/rmtmin.png",
      "Room Temp Min"
    );

    attributeContains(
      "#equipList > li:nth-child(24) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/input_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(24) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(24) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(24) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
  it("Main View Assert ZoneTempSp ", async () => {
    containsText(
      "#equipList > li:nth-child(29) > div > div.vertical.flex.non-auto > div > h4",
      "ZoneTempSp"
    );

    attributeContains(
      "#equipList > li:nth-child(29) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/tempsp.png",
      "Room Temp Min"
    );

    attributeContains(
      "#equipList > li:nth-child(29) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/logical_num.png",
      "Numeric Input Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(29) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(29) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    element(
      "#equipList > li:nth-child(29) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/\d*[.]?\d*\s°F/g);
  });
});
