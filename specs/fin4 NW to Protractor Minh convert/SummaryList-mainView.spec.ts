import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let summaryButton = po.summaryButton;

describe("SummaryList-mainView.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to Summary", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(summaryButton);

    waitService.waitForMilliseconds(1500);
  });
  it("Main View", async () => {
    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    waitService.waitForMilliseconds(1999);

    click("#innerApp > header > button:nth-child(3)");

    waitService.waitForMilliseconds(5000);

    frame(0);

    moveToElement("#innerApp > header > button:nth-child(3)", 1, 1);

    click("#innerApp > header > button:nth-child(3)");

    elementPresent("#content");
  });
  it("Summary menu", async () => {
    elementPresent("#innerApp > header", "summary app is open");

    click("#innerApp > section > ul > li > div");

    waitService.waitForMilliseconds(1000);
  });
  it("assertions top", async () => {
    getText("#fooAppContainer > header > h4", function(result) {
      console.log(result.value);

      containsText("#fooAppContainer > header > h4", result.value);
    });

    waitService.waitForMilliseconds(500);
  });
  it("tile numeric", async () => {
    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(1) > div",
      "status-bubble",
      "Magic Buttons"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > div > span",
      "sm-command",
      "numeric input"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button ",
      "action-numeric"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button ",
      "action-numeric"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend"
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
          result.value
        );
      }
    );

    waitService.waitForMilliseconds(500);
  });
  it("Check override", async () => {
    click(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      "#point-actions > div > div > div > button:nth-child(3)",
      "MANUAL SET"
    );

    waitService.waitForMilliseconds(1000);

    click("#point-actions > div > div > div > button:nth-child(3)");

    click("#action-value");

    clearValue("#action-value");

    setValue("#action-value", "69");

    value("#action-value", "69", "value is 69");

    click("#point-actions > div > div > footer > button.rounded.focus");

    waitService.waitForMilliseconds(1000);

    refresh();

    waitService.waitForMilliseconds(3000);

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
          result.value
        );
      }
    );

    waitService.waitForMilliseconds(500);
  });
  it("assertion bottom", async () => {
    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.left > span.uppercase",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.left > span.uppercase",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.left > span.mono.margin.small.l-side",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.left > span.mono.margin.small.l-side",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.right > span.uppercase",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.right > span.uppercase",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.right > span.mono.margin.small.l-side",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(1) > div.flex.horizontal.middle.right > span.mono.margin.small.l-side",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.left > span.uppercase",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.left > span.uppercase",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.left > span.mono.margin.small.l-side",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.left > span.mono.margin.small.l-side",
          result.value
        );
      }
    );

    containsText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.left > span.italic",
      "%"
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.right > span.uppercase",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.right > span.uppercase",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.right > span.mono.margin.small.l-side",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div:nth-child(2) > div.flex.horizontal.middle.right > span.mono.margin.small.l-side",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div.horizontal.middle.stat-segment.center > span.uppercase",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div.horizontal.middle.stat-segment.center > span.uppercase",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > section:nth-child(3) > div.horizontal.middle.stat-segment.center > span.mono.margin.small.l-side",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > section:nth-child(3) > div.horizontal.middle.stat-segment.center > span.mono.margin.small.l-side",
          result.value
        );
      }
    );

    waitService.waitForMilliseconds(500);
  });
  it("dropdown menu", async () => {
    click("#fooAppContainer > section > div > div > select");

    waitService.waitForMilliseconds(1000);

    containsText(
      "#fooAppContainer > section > div > div > select > option:nth-child(1)",
      "Damper"
    );

    containsText(
      "#fooAppContainer > section > div > div > select > option:nth-child(2)",
      "Occ Mode"
    );

    containsText(
      "#fooAppContainer > section > div > div > select > option:nth-child(3)",
      "Room Setpoint"
    );

    containsText(
      "#fooAppContainer > section > div > div > select > option:nth-child(4)",
      "Room Temp"
    );

    click(
      "#fooAppContainer > section > div > div > select > option:nth-child(2)"
    );

    getText("#fooAppContainer > header > h4", function(result) {
      console.log(result.value);

      containsText("#fooAppContainer > header > h4", result.value);
    });

    waitService.waitForMilliseconds(500);
  });
  it("tile boolean", async () => {
    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(1) > div",
      "status-bubble",
      "Magic Buttons"
    );

    cssClassPresent(
      "#barAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > div > span",
      "item-boolean",
      "boolean input"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button ",
      "action-boolean"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button ",
      "action-boolean"
    );

    cssClassPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend"
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
          result.value
        );
      }
    );

    waitService.waitForMilliseconds(500);
  });
  it("Action Boolean Override", async () => {
    click(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      "#point-actions > div > div > div > button:nth-child(4)",
      "MANUAL ON"
    );

    waitService.waitForMilliseconds(1000);

    click("#point-actions > div > div > div > button:nth-child(4)");

    containsText("#point-actions > div > header", "Occ Mode Action");

    click("#point-actions > div > div > footer > button.rounded.focus");

    waitService.waitForMilliseconds(1000);

    refresh();

    waitService.waitForMilliseconds(3000);

    click("#barAppContainer > section > div > div > select");

    waitService.waitForMilliseconds(1000);

    click(
      "#barAppContainer > section > div > div > select > option:nth-child(2)"
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(2) > span",
          result.value
        );
      }
    );

    getText(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
      function(result) {
        console.log(result.value);

        containsText(
          "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(3) > span",
          result.value
        );
      }
    );

    waitService.waitForMilliseconds(500);
  });
  it("verify trend", async () => {
    click(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#fooAppContainer > section > ul > li:nth-child(1) > div.horizontal.middle.center.padded.normal.all-sides > div > div > div"
    );

    moveToElement(
      "#fooAppContainer > section > ul > li:nth-child(1) > div.horizontal.middle.center.padded.normal.all-sides > div > div > div",
      25,
      25
    );

    elementPresent(".ractive-tooltip");
  });
  it("verify.schedules", async () => {
    click(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button:nth-child(2)"
    );

    getText(
      "#schedule0 > div > div:nth-child(1) > span.italic.flex.non-auto.margin.small.v-sides",
      function(result) {
        console.log(result.value);

        containsText(
          "#schedule0 > div > div:nth-child(1) > span.italic.flex.non-auto.margin.small.v-sides",
          result.value
        );
      }
    );

    getText("#schedule0Name", function(result) {
      console.log(result.value);

      containsText("#schedule0Name", result.value);
    });

    getText(
      "#schedule0 > div > div:nth-child(2) > span.italic.flex.non-auto.margin.small.v-sides",
      function(result) {
        console.log(result.value);

        containsText(
          "#schedule0 > div > div:nth-child(2) > span.italic.flex.non-auto.margin.small.v-sides",
          result.value
        );
      }
    );

    getText("#schedule0Value", function(result) {
      console.log(result.value);

      containsText("#schedule0Value", result.value);
    });

    getText(
      "#schedule0 > div > div:nth-child(3) > span.italic.flex.non-auto.margin.small.v-sides",
      function(result) {
        console.log(result.value);

        containsText(
          "#schedule0 > div > div:nth-child(3) > span.italic.flex.non-auto.margin.small.v-sides",
          result.value
        );
      }
    );

    getText("#schedule0Update", function(result) {
      console.log(result.value);

      containsText("#schedule0Update", result.value);
    });

    getText(
      "#schedule0 > div > div:nth-child(4) > span.italic.flex.non-auto.margin.small.v-sides",
      function(result) {
        console.log(result.value);

        containsText(
          "#schedule0 > div > div:nth-child(4) > span.italic.flex.non-auto.margin.small.v-sides",
          result.value
        );
      }
    );

    getText("#schedule0UpdateVal", function(result) {
      console.log(result.value);

      containsText("#schedule0UpdateVal", result.value);
    });

    waitService.waitForMilliseconds(100);
  });
  it("magic buttons", async () => {
    click(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(1) > div"
    );

    waitService.waitForMilliseconds(2000);

    cssClassPresent(
      "body > app > div.modal > div > div.root.action.button.vertical.middle.center > i",
      "icon-direction"
    );

    containsText(
      "body > app > div.modal > div > div.root.action.button.vertical.middle.center > label",
      "City Center Vav-01"
    );

    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(6) > i",
      "icon-document"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(6) > label",
      "Cafe Lounge"
    );

    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(5) > i",
      "sm-point"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(5) > label",
      "Point Graphics"
    );

    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(4) > i",
      "icomoon-schedule"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(4) > label",
      "Normal Work Week"
    );

    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(3) > i",
      "icon-conversation"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(3) > label",
      "Notes"
    );

    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2) > i",
      "icon-scale"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(2) > label",
      "Overrides"
    );

    cssClassPresent(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1) > i",
      "sm-equip"
    );

    containsText(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1) > label",
      "City Center AHU-1 Graphic"
    );

    waitService.waitForMilliseconds(500);
  });
});
