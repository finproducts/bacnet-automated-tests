import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let selectorLibrary= require('./po.js');

let logicBuilderButton= po.logicBuilderButton;

let mainRoutine= (".variables .scroller > div:nth-child(2) > i");

let alarmRoutine= ('#panel > section.variables > div.scroller > div:nth-child(2) > span');



describe('LogicBuilder-bugDeleteSetterTab.js.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',9000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(1500);
}

    
  );  
  it('Add variables, routines and macro blocks', async () => 
    
    {
click('.add.panel-button .variable');

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(1)');

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.tab > span:nth-child(4)','A new routine is added');

click('.add.panel-button .macro');

waitService.waitForMilliseconds(500);

elementPresent('.variables .scroller .macro','A macroblock is added');
}

    
  );  
  it('Drag the variables and macroblocks to the stage', async () => 
    
    {
execute(function(simple)
{
app.set('blocks.0.x',40);

app.set('blocks.0.y',26);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .boolean.variable  b.boolean.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.boolean.block','The boolean setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',200);

app.set('blocks.1.y',70);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .boolean.variable  b.boolean.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.boolean.block','The boolean getter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',35);

app.set('blocks.2.y',136);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .number.variable  b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block','The number setter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',384);

app.set('blocks.3.y',90);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .number.variable  b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block','The number getter block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',222);

app.set('blocks.4.y',187);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .string.variable  b.string.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.string.block','The string setter block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',705);

app.set('blocks.5.y',174);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .string.variable  b.string.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.string.block','The string getter block is on stage');

execute(function(simple)
{
app.set('blocks.6.x',571);

app.set('blocks.6.y',285);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .date.variable  b.date.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.date.block','The date setter block is on stage');

execute(function(simple)
{
app.set('blocks.7.x',901);

app.set('blocks.7.y',228);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .date.variable  b.date.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.date.block','The date getter block is on stage');

execute(function(simple)
{
app.set('blocks.8.x',754);

app.set('blocks.8.y',310);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .time.variable  b.time.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.time.block','The time setter block is on stage');

execute(function(simple)
{
app.set('blocks.9.x',1100);

app.set('blocks.9.y',304);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .time.variable  b.time.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.time.block','The time getter block is on stage');

execute(function(simple)
{
app.set('blocks.10.x',958);

app.set('blocks.10.y',369);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .datetime.variable  b.datetime.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.datetime.block','The datetime setter block is on stage');

execute(function(simple)
{
app.set('blocks.11.x',1306);

app.set('blocks.11.y',328);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .datetime.variable  b.datetime.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.datetime.block','The datetime getter block is on stage');

execute(function(simple)
{
app.set('blocks.12.x',1190);

app.set('blocks.12.y',418);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .macro b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.macro.commandWithDataOutputs.block','The macro block is on stage');

execute(function(simple)
{
app.set('blocks.13.x',1522);

app.set('blocks.13.y',385);
}
,[],function(result)
{}
);

setValue('.blocklibrary .filters > div > input[type="text"]','if');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary .scroller > div > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .if.block','The if block is on stage');

execute(function(simple)
{
app.set('blocks.14.x',540);

app.set('blocks.14.y',104);
}
,[],function(result)
{}
);

clearValue('.blocklibrary .filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

click('.blocks');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Link the blocks in the stage', async () => 
    
    {
click('.__start__ > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.date.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.date.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.time.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.time.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.datetime.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.datetime.block > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.boolean.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.boolean.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.string.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.string.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.date.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.date.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.time.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.time.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.datetime.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.datetime.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Delete blocks', async () => 
    
    {
click('.__start__');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementPresent('.__start__','routine start block is still present after trying to delete it');

click('.blocks .if.block');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementNotPresent('.blocks .if.block','If block is deleted');

elementNotPresent('#stage > div.scroller > svg > path:nth-child(13)','Link to IF block is deleted');

click('.blocks .get.string.block');

waitService.waitForMilliseconds(500);

keys(driver,Keys,DELETE);

waitService.waitForMilliseconds(500);

elementNotPresent('.blocks .get.string.block','String getter block is deleted');

elementNotPresent('#stage > div.scroller > svg > path:nth-child(12)','Link to String getter block is deleted');

elementPresent('#stage > div.scroller > div > div.set.string.block > div.data.sockets > div.input > div > div > input[type="text"]','String setter text input is present after deleting linked getter');

click('#stage > div.scroller > div > div.set.time.block');

waitService.waitForMilliseconds(1000);
}

    
  );  
  
  
});