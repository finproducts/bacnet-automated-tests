import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let programName= "test"  +  new Date().valueOf();

let selectorLibrary= require('./po.js');

let logicBuilderButton= po.logicBuilderButton;



describe('LogicBuilder-testMenuSection2.js',() =>{
  
  it('Login', async () => 
    
    {}

    
  );  
  it('go to City Center VAV-1', async () => 
    
    {
click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)');

waitService.waitForMilliseconds(1000);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)');

waitService.waitForMilliseconds(500);

click('body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)');

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('go to Logic Builder', async () => 
    
    {
waitService.waitForMilliseconds(1500);

click('body > app > section > header > button.link-button.pna.launcher-open-btn');

waitService.waitForMilliseconds(500);

click(logicBuilderButton);

waitService.waitForMilliseconds(500);

click(logicBuilderButton  +  "> ul > li:nth-child(2)");

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Create bLine Program', async () => 
    
    {
click('#content > div.form-item-holder.textinput > input');

waitService.waitForMilliseconds(500);

keys("test"  +  new Date().valueOf());

waitService.waitForMilliseconds(500);

click('#content > div.form-item-holder.list > select > option:nth-child(2)');

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)');

waitService.waitForMilliseconds(500);

click('body > app > div.modal.form-stack.vertical.middle.center > div > div > button');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a');

waitService.waitForMilliseconds(1000);

frame(0);

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('All menu buttons are present', async () => 
    
    {
elementPresent('#panel > section.controls > div.icons > i.save.icon','Save icon');

elementPresent('#panel > section.controls > div.icons > div > i','Alignment Icon');

elementPresent('#panel > section.controls > div.icons > i.undo.icon.disabled','undo button');

elementPresent('#panel > section.controls > div.icons > i.redo.icon.disabled','redo button');

elementPresent('#panel > section.controls > div.icons > div > i',' alignment icon');

elementPresent('#panel > section.controls > div.icons > i.enum.icon','edit enum icon ');

elementPresent('#panel > section.controls > div.icons > i.collapse.icon.fr','collapse button ');

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Verify Tool Tip Alignment', async () => 
    
    {
moveToElement('#panel > section.controls > div.icons > div > i',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Align blocks',''Align blocks' tool tip is present');
}

    
  );  
  it('Verify Tool Tip Enum Editor', async () => 
    
    {
moveToElement('#panel > section.controls > div.icons > i.enum.icon',1,1);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Open Enum editor',''Open Enum editor' tool tip is present');
}

    
  );  
  it('Use Colapse and Verify Tool Tip', async () => 
    
    {
moveToElement('i.collapse.icon.fr',2,2);

waitService.waitForMilliseconds(500);

containsText('.ractive-tooltip','Collapse side panel');
}

    
  );  
  it('Verify Tool Tip Expand', async () => 
    
    {
click('#panel > section.controls > div.icons > i.collapse.icon.fr');

waitService.waitForMilliseconds(1000);

moveToElement('#stage > div > div > div',0,0);

waitService.waitForMilliseconds(1000);

moveToElement('i.collapse.icon.fr',2,5);

waitService.waitForMilliseconds(1500);

elementPresent('i.collapse.icon.fr');

click('i.collapse.icon.fr');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Use Undo from menu button', async () => 
    
    {
moveToElement('#stage > div.scroller > div > div',100,100);

waitService.waitForMilliseconds(500);

mouseButtonDown(0);

waitService.waitForMilliseconds(500);

moveTo(null,50,50);

waitService.waitForMilliseconds(500);

mouseButtonUp(0);

waitService.waitForMilliseconds(500);

moveToElement('i.undo.icon',5,5);

waitService.waitForMilliseconds(200);

waitService.waitForMilliseconds(500);

click('i.undo.icon');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add Bool', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(3000);

click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)');

waitService.waitForMilliseconds(3000);

click('#content > div.form-item-holder.textinput > input');

keys('boolVar');

waitService.waitForMilliseconds(1000);

click('#content > div:nth-child(2) > div > input');

waitService.waitForMilliseconds(1000);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(1000);

click('#content > div.form-item-holder.combobox > select > option:nth-child(2)');

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);

click('#content > div.form-item-holder.combobox > select > option:nth-child(1)');

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1000);

moveToElement('#controlBar > button',1,1);

click('#controlBar > button');

waitService.waitForMilliseconds(6000);
}

    
  );  
  it('Add Str', async () => 
    
    {
frame(null);

waitService.waitForMilliseconds(3000);

click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full');

waitService.waitForMilliseconds(1000);

click('body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)');

waitService.waitForMilliseconds(1000);

click('#content > div.form-item-holder.textinput > input');

keys('strVar');

waitService.waitForMilliseconds(1000);

click('#content > div:nth-child(2) > div > input');

waitService.waitForMilliseconds(1000);

click('#content > div.form-item-holder.combobox > select');

waitService.waitForMilliseconds(1000);

click('#content > div.form-item-holder.combobox > select > option:nth-child(3)');

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

setValue('#content > div.form-item-holder.textinput > input','stringValue');

waitService.waitForMilliseconds(1000);

click('#controlBar > button:nth-child(2)');

waitService.waitForMilliseconds(1500);

moveToElement('#controlBar > button',1,1);

click('#controlBar > button');

waitService.waitForMilliseconds(4000);
}

    
  );  
  it('Use Search', async () => 
    
    {
frame(0);

moveToElement('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',1,1);

click('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]');

setValue('#panel > section.blocklibrary > div > div.filters > div > input[type="text"]','boolVar');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Use Redo from menu button', async () => 
    
    {
frame(0);

moveToElement('i.redo.icon',1,1);

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

click('i.redo.icon');

waitService.waitForMilliseconds(700);
}

    
  );  
  it('Use search', async () => 
    
    {
click('#panel > section.variables > div.filters > div > input[type="text"]');

setValue('#panel > section.variables > div.filters > div > input[type="text"]','boolVar');
}

    
  );  
  it('Collapse menu', async () => 
    
    {
waitService.waitForMilliseconds(1000);

click('#panel > section.controls > div.icons > i.collapse.icon.fr');

waitService.waitForMilliseconds(5000);

click('#panel > section.controls > div.icons > i.collapse.icon.fr');

waitService.waitForMilliseconds(5000);
}

    
  );  
  
  
});