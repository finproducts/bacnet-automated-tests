import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let historianButton = po.historianButton;

let trendName = "trend" + new Date().valueOf();

let trendSAName = "trend" + new Date().valueOf();

let saveX = "trend" + new Date().valueOf();

describe("HistorianSeriesOptions.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Historian", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(historianButton);

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)",
      "historian is opened"
    );
  });
  it("new trend", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    click("#equipList > li:nth-child(2) > div > div.switch");

    click("#equipList > li:nth-child(4) > div > div.switch");

    waitService.waitForMilliseconds(1000);

    click("#relativeSelector");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("SeriesOptions OPEN", async () => {
    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(1)",
      "auto"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(2)",
      "1min"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(3)",
      "5min"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(4)",
      "10min"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(5)",
      "15min"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(6)",
      "30min"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(7)",
      "1hr"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(8)",
      "1day"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(9)",
      "1week"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(10)",
      "1mo"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(11)",
      "3mo"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > div > div > div > select > option:nth-child(12)",
      "1yr"
    );

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-ColourByInput1", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(1) > div > input"
    );

    clearValue(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(1) > div > input"
    );

    setValue(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(1) > div > input",
      "#000000"
    );

    waitService.waitForMilliseconds(1000);

    cssProperty(
      "#colorWell0-0",
      "background-color",
      "rgba(0, 0, 0, 1)",
      "the trend is black now"
    );

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-ColourByInput2", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(2) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click("#colorWell0-1");

    waitService.waitForMilliseconds(1000);

    click(
      "#fooAppContainer > div.color-selector.vertical > section > div.vertical.flex.non-auto.can-scroll > ul > li:nth-child(115)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "#fooAppContainer > div.color-selector.vertical > footer > button.minimal.focus.margin.normal.l-side"
    );

    waitService.waitForMilliseconds(1990);

    cssProperty(
      "#colorWell0-1",
      "background-color",
      "rgba(204, 0, 0, 1)",
      "the second trend is red"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("SeriesOptions-Interval-CreateNew", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(2) > div.horizontal.middle.padded.normal.all-sides > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.series-drop-zone.vertical > div > ul > li:nth-child(1) > div.bucket.vertical.middle.center"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.series-drop-zone.vertical > div.horizontal.middle.center.choice-box.padded.large.v-sides > button.minimal.focus.margin.normal.l-side"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(3)",
      "AHU-1 filter is a separate trend now"
    );

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-stick A with B ", async () => {
    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li > div > button:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "#fooAppContainer > div.series-drop-zone.vertical > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.series-drop-zone.vertical > div.horizontal.middle.center.choice-box.padded.large.v-sides > button.minimal.focus.margin.normal.l-side"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    elementPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li > div > div.horizontal.middle.flex.non-auto.wrap > div > span:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementNotPresent(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)",
      "Only 2 trends now"
    );

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Column", async () => {
    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(400);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div > svg > g:nth-child(8) > g",
      "Chart is column"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Line", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div > svg > g:nth-child(3) > g.amcharts-category-axis",
      "Chart is line"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Curve", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div > svg > g:nth-child(3) > g.amcharts-category-axis",
      "chart is curve"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Area", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div",
      "chart is area"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Scatter", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(5)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div.amcharts-chart-div > svg > g:nth-child(13) > g",
      "chart is scatter"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Stepped", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(6)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div > svg > desc",
      "chart is Step"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
  it("SeriesOptions-Interval-GraphType-Dashed", async () => {
    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > section > ul > li:nth-child(1) > ul > li:nth-child(1) > div.vertical > div:nth-child(2) > div > div > select > option:nth-child(7)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "#fooAppContainer > div.modal-overlay.series-options.app-trends.vertical > header > button:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div.amChartsPanel.amcharts-stock-panel-div.amcharts-stock-panel-div-stockPanel0 > div > div > svg > g:nth-child(3) > g.amcharts-category-axis > g:nth-child(2) > rect",
      "chart is Dash"
    );

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(500);

    click("#fooAppContainer > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(500);
  });
});
