import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

describe("LogicBuilder-kmctest_Assertion.js", () => {
  it("Go to Block Programming KMC", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Sidepanel-Assertions", async () => {
    containsText(
      "#panel > section.controls > div.program-name > span.language",
      "KMC",
      "KMC language is displayed"
    );

    containsText(
      "#panel > section.controls > div.program-name > span:nth-child(1)",
      "Unnamed Program",
      "Title is Unnamed Program"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.file.icon",
      "New Program button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > label > i",
      "Open file button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.save.icon",
      "Save button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.import.icon",
      "Import program from code button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.export.icon",
      "Export current program button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > div > i",
      "Alignment button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.undo.icon.disabled",
      "undo button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.redo.icon.disabled",
      "redo button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.feed.icon",
      "live values button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.enum.icon",
      "edit enum button is present"
    );

    elementPresent(
      "#panel > section.controls > div.icons > i.collapse.icon.fr",
      "collapse button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(2) > i.variable.symbol",
      "Add Variable button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(3) > i.routine.symbol",
      "Add new routine button is present"
    );

    elementPresent(
      "#panel > section.variables > header > div:nth-child(4) > i.macro.symbol.mrxs",
      "Add new macro button is present"
    );

    elementPresent(
      "#panel > section.variables > center > span > u",
      "Show Objects"
    );

    elementPresent(
      "#panel > section.variables > center > span > span:nth-child(2)",
      "Show Local Variables"
    );

    elementPresent(
      "#panel > section.variables > center > span > span:nth-child(3)",
      "Show routines"
    );

    elementPresent(
      "#panel > section.variables > center > span > span:nth-child(4)",
      "Show macros"
    );

    elementPresent(
      "#panel > section.variables > div.filters > div",
      "search input"
    );

    containsText("#panel > section.blocklibrary > header", "BLOCK LIBRARY");

    elementPresent(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "search input"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(2) > div",
      "control flow"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(3) > div",
      "language"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(4) > div",
      "math"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(5) > div",
      "command"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(6) > div",
      "logical"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(7) > div",
      "status"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(8) > div",
      "time"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(9) > div",
      "trigonometry"
    );

    elementPresent(
      "#panel > section.blocklibrary > div > div:nth-child(10) > div",
      "value"
    );

    elementPresent(
      "#stage > div.scroller > div > div",
      "Start block is present"
    );
  });
  it("Functionality", async () => {});
});
