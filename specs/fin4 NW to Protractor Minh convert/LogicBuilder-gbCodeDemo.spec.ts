import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "JSGB" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let graphicsBuilderButton = po.graphicsBuilderButton;

describe("LogicBuilder-gbCodeDemo.js", () => {
  it("go to City Center AHU-1", async () => {
    

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Graphics Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton);

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton + "> ul > li:nth-child(3)");

    waitService.waitForMilliseconds(3000);
  });
  it("New Graphic Form", async () => {
    click("#content > div.form-item-holder.textinput > input");

    keys(programName);

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(300);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    waitForElementPresent(
      "body > app > div.modal.form-stack.vertical.middle.center > div",
      2000,
      "form is launched"
    );

    moveToElement("#controlBar > button", 1, 1);

    waitService.waitForMilliseconds(5000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Open The Graphic", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Drag a Gauge", async () => {
    frame(0);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#container > ul:nth-child(2) > li:nth-child(4) > div",
      "#main-content > section > div"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#projects-container > div > div > div:nth-child(3)",
      "Button is dragged on the stage"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Drag a button", async () => {
    frame(0);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#container > ul:nth-child(6) > li:nth-child(2) > iron-icon",
      "#main-content > section > div"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#projects-container > div > div > button",
      "Button is dragged on the stage"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Right-click on button", async () => {
    moveToElement("#projects-container > div > div > button", 2, 2);

    mouseButtonClick(2);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1)",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    click(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scriptArea > div.ace_scroller > div",
      "Script editor form launched succesfully"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Script Editor Form", async () => {
    frame(1);

    waitService.waitForMilliseconds(5000);

    click("#editorModeSelector > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("Add and edit fadeOut block", async () => {
    frame("blockFrame");

    click(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    clearValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "fadeOut"
    );

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div.category > div.contents > div",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.1.x", 450);

      app.set("blocks.1.y", 250);
    }, []);

    waitService.waitForMilliseconds(1000);

    click(
      '#stage > div > div > div.fadeOut.block > div.data.sockets > div.input > div:nth-child(2) > div > input[type="text"]'
    );

    setValue(
      '#stage > div > div > div.fadeOut.block > div.data.sockets > div.input > div:nth-child(2) > div > input[type="text"]',
      "2000"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Add and edit queryAll block", async () => {
    click(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    clearValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]'
    );

    setValue(
      '#panel > section.blocklibrary > div > div.filters > div > input[type="text"]',
      "queryAll"
    );

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#panel > section.blocklibrary > div > div.category > div.contents > div:nth-child(1)",
      ".blocks"
    ]);

    waitService.waitForMilliseconds(500);

    execute(function(simple) {
      app.set("blocks.2.x", 50);

      app.set("blocks.2.y", 300);
    }, []);

    waitService.waitForMilliseconds(1000);

    click(
      "#stage > div > div > div.queryAll.block > div > div.input > div > div > input"
    );

    setValue(
      "#stage > div > div > div.queryAll.block > div > div.input > div > div > input",
      "component"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Link start to fadeOut", async () => {
    click("#stage > div > div > div.__start__.block > div > div > div > b");

    waitService.waitForMilliseconds(1000);

    click(
      "#stage > div > div > div.fadeOut.block > div.exec.sockets > div.in > div > b"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Link fadeOut to queryAll", async () => {
    click(
      "#stage > div > div > div.fadeOut.block > div.data.sockets > div.input > div:nth-child(1) > b"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "#stage > div > div > div.queryAll.block > div > div.output > div > b"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Switch between blocks and code (due to bug)", async () => {
    frame("blockFrame");
  });
  it("something", async () => {
    waitService.waitForMilliseconds(1000);

    frame(null);

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);

    click("#editorModeSelector > ul > li:nth-child(2)");

    click("#content > footer > ui-button-bar > ul > li:nth-child(2)");

    waitService.waitForMilliseconds(1000);
  });
  it("save", async () => {
    click("#content > footer > ui-button:nth-child(5) > div");

    waitService.waitForMilliseconds(1000);
  });
  it("check the program-Preview", async () => {
    click("#previewButton");

    waitService.waitForMilliseconds(4000);

    click("#worldContainer > div > div > button");

    waitService.waitForMilliseconds(2000);
  });
  it("opacity", async () => {
    cssProperty(
      "#worldContainer > div > div > button",
      "opacity",
      "0",
      "is faded"
    );

    waitService.waitForMilliseconds(1000);
  });
});
