import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let fs= require('fs');

let macroCode= fs.readFileSync('./tests/fixtures/programs/macroBlockEnum.txt').toString();



describe('LogicBuilder-testMacroBlockEnum.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

setValue('body > div.auth-window > div > div.auth-input-container > div.auth-username-container > input','su');

waitService.waitForMilliseconds(250);

setValue('body > div.auth-window > div > div.auth-input-container > div.auth-password-container > input','su');

waitService.waitForMilliseconds(1000);

click('body > div.auth-window > div > div.auth-footer > button');

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(1500);
}

    
  );  
  it('Add new Enum and edit it', async () => 
    
    {
click('i.enum.icon');

waitService.waitForMilliseconds(500);

elementPresent('.enum.modal','Enum window is open');

click('.enum.modal > div > div.window > div > div.list > div.controls > i.plus.icon');

waitService.waitForMilliseconds(500);

click('.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(9)');

waitService.waitForMilliseconds(500);

click('.enum.modal > div > div.window > div > div.values > div > i');

waitService.waitForMilliseconds(500);

clearValue('.enum.modal > div > div.window > div > div.values > div > input');

waitService.waitForMilliseconds(500);

setValue('.enum.modal > div > div.window > div > div.values > div > input','minMax');

waitService.waitForMilliseconds(500);

keys(driver,Keys,ENTER);

clearValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.key > input');

waitService.waitForMilliseconds(500);

setValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.key > input','min');

waitService.waitForMilliseconds(500);

clearValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.value > input');

waitService.waitForMilliseconds(500);

setValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.value > input','min');

waitService.waitForMilliseconds(500);

clearValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.key > input');

waitService.waitForMilliseconds(500);

setValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.key > input','max');

waitService.waitForMilliseconds(500);

clearValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input');

waitService.waitForMilliseconds(500);

setValue('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input','max');

waitService.waitForMilliseconds(500);

click('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input');

waitService.waitForMilliseconds(500);

keys(driver,Keys,ENTER);

elementPresent('.enum.modal > div > div.window > div > div.values > table > tr:nth-child(3)','A third row for enum is present');

click('.enum.modal > div > div.close > i');

waitService.waitForMilliseconds(500);

elementNotPresent('.enum.modal','Enum window is open');
}

    
  );  
  it('Add variables in bLine', async () => 
    
    {
execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

elementPresent('.variables > div.scroller > div:nth-child(3)');

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

elementPresent('.variables > div.scroller > div:nth-child(4)');

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

elementPresent('.variables > div.scroller > div:nth-child(5)');
}

    
  );  
  it('Add a macroblock and drag it to stage in bLine', async () => 
    
    {
click('.add.panel-button .macro');

waitService.waitForMilliseconds(500);

moveToElement('.variables .scroller .macro > span > span',1,1);

waitService.waitForMilliseconds(500);

click('.variables .scroller .macro > i.pencil.icon');

waitService.waitForMilliseconds(500);

keys([driver.Keys.CONTROL,'a', driver.Keys.CONTROL, driver.Keys.DELETE]);

waitService.waitForMilliseconds(500);

keys('min-max');

waitService.waitForMilliseconds(500);

keys(driver,Keys,ENTER);

containsText('.variables .scroller .macro > span > span','min-max');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller .macro  b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .macro.commandWithDataOutputs.block:nth-child(2)','The macro block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',380);

app.set('blocks.1.y',241);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller .macro  b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .macro.commandWithDataOutputs.block:nth-child(3)','The second macro block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',725);

app.set('blocks.2.y',353);
}
,[],function(result)
{}
);
}

    
  );  
  it('Open macroblock by clicking the icon on the block', async () => 
    
    {
click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > header > i');

waitService.waitForMilliseconds(500);

cssClassPresent('.variables .scroller .macro','viewing','Macroblock is open for editing');

elementPresent('.bLine.banner.macro','Macroblock banner is present inside');

click('i.cog.icon');

waitService.waitForMilliseconds(500);

cssClassPresent('.bLine.banner.macro > div','open','macroblock settings window is open after clicking the settings icon');
}

    
  );  
  it('Edit ports for macroblock', async () => 
    
    {
click('#stage > div.bLine.banner.macro > div > div > div.tac.mtm > button');

waitService.waitForMilliseconds(500);

elementPresent('.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4)','A fourth port is added');

click('.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(1)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label','input','Fourth port is now set to 'input'');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label','number','Data type changed to number for first port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label','number','Data type changed to number for second port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','number','Data type changed to number for third port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(3)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > label','string','Data type changed to number for fourth port');

clearValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]','result');

waitService.waitForMilliseconds(500);

value('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]','result','The name for the third port is 'result'');

clearValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]','function');

waitService.waitForMilliseconds(500);

value('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]','function','The name for the fourth port is 'function'');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(10)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > label','minMax','minMax enum was selected');
}

    
  );  
  it('Drag blocks from the library to the stage', async () => 
    
    {
execute(function(simple)
{
app.set('blocks.0.x',47);

app.set('blocks.0.y',37);
}
,[],function(result)
{}
);

setValue('.blocklibrary > div > div.filters > div > input[type="text"]','if');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .if.block','The IF block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',209);

app.set('blocks.1.y',49);
}
,[],function(result)
{}
);

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('.blocklibrary > div > div.filters > div > input[type="text"]','cmpgt');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div:nth-child(1)', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .cmpGt.block','The cmpGt block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',196);

app.set('blocks.2.y',386);
}
,[],function(result)
{}
);

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('.blocklibrary > div > div.filters > div > input[type="text"]','cmpeq');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .cmpEq.block','The cmpEq block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',195);

app.set('blocks.3.y',468);
}
,[],function(result)
{}
);

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('.blocklibrary > div > div.filters > div > input[type="text"]','xor');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .xor.block','The xor block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',456);

app.set('blocks.4.y',413);
}
,[],function(result)
{}
);

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Drag ports to the stage', async () => 
    
    {
execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(1) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block:nth-child(6)','Port 1 block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',80);

app.set('blocks.5.y',221);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(2) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block:nth-child(7)','Port 2 block is on stage');

execute(function(simple)
{
app.set('blocks.6.x',80);

app.set('blocks.6.y',267);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(8)','First port 3 block is on stage');

execute(function(simple)
{
app.set('blocks.7.x',374);

app.set('blocks.7.y',23);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(9)','Second port 3 block is on stage');

execute(function(simple)
{
app.set('blocks.8.x',463);

app.set('blocks.8.y',143);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.string.block','Port 4 block is on stage');

execute(function(simple)
{
app.set('blocks.9.x',36);

app.set('blocks.9.y',512);
}
,[],function(result)
{}
);

click('i.cog.icon');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Link blocks betweem them', async () => 
    
    {
click('.blocks .__start__ > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(8) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(9) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(500);

click('.blocks .xor.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpGt.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .xor.block > div > div.input > div:nth-child(1) > b');

click('.blocks .cmpEq.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .xor.block > div > div.input > div:nth-child(2) > b');

click('.blocks .get.string.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpEq.block > div > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(100);

clearValue('.blocks .cmpEq.block > div > div.input > div:nth-child(2) > div > input');

waitService.waitForMilliseconds(500);

setValue('.blocks .cmpEq.block > div > div.input > div:nth-child(2) > div > input','min');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(6) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpGt.block > div > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(7) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpGt.block > div > div.input > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(6) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(8) > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(7) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(9) > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > svg > path:nth-child(1)');

elementPresent('#stage > div.scroller > svg > path:nth-child(2)');

elementPresent('#stage > div.scroller > svg > path:nth-child(3)');

elementPresent('#stage > div.scroller > svg > path:nth-child(4)');

elementPresent('#stage > div.scroller > svg > path:nth-child(5)');

elementPresent('#stage > div.scroller > svg > path:nth-child(6)');

elementPresent('#stage > div.scroller > svg > path:nth-child(7)');

elementPresent('#stage > div.scroller > svg > path:nth-child(8)');

elementPresent('#stage > div.scroller > svg > path:nth-child(9)');

elementPresent('#stage > div.scroller > svg > path:nth-child(10)');

elementPresent('#stage > div.scroller > svg > path:nth-child(11)');

cssClassPresent('.blocks .__start__ > div > div > div ','linked');

cssClassPresent('.blocks .if.block > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(1) ','linked');

cssClassPresent('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(2) ','linked');

cssClassPresent('.blocks .if.block > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(8) > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(9) > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(9) > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(8) > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .get.number.block:nth-child(6) > div > div.output > div ','linked');

cssClassPresent('.blocks .get.number.block:nth-child(7) > div > div.output > div ','linked');

cssClassPresent('.blocks .get.string.block > div > div.output > div ','linked');

cssClassPresent('.blocks .cmpGt.block > div > div.output > div ','linked');

cssClassPresent('.blocks .cmpGt.block > div > div.input > div:nth-child(1) ','linked');

cssClassPresent('.blocks .cmpGt.block > div > div.input > div:nth-child(2) ','linked');

cssClassPresent('.blocks .cmpEq.block > div > div.output > div ','linked');

cssClassPresent('.blocks .cmpEq.block > div > div.input > div:nth-child(1) ','linked');

cssClassPresent('.blocks .xor.block > div > div.output > div ','linked');

cssClassPresent('.blocks .xor.block > div > div.input > div:nth-child(1) ','linked');

cssClassPresent('.blocks .xor.block > div > div.input > div:nth-child(2) ','linked');
}

    
  );  
  it('Use macro block', async () => 
    
    {
click('.variables .scroller .function:nth-child(1) > i');

waitService.waitForMilliseconds(500);

click('.variables .scroller .function:nth-child(2) > i');

waitService.waitForMilliseconds(500);

click('.variables .scroller .function:nth-child(1) > i');

click('.__start__ > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller > div:nth-child(4) > b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block','Foo getter block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',143);

app.set('blocks.3.y',341);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.variables .scroller > div:nth-child(5) > b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block','Abc getter block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',143);

app.set('blocks.4.y',296);
}
,[],function(result)
{}
);

click('.blocks .get.number.block:nth-child(4) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.data.sockets > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(4) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.data.sockets > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(5) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.data.sockets > div.input > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(5) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.data.sockets > div.input > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.data.sockets > div.output > div:nth-child(1) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.number.block > li:nth-child(4)');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.data.sockets > div.output > div:nth-child(1) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.number.block > li:nth-child(4)');

waitService.waitForMilliseconds(500);

waitService.waitForMilliseconds(100);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(2) > div.data.sockets > div.input > div.socket.no-select.string > div > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(1)');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block:nth-child(3) > div.data.sockets > div.input > div.socket.no-select.string > div > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open.mlxs > li:nth-child(2)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('verify generated code', async () => 
    
    {
waitService.waitForMilliseconds(1000);

execute(function(simple)
{
return app.generateCode();
}
,[],function(result)
{
console.log(result.value);

console.log(macroCode);

console.assert(result.value  ===  macroCode);
}
);
}

    
  );  
  
  
});