import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let trendName = "trend" + new Date().valueOf();

let trendSAName = "trend" + new Date().valueOf();

let saveX = "trend" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let historianButton = po.historianButton;

describe("HistorianExit.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Historian", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(historianButton);

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)",
      "historian is opened"
    );
  });
  it("new trend", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    containsText("#equipList > li:nth-child(1) > a > h4", "City Center AHU-1");

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    click("#equipList > li:nth-child(2) > div > div.switch");

    click("#equipList > li:nth-child(3) > div > div.switch");

    waitService.waitForMilliseconds(1000);

    click("#relativeSelector");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("Exit", async () => {
    click("#fooAppContainer > ul > li:nth-child(5)");

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input"
    );

    setValue(
      "body > app > div.modal.vertical.middle.center > div > section > div:nth-child(1) > input",
      saveX
    );

    waitService.waitForMilliseconds(1000);

    click("body > app > div.modal.vertical.middle.center > div > footer");

    waitService.waitForMilliseconds(500);
  });
});
