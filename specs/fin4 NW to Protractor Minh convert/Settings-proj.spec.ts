import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let settingsButton = po.settingsButton;

let root =
  "body > app > section > div > div.app-nav.vertical > nav > div:nth-child";

let OK = "#controlBar > button:nth-child(2)";

let displayName = "TestProject";

let projDescription = "New project description to test the form ";

let cat = "testCategory";

let disCat = "category test";

let query = "projMeta";

describe("Settings-proj.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to settings", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(settingsButton);

    waitService.waitForMilliseconds(3000);
  });
  it("go to settings > ext", async () => {
    click(settingsButton + " > ul > li:nth-child(3)");

    waitService.waitForMilliseconds(1000);
  });
  it("refresh", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(2000);
  });
  it("assert the other options", async () => {
    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)",
      "Project Settings"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-item-icon.icon-gear",
      "icon-gear"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Project Settings"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-item-icon.icon-search",
      "icon-search"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Audit Settings"
    );

    cssClassPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(7) > a > div > div > span.nav-item-icon.icon-tags",
      "icon-tags"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(7) > a > div > div > span.nav-label.flex.italic.mnl.single-line-text.non-interactive",
      "Project Categories"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Test Project Settings", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    containsText("#title", "Project Settings");

    containsText(
      "#content > div.form-item-holder.textinput > label",
      "Project Display Name"
    );

    elementPresent("#content > div.form-item-holder.textinput > input");

    clearValue("#content > div.form-item-holder.textinput > input");

    setValue("#content > div.form-item-holder.textinput > input", displayName);

    containsText(
      "#content > div.form-item-holder.textarea > label",
      "Project Description"
    );

    elementPresent("#content > div.form-item-holder.textarea > textarea");

    clearValue("#content > div.form-item-holder.textarea > textarea");

    setValue(
      "#content > div.form-item-holder.textarea > textarea",
      projDescription
    );

    click(OK);
  });
  it("Test Audit Settings", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5)"
    );

    containsText("#title", "Audit Settings");

    containsText(
      "#content > div:nth-child(1) > div > label",
      "Action : Log each time an action is invoked"
    );

    containsText(
      "#content > div:nth-child(2) > div > label",
      "Commit : Log each time a diff is committed to the folio database"
    );

    containsText(
      "#content > div:nth-child(3) > div > label",
      "Login : Log each time a user logs into the REST HTTP API"
    );

    click("#content > div:nth-child(1) > div > div");

    click("#content > div:nth-child(2) > div > div");

    click("#content > div:nth-child(3) > div > div");

    click(OK);
  });
  it("Test Project Categories", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(7)"
    );

    containsText("#title", "Project Categories");

    containsText("#content > div > label", "Select Category");

    click("#content > div > select");

    waitService.waitForMilliseconds(500);

    containsText("#content > div > select > option:nth-child(1)", "Create New");

    containsText("#content > div > select > option:nth-last-child(2)", "HVAC");

    click("#content > div > select > option:nth-child(1)");

    click(OK);

    waitService.waitForMilliseconds(500);

    containsText("#title", "New Category");

    containsText("#content > div:nth-child(1) > label", "Name");

    elementPresent("#content > div:nth-child(1) > input");

    containsText("#content > div:nth-child(2) > label", "Display Name");

    elementPresent("#content > div:nth-child(2) > input");

    clearValue("#content > div:nth-child(1) > input");

    setValue("#content > div:nth-child(1) > input", cat);

    clearValue("#content > div:nth-child(2) > input");

    setValue("#content > div:nth-child(2) > input", disCat);

    click(OK);

    finEval(query, function(output) {
      console.log(output[0]);

      if (
        output.length > 0 &&
        output[0].dis == displayName &&
        output[0].doc == projDescription
      ) {
        console.log("Display name and description are correct");
      } else {
        console.log("Error");
      }
    });

    waitService.waitForMilliseconds(1000);
  });
});
