import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let fs= require('fs');

let macroCode= fs.readFileSync('./tests/fixtures/programs/macroBlock.txt').toString();



describe('LogicBuilder-testMacroBlock.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(1500);
}

    
  );  
  it('Add variables in bLine', async () => 
    
    {
execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(1000);

elementPresent('.variables > div.scroller > div:nth-child(3)');

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

elementPresent('.variables > div.scroller > div:nth-child(4)');

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

elementPresent('.variables > div.scroller > div:nth-child(5)');
}

    
  );  
  it('Add a macroblock and drag it to stage in bLine', async () => 
    
    {
click('.add.panel-button .macro');

waitService.waitForMilliseconds(500);

moveToElement('.variables .scroller .macro > span > span',1,1);

waitService.waitForMilliseconds(500);

click('.variables .scroller .macro > i.pencil.icon');

waitService.waitForMilliseconds(500);

keys([driver.Keys.CONTROL,'a', driver.Keys.CONTROL, driver.Keys.DELETE]);

waitService.waitForMilliseconds(500);

keys('min-max');

waitService.waitForMilliseconds(500);

keys(driver,Keys,ENTER);

containsText('.variables .scroller .macro > span > span','min-max');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller .macro  b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .macro.commandWithDataOutputs.block','The macro block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',406);

app.set('blocks.1.y',268);
}
,[],function(result)
{}
);
}

    
  );  
  it('Open macroblock by clicking the icon on the block', async () => 
    
    {
click('.blocks .macro.commandWithDataOutputs.block > header > i');

waitService.waitForMilliseconds(500);

cssClassPresent('.variables .scroller .macro','viewing','Macroblock is open for editing');

elementPresent('.bLine.banner.macro','Macroblock banner is present inside');

click('i.cog.icon');

waitService.waitForMilliseconds(500);

cssClassPresent('.bLine.banner.macro > div','open','macroblock settings window is open after clicking the settings icon');
}

    
  );  
  it('Edit ports for macroblock', async () => 
    
    {
click('#stage > div.bLine.banner.macro > div > div > div.tac.mtm > button');

waitService.waitForMilliseconds(500);

elementPresent('.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4)','A fourth port is added');

click('.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label','output','Fourth port is now set to 'output'');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label','number','Data type changed to number for first port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label','number','Data type changed to number for second port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label','number','Data type changed to number for third port');

click('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open > li:nth-child(2)');

waitService.waitForMilliseconds(500);

containsText('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > label','number','Data type changed to number for fourth port');

clearValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]','min');

waitService.waitForMilliseconds(500);

value('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(3) > td:nth-child(2) > input[type="text"]','min','The name for the third port is 'min'');

clearValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]','max');

waitService.waitForMilliseconds(500);

value('#stage > div.bLine.banner.macro > div > div > div.scroll > table > tbody > tr:nth-child(4) > td:nth-child(2) > input[type="text"]','max','The name for the fourth port is 'max'');
}

    
  );  
  it('Drag blocks from the library to the stage', async () => 
    
    {
execute(function(simple)
{
app.set('blocks.0.x',106);

app.set('blocks.0.y',71);
}
,[],function(result)
{}
);

setValue('.blocklibrary > div > div.filters > div > input[type="text"]','if');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .if.block','The IF block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',373);

app.set('blocks.1.y',70);
}
,[],function(result)
{}
);

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);

setValue('.blocklibrary > div > div.filters > div > input[type="text"]','cmpgt');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.blocklibrary > div > div.category > div.contents > div:nth-child(1)', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .cmpGt.block','The cmpGt block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',140);

app.set('blocks.2.y',211);
}
,[],function(result)
{}
);

clearValue('.blocklibrary > div > div.filters > div > input[type="text"]');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Drag ports to the stage', async () => 
    
    {
execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(1) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block:nth-child(4)','Port 1 block is on stage');

execute(function(simple)
{
app.set('blocks.3.x',4);

app.set('blocks.3.y',274);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(2) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block:nth-child(5)','Port 2 block is on stage');

execute(function(simple)
{
app.set('blocks.4.x',14);

app.set('blocks.4.y',338);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(6)','First port 3 block is on stage');

execute(function(simple)
{
app.set('blocks.5.x',551);

app.set('blocks.5.y',121);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(7)','First port 4 block is on stage');

execute(function(simple)
{
app.set('blocks.6.x',625);

app.set('blocks.6.y',293);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(3) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(8)','Second port 3 block is on stage');

execute(function(simple)
{
app.set('blocks.7.x',408);

app.set('blocks.7.y',296);
}
,[],function(result)
{}
);

execute(dragAndDrop,['.bLine.banner.macro table > tbody > tr:nth-child(4) > td:nth-child(1) > b', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(9)','Second port 4 block is on stage');

execute(function(simple)
{
app.set('blocks.8.x',507);

app.set('blocks.8.y',445);
}
,[],function(result)
{}
);

click('i.cog.icon');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Link blocks betweem them', async () => 
    
    {
click('.blocks .__start__ > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(6) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(6) > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(7) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(8) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(8) > div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(9) > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpGt.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .if.block > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(4) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpGt.block > div > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(5) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .cmpGt.block > div > div.input > div:nth-child(2) > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(4) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(7) > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(5) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(6) > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(4) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(8) > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .get.number.block:nth-child(5) > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(9) > div.data.sockets > div > div > b');

waitService.waitForMilliseconds(500);

elementPresent('#stage > div.scroller > svg > path:nth-child(1)');

elementPresent('#stage > div.scroller > svg > path:nth-child(2)');

elementPresent('#stage > div.scroller > svg > path:nth-child(3)');

elementPresent('#stage > div.scroller > svg > path:nth-child(4)');

elementPresent('#stage > div.scroller > svg > path:nth-child(5)');

elementPresent('#stage > div.scroller > svg > path:nth-child(6)');

elementPresent('#stage > div.scroller > svg > path:nth-child(7)');

elementPresent('#stage > div.scroller > svg > path:nth-child(8)');

elementPresent('#stage > div.scroller > svg > path:nth-child(9)');

elementPresent('#stage > div.scroller > svg > path:nth-child(10)');

elementPresent('#stage > div.scroller > svg > path:nth-child(11)');

elementPresent('#stage > div.scroller > svg > path:nth-child(12)');

cssClassPresent('.blocks .__start__ > div > div > div ','linked');

cssClassPresent('.blocks .if.block > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(1) ','linked');

cssClassPresent('.blocks .if.block > div.exec.sockets > div.out > div:nth-child(2) ','linked');

cssClassPresent('.blocks .if.block > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(6) > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(6) > div.exec.sockets > div.out > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(7) > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(8) > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(8) > div.exec.sockets > div.out > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(9) > div.exec.sockets > div.in > div ','linked');

cssClassPresent('.blocks .get.number.block:nth-child(4) > div > div.output > div ','linked');

cssClassPresent('.blocks .get.number.block:nth-child(5) > div > div.output > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(6) > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(7) > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(8) > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .set.number.block:nth-child(9) > div.data.sockets > div > div ','linked');

cssClassPresent('.blocks .cmpGt.block > div > div.output > div ','linked');

cssClassPresent('.blocks .cmpGt.block > div > div.input > div:nth-child(1) ','linked');

cssClassPresent('.blocks .cmpGt.block > div > div.input > div:nth-child(2) ','linked');
}

    
  );  
  it('Use macro block', async () => 
    
    {
click('.variables .scroller .function:nth-child(1) > i');

waitService.waitForMilliseconds(500);

click('.variables .scroller .function:nth-child(2) > i');

waitService.waitForMilliseconds(500);

click('.variables .scroller .function:nth-child(1) > i');

click('.__start__ > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block > div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['.variables .scroller > div:nth-child(4) > b.number.get.block', '.blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .get.number.block','Foo getter block is on stage');

execute(function(simple)
{
app.set('blocks.2.x',236);

app.set('blocks.2.y',348);
}
,[],function(result)
{}
);

click('.blocks .get.number.block > div > div.output > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.input > div:nth-child(1) > b');

waitService.waitForMilliseconds(500);

setValue('.blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.input > div:nth-child(2) > div > input','3');

click('.blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div:nth-child(1) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.number.block > li:nth-child(3)');

waitService.waitForMilliseconds(500);

click('.blocks .macro.commandWithDataOutputs.block > div.data.sockets > div.output > div:nth-child(2) > div > label');

waitService.waitForMilliseconds(500);

click('#ractive-select-dropdown-container > ul.dropdown.open.pill.macro-output-dropdown.get.number.block > li:nth-child(4)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('verify generated code', async () => 
    
    {
waitService.waitForMilliseconds(1000);

execute(function(simple)
{
return app.generateCode();
}
,[],function(result)
{
console.log(result.value);

console.log(macroCode);

console.assert(result.value  ===  macroCode);

click('#panel > section.variables > div.scroller > div.macro.viewing > span > span');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(2)');

waitService.waitForMilliseconds(500);

click('#panel > section.variables > div.scroller > div.macro.viewing > span > span');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(1)');

waitService.waitForMilliseconds(500);

click('#stage > div.tab > span:nth-child(2)');

waitService.waitForMilliseconds(5000);
}
);
}

    
  );  
  
  
});