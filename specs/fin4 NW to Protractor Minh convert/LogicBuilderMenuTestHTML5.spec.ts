import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

describe("LogicBuilderMenuTestHTML5.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton);

    waitService.waitForMilliseconds(500);

    click(logicBuilderButton + "> ul > li:nth-child(3)");

    waitService.waitForMilliseconds(1000);
  });
  it("Logic builder menu options", async () => {
    waitService.waitForMilliseconds(200);

    moveToElement(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)",
      1,
      1
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Logic Builder Menu>>Logic Info>> Blocks", async () => {
    waitService.waitForMilliseconds(1000);

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > div > h6",
      "Logic Debug Blocks Category",
      "Logic Debug Blocks Category divider"
    );

    waitService.waitForMilliseconds(1000);

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5)",
      "Commands",
      "Commands submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(6)",
      "Control flow",
      "Control Flow submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(7)",
      "Expression",
      "Expression submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8)",
      "Language",
      "Language submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(9)",
      "Logical",
      "Logical submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) ",
      "Math",
      "Math submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(11)",
      "Misc",
      "Misc submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12)",
      "Strings",
      "Strings submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(13)",
      "Time",
      "Time submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(14)",
      "Weather",
      "Weather submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > div > h6",
      "Logic Debug Blocks Type",
      "Logic Debug Blocks Type divider"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(16)",
      "Boolean",
      "Boolean submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(17)",
      "Date",
      "Date submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(18)",
      "DateTime",
      "DateTime submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(19",
      "Miscellaneous",
      "Miscellaneous submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(20)",
      "Number",
      "Number submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(21)",
      "Object",
      "Object submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(22)",
      "Statement",
      "Statement submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(23)",
      "String",
      "String submenu is present"
    );

    containsText(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(24)",
      "Time",
      "Time submenu is present"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitForElementPresent("body", "REFRESHED");

    waitService.waitForMilliseconds(2000);
  });
  it("Back to Logic Builder >> Programs Menu ", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)",
      "BACK TO PROGRAMS"
    );

    waitService.waitForMilliseconds(2000);
  });
  it("Logic Builder Menu>>Logic Info>> Programs", async () => {
    waitService.waitForMilliseconds(2000);

    moveToElement(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)",
      1,
      1
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)"
    );

    waitForElementPresent("body", "REFRESHED");

    waitService.waitForMilliseconds(2000);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.divider-container",
      "Logic Debug Programs divider"
    );
  });
  it("Click on the 10th program from the list, see the options present", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > a > div > div > div.drag.hitarea.full"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(1)",
      "Edit button"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(2)",
      "ProgON button"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(3)",
      "More button"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(4)",
      "Delete button"
    );

    waitService.waitForMilliseconds(500);
  });
  it("Click on Edit, bLine should open", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(1)"
    );

    frame(0);

    waitService.waitForMilliseconds(500);

    elementPresent("#stage > div.scroller > div", "bLine is open");

    waitService.waitForMilliseconds(500);
  });
  it("Go back to  Logic Builder Menu>>Logic Info>> Programs", async () => {
    frame(null);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(2000);

    waitService.waitForMilliseconds(1000);

    moveToElement(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15)",
      1,
      1
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Click the Program ON button", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(2)"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#content > div > div > input", "ProgramON form");

    setValue("#content > div > div > input", " and test");

    click("#content > div > div > button");

    waitService.waitForMilliseconds(600);

    click("#content > div > div > button");

    click(
      "body > app > div:nth-child(4) > div > footer > button.button-default.focused.margin.small.l-side"
    );

    click(
      "body > app > div:nth-child(4) > div > footer > button.button-default.focused.margin.small.l-side"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);
  });
  it("Click the More button", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(3)"
    );

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(12) > a > div > div > div.drag.handle.margin.l-side.normal",
      "more submenu is open"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1)"
    );

    waitService.waitForMilliseconds(3000);
  });
  it("Click the Delete Button", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(15) > ul > li:nth-child(4)"
    );

    elementPresent("#controlBar > button:nth-child(2)", "delete form is open");

    waitService.waitForMilliseconds(2000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(2000);
  });
  it("Programs>>Logic info>>TASKS", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.divider-container",
      "tasks menu is present"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(4)"
    );

    waitForElementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)",
      "Faults"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1)"
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(5)"
    );

    waitForElementPresent(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(3)",
      "OK"
    );

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(1)"
    );

    waitService.waitForMilliseconds(2000);

    waitService.waitForMilliseconds(5000);
  });
});
