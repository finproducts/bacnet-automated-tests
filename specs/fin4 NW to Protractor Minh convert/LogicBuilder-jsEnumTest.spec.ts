import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "jsProgram" + new Date().valueOf();

describe("LogicBuilder-jsEnumTest.js", () => {
  it("Go to Block Programming js", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(1)");

    waitService.waitForMilliseconds(500);
  });
  it("New JS PROGRAM", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(2) > i.variable.symbol"
    );

    elementPresent(
      "#panel > section.variables > div.scroller > div",
      "New Variable is created"
    );

    click("#panel > section.controls > div.icons > i.file.icon");

    waitForElementPresent(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > header",
      "Save Prompt"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button:nth-child(1)"
    );

    elementPresent(
      "#panel > section.variables > div.scroller > div",
      "No new program is created"
    );

    waitService.waitForMilliseconds(1000);

    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.newProgram.modal > div > div.window > div > span:nth-child(1)",
      "JS option is present"
    );

    click(
      "body > app > div.newProgram.modal > div > div.window > div > span:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementNotPresent(
      "#panel > section.variables > div.scroller > div",
      "new program is created"
    );

    waitService.waitForMilliseconds(900);
  });
  it("Rename JS PROGRAM", async () => {
    click(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input"
    );

    clearValue(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input"
    );

    setValue(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input",
      programName
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Enum Editor", async () => {
    click("#panel > section.controls > div.icons > i.enum.icon");

    elementPresent(
      "body > app > div.enum.modal > div > div.window > header",
      "Enum Editor form is open"
    );
  });
  it("Enum Form", async () => {
    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.controls > i.plus.icon"
    );

    waitService.waitForMilliseconds(200);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.controls > i.plus.icon"
    );

    waitService.waitForMilliseconds(200);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.controls > i.plus.icon"
    );

    waitService.waitForMilliseconds(200);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.controls > i.plus.icon"
    );

    waitService.waitForMilliseconds(4000);

    elementPresent(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(2)",
      "newEnum1 is created"
    );

    elementPresent(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(3)",
      "newEnum2 is created"
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.controls > i.minus.icon.mls"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(3)"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.controls > i.minus.icon.mls"
    );

    waitService.waitForMilliseconds(1000);

    elementNotPresent(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(3)",
      "newEnum2 is deleted"
    );

    elementNotPresent(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(4)",
      "newEnum3 is deleted"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("New Enum edit", async () => {
    click(
      "body > app > div.enum.modal > div > div.window > div > div.list > div.items > div:nth-child(2)"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > div > i"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > div > input"
    );

    clearValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > div > input"
    );

    setValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > div > input",
      "editedEnum"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.key > input"
    );

    setValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.key > input",
      "numeric"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input"
    );

    clearValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input"
    );

    setValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input",
      "12345678"
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input"
    );

    waitService.waitForMilliseconds(1000);

    keys(driver, keys, ENTER);

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.key > input"
    );

    clearValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.key > input"
    );

    setValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.key > input",
      "boolean"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.value > input"
    );

    clearValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.value > input"
    );

    setValue(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(1) > td.value > input",
      "false"
    );

    click(
      "body > app > div.enum.modal > div > div.window > div > div.values > table > tr:nth-child(2) > td.value > input"
    );

    keys(driver, keys, ENTER);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);
  });
});
