import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "jsProgram" + new Date().valueOf();

describe("LogicBuilder-jsTopMenu.js", () => {
  it("Go to Block Programming js", async () => {
    url("localhost:85/pod/finBlockProgrammingExt/index.html");

    waitService.waitForMilliseconds(1000);

    waitForElementPresent(".blocks", 1000);

    click("i.file.icon");

    waitService.waitForMilliseconds(500);

    click("button.mls.danger.button");

    waitService.waitForMilliseconds(500);

    click(".tac span:nth-child(1)");

    waitService.waitForMilliseconds(500);
  });
  it("New JS PROGRAM", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol"
    );

    elementPresent(
      "#panel > section.variables > div.scroller > div",
      "New Variable is created"
    );

    click("#panel > section.controls > div.icons > i.file.icon");

    waitForElementPresent(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > header",
      "Save Prompt"
    );

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button:nth-child(1)"
    );

    elementPresent(
      "#panel > section.variables > div.scroller > div",
      "No new program is created"
    );

    waitService.waitForMilliseconds(1000);

    click("#panel > section.controls > div.icons > i.file.icon");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.unsavedWorkWarning.modal > div > div.window > footer > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.newProgram.modal > div > div.window > div > span:nth-child(1)",
      "JS option is present"
    );

    click(
      "body > app > div.newProgram.modal > div > div.window > div > span:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementNotPresent(
      "#panel > section.variables > div.scroller > div",
      "new program is created"
    );

    waitService.waitForMilliseconds(900);
  });
  it("Rename JS PROGRAM", async () => {
    click(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input"
    );

    clearValue(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input"
    );

    setValue(
      "#panel > section.controls > div.program-name > span:nth-child(1) > input",
      programName
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Save JS PROGRam", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol"
    );

    click("#panel > section.controls > div.icons > i.save.icon");

    waitService.waitForMilliseconds(300);

    elementPresent(
      "#panel > section.controls > div.icons > i.save.icon.disabled",
      "Program is saved"
    );
  });
  it("Export JS code", async () => {
    click("#panel > section.controls > div.icons > i.export.icon");

    waitService.waitForMilliseconds(900);

    click("body > app > div.export.modal > div > div.close");
  });
  it("D&D", async () => {
    click(
      "#panel > section.variables > header > div:nth-child(3) > i.variable.symbol"
    );

    elementPresent(
      "#panel > section.variables > div.scroller > div:nth-child(1)"
    );

    for (let n = 1; n <= 9; n++) {
      execute(dragAndDrop, [
        "#panel > section.variables > div.scroller > div:nth-child(1) > b.variable.get.block",
        ".blocks"
      ]);
    }

    waitService.waitForMilliseconds(1000);

    execute(
      function(simple) {
        let numberOfBlocks = app.get("blocks").length;

        for (let n = 1; n < numberOfBlocks; n++) {
          app.set('blocks."  +  n  +  ".x', 210 + n * 20);

          app.set('blocks."  +  n  +  ".y', 404 + n * 20);
        }
      },
      [],
      function(result) {}
    );

    moveToElement("#stage > div.scroller > div > div:nth-child(9)", 1, 1);

    waitService.waitForMilliseconds(5000);
  });
  it("Align Left", async () => {
    waitService.waitForMilliseconds(900);

    keys(driver, Keys, CONTROL);

    click("#stage > div > div > div:nth-child(2)");

    click("#stage > div > div > div:nth-child(3)");

    click("#stage > div > div > div:nth-child(4)");

    click("#stage > div > div > div:nth-child(5)");

    click("#stage > div > div > div:nth-child(6)");

    click("#stage > div > div > div:nth-child(7)");

    click("#stage > div > div > div:nth-child(8)");

    click("#stage > div > div > div:nth-child(9)");

    click("#stage > div > div > div:nth-child(10)");

    click("#panel > section.controls > div.icons > div");

    click(
      "#panel > section.controls > div.icons > div > div > i.align-left.icon"
    );

    waitService.waitForMilliseconds(900);

    cssProperty(
      "#stage > div > div > div:nth-child(3)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(4)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(5)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(6)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(7)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(8)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(9)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "left",
      "230px",
      "blocks are aligned to the left"
    );

    waitService.waitForMilliseconds(1000);

    keys(driver, Keys, CONTROL);
  });
  it("Undo Align Left", async () => {
    click("#panel > section.controls > div.icons > i.undo.icon");

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "left",
      "390px",
      "undo align succesful"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Align Right", async () => {
    waitService.waitForMilliseconds(900);

    keys(driver, Keys, CONTROL);

    click("#panel > section.controls > div.icons > div");

    click(
      "#panel > section.controls > div.icons > div > div > i.align-right.icon.mls"
    );

    waitService.waitForMilliseconds(2000);

    cssProperty(
      "#stage > div > div > div:nth-child(3)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(4)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(5)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(6)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(7)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(8)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(9)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "left",
      "584px",
      "blocks are aligned to the right"
    );

    waitService.waitForMilliseconds(1000);

    keys(driver, Keys, CONTROL);
  });
  it("Undo Align Right", async () => {
    click("#panel > section.controls > div.icons > i.undo.icon");

    cssProperty(
      "#stage > div > div > div:nth-child(2)",
      "left",
      "230px",
      "undo align succesful"
    );

    waitService.waitForMilliseconds(100);
  });
  it("Align Top", async () => {
    waitService.waitForMilliseconds(900);

    keys(driver, Keys, CONTROL);

    click("#panel > section.controls > div.icons > div");

    click(
      "#panel > section.controls > div.icons > div > div > i.align-top.icon.mls"
    );

    waitService.waitForMilliseconds(2000);

    cssProperty(
      "#stage > div > div > div:nth-child(3)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(4)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(5)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(6)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(7)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(8)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(9)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "top",
      "424px",
      "blocks are aligned to the top"
    );

    waitService.waitForMilliseconds(1000);

    keys(driver, Keys, CONTROL);
  });
  it("Undo Align Top", async () => {
    click("#panel > section.controls > div.icons > i.undo.icon");

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "left",
      "390px",
      "undo align succesful"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Align Bottom", async () => {
    waitService.waitForMilliseconds(900);

    keys(driver, Keys, CONTROL);

    click("#panel > section.controls > div.icons > div");

    click(
      "#panel > section.controls > div.icons > div > div > i.align-bottom.icon.mls"
    );

    waitService.waitForMilliseconds(2000);

    cssProperty(
      "#stage > div > div > div:nth-child(3)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(4)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(5)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(6)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(7)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(8)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(9)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "top",
      "584px",
      "blocks are aligned to the bottom"
    );

    waitService.waitForMilliseconds(1000);

    keys(driver, Keys, CONTROL);
  });
  it("Undo Align Bottom", async () => {
    click("#panel > section.controls > div.icons > i.undo.icon");

    cssProperty(
      "#stage > div > div > div:nth-child(10)",
      "left",
      "390px",
      "undo align succesful"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Redo Align Bottom", async () => {
    click("#panel > section.controls > div.icons > i.undo.icon");

    cssProperty(
      "#stage > div > div > div:nth-child(2)",
      "top",
      "424px",
      "redo succesful"
    );

    waitService.waitForMilliseconds(1000);
  });
});
