import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let logicBuilderButton = po.logicBuilderButton;

let programName = "test" + new Date().valueOf();

describe("LogicBuilder-testVarbLine2.js", () => {
  it("go to City Center VAV-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Logic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click("body > app > section > section > ul > li:nth-child(18) > div");

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > section > ul > li:nth-child(18) > ul > li:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Create bLine Program", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(500);

    keys(programName);

    waitService.waitForMilliseconds(500);

    keys(driver, Keys, CONTROL);

    waitService.waitForMilliseconds(500);

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > div.modal.form-stack.vertical.middle.center > div > div > button"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div.nav-container.vertical.middle.no-flex.mobile-hidden > a"
    );

    waitService.waitForMilliseconds(1000);

    frame(0);

    waitService.waitForMilliseconds(1000);
  });
  it("Add variables", async () => {
    frame(null);

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(1000);

    keys("numVar");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(100);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    waitService.waitForMilliseconds(100);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(100);

    setValue("#content > div.form-item-holder.numericstepper > input", "100");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(100);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);

    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.textinput > input");

    keys("boolVar");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(2)"
    );

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(1)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);

    frame(null);

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.textinput > input");

    keys("strVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(100);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#controlBar > button:nth-child(2)");

    setValue(
      "#content > div.form-item-holder.textinput > input",
      "stringValue"
    );

    waitService.waitForMilliseconds(100);

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);

    frame(null);

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(100);

    keys("dateVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(1000);

    click("#content > div:nth-child(2) > select > option:nth-child(2)");

    click("#content > div:nth-child(3) > select > option:nth-child(2)");

    click("#content > div:nth-child(4) > select > option:nth-child(2)");

    click("#controlBar > button:nth-child(2)");

    waitService.waitForMilliseconds(500);

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);

    frame(null);

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(100);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(10) > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(100);

    click("#content > div.form-item-holder.textinput > input");

    keys("timeVar");

    waitService.waitForMilliseconds(100);

    click("#content > div:nth-child(2) > div > input");

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(5)"
    );

    click("#controlBar > button:nth-child(2)");

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(1) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(2) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(3) > select > option:nth-child(2)"
    );

    click(
      "#content > div.form-item-holder.group > div > div:nth-child(4) > select > option:nth-child(2)"
    );

    click("#controlBar > button:nth-child(2)");

    click("#controlBar > button");

    waitService.waitForMilliseconds(6000);

    frame(null);
  });
});
