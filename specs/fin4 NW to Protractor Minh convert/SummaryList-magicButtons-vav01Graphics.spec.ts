import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let programName = "test" + new Date().valueOf();

let selectorLibrary = require("./po.js");

let summaryButton = po.summaryButton;

describe("SummaryList-magicButtons-vav01Graphics.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );
  });
  it("go to Summary", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(summaryButton);

    waitService.waitForMilliseconds(1500);
  });
  it("Summary menu", async () => {
    elementPresent("#innerApp > header", "summary app is open");

    click("#innerApp > section > ul > li > div");

    waitService.waitForMilliseconds(1000);
  });
  it("magic buttons-Graphics-VAV-01", async () => {
    click(
      "#fooAppContainer > section > ul > li:nth-child(1) > div > div.vertical.flex.non-auto > div:nth-child(1) > div"
    );

    waitService.waitForMilliseconds(2000);

    click(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(1) > i"
    );
  });
  it("Click on Graphics", async () => {
    click(
      "body > app > div.modal > div > div.buttons.vertical.middle.center > div:nth-child(4)"
    );

    waitService.waitForMilliseconds(4000);

    containsText("#innerApp > header > h4", "— Graphics —");

    containsText(
      "#innerApp > section > ul:nth-child(1) > li > a > div.vertical.flex.non-auto.margin.normal.l-side > span",
      "ON: City Center Vav-01"
    );
  });
});
