import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let programName = "JSGB" + new Date().valueOf();

describe("LogicBuilder-JSFromGB.js.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Graphic Builder", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton);

    waitService.waitForMilliseconds(500);

    click(graphicsBuilderButton + "> ul > li:nth-child(3)");

    waitService.waitForMilliseconds(3000);
  });
  it("New Graphic Form", async () => {
    click("#content > div.form-item-holder.textinput > input");

    waitService.waitForMilliseconds(1000);

    keys(programName);

    waitService.waitForMilliseconds(1000);

    click("#content > div.form-item-holder.combobox > select");

    waitService.waitForMilliseconds(300);

    click(
      "#content > div.form-item-holder.combobox > select > option:nth-child(4)"
    );

    click("#controlBar > button:nth-child(2)");

    waitForElementPresent(
      "body > app > div.modal.form-stack.vertical.middle.center > div",
      2000
    );

    moveToElement("#controlBar > button", 1, 1);

    waitService.waitForMilliseconds(5000);

    click("#controlBar > button");

    waitService.waitForMilliseconds(1000);
  });
  it("Open The Graphic", async () => {
    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > a > div > div > div.drag.hitarea.full"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > div > div.app-nav.vertical > nav > div:nth-child(8) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Drag a button", async () => {
    frame(0);

    waitService.waitForMilliseconds(1000);

    waitService.waitForMilliseconds(1000);

    execute(dragAndDrop, [
      "#container > ul:nth-child(6) > li:nth-child(2) > iron-icon",
      "#main-content > section > div"
    ]);

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "#projects-container > div > div > button",
      "Button is dragged on the stage"
    );
  });
  it("Right-click on button", async () => {
    moveToElement("#projects-container > div > div > button", 2, 2);

    mouseButtonClick(2);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    moveToElement(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1)",
      2,
      2
    );

    waitService.waitForMilliseconds(300);

    click(
      "body > ul > li.context-menu-item.context-menu-submenu.icon.icon-events > ul > li:nth-child(1) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#scriptArea > div.ace_scroller > div",
      "Script editor form launched succesfully"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Script Editor Form", async () => {
    frame(1);

    waitService.waitForMilliseconds(5000);

    click("#editorModeSelector > ul > li:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
});
