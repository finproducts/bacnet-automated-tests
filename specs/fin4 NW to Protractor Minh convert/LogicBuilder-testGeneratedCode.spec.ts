import {by, browser, element, ExpectedConditions as EC, $, $$} from 'protractor';
import {finEval, frame, end, getText, clearValue, setValue, elementPresent, containsText, notContainsText,resizeWindow, getElementSize,hidden, getLocationInView,equal, notEqual,
      getAttribute,urlContains,click,elements,elementNotPresent,value,moveToElement,waitForElementPresent,waitForElementNotPresent,cssClassPresent,cssClassNotPresent,cssProperty,
      window_handles,url, switchWindow} from '../../helpers/nwapis';
import {waitService} from '../../helpers/wait-service';
import Fin4PO from '../../page_objects/finstack/fin4.page';


let po = new Fin4PO();


let expect= require('chai').expect;

let dragAndDrop= require('html-dnd').codeForSelectors;

let fs= require('fs');

let bLineCode= fs.readFileSync('./tests/fixtures/programs/bLineCode.txt').toString();



describe('LogicBuilder-testGeneratedCode.js',() =>{
  
  it('Go to Block Programming bLine', async () => 
    
    {
url('localhost:85/pod/finBlockProgrammingExt/index.html');

waitService.waitForMilliseconds(1000);

waitForElementPresent('.blocks',1000);

click('i.file.icon');

waitService.waitForMilliseconds(500);

click('button.mls.danger.button');

waitService.waitForMilliseconds(500);

click('.tac span:nth-child(6)');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('Add variables and drag them to stage', async () => 
    
    {
execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(function(simple)
{
app.addVariable([object Object])  ,  []  ,  [object Object]
}
);

waitService.waitForMilliseconds(500);

execute(dragAndDrop,['#panel > section.variables > div > div.number.variable > b.number.set.block', '.scroller .blocks']);

waitService.waitForMilliseconds(1000);

elementPresent('.blocks .set.number.block:nth-child(2)','The numeric n1 setter block is on stage');

execute(function(simple)
{
app.set('blocks.1.x',443);

app.set('blocks.1.y',228);
}
,[],function(result)
{}
);

waitService.waitForMilliseconds(1000);

execute(dragAndDrop,['.variables .scroller div:nth-child(4)  b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.2.x',670);

app.set('blocks.2.y',293);
}
,[],function(result)
{}
);

waitService.waitForMilliseconds(1000);

execute(dragAndDrop,['.variables .scroller div:nth-child(5)  b.number.set.block', '.blocks']);

waitService.waitForMilliseconds(1000);

execute(function(simple)
{
app.set('blocks.3.x',856);

app.set('blocks.3.y',410);
}
,[],function(result)
{}
);

waitService.waitForMilliseconds(1000);
}

    
  );  
  it('Link blocks', async () => 
    
    {
click('.__start__.block > div > div > div > b');

waitService.waitForMilliseconds(500);

click('.blocks .set.number.block:nth-child(2) div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div:nth-child(2) > div.data.sockets > div.input > div > div > div > input[type="number"]','123');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(2) div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(3) div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div:nth-child(3) > div.data.sockets > div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div:nth-child(3) > div.data.sockets > div.input > div > div > div > input[type="number"]','999');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(3) div.exec.sockets > div.out > div > b');

waitService.waitForMilliseconds(500);

click('#stage > div.scroller > div > div:nth-child(4) div.exec.sockets > div.in > div > b');

waitService.waitForMilliseconds(500);

clearValue('#stage > div.scroller > div > div:nth-child(4) > div.data.sockets >  div.input > div > div > div > input[type="number"]');

waitService.waitForMilliseconds(500);

setValue('#stage > div.scroller > div > div:nth-child(4) > div.data.sockets > div.input > div > div > div > input[type="number"]','8769678');

waitService.waitForMilliseconds(500);
}

    
  );  
  it('verify generated code', async () => 
    
    {
waitService.waitForMilliseconds(1000);

execute(function(simple)
{
return app.generateCode();
}
,[],function(result)
{
console.log(result.value);

waitService.waitForMilliseconds(5000);

expect(result.value);

to;

equal(bLineCode);
}
);
}

    
  );  
  
  
});