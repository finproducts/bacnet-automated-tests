import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let selectorLibrary = require("./po.js");

let pointGraphicsButton = po.pointGraphicsButton;

let queryNUM =
  'readAll(point and  kind == "Number" and equipRef==@1eeaf2cd-f2e9b66d).sort((a,b) => if(a->navName > b->navName) 1 else -1)[0..0]';

let queryBool =
  'readAll(point and  kind == "Bool" and equipRef==@1eeaf2cd-f2e9b66d).sort((a,b) => if(a->navName > b->navName) 1 else -1)[0..0]';

let id = "City Center AHU-1";

let OK = "data is correct";

let error = "ERROR!";

describe("PointGraphics-Folio.js", () => {
  it("go to City Center AHU-1", async () => {
    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to point graphics", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(pointGraphicsButton);

    waitService.waitForMilliseconds(1000);
  });
  it("Assert CHVW-Numeric Output", async () => {
    containsText(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > div > h4",
      "CHWV"
    );

    attributeContains(
      "#equipList > li:nth-child(1) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/image3.png",
      "Heater"
    );

    attributeContains(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_num.png",
      "Numeric Output Icon"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button",
      "action-numeric",
      "Numeric button-purple"
    );

    cssClassPresent(
      "#equipList > li:nth-child(1) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(1) > div > div.vertical.flex.non-auto > span:nth-child(2)"
    );

    text;

    to;

    match(/\d*[.]?\d*\s%/g);
  });
  it("Assert EF Boolean Output", async () => {
    containsText(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > div > h4",
      "EF"
    );

    attributeContains(
      "#equipList > li:nth-child(2) > div > div.point-thumb.margin.normal.r-side.no-flex",
      "style",
      "/pod/finStackMobileExt/assets/images/points/fan.png",
      "Fan"
    );

    attributeContains(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > div > img",
      "src",
      "/pod/finStackMobileExt/assets/images/icons/output_bool.png",
      "Boolean Output"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > button",
      "action-boolean",
      "Boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > button > span",
      "icomoon-lighting",
      "Lightning symbol"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button",
      "action-boolean",
      "boolean button-green"
    );

    cssClassPresent(
      "#equipList > li:nth-child(2) > div > div.vertical.no-flex.right.middle > div > button > span",
      "icomoon-trend",
      "Trend Symbol"
    );

    element(
      "#equipList > li:nth-child(2) > div > div.vertical.flex.non-auto > span:nth-child(2) "
    );

    text;

    to;

    match(/([ONOFF])/g);
  });
  it("QueryNUM", async () => {
    finEval(queryNUM, function(output) {
      console.log(output);

      if (
        output.length > 0 &&
        output[0].cmd == "✓" &&
        output[0].navName == "CHWV" &&
        output[0].kind == "Number" &&
        output[0].point == "✓" &&
        output[0].unit == "%" &&
        output[0].writable == "✓" &&
        output[0].his == "✓"
      ) {
        console.log(OK);
      } else {
        console.log(error);
      }
    });

    waitService.waitForMilliseconds(1000);
  });
  it("QueryBool", async () => {
    finEval(queryBool, function(output) {
      console.log(output);

      if (
        output.length > 0 &&
        output[0].cmd == "✓" &&
        output[0].navName == "EF" &&
        output[0].kind == "Bool" &&
        output[0].point == "✓" &&
        output[0].writable == "✓" &&
        output[0].his == "✓"
      ) {
        console.log(OK);
      } else {
        console.log(error);
      }
    });

    waitService.waitForMilliseconds(1000);
  });
});
