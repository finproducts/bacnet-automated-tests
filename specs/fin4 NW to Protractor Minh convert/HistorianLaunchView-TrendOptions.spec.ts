import {
  by,
  browser,
  element,
  ExpectedConditions as EC,
  $,
  $$
} from "protractor";
import {
  finEval,
  frame,
  end,
  getText,
  clearValue,
  setValue,
  elementPresent,
  containsText,
  notContainsText,
  resizeWindow,
  getElementSize,
  hidden,
  getLocationInView,
  equal,
  notEqual,
  getAttribute,
  urlContains,
  click,
  elements,
  elementNotPresent,
  value,
  moveToElement,
  waitForElementPresent,
  waitForElementNotPresent,
  cssClassPresent,
  cssClassNotPresent,
  cssProperty,
  window_handles,
  url,
  switchWindow
} from "../../helpers/nwapis";
import { waitService } from "../../helpers/wait-service";
import Fin4PO from "../../page_objects/finstack/fin4.page";

let po = new Fin4PO();

let expect = require("chai").expect;

let dragAndDrop = require("html-dnd").codeForSelectors;

let selectorLibrary = require("./po.js");

let historianButton = po.historianButton;

let trendName = "trend" + new Date().valueOf();

let trendSAName = "trend" + new Date().valueOf();

let saveX = "trend" + new Date().valueOf();

describe("HistorianLaunchView-TrendOptions.js", () => {
  it("go to City Center VAV-1", async () => {
    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li.horizontal.middle.launcher-nav-item > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(3) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4)"
    );

    waitService.waitForMilliseconds(500);

    click(
      "body > app > section > header > div.flip-box.horizontal.flex > div > div > ul > li:nth-child(4) > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("go to Historian", async () => {
    waitService.waitForMilliseconds(1500);

    click(
      "body > app > section > header > button.link-button.pna.launcher-open-btn"
    );

    waitService.waitForMilliseconds(500);

    click(historianButton);

    waitService.waitForMilliseconds(500);

    elementPresent(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)",
      "historian is opened"
    );
  });
  it("new trend", async () => {
    click(
      "#equipDrawer > div.app-equip.active-app > section > ul > li:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)",
      "New trend form is opened"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header > button:nth-child(1)"
    );

    waitService.waitForMilliseconds(1000);

    containsText("#equipList > li:nth-child(1) > a > h4", "City Center AHU-1");

    click("#equipList > li:nth-child(1) > a > h4");

    waitService.waitForMilliseconds(1000);

    click("#equipList > li:nth-child(1) > div > div.switch");

    click("#equipList > li:nth-child(2) > div > div.switch");

    click("#equipList > li:nth-child(4) > div > div.switch");

    waitService.waitForMilliseconds(1000);

    click("#relativeSelector");

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > footer > button.button-default.focused.margin.small.l-side"
    );

    waitService.waitForMilliseconds(1000);

    elementPresent("#trendsView > div > header > button:nth-child(1)");

    click("#trendsView > div > header > button:nth-child(1)");

    waitService.waitForMilliseconds(1000);
  });
  it("Launch View", async () => {
    click("#fooAppContainer > ul > li:nth-child(6)");

    waitService.waitForMilliseconds(1000);

    elementPresent("body > app > section", "view is open");

    waitService.waitForMilliseconds(1000);
  });
  it("Click on a trend", async () => {
    click(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div > div > div.amChartsLegend.amcharts-legend-div > svg > g > g > g.amcharts-legend-item-graph1 > rect"
    );

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div > div > div.amChartsLegend.amcharts-legend-div > svg > g > g > g.amcharts-legend-item-graph1 > rect",
      "chart is disabled"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Slider", async () => {
    click(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-scrollbar-chart-div > div > div > svg > g:nth-child(12) > g > rect:nth-child(8)"
    );

    mouseButtonDown(0);

    moveToElement(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-scrollbar-chart-div",
      1,
      10
    );

    mouseButtonUp();

    waitService.waitForMilliseconds(5000);
  });
  it("Refresh", async () => {
    click(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.horizontal.middle.right.margin.normal.t-side > button:nth-child(3)"
    );

    waitForElementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.chart-container.padded.small.all-sides.vertical.stretch.flex.non-auto > div.flex.non-auto.margin.normal.b-side > div > div > div.amcharts-panels-div > div > div > div.amcharts-chart-div",
      5000,
      "Refreshed"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("List button", async () => {
    click(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.horizontal.middle.right.margin.normal.t-side > button:nth-child(2)"
    );

    waitService.waitForMilliseconds(4000);

    elementPresent(
      "#trendsView > div > section > div.vertical.flex > div:nth-child(1) > div.data-grid-view.can-scroll.padded.small.v-sides.vertical.flex.non-auto > table > tbody > tr:nth-child(1) > td:nth-child(2)",
      "list view"
    );
  });
  it("Today actions-sliders", async () => {
    elementPresent(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.middle-button.flex.tall-text"
    );

    click(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.previous-button"
    );

    containsText(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.middle-button.flex.tall-text",
      "Yesterday"
    );

    click(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.next-button"
    );

    containsText(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.middle-button.flex.tall-text",
      "Today"
    );

    waitService.waitForMilliseconds(1000);
  });
  it("Today actions-button", async () => {
    click(
      "#trendsView > div > section > div.horizontal.middle.center.padded.normal.all-sides.period-selector-container.no-flex > div > button.middle-button.flex.tall-text"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div",
      "table is open"
    );

    elementPresent(
      ".date-item.horizontal.middle.center.flex.non-auto.mono.today.selected",
      "present date is highlighted"
    );

    waitService.waitForMilliseconds(1000);

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div.horizontal.middle.flex.period-selector-container > ul > li:nth-child(2)"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div.horizontal.middle.flex.period-selector-container > ul > li.date-period-selector.horizontal.middle.center.flex.non-bold.selected",
      "week"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div.horizontal.middle.flex.period-selector-container > ul > li:nth-child(3)"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div.horizontal.middle.flex.period-selector-container > ul > li.date-period-selector.horizontal.middle.center.flex.non-bold.selected",
      "month"
    );

    click(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div.horizontal.middle.flex.period-selector-container > ul > li:nth-child(4)"
    );

    elementPresent(
      "body > app > div.modal.vertical.middle.center > div > header.vertical.padded.small.v-sides > div.horizontal.middle.flex.period-selector-container > ul > li.date-period-selector.horizontal.middle.center.flex.non-bold.selected",
      "year"
    );

    waitService.waitForMilliseconds(1000);
  });
});
