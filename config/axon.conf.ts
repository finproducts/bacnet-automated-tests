import {browser, Config} from 'protractor';
import MainPage from '../page_objects/finstack/mainPage';



const headless:boolean = process.argv.indexOf('--headless') > 0;

let chromeSettings:any = {
  args :['--window-size=1920,1080']
}
let config:Config = {
  baseUrl: 'http://localhost:8080/finMobile/demo/',
  //baseUrl:'http://10.10.200.99:85/finMobile/demo#frame?app=graphics&url=%2Ffin5%2Fdemo%3FprojectRef%3D%401eedac62-ea569755%26title%3DshowSettings',
  specs:[
   

  '../specs/Bacnet_Fin5/bacnet/general/BacnetSim/readSim/testFinSyncHisPointsJob_READSIM.spec.js',
  //'../specs/Bacnet_Fin5/bacnet/general/BacnetSim/readSim/**/*.spec.js',
  ],

  capabilities: {
    browserName: 'chrome',
    acceptInsecureCerts: true,
		chromeOptions: chromeSettings
	},  
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() { }
  },

  allScriptsTimeout: 120000,
  directConnect: !process.env.SELENIUM_URL,
  onPrepare: () =>{
   
    const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
    jasmine.getEnv().addReporter(new SpecReporter({
      spec: {
        displayStacktrace: true
      }
    }));
    const HtmlReporter = require('protractor-beautiful-reporter');
    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory:'reports/',
      takeScreenShotsOnlyForFailedSpecs:true
    }).getJasmine2Reporter());

    var protractorMatchers = require('jasmine-protractor-matchers');
    beforeEach(function() {
        jasmine.addMatchers(protractorMatchers);
        browser.waitForAngularEnabled(false);
    });

    beforeAll(async function(){
      jasmine.addMatchers(protractorMatchers);
        browser.waitForAngularEnabled(false);
      const page = new MainPage();
      await page.open();  
      await page.login();
    })
  }
}

exports.config = config;