import {$} from 'protractor';
import { LoginPage } from "./loginPage";
import testData from '../../test_data/finstack';
import { waitService } from '../../helpers/wait-service';

export default class MainPage extends LoginPage
{
  mainFrame = $('#content');

  constructor(){
    super();
    this.url = testData.url
  }

  async open(){
    await super.open();
    await this.defaultWait();
  
  }

  async defaultWait(){
    await waitService.waitForMilliseconds(5000);
  }

  async login(){
    super.login(testData.auth.username, testData.auth.password);
    await this.defaultWait();
  }

}