import { browser, $, element, ExpectedConditions as EC } from "protractor";
import MainPage from "./mainPage";
import { waitService } from "../../helpers/wait-service";

export class LoginPage {
  userInput = $('#username');
  pwdInput = $('#password');
  submitBtn = $('#loginButton');
  //menu = $('button.link-button.pna.launcher-open-btn');

  url: string = '';
  constructor() {

  }

  async open(){
    await browser.get(this.url);
    browser.manage().window().maximize();
  }

  login(username: string, password: string) {
    waitService.headlessWaiter(this.userInput);
    this.userInput.clear();
    this.userInput.sendKeys(username);

    waitService.headlessWaiter(this.pwdInput);
    this.pwdInput.clear();
    this.pwdInput.sendKeys(password);

    waitService.headlessWaiter(this.submitBtn);
    this.submitBtn.click();
  }
}
